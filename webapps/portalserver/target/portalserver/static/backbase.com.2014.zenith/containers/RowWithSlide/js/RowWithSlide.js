/* globals b$ */
(function() {
    'use strict';
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
    //  ----------------------------------------------------------------
    var RowWithSlide = Container.extend(function(bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'RowWithSlide',
        namespaceURI: 'http://backbase.com/2014/zenith',
        // turning toggle off for now EL20141114
        // DOMReady: function() {
        //     $(this.getDisplay('mainNavArea')).on('mouseenter mouseleave', this.toggle.bind(this) );
        //     return Container.prototype.DOMReady.apply(this, arguments);
        // },
        toggle: function(event) {
            return;
            var oMainNav = this.getDisplay('mainNavArea'),
                contentArea = this.getDisplay('contentArea'),
                callFn  = ['mouseenter','mouseover'].indexOf(event.type) >= 0 ? 'addClass' : 'removeClass',
                animateMenu = function() {
                    b$._private.htmlAPI[ callFn ](oMainNav, 'z-rowWithSlide-open');
                    b$._private.htmlAPI[ callFn ](contentArea, 'z-rowWithSlide-openen');
                };

            clearTimeout(this.animTimer);

            this.animTimer = setTimeout(function() {
                animateMenu();
            }, 300);
        }
    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            var sTemplate = backbase_com_2014_zenith.RowWithSlide(data);
            return sTemplate;
        },
        handlers: {
            'DOMNodeInsertedIntoDocument': function(event) {
                // fix for IE8 calc() css rule
                if (event.target === this && navigator.appVersion.indexOf('MSIE 8') !== -1) {
                    var myHeight = b$._private.htmlAPI.getBoxObject(this.htmlNode, 'border');
                    var oContentArea = this.getDisplay('content');
                    oContentArea.style.height = (myHeight.h - 60) + 'px';
                }
            }
        }
    });
})();
