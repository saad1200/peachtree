(function () {
	'use strict';
	var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
//  ----------------------------------------------------------------
	var SimpleTabBox = Container.extend(function (bdomDocument, node) {
		Container.apply(this, arguments);
		this.isPossibleDragTarget = true;
	}, {
		localName: 'SimpleTabBox',
		namespaceURI: 'http://backbase.com/2014/zenith',
		selectTab: function(iIndex) {
			b$._private.htmlAPI.removeClass(this.getDisplay('headSelected'), 'bp-SimpleTabBox--headSelected');
			b$._private.htmlAPI.removeClass(this.getDisplay('bodySelected'), 'bp-SimpleTabBox--bodySelected');

			if (this.htmlAreas[iIndex]) {
				b$._private.htmlAPI.addClass(this.htmlAreas[iIndex], 'bp-SimpleTabBox--bodySelected');
				b$._private.htmlAPI.addClass(this.getDisplay('tab', true)[iIndex], 'bp-SimpleTabBox--headSelected');
			};
		}
	}, {
		template: function(json) {
			var data = {item: json.model.originalItem};
			var sTemplate = backbase_com_2014_zenith.SimpleTabBox(data);
			return sTemplate;
		},
		handlers: {
			click: function(event) {
				if (event.target === this) {
					var oTab = b$.ua.queryAncestor(event.htmlTarget, '.bp-SimpleTabBox--tab');
					if (oTab) {
						event.preventDefault();
						this.selectTab(parseInt(oTab.getAttribute('data-tabnr'), 10));
					};
				};
			}
		}
	});
})();