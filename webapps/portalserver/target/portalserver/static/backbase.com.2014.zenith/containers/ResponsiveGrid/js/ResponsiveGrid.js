(function () {
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container'),
        errorMessageTPL = '<tr><td colspan="3"><ul class="bp-ResponsiveGrid-errormsg" style="color:red;display:none"></ul></td></tr>',
        errorMessages = {
            doesNotMatchColumns: 'The number of columns and number of column widths do not match.',
            notAllowedValueMarginTop: 'The value for margin top is not allowed put the value in pixels.'
        },
        getLengthOfNumber = function (num){
            return String(num).length;
        },
        validators = {
            matchColumns: function (csPref, numberOfColumns) {
                return true;
            },
            suffix: function (input, expectedSuffix, softCheck){
                var suffix,
                    numberStr = input.value,
                    number = parseInt(numberStr, 10),
                    lenNumber,
                    result = false;

                if(!isNaN(number)){
                    lenNumber = getLengthOfNumber(number);

                    if (lenNumber !== numberStr.length) {
                        suffix = numberStr.substr(lenNumber);
                        if (suffix === expectedSuffix) {
                            result = true;
                        }
                    }else{
                        input.value = numberStr + expectedSuffix;
                        result = true;
                    }
                }else if(numberStr.length === 0 && softCheck){
                    result = true;
                }

                return result;
            }
        },
        ResponsiveGrid = Container.extend(function (bdomDocument, node) {
                Container.apply(this, arguments);
                this.isPossibleDragTarget = true;
            },
            {
                localName: 'ResponsiveGrid',
                namespaceURI: 'http://backbase.com/2014/zenith',
                preparePrefs: function () {
                    var aPrefs, order, aNewPrefs, i, j, orderLen, prefsLen;

                    // filter preferences for view
                    aPrefs = b$.portal.portalModel.filterPreferences(this.model.preferences.array);
                    prefsLen = aPrefs.length;

                    // the order to appear in form
                    order = ['nColumns', 'columns_span_xs', 'columns_span_sm', 'columns_span_md', 'columns_span_lg', 'marginTop'];
                    orderLen = order.length;
                    aNewPrefs = [];

                    for (i = 0; i < orderLen; i++) {
                        for (j = 0; j < prefsLen; j++) {
                            if (order[i] == aPrefs[j].name) {
                                aNewPrefs.push(aPrefs[j]);
                            }
                        }
                    }

                    // error message to be shown when column and number of width values do not match
                    aNewPrefs.push(errorMessageTPL);

                    return aNewPrefs;
                },
                renderErrors: function (oForm, errors) {
                    var errorContainer = b$.ua.querySelector(oForm.htmlNode, '.bp-ResponsiveGrid-errormsg');
                    errorContainer.innerHTML = '';
                    if (errors.length) {
                        errors.forEach(function (key) {
                            var error = document.createElement('li'),
                                message = document.createTextNode(errorMessages[key]);
                            error.appendChild(message);
                            errorContainer.appendChild(error);
                        });
                        errorContainer.style.display = "";
                    } else {
                        errorContainer.style.display = 'none';
                    }
                },
                validateValues: function (oForm, event) { // validates if the number of columns is equal to number of column widths otherwise show error msg
                    // get values from the form
                    var numberOfColumns = oForm.htmlNode.elements['nColumns'].value,
                        columnsSpans = { },
                        mediaQueries = ['xs', 'sm', 'md', 'lg'],
                        marginTop = oForm.htmlNode.elements['marginTop'],
                        expectedSufix = 'px',
                        errors = [],
                        colSpanKey;

                    for (var i = 0; i < mediaQueries.length; i++) {
                        colSpanKey = 'columns_span_' + mediaQueries[i];
                        columnsSpans[colSpanKey] = oForm.htmlNode.elements[colSpanKey].value;
                    }

                    if (!validators.matchColumns(columnsSpans.xs, numberOfColumns) ||
                        !validators.matchColumns(columnsSpans.sm, numberOfColumns) ||
                        !validators.matchColumns(columnsSpans.md, numberOfColumns) ||
                        !validators.matchColumns(columnsSpans.lg, numberOfColumns) ) {
                        errors.push('doesNotMatchColumns');
                    }

                    if(!validators.suffix(marginTop, expectedSufix, true)){
                        errors.push('notAllowedValueMarginTop');
                    }

                    if (errors.length) {
                        this.renderErrors(oForm, errors);
                        if (event) {
                            event.stopImmediatePropagation();
                        }
                    }
                }
            },
            {
                template: function (json) {
                    var data = {item: json.model.originalItem};
                    var sTemplate = window.templates_ResponsiveGrid.ResponsiveGrid(data);
                    return sTemplate;
                },
                handlers: {
                    'preferences-form': function (event) {
                        var aNewPrefs;
                        if (event.target == this) {
                            aNewPrefs = this.preparePrefs();
                            event.detail.customPrefsModel = aNewPrefs;
                        }
                    },
                    'preferenceFormReady': function (event) {
                        var oContainer = event.target.parentNode, i, elementsLen;
                        if (oContainer == this) {
                            b$._private.html.addEventListener(event.target, 'save', function (evt) {
                                oContainer.validateValues(event.target, evt);
                            }, false);
                            elementsLen = event.target.htmlNode.elements.length
                            // setting change listener on input for IE8
                            for (i = 0; i < elementsLen; i++) {
                                b$._private.html.addEventListener(event.target.htmlNode.elements[i], 'change', function () {
                                    oContainer.validateValues(event.target);
                                }, false);
                            }

                            oContainer.validateValues(event.target);
                        }
                    },
                    'savePreferenceForm': function (event) {
                        var oContainer = event.target.parentNode,
                            numberOfColumns,
                            fullSeries;
                        if (oContainer == this) {
                            numberOfColumns = parseInt(event.detail.nColumns);
                        }
                    },
                    'preferencesSaved': function (event) {
                        var oContainer = event.target;
                        if (oContainer == this) {
                            event.target.refreshHTML();
                        }
                    }
                }
            }
        );
})();