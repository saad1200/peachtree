(function () {
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');

    var ResizeableTwoColumn = Container.extend(function (bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'ResizeableTwoColumn',
        namespaceURI: 'http://backbase.com/2014/zenith',
        toggleSidebar: function() {
            var bOpen = $(this.htmlNode).hasClass('bp-resizeable-two-column-sidebar-hide');
            var that = this;
            var scButton = $(document).find('.bc-search-cancel-button');

            if (!bOpen) {

                var bdPageContainerWide = $('.bd-pageContainer').hasClass('bd-pagesLayoutWide');
                if(bdPageContainerWide) {
                    $('.bd-pageContainer').removeClass('bd-pagesLayoutWide');
                    $('.bd-pageSettingsContainer').css('right', '0');
                }

                $(this.getDisplay('left')).animate({
                    width: '0%'
                }, {
                    duration: 100,
                    easing: 'linear',
                    complete: function() {
                        this.style.overflow = 'visible';
                        $('.bd-pageScrollContainer').css({'position': 'relative'});
                        $(that.htmlNode).addClass('bp-resizeable-two-column-sidebar-hide');
                        that.setPreference('sidebar', 'false');
                        that.setPreference('colSize', '0');
                        that.model.save();
                    }
                });
                $(this.getDisplay('right')).animate({
                    width: '100%'
                }, {
                    duration: 100,
                    easing: 'linear'
                });
            } else {
                $(this.getDisplay('left')).animate({
                    width: '23%'
                }, {
                    duration: 100,
                    easing: 'linear',
                    complete: function() {
                        this.style.overflow = 'visible';
                        $(that.htmlNode).removeClass('bp-resizeable-two-column-sidebar-hide');
                        that.setPreference('sidebar', 'true');
                        that.setPreference('colSize', 'invalid');
                        that.model.save();
                    }
                });
                $(this.getDisplay('right')).animate({
                    width: '77%'
                }, {
                    duration: 100,
                    easing: 'linear'
                });
            }
        },
        toggleRow: function(row, internal) {
            var animation = { duration: 100, easing: 'linear' };
            var bOpen = $(row).hasClass('bp-resizeable-two-column-open');
            var row1 = this.getDisplay('row1');
            var row2 = this.getDisplay('row2');
            var row3 = this.getDisplay('row3');
            var prefName = row == row1 ? 'row1' : 'row3';
            var row1IsOpened = this.getPreference('row1') == 'true';
            var row3IsOpened = this.getPreference('row3') == 'true';

            var closedHeight = 39;
            var openedRow1Height = 119;
            var openedRow3Height = 183;
            var openedHeight = row == row1 ? openedRow1Height : openedRow3Height;
            var newRowHeight = bOpen ? closedHeight : openedHeight;

            var isClosing = row == row1 ? row1IsOpened : row3IsOpened;
            var currentRow2Height = b$._private.htmlAPI.getBoxObject(row2).h;
            var newRow2Height = currentRow2Height + (isClosing ? 1 : -1) * (openedHeight - closedHeight);

            $.when(
                $(row).animate({ height: newRowHeight + 'px' }, animation).promise(),
                $(row2).animate({ height: newRow2Height + 'px' }, animation).promise()
            ).then(function () {
                $(row).toggleClass('bp-resizeable-two-column-open', !bOpen);
                this.setPreference(prefName, (!bOpen).toString());
                this.model.save();
                if (internal) {
                    var oEvent = this.ownerDocument.createEvent('CustomEvent');
                    oEvent.initCustomEvent('RTCToggleRow', true, true, {
                       'sidebar': this.getPreference('sidebar'),
                       'row1': this.getPreference('row1'),
                       'row3': this.getPreference('row3')
                    });
                    this.dispatchEvent(oEvent);
                }
            }.bind(this));
        },
        resizeColumn: function(event) {
            var that = this,
                newSizeL = null,
                oLeftSide = that.getDisplay('left'),
                oDivider = that.getDisplay('divider'),
                oRightSide = that.getDisplay('right'),

                // hide search cancel button if it is visible on resize column.
                scButton = $(document).find('.bc-search-cancel-button');



            var resize = function(event) {
                event.preventDefault();

                newSizeL = event.clientX - 60;

                if (newSizeL < 280) {
                    newSizeL = 280;
                    return;
                }

                //Make the diver much wider so that dragging using mouseover will not drop outside of divider and stop the drag
                var dividerProtectorWidth = 560;

                oDivider.style.width = dividerProtectorWidth + 'px';
                oDivider.style.left = (newSizeL - dividerProtectorWidth / 2) + 14 + 'px';
                oLeftSide.style.width = newSizeL + 'px';
                oRightSide.style.width = 'calc(100% - '+newSizeL+'px)';
            };

            var stopResizing = function(event) {
                event.preventDefault();

                that.setPreference('colSize', newSizeL);
                that.model.save();

                $(document.body).off('mousemove', resize);
                $(document.body).off('mouseup', stopResizing);
                $(document.body).off('mouseleave','.bp-resizeable-two-column--divider', stopResizing);

                oDivider.style.width = '5px';
                oDivider.style.left = '';
            };

            $(document.body).on('mousemove', resize);
            $(document.body).on('mouseup', stopResizing);
            $(document.body).on('mouseleave','.bp-resizeable-two-column--divider', stopResizing);

        }
    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            return backbase_com_2014_zenith.ResizeableTwoColumn(data);
        },
        handlers: {
            'click': function(event) {
                if (event.target === this && ($(event.htmlTarget).hasClass('bd-resize-buttons-right') || $(event.htmlTarget).hasClass('bd-resize-buttons-left'))) {
                    this.toggleSidebar();
                } else if (event.target === this && $(event.htmlTarget).hasClass('bd-resize-buttons-toggle')) {
                    if (b$.ua.queryAncestor(event.htmlTarget, '.bp-resizeable-two-column--row1')) {
                        this.toggleRow(this.getDisplay('row1'), true);
                    } else if (b$.ua.queryAncestor(event.htmlTarget, '.bp-resizeable-two-column--row3')) {
                        this.toggleRow(this.getDisplay('row3'), true);
                    }
                }
            },
            'mousedown': function(event) {
                if (event.target === this && (event.htmlTarget == this.getDisplay('divider') || event.htmlTarget.parentNode == this.getDisplay('divider') || event.htmlTarget.className.indexOf('bi-pm-grippy-left') !== -1)) {
                    var that = this;
                    that.resizeColumn(event);
                    event.preventDefault();
            }
            },
            'WToggleRow': function(event) {
                if (b$.ua.queryAncestor(event.target.htmlNode, '.bp-resizeable-two-column--row1')) {
                    if (event.detail.open !== (this.getPreference('row1') == 'true')) {
                        this.toggleRow(this.getDisplay('row1'));
                    }
                } else {
                    if (event.detail.open !== (this.getPreference('row3') == 'true')) {
                        this.toggleRow(this.getDisplay('row3'));
                    }
                }
            }
        }
    });
})();
