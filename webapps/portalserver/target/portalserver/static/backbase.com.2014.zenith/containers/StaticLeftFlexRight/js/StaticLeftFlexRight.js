/* globals b$ */
(function() {
    'use strict';
    var Container = b$.bdom.getNamespace('http://backbase.com/2013/portalView').getClass('container');
    //  ----------------------------------------------------------------
    var StaticLeftFlexRight = Container.extend(function(bdomDocument, node) {
        Container.apply(this, arguments);
        this.isPossibleDragTarget = true;
    }, {
        localName: 'StaticLeftFlexRight',
        namespaceURI: 'http://backbase.com/2014/zenith'
    }, {
        template: function(json) {
            var data = {item: json.model.originalItem};
            var sTemplate = backbase_com_2014_zenith.StaticLeftFlexRight(data);
            return sTemplate;
        }
    });
})();
