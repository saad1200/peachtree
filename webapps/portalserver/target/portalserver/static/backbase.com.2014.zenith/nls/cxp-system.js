/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : boilerplate.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */
define({
    root: {
        sessionTimeout: {
            'heading': 'You\'re Idle. Do Something! ',
            'message': 'You\'ll be logged out in {{countdown}} second(s).'
        }
    },
    'nl-nl': true
});
