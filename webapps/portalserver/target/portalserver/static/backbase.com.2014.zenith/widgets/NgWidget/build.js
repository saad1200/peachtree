({
    name: 'zenith/widgets/NgWidget/scripts/index',
    namespace: false,
    allowSourceOverwrites: false,
    baseUrl: '../../../', // back to webapp/
    paths: {
        'zenith/widgets/NgWidget': 'backbase.com.2014.zenith/widgets/NgWidget', // include everything from Widget
        'zenith': 'empty:', // Exclude
        'jquery': 'empty:', // Exclude
        'angular': 'empty:', // Exclude
        'ng': 'empty:', // Exclude
        'mustache': 'empty:', // Exclude
        'ext-lib': 'empty:', // Exclude
        'tpl': 'ext-lib/requirejs/require-tpl',  // Exclude
        'text'   : 'ext-lib/requirejs/require-text', // Exclude
        'i18n': 'ext-lib/requirejs/require-i18n' // Exclude
    },
    exclude: ['text','tpl','i18n'],
    preserveLicenseComments: false,
    useStrict: true,
    outSourceMap: true,
    wrap: true,
    findNestedDependencies: true,
    // optimize: 'none',
    // appDir: './',
    // dir: 'build'
    out: 'build/index.js'
})
