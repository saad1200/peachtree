/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : i18n.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

define({
    root: {

        LABELS: {
            Demo: 'Good day',
            EventsTitle: 'External Widget events'
        },

         SYSTEM: {

        }
    },

    'nl-nl': true
});
