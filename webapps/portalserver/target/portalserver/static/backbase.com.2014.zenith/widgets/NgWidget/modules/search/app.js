/* globals define, angular */
/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : app.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

define([
    'angular'
], function() {

    'use strict';

    function TemplateService($rootScope, $http, $q, $templateCache) {
        var templatesPath, templateMap, templateUrl, getTemplate;

        templatesPath = $rootScope.paths.mod + 'search/templates';
        templateMap = {
            search: '/bb-widget-search.html'
        };

        getTemplate = function(tpl, returnPromise) {
            templateUrl = templatesPath + templateMap[tpl];
            if (returnPromise) {
                return $q.when($templateCache.get(templateUrl) || $http.get(templateUrl, {
                    cache: true
                }));
            } else {
                return templateUrl;
            }
        };
        return {
            getTemplate: getTemplate
        };
    }

    function truncate() {
        return function(text, length) {
            if (text) {
                var ellipsis = text.length > length ? '...' : '';
                return text.slice(0, length) + ellipsis;
            }
            return text;
        };
    }


    function WidgetSearch(templateService) {

        var linkFn, ctrl;

        linkFn = function(scope, element, attrs, ctrl) {
            //debugger;
            var searchInput = element.find('.bb-widget-search-input');
            var onSubmitSearch = scope.onSubmitSearch;
            searchInput.val(scope.ngModel);
            element.bind('click', function(ev) {
                if (element.hasClass('bb-widget-search-open') && searchInput.val() !== '' && searchInput[0] !== ev.target) {
                    scope.$apply(function() {
                        if (angular.isFunction(onSubmitSearch)) {
                            onSubmitSearch.call(null, {query: searchInput.val() });
                        }
                    });
                    ev.stopPropagation();
                    ev.preventDefault();
                } else if (searchInput[0] !== ev.target) {
                    element.toggleClass('bb-widget-search-open');
                }
            });
        };

        ctrl = function($scope, $element, $attrs, $transclude) {
            $scope.ceva = 'Bla';
        };

        return {
            restrict: 'E',
            templateUrl: templateService.getTemplate('search'),
            replace: false,
            priority: 0,
            transclude: true,
            scope: {
                ngModel: '=',
                placeholder: '@',
                onSubmitSearch: '&'
            },
            terminal: false,
            require: '?ngModel',
            controller: ctrl,
            // Its job is to bind a scope with a DOM resulted in a 2-way data binding.
            // You have access to scope here unlike compile function so that you can
            // create custom listeners using $watch method.

            link: linkFn
            // Compile function produces a link function. This is the place where
            // you can do the DOM manipulation mostly.
            /*
            compile: function compile(tElement, tAttrs, transclude) {
              return {
                pre: function preLink(scope, iElement, iAttrs, controller) { ... },
                post: function postLink(scope, iElement, iAttrs, controller) { ... }
              }
            }
            */

        };
    }

    var name = 'ngWidget.bbSearch',
        deps = [];

    return angular.module(name, deps)
        .service('templateService', ['$rootScope', '$http', '$q', '$templateCache', TemplateService])
        .directive('bbWidgetSearch', ['templateService', WidgetSearch])
        .filter('truncate', [truncate]);

});
