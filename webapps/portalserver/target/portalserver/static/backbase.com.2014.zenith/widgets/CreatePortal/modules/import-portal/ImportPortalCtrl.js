/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : ImportPortalCtrl.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */


define(function(require, exports, module) {

    'use strict';

    var labels = require('i18n!zenith/widgets/CreatePortal/modules/import-portal/nls/labels'),
        config = require('zenith/core/config'),
        bus = require('zenith/bus'),
        xml2json = require('zenith/utils/xml2json'),
        jsonpointer = require('jsonpointer'),

        IMPORT_API_URL = config.contextRoot + '/orchestrator/import/upload',
        importData = {};


    /**
     * [ModalInstanceCtrl description]
     * @param {[type]} $scope         [description]
     * @param {[type]} $modalInstance [description]
     */
    var ModalInstanceCtrl = function($scope, $modalInstance, $bbModal) {

        var importItem = null,
            processing = false,
            processNtf;

        var fileUploadConfig = {
            url: IMPORT_API_URL,
            //singleFileUploads: true,

            add: function (files, uploadFiles) {
                var file = files[0];
                // console.log('add', file);

                if(file.name.indexOf('.zip') === -1) {
                    $bbModal.notify({
                        icon: 'error',
                        text: labels.IMPORT_TXT_WRONG_FORMAT
                    });
                    importData.label = labels.IMPORT_TXT_DROP;
                    importItem = null;
                    $scope.disabled = true;

                } else {
                    importData.label = file.name;
                    importItem = uploadFiles;
                    $scope.disabled = false;
                }
                $scope.$apply();
            },
            progress: function(progress, index, file){
                // console.log('progress:', index, file, progress);
            },

            // check ie uploder state
            checkIframeResponse: function(response){
                // console.log(response);
                return response.indexOf('HTTP ERROR') == -1;
            }
        };


        importData.label = labels.IMPORT_TXT_DROP;
        $scope.importData = importData;
        $scope.labels = labels;
        $scope.disabled = true;
        $scope.fileUploadConfig = fileUploadConfig;


        $scope.cancel = function() {
            if(processNtf) {
                processNtf.hide();
            }
            $modalInstance.dismiss('cancel');
        };


        $scope.importItem = function(evt) {
            if (importItem){
                processing = true;

                if(processNtf) {
                    processNtf.hide();
                }
                // Display progress bar
                processNtf = $bbModal.notify({
                    icon: 'loading',
                    text: labels.IMPORT_TXT_NOTIFICATION,
                    delay: -1
                });

                importItem().then(function(response){
                    var responseJson = xml2json(response),
                        responseStatus = jsonpointer.get(responseJson, '/backbaseArchiveImportResponse/status'),
                        missingContents = jsonpointer.get(responseJson, '/backbaseArchiveImportResponse/missingContents'),
                        missingPortals = jsonpointer.get(responseJson, '/backbaseArchiveImportResponse/missingPortals'),
                        missingRepositories = jsonpointer.get(responseJson, '/backbaseArchiveImportResponse/missingRepositories'),
                        messageIcon,
                        messageText;

                    if (responseStatus === 'OK') {
                        messageIcon = 'success';
                        messageText = labels.IMPORT_TXT_SUCCESS_TITLE;
                    } else {
                        messageIcon = 'error';
                        messageText = '<ul>';
                        if (missingContents) {
                            messageText += '<li>Not all content items could be imported - check logs</li>';
                        }
                        if (missingPortals) {
                            messageText += '<li>Not all portals could be imported - check logs</li>';
                        }
                        if (missingRepositories) {
                            messageText += '<li>Not all portal repositories could be imported - check logs</li>';
                        }
                        messageText += '</ul>';
                    }

                    $bbModal.notify({
                        icon: messageIcon,
                        text: messageText
                    });

                    $modalInstance.close();

                }).fail(function(response){
                    var message = response && response.message || {},
                        error = response && response.error || {};

                    // TODO: track missing resources
                    // if(message === 'ERROR_IMPORTING_TO_PORTAL_SERVER'){
                    //     console.log('missing resources');
                    // }

                    $bbModal.notify({
                        icon: 'error',
                        text: error ? error + ' : ' + message : message
                    });

                }).always(function(){
                    processing = false;
                    processNtf.hide();
                });


            }
        };

    };


    /**
     * [ImportPortalCtrl description]
     * @param {[type]} $modal          [description]
     * @param {[type]} $rootScope      [description]
     */

    var ImportPortalCtrl = function($modal, $rootScope, WIDGET_EVENTS) {

        function importModal () {
            var modalInstance = $modal.open({
                templateUrl: $rootScope.importPortalPaths.tpl + 'import-portal-modal.ng.html',
                controller: [ '$scope', '$modalInstance', '$bbModal' , ModalInstanceCtrl],
                windowClass: 'importPortal-modal',
                backdrop: 'static',
                resolve: {

                }
            });

            modalInstance.result.then(function(response) {
                bus.publish(WIDGET_EVENTS.portalImported);
            }, function() {
                // console.info('Cancel');
            });
        }

        this.labels = labels;
        this.importModal = importModal;
    };



    return ['$modal', '$rootScope', 'WIDGET_EVENTS',  ImportPortalCtrl];

});
