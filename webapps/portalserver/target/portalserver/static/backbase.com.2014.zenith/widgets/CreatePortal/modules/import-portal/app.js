/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : app.js
 *  Description:
 *  Import Portal Module App
 *  ----------------------------------------------------------------
 */
define(function(require, exports, module) {

    'use strict';

    /*----------------------------------------------------------------*/
    /*  Import
    /*----------------------------------------------------------------*/
    var ng = require('angular'),
        config = require('zenith/core/config'),
        staticPath = config.contextRoot + '/static/backbase.com.2014.zenith/widgets/CreatePortal/modules/import-portal/',
        ImportPortalCtrl = require('zenith/widgets/CreatePortal/modules/import-portal/ImportPortalCtrl'),
        Fileupload = require('backbase.com.2014.components/modules/fileupload/scripts/app'),
        Components = require('backbase.com.2014.components/scripts/app');

    require('backbase.com.2014.components/scripts/modal');

    /*----------------------------------------------------------------*/
    /* Main App
    /*----------------------------------------------------------------*/

    var name = 'cxpImportPortal';
    var deps = [
        'backbase.com.2014.components', // provides ui.bootstrap
        'bbModal',
        Fileupload.name
    ];

    function Configure() {
        // do upfront configuration
    }

    function Runner($rootScope) {
        $rootScope.importPortalPaths = {
            tpl : staticPath + 'templates/'
        };
    }


    module.exports =  ng.module(name, deps)
        .config([ Configure ])
        .controller('ImportPortalCtrl' ,ImportPortalCtrl)
        .run([ '$rootScope', Runner ]);
});
