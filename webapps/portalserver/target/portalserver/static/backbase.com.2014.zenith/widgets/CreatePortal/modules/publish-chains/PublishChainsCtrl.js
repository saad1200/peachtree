/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : PubChainCtrl.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

define([
    'zenith/utils/url'
], function(urlUtils) {
    'use strict';

    var PublishChainsCtrl = function($scope, $log, PublishChainsSrv) {

        this.options = [];
        this.label = '';
        this.pubChainsConfig = [];
        this.portalPubChains = [];
        //portalName indicate that it is create portal or edit the setting of the portal
        this.portalName = urlUtils.getUrl2State()[0];
        this.portalName = this.portalName && this.portalName !== '_' ? this.portalName : '' ;

        var self = this,
            resetData = function() {
                self.options = [];
                self.label = '';
            },
            loop = function(callback) {
                var i,l,c;
                for(i = 0, l = self.options.length; i < l; i++){
                    c = self.options[i];
                    if(callback(i, c)) break;
                }
            },
            getPublishChainsConfig = function() {
                return PublishChainsSrv.getPubChainsConfig().then(
                    function(data) {
                        // console.log('Config', data);
                        self.pubChainsConfig = data;
                    },
                    function(err) {
                        $log.log(err);
                    }
                );

            },
            getPortalPublishChains = function(portalName) {
                return PublishChainsSrv.getPortalPubChains(portalName).then(
                    function(data) {
                        // console.log('Portal', data);
                        self.portalPubChains = data;
                    },
                    function(err) {
                        $log.log(err);
                    }
                );
            },
            getSelectedPubChains = function() {
                return PublishChainsSrv.getSelectedPubChains(self.portalName).then(
                    function(data) {
                        // console.log('Selected', data);
                        self.portalPubChains = data.portalPubChains;
                        self.pubChainsConfig = data.pubChainsConfig;
                    },
                    function(err) {
                        $log.log(err);
                    }
                );
            },
            getOptions = function(isUpdatePortal) {
                resetData();
                if(isUpdatePortal || self.portalName){
                    getSelectedPubChains().then(function () {
                        initDDOptions(self.pubChainsConfig);
                        updateDDOption(self.portalPubChains);
                        updateLabel();
                    });
                }else{
                    getPublishChainsConfig().then(function () {
                        initDDOptions(self.pubChainsConfig);
                        updateLabel();
                        $scope.$emit('publishChains.value', getOptionValue());
                    });
                }
            },
            initDDOptions = function(data) {
                var i,l,c, checked = false;
                for(i = 0, l = data.length; i < l; i++){
                    c = data[i];
                    //select the first chain on portal creation
                    if(!self.portalName && i === 0){
                        checked = true;
                    }else{
                        checked = false;
                    }
                    self.options.push({
                        name: 'publishChains',
                        text : c.publishChainName,
                        value : c.publishChainName,
                        checked : checked
                    });
                }
            },
            updateDDOption = function(data) {
                var i,l,c, tmpMap = {};
                //turn array into map for fast mapping
                for(i = 0, l = data.length; i < l; i++){
                    c = data[i];
                    tmpMap[c] = true;
                }

                loop(function(idx, opt) {
                    self.options[idx].checked = tmpMap[opt.value] ? true : false;
                });
            },
            getOptionValue = function() {
                var results = [];

                loop(function(idx, opt) {
                    if(opt.checked){
                        results.push(opt.value);
                    }
                });

                return {
                    isChanged : self.portalName ? isPublishChainsChanged(results) : false,
                    value : results.join(';')
                };
            },
            isPublishChainsChanged = function(data) {
                var i,l, isChanged = false,
                    oldVal = (self.portalPubChains.slice(0)).sort(),
                    newVal = (data.slice(0)).sort();

                if(oldVal.length === newVal.length){
                    for(i = 0, l = oldVal.length; i < l; i++){
                        if(oldVal[i] !== newVal[i]){
                            isChanged = true;
                            break;
                        }
                    }
                }else{
                    isChanged = true;
                }

                return isChanged;
            },
            //TODO: find a better way to change the label ( for sure not in the controller )
            updateLabel = function() {
                var count = 0;

                if(self.options.length){
                    loop(function(idx, opt) {
                        if(opt.checked){
                            count += 1;
                        }
                    });

                    if(count < 1){
                        self.label = 'Select a publish chain';
                    }else if(count === 1){
                        self.label = count + ' chain';
                    }else{
                        self.label = count + ' chains';
                    }
                }else{
                    self.label = 'No publish chain available' ;
                }
            };


        this.change = function(opt) {
            updateLabel();
            $scope.$emit('publishChains.value', getOptionValue());
        };

        getOptions();


    };

    return ['$scope', '$log', 'PublishChainsSrv', PublishChainsCtrl];

});
