/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : default.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */
define({
    root: {
        SYSTEM : {
            VALIDATION : {
                'specialChars': 'Name cannot contain special characters or reserved portal words.',
                'exceedMaxLength': 'Encoded portal name has exceeded maximum length of 100 characters.'
            }
        }
    }
});
