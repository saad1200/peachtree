/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : PubChainSrv.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

define([
    'zenith/core/config',
    'zenith/http'
], function(config, Http) {
    'use strict';

    var PublishChainsSrv = function($q) {
        var self = this;


        this.getPortalPubChains = function(portalName) {
            var defer = $q.defer();
            //TODO: portalName maybe optional in the future
            if(!portalName){
                defer.reject('Portal Name not exists');
            }else{
                Http({
                    url: config.contextRoot + '/portals/'+portalName+'.xml?pc=false',
                    convertToJson: true
                }).then(
                    function(data) {
                        var pubChains = data.portal.properties.publishChains ? data.portal.properties.publishChains : {};
                        pubChains = typeof pubChains.value === 'string' || pubChains.value instanceof String ?
                            pubChains.value.split(';') : [];
                        defer.resolve( pubChains );
                    },
                    function(err) {
                        defer.reject(err);
                    }
                );
            }

            return defer.promise;
        };

        this.getPubChainsConfig = function() {
            var defer = $q.defer();

            Http({
                url: config.contextRoot + '/orchestrator/configuration',
                convertToJson: true
            }).then(
                function(data) {
                    var pubChains = data.configuration.publishChains && data.configuration.publishChains.publishChain ?
                            data.configuration.publishChains.publishChain : [];

                    defer.resolve( pubChains );
                },
                function(err) {
                    defer.reject(err);
                }
            );

            return defer.promise;
        };


        this.getSelectedPubChains = function(portalName) {
            var defer = $q.defer();

            $q.all({
                portalPubChains : self.getPortalPubChains(portalName),
                pubChainsConfig : self.getPubChainsConfig()
            }).then(
                function(data) {
                    var portalPubChains = data.portalPubChains,
                        pubChainsConfig = data.pubChainsConfig,
                        i,l,c, tmpMap = {}, results = {};
                    //turn array into map for fast mapping
                    for(i = 0, l = portalPubChains.length; i < l; i++){
                        c = portalPubChains[i];
                        tmpMap[c] = true;
                    }

                    for(i = 0, l = pubChainsConfig.length; i < l; i++){
                        c = pubChainsConfig[i].publishChainName;
                        results[c] = tmpMap[c] ? true : false;
                    }

                    defer.resolve( {
                        portalPubChains : portalPubChains,
                        pubChainsConfig : pubChainsConfig,
                        selectedPubChains : results
                    } );
                },
                function(err) {
                    defer.reject(err);
                }
            );

            return defer.promise;
        };

    };

    return ['$q', PublishChainsSrv];
});
