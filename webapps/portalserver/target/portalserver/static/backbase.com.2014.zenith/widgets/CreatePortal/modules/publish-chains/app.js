/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : app.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */
define(function(require, exports, module) {
    'use strict';

    var
        name = 'PublishChains',
        ng = require('angular'),
        //config = require('zenith/core/config'),
        CheckboxMultiSelect = require('backbase.com.2014.components/scripts/checkboxMultiSelect'),
        PublishChainsCtrl = require('zenith/widgets/CreatePortal/modules/publish-chains/PublishChainsCtrl'),
        PublishChainsSrv = require('zenith/widgets/CreatePortal/modules/publish-chains/PublishChainsSrv'),
        // debugger;
        deps = [
            'bbMultiSelect.checkBox'
        ];

    module.exports = ng.module(name, deps)
        .controller('PublishChainsCtrl', PublishChainsCtrl)
        .service('PublishChainsSrv', PublishChainsSrv);

});
