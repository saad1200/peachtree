/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : i18n.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

define({
    root: {

        LABELS: {
            logoutTitle: 'Click to logout',
            aboutModalTitle: 'Support and About CXP Manager',
            fullScreenTitle: 'Toggle Full Screen',
            whatsNew: 'What is New',
            quickGuide: 'Quick Guide',
            myBB: 'My Backbase',
            cxp: 'Backbase customer experience platform manager'
        },

         SYSTEM: {

        }
    }
});
