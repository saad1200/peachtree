/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : app.js
 *  Description: MIgration of PublishApp
 *  ----------------------------------------------------------------
 */
define(function(require, exports, module) {

    'use strict';
    /*----------------------------------------------------------------*/
    /* Deps & Declaring vars & setting intitial states
    /*----------------------------------------------------------------*/
    var Promise = require('zenith/promise'),
        httpPortals = require('zenith/http/portals'),
        item = require('zenith/utils/item'),
        error = require('zenith/core/errors-handler'),
        portalName = item.url2state('portalName'),
        portalDetails;

    /*----------------------------------------------------------------*/
    /* Get data
    /*----------------------------------------------------------------*/
    if (portalName) {
        portalDetails = httpPortals.portalDetails(portalName === '_' ? 'dashboard' : portalName);
    }

    /*----------------------------------------------------------------*/
    /* Start the widget
    /*----------------------------------------------------------------*/
     exports.init = function($widget) {
        Promise.all(portalDetails)
            .then(function(portal) {
                if(portal) {
                    // legacy
                    bd.selectedPortalName = portal.name;
                    bd.selectedPortalTitle = portal.getProperty('title');
                    bd.selectedPortalOptimizedFor = portal.getProperty('DefaultDevice');
                    bd.selectedPortalTargetDevice = portal.getProperty('TargetedDevice');
                    bd.selectedPortalRepository = portal.repositoryId;

                    var Publishing = b$.require('bd.publishing.Publishing');
                    var publishing = new Publishing();
                    bd.observer.subscribe(publishing.EVENT_CONFIG, function(){
                        if ($widget.model.preferences.getByName('section')) {
                            bd.publishing.selectedSection = $widget.model.preferences.getByName('section').value;
                        }
                        if ($widget.model.preferences.getByName('workpackageId')) {
                            bd.publishing.selectedWorkpackage = $widget.model.preferences.getByName('workpackageId').value;
                        }
                        bd.widgets.PublishApp.Maximized($widget.body);
                    });

                    bd.observer.subscribe(publishing.EVENT_NOT_AVAILABLE, function(){
                        bd.widgets.PublishApp.displayError($widget.body);
                    });
                }

            }, function(err) {
                error.trigger(err);
            });

        //
    };

});
