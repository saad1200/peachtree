/**
 * Copyright (c) 2012 Backbase B.V.
 */
b$.module("bd.widgets.PublishApp", function() {
	'use strict';

	/*----------------------------------------------------------------*/
	/* Polyfills
	/*----------------------------------------------------------------*/
	Function.prototype.bind = Function.prototype.bind || function() {
		var fn = this,
			args = Array.prototype.slice.call(arguments),
			object = args.shift();
		return function(){
			return fn.apply(object,
				args.concat(Array.prototype.slice.call(arguments)));
		};
	};

    $(document).on('portalServerHostSet', function() {
        bd.widgets.PublishApp.refresh(false);        
    });

	var MSG_APPROVE_PKG_SUCCESS       = "Work package was successfully approved",
		MSG_CLEAR_APPROVE_REQ_SUCCESS = "Approval state was cleared",
		MSG_APPROVE_REQ_SUCCESS       = "Item was marked for approval",
		MSG_REJECT_PKG_SUCCESS        = "Work package was successfully rejected",
		MSG_CLEAR_REJECT_REQ_SUCCESS  = "Approval state was cleared",
		MSG_REJECT_REQ_SUCCESS        = "Item was marked for rejection",
		MSG_PUB_DONE                  = "Publishing is done",
		MSG_VIEW_ON_STAGING           = "View on staging (opens in pop up)",
		MSG_UNPUBLISH                 = "Unpublish",
		MSG_EDITED                    = "Edited Properties",
		MSG_APPROVER                  = "Approver message",
		MSG_MESSAGE                   = "{{user}} message",
		TXT_BTN_OK                    = "Ok",
		TXT_APPROVER                  = "Approver",
		TXT_PUBLISHER                 = "Publisher",
		MSG_PACKAGE_CHANGED           = "Package {{id}} status changed from {{state1}} to {{state2}}",
		MSG_PUB_STATUS                = "Publication status and activity",
		MSG_APPROVAL_STATUS           = "Publication Approval status",
		MSG_PORTAL_SETTINGS           = "Settings for {{portalTitle}}",
		MSG_APPROVAL_NOT_POSSIBLE     = "This publication requires you to approve each individual page first",
		MODAL_DATA_CANCEL             = {
			title   : "CANCEL PUBLICATION",
			msg     : "Are you sure you want to cancel this publication?<br/><br/>Comment:",
			yes     : "Yes",
			no      : "No",
			call_id : "STOP"
		},
		MODAL_DATA_REVOKE             = {
			title   : "CANCEL PUBLICATION",
			msg     : "Are you sure you want to cancel this publication?<br/><br/>Comment:",
			yes     : "Yes",
			no      : "No",
			call_id : "REVOKE"
		},
		MODAL_DATA_DISAPPROVE         = {
			title   : "DISAPPROVE WORK PACKAGE",
			msg     : "Are you sure you want to disapprove this publication?<br/><br/>Comment:",
			yes     : "Disapprove",
			no      : "Cancel",
			call_id : "DISAPPROVE"
		},
        TYPE_STRING = "STRING",
        TYPE_DATE = "DATE",
        LABEL_STATUS = "status",
        LABEL_USER = "publisher",
		Publishing, publishing, load, renderDashboard, displayError, msgType, $menuItemsContainer, $menuItems,
        $publishAppFilters, tabs, tabPublishing, tabSettings, detailsPane, detailsPanePublish, detailsPaneApproval,
        packagesData, packagesDataHistory, currentTabId, currentDetailsTab, HTMLTemplateURL, overviewObj, HEADER_URL,
        Filter = function(){
            // this: status, user, date, name, xml;
        },
        filter, publishers = [],
        $body = $('body'),
        headerHeight = 38,
        grippyHeight = 6,
        REST_API_URLS;

    Filter.prototype = {
        setStatus : function(val){
            this.status = val;
        },
        setUser : function(val){
            this.user = val;
        },
        setDate : function(val){
            this.date = val;
        },
        setName : function(val){
            this.name = val ? '%' + val + '%' : '';
        },
        reset : function(){
            this.setStatus('');
            this.setUser('');
            this.setDate('');
            this.setName('');
        },
        toXML : function(){
            var xml = '';
            xml = this.status ? xml + '<filter name="workpackagestatus">' + this.status + '</filter>' : xml;
            xml = this.user ? xml + '<filter name="publishername">' + this.user + '</filter>' : xml;
            xml = this.date ? xml + '<filter name="workpackagecreationdate">' + this.date + '</filter>' : xml;
            xml = this.name ? xml + '<filter name="workpackagename">' + this.name + '</filter>' : xml;
            return xml ? '<filters>' + xml + '</filters>' : '';
        },
        toJson : function(){
            var json = [];
            if(this.status){
                json.push({
                    name: 'workpackagestatus',
                    value: this.status,
                    type: TYPE_STRING
                });
            }
            if(this.user){
                json.push({
                    name: 'publishername',
                    value: this.user,
                    type: TYPE_STRING
                });
            }
            if(this.date){
                json.push({
                    name: 'workpackagecreationdate',
                    value: this.date.replace(/[\-\\]/g, ''), // simpledate expected server side, so remove delimiters
                    type: TYPE_DATE
                });
            }
            if(this.name){
                json.push({
                    name: 'workpackagename',
                    value: this.name,
                    type: TYPE_STRING
                });
            }
            return json;
        },
        toUrlParamsStr: function () {
            return this.toJson().reduce(function (result, param) {
                return result + (result ? '&' : '') + param.name + '=' + param.value;
            }, '');
        }
    };

	Publishing = b$.require('bd.publishing.Publishing');

	load = function (oGadgetBody, sGadgetUrl) {

        var statusMapping = {
            approval: {
                list: {
                    availableStatuses : ['AWAITING_APPROVAL'],
                    statuses : [],
                    pageSize : 20,
                    page     : 1,
                    filters  : []
                },
                listHistory: {
                    availableStatuses : ['APPROVED', 'REJECTED', 'REVOKED'],
                    statuses : [],
                    pageSize : 20,
                    page     : 1,
                    filters  : []
                }
            },
            publishing: {
                list: {
                    availableStatuses : ['PUBLISHING_TO_FIRST_LEVEL', 'PUBLISHED_TO_STAGING', 'PUBLISHING', 'CREATING','SCHEDULED'],
                    statuses : [],
                    pageSize : 30,
                    page     : 1,
                    filters  : []
                },
                listHistory: {
                    availableStatuses : ['PUBLISHED', 'FAILED', 'APPROVED_BUT_FAILED', 'REJECTED', 'REVOKED','PARTIALLY_PUBLISHED'],
                    statuses : [],
                    pageSize : 20,
                    page     : 1,
                    filters  : []
                }
            }
        },
		$oGadgetBody, aPackagesStatus, oTransitionMessages, defaults,
		init, menuClick, showTab, showDetails, showDetailsPanel, $statusFilter, $userFilter, $nameFilter, getPackages, getSourceItem, reject, enrichListEntry, getListData, loadMore, publishingPoller, datefield,
		revokeButtonClick, cancelButtonClick, getWorkpackageData, rowClick, detailsCallback, toggleHistory, refresh, refreshDetails, updatePackageStatus, updateUrl, startupSelection;

        require([
            'zenith/widgets/PublishWizard/scripts/app',
            'css!zenith/widgets/PublishWizard/styles/base'
        ], function(app) {
            angular.bootstrap($('.bc-tab-labels')[0], [app.name]);
        });

        composeAvailableStatuses(statusMapping.approval);
        composeAvailableStatuses(statusMapping.publishing);

        /*
         * @description Accepts statusMapping.approval or statusMapping.publishing and composes availableStatuses
         */
        function composeAvailableStatuses(obj) {
            obj.availableStatuses = be.utils.mergeArrays(obj.list.availableStatuses, obj.listHistory.availableStatuses);
        }

        var initFilters = function(currentTabId){
            filter = filter || new Filter();
            var approvalFilterOptions = [],
                publishFilterOptions = [],
                dd_status, dd_user, user_options,
                onUserSelect, onStatusSelect, onDateSet, toSearch, onSearch,
                filterButtonClick, reset, $dateFilter, $filterButton;

            $statusFilter = $('.bd-publish-app-filter-status', $oGadgetBody);
            $userFilter = $('.bd-publish-app-filter-user', $oGadgetBody);
            $nameFilter = $('.bd-publish-app-search', $oGadgetBody);
            $dateFilter = $('.bd-publish-app-filter-date', $oGadgetBody);
            $filterButton = $('.bd-publish-app-filter-buttons', $oGadgetBody);

            onUserSelect = function(){
                filter.setUser($(this).text().replace(/\s/g, '_'));
                refresh(false);
            };
            onStatusSelect = function(data){
                filter.setStatus($(this).text().replace(/\s/g, '_'));
                refresh(false);
            };
            onDateSet = function(d){
                filter.setDate(d);
                refresh(false);
            };
            onSearch = function(e){
                clearTimeout(toSearch);
                var val = $(this).val();
                toSearch = setTimeout(function(){
                    filter.setName(val);
                    refresh(false);
                }, 300);
            };
            filterButtonClick = function(e){
                var $target = $(e.target);
                if($target.hasClass('bc-refresh-lg') || $target.hasClass('bd-publish-app-btn-refresh')){
                    refresh();
                }else if($target.hasClass('bd-publish-app-btn-reset')){
                    resetFilters();
                }
            };

            $statusFilter.empty();
            statusMapping.approval.list.availableStatuses.concat(statusMapping.approval.listHistory.availableStatuses).forEach(function(item){
                var val = item.replace(/_/g, ' ');
                approvalFilterOptions.push({name:val, value:val, classname:'bd-approval-status', handler:onStatusSelect});
            });
            statusMapping.publishing.list.availableStatuses.concat(statusMapping.publishing.listHistory.availableStatuses).forEach(function(item){
                var val = item.replace(/_/g, ' ');
                publishFilterOptions.push({name:val, value:val, classname:'bd-approval-status', handler:onStatusSelect});
            });
            dd_status = bc.component.dropdown({
                target: $statusFilter,
                label: LABEL_STATUS,
                uid: 'bd-app-filter-status',
                options: currentTabId == 'approval' ? approvalFilterOptions : publishFilterOptions,
                minWidth:202,
                marginLeft:0,
                execSelect:true,
                centerDropdown:true
            });

            $userFilter.empty();
            user_options = publishers.map(function(item){
                return {name:item, handler: onUserSelect};
            });
            dd_user = bc.component.dropdown({
                target: $userFilter,
                label: LABEL_USER,
                uid: 'bd-publish-app-filter-user',
                options: user_options,
                marginLeft:0,
                execSelect:true,
                centerDropdown:true
            });

            $dateFilter.empty();
            datefield = bc.component.dateField({
                target: $dateFilter,
                placeholder: 'MM/DD/YYYY',
                callback: onDateSet
            });

            $nameFilter
                .off('keyup.filter')
                .on('keyup.filter', onSearch);

            $filterButton
                .off('click.filter')
                .on('click.filter', filterButtonClick);

            $publishAppFilters.removeClass('hidden');
        };

		/* helpers/ utilities */
		getSourceItem = function (items) {
			return items.filter(function (item) {
				return item.isSourceItem === true;
			})[0];
		};

		/*
		 * data handling:
		 * get publishing data
		 */
        getPackages = function (callback) {

            var map = statusMapping[currentTabId],
                historyStatuses = [],
                currentStatuses = [];

            if (!currentTabId || bd.publishing.selectedSection && currentTabId !== bd.publishing.selectedSection || !map) {
                currentTabId = bd.publishing.selectedSection || 'approval';
            }

            // first clear container and show spinner
            tabPublishing.html("<div class=\"bd-spinner\"></div>");

            if (filter.status) {
                map.list.availableStatuses.some(function (item) {
                    if (item === filter.status) {
                        currentStatuses = [item];
                    }
                });
                if (currentStatuses.length == 0) {
                    map.listHistory.availableStatuses.some(function (item) {
                        if (item === filter.status) {
                            historyStatuses = [item];
                        }
                    });
                }
            }
            var getList = function(callback){
                publishing.getPackagesList(currentStatuses, map.list.filters, currentTabId, function (data) {
                    packagesData = data;
                    if (jQuery.isFunction(callback)){
                        callback(data);
                    }
                }, null, map.list.pageSize);
            };

            var getHistoryList = function(callback){
                publishing.getPackagesList(historyStatuses, map.listHistory.filters, currentTabId, function (data) {
                    packagesDataHistory = data;
                    if (jQuery.isFunction(callback)) {
                        callback(data);
                    }
                }, null, map.listHistory.pageSize);
            };

            var getBoth = function(){
                currentStatuses = map.list.availableStatuses;
                historyStatuses = map.listHistory.availableStatuses;
                getList(function(data){
                    getHistoryList(function(data){
                        callback(data);
                    });
                });
            };

            packagesDataHistory = packagesData = []; // clean slate first
            if(historyStatuses.length > 0){
                return getHistoryList(callback);
            }else if(currentStatuses.length > 0){
                return getList(callback);
            }else{
                return getBoth(callback);
            }
        };
        /*
		 * Add calculated attributes used in mustache template
		 * @param  {Object} model
		 * @return {Object} model - enriched model
		 */
		enrichListEntry = function (model) {

			model.name                   = model.workPackageName.replace(/&/g, '&amp;');
			model.uuid                   = model.workPackageUuid;
			model.publishState           = model.workPackageState;
			model.publishStateDisplay    = publishing.STATE_DISPLAY[model.workPackageState];
			model.stateLowercase         = model.workPackageState.toLowerCase().split('_').join('-');
			model.approvalState          = model.approvalJobState;
			model.approvalStateLowercase = model.approvalState.toLowerCase().split('_').join('-');
			model.creationDate           = model.workPackageCreationDate;
			model.isActiveWorkpackage    = false;
			model.publishedToStaging     = model.workPackageState === publishing.STATE_PUBLISHED_TO_STAGING;
			model.isScheduled            =  model.workPackageState === 'SCHEDULED';
			model.displayFailed          = [publishing.STATE_FAILED, publishing.STATE_PARTIALLY_PUBLISHED].indexOf(model.workPackageState) !== -1;;

			return model;
		};

		/**
		 * Puts workpackages data into list and history arrays according to selected section (publishing/approval)
		 * @param  {String} sectionId
		 * @return {Object}
		 */
		getListData = function (sectionId) {
			var data = {
				list: [],
				listHistory: []
			}, i, j, r, d, model,
            urlSuffixes = REST_API_URLS.overview.suffixes;

			if (be.utils.getCookie('bd-pub-showApprovalHistory') == 'true') {
				data.showApprovalHistory = true;
			}

			for (i = 0; i < packagesData.length; i += 1) {
				model = packagesData[i];
				if (sectionId === 'approval' && model.workPackageState === publishing.STATE_PUBLISHED_TO_STAGING || sectionId === 'publishing') {
					data['list'].push(enrichListEntry(model));
				}
			}
			for (i = 0; i < packagesDataHistory.length; i += 1) {
				if (sectionId === 'approval' && !packagesDataHistory[i].approverName) {
					continue;
				}
				model = packagesDataHistory[i];
				data['listHistory'].push(enrichListEntry(model));
			}

            data.listLength = String(overviewObj[sectionId + urlSuffixes.MAIN_LIST_SUFFIX]);
            data.listHistoryLength = String(overviewObj[sectionId + urlSuffixes.HISTORY_LIST_SUFFIX]);
            return data;
		};

		/*
		 * shows either the publishing or the approval (workpackages) list
		 * clears detail pane
		 */
		showTab = function (id) {
			var templateData, htmlContent, tabObj, data;
			tabObj = defaults[id] || {};
			currentTabId = id;
			// clear details
			detailsPane.html('');

			data = getListData(id);
			// repopulate list
			templateData = {
				id: id,
				title: tabObj.message,
                header: be.utils.processHTMLTemplateByUrl(HEADER_URL, {
                    text: tabObj.headerText,
                    length: data.listLength
                }),
				list: be.utils.processHTMLTemplateByUrl(HTMLTemplateURL + tabObj.template + '-listLine' + ".html", data),
                historyHeader: be.utils.processHTMLTemplateByUrl(HEADER_URL, {
                    text: tabObj.historyHeaderText,
                    length: data.listHistoryLength
                }),
				listHistory: be.utils.processHTMLTemplateByUrl(HTMLTemplateURL + tabObj.template + '-listHistoryLine' + ".html", data)
			};
			// process appropriate html (publishing or approval)
			htmlContent = be.utils.processHTMLTemplateByUrl(HTMLTemplateURL + tabObj.template + ".html", templateData);

			tabPublishing.html(htmlContent);

			var queryWorkpackage = be.utils.getQueryStringParam('workpackage');
			var autoHighlightSelector = (id === 'approval' ? 'data-status="AWAITING_APPROVAL"' : 'data-status="PUBLISHED_TO_STAGING"');
			if (bd.publishing.selectedWorkpackage || queryWorkpackage) {
				autoHighlightSelector = "data-uuid='" + (bd.publishing.selectedWorkpackage || queryWorkpackage) + "'";
				delete bd.publishing.selectedWorkpackage;
				queryWorkpackage = null;
			}

			var selectedWorkpackage = jQuery("table.bd-pub-tableList tr[" + autoHighlightSelector + "]", tabPublishing).get(0);

			if (selectedWorkpackage) {
				startupSelection = true;
			}
			jQuery(selectedWorkpackage).trigger('click').promise().done(function () {
				if (selectedWorkpackage) {
					selectedWorkpackage.scrollIntoView(false);
				}
			});

            var totalHeight = tabPublishing.parent().height() - 130;
            if (selectedWorkpackage) {
                jQuery(selectedWorkpackage).parents('.bd-publishApp-list').height(totalHeight - 22);
                jQuery('.bd-publishApp-list-container', tabPublishing).height(0);
                jQuery(selectedWorkpackage).parents('.bd-publishApp-list').find('.bd-publishApp-list-container').height(totalHeight - 59);
            } else {
                jQuery('.bd-publishApp-list', tabPublishing).eq(0).height(totalHeight + 3);
                jQuery('.bd-publishApp-list-container', tabPublishing).eq(0).height(totalHeight - 59);
                jQuery('.bd-publishApp-list-container', tabPublishing).eq(1).height(0);
            }

            if (matchMedia) {
                var mq = matchMedia("(max-width: 1240px)");
                mq.addListener(widthChange);
                widthChange(mq);
            }

            // media query change
            function widthChange(mq) {
                if (jQuery('.bd-publishApp-list-container', tabPublishing).eq(1).height() < 39) {
                    changeListHeight(mq.matches, 0);
                } else {
                    changeListHeight(mq.matches, 1);
                }
            }

            function changeListHeight (isMatch, listIndex) {
                var totalHeight = tabPublishing.parent().height() - 130,
                    publishListHeight = 0;

                if (isMatch) {
                    publishListHeight = totalHeight - 37;
                } else {
                    publishListHeight = totalHeight + 3;
                }
                jQuery('.bd-publishApp-list-container', tabPublishing).eq(listIndex).parents('.bd-publishApp-list')
                    .height(publishListHeight);
                jQuery('.bd-publishApp-list-container', tabPublishing).eq(listIndex).height(totalHeight - 59);
            }

            $(window).on('resize', be.utils.throttle(function(){
                var totalHeight = jQuery('.bd-publishApp-listContainer', $oGadgetBody).height(),
                    tabMenuHeight = $menuItemsContainer.height(),
                    $list = jQuery('.bd-publishApp-list:eq(0)', $oGadgetBody),
                    topHeight = Math.min($list.height(), totalHeight  - (headerHeight * 2) - tabMenuHeight - $publishAppFilters.height());

                jQuery('.bd-publishApp-listContainer > .bd-publishApp-accordion', $oGadgetBody).height(totalHeight - headerHeight - tabMenuHeight - $publishAppFilters.height() + grippyHeight);
                $list.height( topHeight );
                $list.find('.bd-publishApp-list-container').height( topHeight - headerHeight );
            }, 50));

            $oGadgetBody.delegate('.bd-pub-warning-close', 'click', function (evt) {
				evt.stopPropagation();

				jQuery(evt.target).parent().css({
					height: 0
				});
				jQuery('.bd-pub-approval-details', $oGadgetBody).removeClass('bd-pub-faulty');
			});

			$oGadgetBody.find('.bd-publishApp-list-container').scroll(function (evt) {
				var targetSection;
				if (!evt.target.paginationProgress && evt.target.scrollTop + jQuery(evt.target).height() > jQuery('table', evt.target).height() - 100) {
					evt.target.paginationProgress = true;
					targetSection = jQuery('table', evt.target).hasClass('bd-pub-tableHistory') ? 'listHistory' : 'list';
					loadMore(id, targetSection, jQuery('table', evt.target), 'FORWARDS');
				} else if (!evt.target.paginationProgress && evt.target.scrollTop === 0) {
					evt.target.paginationProgress = true;
					targetSection = jQuery('table', evt.target).hasClass('bd-pub-tableHistory') ? 'listHistory' : 'list';
					loadMore(id, targetSection, jQuery('table', evt.target), 'BACKWARDS');
				}
			});
			$menuItems.removeClass('bc-active-tab').filter('[data-id="' + id + '"]').addClass('bc-active-tab');

			if(id === 'publishing') {
				reflectProgress(tabPublishing, data);
			} else {
				publishingPollerUnsubscribe();
			}
		};
		/*----------------------------------------------------------------*/
		/*  Reflect the changes when a publish workakage is changed
		/*----------------------------------------------------------------*/
		var
		pollersNotifiers = {},

		publishingPollerUnsubscribe = function publishingPollerUnsubscribe () {
			if( $.isEmptyObject(pollersNotifiers) ) return;
			for( var poller in pollersNotifiers) {
				publishingPoller.unsubscribe(pollersNotifiers[poller]);
			}
			pollersNotifiers = {};
		},

		reflectProgress = function(tab$, data) {

			var pList = data.list || []; // the publish list
			var subscriber = publishingPoller.subscribe;  //alias
			var unsubscriber = publishingPoller.unsubscribe; //alias
			var getNodeText = be.utils.getNodeText; //alias

			var reflectOnStates  = ['PUBLISHING','PUBLISHING_TO_FIRST_LEVEL','CREATING'];
			var finalStates  	 = statusMapping[currentTabId].listHistory.availableStatuses;
			var timestamp = null;
			var succesfullProcesses = function(progress){
//                return progress.total - progress.fail;
                return progress.done;
            };

			function onFetchSuccess(xmlDoc) {
				var wp = this; //bound from the subscriber
				var $xml = jQuery(xmlDoc);
				var $wp = tab$.find('.bd-widgetItemHolder[data-uuid="' + wp.uuid + '"]');
                var $progressbar = $wp.find('.bd-pub-progress-bar');
                var $process = $wp.find('.bd-pub-process');
                var $state = $wp.find('.bd-pub-state');
                var progressApi = $progressbar.data('api');
                var $fail = $process.find('.bd-pub-fail-process');
                var $totalCount =  $process.find('[data-count="total"]');
                var $doneCount = $process.find('[data-count="done"]');
                var failCount = $fail.find('[data-count="fail"]');

				// NOTE: if you opt for the status API use 'workpackagestatus' instead of 'workpackagelistresult'
				var $updates = $xml.find("workpackagestatus").filter(function () {
					return $(this).find("workPackageUuid").text() === wp.uuid;
				}).each(function () {

					timestamp = wp.statusChangeDateTS;
					var $item =  jQuery(this);
					var item = enrichListEntry(publishing.parseWorkPackagesListRow($item));

					if( $.inArray(wp.workPackageState, reflectOnStates) === -1 ) {
						//$progressbar.hide();
						$process.hide();
					}else {
						//$progressbar.show();
						$process.show();
					}

					progressApi.setValue(item.progress.done);
					progressApi.setValue(item.progress.fail, 'fail');

					$totalCount.text(item.progress.total);
					$doneCount.text(item.progress.done);
					failCount.text(item.progress.fail);

					$state.text(item.publishStateDisplay);

					if(item.progress.fail > 0) {
						$fail.show();
					} else {
						$fail.hide();
					}
					$wp.find('.bd-pub-states').attr('class','bd-pub-states bd-pub-' + item.stateLowercase);

					timestamp = item.statusChangeDateTS;
					if( $.inArray(item.workPackageState, finalStates ) >= 0 && typeof pollersNotifiers[item.workPackageUuid] !== 'undefined' ) {
						unsubscriber.call(publishingPoller, pollersNotifiers[item.workPackageUuid]);
						$oGadgetBody.trigger('publishing-done', {wp: item, $wp: $wp } );
					}
				});

				if (!timestamp) {
					timestamp = parseInt(wp.statusChangeDateTS, 10) + 1;
				}
				if (timestamp && $updates.length > 0){
					publishingPoller.lastUpdate = parseInt(timestamp, 10) + 1;
				}

			}

			function onFetchError(err) {
				bc.component.notify({
					icon: 'error', // checkbox, attention, error, loading
					message: 'Error fetching data. Plese check the network connection!'
				});
				(typeof wp !== 'undefined') && unsubscriber.call(publishingPoller, pollersNotifiers[wp.uuid]);
			}

			jQuery.each(pList, function(i, wp) {

				var $wp = tab$.find('.bd-widgetItemHolder[data-uuid="' + wp.uuid + '"]');
					bc.component.progress( $wp.find('.bd-pub-progress-bar'),  {
						max :  wp.progress.total,
						segments: [
							{ name: 'done', value:  succesfullProcesses(wp.progress) },
							{ name: 'fail', value:  wp.progress.fail }
						],
						onReady: function(obj) {
							if( $.inArray(wp.workPackageState, reflectOnStates) === -1 ) {
								obj.$el.hide();
							}
						}
					});

				publishingPoller.lastUpdate = wp.statusChangeDateTS;

				if( typeof pollersNotifiers[wp.uuid] === 'undefined') {
					pollersNotifiers[wp.uuid] = subscriber.call(publishingPoller, onFetchSuccess.bind(wp), onFetchError);
				}

			});
		}

		/**
		 * Pagination method - loads next/previous entries
		 * @param  {String} tab             Tab name (approval/publishing)
		 * @param  {String} section         Section name (list/history)
		 * @param  {Object} container       jQuery list container
		 * @param  {String} pagingDirection Pagination direction (FORWARDS/BACKWARDS)
		 */
		loadMore = function (tab, section, container, pagingDirection) {
			var uuid = pagingDirection === 'FORWARDS' ? container.find('tr').last().data('uuid') : container.find('tr').first().data('uuid');
			if (uuid) {
				publishing.getPackagesList(statusMapping[tab][section].availableStatuses, statusMapping[currentTabId].list.filters, tab, function (entries) {
					if (entries.length === 0) {
						statusMapping[tab][section].page--;
					}

					var data = {};
					data[section] = [];
					for (var i = 0, l = entries.length; i < l; i++) {
						var model = enrichListEntry(entries[i]);
						data[section].push(model);
					}
					if (pagingDirection === 'FORWARDS') {
						container.find('tbody').append(be.utils.processHTMLTemplateByUrl(HTMLTemplateURL + defaults[tab].template + '-' + section + 'Line' + ".html", data));
					} else {
						container.find('tbody').prepend(be.utils.processHTMLTemplateByUrl(HTMLTemplateURL + defaults[tab].template + '-' + section + 'Line' + ".html", data));
					}
					delete container.parent()[0].paginationProgress;
				}, uuid, statusMapping[tab][section].pageSize, pagingDirection);
			}
		};

		/**
		 * Refresh current list tab
		 */

        refresh = function (reset) {
            var map = statusMapping[currentTabId],
                filters = filter ? filter.toJson() : [],
                availableStatuses = (filter && filter.status) ? [filter.status] : map.availableStatuses;

            map.list.page = 1;
            map.listHistory.page = 1;
            map.list.filters = filters;
            map.listHistory.filters = filters;

            updateOverview(availableStatuses, filters, currentTabId).then(function () {
                getPackages(function () {
                    showTab(currentTabId, reset);
                });
            });
        };

		/**
		 * Refresh currently selected package info pane
		 * @param  {String} id   Workpackage id
		 * @param  {Object} data Model
		 */
		refreshDetails = function (id, data) {
			getWorkpackageData(data.uid, true);
		};

		/*
		 * calls reject on publishing object
		 * after OK on prompt
		 */
		reject = function (id, onSuccess, onError, params) {
			be.utils.confirm({
				title         : params.title,
				message       : params.msg,
				yesCallback   : function () {
					publishing.reject(id, jQuery(".bd-text-box").val(), onSuccess, onError, params.call_id);
				},
				noCallback    : null,
				cancelBtnText : params.no,
				okBtnText     : params.yes,
				showTextBox   : true
			});
		};

		revokeButtonClick = function (e) {
			e.stopPropagation();
			reject(jQuery(this).data('id'), refresh, refresh, MODAL_DATA_REVOKE);
		};

		cancelButtonClick = function (e) {
			e.stopPropagation();
			reject(jQuery(this).data('id'), refresh, refresh, MODAL_DATA_CANCEL);
		};

        getWorkpackageData = function (uuid, validate) {
            publishing.getPackageByUuid(uuid, function (data) {
                var workpackage = data;

                var approved = 0;
                workpackage.stateLowercase = workpackage.state.toLowerCase().split("_").join("-");
                workpackage.isFailedState = workpackage.state === publishing.STATE_FAILED;
                workpackage.approvalStateLowercase = workpackage.approvalState.toLowerCase().split("_").join("-");
                workpackage.publishedToStaging = (workpackage.state == publishing.STATE_PUBLISHED_TO_STAGING);
                workpackage.name = workpackage.name.replace(/&/g, "&amp;");
                workpackage.description = workpackage.description ? workpackage.description.replace(/&/g, "&amp;") : workpackage.description;
                workpackage.approvalComment = workpackage.approvalComment ? workpackage.approvalComment.replace(/&/g, "&amp;") : workpackage.approvalComment;

                if (workpackage.publishedToStaging) {
                    data.hasItemsForApproval = true;
                } else {
                    data.hasApprovalHistory = true;
                }

                for (var j = 0; j < workpackage.requests.length; j += 1) {
                    var r = workpackage.requests[j];
                    if (r.approvalState == "APPROVED" || !r.isApprovalJob) {
                        r.stateApproved = true;
                        approved++;
                    } else if (r.approvalState == "REJECTED") {
                        r.stateDisapproved = true;
                    }
                    r.hasItems = r.items.length > 1;

                    r.typeOfChange = '';

                    if (r.sourceItem && r.sourceItem.publishActionType == 'UNPUBLISH') {
                        r.className = 'bd-pub-icon-unpublish';
                        r.typeOfChange += MSG_UNPUBLISH;
                    }

                    if (r.sourceItem && r.sourceItem.lastModifiedTimestamp) {
                        r.lastModified = bd.date.formatDateTime(r.sourceItem.lastModifiedTimestamp);
                    }

                    for (var i = 0, l = r.items.length; i < l; i++) {
                        var item = r.items[i];
                        if (item.type.toLowerCase() === 'page') {
                            r.pageLastModified = bd.date.formatDateTime(item.lastModifiedTimestamp);
                        }
                    }

                    if (r.sourceItem && r.sourceItem.invalidDependencies) {
                        r.sourceItem.dependencyWarnings = [];
                        for (i = 0, l = r.sourceItem.invalidDependencies.length; i < l; i++) {
                            workpackage.dependencyWarnings = true;
                            var dependency = r.sourceItem.invalidDependencies[i];
                            r.sourceItem.dependencyWarnings.push({
                                itemTitle: r.sourceItem.title,
                                publishAction: publishing.dependencyWarningText.publishAction[r.sourceItem.publishActionType],
                                relationType: publishing.dependencyWarningText.itemType[dependency.itemType],
                                dependencyType: publishing.dependencyWarningText.dependencyType[dependency.dependencyType],
                                dependencyAction: publishing.dependencyWarningText.action[dependency.dependencyType],
                                dependencyItemTitle: dependency.itemTitle
                            });
                        }
                    }
                }

                if (approved == workpackage.requests.length) workpackage.approved = true;
                /* old shit */

                showDetailsPanel(workpackage, currentTabId, defaults[currentTabId].detailsCallback);
            }, validate);
        };

		/**
		 * Update state url with new section/workpackage data
		 * @param  {String} tabId         'approval' or 'publishing'
		 * @param  {String} workpackageId (optional)
		 */
		updateUrl = function (tabId, workpackageId){
			var aStateData = [bd.selectedPortalName === 'dashboard' ? '_' : bd.selectedPortalName, tabId, workpackageId || ''];
			var state = aStateData.join('/');
			var aLocation = window.location.pathname.split(b$.view.url2state.separator);
			window.history.pushState({}, '', [aLocation[0], state].join(b$.view.url2state.separator));
		};

		/* main menu (left column) item click */
        menuClick = function () {
            var menuItem, id;

            $menuItems.removeClass("bc-active-tab");
            tabs.removeClass("bc-active-tab");
            detailsPane.removeClass("bc-active-tab");

            menuItem = jQuery(this).addClass("bc-active-tab");
            id = menuItem.data('id');

            if (id) {
                currentTabId = id;
                resetFilters();
                statusMapping[id].list.page = 1;
                statusMapping[id].listHistory.page = 1;
            } else {
                console.log("unhandled click", menuItem.attr("class"), menuItem);
            }
            initFilters(id);
        };

		rowClick = function (e) {
			var tr = jQuery(this),
				currentClass = 'bd-ui-trSelected';
			jQuery("tr", tabPublishing).removeClass(currentClass);
			tr.addClass(currentClass);

			if (!startupSelection){
				updateUrl(currentTabId, tr.data('uuid'));
			} else {
				startupSelection = false;
			}

			getWorkpackageData(tr.data('uuid'), currentTabId === 'approval' && tr.data('status') === 'PUBLISHED_TO_STAGING');
		};

		showDetailsPanel = function (data, id, callback) {
			var htmlContent = be.utils.processHTMLTemplateByUrl(HTMLTemplateURL + id + "Details.html", data);
			detailsPane.html(htmlContent);

			jQuery(".bd-pub-revoke", detailsPane).unbind("click", revokeButtonClick).bind("click", revokeButtonClick);
			jQuery(".bd-pub-cancel", detailsPane).unbind("click", cancelButtonClick).bind("click", cancelButtonClick);

			if (id === "publishing") {
				detailsPane.off()
					.on("click", ".bc-tab-label", function () {
					currentDetailsTab = jQuery(this).hasClass('bd-pub-tab-status') ? '.bd-pub-tab-status' : '.bd-pub-tab-content';
                });

				if (!currentDetailsTab) {
					currentDetailsTab = '.bd-pub-tab-status';
				}
				jQuery(currentDetailsTab).click();
			}
			jQuery(".bd-log-failed", detailsPane).unbind("click").bind("click", function () {
				jQuery('.bd-log-hidden', detailsPane).toggle();
				jQuery('.bd-publish-app-log-expand', detailsPane).toggleClass('bd-open');
			});
			jQuery(".bd-publish-app-items-expand", detailsPane).unbind("click").bind("click", function () {
				var name = jQuery(this).data('name');
				jQuery('.bd-pub-item', detailsPane).filter(function (idx, item) {
					return jQuery(item).data('parent') === name;
				}).toggle();
				jQuery(this).toggleClass('bd-open');
			});
			jQuery(".bd-pub-launchPageApp", detailsPane).unbind("click").bind("click", function () {
				be.closeCurrentDialog();
				jQuery('#widget-PageMgmtMVC').trigger('click');
			});

			jQuery(".bd-pub-view", detailsPane).unbind("click").bind("click", function (ev) {
				$(ev.currentTarget).find('.bd-publish-result-logs')
					.toggleClass('open')
					.promise().done(function() {
						var $icon = $('.bd-pub-toggle-result-logs i', $(ev.currentTarget));
						$icon.toggleClass('open', this.hasClass('open'));
					});

			});

			if (jQuery.isFunction(callback)) {
				callback(data);
			}
		};

		/* Approval Tab */
		detailsCallback = function (data) {
			// hide revoke button if in 'end state'
			if (data.state == "FAILED" || data.state == "PUBLISHED" || data.state == "REJECTED") {
				jQuery(".bd-pub-approval-details .bd-pub-revoke").hide();
			} else {
				jQuery(".bd-pub-approval-details .bd-pub-revoke", $oGadgetBody).unbind("click").bind("click", function () {
					reject(data.approvalId, refresh, refresh, MODAL_DATA_REVOKE);
				}).show();
			}

			// show and activate buttons only when state == 'PUBLISHED_TO_STAGING'
			if (data.state === "PUBLISHED_TO_STAGING") {

				jQuery(".bd-pub-approval-details .bd-pub-actions .bd-pub-approve, .bd-publishApp-approveBtn", $oGadgetBody).unbind("click").bind("click", function (evt) {
					var $this = jQuery(this),
						id = $this.data('id');

					evt.stopPropagation();

					if ($this.hasClass('bd-publishApp-approveBtn')) {
						if (!$this.hasClass('bc-gradient-grey-disabled')) { // Approve Workpackage
							publishing.approve(id, function () {
								refresh();
								bc.component.notify({
									uid: '6743',
									icon: 'checkbox', // checkbox, attention, error, loading
									message: MSG_APPROVE_PKG_SUCCESS
								});
							});
						} else {
							bc.component.notify({
								uid: '6743',
								icon: 'attention', // checkbox, attention, error, loading
								message: MSG_APPROVAL_NOT_POSSIBLE
							});
						}

					} else { // approve single request
						if ($this.hasClass('bd-selected')) {
							publishing.awaiting(id, function () {
								refreshDetails(id, data);
							});
						} else {
							publishing.approve(id, function () {
								refreshDetails(id, data);
							}, function (responseData) {
								var jsonData = bd.xmlToJson({
									xml: responseData.responseXML
								}).errorMessage || {};
								bc.component.notify({
									uid: '2234',
									icon: 'error', // checkbox, attention, error, loading
									message: jsonData.message
								});
							});
						}
					}
				});

				jQuery(".bd-pub-approval-details .bd-pub-actions .bd-pub-reject, .bd-publishApp-disapproveBtn", $oGadgetBody).unbind("click").bind("click", function (evt) {
					var $this = jQuery(this),
						id = $this.data('id');

					evt.stopPropagation();

					if ($this.hasClass('bd-publishApp-disapproveBtn')) { // Reject workpackage
						reject(
						data.approvalId,

						function () {
							refresh();
							bc.component.notify({
								uid: '0029',
								icon: 'attention', // checkbox, attention, error, loading
								message: MSG_REJECT_PKG_SUCCESS
							});
							//bd.operationCompleteMsg(MSG_REJECT_PKG_SUCCESS, 'attentionIcon');
						},
						null,
						MODAL_DATA_DISAPPROVE);
					} else { // Reject single request
						if ($this.hasClass('bd-selected')) {
							publishing.awaiting(id, function () {
								refreshDetails(id, data);
							});
						} else {
							// straight to db (no modal)
							publishing.reject(
							id,
								'',

							function () {
								refreshDetails(id, data);
							});
						}
					}
				});

			}

			detailsPane.find('.bd-pub-view').each(function (e) {
				var link = jQuery(this);
				var show = typeof link.data('state') !== 'undefined' && link.data('state') === "PUBLISHED_TO_STAGING";
				var type = link.data('type');

                var hasScheduledState = data.publishState === publishing.STATE_SCHEDULED;
                var editorialOrchestratorName = publishing.targetOrchestrators.EditorialOrchestrator.name;
                var prevTagretOrchestrator = data.publishPrevTargetOrchestrator;

                var targetOrchestrator = hasScheduledState ? editorialOrchestratorName:
                    (prevTagretOrchestrator ? prevTagretOrchestrator : data.publishTargetOrchestrator);

                var psHost = publishing.targetOrchestrators[targetOrchestrator].portalServerHost;

				if (type !== 'Content'){
					var url = bd.contextRoot + "/portals/" + (data.publishOrigin || bd.selectedPortalName) + '/links/' + link.data('name') + '.xml';
					be.utils.ajax({
						url: url,
						dataType: "xml",
						type: "GET",
						cache: false,
						success: function (oXml) {
                            // TODO cache selectors
                            var $xml = jQuery(oXml);
                            var title = $xml.find('> link > properties > property[name="title"]').text();
                            var url = $xml.find('> link > properties > property[name="generatedUrl"]').text();
                            var itemType = $xml.find('> link > properties > property[name="itemType"]').text();
                            var parentItemName = $xml.find('> link > parentItemName').text();

                            if (data.publishPrevTarget) {
                                url = url.replace(data.publishOrigin, data.publishPrevTarget);
                            }

                            url = psHost.externalUrl + url;

                            if (parentItemName === 'masterpage_root'){
                                jQuery('.bd-pub-itemType', link).addClass('bc-masterpage');
                            } else {
                                jQuery('.bd-pub-itemType', link).addClass('bc-' + itemType.toLowerCase());
                            }
                            link.data('url', url);
                            link.data('title', title);
						},
						error: function (oRequest, sStatus, oError) {}
					});
				} else {
					be.cmis.getMetaData({
						contentUid: link.data('name'),
                        repository: link.data('repository') === 'contentRepository' ? 'contentRepository' : bd.selectedPortalRepository,
						errorCallback: function(){
							jQuery('.bd-pub-itemType', link).addClass('bc-image');
							jQuery('.bd-pub-page-name', link).text(link.data('name'));
						}
					}, function(metadata){
						if (metadata.thumbUrl && metadata.thumbUrl.property && metadata['cmis:objectTypeId'].property !== 'bb:structuredcontent' && metadata['cmis:objectTypeId'].property !== 'cmis:document'){
							var img = jQuery('<img src="' + metadata.thumbUrl.property + '">');
							jQuery('.bd-pub-itemType', link).removeClass('bc-icn').append(img);
						} else if (metadata['cmis:objectTypeId'].property === 'bb:structuredcontent') {
							jQuery('.bd-pub-itemType', link).addClass('bc-structuredcontent');
						}

                        var contextPath = (psHost.contextPath == '/') ? '' : (psHost.contextPath.replace(/^\/|\/$/g, '') + '/');

						if (metadata['cmis:objectTypeId'].property === 'bb:structuredcontent') {
                            var portalName = (link.data('repository') == 'contentRepository') ? '' : ('/'+bd.selectedPortalName);
                            var sUrl = be.contextRoot + portalName +'/catalog/'+ metadata['bb:typeDefinitionName'].property;
			                be.utils.ajax({
			                    url : sUrl,
			                    dataType: 'xml',
			                    error: function(response) {
                                    var bareUrl = metadata.url.property.replace(/\/atom\/[0-9a-f\-]+\//, '/atom/' + link.data('repository') + '/');
                                    if (bareUrl.indexOf(be.contextRoot) !== -1) {
                                        bareUrl = bareUrl.slice(be.contextRoot.length);
                                    }
			                    	link.data('url', psHost.address.replace(/\/$/, '') + (psHost.port ? ':' + psHost.port : '') + '/' + contextPath + bareUrl.replace(/^\//, ''));
			                    },
			                    success: function(response) {
			                    	var sTemplate = $(response).find('property[name="previewTemplate"] value').text();
			                    	var title;
			                    	// the the previewTemplate property is there
			                    	if (sTemplate) {
			                    		// render the sctructured content by using the rendered service
										var repositoryName = (link.data('repository') == 'contentRepository') ? 'contentRepository' : '@portalRepository';
										var contextItemName = (repositoryName == 'contentRepository') ? '[BBHOST]' : bd.selectedPortalName;
										var url = 'contenttemplates/rendered?contentRef.contentRef=cs:'+ repositoryName +':'+ metadata['cmis:objectId'].property +'&templateUrl='+ sTemplate +'&contextItemName='+ contextItemName;
										// add url to data store
										var sUrl = psHost.address.replace(/\/$/, '') + (psHost.port ? ':' + psHost.port : '') + '/' + contextPath + url;
										link.data('url', sUrl);
										// add title to data store
										title = metadata['cmis:name'] ? metadata['cmis:name'].property : undefined;
										link.data('title', title);
										jQuery('.bd-pub-page-name', link).text(title);
			                    	} else {
                                        title = metadata['bb:title'] ? metadata['bb:title'].property : undefined;
                                        link.data('title', title);
                                        jQuery('.bd-pub-page-name', link).text(title);
                                        // if there is no template show the json object
                                        var bareUrl = metadata.url.property.replace(/\/atom\/[0-9a-f\-]+\//, '/atom/' + link.data('repository') + '/');
                                        if (bareUrl.indexOf(be.contextRoot) !== -1) {
                                            bareUrl = bareUrl.slice(be.contextRoot.length);
                                        }
                                        var newUrl = psHost.address.replace(/\/$/, '') + (psHost.port ? ':' + psHost.port : '') + '/' + contextPath + bareUrl.replace(/^\//, '');
                                        link.data('url', newUrl);
                                    }
                                }
                            });
                        } else {
                            // If there is a contextroot in the metadata url strip it and set it with the environment contextroot
                            var linkRepositoryId = link.data('repository');
                            var bareUrl = (!linkRepositoryId) ? metadata.url.property :
                                (metadata.url.property.replace(/\/atom\/[0-9a-f\-]+\//, '/atom/' + linkRepositoryId + '/'));
                            if (bareUrl.indexOf(be.contextRoot) !== -1) {
                                bareUrl = bareUrl.slice(be.contextRoot.length);
                            }
                            // add url to data store
                            var url = psHost.address.replace(/\/$/, '') + (psHost.port ? ':' + psHost.port : '') + '/' + contextPath + bareUrl.replace(/^\//, '');
							link.data('url', url);
							// add title to data store
							var title = metadata['cmis:name'] ? metadata['cmis:name'].property : undefined;
							link.data('title', title);
						}
					});
				}
			});

			if (data.state === "PUBLISHED_TO_STAGING") {
				detailsPane.find('.bd-pub-view').addClass('bd-pub-active').find('.bd-pub-preview').attr('title', MSG_VIEW_ON_STAGING).click(function () {
					var link = jQuery(this).parent();
					var url = link.data('url');
					var title = link.data('title');

					if (url) {
					    url = encodeURI(url);
						var win = window.open(url); //, '_blank', 'width=' + window.windowsize.width + ',height=' + window.windowsize.height + 'menubar=0,titlebar=0,toolbar=0');
					}
				});
			}
		};

		/* col 2 */
		updatePackageStatus = function (data) {
			if (aPackagesStatus[data.uid]) {
				if (aPackagesStatus[data.uid] !== data.state) {
					// TODO Use oTransitionMessages
					bc.component.notify({
						uid: '9982',
						icon: 'checkbox', // checkbox, attention, error, loading
						message: MSG_PACKAGE_CHANGED.replace("{{id}}", data.uid)
							.replace("{{state1}}", aPackagesStatus[data.uid])
							.replace("{{state2}}", data.state)
					});
					//bd.operationCompleteMsg('Package ' + data.uid + ' status changed from ' + aPackagesStatus[data.uid] + ' to ' + data.state);
				}
			}
			aPackagesStatus[data.uid] = data.state;
		};

		toggleHistory = function (event) {
			var $that = jQuery(this).parent('.bd-toggle');
			if ($that.find('.bd-dropdown').slideToggle('fast', function () {
				if (jQuery(this).is(':hidden')) {
					$that.removeClass('bd-open');
					be.utils.setCookie({
						'bd-pub-showApprovalHistory': 'false'
					});
				}
			}).is(':visible')) {
				$that.addClass('bd-open');
				be.utils.setCookie({
					'bd-pub-showApprovalHistory': 'true'
				});
			}
		};
        var resetFilters = function(){
            // clear filters and refresh
            filter.reset();
            $('.bc-label', $statusFilter).text(LABEL_STATUS).attr('title', LABEL_STATUS);
            $('.bc-label', $userFilter).text(LABEL_USER).attr('title', LABEL_USER);
            $('.bd-publish-app-filter-date .bc-date-input', $oGadgetBody).val('');
            $nameFilter.val('');
            refresh();
        };

        var updateOverview = function (aStatusList, aFilterList, type) {
            return publishing.getPackagesListCount(aStatusList, aFilterList, type).then(function (data) {
                overviewObj = bd.xmlToJson({xml: data.firstChild});
            });
        };

        var getMetaData = function () {
            return be.utils.ajax({
                url: REST_API_URLS.metadata,
                dataType: "xml",
                type: "GET",
                cache: false
            });
        };

		/* start up */
		init = function() {

            var REST_API_BASE_URL;

            publishing = new Publishing();

            if( typeof publishing.portalServerHost ==='undefined') {
                throw new Error('Unable to get the portalServerHost: ' + publishing.portalServerHost );
            }

            REST_API_BASE_URL = bd.contextRoot + '/orchestrator/hosts/'
                + publishing.portalServerHost + '/portals/'
                + (bd.selectedPortalName === 'dashboard' ? '[BBHOST]' : bd.selectedPortalName)
                + '/workpackages/';
            REST_API_URLS = {
                'list': REST_API_BASE_URL + 'list/{{lastUpdate}}',
                'statuses': REST_API_BASE_URL + 'statuses/' + '{{lastUpdate}}?all=true',
                'metadata': REST_API_BASE_URL + 'metadata',
                'overview': {
                    'approval': REST_API_BASE_URL + 'approval-overview',
                    'publishing': REST_API_BASE_URL + 'publishing-overview',
                    'suffixes': {
                        'MAIN_LIST_SUFFIX': 'Count',
                        'HISTORY_LIST_SUFFIX': 'HistoryCount'
                    },
                    'statusParameterName': 'status'
                }
            };

			HTMLTemplateURL = be.contextRoot + "/static/backbase.com.2012.tango/widgets/PublishApp/html/";
			HEADER_URL = HTMLTemplateURL + "publishAppListHeader.html";

			defaults = {
				publishing: {
					message: MSG_PUB_STATUS,
					template: 'packageList',
					callback: showDetails,
					detailsCallback: detailsCallback,
                    headerText: "Publications in progress",
                    historyHeaderText: "Publication history"
				},
				approval: {
					message: MSG_APPROVAL_STATUS,
					template: 'packageListApproval',
					callback: showDetails,
					detailsCallback: detailsCallback,
                    headerText: "Awaiting approval",
                    historyHeaderText: "Approval history"
				},
				settings: {
					message: MSG_PORTAL_SETTINGS.replace("{{portalTitle}}", bd.selectedPortalTitle),
					template: 'settings'
				}
			};

            $oGadgetBody = jQuery(oGadgetBody);

            aPackagesStatus = {};
            oTransitionMessages = {
                'NOT PUBLISHED': {
                    'PUBLISHED': {
                        message: MSG_PUB_DONE,
                        icon: 'checkboxIcon' // checkboxIcon, attentionIcon, errorIcon, loadingIcon
                    }
                }
            };

            /* get data */
            currentTabId = 'approval';

            var start = function(data) {
                currentTabId = bd.publishing.selectedSection || "approval";
                delete bd.publishing.selectedSection;

                // populate publishers array (for user drop down)
                publishers = [];
                $('publisherusername', $(data)).each(function(item){
                    publishers.push($(this).text());
                });

                /*----------------------------------------------------------------*/
                /* Create poller to get the statuses for all the users
                 /*----------------------------------------------------------------*/

                publishingPoller = new be.utils.notifier(REST_API_URLS.statuses);

                initFilters(currentTabId);
                // refresh(false);
            };

            var list = statusMapping[currentTabId].list,
                aStatusList = list.availableStatuses,
                aFilterList = list.filters;

            $.when(getMetaData(), updateOverview(aStatusList, aFilterList, currentTabId)).done(start);

			aPackagesStatus = {};
			oTransitionMessages = {
				'NOT PUBLISHED': {
					'PUBLISHED': {
						message: MSG_PUB_DONE,
						icon: 'checkboxIcon' // checkboxIcon, attentionIcon, errorIcon, loadingIcon
					}
				}
			};

			jQuery(".bd-pageTitle", $oGadgetBody).text('Publish | ' + bd.selectedPortalTitle);

            $menuItemsContainer = jQuery('.bc-tab-labels', $oGadgetBody);
			$menuItems = jQuery(".bc-tab-labels .bc-tab-label", $oGadgetBody);
            $publishAppFilters = jQuery('.bd-publish-app-filters', $oGadgetBody);

			tabs = jQuery(".bd-publishApp-listContainer > .bd-publishApp-list", $oGadgetBody);
			tabPublishing = jQuery(".bd-publishApp-listContainer > .bd-publishApp-accordion", $oGadgetBody);
			tabSettings = jQuery(".bd-publishApp-listContainer.bd-pub-settings > .bd-publishApp-list", $oGadgetBody);

			msgType = jQuery(".bd-publishApp-header > h3", $oGadgetBody);

			detailsPane = jQuery(".bd-publishApp-detailsContainer", $oGadgetBody);
			detailsPanePublish = jQuery(".bd-pub-publish-details", detailsPane);
			detailsPaneApproval = jQuery(".bd-pub-approval-details", detailsPane);


			/* bindings */
			$menuItems.on("click", menuClick);

			$oGadgetBody.delegate(".bd-pub-refresh-btn", "click", refresh);

			jQuery(".bd-pub-actions.bd-large-buttons li", $oGadgetBody).delegate("click", function () {
				jQuery(this).toggleClass("bc-active-tab");
			});

			$oGadgetBody.delegate('.bd-ui-resize-grip', 'mousedown', function (evt) {
				var totalHeight = jQuery('.bd-publishApp-listContainer > .bd-publishApp-accordion', $oGadgetBody).height();

				var $list = jQuery('.bd-publishApp-list', $oGadgetBody).eq(0);
				var $historyList = jQuery('.bd-publishApp-list', $oGadgetBody).eq(1);
				var initialHeight = $list.height();
				var initialY = evt.clientY;

				var topHeight, bottomHeight;

				$oGadgetBody.on('mousemove', function (evt) {
					topHeight = initialHeight - (initialY - evt.clientY);
					bottomHeight = totalHeight - topHeight - grippyHeight;

					if (bottomHeight < headerHeight) {
						bottomHeight = headerHeight;
						topHeight = totalHeight - bottomHeight - grippyHeight;
					} else if (topHeight < headerHeight) {
						topHeight = headerHeight;
						bottomHeight = totalHeight - topHeight - grippyHeight;
					}
					$list.height(topHeight);
					$list.find('.bd-publishApp-list-container').height(topHeight - headerHeight);
					$historyList.height(bottomHeight);
					$historyList.find('.bd-publishApp-list-container').height(bottomHeight - headerHeight);
				}).on('mouseup', function (evt) {
					$oGadgetBody.off('mousemove');
				});
			});

			$oGadgetBody.delegate('.bd-publishApp-list > h4', 'click', function (evt) {
				var totalHeight = jQuery('.bd-publishApp-listContainer > .bd-publishApp-accordion', $oGadgetBody).height(),
					topHeight, bottomHeight;

				var $list = jQuery('.bd-publishApp-list', $oGadgetBody).eq(0);
				var $historyList = jQuery('.bd-publishApp-list', $oGadgetBody).eq(1);

				var $targetContainer = jQuery(evt.target).parent();

				var toggleTransition = function (elmList) {
					for (var i = 0, l = elmList.length; i < l; i++) {
						elmList[i].toggleClass('bd-publishApp-height-transition');
					}
				};
				toggleTransition([$list, $list.find('.bd-publishApp-list-container'), $historyList, $historyList.find('.bd-publishApp-list-container')]);
				setTimeout(function () {
					toggleTransition([$list, $list.find('.bd-publishApp-list-container'), $historyList, $historyList.find('.bd-publishApp-list-container')]);
				}, 500);

				if ($targetContainer[0] === $list[0]) {
					bottomHeight = headerHeight;
					topHeight = totalHeight - bottomHeight - grippyHeight;
				} else if ($targetContainer[0] === $historyList[0]) {
					topHeight = headerHeight;
					bottomHeight = totalHeight - topHeight - grippyHeight;
				}

				$list.height(topHeight);
				$list.find('.bd-publishApp-list-container').height(topHeight - headerHeight);
				$historyList.height(bottomHeight);
				$historyList.find('.bd-publishApp-list-container').height(bottomHeight - headerHeight);
			});

			$oGadgetBody.delegate(".bd-pub-tableList > tbody > tr", "click", rowClick);



			/*----------------------------------------------------------------*/
			/* Create poller to get the statuses for all the users
			/*----------------------------------------------------------------*/
			if( typeof publishing.portalServerHost ==='undefined') {
				throw new Error('Unable to get the portalServerHost: ' + publishing.portalServerHost );
			}

			publishingPoller = new be.utils.notifier(REST_API_URLS.statuses);

			$oGadgetBody.on('publishing-done', function(ev, data) {
				// Publishing is done to a final states
			});
		};

        this.refresh = refresh;
		init();

	};

	displayError = function(oGadgetBody){
		var $oGadgetBody = jQuery('.bd-widgetContent', oGadgetBody);
		$oGadgetBody.html(be.utils.processHTMLTemplateByUrl(be.contextRoot + '/static/backbase.com.2012.tango/widgets/PublishApp/html/notAvailable.html'));
	};

	this.Maximized = load;
	this.Dashboard = function() {console.log('Dashboard method is deprecated')};
	this.displayError = displayError;
	bd.widgets.PublishAppLoaded = true;
});
