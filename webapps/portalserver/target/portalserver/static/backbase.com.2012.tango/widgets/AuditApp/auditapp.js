/**
 * Copyright © 2012 Backbase B.V.
 */
(function($){ // <-- to make sure $ in our code points to jQuery
    b$.module("bd.widgets.AuditApp", function() {
        /*
        * config/ constants
        * */
        var URL_HTML_TEMPLATE_EVENTS        = be.contextRoot + "/static/backbase.com.2012.tango/widgets/AuditApp/html/auditEventsList.html";
        var URL_HTML_TEMPLATE_PORTALS       = be.contextRoot + "/static/backbase.com.2012.tango/widgets/AuditApp/html/portalList.html";
        var URL_HTML_TEMPLATE_FOLD_OUT      = be.contextRoot + "/static/backbase.com.2012.tango/widgets/AuditApp/html/foldOut.html";
        var STANDARD_PAGE_SIZE              = 40;
        var ALL_PORTALS                     = "All Portals";
        var URL_AUDIT                       = be.contextRoot + "/auditevents/";
        // URL_AUDIT                           = be.contextRoot + "/static/backbase.com.2012.tango/widgets/AuditApp/xml/audit_test.xml";
        var URL_AUDIT_META                  = be.contextRoot + "/auditmeta/";
        var NOTIFICATION_META               = "meta";
        var NOTIFICATION_CHANGE             = "change";
        var FOLD_OUT_ACTION_NAME            = "event-type";
        var FOLD_OUT_ITEM_TYPE_NAME         = "item-type";
        var FOLD_OUT_USER_NAME              = "user";
        var FOLD_OUT_ITEM_NAME_ACTION       = "Event";
        var FOLD_OUT_ITEM_NAME_ITEM_TYPE    = "Type";
        var FOLD_OUT_ITEM_NAME_USER         = "User";
        var ITEM_MESSAGE                    = "Enter Item Name";
        var NO_TITLE                        = "[no title]";

        var ACTION_DISPLAY_TEXT_MAP         = {
            REVOKED_PACKAGE: "REVOKED",
            REJECTED_PACKAGE: "REJECTED",
            APPROVED_PACKAGE: "APPROVED",
            PUBLISHED_PACKAGE: "PUBLISHED",
            APPROVED_PUBLISHED: "APPROVED",
            APPROVED_UNPUBLISHED: "APPROVED",
            REJECTED_PUBLISHED: "REJECTED",
            REJECTED_UNPUBLISHED: "REJECTED",
            REVOKED_PUBLISHED: "REVOKED",
            REVOKED_UNPUBLISHED: "REVOKED"
        };
        var actionDisplayTextReverseMap = {};
        var USER_COLORS;

        var DateLib             = b$.require('bd.date.DateLib');
        var FoldOutCollection   = b$.require('bd.ui.foldout.FoldOutCollection');
        var FoldOut             = b$.require('bd.ui.foldout.FoldOut');
        var dateLib; // DateLib instance

        var startDate, oldestDate;

        var Class = b$.Class;
        var $body, widget;

        // andy: get user list and dedupe
        var getUserColors = function(oNames, propertyName) {
            var hex, colors = {}, color = {};
            var i = 0;
            jQuery(oNames).each(function(a){
                hex = "#" + Math.random().toString(16).slice(2, 8);
                while(!colors[hex] && i++ < 100){
                    hex = "#" + Math.random().toString(16).slice(2, 8);
                    colors[hex] = true;
                }
                colors[this[propertyName]] = hex;
            });

            return colors;
        };

        /*
         * replaces non-alphanumeric characters with empty string
         * and replaces spaces with underscores
        */
        var idfy = function(str){
            str = str.toLowerCase();
            str = str.replace(/\s/g, "_");
            str = str.replace(/[^a-z0-9_]/gi, "");
            return str;
        };
        var formatDateForServer = function(dateString){
//            return encodeURIComponent(DateLib.dateToTimestamp(dateString));
//            return DateLib.dateToTimestamp(dateString);
//            return encodeURI(DateLib.dateToTimestamp(dateString));
//            console.log(encodeURIComponent(DateLib.dateToTimestamp(dateString)));
//            return encodeURIComponent(dateLib.formatDateForServer(dateString));
            return encodeURIComponent(DateLib.dateToTimestamp(dateString));
        };
        var getFromDate = function(val){
            val = val || dateLib.currentDay();
            var t = DateLib.delimiter + "00" + DateLib.delimiter + "00" + DateLib.delimiter + "00" + DateLib.delimiter + "000";
            return formatDateForServer(val + t);
        };
        var getToDate = function(val){
            val = val || dateLib.currentDay();
            var t = DateLib.delimiter + "23" + DateLib.delimiter + "59" + DateLib.delimiter + "00" + DateLib.delimiter + "000";
            return formatDateForServer(val + t);
        };

        // value objects
        var AuditListVO = function(arrEvents, totalNrOfEvents){
            var i;
            this.list               = arrEvents;
            this.hasItems           = arrEvents.length > 0;
            if(this.hasItems){
                this.totalNrOfEvents    = parseInt(totalNrOfEvents);
                this.timezone           = "(" + DateLib.getGMTFromTimeStamp(arrEvents[0].timestamp) + ")";
                this.listDate           = arrEvents[0].date.split(" ")[0];
                this.hasMoreItems       = arrEvents.length < this.totalNrOfEvents;
            }
        };
        var AuditEventVO = function(obj){
            this.id         = obj.id;                                              //  - id (not used now)
            this.userName   = obj.userName;                                        //  - username (e.g. 'admin')
            this.actionText = ACTION_DISPLAY_TEXT_MAP[obj.action] || obj.action;   //  - display action
            this.action     = actionDisplayTextReverseMap[this.actionText] || obj.action;  //  - action (e.g. CREATE, UPDATE, DELETE). Could be multiple actions separated by comma.
            this.context    = obj.context ? obj.context : "(none)";                //  - context name (usually the name of the portal)
            this.itemType   = obj.itemType.toLowerCase();                          //  - item type
            this.itemName   = obj.itemName ? obj.itemName : "(none)";              //  - item name (generated item name, unique within the scope of the context)
            this.title      =  function(){                                         //  - item title (if it has one, human-readable)
                var hasTitle = obj.itemTitle && typeof obj.itemTitle === "string";
                var t =  hasTitle ? obj.itemTitle : obj.itemName;
                t = t.replace(/_/g, " ").replace(/&/g, '&amp;');
                if(!hasTitle){
                    return this.itemType !== "user" ? NO_TITLE + " " + t : t;
                }
                return t;
            }
            this.timestamp  = DateLib.localizeTimestamp(obj.timestamp);            //  - event timestamp (localized)
            this.date       = bd.date.formatDateTime(this.timestamp);
            this.day        = DateLib.getDayFromTimeStamp(this.timestamp);
            this.hr_action  = obj.actionText;
            this.newDay     = null;
            this.color      = function(){
                return USER_COLORS[obj.userName];
            };
        };

        var ObserverVO = function(sEventType, fFunc, oContext){
            this.eventType  = sEventType;
            this.func       = fFunc;
            this.context    = oContext;
        };

        var FilterVO = function(obj){
            obj             = obj || {};
            this.context    = obj.context;
            this.itemType   = obj.itemType;
            this.action     = obj.action;
            this.item       = obj.item;
            this.itemTitle  = obj.itemTitle;
            this.user       = obj.user;
            this.from       = obj.from;
            this.to         = obj.to;
            this.ps         = obj.ps || STANDARD_PAGE_SIZE;
            this.of         = obj.of;
            this.filter     = this.getFilter();
        };

        FilterVO.prototype.getFilter = function(){
            var f = [];
            if(this.context)            f.push("context="   + this.context);
            if(this.itemType)           f.push("itemtype="  + this.itemType);
            if(this.action)             f.push("action="    + this.action);
            if(this.item)               f.push("item="      + this.item);
            if(this.itemTitle)          f.push("itemtitle=" + this.itemTitle);
            if(this.user)               f.push("user="      + this.user);
            if(this.from)               f.push("from="      + this.from);
            if(this.to)                 f.push("to="        + this.to);
            if(this.ps)                 f.push("ps="        + this.ps);
            if(this.of)                 f.push("of="        + this.of);
            return f.join("&");
        };

        var Observable = Class.extend(function(){
            this.observers = [];
        });
        Observable.prototype.addObserver = function(vo){
            this.observers.push(vo);
        };
        Observable.prototype.notifyObservers = function(eventType, eventData){
            var i, obs;
            for(i = 0; i < this.observers.length; i++){
                obs = this.observers[i];
                if(obs.eventType === eventType){
                    obs.func(eventData);
                }
            }
        };

       /*
       * data storage/ service
       * */
        var Model = Observable.extend(function(){
             Observable.call(this);
        });

        Model.prototype.request = function(iParams, notification){

            var params = iParams || {};
            var filter  = params && typeof params.filter != "undefined" && params.filter.length > 0 ? "?" + params.filter : "";
            var url     = params.url + filter;
            var that    = this;

            // console.log("getting", url);

            be.utils.ajax({
                url         : url,
                data        : params.data,
                processData : true,
                dataType    : "xml",
                cache       : false,
                type        : "GET",
                encodeURI   : false,
                success     : function (data){
                    data = bd.xmlToJson({xml:data});
                    if(!data) return;
                    if(params.processData){
                        data = params.processData(data);
                    }
                    that.notifyObservers(notification, data);
                }
            });
        };

        Model.prototype.getMetadata = function(){
            var params = {};
            params.url = URL_AUDIT_META;
            this.request(params, NOTIFICATION_META);
        };

        Model.prototype.getList = function (iParams){
            var params = iParams || {};
            params.url = URL_AUDIT;
            params.processData = this.setList;
            this.request(params, NOTIFICATION_CHANGE);
        };
        Model.prototype.setList = function (data){
            var i, events, totalNrOfEvents = 0;
            if(data.auditEvents.oldestAuditDate){
                oldestDate = DateLib.localizeTimestamp(data.auditEvents.oldestAuditDate);
            }
            // console.log("data", oldestDate); //oldestAuditDate

            totalNrOfEvents = data.auditEvents.numberOfEvents;
            events          = be.utils.convertToArray(data.auditEvents.auditEvent);
            for(i = 0; i < events.length; i++){
                events[i] = new AuditEventVO(events[i]);
            }

            data = new AuditListVO(events, totalNrOfEvents);

            return data;

        };

        // mediator/ controller
        var Controller = function(view, model){

            var refreshList = function(params){
                model.getList(params);
            };
            var onModelUpdate = function(data){
    //            console.log("model update", data)
                view.update(data);
            };
            var onViewUpdate = function(data){
    //            console.log("view update", data)
                model.getList(data);
            };
            var onMetadata = function(data){
                view.setMetadata(data);
            };
            var init = function(){
                model.getMetadata();
                var vo = new FilterVO({
                    from:getFromDate(),
                    to:getToDate()
                });
                refreshList(vo);
            };
            return {
                refreshList     : refreshList,
                onModelUpdate   : onModelUpdate,
                onViewUpdate    : onViewUpdate,
                onMetadata      : onMetadata,
                init            : init
            };
        };

        /*
        * view (component)
         */
        var View = Observable.extend(function(){


            var elSelectedPortal,
                that            = this,
                mainContent     = jQuery(".bd-audit-main"            ,$body),
                list            = jQuery(".bd-audit-list-container" , $body),
                portalMenu      = jQuery(".bd-audit-nav"            , $body),
                searchBox       = jQuery(".bd-audit-item-input"     , $body),
                searchCancelBtn = jQuery('.bd-audit-searchholder .bd-icon-wrapper > .bc-icn');
                $dateField      = jQuery(".bd-audit-date-input"     , $body);

            var form = {
                  form      : jQuery(".bd-audit-header"      , $body)
                , from      : jQuery(".bd-audit-date-input"  , $body)
                , to        : null
                , context   : null
                , user      : null
                , users     : []
                , item      : null
                , prevItem  : null
                , itemTitle : jQuery(".bd-audit-item-input"  , $body).val(ITEM_MESSAGE)
                , itemTypes : []
                , actions   : []
                , of        : null
                , page      : 0
                , size      : STANDARD_PAGE_SIZE
                , tempVal   : ""

                , testDate : function(str){
                    return dateLib.testDate(str);
                }

                , formatDate : function(str){
                    return dateLib.formatDate(str);
                }
                , setFromDate : function(val){
                    this.from.val(val);
                }
                , getFromDate : function(){
                    return getFromDate(this.from.val());
                }
                , getToDate : function(){
                    // sending same date for 'to' and 'from' will fetch the full ('to') day
                    return getToDate(this.from.val());
                }
                , setUser : function(val){
                    val = jQuery.trim(val);
                    this.user = val;
                    if(val == ""){
                        this.users = [];
                    }
                }
                // user: multi select mode
                , getUser : function(){
                    var val = this.users.length > 0 ? this.users.join("||") : null;
                    return val;
                }
                , addUser : function(val, noToggle){
                    var i;
                    val = jQuery.trim(val);
                    for(i = 0; i < this.users.length; i++){
                        if(this.users[i] === val){
                            return noToggle || this.removeUser(val);
                        }
                    }
                    this.users.push(val);
                }
                , removeUser : function(val){
                    var i;
                    val = jQuery.trim(val);
                    for(i = 0; i < this.users.length; i++){
                        if(this.users[i] === val){
                            this.users.splice(i,  1);
                            break;
                        }
                    }
                }
                , clearUsers : function(){
                    this.users = [];
                }
                , getItem : function(){
                    var val = this.item;
                    return val || null;
                }
                , setItem : function(val){
                    if(val != "" && this.prevItem == val){
                        this.item = "";
                    }else{
                        this.prevItem = this.item;
                        this.item = val;
                    }
                }
                , getItemTitle : function(){
                    var val = this.itemTitle.val();
                    if(val == ITEM_MESSAGE) val = null;
                    return val || null;
                }
                , setItemTitle : function(val){
                    this.itemTitle.val(val);
                }
                , getContext : function(){
                    return this.context;
                }
                , setContext : function(val){
                    this.resetOffset();
                    this.context = val;
                }
                , getItemType : function(){
                    var val = this.itemTypes.length > 0 ? this.itemTypes.join(",").toUpperCase() : null;
                    return val;
                }
                , addItemType : function(val, noToggle){
                    var i;
                    val = jQuery.trim(val).toUpperCase();
                    for(i = 0; i < this.itemTypes.length; i++){
                        if(this.itemTypes[i] === val){
                            return noToggle || this.removeItemType(val);
                        }
                    }
                    this.itemTypes.push(val);
                }
                , removeItemType : function(val){
                    var i;
                    val = jQuery.trim(val).toUpperCase();
                    for(i = 0; i < this.itemTypes.length; i++){
                        if(this.itemTypes[i] === val){
                            this.itemTypes.splice(i,  1);
                            break;
                        }
                    }
                }
                , getAction : function(){
                    var val = this.actions.length > 0 ? this.actions.join(",").toUpperCase() : null;
                    return val;
                }
                , clearActions : function(){
                    this.actions = [];
                }
                , addAction : function(val, noToggle){
                    var i;
                    val = jQuery.trim(val).toUpperCase();
                    for(i = 0; i < this.actions.length; i++){
                        if(this.actions[i] === val){
                            return noToggle || this.removeAction(val);
                        }
                    }
                    this.actions.push(val);
                }
                , removeAction : function(val){
                    var i;
                    val = jQuery.trim(val).toUpperCase();
                    for(i = 0; i < this.actions.length; i++){
                        if(this.actions[i] === val){
                            this.actions.splice(i,  1);
                            break;
                        }
                    }
                }
                , getNrOfActions : function(){
                    return actions.length;
                }
                , getNrOfTypes : function(){
                    return types.length;
                }
                , getPageSize : function(){
                    return this.size || STANDARD_PAGE_SIZE;
                }
                , setPageSize : function(val){
                    this.size = val;
                }
                , getPage : function(){
                    return this.page;
                }
                , resetOffset : function(){
                    this.of = null;
                    this.page = 0;
                }
                , getOffset : function(){
                    return this.of;
                }
                , upOffset : function(){
                    if(this.of == null){
                        this.of = 0;
                        this.page = 0;
                    }
                    this.page += 1;
                    this.of += this.getPageSize();
                }
                , reset : function(){
                    this.resetOffset();
                    this.setUser("");
                    this.setContext("");
                    dateLib.reset();
                    this.setFromDate(startDate);
                    this.itemTypes = [];
                    this.actions = [];
                    this.setItem("");
                    this.setItemTitle("");
                    //$('.bc-close').click();
                    //this.searchCancelBtn("");
                }
                , submit : function(up){
                    if(up){
                        this.upOffset();
                    }else{
                        this.resetOffset();
                    }
                    sendData();
                }
                , addEventListeners : function(){
                    this.form
                        .off()
                        .on("submit", "form", function(e){
                            e.preventDefault();
                            if(validDate(e)){
                                form.submit();
                            }
                            return false;
                        })
                        .on("focus", ".bd-audit-item-input", function(e){
                            var $this = jQuery(this);
                            if($this.val() === ITEM_MESSAGE){
                                $this.val("");
                            }
                        })
                        .on("blur", ".bd-audit-item-input", function(e){
                            var $this = jQuery(this);
                            if($this.val() === ""){
                                $this.val(ITEM_MESSAGE);
                            }
                        });

                    searchBox.on('keyup', function(e){
                        if (searchBox.val() !== '') {
                            searchCancelBtn.removeClass('bc-search-black').addClass('bc-close');
                        } else {
                            searchCancelBtn.removeClass('bc-close').addClass('bc-search-black');
                        }
                    });


                    searchCancelBtn.on('click', function(e){
                        searchBox.val('');
                        // searchCancelBtn.removeClass('bc-close').addClass('bc-search-black');
                        searchBox.trigger('keyup');
                    }
                    );
                }

            };

            Observable.call(this);

            var sendData = function(){
                // clear list
                list.html('<p class="bd-audit-message bd-loading"></p>');
                // collect form data, fill VO and notify
                var vo = new FilterVO({
                    from        : form.getFromDate(),
                    to          : form.getToDate(),
                    user        : form.getUser(),
                    item        : form.getItem(),
                    itemTitle   : form.getItemTitle(),
                    context     : form.getContext(),
                    itemType    : form.getItemType(),
                    action      : form.getAction(),
                    ps          : form.getPageSize(),
                    of          : form.getOffset()
                });
// console.log("sendData", vo);
                that.notifyObservers(
                    NOTIFICATION_CHANGE,
                    vo
                );
            };

            var reset = function(){
                if(that.foldOuts){
                    that.foldOuts.clear();
                }
                form.reset();
            };

            var changeContext = function(contextName){
                jQuery(".bd-audit-" + contextName, this.portalMenu).trigger("click");
            };

            var setDate = function(dir){
                var val = $dateField.val();
                var d = DateLib.parseDate(val);
                if(d){
                     if(dir === 1){
                         if(val != startDate.toString()){
                            d = d.next();
                        }
                    }else {
                        if(!oldestDate || DateLib.diff(oldestDate, DateLib.dateToTimestamp(val)) > 0){
                            d = d.prev();
                        }
                    }

//                    if(d.isLastNextDay){
//                        // this.$dateField.parent().parent().children('.bd-date-incr').addClass('bd-audit-btn-disabled')
//                    }else{
//                        $dateField.parent().parent().children('.bd-date-incr').removeClass('bd-audit-btn-disabled')
//                    }
//
//                    if(d.isLastPrevDay){
//                        // this.$dateField.parent().parent().children('.bd-date-decr').addClass('bd-audit-btn-disabled');
//                    }else{
//                        $dateField.parent().parent().children('.bd-date-decr').removeClass('bd-audit-btn-disabled')
//                    }

                    if($dateField.val() != d.toString(bc.dateTimeFormat.dateShort)){
                        $dateField.val(d.toString(bc.dateTimeFormat.dateShort));
                        form.submit();
                    }
                }
            };

            var resetMenu = function(){
                jQuery("li", this.portalMenu).removeClass("bd-audit-selected");
                this.elSelectedPortal = jQuery(".bd-audit-portal-all", this.portalMenu).addClass("bd-audit-selected");
            };


            var validDate = function(){
                //TODO: change this check, move to datelib at least
                var val = $dateField.val();

                var delimiter = bc.dateTimeFormat.dateShort.match(/([\/\.\-])/)[0];
                var tmp = bc.dateTimeFormat.dateShort.split(delimiter);
                for (var i = 0; i < 3; i++){
                    tmp[i] = tmp[i].replace(/^[dM]{1}$/, "\\d{1,2}")
                             .replace(/^(dd|MM)$/, "\\d{2}")
                             .replace(/^y$/, "\\d{2,4}")
                             .replace(/^yy$/, "\\d{4}");
                }
                var dateRe = new RegExp(tmp.join("\\" + delimiter));
                if(val.match(dateRe) === null){//find anything that is not the digit or /
                    bc.component.notify({
                        uid: '12321',
                        icon: 'attention', // icn-checkbox, icn-attention, icn-error, icn-loading
                        message: "Please enter date as " + bc.dateTimeFormat.dateShort,
                        delay: 2000 // optional time on screen before fade-out, default is 1000 });
                    });
                    return false;
                }
                return true;
             };

            var initFoldOuts = function(){

                var $filters = jQuery(".bd-filters", $body);

                var fos = new FoldOutCollection();

                // configure actions (event types) fold out
                var foldOutActions = jQuery(".bd-fold-out.bd-fold-out-actions", $filters);
                foldOutActions = new FoldOut(foldOutActions, FOLD_OUT_ACTION_NAME, FOLD_OUT_ITEM_NAME_ACTION);
                var foldOutActionsOptionClick = function($el){
                    var action = $el.data("id");
                    if($el.hasClass("bd-selected")){
                        form.addAction(action);
                    }else{
                        form.removeAction(action);
                    }
                    form.submit();
                };
                var foldoutActionsAllClick = function($options){
                    $options.each(function(){
                        var val = jQuery(this).data("id");
                        form.addAction(val, true);
                    });
                    form.submit();
                };
                var foldoutActionsNoneClick = function($options){
                    $options.each(function(){
                        var val = jQuery(this).data("id");
                        form.removeAction(val);
                    });
                    form.submit();
                };
                foldOutActions.setOptionClick(foldOutActionsOptionClick);
                foldOutActions.setAllClick(foldoutActionsAllClick);
                foldOutActions.setNoneClick(foldoutActionsNoneClick);
                fos.add(foldOutActions);


                // configure item types fold out
                var foldOutItemTypes = jQuery(".bd-fold-out.bd-fold-out-item-types", $filters);
                foldOutItemTypes = new FoldOut(foldOutItemTypes, FOLD_OUT_ITEM_TYPE_NAME, FOLD_OUT_ITEM_NAME_ITEM_TYPE);
                var foldOutItemTypesOptionClick = function($el){
                    var itemType = $el.text();
                    if($el.hasClass("bd-selected")){
                        form.addItemType(itemType);
                    }else{
                        form.removeItemType(itemType);
                    }
                    form.submit();
                };
                var foldoutItemTypesAllClick = function($options){
                    $options.each(function(){
                        var val = jQuery(this).text();
                        form.addItemType(val, true);
                    });
                    form.submit();
                };
                var foldoutItemTypesNoneClick = function($options){
                    $options.each(function(){
                        var val = jQuery(this).text();
                        form.removeItemType(val);
                    });
                    form.submit();
                };
                foldOutItemTypes.setOptionClick(foldOutItemTypesOptionClick);
                foldOutItemTypes.setAllClick(foldoutItemTypesAllClick);
                foldOutItemTypes.setNoneClick(foldoutItemTypesNoneClick);
                fos.add(foldOutItemTypes);

                // configure user fold out
                var foldOutUsers = jQuery(".bd-fold-out.bd-fold-out-users", $filters);
                foldOutUsers = new FoldOut(foldOutUsers, FOLD_OUT_USER_NAME, FOLD_OUT_ITEM_NAME_USER);
                var foldOutUsersOptionClick = function($el){
                    var user = $el.text();
                    if($el.hasClass("bd-selected")){
                        form.addUser(user);
                    }else{
                        form.removeUser(user);
                    }
                    form.submit();
                };
                var foldoutUsersAllClick = function($options){
                    $options.each(function(){
                        var val = jQuery(this).text();
                        form.addUser(val, true);
                    });
                    form.submit();
                };
                var foldoutUsersNoneClick = function($options){
                    $options.each(function(){
                        var val = jQuery(this).text();
                        form.removeUser(val);
                    });
                    form.submit();
                };
                foldOutUsers.setOptionClick(foldOutUsersOptionClick);
                foldOutUsers.setAllClick(foldoutUsersAllClick);
                foldOutUsers.setNoneClick(foldoutUsersNoneClick);
                fos.add(foldOutUsers);

                // (remove and) add event listeners
                fos.addEventListeners();

                return fos;

            };

            var hideTimeline = function(data){

                jQuery(".bd-timeline", $body).hide();
                jQuery(".bd-filters", $body).addClass("no-timeline");
                list.addClass("no-timeline");

            };

            var init = function(){

                hideTimeline();
                form.addEventListeners();


                portalMenu.on( "click", "li", function( e ) {
                    var context = "";
                    jQuery(".bd-audit-selected").removeClass("bd-audit-selected");

                    elSelectedPortal = jQuery(this);
                    elSelectedPortal.addClass("bd-audit-selected");

                    context = elSelectedPortal.text() !== ALL_PORTALS ? elSelectedPortal.text() : "";

                    reset();
                    form.setContext(context);
                    form.submit();
                });

                mainContent
                    .off()
                    .on("click", ".bd-audit-button-bar-button-reset", function(e){
                        reset();
                        form.submit();
                        resetMenu();
                    })
                    .on("click", ".bd-audit-button-bar-button-more", function(e){
                        form.submit(true);
                    })
                    .on("click", ".bd-audit-button-bar-button-refresh", function(e){
                        form.submit();
                    })
                    .on("click", ".bd-audit-context", function(e){
                        var val = jQuery(this).text();
                        changeContext(val);
                    })
                    .on("click", ".bd-audit-action", function(e){
                        var val = jQuery(this).data("id");
                        that.foldOuts.selectOnly(FOLD_OUT_ACTION_NAME, val.toLowerCase());
                        form.clearActions();
                        form.addAction(val);
                        form.submit();
                    })
                    .on("click", ".bd-audit-user-name", function(e){
                        var val = jQuery(this).text();
                        that.foldOuts.selectOnly(FOLD_OUT_USER_NAME, val);
                        form.clearUsers();
                        form.addUser(val);
                        form.submit();
                    })
                    .on("click", ".bd-audit-item-name", function(e){
                        var val = jQuery(this).attr('name');
                        form.setItem(val);
                        form.submit();
                        form.setItem(""); // reset immediately
                    })
                    .on("click", ".bd-date-decr", function(e){
                        e.preventDefault();
                        if (!jQuery(this).hasClass('bd-audit-btn-disabled')) {
                           setDate(-1);
                        }
                    })
                    .on("click", ".bd-date-incr", function(e){
                        e.preventDefault();
                        if (!jQuery(this).hasClass('bd-audit-btn-disabled')) {
                           setDate(1);
                        }
                    })
                    .on("mouseover", ".bc-hovertip", bc.component.moTooltip)
                    .on("click", ".bd-audit-button-load-all", function(e){
                        form.setPageSize(-1);
                        form.submit();
                        form.setPageSize(STANDARD_PAGE_SIZE); // reset immediately
                    });

                // hides open fold out that is not currently clicked fold out
                $body.on("mouseup", function(e){
                    jQuery(".bd-fold-out").each(function(){
                        if(!((this == e.target) || jQuery.contains(this, e.target))){
                            jQuery(this).removeClass("bd-selected");
                        }
                    });
                });
            };

            init();

            // public
            this.portalMenu = portalMenu;
            this.form = form;
            this.list = list;
            this.$dateField = $dateField;
            this.initFoldOuts = initFoldOuts;
            this.resetMenu = resetMenu;
            this.sendData = sendData;
        });

        View.prototype.setMetadata = function(data){

            var eventTypesList, itemTypesList, userNamesList, portalMenuHTML;
            var eventTypes = [], eventTypesMapped = {}, itemTypes = [], userNames = [], contexts = [], i, temp;
            var temp, key, joinedEventTypes;

            data = data.auditMetadata;

            // quick conversions.
            temp = be.utils.convertToArray(data.eventTypes.eventType);
            for(i = 0; i < temp.length; i++){
                key = (ACTION_DISPLAY_TEXT_MAP[temp[i]] || temp[i]);
                if (eventTypesMapped[key]) {
                    eventTypesMapped[key].push(temp[i]);
                } else {
                    eventTypesMapped[key] = [temp[i]];
                }
            }
            for (key in eventTypesMapped) {
                if(eventTypesMapped.hasOwnProperty(key)) {
                    joinedEventTypes = eventTypesMapped[key].join();
                    eventTypes.push({item:key.toLowerCase(), id:joinedEventTypes});
                    actionDisplayTextReverseMap[key] = joinedEventTypes;
                }
            }

            temp = be.utils.convertToArray(data.itemTypes.itemType);
            for(i = 0; i < temp.length; i++){
                itemTypes.push({item:temp[i].toLowerCase(), id:idfy(temp[i])});
            }
            temp = be.utils.convertToArray(data.userNames.userName);
            for(i = 0; i < temp.length; i++){
                userNames.push({item:temp[i], id:idfy(temp[i])});
            }
            temp = be.utils.convertToArray(data.contexts.context);
            for(i = 0; i < temp.length; i++){
                contexts.push({item:temp[i], id:idfy(temp[i])});
            }

            USER_COLORS = USER_COLORS || getUserColors(userNames, "item");

            eventTypesList = be.utils.processHTMLTemplateByUrl(
                URL_HTML_TEMPLATE_FOLD_OUT, {items: eventTypes, cssName:FOLD_OUT_ACTION_NAME, title: FOLD_OUT_ITEM_NAME_ACTION + "s"}
            );
            jQuery(".bd-fold-out-actions", $body).html(eventTypesList);

            itemTypesList = be.utils.processHTMLTemplateByUrl(
                URL_HTML_TEMPLATE_FOLD_OUT, {items: itemTypes, cssName:FOLD_OUT_ITEM_TYPE_NAME, title: FOLD_OUT_ITEM_NAME_ITEM_TYPE + "s"}
            );
            jQuery(".bd-fold-out-item-types", $body).html(itemTypesList);

            userNamesList = be.utils.processHTMLTemplateByUrl(
                URL_HTML_TEMPLATE_FOLD_OUT, {items: userNames, cssName:FOLD_OUT_USER_NAME, title: FOLD_OUT_ITEM_NAME_USER + "s"}
            );
            jQuery(".bd-fold-out-users", $body).html(userNamesList);

            portalMenuHTML = be.utils.processHTMLTemplateByUrl(
                URL_HTML_TEMPLATE_PORTALS, {items:contexts, all:ALL_PORTALS}
            );
            this.portalMenu.html(portalMenuHTML);
        };

        View.prototype.update = function(data){
            var html;

            data.hasNext = data.totalNrOfEvents > (this.form.getPageSize() * (this.form.getPage() + 1));

            html = be.utils.processHTMLTemplateByUrl(
                URL_HTML_TEMPLATE_EVENTS, data
            );
            if(this.list && this.list.length > 0){

                this.list.html(html);

                this.foldOuts = this.foldOuts || this.initFoldOuts();
                if(!startDate){
                    startDate = dateLib.currentDay();
                    this.$dateField.val(startDate);
                    this.resetMenu();
                }
            }

            jQuery(".bd-audit-button-bar-button-more", this.mainContent).toggle(data.hasNext);

        };

        View.prototype.cleanUp = function(){
            jQuery(".bd-audit-list-header-container").remove();
        };

        // Application
        var initApp = function(gadget){

            var model = new Model(),
                view = new View(),
                controller = new Controller(view, model);

            dateLib = new DateLib();
            model.addObserver(new ObserverVO(NOTIFICATION_CHANGE, controller.onModelUpdate));
            model.addObserver(new ObserverVO(NOTIFICATION_META, controller.onMetadata));
            view.addObserver(new ObserverVO(NOTIFICATION_CHANGE, controller.onViewUpdate));

            controller.init();

            widget.Dashboard = cleanUp;
            bd.widgets.auditLoaded = true;
        };

        var load = function(oGadgetBody, sGadgetUrl) {
            $body = jQuery(oGadgetBody);
            initApp();

        }
        var cleanUp = function(){
            startDate = null;
        }

        widget = this;
        widget.Maximized = load;

    });
})(jQuery);
