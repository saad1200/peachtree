/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2013 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : content.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

be.utils.define('be.ice.model.content', [
	'jQuery',
    'be.ice.model.cmis',
    'be.ice.util',
    'be.utils',
    'be.ice.model.readObjectsStrategies'
], function ($, cmis, util, Utils, readObjectsStrategies) {

	'use strict';

	var getObject = cmis.getObject;
	var createFolder = cmis.createFolder;
	var saveNewContent = cmis.saveNewContent;
	var cmisReadContent = cmis.readContent;
	var deleteContent = cmis.deleteContent;
    var currentReadObjectsStrategy = {
        readObjects: _readObjects.bind(this)
    };
    readObjectsStrategies.setReadImmediatelyStrategy(currentReadObjectsStrategy);

	var config = {
		// ASSUMPTION: absolute urls point into static folder, relative into CS
		// absoluteUrl: /^https?:\/\/|^\$\(contextRoot\)\//,
		absoluteUrl: /^\$\(contextRoot\)\//,
		rootMarker: '$(contextRoot)',
		contextRoot: util.getContextRoot()
	};

	var relToContentRef = function(jsonStr, relationships){
        Object.keys(relationships).forEach(function (relId) {
            var curRelationship = relationships[relId];

            if (!curRelationship['bb:targetRepositoryId'] || !curRelationship['cmis:targetId']) {
                throw new Error('relationship object should have "bb:targetRepositoryId" and "cmis:targetId" properties');
            }
            jsonStr = jsonStr.replace(new RegExp('rel:' + relId, "g"),
                ['cs', curRelationship['bb:targetRepositoryId'] === 'contentRepository' ? 'contentRepository' :
                    '@portalRepository', curRelationship['cmis:targetId']].join(':'));
        });

        return jsonStr;
    };

	// folder helpers
	var getFolders = function(path) {
		return path.split('/').slice(1)
			.reduce(function(folders, folder) {
				var previous = folders.slice(-1) || '';
				folders.push(previous + '/' + folder);
				return folders;
			}, []);
	};
	var getOrCreateFolder = function(folder) {
		return getObject({
			path: folder,
			repository: be.ice.model.repositoryId,
			htmlDecode: true
		}, null, null, be.ice.model.repositoryId)
			// .fail doesn't chain, so use .then(null, failHandler)
			.then(null, function() {
				return createFolder({
					path: folder.replace(/\/[^\/]+$/, '') || '/',
					folderName: folder.replace(/^.*\//, ''),
					repository: be.ice.model.repositoryId
				});
			});
	};

	// api
	var createContent = function(path, content, meta) {
		// TODO: cleanup
		var fileName = path.replace(/^.*\//, ''),
			isRichText = meta && meta['cmis:objectTypeId'] === 'bb:richtext',
			contentType = (isRichText ? 'text/html' : 'application/json') + '; charset=utf-8',
			objectType = isRichText ? 'bb:richtext' : 'bb:structuredcontent';

		path = path.replace(/\/[^\/]+$/, '');
		var save = util.bind(saveNewContent, {
			path: path,
			fileName: fileName,
			content: content,
			// type 'text' cannot be versioned...
			//type: 'bb:richtext',
			type: contentType,
			objectType: objectType, //bd.uiEditingOptions.cmisTypeData['text'],
			properties: meta,
			repository: be.ice.model.repositoryId,
			htmlDecode: true
		});

		// save the content directly (fast path)
		return save()
			// .fail doesn't chain, so use .then(null, failHandler)
			.then(null, function(xhr) {
				if (xhr.status == 404) {
					// cannot save, folder doesn't exist yet (slow path)
					// check / create all folders on path
					return util.chain(getFolders(path)
						.map(function(folder) {
							return util.bind(getOrCreateFolder, folder);
						})
						// now folder should exist, so try again
						.concat(save)
					).then(function(results) {
						// cleanup return values, no need to return all
						// folder check/create results
						return results[results.length - 1];
					});
				}
				return xhr;
			});
	};


    /**
     * [readObject combine cmis.getObject and cmis.readContent together]
     */
    var readObject = function(path) {
        if (config.absoluteUrl.test(path)) {
            return getTemplate(path);
        } else {

            var deferred = $.Deferred();

            getEntryObject(path).then(function(object) {
                // console.log('readObject', object);
                var meta = object.meta;

                // only read content stream for bb:richtext
                if(meta && (meta['cmis:objectTypeId'] == 'bb:structuredcontent' || meta['cmis:objectTypeId'] == 'bb:richtext')) {

                    if (meta['cmis:contentStreamLength'] != '0') {
                        cmisReadContent(object.objectId, null, null, object.repositoryId, true).then(function(content) {
                            if ($.isXMLDoc(content)) {
                                content = be.utils.xml2text(content);
                            }

                            // cleanup reachtext
                            object.content = relToContentRef(content, object.relationships);
                            deferred.resolve(object);
                        });
                    } else {
                        deferred.resolve(object);
                    }

                } else {
                    object.browserRef = path;
                    deferred.resolve(object);
                }
            }, function(error){
                deferred.resolve({
                    error: 'Can not read CMIS object by path'
                });
            });
            return deferred.promise();
        }
    };

    /**
     * [getTemplate wrapper for cmis.readContent]
     */
	var getTemplate = function(path) {
		// TODO: move to model.js
		path = path.replace(config.rootMarker, config.contextRoot);
		return Utils.loadTemplateByUrl(path, true)
			// strip off status and xhr params
			.then(function(data) {
				return {content: data};
			});
	};

    /**
     * [readContent wrapper for cmis.readContent]
     */
	var readContent = function(path) {
		if (config.absoluteUrl.test(path)) {
			return getTemplate(path);
		} else {
			return getEntryObject(path)
				.then(function(object) {
					return cmisReadContent({
						contentUid: object.objectId
					});
				})
				// .fail doesn't chain, so use .then(null, failHandler)
				.then(null, function() {
					return $.when('');
				});
		}
	};

	function getObjectIdByPath(path) {
        var ref = path.split(':');

        if(ref && ref.length === 2){ // [repo]:[objectId]
            return ref[1];
        } else if(ref && ref.length === 3){ // cs:[repo]:[objectId]
            return ref[2];
        }

        return null;
	}

	function isPathALinkForTemplate(path) {
        return config.absoluteUrl.test(path);
    }

    /**
     * [getEntryObject description]
     * @param  {[type]} path [description]
     * @return Promise to return object {objectId: '...', name: '...', contentRef: '...'}
     */
    var getEntryObject = function(path) {
        var ref = path.split(':');
        if(ref && ref.length === 2){ // [repo]:[objectId]
            return cmis.getEntryObject(ref[1], ref[0]);
        } else if(ref && ref.length === 3){ // cs:[repo]:[objectId]
            if (ref[1] === '@portalRepository'){
                ref[1] = be.ice.model.repositoryId;
            }
            return cmis.getEntryObject(ref[2], ref[1]);
        }
        return cmis.getObject({
            path: path,
            repository: be.ice.model.repositoryId,
            htmlDecode: true
        });
    };

    function readObjects(pathArr) {
        return currentReadObjectsStrategy.readObjects(pathArr);
    }

    function _readObjects(pathArr) {
        var templatePaths = [],
            templatePromises = {},
            cmisPaths = [],
            cmisObjectIds = [],
            cmisObjectIdToPathMap = [];

        Utils.unique(pathArr).forEach(function (path) {
            if (config.absoluteUrl.test(path)) {
                templatePaths.push(path);
            } else {
                cmisPaths.push(path);
            }
        });

        templatePromises = templatePaths.map(function (path) {
            return getTemplate(path).then(function (template) {
                return {pathToRead: path, data: template};
            });
        });

        if (!cmisPaths.length) {
            if (templatePromises.length) {
                return $.when.apply($, templatePromises).then(function () {
                    return Array.prototype.slice.call(arguments);
                });
            } else {
                return $.when([]);
            }
        } else {
            cmisPaths.forEach(function (path) {
                var id = getObjectIdByPath(path);

                cmisObjectIdToPathMap.push({
                    id: id,
                    path: path
                });

                cmisObjectIds.push(id);
            });
        }

        return cmis.getEntryObjects(cmisObjectIds).then(function (cmisObjects) {
            parseCmisObjects(cmisObjects, cmisObjectIdToPathMap);

            if (templatePromises.length) {
                return $.when.apply($, templatePromises).then(function () {
                    var templateObjects = Array.prototype.slice.call(arguments);
                    return cmisObjects.concat(templateObjects);
                });
            } else {
                return cmisObjects;
            }
        });
    }

    /*
     * @param cmisObjectsReturned Objects returned from cmis.getEntryObjects
     * @param cmisObjectIdToPathMap Object map with ids/path of objects requested
     */
    function parseCmisObjects(cmisObjectsReturned, cmisObjectIdToPathMap) {
        cmisObjectIdToPathMap.forEach(function (objectIdToPath) {
            var objectId = objectIdToPath.id;

            var cmisObject = cmisObjectsReturned.filter(function (curCmisObject) {
                if (!curCmisObject.objectId) {
                    throw new Error('curCmisObject should have "objectId" property');
                }
                return curCmisObject.objectId === objectId;
            })[0];

            if (!cmisObject) {
                console.log("Warning. CMIS object with id=" + objectId + " not found in CMIS DB");
                return;
            }

            cmisObject.relationships = cmisObject.relationships.reduce(function (relationships, relationship) {
                cmisObjectsReturned.forEach(function (curCmisObject) {
                    if (curCmisObject.objectId === relationship.to) {
                        relationships[relationship.id] = {
                            'bb:targetRepositoryId': curCmisObject.repositoryId,
                            'cmis:targetId': curCmisObject.objectId
                        }
                    }
                });

                return relationships;
            }, {});

            var meta = cmisObject.meta;

            if(meta && (meta['cmis:objectTypeId'] === 'bb:structuredcontent' ||
                meta['cmis:objectTypeId'] === 'bb:richtext') && meta['cmis:contentStreamLength'] != '0') {

                cmisObject.content = relToContentRef(cmisObject.content, cmisObject.relationships);
            } else {
                cmisObject.browserRef = objectIdToPath.path;
            }
        });
    }

	/**
     * @method createOrUpdateContent
     * Creates or updates content, queueing the requests.
     * @param {String} path cmis path
     * @param {String} content JSON string of the content to be saved 
	 * @param {Object} meta properties of the cmis object
     * @returns {Object} Promise of the saving request.
     */
    function createOrUpdateContent(path, content, meta) {
		// property holding the promise of the last pending request for each path
        createOrUpdateContent.pending = createOrUpdateContent.pending || {};

        var pending = createOrUpdateContent.pending;
        var executeUpdate = function () {
			// a check for content existence is performed to determine if the content has to be created or updated
            return getEntryObject(path)
                .then(function(object) {
                    // the content exists => update 
                    var isRichText = meta && meta['cmis:objectTypeId'] === 'bb:richtext',
                        contentType = (isRichText ? 'text/html' : 'application/json') + '; charset=utf-8';
                    if (isRichText) {
                        delete meta['cmis:objectTypeId'];
                    }
                    return cmis.updateContent({
                        ContentUid: object.objectId,
                        content: content,
                        properties: meta,
                        repository: object.repositoryId,
                        type: contentType
                    });
                },
                function(xhr) {
                    if (xhr.status == 404) {
                        // the content does not exist => creation
                        return createContent(path, content, meta);
                    }
                    return xhr;
                });
        };
		
		// requests for the same path are queued
        pending[path] = $.when(pending[path]).then(executeUpdate, executeUpdate);
        pending[path].always(function(){
			// removal of last completed request for the path
            if (pending[path].state() !== 'pending') {
                delete pending[path];
            }
        });
        
        return pending[path];
    }
    
    function setReadObjectsStrategy (strategy) {
        currentReadObjectsStrategy = strategy;
    }

	return {
		createContent: createContent,
		readObject: readObject,
        getEntryObject: getEntryObject,
        getObjectIdByPath: getObjectIdByPath,
		readContent: readContent,
        readObjects: readObjects,
        isPathALinkForTemplate: isPathALinkForTemplate,

		// TODO: fix return value, it returns [] on update
		updateContent: createOrUpdateContent,

		// TODO: test this
		// TODO: remove empty folders as well?
		deleteContent: function(path) {
			return getObject(path)
				.then(function(object) {
					return deleteContent({
						objectId: object.id
					});
				});
		},
		getId: function(path) {
			return getObject(path)
				// catch the 404s
				.then(null, function() {
					return $.when('');
				});
		},
        setReadObjectsStrategy: setReadObjectsStrategy
	};
});
