/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2013 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : unified.js
 *  Description:
 *  Unified model
 *  ----------------------------------------------------------------
 */

be.utils.define('be.ice.model.unified', [
    'be.ice.model.xforms',
    'be.ice.model.portal',
    'be.ice.model.links',
    'be.ice.model.content',
    'be.ice.util',
    'be.ice.config',
    'be.utils',
    'jQuery'
], function (xforms, portal, links, content, util, config, Utils, $) {
    'use strict';

    var _testinit = function (xf, pm, lm, cm) {
        xforms = xf;
        portalmodel = pm;
        linkmodel = lm;
        contentmodel = cm;
    };

    var get = function (widget, optionalTemplate) {

        // TODO: linkRefs and contentRefs lives in the model now
        //      => no need to request CMIS and links

        var name = portal.getName(widget);
        var prefs = portal.getPreferences(widget);

        if (optionalTemplate) {
            prefs.forEach(function (item) {
                if (item.name == 'templateUrl') {
                    item.value = optionalTemplate;
                    return item;
                }
            });
        }

        var model = xforms.preferences2model(prefs, name);

        return processModelLinks(model).then(function () {
            return model;
        });
    };

    var processModelLinks = function processModelLinks(model, correlationId) {
        var itemsToRead = [],
            processedLinkPromises = [];

        Object
            .keys(model)
            .map(function (key) {
                return model[key];
            })
            .filter(function (item) {
                return item.ref !== undefined ||
                    item.contentRef !== undefined ||
                    item.browserRef !== undefined ||
                    item.linkRef !== undefined;
            })
            .forEach(function (item) {
                var pathToRead = item.ref || item.contentRef || item.browserRef;

                if (pathToRead) {
                    itemsToRead.push({item: item, path: pathToRead});
                } else if (item.linkRef) {
                    processedLinkPromises.push(
                        links.resolveLink({
                            uuid: item.linkRef
                        }).then(function (link) {
                            item.path = link.path;
                            return item;
                        })
                    );
                }
            });

        if (!itemsToRead.length) {
            return $.when.apply($, processedLinkPromises);
        }

        var pathsToRead = itemsToRead.map(function (curItemToPathObj) {
            return curItemToPathObj.path;
        });

        var readObjectsPromise = readObjects(pathsToRead, itemsToRead);
        processedLinkPromises.push(readObjectsPromise);

        return $.when.apply($, processedLinkPromises);
    };

    function readObjects(pathsToRead, itemsToRead) {
        return content.readObjects(pathsToRead).then(function (objects) {
            itemsToRead.forEach(function (curItemToPathObj) {
                var curItem = curItemToPathObj.item,
                    object = getObjectByItem(curItemToPathObj.path, objects);

                if (!object) {
                    console.log("Warning. Content object with path=" + curItemToPathObj.path +
                        " wasn't found.");
                    return;
                }

                // cmisObject - here must be data ()
                if (curItem.contentRef && object.content && object.meta &&
                    object.meta['cmis:objectTypeId'] == 'bb:structuredcontent') {

                    var json = JSON.parse(object.content.replace(/"\\&quot;|\\&quot;"/g, '\\\"')),
                        hasLinkToImage = typeof json.image === 'string',
                        changeToken;

                    if (hasLinkToImage) {
                        var imageCmisObjectId = content.getObjectIdByPath(json.image);

                        var imageCmisObject = objects.filter(function (curObject) {
                            return curObject.objectId === imageCmisObjectId;
                        })[0];

                        if (!imageCmisObject) {
                            console.log("Warning. CMIS image object with id=" + imageCmisObjectId +
                                " not found in CMIS DB");
                        } else {
                            changeToken = imageCmisObject.meta["cmis:changeToken"];
                        }
                    }

                    json = xforms.contentRef2obj(json, changeToken);
                    object.content = JSON.stringify(json);
                }

                $.extend(curItem, object);
                // if browserRef defined use it
                if (curItem.browserRef == curItem.contentRef) {
                    curItem.contentRef = null;
                }
            });
        });
    }

    /*
     * param path The path for specific object
     * objects The list of objects to find the specific one
     */
    function getObjectByItem(path, objects) {
        var result;

        objects.forEach(function (curObj) {
            if (result) {
                return;
            }

            if (curObj.pathToRead === path) {
                if (content.isPathALinkForTemplate(path)) {
                    result = curObj.data;
                }
            } else if (curObj.objectId === content.getObjectIdByPath(path)){
                result = curObj;
            }
        });

        return result;
    }

    var save = function (newmodel, oldmodel/*optional*/) {
        var widget = portal.getWidgetByName(newmodel.widgetName);
        // we allow passing in the oldmodel for easier testing
        return $.when(oldmodel || get(widget))
            .then(function (oldmodel) {
                var cleanModel = {};

                return util.chain(Object
                        .keys(newmodel)
                        // filter out template
                        .filter(function (key) {
                            return key !== 'template' && key !== 'widgetName';
                        })
                        // only save contentRefs
                        .filter(function (key) {
                            return newmodel[key].contentRef !== undefined;
                        })
                        .filter(function (key) {

                            var oldRef = oldmodel[key] && oldmodel[key].contentRef;

                            newmodel[key] = xforms.obj2contentRef(newmodel[key]);

                            // content reference already set to UUID
                            if (oldRef && oldRef.indexOf(':') > -1) {
                                newmodel[key].contentRef = oldRef;
                            }


                            var modified = oldmodel[key] === undefined || newmodel[key] !== oldmodel[key];


                            if (!modified && oldmodel[key]) {
                                // check if metadata modified
                                $.each(newmodel[key].meta || [], function (i, el) {
                                    var meta = oldmodel[key].meta;
                                    if (meta && meta[i] != el) modified = true;
                                });
                            }

                            // console.log(key, modified);

                            return modified;
                        })

                        .map(function (key) {
                            cleanModel[key] = newmodel[key];
                            var item = newmodel[key],
                                adjustContentRef = function (object) {
                                    if (object && object.contentRef) {
                                        // Modify contentRef from path to "repo:uuid" format
                                        item.contentRef = object.contentRef;
                                    } else {
                                        item.contentRef = contentRef;
                                    }
                                    return newmodel;
                                };
                            
                            if (item.meta && item.meta['cmis:objectTypeId'] == 'bb:richtext') {
                                var itemContent = item.content ? item.content.replace(/ _src="/gi, ' src="') : '';
                                return function () {
                                    return content.updateContent(item.contentRef, itemContent, item.meta).then(adjustContentRef);
                                };
                            }

                            var contentRef = item.contentRef;
                            delete item.contentRef;

                            var jsonStr = JSON.stringify(item);
                            jsonStr = jsonStr.replace(/ _src=\\"/gi, ' src=\\"');

                            return function () {
                                return content.updateContent(contentRef, jsonStr).then(adjustContentRef);
                            };
                        })
                ).then(function (data) {
                        var bbModel = xforms.model2preferences(newmodel);
                        // Invalidate cache on a widget
                        widget.model.setPreference('widgetContentsUpdated', new Date().getTime());

                        // All content operations are complete
                        // we can save portal model
                        portal.saveModel(widget, bbModel).then(function(){
                            if (top.bd && top.bd.observer) {
                                top.bd.observer.notifyObserver(top.bd.pm.observer.updateVisibleLink);
                            }
                        });

                        for (var i = 0; i < bbModel.length; i++) {
                            if (bbModel[i].type === 'contentRef') {
                                var contentUrl = bbModel[i].value;
                                var url = encodeURI(
                                    b$.portal.config.serverRoot +
                                    '/caches/all/portals/' + (b$.portal.portalName || '[BBHOST]') +
                                    '/content/' + contentUrl
                                );

                                // Clear cache on save.
                                var ajaxOptions = {
                                      type: 'PUT',
                                      url: url
                                };
                                be.utils.setXSRFHeader(ajaxOptions);
                                $.ajax(ajaxOptions);
                            }
                        }
                        return data;
                    });
            });
    };


    return {
        _testinit: _testinit,
        get: get,
        save: save
    };
});
