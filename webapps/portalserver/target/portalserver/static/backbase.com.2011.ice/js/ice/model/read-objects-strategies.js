/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2017 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : read-objects-strategies.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

be.utils.define('be.ice.model.readObjectsStrategies', ['$'], function ($) {
	'use strict';

    function getUniquePaths(configArr) {
        return configArr.reduce(function (resultArr, configObj) {
            configObj.paths.forEach(function (path) {
                if (resultArr.indexOf(path) === -1) {
                    resultArr.push(path);
                }
            });

            return resultArr;
        }, [])
    }

    var ReadImmediatelyStrategy = {},
        ReadByCommandStrategy;

    function setReadImmediatelyStrategy(strategy) {
        $.extend(ReadImmediatelyStrategy, strategy);
    }

    ReadByCommandStrategy = function IIFE() {
        var deferredReadObjects = [];

        function readObjects(paths) {
            var readObjectsDfd = $.Deferred();

            deferredReadObjects.push({
                paths: paths,
                readObjectsDfd: readObjectsDfd
            });

            return readObjectsDfd.promise();
        }

        function readNow() {
            var readObjectsPromise = ReadImmediatelyStrategy.readObjects(getUniquePaths(deferredReadObjects));

            deferredReadObjects.forEach(function (curConfigObj) {
                var resolve = curConfigObj.readObjectsDfd.resolve,
                    reject = curConfigObj.readObjectsDfd.reject;

                readObjectsPromise.then(resolve, reject);
            });

            deferredReadObjects = [];
        }

        return {
            readObjects: readObjects,
            readNow: readNow
        }
    } ();

    var api = {
        ReadImmediately: ReadImmediatelyStrategy,
        ReadByCommand: ReadByCommandStrategy
    };
    
    function getStrategies(readImmediatelyStrategyParam) {
        if (!ReadImmediatelyStrategy) {
            if (!readImmediatelyStrategyParam) {
                throw new Error("ReadImmediately strategy should be set first");
            } else {
                setReadImmediatelyStrategy(readImmediatelyStrategyParam);
            }
        }

        return api;
    }

    return {
        getStrategies: getStrategies,
        setReadImmediatelyStrategy: setReadImmediatelyStrategy
    }
});
