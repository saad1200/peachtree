/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : global-require.config.js
 *  Description: This is the requrejs config file used across all
 *  modules. The file is loaded in the 'BB_Dashboard_Web' template.
 *  It's used as a starting definition point for configure all the
 *  path, maps, shims and other requirejs configuratation.
 *  ----------------------------------------------------------------
 */

/* globals define, require, b$ */


if (window.jQuery) {
    define('jquery', function () {
        'use strict';
        return window.jQuery;
    });
}

require.config({

    // The number of seconds to wait before giving up on loading a script. Setting it to 0 disables the timeout. The default is 7 seconds. Setting this to 24 for support on slower systems.
    waitSeconds: 24,

    config: (function(b$) {
        'use strict';
        var url2State = null;
        if(typeof b$.view !== 'undefined') {
            url2State = b$.view.url2state.active = false;
        }
        return {
            portal : b$.portal,
            url2state   : url2State
        };
    })(b$ || {}),

    baseUrl: (function(portal) {
        'use strict';
        var root = portal.contextRoot || portal.config.serverRoot || '';
        return root +  '/static';
    })(window.bd && window.bd.portal || b$.portal || {} ),


    /*----------------------------------------------------------------*/
    /* Define global paths here
    /*----------------------------------------------------------------*/
    paths: {
        // Angular
        'angularPath'  : 'ext-lib/angular-1.2.10',
        'angular'      : 'ext-lib/angular-1.2.10/angular',
        'ng'           : 'ext-lib/angular-1.2.10/ng',

        // Other Libs
        'mustache'     : 'ext-lib/mustache-0.8.1/mustache',
        'jquery'       : 'ext-lib/jquery.min', // jquery external plugins
        'plugins'      : 'ext-lib/jquery',
        'underscore'   : 'ext-lib/underscore-1.6.0/underscore-min',
        'jsonpointer'  : 'ext-lib/jsonpointer/jsonpointer',
        'ui-bootstrap' : 'ext-lib/angular-bootstrap/ui-bootstrap-tpls-0.11.0.min',
        'conf'         : 'conf',
        'showdown'     : 'ext-lib/showdown-1.6.4/showdown.min',
        'xss'          : 'ext-lib/xss/xss.min'
    },

    /*----------------------------------------------------------------*/
    /* Non AMD modules
    /*----------------------------------------------------------------*/
    shim : {
        // For legacy support
        'jquery'       : { exports: 'jQuery'},
        'underscore'   : { exports: '_' },
        'angular'      : { exports: 'angular' },
        'ui-bootstrap' : { deps: ['angular'], exports: 'ui-bootstrap' },
        'xss'          : { exports: 'filterXSS' },

        //ngModules
        'ng/angular-validator'  : { deps: [ 'angular' ] },
        'ng/angular-focus'      : { deps: [ 'angular' ] },
        'ng/angular-hotkeys'    : { deps: [ 'angular' ] },

        'angularPath/angular-animate'       : { deps: [ 'angular' ] },
        'angularPath/angular-cookies'       : { deps: [ 'angular' ] },
        'angularPath/angular-loader'        : { deps: [ 'angular' ] },
        'angularPath/angular-mocks'         : { deps: [ 'angular' ] },
        'angularPath/angular-resource'      : { deps: [ 'angular' ] },
        'angularPath/angular-route'         : { deps: [ 'angular' ] },
        'angularPath/angular-sanitize'      : { deps: [ 'angular' ] },
        'angularPath/angular-scenario'      : { deps: [ 'angular' ] },
        'angularPath/angular-touch'         : { deps: [ 'angular' ] }

    },


    /*----------------------------------------------------------------*/
    /* Mapping to requirejs Plugins
    /*----------------------------------------------------------------*/
    map: {

        '*': {
            'css'    : 'ext-lib/requirejs/require-css',
            'i18n'   : 'ext-lib/requirejs/require-i18n',
            'text'   : 'ext-lib/requirejs/require-text',
            'tpl'    : 'ext-lib/requirejs/require-tpl',
            'jQuery' : 'jquery' //to be removed
        }
    }

});

/*----------------------------------------------------------------*/
/* Catch requirejs errors
/*----------------------------------------------------------------*/
// require.onError = function (err) {
//     'use strict';
//     console.log(err);
// };



/*----------------------------------------------------------------*/
/*  Global function to start widgets
/*----------------------------------------------------------------*/

(function (root, jQuery, definition) {
    'use strict';
    // export mechanism that works in node, browser and some other places.
    if (typeof define === 'function') {
        define('requireWidget', definition);
    } else if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        // node style commonJS.
        module.exports = definition();
    }
    // export to window
    root.requireWidget = definition();
})( this , $ , function (requireWidget) {

     // #TODO (dragosh) errorManagement
    // EX: throw new Error( msg(ERROR_MESSAGES.SAMPLE_MESSAGE, 'param1', 'param2') );

    'use strict';
   /*----------------------------------------------------------------*/
    /* Constants
    /*----------------------------------------------------------------*/
    var ERROR_MESSAGES = {
        SAMPLE_MESSAGE: 'Sample error for {0} and {1} to throw.'
    };
    /*----------------------------------------------------------------*/
    /* Vars
    /*----------------------------------------------------------------*/
    var prototypeOfArray    = Array.prototype;
    var prototypeOfObject   = Object.prototype;
    var prototypeOfFunction = Function.prototype;
    var __slice             = prototypeOfArray.slice;
    var __call              = prototypeOfFunction.call;

    /*----------------------------------------------------------------*/
    /* Helpers
    /*----------------------------------------------------------------*/
    var _toString           = __call.bind(prototypeOfObject.toString);
    var _slice              = __call.bind(__slice);

    /*----------------------------------------------------------------*/
    /* Utils use jQuery
    /*----------------------------------------------------------------*/
    var utils = utils || {};
    utils = {
        extend: $.extend,
        isFunction: $.isFunction,
        isPlainObject: $.isPlainObject,
        isAngular: function(app) {
            // #todo make a better check on angular apps
            return $.isPlainObject(app) && app._invokeQueue;
        }
    };

    /*----------------------------------------------------------------*/
    /* Public API
    /*----------------------------------------------------------------*/
    function loadWidget() {

        // fails in ie8
        // var args = _slice(arguments, 0);
        var args = Array.prototype.slice.call(arguments);

        var widget = {
            instance: args[0],
            config: utils.extend(true, {}, requirejs.s.contexts._.config),
            app: args[1]
        };

        var fn = require.config(widget.config);
            fn([widget.app], function(app) {
                var wi = widget.instance;
                // throws an error in Safari on 'use strict' mode
                try {
                    wi.name = app.name || '';
                } catch(e) {}

                if( utils.isFunction(app) ) {
                    app.call(null, wi );
                } else if(utils.isAngular(app)) {
                    //is angular
                    app.constant('Widget', wi);
                    angular.bootstrap( wi.body, [ app.name ]);

                } else if( utils.isPlainObject(app) ) {
                    // Call if you find an init function
                    if(utils.isFunction(app.init)) {
                        app.init.call(null, ( typeof wi === 'string' ? $(wi) : wi ) );
                    }

                }
            });

    }

    /*----------------------------------------------------------------*/
    /* Private
    /*----------------------------------------------------------------*/
    /**
    * Uncoment if used as error
    */
    // function msg(str) {
    //     if (str === null) { return null; }
    //     for (var i = 1, len = arguments.length; i < len; ++i) {
    //         str = str.replace('{' + (i - 1) + '}', String(arguments[i]));
    //     }
    //     return str;
    // }

    /*----------------------------------------------------------------*/
    /* Exporting
    /*----------------------------------------------------------------*/
    var exports = {
        requireWidget: loadWidget
    };

    return exports.requireWidget;
});
