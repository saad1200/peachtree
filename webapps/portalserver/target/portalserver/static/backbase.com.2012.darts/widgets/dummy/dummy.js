/**
 * Copyright � 2011 Backbase B.V.
 */

if (!window.bd) window.bd = {};

be.dummy = (function() {
	var onload = function(oGadget) { 
	};
	
	//public functions
	return {
		onload: onload
		};

}());