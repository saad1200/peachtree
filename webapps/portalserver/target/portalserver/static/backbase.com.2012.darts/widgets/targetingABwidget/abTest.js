/**
 * Copyright � 2011 Backbase B.V.
 */

if (!window.bd) window.bd = {};

be.abTest = (function() {
	var onload = function(oGadget) { 
	};
	var swapImages = function(imageName, parentContainer) {
		$('.bd-abImage', parentContainer).hide();
		var imageName = $(imageName, parentContainer);
    	$(imageName, parentContainer).show();
    }; 
	
	//public functions
	return {
		onload: onload,
		swapImages: swapImages
		};

}());