/**
 * Copyright � 2011 Backbase B.V.
 */

if (!window.bd) window.bd = {};

be.targetingContentViewerWidget = (function() {
	var contentRefType = 'contentRef';
	var $browserParentContainer = null;
	var $browserContainer = null;
	var $draggedContainer = null;
	var draggedContainerData = '';
	var insertTarget = null;
	var inserted = false;
	var currentTarget = null;

	var oGadgetArray = {};
	var oGadgetIndex = 0;

	var reInit = true;
	var opened = false;
	var onload = function(oGadget) {
			if (bd.designMode == 'true') {
				init(oGadget);
			}

			var contentPath = oGadget.model.getPreference('contentPath');
			if(contentPath) {
				params = {
						path : contentPath,
						errorCallback: function(){
							$('.bd-imageContent-dropArea', oGadget.body).addClass("bd-imageError");
//							$('img', oGadget.body).attr({
//								src: bd.contextRoot+ '/static/dashboard/media/blank.gif',
//							});
							oGadget.imageNotFound = imageNotFound;
						}
				};
				be.cmis.getMetaData(params, function(metaData){
					   $('img', oGadget.body).attr({
						   "src": metaData['url'].property,
						   "data-imageName": metaData['cmis:name'].property
						});
					   $('img', oGadget.body).attr(be.commonContentUtils.returnMetaData(metaData, null));
				});
			} else {
				if (bd.designMode == 'true') {
					//var contentDropArea = $('.bd-imageContent-dropArea', oGadget.body);
					//bd.addDropArea(contentDropArea);
//					$(contentDropArea).children('img').attr("src", bd.contextRoot+ '/static/dashboard/media/blank.gif');
				}
			}
	};

	var swapImages = function(imageName, parentContainer) {
		$('.bd-imageContent-dropArea', parentContainer).hide();
		var imageName = $(imageName, parentContainer);
    	$(imageName, parentContainer).show();
    };

	var imageNotFound = function(oGadget){
		oGadget.model.setPreference('contentPath', "");
	};

	var saveContent = function(oGadget){
    	if(oGadget.model.getPreference('contentPath') == null){
    		oGadget.model.createPreference("contentPath", "", contentRefType, "", "");
    	}
		oGadget.model.setPreference('contentPath', oGadget.imgParam.file.path);
		oGadget.model.save();
	};

	var init = function(oGadget) {
		var $oGadgetBody = $(oGadget.body);
        var contentDropArea = $('.bd-imageContent-dropArea', oGadget.body).addClass('bd-image');

		oGadget.ToolbarFunc = oGadget.ToolbarFunc || {};
		oGadget.hasToolBar = true;
		oGadget.saveContent = saveContent;
		oGadget.ToolbarFunc.insertImage = function(file,oGadget){
			var img = $(contentDropArea).children('img');

			img.attr({
				"src": file.url,
				"data-imageName": file.name
				});
			img.attr(be.commonContentUtils.returnMetaData(file.metaData, null));
			img.attr('contentSrc', file.contentUId);
			if($(contentDropArea).hasClass('bd-imageError')){
				$(contentDropArea).removeClass('bd-imageError');
				imageNotFound(oGadget);
			}

	    	if(oGadget.model.getPreference('contentPath') == null){
	    		oGadget.model.createPreference("contentPath", "", contentRefType, "", "");
	    	}
	    	oGadget.model.setPreference('contentPath', file.path);
	    	oGadget.model.save();
            bd.removeDropArea(contentDropArea);
		};

		oGadget.ToolbarFunc.deleteImage = function(image,oGadget,nicToolbar){
			var $imageTag = $(image);
			oGadget.model.setPreference('contentPath', "");
			bd.addDropArea($('.bd-imageContent-dropArea', oGadget.body));
//			$imageTag.attr("src", bd.contextRoot+ '/static/dashboard/media/blank.gif');
//			$imageTag.attr({
//				"data-imageName" : 'blank.gif',
//				"contentsrc": '',
//				"alt": ''
//			});
			oGadget.model.save();
			$(nicToolbar.pane.pane).children().hide();
			nicToolbar.addPane();
		};

        if (bd.designMode == "true") {
        	   //FIXME: remove this when targeting content viewer is switched to CKEditor
            	bd.nicEditToolbar.init();
            	var $editArea = $('.bd-imageContent-dropArea', $oGadgetBody);
            	//$editArea.attr('defaultToolbarTab',bd.uiEditingOptions.imageViewer.defaultToolbarTab);
            	$editArea.attr('defaultToolbarTab','ImageLink');
                // removing widget ID from editorList before initialization
            	//FIXME: remove this when targeting content viewer is switched to CKEditor
            	bd.nicEditToolbar.removeContent(oGadget.model.name);
            	//FIXME: remove this when targeting content viewer is switched to CKEditor
            	bd.nicEditToolbar.initToolBar({ oGadget : oGadget, $editArea : $editArea, noContentEditable : true});

        }
        $oGadgetBody.bind('dragstart drop', function(event){
    	    event.preventDefault();
    	    return false;
    	});

        bd.bindDropEvent(oGadget);
		//bd.removeDropArea(oGadget);


	};

	//public functions
	return {
		onload: onload,
		swapImages: swapImages
	};

}());
