/*!
 *	----------------------------------------------------------------
 *	Copyright Backbase b.v. 2003/2012
 *	All rights reserved.
 *	----------------------------------------------------------------
 *	Version 5.5
 *	Author : Backbase R&D - Amsterdam - New York
 *	----------------------------------------------------------------
 */
/* globals b$ */

b$.module('b$.portal.view.bdom.container.targetContainer', function() {
    'use strict';

    var NS = b$.bdom.getNamespace('http://backbase.com/2012/portalView'),
        Container = NS.getClass('container');


    NS.registerElement('TCont', Container.extend(function (bdomDocument, namespaceURI, localName, node) {
        Container.call(this, bdomDocument, namespaceURI, localName, node);
        this.isPossibleDragTarget = false;
    }, {
        buildHTML: function (elm) {
            this.htmlAreas = jQuery('.bp-area', elm);
			this.model.addEventListener.call(this, 'DOMNodeRemoved', this.DOMNodeRemoved);
            return elm;
        },

        DOMNodeRemoved : function(ev){
            if(this.DOMNodeRemovedHandler){
                this.DOMNodeRemovedHandler.call(this, ev);
            }
        },

		readyHTML : function () {
            // console.log('-- readyHTML --');

            var This = this, rs,
                contextRoot = window.bd && window.bd.contextRoot,
                isDesign = window.bd && window.bd.designMode || window.top && window.top.bd && window.top.bd.designMode,
                isDev = isDesign && window.top && window.top.bd && window.top.bd.devMode;

            if(isDesign){
                rs = new b$._private.ResourceSequence();

                if (!window.require) {
                    rs.addURI('text/javascript', contextRoot + '/static/ext-lib/requirejs/require.min.js');
                }
                rs.addURI('text/javascript', contextRoot + '/static/backbase.com.2012.darts/js/require.config.js');

                if (isDev) {
                    //console.log('devMode');
                    rs.addURI('text/javascript', contextRoot + '/static/backbase.com.2012.darts/js/targetingDesignMode.js');
                    rs.addURI('text/javascript', contextRoot + '/static/backbase.com.2012.darts/js/targetingPreferences.js');
                    rs.addURI('text/javascript', contextRoot + '/static/backbase.com.2012.darts/js/lib/jquery.chosen.js');
                    rs.addURI('text/javascript', contextRoot + '/static/backbase.com.2012.darts/js/lib/jquery.sortable.js');
                    rs.addURI('text/css', contextRoot + '/static/backbase.com.2012.darts/css/chosen/chosen.css');
                    rs.addURI('text/css', contextRoot + '/static/backbase.com.2012.darts/css/targeting.css');
                } else {
                    rs.addURI('text/javascript', contextRoot + '/static/backbase.com.2012.darts/js/darts.designMode-lib.min.js');
                    rs.addURI('text/css', contextRoot + '/static/backbase.com.2012.darts/css/darts.designMode.min.css');
                }

                rs.startLoading( function() {
                    This.initDesignMode();
                });
            }
        }

    }));
});


