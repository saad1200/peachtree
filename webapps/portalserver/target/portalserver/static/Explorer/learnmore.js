/**
 * Load learnmore script which is used for setting url on
 * documentation links using properties defined in uiEditingOptions.
 */
(function() {
    'use strict';
    require(['backbase.com.2014.components/scripts/learnmore']);
}());