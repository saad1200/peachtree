/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : view-mustache.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */


/* global b$, jQuery */

//	----------------------------------------------------------------
(function ($) {
//	----------------------------------------------------------------

	'use strict';

	var Observable = b$.require('b$.mvc.Observable');
	var be = be || top.be;
	var bd = bd || top.bd;
	var contextRoot = be.contextRoot + '/static/Explorer/';
	// var globalIcon = {

	// 			'portal'   : 'bc-portal',
	// 			'page'     : 'bc-page',
	// 			'container': 'bc-container',
	// 			'widget'   : 'bc-widget-explorer',
	// 			'link'     : 'bc-link',
	// 			'template' : 'bc-template',
	// 			'catalog'  : 'bc-catalog',

	// 		};

//	----------------------------------------------------------------
	var viewMustache = Observable.extend(function (){
//	----------------------------------------------------------------
		Observable.call(this);
		this.tmpl_urls = {};
        this.cued = 0;
	}, {
		register: function(name, url) {
			var d = $.Deferred();
			this.tmpl_urls[name] = url;

			// Cache templates
			be.utils.processHTMLTemplateByUrl(url, {}, function (htmlContent) {
				d.resolve();
			});

			return d.promise();
		},


		apply: function(name, xml, trg) {
			var dataJson = bd.xmlToJson({xml: xml});

			return this.applyJSON(name, dataJson, trg);
		},


		applyJSON: function(name, dataJson, trg) {

			var url = this.tmpl_urls[name],
				isContentRepoItem = false,
				itemToArray = function(data){
					if(data && data.portals && !$.isArray(data.portals.portal)){
						data.portals.portal = [data.portals.portal];
					}

					if(data && data.contentRepositories) {
						isContentRepoItem = true;
					}
				},
				getTemplate = function(data){
					return be.utils.processHTMLTemplateByUrl(url, data);
				},

				getItems = function(obj, itemList){
					var items = [];
					itemList = itemList || 'portal|widget|page|container|template|link|contenttype|feature';

					if(isContentRepoItem) {
						obj.item_nodeName = 'contentRepository';
						items.push(obj);
					} else {
						$.each(obj || [], function(i, el){
							if(itemList.indexOf( i, itemList ) > -1){

								if($.isArray(el)){
									$.each(this, function(j, item){
										item.item_nodeName = (i === 'feature') ? item.item_nodeName = 'feature' : item.item_nodeName = i;
									});
									items = items.concat(el);
								} else {
									el.item_nodeName = (i === 'feature') ? el.item_nodeName = 'feature' : el.item_nodeName = i;
									items.push(el);
								}
							}
						});
					}

					return items;
				},

				contextFn = function(){
					var obj = {}, htmlContent = '',
						context = this;

					return function(text, render){
						obj = parseContext(context, text);
						htmlContent = getTemplate(obj);
						return htmlContent;
					};
				},

				childrenFn = function(){
					var htmlContent = '',
						items, obj = {},
						children = this && this.children;

					return function(text, render){
						if(children){
							if(text === 'childrenArray'){
								items = getItems(children);
								obj.childrenArray = items;
							} else {
								obj = children;
							}
							extendObj(obj);
							htmlContent = getTemplate(obj);
						}
						return htmlContent;
					};
				},

				itemFn = function(){

					// there is unnecessary children item in json coming
					// from xml file - remove it (as contentRepository has no children)
					if(isContentRepoItem) {
						delete this.children;
					}

					var htmlContent = '',
						items, obj = {},
						children = this && this.children;

					return function(text, render){

						items = getItems(children || this, text);

						if(items && items.length && !isContentRepoItem){
							obj.itemsArray = items;
							extendObj(obj);
							htmlContent = getTemplate(obj);
						} else {
							if(items.length && items[0].totalSize !== '0') {
								obj.contentRepositoryList = items;
								extendObj(obj);
								htmlContent = getTemplate(obj);
							}
						}
						return htmlContent;
					};
				},

				extendObj = function(obj){
					obj = obj || {};
					obj.contextRoot = contextRoot;
					obj.itemFn = itemFn;
					obj.contextFn = contextFn;
					obj.childrenFn = childrenFn;
					return obj;
				},


				/**
				* Simple filtering on js object
				* @param context: json object
				* @param text: value passed to context function in mustache template
				*        examples: 'template[type="PAGE"]', 'template|container'
				* @returns filtered context object
				*/
				parseContext = function(context, text){
					var trim = be.utils.trimString,
						filteredObject,
						filterIndex, isFilter, nodeName, filterStr,
						valueIndex, isValue, filterName, filterValue;

					function filter (el){
						el.context_nodeName = nodeName;
						if(isFilter){
							if(filterValue && filterName){
								return el[filterName] === filterValue;
							} else if (filterName) {
								return el[filterName];
							} else if (filterValue) {
								return el === filterValue;
							}
						} else {
							return true;
						}
					}

					text = trim(text);

					if(text) {
						var nodes = text.split('|');
						for(var i = 0, len = nodes.length; i < len; i++){
							filterIndex = text.indexOf('[');
							isFilter = filterIndex > -1;
							nodeName = trim( isFilter ? text.substring(0, filterIndex) : text);

							if(isFilter){
								filterStr = trim(text.substring(filterIndex + 1, text.length - 1));
								valueIndex = filterStr.indexOf('=');
								isValue = valueIndex > -1;
								filterName = trim(isValue ? filterStr.substring(0, valueIndex) : filterStr);
								filterValue = isValue && trim(filterStr.substring(valueIndex + 1));
							}

							if (nodeName) {
								filteredObject = filteredObject || {};


								filteredObject[nodeName] = $.grep(context[nodeName], filter);
								extendObj(filteredObject);
							}
						}
					}
					return filteredObject || context;
				};

			itemToArray(dataJson);
			extendObj(dataJson);

			if (url) {
				var content = getTemplate(dataJson);
				if (trg) {
					trg.innerHTML = content;
				}
				return content;
			}
			return '';
		}
	});



	window.View_mustache = viewMustache;



}(jQuery));
