/**
	Copyright Backbase R&D 2011
	--------------------------------
	Backbase Portal 5
	view-withxslt.js
	version 1.00
	@author : G. Kaandorp
**/

//	----------------------------------------------------------------
(function () {
//	----------------------------------------------------------------
	var Observable = b$.require('b$.mvc.Observable');

//	----------------------------------------------------------------
	var view_xslt = Observable.extend(function (){
//	----------------------------------------------------------------
		Observable.call(this);
		this.xslts = {};
		this.xslts_xml = {};
        this.cued = 0;
	},{
		register : function(name, url) {
			var This = this;
            this.cued += 1;
			jQueryRestAjax(url,null,'GET', function (xml) {
				var oProcessor = new XSLTProcessor();
				oProcessor.importStylesheet(xml);
				This.xslts[name] = oProcessor;
				This.xslts_xml[name] = xml;
                This.cued -= 1;
                if(This.cued == 0){
                    var bd = bd || top.bd;
                    bd.observer.notifyObserver("view_xslt.done");
                }
			})
		},
		apply : function(name, xml, trg) {
			if (this.xslts[name]) {
				if (!xml.nodeName) {	// detect string or xml doc
					var oDOMParser = new DOMParser();
					xml = oDOMParser.parseFromString(xml,'text/xml');
				}
				var str;
				if (window.ActiveXObject) { 
					str=xml.transformNode( this.xslts_xml[name]); 
				}
				else {
					var doc = (trg) ? trg.ownerDocument : document;
					var result = this.xslts[name].transformToDocument(xml, doc);
					
					var serializer = new XMLSerializer();
					var str = serializer.serializeToString(result);
				}
				if (trg) {
					trg.innerHTML = str;
				}
				return str;
			}
			return '';
		}
	});


//	----------------------------------------------------------------
	function jQueryRestAjax(url, data, method, callback, errcallback) {
//	----------------------------------------------------------------
		var be = be || top.be;
    //    url = be.contextRoot + "/static/Explorer/" + url;
        return be.utils.ajax({
	//	return $.ajax({
			type: method,
			url: url,
			cache: false,
			data: data,
			dataType: "xml",
			contentType: 'application/xml',
			success: callback,
			error: errcallback
		});
	}


	this.View_xslt = view_xslt;
	
	
	


	/**
	 * XSLTProcessor is a global JavaScript object. 
	 * It provides the means to transform XML documents with XSLT. 
	 * It uses the browser's XSLT 1.0 implementation.
	 */
	var XSLTProcessor = window.XSLTProcessor;
	
	// force IE to use ActiveXObject XSLTProcessor
	if (!XSLTProcessor || $.browser.msie) {
		XSLTProcessor = function(){
			this._template = new ActiveXObject('MSXML2.XSLTemplate');
			this._outputMethod = "xml";
			this._parameters = {};
		};

		/**
		 * Imports the stylesheet into this XSLTProcessor for transformations.
		 *
		 * @param {DOMNode} oXsltDocument 
		 *     The root-node of an XSLT stylesheet.
		 */
		XSLTProcessor.prototype.importStylesheet = function(oXsltDocument){
			// convert stylesheet to free threaded
			var oConverted = new ActiveXObject('MSXML2.FreeThreadedDOMDocument');
			oConverted.async = false;

			// make included/imported stylesheets work if exist and xsl was originally loaded from url and is documentElement
			if (oXsltDocument.nodeType == oXsltDocument.DOCUMENT_NODE && oXsltDocument.url) {
				oXsltDocument.setProperty('SelectionLanguage', 'XPath');
				oXsltDocument.setProperty('SelectionNamespaces', 'xmlns:xsl="http://www.w3.org/1999/XSL/Transform"');

				if (oXsltDocument.selectSingleNode('//xsl:*[local-name() = \'include\' or local-name() = \'import\']') !== null) {
					oConverted.load(oXsltDocument.url);
				} else {
					oConverted.loadXML(oXsltDocument.xml);
				}
			} else {
				oConverted.loadXML(oXsltDocument.xml);
			}
			
			oConverted.setProperty('SelectionNamespaces', 'xmlns:xsl="http://www.w3.org/1999/XSL/Transform"');

			var oOutput = oConverted.selectSingleNode('//xsl:output');
			this._outputMethod = oOutput ? oOutput.getAttribute('method') : 'html';

			this._template.stylesheet = oConverted;

			// (re)set default param values (FIXME: should it be done?)
			this.clearParameters();
		};
		
		/**
		 * Transforms the source node to string.
		 *
		 * @param {DOMNode} oNode 
		 *     The DOM node to transform.
		 * @return {string}
		 *     Transformation result as a string.
		 */
		XSLTProcessor.prototype._transformToString = function(oNode) {
			var oProcessor = this._template.createProcessor();
			
			for (var sNsURI in this._parameters) {
				for (var sName in this._parameters[sNsURI]) {
					if (sNsURI) {
						oProcessor.addParameter(sName, this._parameters[sNsURI][sName], sNsURI);
					} else {
						oProcessor.addParameter(sName, this._parameters[sNsURI][sName]);
					}
				}
			}
			
			oProcessor.input = oNode;
			oProcessor.output = null;
			oProcessor.transform();

			return oProcessor.output;
		};
		 

		/**
		 * Transforms the given DOM node and return the transformation result as a new DOM document.
		 *
		 * @param {DOMNode} oNode 
		 *     The DOM node to transform.
		 * @return {DOMDocument}
		 *     The transformation result as the DOM document.
		 */
		XSLTProcessor.prototype.transformToDocument = function(oNode){
			var sResult = this._transformToString(oNode),
				sOutputMethod = this._outputMethod,
				oIFrame = null;
			
			if (sOutputMethod == 'text') {
				// FIXME: specify the exact element name consistent across browsers
				return new DOMParser().parseFromString("<transformation>" + sResult + "</transformation>", "text/xml");
			} else if (sOutputMethod == 'xhtml' || sOutputMethod == 'html') {
				// FIXME: the research can be continued with the cloneNode
				throw new Error("Feature is not supported!");
			} else if (sOutputMethod == 'xml') {
				return new DOMParser().parseFromString(sResult, "text/xml");
			}
		 };

		/**
		 * Transforms the given DOM node and return the transformation result as a new DOM fragment.
		 * Note: The xsl:output method must match the nature of the owner document (XML/HTML).
		 *
		 * @param {DOMNode} oNode 
		 *     The DOM node to transform.
		 * @param {DOMDocument} oOwnerDocument 
		 *     The owner of the result fragment.
		 * @return {DOMDocumentFragment} 
		 *     The transformation result as DOM fragment.
		 */
		XSLTProcessor.prototype.transformToFragment = function(oNode, oOwnerDocument) {
			var sResult = this._transformToString(oNode),
				sOutputMethod = this._outputMethod,
				oResult = oOwnerDocument.createDocumentFragment(),
				oElement = null;

			if (sOutputMethod == "text") {
				oResult.appendChild(oOwnerDocument.createTextNode(sResult));
			} else if (sOutputMethod == "xhtml" || sOutputMethod == "html" || sOutputMethod == "xml") {
				if (sOutputMethod == "xml") {
					oElement = new DOMParser().parseFromString(
						'<root>' + sResult.replace(/\w*<\?xml[^\?]*\?>/g, '') + '</root>', "text/xml"
					).documentElement;
				} else {
					oElement = createElementFromString("<div>" + sResult + "</div>");
				}
				
				while (oElement.firstChild) {
					oResult.appendChild(oElement.firstChild);
				}
			}
			
			return oResult;
		};

		/**
		 * Sets a parameter to be used in subsequent transformation.
		 *
		 * @param {string} sNsURI 
		 *     The namespace URI of parameter to set.
		 * @param {string} sName 
		 *     The local name of parameter to set.
		 * @param {variant} vValue 
		 *     The new parameter value.
		 */
		XSLTProcessor.prototype.setParameter = function(sNsURI, sName, vValue) {
			sNsURI = sNsURI || "";
			
			// update updated params for getParameter
			if (!this._parameters[sNsURI]) {
				this._parameters[sNsURI] = {};
			}
			
			this._parameters[sNsURI][sName] = vValue;
		};

		/**
		 * Gets a parameter if previously set by setParameter; otherwise, returns null.
		 *
		 * @param {string} sNsURI 
		 *     The namespace URI of parameter to get.
		 * @param {string} sName 
		 *     The local name of parameter to get.
		 * @return {string}
		 *     The parameter value, if previously set by setParameter; otherwise, null.
		 */
		XSLTProcessor.prototype.getParameter = function(sNsURI, sName) {
			var oNSHash = this._parameters[sNsURI || ""];
			
			return oNSHash && oNSHash[sName] || null;
		};

		/**
		 * Removes a parameter, if set.
		 *
		 * @param {string} sNsURI 
		 *     The namespace URI of parameter to remove.
		 * @param {string} sName 
		 *     The local name of parameter to remove.
		 */
		XSLTProcessor.prototype.removeParameter = function(sNsURI, sName) {
			var oNSHash = this._parameters[sNsURI || ""];
			
			if (oNSHash && oNSHash.hasOwnProperty(sName)) {
				delete oNSHash[sName];
			}
		};

		/**
		 * Removes all set parameters.
		 */
		XSLTProcessor.prototype.clearParameters = function() {
			this._parameters = {};
		};

		/**
		 * Removes all parameters and stylesheets.
		 */
		XSLTProcessor.prototype.reset = function(){
			this.clearParameters();
			
			this._template = new ActiveXObject('MSXML2.XSLTemplate');
		};
	}
	

	
	

}());
