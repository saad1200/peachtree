<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ns2="http://ns.backbase.com/"
	version="2.0">

	<xsl:output method="html" omit-xml-declaration="yes" indent="no" cdata-section-elements="" encoding="UTF-8"/>


	<xsl:template match="/*">
		<div>
			<xsl:apply-templates select="portal | page | container | widget | template"  mode="inner" />
		</div>
	</xsl:template>

	<xsl:template match="/portal | /page | /container | /widget | /template">
		<div>
			<xsl:apply-templates select="." mode="inner" />
		</div>
	</xsl:template>


	<xsl:template match="children" mode="kids">
		<table class="ex-detail-table">
			<tbody>
			<xsl:apply-templates select="portal | page | container | widget | template"  mode="inner2" />
			</tbody>
		</table>
	</xsl:template>


	<xsl:template match="portal | page | container | widget | template" mode="inner">
			<xsl:apply-templates select="children"  mode="kids" />
	</xsl:template>



	<xsl:template match="portal | page | container | widget | template" mode="inner2">
		<tr class="ex-detail-tr">
			<xsl:attribute name="contextItemName">
				<xsl:value-of select="contextItemName/text()"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="name/text()"/>
			</xsl:attribute>
			<xsl:attribute name="tag">
				<xsl:value-of select="name()"/>
			</xsl:attribute>
			<td class="ex-detail-td">
				<div class="bc-tree-div">
					<a class="bc-tree-a">
						<xsl:value-of select="name"/>
					</a>
				</div>
			</td>
			<td class="ex-detail-td">
				<xsl:value-of select="name()"/>
			</td>
		</tr>
	</xsl:template>



</xsl:stylesheet>