/**
 Copyright Backbase R&D 2011
 --------------------------------

 Portal Data Model Explorer
 version 1.05

 **/


//  ----------------------------------------------------------------
(function ($) {
//  ----------------------------------------------------------------
    //'use strict';


    var be = window.be || top.be,
        bd = window.bd || top.bd,
        PAGE_SIZE = 20,
        exporerDV,
        view = new View_mustache(),
        contextRoot = be.contextRoot + "/static/Explorer/",
        TREE_VIEW_NAME = 'tree',

        d1 = $.Deferred(),
        r1 = view.register(TREE_VIEW_NAME, contextRoot + 'templates/view_Tree.html'),
        r2 = view.register('list', contextRoot + 'templates/view_List.html'),
        r3 = view.register('detail', contextRoot + 'templates/view_Detail.html'),
        r4 = view.register('props', contextRoot + 'templates/view_Properties.html'),
        notify = function(message, isError, options) {      // Change the modal type if we receive an error to show the OK button to wait for the user.
            var modalType = 'notify',
                defaultOptions = {
                    uid: '1231',
                    icon: 'checkbox',
                    message: message
                };

            options = options || {};
            $.extend(options, defaultOptions);

            if (isError) {
                modalType = 'dialog';
                options.icon = 'error';
                options.title = options.title || 'Sorry';
                options.html = message;
                options.text = message;
                options.buttons = [
                    {
                        title: 'OK',
                        type: 'white'
                    }
                ];
            }

            bc.component[modalType](options);
        };

    // Overwrite map: instanceof does not work cross frame
    Array.prototype.map = top.Array.prototype.map;

    $.when(r1, r2, r3, r4, d1.promise()).done(function() {
        exporerDV.readyDOM();
    });
    var portalServerRoot = (window.contextRoot || bd && bd.contextRoot) + '/';
    var portalServer = new PortalServer_rawjsonapi(portalServerRoot);

    var Observable = b$.require('b$.mvc.Observable');
    var HashMap = b$.require('b$.mvc.HashMap');

    var bufferDIV = document.createElement('div');

    var selectedItemId,
        selectedListItemId,
        isDetailsRefresh = false;


    var domBuilder = function (s) {
        return $(s)[0];

        bufferDIV.innerHTML = s;
        var elm = bufferDIV.firstChild;
        bufferDIV.innerHTML = '';
        return elm;
    };


    // Utils

    var getItemRefFromHTML = function (elm) {
        return {
            contextItemName : elm.getAttribute('contextitemname'),
            tag : elm.getAttribute('tag'),
            name : elm.getAttribute('name'),
            parentItemName : elm.getAttribute('parentitemname'),
            uuid: elm.getAttribute('data-uuid')
        };
    };

    var setItemRefToHTML = function (item, elm) {
        elm.setAttribute('contextitemname', item.contextItemName);
        elm.setAttribute('tag', item.tag);
        elm.setAttribute('name', item.name);
        elm.setAttribute('parentitemname', item.parentItemName);
    };



    //  ----------------------------------------------------------------
    //      South-Link_icons
    //  ----------------------------------------------------------------

    var globalIcon = {
        portal: 'bi-pm-house',
        page: 'bi-pm-page',
        container: 'bi-pm-layout',
        widget: 'bi-pm-diamond',
        link: 'bi-pm-link',
        feature: 'bi-pm-sharedfeatures',
        template: 'bi-pm-stamp',
        templatepage: 'bi-pm-page-stamp',
        templatewidget: 'bi-pm-diamond-stamp',
        templatecontainer: 'bi-pm-layout-stamp',
        catalog: 'bc-catalog',
        contentRepository: 'bi-pm-database',
        contenttype: 'bi-pm-structuredcontent-type'
    };

    function getIcon (json) {
        var iconHtml = '';
        var iconClass;
        if (json){
            iconClass = globalIcon[json.tag + (json.type ? json.type.toLowerCase() : '')] || globalIcon[json.tag];
            iconHtml = '<i class="bi ' + iconClass +'"></i>';
        }
        return iconHtml;
    }




//  ----------------------------------------------------------------
    var DomView = Observable.extend(function (app, parent){
//  ----------------------------------------------------------------
        Observable.call(this);
        this.dom = null;
    }, {
        buildDOM: function () {
        },
        getAttachableDOM: function () {
            if ( !this.dom ) {
                this.buildDOM();
            }
            return this.dom;
        },
        readyDOM: function () {
        }
    });


//  ----------------------------------------------------------------
    var ExplorerDomView = DomView.extend(function (app, parent){
//  ----------------------------------------------------------------
        DomView.call(this);
        this.app = app;
        this.parent = parent;

        this.addObserver('itemUpdated', this.itemUpdatedListener, this);
        this.addObserver('reloadItemsTree', this.reloadItemsTree);

    }, {

        buildDOM: function () {

            var s = '<div style="width:100%;height:100%">' +
                '<div class="ui-layout-north"></div>' +
                '<div class="ui-layout-outer-west"></div>' +
                '<div class="ui-layout-outer-center">' +
                '   <div class="ui-layout-north"></div>' +
                '   <div class="ui-layout-inner-center"></div>' +
                '</div>' +
                '<div class="ui-layout-south"></div>' +
                '<div name="popups">' +
                '<div name="popup1" class="bd-dropdown bd-roundcorner-3 ex-dropdown-popup" style="display: none;">'+
                    '<a onclick="return false" class="ex-act" data-for-mode="link" style="display: none" action="createRootLink" href="#">Create Links Header</a>'+
                    '<a onclick="return false" class="ex-act" action="delete" href="#">Delete</a>'+
                '</div>' +
                '<div name="popup2" class="bd-dropdown bd-roundcorner-3 ex-dropdown-popup" style="display: none;">'+
                '<div class="portal-explorer-dropdown-header">Add New Item</div>'+
                '<a onclick="return false" class="ex-act" action="createPortal" href="#"><span class="bi bi-pm-house"></span> Portal</a>'+
                '<a onclick="return false" class="ex-act" action="createPage" href="#"><span class="bi bi-pm-page"></span> Page</a>'+
                '<a onclick="return false" class="ex-act" action="createContainer" href="#"><span class="bi bi-pm-layout"></span> Container</a>'+
                '<a onclick="return false" class="ex-act" action="createWidget" href="#"><span class="bi bi-pm-diamond"></span> Widget</a>'+
                '<a onclick="return false" class="ex-act" action="createLink" href="#"><span class="bi bi-pm-link"></span> Link</a>'+
                '<a onclick="return false" class="ex-act" action="createPageTemplate" href="#"><span class="bi bi-pm-page-stamp"></span> Page Template</a>'+
                '<a onclick="return false" class="ex-act" action="createContainerTemplate" href="#"><span class="bi bi-pm-layout-stamp"></span> Container Template</a>'+
                '<a onclick="return false" class="ex-act" action="createWidgetTemplate" href="#"><span class="bi bi-pm-diamond-stamp"></span> Widget Template</a>'+
                '</div>' +
                '</div>' +
                '</div>';
            this.dom = domBuilder(s);

        },

        readyDOM: function () {
            var This = this;


            this.outerLayout = $(this.dom).layout({
                center__paneSelector:   ".ui-layout-outer-center",
                west__paneSelector:     ".ui-layout-outer-west",
                east__paneSelector:     ".ui-layout-outer-east",
                west__size:             300,
                east__size:             300,
                spacing_open:           4, // ALL panes
                spacing_closed:         4, // ALL panes
                north__size:            0,
                south__size:            400,
                north__resizable:       false,
                north__closable:        false,
                south__initClosed:      true,
                east__initClosed:       true,
            });

            this.innerLayout = $('.ui-layout-outer-center', this.dom).layout({
                center__paneSelector:   ".ui-layout-inner-center",
                west__paneSelector:     ".ui-layout-inner-west",
                east__paneSelector:     ".ui-layout-inner-east",
                north__size:            300,
                west__size:             75,
                east__size:             0,
                spacing_open:           4,// ALL panes
                spacing_closed:         4,// ALL panes
            });


            var panelOutWest = new EPDV_Tree(this, this);
            this.outerLayout.panes.west[0].appendChild(panelOutWest.getAttachableDOM());
            panelOutWest.readyDOM();

            var panelNorth = new EPDV_List(this,this);
            this.innerLayout.panes.north[0].appendChild(panelNorth.getAttachableDOM());
            panelNorth.readyDOM();

            var panelCenter = new EPDV_Detail(this,this);
            this.innerLayout.panes.center[0].appendChild(panelCenter.getAttachableDOM());
            panelCenter.readyDOM();

            var panelConsole = new EPDV_Console(this,this);
            this.outerLayout.panes.south[0].appendChild(panelConsole.getAttachableDOM());
            panelCenter.readyDOM();

            panelNorth.temporaryDetailLink = panelCenter;

            this.panelOutWest = panelOutWest;
            this.panelNorth = panelNorth;
            this.panelCenter = panelCenter;




            //  ----------------------------------------------------------------
            //  Portal Dialogs
            //  ----------------------------------------------------------------

            this.dialogs = [];
            this.dialogs.popup1 = $('[name="popup1"]', this.dom)[0];
            this.dialogs.popup2 = $('[name="popup2"]', this.dom)[0];

            $(this.dom).delegate('.ex-act', 'click', function (evt) {
                var action = evt.currentTarget.getAttribute('action'),
                    callback,
                    callbackEnd,
                    aItems,
                    item,
                    preference,
                    tag;

                switch(action) {
                    case 'createPortal':
                        This.createPortal();
                        break;
                    case 'createPageTemplate':
                        This.createTemplate('PAGE');
                        break;
                    case 'createContainerTemplate':
                        This.createTemplate('CONTAINER');
                        break;
                    case 'createWidgetTemplate':
                        This.createTemplate('WIDGET');
                        break;
                    case 'createPage':
                        This.createItem('page');
                        break;
                    case 'createContainer':
                        This.createItem('container');
                        break;
                    case 'createWidget':
                        This.createItem('widget');
                        break;
                    case 'createLink':
                        This.createLink();
                        break;
                    case 'createRootLink':
                        This.createLink('root');
                        break;
                    case 'cut':
                        alert('stub')
                        break;
                    case 'copy':
                        alert('stub')
                        break;
                    case 'paste':
                        alert('stub')
                        break;
                    case 'delete':
                        callback = function() {};
                        callbackEnd =  function(){
                            if (This.panelNorth.currentQuery){
                                This.panelNorth.loadContent(This.panelNorth.currentQuery.contextItemName, This.panelNorth.currentQuery.tag, This.panelNorth.currentQuery.name);
                            }
                            notify('Item has been deleted');
                        };

                        aItems = This.panelNorth.selection.array;
                        for (var i = 0; i < aItems.length; i++) {
                            item = getItemRefFromHTML(aItems[i]);
                            if (i==aItems.length-1) callback = callbackEnd;
                            portalServer.deleteItem(item, callback);
                            // remove deleted item from the tree
                            $('a[data-uuid="' + item.uuid + '"]').parent().parent().remove();
                        }
                        break;
                    case 'addPreference':
                        This.panelCenter.addPreference();
                        break;
                    case 'removePreference':  //delete property
                        preference = evt.currentTarget.getAttribute('actionData');
                        This.panelCenter.removePreference(This.panelCenter.item, preference);
                        break;
                    case 'addTag':
                        This.panelCenter.addTag();
                        break;
                    case 'deleteTag':
                        tag = evt.currentTarget.getAttribute('actionData');
                        This.panelCenter.deleteTag(tag);
                        break;
                }

            });

            //  ----------------------------------------------------------------
            //  Portal Select
            //  ----------------------------------------------------------------

            var elm = panelOutWest.getInsertableDOM();
            panelOutWest.loadPortals();
            panelNorth.loadContent('', 'portal', '');  // automatically updates content
            panelCenter.loadItemsDetail([], false);


            this.itemUpdated({contextItemName:'[BBHOST]', tag:'portal', name:''});


            var draggable = {
                distance: 7,
                cursorAt: {
                    left: -10, top: 0
                },
                helper: function(ev, ui) {
                    var trg = ev.originalEvent.target || ev.originalEvent.srcElement,
                        str = '',
                        tagName = '',
                        elm;

                    if($(trg).hasClass('bc-tree-a')) {
                        elm = trg.parentNode.parentNode;
                        tagName = elm.getAttribute('tag');
                        str = trg.innerHTML;
                    }
                    if ($(trg).hasClass('bc-tree-selector')){
                        elm = trg.parentNode.parentNode;
                        tagName = elm.getAttribute('tag');
                        str = $(elm).find('.bc-tree-a')[0].innerHTML;
                    }
                    if($(trg).hasClass('ex-td-label')) {
                        elm = trg.parentNode.parentNode;
                        tagName = elm.getAttribute('tag');
                        for (var i = 0; i < This.panelNorth.selection.array.length; i++) {
                            str += $('.ex-td-label',This.panelNorth.selection.array[i])[1].innerHTML+'<br/>';
                        }
                    }
                    if (This.panelNorth.dom === $(trg)[0].parentNode){
                        for (var i = 0; i < This.panelNorth.selection.array.length; i++) {
                            str += $('.ex-td-label',This.panelNorth.selection.array[i])[1].innerHTML+'<br/>';
                        }
                    }
                    elm = $('<div class="ex-dragholder"><i class="bd-explorer-icon ex-dragicon bi-m bd-explorer-' + tagName + '"></i> '+str+'<span>&nbsp;</span></div>')[0];
                    document.body.appendChild(elm);
                    return elm;
                },
                start: function(ev, ui) {
                    var trg = ev.originalEvent.target || ev.originalEvent.srcElement,
                        elm = null,
                        item;

                    This.dragItems = [];
                    if($(trg).hasClass('bc-tree-a')) {
                        elm = trg.parentNode.parentNode;
                        item = getItemRefFromHTML(elm);
                        This.dragItems.push(item)
                        window.DRAGMODE = elm.parentNode.parentNode.getAttribute('mode');
                    }
                    if($(trg).hasClass('ex-td-label')) {
                        elm = trg.parentNode.parentNode;
                        for (var i = 0; i < This.panelNorth.selection.array.length; i++) {
                            elm = This.panelNorth.selection.array[i]
                            item = getItemRefFromHTML(elm);
                            This.dragItems.push(item)
                        }
                    }
                    $('.bd-widget-Explorer').addClass('bd-dragging');
                    if (This.dragItems.length) {
                    }
                    else
                        return false;
                },

                drag: function(ev, ui) {
                    var s = '';
                    if (ev.ctrlKey) {
                        s = ' (copy)';
                    }
                    else if (ev.altKey) {
                        s = ' (extend)';
                    }
                    if (window.DRAGMODE == 'catalog') {
                        s = ' (extend)';
                    }
                    ui.helper[0].lastChild.innerHTML = s;
                },

                stop: function(ev, ui) {
                    var trg = ev.originalEvent.target || ev.originalEvent.srcElement,
                        li,
                        targetItem,
                        mode,
                        createMode,
                        item,
                        parentItemName,
                        callback,
                        callbackEnd,
                        n2;

                    if($(trg).hasClass('bc-tree-a')) {
                        li = trg.parentNode.parentNode;

                        $('.bd-widget-Explorer').removeClass('bd-dragging');

                        targetItem = getItemRefFromHTML(li);
                        mode = li.getAttribute('mode');
                        if (mode) li = li.parentNode.parentNode;

                        if (mode != 'templates') {
                            createMode = 'move';
                            if (ev.ctrlKey)createMode = 'copy';
                            if (ev.altKey)createMode = 'extend';
                            if (window.DRAGMODE == 'catalog' )createMode = 'extend';
                            if (mode == 'catalog' )createMode = 'extend';
                            if (ev.shiftKey)createMode = 'move';
                            window.DRAGMODE = '';

                            item = This.dragItems[0];
                            parentItemName = item.parentItemName;


                            if (targetItem.contextItemName == item.contextItemName || ( targetItem.tag=='portal' && targetItem.name == item.contextItemName)) {
                                callback = function(){};
                                callbackEnd =  function(){
                                    var res = $('[contextItemName="'+item.contextItemName+'"][name="'+targetItem.parentItemName+'"]',This.panelOutWest.dom)[0];

                                    This.panelOutWest.loadSubTreeItem(targetItem.contextItemName, targetItem.tag, targetItem.name, li, function () {});
                                    if (This.panelNorth.currentQuery) {
                                        This.panelNorth.loadContent(This.panelNorth.currentQuery.contextItemName, This.panelNorth.currentQuery.tag, This.panelNorth.currentQuery.name);
                                    }

                                };

                                for (var i = 0; i < This.dragItems.length; i++) {
                                    item = This.dragItems[i];
                                    if (i == This.dragItems.length-1) callback = callbackEnd;

                                    n2 = portalServer.simpleRenamer(item.name);

                                    if (createMode == 'copy') {
                                        portalServer.copyItem(item, callback);
                                        notify('Item copied successfully');
                                    }
                                    else if (createMode == 'extend') {
                                        item.extendedItemName = item.name;
                                        item.parentItemName = targetItem.name;
                                        item.name = n2;
                                        portalServer.newItem(item,callback);
                                        notify('Item extended successfully');
                                    }
                                    else {
                                        item.parentItemName = targetItem.name;
                                        portalServer.updateItem(item, callback);
                                    }

                                }
                            }

                        }
                    }

                }
            };

            $( this.dom ).draggable(draggable);

        },


        createItem: function(itemType){
            var This = this,
                content =
                '<li class="bc-g" style="margin:10px">' +
                '<div class="bc-1-3">' +
                '<p class="bc-i bc-label">Name</p>' +
                '</div>' +
                '<div class="bc-2-3">' +
                '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" name="itemName" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                '</div>' +
                '</li>',
                portalName = this.panelOutWest.getSelectedPortal(),
                $form;
            if (portalName) {
                $form = bc.component.modalform({
                    width: '450px',
                    title: 'Create ' + itemType,
                    content: content,
                    uuid: '19284698465',
                    ok: 'Create',
                    cancel: 'Cancel',
                    okCallback: function(theForm) {
                        var properties = [],
                            itemName = theForm.find('input[name="itemName"]').val();

                        if (itemType === 'widget') {
                            properties.push(portalServer.propertyObject('widgetChrome', '', 'Widget Chrome', 'string', 'select-one,designModeOnly', true));
                        }
                        portalServer.newItem(portalServer.itemObject(portalName, itemType, itemName, null, null, properties), function () {
                            This.panelOutWest.setSelected('[BBHOST]', 'portal', portalName, 'catalog');
                            notify( itemType   +   '  has been created ');
                            // reload tree items
                            This.notifyObservers('reloadItemsTree', itemType, null, portalName);
                        });
                    }
                });
            }
        },

        createPortal : function () {
            var This = this,
                content =
                    '<li class="bc-g" style="margin:10px">' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Portal name</p>' +
                    '</div>' +
                    '<div class="bc-2-3">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '</li>',
                $form = bc.component.modalform({
                    width: '450px',
                    title: 'Create Portal',
                    content: content,
                    uuid: '1928469823465',
                    ok: 'Create',
                    cancel: 'Cancel',
                    okCallback: function(theForm) {
                        var portalName = theForm[0][0].value;
                        url = be.contextRoot + "/portals/" + portalName + "/createPortal?title=" + portalName + "&targetDevice=mixed&optimizedFor=mixed&addAllWidgets=true";
                        be.utils.ajax({
                            url : url,
                            success : function () {
                                This.panelOutWest.loadPortals();
                                This.panelNorth.loadContent('', 'portal', '');
                                notify('Portal has been created');
                                if(top.bd && top.bd.refreshPortalList){
                                    top.bd.refreshPortalList();
                                }
                                // reload tree items
                                This.notifyObservers('reloadItemsTree', 'portal');
                            },
                            type : 'POST'
                        });
                    }
                });
        },
        createTemplate : function (type) {
            var This = this,
                portalName = this.panelOutWest.getSelectedPortal(),
                content =
                    '<li class="bc-g" style="margin:10px">' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Template name</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" name="name" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Bundle name</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" name="bundleName" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Title</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" name="title" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '</li>',

                $form = bc.component.modalform({
                    width: '450px',
                    title: 'Create Template',
                    content: content,
                    uuid: '1928469823465',
                    ok: 'Create',
                    cancel: 'Cancel',
                    okCallback: function($createForm) {
                        var templateName = $createForm.find('input[name="name"]').val(),
                            bundleName = $createForm.find('input[name="bundleName"]').val(),
                            titleName = $createForm.find('input[name="title"]').val();

                        portalServer.newItem(
                            portalServer.itemObject(
                                portalName,
                                'template',
                                templateName,
                                null,
                                null,
                                [
                                    {'name': 'BundleName','value': bundleName},
                                    {'name': 'DefaultDevice','value': 'Web'},
                                    {'name': 'Web','value': templateName},
                                    {'name': 'title','value':titleName }
                                ],
                                {'type': type}
                            ), function () {
                                notify('Template has been created');
                                // reload tree items
                                This.notifyObservers('reloadItemsTree', 'template', type, portalName);
                            }
                        );
                    }
                });
        },
        createLink : function (linkType) {
            var This = this,
                portalName = this.panelOutWest.getSelectedPortal(),
                isRootLink = linkType === 'root',
                content =
                    '<li class="bc-g" style="margin:10px">' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Create link for ' + portalName + '</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Menu Label</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">Link Title</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '<div class="bc-1-3">' +
                    '<p class="bc-i bc-label">URL</p>' +
                    '</div>' +
                    '<div class="bc-2-3 ex-newitem-dialogue">' +
                    '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" value="http://" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                    '</div>' +
                    '</li>',

                $form = bc.component.modalform({
                    width: '450px',
                    title: 'Create Link',
                    content: content,
                    uuid: '19284693465',
                    ok: 'Create',
                    cancel: 'Cancel',
                    okCallback: function(theForm) {
                        var linkName = theForm[0][0].value,
                            labelName = theForm[0][1].value,
                            titleName = theForm[0][2].value,
                            urlName = theForm[0][3].value;

                        if(linkName && labelName && urlName) {
                            var properties = [
                                portalServer.propertyObject('title', labelName, 'Link Title', 'string', '', true),
                                portalServer.propertyObject('menuAccessibilityTitle', titleName, 'Menu Label', 'string', '', true),
                                portalServer.propertyObject('Url', urlName, 'URL', 'string', '', true),
                                portalServer.propertyObject('itemType', isRootLink ? 'menuHeader' : 'externalLink', '', 'string', '', true)
                            ];
                            portalServer.newItem(
                                portalServer.itemObject(
                                    portalName,
                                    'link',
                                    linkName,
                                    null,
                                    isRootLink ? portalName : 'navroot_mainmenu',
                                    properties
                                ),
                                function () {
                                    This.panelOutWest.setSelected('[BBHOST]', 'portal', portalName, 'catalog');
                                    notify('Link has been created');
                                    // reload tree items
                                    This.notifyObservers('reloadItemsTree', 'link', null, portalName);
                                }
                            );
                        }
                    }
                });
        },

        itemUpdated : function (item) {
            this.notifyObservers('itemUpdated', item);
        },

        itemUpdatedListener : function (ev) {
        },

        reloadItemsTree : function(itemType, tType, portalName) {
            var items = 'li[mode="items"]',
                catalog = 'li[mode="catalog"]',
                links = 'li[mode="link"]',
                templatesRoot = 'li[mode="templates"] ',
                templateType = ' li[filter*={{ITEM_TYPE}}]',
                mainNavLink = ' .bc-tree-ul li[name="navroot_mainmenu"] ',
                portalRootTree = '.bc-tree-ul.tree-root > li[tag="portal"]',
                portalToReload = portalRootTree +  ' .bc-tree-ul li[name="' + portalName + '"] ',
                contentRepoSelector = 'li[mode="contentRepository"][contextItemname="'+portalName+'"]';

            // for deletion of items portal is not defined
            if(portalName === null) portalToReload = '.bc-tree-ul.tree-root > li[tag="portal"] .bc-tree-ul ';

            var updateTree = function(component) {
                var tPlus, tMinus = $(portalToReload + component + ' .bc-tree-minus');

                if(component === 'contentRepository') {
                    if(portalName === '[BBHOST]') {
                        // find shared repositories folder and remove the repository
                        $(portalRootTree + ' .bc-tree-ul li[name="' + portalName + '"][mode="contentRepository"] li[tag="contentRepository"]').remove();
                    } else {
                        $(portalToReload + component + ' li[tag="contentRepository"]').remove();
                    }
                    return;
                }

                // for templates and catalog items outside portal
                if(portalName === '[BBHOST]') {
                    tMinus = $($.trim(portalToReload) + component + ' .bc-tree-minus');
                }

                if(tMinus) {
                    $(tMinus).click();
                    tPlus = $(portalToReload + component + ' .bc-tree-plus');

                    // for templates and catalog items outside portal
                    if(portalName === '[BBHOST]') {
                        tPlus = $($.trim(portalToReload) + component + ' .bc-tree-plus');
                    }
                    if(tPlus) {
                        if(itemType != 'template' || itemType != 'link')
                        {
                            $(tPlus).click();
                        } else {
                            $(tPlus[0]).click();
                        }

                    }
                    if(itemType == 'template') {
                        var tTLinkPlus,
                            tTlinkMinus = $(portalToReload + component + templateType.replace('{{ITEM_TYPE}}', tType) + ' .bc-tree-minus');

                        if(tTlinkMinus) {
                            $(tTlinkMinus).click();
                            tTLinkPlus = $(portalToReload + component + templateType.replace('{{ITEM_TYPE}}', tType) + ' .bc-tree-plus');
                            $(tTLinkPlus).click();
                        }
                        if(portalName === '[BBHOST]') {
                            $($($.trim(portalToReload) + component + ' .bc-tree-a')[0]).click();
                        } else {
                            $($(portalToReload + component + ' .bc-tree-a')[0]).click();
                        }
                    }
                    if(itemType == 'link') {
                        var mainLinkPlus,
                            mainLinkMinus = $(portalToReload + component + mainNavLink + ' .bc-tree-minus');

                        if(mainLinkMinus) {
                            $(mainLinkMinus).click();
                            mainLinkPlus = $(portalToReload + component + mainNavLink + ' .bc-tree-plus');
                            $(mainLinkPlus).click();
                        }
                        if(portalName === '[BBHOST]') {
                            $($($.trim(portalToReload) + component + ' .bc-tree-a')[0]).click();
                        } else {
                            $($(portalToReload + component + ' .bc-tree-a')[0]).click();
                        }

                    }
                }
            };

            if(itemType === 'portal') {
                updateTree(portalRootTree);
            }
            if(itemType === 'page') {
                updateTree(catalog);
            }
            if(itemType === 'container') {
                updateTree(catalog);
            }
            if(itemType === 'widget') {
                updateTree(catalog);
            }
            if(itemType === 'template') {
                updateTree(templatesRoot);
            }
            if(itemType === 'link') {
                updateTree(links);
            }
            if(itemType === 'contentRepository') {
                updateTree('contentRepository');
            }
            updateTree(items);
        }
    });



//  ----------------------------------------------------------------
    var ExplPanelDomView = DomView.extend(function (app, parent){
//  ----------------------------------------------------------------
        DomView.call(this);
        this.app = app;
        this.parent = parent;
    },{
        panelString :  function (h,b) {
            return '<div class="ex-panel">' +
                '   <div class="ex-panel-h"><span class="ex-panel-h-label">'+h+'</span></div>' +
                '   <div class="ex-panel-b">'+b+'</div>' +
                '</div>';
        },
        buildDOM : function () {
            this.dom = domBuilder(this.panelString('Default',''));
        },
        getInsertableDOM : function () {
            return $('.ex-panel-b', this.dom)[0];
        },
        readyDOM : function () {
            this.elmHead = $('.ex-panel-h', this.dom);
            this.elmBody = $('.ex-panel-b', this.dom);
            this.elmHeadLabel = $('.ex-panel-h-label', this.dom);

        }
    });

//  ----------------------------------------------------------------
    var EPDV_Console = ExplPanelDomView.extend(function (app, parent){
//  ----------------------------------------------------------------
        ExplPanelDomView.call(this, app, parent);
    },{
        buildDOM : function () {
            this.dom = domBuilder(this.panelString('Console <button>Import</button><button>Export</button>','<textarea style="width:100%;height:100%;"></textarea>'));
        },
        readyDOM : function () {
            var This = this;

            this.textarea = $('textarea', this.dom)[0];
            $(this.dom).delegate('button', 'click', function (evt) {
//              This.app
            });
        }
    });



//  ----------------------------------------------------------------
    var EPDV_Tree = ExplPanelDomView.extend(function (app, parent){
//  ----------------------------------------------------------------
        ExplPanelDomView.call(this, app, parent);
    },{

        panelString :  function (h,b) {

            return '<div class="ex-panel">' +
                '   <div class="ex-panel-h"><span class="ex-panel-h-label">'+
                '<a class="ex-act-popup ex-act-popup2" href="" for="popup2">'+
                '<span class="">'+
                '<span class="bc-button-split bd-grey bd-button bd-action-button bd-roundcorner-left-3 ex-but bd-left-btn"><span class="bi bi-s bi-pm-plus"></span></span>'+
                '<span class="bc-select-grip bd-selectbox bd-action-select bd-button bd-grey bd-roundcorner-right-3 bd-right-btn" ><span class="bd-icn bd-triangle"></span></span>'+
                '</span>'+
                '</a>'+h+'</span></div>' +
                '   <div class="ex-panel-b">'+
                '<div class="css-alignright">'+
                '<ul class="css-ul-horzlist">'+
                '<li class="css-li-horzlist">'+
                '</li>'+
                '</ul>'+
                '</div>'+
                '<div class="bcj-body">'+
                '</div>'+
                '</div>' +
                '</div>';
        },

        buildDOM : function () {

            var This = this;
            this.dom = domBuilder(this.panelString('Tree',''));

            function toggleMenuForMode(menuEl, mode) {
                menuEl.style.display = menuEl.dataset.forMode === mode ? 'block' : 'none';
            }

            $('body').delegate('*', 'click', function (evt) {
                if (window.currentPopup) {
                    window.currentPopup.style.display = 'none';
                    window.currentPopup = null;
                }
                if (window.currentDropdown) {
                    window.currentDropdown.removeClass('bd-open');
                    window.currentDropdown = null;
                }
            });
            $('body').delegate('.ex-act-popup', 'click', function (evt) {
                var popupName, popup, dropdown, off, width, height, poff, rightOffset, topOffset;

                if(!$(evt.target).hasClass('bc-button-split')) {
                    var selected = This.app.panelOutWest.getSelected();
                    var selectedMode = selected && selected.mode;
                    popupName = evt.currentTarget.getAttribute('for');
                    popup = This.app.dialogs[popupName];
                    toggleMenuForMode(popup.querySelector('[action="createRootLink"]'), selectedMode);
                    dropdown = $(evt.currentTarget);
                    if(window.currentPopup) window.currentPopup.style.display = 'none';
                    window.currentPopup = popup;
                    window.currentDropdown = dropdown;
                    if (popup) {
                        off = $(evt.currentTarget).offset();
                        height = $(evt.currentTarget).height();
                        width = $(evt.currentTarget).width();
                        poff = $(popup.parentNode).offset();
                        rightOffset = window.innerWidth - off.left - width - 10;
                        topOffset = off.top + height - poff.top + 5;

                        popup.style.top = topOffset + 'px';
                        popup.style.right = rightOffset + 'px';
                        popup.style.display = 'block';
                        dropdown.addClass('bd-open');
                    }
                }
                return false;
            });

            // TREE
            $(this.dom).delegate('.bc-tree-plus','click', function (evt){
                var li = evt.target.parentNode.parentNode,
                    contextItemName = li.getAttribute('contextitemname'),
                    tag = li.getAttribute('tag'),
                    name = li.getAttribute('name'),
                    mode = li.getAttribute('mode') || tag,
                    parentItemName = li.getAttribute('parentItemName');

                $(evt.target).removeClass('bc-tree-plus');
                $(evt.target).addClass('bc-tree-minus');
                $(evt.target.parentNode.parentNode).removeClass('bc-tree-closed');

                if(mode == 'items') {
                    if(!li.loaded) {
                        li.loaded = true;
                    }
                    This.loadSubTree(name,'page','', li);
                }
                else if(mode == 'catalog') {
                    if(!li.loaded) {
                        li.loaded = true;
                    }
                    This.loadSubTree(name,'catalog', '', li);
                }
                else if(mode == 'link') {
                    if(!li.loaded) {
                        li.loaded = true;
                    }
                    if(parentItemName){
                        // nested links
                        This.loadSubTree(contextItemName, 'link', '', li, null, null, name);
                    }else {
                        // root links
                        This.loadSubTree(name, 'link', '', li);
                    }
                }
                else if(mode == 'templates') {
                    This.loadSubTree(name,'template', '', li);
                } else if(mode == 'contentRepository') {
                    This.loadSubTree(name,'contentRepository', name, li);
                }


            });

            $(this.dom).delegate('.bc-tree-minus','click', function (evt){
                var trg = evt.target;
                $(trg).removeClass('bc-tree-minus');
                $(trg).addClass('bc-tree-plus');
                $(trg.parentNode.parentNode).addClass('bc-tree-closed');
            });

            $(this.dom).delegate('.bc-tree-a','click', function (evt) {
                var li = evt.target.parentNode.parentNode,
                    contextItemName = li.getAttribute('contextitemname'),
                    tag = li.getAttribute('tag'),
                    name = li.getAttribute('name'),
                    mode = li.getAttribute('mode'),
                    filter = li.getAttribute('filter'),
                    parentItemName = li.getAttribute('parentItemName'),
                    isRepoItem = $(li).data('repoitem'),
                    contentRepositoryItem;

                selectedItemId = $(evt.target).data('uuid');

                if (contextItemName === '[BBHOST]' && (!name || name === '[BBHOST]')) {
                    This.app.panelCenter.loadItemsDetail([], false); // Clear center panel

                } else {

                    if(tag === 'contentRepository') {
                        // to show content repository, name and contextItemName attrs need to be set from
                        // the selected content repository item
                        contentRepositoryItem = $(evt.target.parentNode.parentNode).find('ul li[tag="contentRepository"]')[0];

                        if(contentRepositoryItem) {
                            tag = contentRepositoryItem.getAttribute('tag');
                            name = contentRepositoryItem.getAttribute('name');
                            mode = contentRepositoryItem.getAttribute('mode');
                            parentItemName = contentRepositoryItem.getAttribute('parentItemName');
                        }

                    }

                    This.app.panelCenter.loadItemsDetail([{contextItemName:contextItemName,tag:tag,name:name}], isRepoItem ? true : false); // This code copied from dblclick

                }

                $('.bc-tree-selected',This.dom).removeClass('bc-tree-selected');
                $($('.bc-tree-selector',li)[0]).addClass('bc-tree-selected');

                if(mode == 'items') {
                    This.app.panelNorth.loadContent(contextItemName, tag, name);
                }
                else if(mode == 'catalog') {
                    This.app.panelNorth.loadContent(name, 'catalog', '' );
                }
                else if(mode == 'templates') {
                    This.app.panelNorth.loadContent(contextItemName, 'template', '');
                }
                else if(mode == 'link' || tag == 'link') {

                    if(parentItemName){
                        // nested links
                        This.app.panelNorth.loadContent(contextItemName, 'link', '', null, null, name);
                    }else {
                        // root links
                        This.app.panelNorth.loadContent(name, 'link', '');
                    }
                }
                else if (mode == null && filter){
                    This.app.panelNorth.loadContent(contextItemName, 'template', '', filter);
                }
                else {
                    This.app.panelNorth.loadContent(contextItemName, tag, name);
                }
            });
        },

        setSelected : function (contextItemName, tag, name, mode) {
            var q, res;
            $('[cont]',this.dom)
            q = '[contextItemName="'+contextItemName+'"][name="'+name+'"]';
            if (mode) q += '[mode="'+mode+'"]';
            res = $(q,this.dom)[0];
            if (res) {
                $('.bc-tree-selected',this.dom).removeClass('bc-tree-selected');
                $($('.bc-tree-selector',res)[0]).addClass('bc-tree-selected');

                if(mode == 'items') {
                    this.app.panelNorth.loadContent(contextItemName, tag, name)
                }
                else if(mode == 'catalog') {
                    this.app.panelNorth.loadContent(name, 'catalog', '' )
                }
                else if(mode == 'templates') {
                    this.app.panelNorth.loadContent(contextItemName, 'template', '')
                }
                else {
                    this.app.panelNorth.loadContent(contextItemName, tag, name)
                }
            }

        },

        getSelected : function () {
            var elm = $('.bc-tree-selected',this.dom)[0],
                li,
                item;
            if(elm) {
                li = elm.parentNode.parentNode;
                item = {};
                item.contextItemName = li.getAttribute('contextitemname');
                item.tag = li.getAttribute('tag');
                item.name = li.getAttribute('name');
                item.mode = li.getAttribute('mode');
                return item;
            }
            return null;
        },

        getSelectedPortal : function () {
            var item = this.getSelected();
            if (item) {
                if (item.contextItemName == '[BBHOST]')
                    return item.name;
                return item.contextItemName;
            }
            else return '[BBHOST]';

        },

        getInsertableDOM : function () {
            return $('.bcj-body', this.dom)[0];
        },

        XreadyDOM : function () {

        },

        loadPortals : function () {
            var This = this;
            portalServer.loadItem(portalServer.itemObject('','portal'), function (item, xml) {
                var elm = This.getInsertableDOM(),
                    cld = xml.firstChild,
                    nodes,
                    ul;
                if (typeof cld.setAttribute !== 'function'){
                    cld = cld.nextSibling;
                }
                cld.setAttribute('contextRoot', contextRoot);
                view.apply('tree', xml, elm);

                $(elm.firstChild).bind('change', function(evt){
                    var v = evt.target.options[evt.target.selectedIndex].value;
                    viewPortal(v);
                })

                nodes = $('.bc-tree-minus', elm);
                for(var i=1;i<nodes.length;i++) {
                    $(nodes[i]).removeClass('bc-tree-minus');
                    ul = $('> UL',nodes[i].parentNode.parentNode)[0];
                    if (ul && ul.childNodes.length)
                        $(nodes[i]).addClass('bc-tree-plus');
                    else
                        $(nodes[i]).addClass('bc-tree-none');
                    $(nodes[i].parentNode.parentNode).addClass('bc-tree-closed');
                }
            }, false, 1000);
        },

        loadSubTree : function (contextItemName,tag, name, elm, callback, offset, parentItemName) {
            var This = this,
                filter,
                portalItemObject,
                isLoadMoreBtnHidden;

            this.offset = offset || 0;

            if (tag === "catalog") {
                filter = getCatalogItemTypesFilter();
            }

            portalItemObject = portalServer.itemObject(contextItemName, tag, name, null, parentItemName, null, null,
                filter);

            portalServer.loadItem(portalItemObject, function (item, xml) {
                var nodes, pager, i, node,
                    ul = $('UL',elm)[0],                    // get first ul in element
                    cld = xml.firstChild,                       // get root node
                    html = $(view.apply('tree', xml)),  // convert xml to html and write to element
                    showMore = function(){
                        This.loadSubTree(contextItemName,tag, name, elm, callback, This.offset + PAGE_SIZE, parentItemName);
                    };

                if (typeof cld.setAttribute !== 'function'){    // if node is whitespace text node go for the next node
                    cld = cld.nextSibling;
                }
                cld.setAttribute('contextRoot', contextRoot);   // now set the contextRoot attribute

                if(ul){
                    if(This.offset) {
                        $("> li", html).appendTo(ul);
                    } else {
                        ul.innerHTML = html.length ? html.html() : '';
                    }
                }else{
                    html.appendTo(elm);
                }

                nodes = $('.bc-tree-minus', elm);
                for(i = 1; i < nodes.length; i++) {
                    node = $(nodes[i]);
                    node.removeClass('bc-tree-minus');
                    ul = $('> UL', nodes[i].parentNode.parentNode)[0];
                    if (ul && (ul.childNodes.length || $(ul).hasClass('bc-tree-hasChildren-true'))){            // add a plus icon to items with subtree
                        node.addClass('bc-tree-plus');
                    }else{
                        node.addClass('bc-tree-none');
                    }
                    $(nodes[i].parentNode.parentNode).addClass('bc-tree-closed');
                }

                nodes = $("> .bc-tree-ul > .bc-tree-li", elm);
                pager = $(".bd-explorer-paging", elm);

                if(nodes.length === PAGE_SIZE && (!pager.length || pager.length == 0)){
                    pager = $("<div class='bd-explorer-paging ex-load-more-btn'><button class='bd-explorer-btn-next bd-grey bd-roundcorner-3 ex-load-btn'>Load more</button></div>").appendTo(elm);
                    $(".bd-explorer-btn-next", pager).on("click", showMore);
                }

                pager.toggleClass('bd-explorer-paging-hide', nodes.length - This.offset < PAGE_SIZE);

                if(callback){
                    callback();
                }
            },true, PAGE_SIZE, offset);

            function getCatalogItemTypesFilter() {
                var treeViewUrl = view.tmpl_urls[TREE_VIEW_NAME];
                var config = {catalog: true, itemFn: true};

                return $(be.utils.processHTMLTemplateByUrl(treeViewUrl, config)).text().toUpperCase();
            }
        },

        loadSubTreeItem : function (contextItemName,tag, name, elm, callback) {
            portalServer.loadItem(portalServer.itemObject(contextItemName, tag, name), function (item, xml) {
                var cld = xml.firstChild,
                    s,
                    elm2,
                    nodes,
                    ul;

                if (typeof cld.setAttribute !== 'function'){
                    cld = cld.nextSibling;
                }
                cld.setAttribute('contextRoot', contextRoot);
                s = view.apply('tree', xml);
                elm2 = $(s)[0];

                if(elm2) {
                    elm.parentNode.insertBefore(elm2,elm);
                    elm.parentNode.removeChild(elm);
                }

                nodes = $('.bc-tree-minus',elm2);
                for(var i = 1; i < nodes.length; i++) {
                    $(nodes[i]).removeClass('bc-tree-minus');
                    ul = $('> UL',nodes[i].parentNode.parentNode)[0];
                    if (ul && ul.childNodes.length)
                        $(nodes[i]).addClass('bc-tree-plus');
                    else
                        $(nodes[i]).addClass('bc-tree-none');

                    $(nodes[i].parentNode.parentNode).addClass('bc-tree-closed');
                }
                if(callback)callback();

            }, true, 1000)
        }
    });


//  ----------------------------------------------------------------
    var EPDV_List = ExplPanelDomView.extend(function (app, parent){
//  ----------------------------------------------------------------
        ExplPanelDomView.call(this, app, parent);
        this.columns = [];
        this.selection = new HashMap();
    },{
        panelString :  function (h,b) {
            return '<div class="ex-panel">' +
                '   <div class="ex-panel-h"><span class="ex-panel-h-label">'+

                '<a class="ex-act-popup ex-act-popup1" href="" for="popup1" style="margin-right:14px">'+
                '<span class="bd-selectbox bd-grey bd-roundcorner-3">'+
                '<span class="bc-button-split bd-label">Actions</span>'+
                '<span class="bc-select-grip bd-dd-arrow">' +
                '<span class="bd-icn bd-triangle"></span>' +
                '</span>'+
                '</span>'+
                '</a>'+h+'</span>' +
                '<span class="ex-td-label" style=" float: right; margin-right:10px;" title="Use Shift or Ctrl to select multiple ">For selected item(s) </span>'+
                '</div>' +

                '   <div class="ex-panel-b ex-panel-b-a">'+
                '<div class="css-alignright ex-details">'+
                '<ul class="css-ul-horzlist">'+
                '<li class="css-li-horzlist">'+

                '</li>'+
                '</ul>'+
                '</div>'+
                '<div class="bcj-body">'+
                '</div>'+
                '</div>' +
                '</div>';
        },

        buildDOM : function () {
            this.dom = domBuilder(this.panelString('List',''));
        },

        getInsertableDOM : function () {
            return $('.bcj-body', this.dom)[0];
        },

        loadContent : function (contextItemName, tag, name, filter, offset, parentItemName) {
            var off = offset ? offset : 0;
            if(contextItemName == "[BBHOST]" && tag == "portal" && name){
                tag = "page";
                contextItemName = name;
                name = "";
            }
            var params = {
                contextItemName: contextItemName,
                tag: tag,
                name: name,
                filter: filter,
                offset: off,
                parentItemName: parentItemName
            };
            this.currentQuery = {contextItemName:contextItemName, tag:tag, name:name};
            this.removeAllFromSelection();
            var This = this;
            var item = portalServer.itemObject(contextItemName, tag, name, null, parentItemName, null, null, filter);

            portalServer.loadItem(item, function (item, xml) {
                params.html = view.apply('list', xml);
                params.elm = $(This.getInsertableDOM());
                This.enhanceTable(params);
            },true, PAGE_SIZE + 1, off);
        },

        pager: {},

        fetching: false,

        enhanceTable : function (params) {

            var tbody, table,
                This = this,
                columns = this.columns,
                bool = true,
                addRows = function(){
                    This.fetching = true;
                    params.offset += PAGE_SIZE;
                    This.loadContent(params.contextItemName, params.tag, params.name, params.filter, params.offset, params.parentItemName);
                };

            if(params.offset == 0){     // new list: clear container element + add table + add header row + add 'pager'
                params.elm.html("");
                table = $(params.html).appendTo(params.elm);
                /*List table header*/
                var s = '<thead><tr class="ex-list-tr-head td-head"><th style="width:0;"></th><th class="ex-list-th" style="width:3%;"><span class="ex-td-label"></span></th><th class="ex-list-th"><span class="ex-td-label">Title</span></th><th class="ex-list-th"><span class="ex-td-label">Name</span></th>';
                for (var i=0;i<columns.length;i++) {
                    s+='<th class="ex-list-th" onclick="window.exact.toggleColumn(\''+columns[i]+'\')">'+columns[i]+'</th>';
                }
                s += '</tr></thead>';
                tbody = $("tbody", table);
                tbody.before(s);
                var trs = $("tr", tbody);
                if(trs.length > PAGE_SIZE){
                    trs.last().hide();
                    This.pager = $('<div class="bd-explorer-paging"><div class="bc-grey"><span class="bd-explorer-loading">Loading next <span class="bd-expolorer-pagesize">' + PAGE_SIZE + '</span>...</span></div></div>');
                    $(table).after(This.pager);
                    This.pager.hide();
                    params.elm.closest(".ex-panel-b").on("scroll", function() {
                        var $this = $(this);
                        var to;
                        if(!This.fetching){
                            if($this.scrollTop() + $this.innerHeight() >= $this[0].scrollHeight - 20) {
                                if(to){
                                    clearTimeout(to);
                                }
                                to = setTimeout(addRows, 1000);
                                This.pager.show();
                            }
                        }
                    });
                }
            }else{  // call back (after scrolling to bottom of list)
                This.fetching = false;
                This.pager.hide();

                table = $('.ex-list-table', params.elm);
                var newRows = $(params.html).find(".ex-list-tr");
                if(newRows.length <= PAGE_SIZE){
                    params.elm.closest(".ex-panel-b").off("scroll");
                }else{
                    newRows.last().hide();
                }
                tbody = $("tbody", table);
                tbody.append(newRows);
            }

            $('tbody tr', table).each(function(){
                var a, v, t, lis;
                this.style.backgroundColor = bool ? '#fff' : '#fff';
                bool = !bool;

                $('.generated', this).remove();
                a = [];
                lis = $('.ex-properties LI', this);
                for (var i=0;i<columns.length;i++) {
                    v = '';
                    for (var j=0;j<lis.length;j++) {
                        t = $('.prop-name', lis[j]).text();
                        if (t==columns[i]) {
                            v = $('.prop-value', lis[j]).text();
                            break;
                        }
                    }
                    $(this).append('<td class="ex-pagetd generated">'+v+'</td>');
                }
            });
        },

        addToSelection : function (contextItemName, tag, name, obj) {
            this.selection.add(contextItemName+'&'+tag+'&'+name, obj);
        },

        removeFromSelection : function ( obj) {
            this.selection.remove(obj);
        },

        removeAllFromSelection : function () {
            while(this.selection.array.length) {
                $(this.selection.array[0]).removeClass('ex-selected');
                this.selection.remove(this.selection.array[0]);
            }
        },

        readyDOM : function () {
            var This = this;

            $(this.getInsertableDOM()).delegate('TR','mousedown', function (evt){

                var tr = evt.currentTarget,
                    contextItemName = tr.getAttribute('contextitemname'),
                    tag = tr.getAttribute('tag'),
                    name = tr.getAttribute('name'),
                    bLoadCheck = true,
                    isContentRepository = tag === 'contentRepository',
                    high = 0,
                    low = 1000,
                    elm,
                    tbody,
                    aRes,
                    item;

                selectedListItemId = tr.getAttribute('data-uuid');

                if(evt.shiftKey) {
                    for (var i = 0; i < This.selection.array.length; i++) {
                        elm = This.selection.array[i];
                        if (elm.rowIndex < low) low = elm.rowIndex;
                        if (elm.rowIndex > high) high = elm.rowIndex;
                    }

                    if (tr.rowIndex>high) {
                        tbody = tr.parentNode.parentNode;
                        for (var i = tr.rowIndex; i > high; i--) {
                            elm = tbody.rows[i];
                            $(elm).addClass('ex-selected');
                            This.addToSelection(elm.getAttribute('contextitemname'), elm.getAttribute('tag'), elm.getAttribute('name'), elm);
                        }
                    } else if (tr.rowIndex<low) {
                        tbody = tr.parentNode.parentNode;
                        for (var i = tr.rowIndex; i < low; i++) {
                            elm = tbody.rows[i];
                            $(elm).addClass('ex-selected');
                            This.addToSelection(elm.getAttribute('contextitemname'), elm.getAttribute('tag'), elm.getAttribute('name'), elm);
                        }
                    }
                } else if(evt.ctrlKey) {
                    if ($(tr).hasClass('ex-selected')){
                        $(tr).removeClass('ex-selected');
                        This.removeFromSelection(tr);
                    }
                    else {
                        $(evt.currentTarget).addClass('ex-selected');
                        This.addToSelection(contextItemName, tag, name, tr);
                    }
                } else {
                    if (!$(evt.currentTarget).hasClass('ex-selected')) {
                        This.removeAllFromSelection();
                        $(evt.currentTarget).addClass('ex-selected');
                        This.addToSelection(contextItemName, tag, name, tr);
                    }
                    else bLoadCheck = false
                }

                if (bLoadCheck || isDetailsRefresh) {

                    aRes=[];
                    for(var i=0;i<This.selection.array.length;i++) {
                        tr = This.selection.array[i];
                        item = {};
                        item.contextItemName = tr.getAttribute('contextitemname');
                        item.tag = tr.getAttribute('tag');
                        item.name = tr.getAttribute('name');
                        aRes.push(item);
                    }
                    This.temporaryDetailLink.loadItemsDetail(aRes, isContentRepository);
                }

            });
            $(this.getInsertableDOM()).delegate('TR','mouseover', function (evt){
                $(evt.currentTarget).addClass('ex-highlight');
            });
            $(this.getInsertableDOM()).delegate('TR','mouseout', function (evt){
                $(evt.currentTarget).removeClass('ex-highlight');
            });

        }

    });


//  ----------------------------------------------------------------
    var EPDV_Detail = ExplPanelDomView.extend(function (app, parent){
//  ----------------------------------------------------------------
        ExplPanelDomView.call(this, app, parent);
        this.currentTab = '.bd-tab1';
        this.aItems = [];
        this.item = null;
        this.app = app;
    },{
        panelString :  function (h,b) {
            return '<div class="ex-panel-b ex-details">'+
                '<div class="bd-columnHeader bd-tabLabels">'+
                '   <div class="bd-tabLabel bd-tab1 bd-activeTab" for=".bd-tab1">Details</div>'+
                '   <div class="bd-tabLabel bd-tab2" for=".bd-tab2">Properties</div>'+
                '   <div class="bd-tabLabel bd-tab3" for=".bd-tab3">Permissions</div>'+
                '   <div class="bd-tabLabel bd-tab4" for=".bd-tab4">Inheritance</div>'+
                '   <div class="bd-tabLabel bd-tab5" for=".bd-tab5">Tags</div>'+
                '</div>'+
                '<div class="bd-tabs">'+
                '   <div class="bd-tab bd-tab1 bd-activeTab"></div>'+
                '   <div class="bd-tab bd-tab2"></div>'+
                '   <div class="bd-tab bd-tab3"></div>'+
                '   <div class="bd-tab bd-tab4"></div>'+
                '   <div class="bd-tab bd-tab5"></div>'+
                '</div>';
            '</div>';
        },


        refreshTabs : function () {
            var item = this.aItems[0];

            if (this.currentTab == '.bd-tab3') {
                if (item) {
                    if(item.tag === 'feature') {
                        this.showRights({});
                    } else {
                        this.showRights(item);
                    }
                } else {
                    this.rightsImplShow({},{});
                }
            } else if (this.currentTab == '.bd-tab4') {
                if (item) {
                    this.drawInheritance(item);
                }
            }
        },

        refreshTableAndTree: function (newItem) {

            if(newItem.tag === 'contentRepository') {

                var aMinus, aPlus,
                contentRepoSelector = 'li[mode="' + newItem.tag + '"][contextItemname="'+newItem.contextItemName+'"]';

                aMinus = $(contentRepoSelector).find('span[class="bc-tree-minus"]');
                aPlus = $(contentRepoSelector).find('span[class="bc-tree-plus"]');

                if(aMinus) {
                    aMinus.click();
                    aPlus = $(contentRepoSelector).find('span[class="bc-tree-plus"]');
                    if(aPlus) {
                        aPlus.click();
                    }
                } else {
                    if(aPlus) {
                        aPlus.click();
                        aMinus = $(contentRepoSelector).find('span[class="bc-tree-minus"]');
                        if(aMinus) {
                            aMinus.click();
                        }
                    }
                }
                // reload content in table
                this.app.panelNorth.loadContent(newItem.contextItemName, newItem.tag, newItem.name);
            }
        },

        buildDOM : function () {
            var This = this;

            this.dom = domBuilder(this.panelString('Inspecting',''));

            $(this.dom).delegate('.bd-tabLabel', 'click', function() {
                var target = this.getAttribute('for'),
                    $commonContainer;
                if (target) {
                    $commonContainer = $(this).parents(':has(.bd-tabs)');
                    if ($commonContainer.length) {
                        $('.bd-tabLabel, .bd-tab', $commonContainer[0]).removeClass('bd-activeTab');
                        $(target, $commonContainer[0]).addClass('bd-activeTab');
                        $(this).addClass('bd-activeTab');
                        This.currentTab = target;
                        This.refreshTabs();
                    };
                }
            });
            $(this.dom).delegate('.bd-saveButton', 'click', function() {
                var newItem;

                switch (This.currentTab) {
                    case '.bd-tab1':
                        newItem = This.getItemFromForm();
                        This._saveItemsDetail(newItem, This.aItems.slice(0), function () {
                            notify('Details have been saved' );
                            This.loadItemsDetail(This.aItems, This.aItems.slice(0)[0].tag === 'contentRepository');
                            This.refreshTableAndTree(newItem);
                        });
                        break;
                    case '.bd-tab2':
                        newItem = This.getItemFromForm();
                        This._saveItemsProperties(newItem, This.aItems.slice(0), function () {
                            notify('Properties have been saved' );
                            This.loadItemsDetail(This.aItems, This.aItems.slice(0)[0].tag === 'contentRepository');
                        });
                        break;
                    case '.bd-tab3':
                        newItem = This.getItemFromForm();
                        This._saveItemsProperties(newItem, This.aItems.slice(0), function () {
                            notify('Permissions have been saved' );
                            This.loadItemsDetail(This.aItems, This.aItems.slice(0)[0].tag === 'contentRepository');
                        });

                        break;
                    case '.bd-tab4':
                        break;
                    case '.bd-tab5':
                        newItem = This.getItemFromForm();
                        newItem.tags = This.getTagsDeltaModel(newItem, This.aItems.slice(0)[0]);
                        This._saveItemsTags(newItem, function () {
                            notify('Tags have been saved' );
                            This.loadItemsDetail(This.aItems, This.aItems.slice(0)[0].tag === 'contentRepository');
                        });
                        break;
                }
            });
            $(this.dom).delegate('.bd-cancelButton', 'click', function() {

                // tag is needed to make rest call to correct url
                // so it is fetched from the loaded item in the form
                // because on cancel all item details are reloaded
                var f = $('form',this.dom),
                    fDetails = f[0];

                This.loadItemsDetail(This.aItems, fDetails['x-tag'].value === 'contentRepository');
            });

            $(this.dom).delegate('.bd-refreshButton', 'click', function() {
                isDetailsRefresh = true;
                $('li').find("[data-uuid=" + selectedItemId + "]").click();
                $('.bcj-body .ex-list-table tbody tr[data-uuid="' + selectedListItemId + '"]').mousedown();
            });

            $(this.dom).delegate('.ex-if-input', 'change', function(evt) {
                $(evt.currentTarget).addClass('ex-if-input-changed');
            });

        },
        getTagsDeltaModel : function (newItems, oldItems) {
            var blacklistTags = '',
                endResult = [],
                test,
                l, n, item;

            for (l = oldItems.tags.length; l--; ) { // convert old tags to format: "text,value;..."
                item = oldItems.tags[l];
                blacklistTags += item.text + ',' + item.type + ';';
            }

            for (n = 0, l = newItems.tags.length; n < l; n++) {
                item = newItems.tags[n];
                test = blacklistTags; // for checking before and after (next line)
                blacklistTags = blacklistTags.replace(item.text + ',' + item.type + ';', '');
                if (test === blacklistTags) { // add only if not existing (we do delta here)
                    endResult.push(item);
                }
            }

            blacklistTags = blacklistTags.split(';');
            for (l = blacklistTags.length; l--; ) { // walk through left over blacklist to mark for deletion
                item = blacklistTags[l].split(',');
                if (item[0]) endResult.push({
                    text: item[0],
                    type: item[1],
                    blacklist: true // this means: Sever, please delete me!
                });
            }
            return endResult;
        },
        getItemFromForm : function () {
            var f = $('form',this.dom),
                fDetail = f[0],
                fProperties = f[1],
                fRights = f[2],
                fTags = f[3] || f[2],
                props,
                mngbl,
                p,
                tag,
                item = {};

            item.tag = fDetail['x-tag'].value;
            item.name = fDetail['x-name'].value;
            item.contextItemName = fDetail['x-contextItemName'].value;
            item.parentItemName = fDetail['x-parentItemName'].value;
            item.extendedItemName = fDetail['x-extendedItemName'].value;
            item.securityProfile = fDetail['x-securityProfile'].value;

            item.properties = {};
            for (var n in this.item.properties) {
                props = this.item.properties[n];
                mngbl = fProperties['m-'+props.name];
                p = {
                    'name':props.name,
                    'value':fProperties['v-'+props.name].value,
                    'label':fProperties['l-'+props.name].value,
                    'type':fProperties['t-'+props.name].value,
                    'manageable':mngbl ? mngbl.value : '',
                    'viewHint':fProperties['vh-'+props.name].value
                };

                if ((p.value != props.value) || (p.label != props.label) || (p.type != props.type) || (p.viewHint != props.viewHint) || (p.manageable != props.manageable)) {
                    item.properties[p.name] = p;
                }
            }
            item.tags = [];

            for (i = 0, l = this.item.tags.length; i < l; i++){
                // check if that tag is found in the form if not use the new one set from the tags array
                var tagText = fTags['x-tags-'+this.item.tags[i].text+'-text']
                                    ? fTags['x-tags-'+this.item.tags[i].text+'-text'].value : this.item.tags[i].text,
                    tagType = fTags['x-tags-'+this.item.tags[i].text+'-type']
                                    ? fTags['x-tags-'+this.item.tags[i].text+'-type'].value : this.item.tags[i].type;

                tag = {
                    text: tagText,
                    type: tagType
                };
                item.tags.push(tag);
            }

            return item;

            var orgName = this.json.name;
            data.orgName = this.json.name;

        },
        getInsertableDOM : function () {
            return $('.ex-panel', this.dom)[0];
        },
        readyDOM : function () {
            var This = this,
                s = portalServer.serverURL+'groups.xml?ps=-1';// Get Groups

            jQueryRestAjax(s,null,'GET',function (x){
                var data = genericXML2JSON(x),
                    aGroups = [],
                    g,
                    o;
                for (var i =0;i<data.groups[0].group.length;i++) {
                    g = data.groups[0].group[i];
                    o = {
                        id:g.id[0],
                        name:g.name[0],
                        role:g.role[0]
                    }
                    if (g.description)
                        o.description = g.description[0];
                    aGroups.push(o);
                }
                This.aGroups = aGroups;
            });

        },

        loadItemsDetail : function (aItems, isContentRepository) {
            var This = this;
            if (aItems.length) {
                this._loadItems(aItems, [], [],function (aLoadedItems) {

                    This.aItems = aLoadedItems;
                    This._mergeItems(aLoadedItems);

                    This.drawDetails(This.item);
                    This.drawProperties(This.item);
                    This.drawTags(This.item);
                    This.refreshTabs();

                }, isContentRepository)
            }
            else {
                this.aItems = [];
                $('.bd-tabs > .bd-tab', this.dom).html('');
            }

        },

        _mergeItems : function (aItems) {

            var item = {},
                mItem = aItems[0];
            // simple clone... danger!
            for (var n in mItem) {
                if (n == 'properties') {
                    item.properties = {};
                    for (var m in mItem.properties) {
                        item.properties[m] = {};
                        item.properties[m].name = mItem.properties[m].name;
                        item.properties[m].value = mItem.properties[m].value;
                        item.properties[m].type = mItem.properties[m].type;
                        item.properties[m].label = mItem.properties[m].label;
                        item.properties[m].viewHint = mItem.properties[m].viewHint;
                        item.properties[m].manageable = mItem.properties[m].manageable;
                        item.properties[m].itemName = mItem.properties[m].itemName;
                    }
                } else {
                    item[n] = mItem[n];
                }
            }


            for (var i = 1; i < this.aItems.length; i++) {
                mItem = this.aItems[i];
                for (var n in mItem) {
                    if (n == 'properties') {
                        for (var m in mItem.properties) {
                            if(!item.properties[m])
                                item.properties[m] = {};
                            if (item.properties[m].value != mItem.properties[m].value)
                                item.properties[m].value = '***';
                            if (item.properties[m].type != mItem.properties[m].type)
                                item.properties[m].type = '***';
                            if (item.properties[m].label != mItem.properties[m].label)
                                item.properties[m].label = '***';
                            if (item.properties[m].viewHint != mItem.properties[m].viewHint)
                                item.properties[m].viewHint = '***';
                            if (item.properties[m].manageable != mItem.properties[m].manageable)
                                item.properties[m].manageable = '***';
                            if (item.properties[m].itemName != mItem.properties[m].itemName)
                                item.properties[m].itemName = '***';
                        }
                    } else if (n === 'tags') {
                        for (var t in mItem.tags) {
                            if(!item.tags[t])
                                item.tags[t] = {};
                            if (item.tags[t].text != mItem.tags[t].text) {
                                item.tags[t].text = '***';
                                item.tags[t].type = '***';
                            }
                            if (item.tags[t].type != mItem.tags[t].type)
                                item.tags[t].type = '***';
                        }
                    } else if (item[n] != mItem[n]) {
                        item[n] = '***';
                    }
                }
            }
            this.item = item;
        },
        _loadItems : function (aItems, aLoadedItems, aLoadedXML, callback, isContentRepository) {

            var This = this,
                item = aItems.shift();
            if (item) {
                portalServer.loadItem(portalServer.itemObject(item.contextItemName, item.tag, item.name),
                    function (item, xml) {
                        // loading of items in right list
                        var json = portalServer.itemXMLDOC2JSON(xml);

                        aLoadedItems.push(json);
                        aLoadedXML.push(xml);
                        if (aItems.length)
                            This._loadItems(aItems, aLoadedItems, aLoadedXML, callback)
                        else
                            callback(aLoadedItems, aLoadedXML);
                }, false, 1000, null, isContentRepository);
            }
        },


        //  ----------------------------------------------------------------
        //  Details
        //  ----------------------------------------------------------------

        drawDetails: function (json) {
            if (!json.name) {
                return;
            }
            var s = '',
                jsonTag = json.tag === 'contentRepository' ? 'contentrepositorie': json.tag,
                encodedContextItemName = encodeURIComponent(json.contextItemName);

            s += '<div class="bc-bar">';
            s += '<div class="ex-panel-h-label label-details ex-panel-h-south-label">';

            s += getIcon(json) + ' ';
            s += json.name;
            s += json.name && json.contextItemName !== '[BBHOST]' ?
                '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+jsonTag+'s/'+json.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' :

                    json.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+json.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' : '';

            s += json.name && json.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+jsonTag+'s/'+json.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>':

                    json.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+json.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>' : '';


            s += ' </div>';
            s += '  <button class="bd-refreshButton bd-button bd-button-text refresh-btn-details " type="button">Refresh</button>';
            s += '  <button class="bd-cancelButton bd-button bd-button-text cancel-btn-details " type="button">Cancel</button>';
            s += '  <button class="bd-saveButton bd-button bd-green bd-roundcorner-3 save-btn-details" type="submit">Save</button>';
            s += '</div>';
            s += '<form class="ex-details-form">';
            s += '<input type="hidden" name="x-tag" value="'+json.tag+'" />';
            s += '<table class="ex-list-table">';
            s += '<thead><tr class="ex-list-tr-head">';
            s += '<td class="ex-list-th" style="width:260px;"><span class="ex-td-label">Property</span></td>';
            s += '<td class="ex-list-th"><span class="ex-td-label">Label</span></td>';
            s += '</tr></thead>';
            s += '<tbody>';
            s += '<tr><td class="ex-list-td"><span class="ex-td-label">Name</span></td><td class="ex-list-td"><input class="ex-if-input" type="text" name="x-name" value="'+json.name+'"/></td></tr>';
            s += '<tr><td class="ex-list-td"><span class="ex-td-label">Context</span></td><td class="ex-list-td"><input class="ex-if-input" type="text" name="x-contextItemName" value="'+json.contextItemName+'"/></td></tr>';
            s += '<tr><td class="ex-list-td" XXXclass="js-tooltip" tooltipText="Click to inspect." style="cursor:pointer;" onclick="window.exact.showItem(\''+json.tag+'\', \''+json.extendedItemName+'\', \''+json.contextItemName+'\')"><span class="ex-td-label">Extend</span></td><td class="ex-list-td"><input class="ex-if-input" type="text" name="x-extendedItemName" value="'+json.extendedItemName+'"/></td></tr>';
            s += '<tr><td class="ex-list-td"><span class="ex-td-label">Parent</span></td><td class="ex-list-td"><input class="ex-if-input" type="text" name="x-parentItemName" value="'+json.parentItemName+'"/></td></tr>';
            s += '<tr><td class="ex-list-td"><span class="ex-td-label">SecurityProfile</span></td><td class="ex-list-td"><input class="ex-if-input" type="text" name="x-securityProfile" value="'+json.securityProfile+'"/></td></tr>';
            s += '</tbody></table>';
            s += '</form>';
            $('.bd-tabs > .bd-tab1', this.dom)[0].innerHTML = s;
        },

        _saveItemsDetail : function (newItem, aItems, callback) {
            var This = this,
                orgItem = aItems.shift(),
                item = {};

            item.name = orgItem.name;
            if (newItem.name != '***' && newItem.name != orgItem.name) {    // Rename
                item.name = newItem.name;
                item.orgName = orgItem.name;
                orgItem.name = newItem.name;

                var $tr = $('.bcj-body .ex-list-table tbody tr[name="' + item.orgName + '"]');

                $tr.attr('name', item.name);
                $tr.find('td.ex-list-td-catalog-name span').text(item.name);
            }

            item.tag = orgItem.tag;
            item.contextItemName = orgItem.contextItemName;
            item.parentItemName = orgItem.parentItemName;
            if (newItem.contextItemName!='***' && newItem.contextItemName != orgItem.contextItemName) item.contextItemName = newItem.contextItemName;
            if (newItem.extendedItemName!='***' && newItem.extendedItemName != orgItem.extendedItemName) item.extendedItemName = newItem.extendedItemName;
            if (newItem.parentItemName!='***' && newItem.parentItemName != orgItem.parentItemName) item.parentItemName = newItem.parentItemName;
            if (newItem.securityProfile!='***' && newItem.securityProfile != orgItem.securityProfile) item.securityProfile = newItem.securityProfile;

            portalServer.updateItem(item, function (item, xml) {
                if (aItems.length)
                    This._saveItemsDetail(newItem, aItems, callback)
                else
                    callback();
            });
        },

        drawTags: function (json) {
            var s = '',
                jsonTag = ((json.tag === 'contentRepository') || (json.tag === 'contentRepositories')) ? 'contentrepositorie': json.tag,
                encodedContextItemName = encodeURIComponent(json.contextItemName);

            s += '<div class="bc-bar">';
            s += '<div class="ex-panel-h-label ex-label-Tag ex-panel-h-south-label">';

            s += getIcon(json) + ' ';
            s += json.name;
            s += json.name && json.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+jsonTag+'s/'+json.name+'"target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' :
                    json.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+json.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' : '';

            s += json.name && json.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+jsonTag+'s/'+json.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>':
                    json.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+json.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>' : '';

            s +='</div>';
            s += '  <button class="bd-refreshButton bd-button bd-button-text refresh-btn-details " type="button">Refresh</button>';
            s += '  <button class="bd-cancelButton bd-button bd-button-text cancel-btn-details" type="button">Cancel</button>';
            s += '  <button class="bd-saveButton bd-button bd-green bd-roundcorner-3 save-btn-details" type="submit">Save</button>';
            s += '<a class="ex-act bd-button bd-grey bd-roundcorner-3 addTag" href="" onclick="return false" action="addTag">';
            s += '  <span class="">';
            s += '    <span class="bi bi-s bi-pm-plus"></span>';
            s += '  </span>';
            s += '</a>';
            s += '</div>';
            s += '<form class="ex-addTag-form">';
            s += '<input type="hidden" name="x-tag" value="'+json.tag+'" />';
            s += '<table class="ex-list-table">';
            s += '<thead><tr class="ex-list-tr-head">';
            s += '<td class="ex-list-th"><span class="ex-td-label">Tag</span></td>';
            s += '<td class="ex-list-th"><span class="ex-td-label">Type</span></td>';
            s += '<td class="ex-list-th" style="width: 30px;"></td>';
            s += '</tr></thead>';
            s += '<tbody>';



            if(json && json.tags){

                if($.isArray(json.tags)) {
                    if (typeof json.tags.sort === 'function'){
                        json.tags = json.tags.sort(function(a,b){
                            return a.text > b.text ? 1 : a.text < b.text ? -1 : 0;
                        });
                    }

                    for (var n = 0, l = json.tags.length; n < l; n++) {
                        s += '<tr>';
                        s += '<td class="ex-list-td"><input class="ex-if-input" type="text" name="x-tags-'+json.tags[n].text+'-text" value="'+json.tags[n].text+'"/></td>';
                        s += '<td class="ex-list-td"><input class="ex-if-input" type="text" name="x-tags-'+json.tags[n].text+'-type" value="'+json.tags[n].type+'"/></td>';
                        s += '<td class="ex-list-td" style="padding-top:5px">';
                        s += '<a class="ex-act bd-button bd-grey bd-roundcorner-3 ex-trash-button" href="" onclick="return false" action="deleteTag" actionData="'+ (json.tags[n].text + ',' + json.tags[n].type)+'">';
                        s += '<span class="bi bi-s bi-pm-trash">&nbsp;</span>';
                        s += '</a>';
                        s += '</td>';
                        s += '</tr>';
                    }
                }
            }
            s += '</tbody></table>';
            s += '</form>';

            $('.bd-tabs > .bd-tab5', this.dom)[0].innerHTML = s;
        },

        addTag : function () {
            var This = this;
            var content =
                '<li class="bc-g" style="margin:10px">' +
                '<div class="bc-1-3">' +
                '<p class="bc-i bc-label">Tag name</p>' +
                '</div>' +
                '<div class="bc-2-3 ex-addtag-dialogue">' +
                '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                '</div>' +
                '<div class="bc-1-3">' +
                '<p class="bc-i bc-label">Tag type</p>' +
                '</div>' +
                '<div class="bc-2-3 ex-addtag-dialogue">' +
                '<p class="bc-i"><input class="bc-input bc-roundcorner-3 required" style="width:275px" value="regular" type="text" data-cid="bc-textbox-modalname-uid" /></p>' +
                '</div>' +
                '</li>';

            var $form = bc.component.modalform({
                width: '450px',
                title: 'Create Tag',
                content: content,
                uuid: '1928823465',
                ok: 'Create',
                cancel: 'Cancel',
                okCallback: function(theForm) {
                    var tagName = theForm[0][0].value,
                        tagType = theForm[0][1].value;

                    if($('.ex-addTag-form input[name="x-tags-'+tagName+'-text"]').length) {
                        notify('Tag with name '+tagName+' already exists.', true);
                        return false;
                    }

                    if (tagName && tagType) {
                        for (var i=0;i<This.aItems.length;i++) {
                            var item = {
                                contextItemName:This.aItems[i].contextItemName,
                                tag:This.aItems[i].tag,
                                name:This.aItems[i].name,
                                tags : [{ // we send delta, so only one here
                                    text:tagName,
                                    type:tagType
                                }]
                            };

                            portalServer.updateItem(item, function(){
                                notify('Tag has been added' );
                                This.loadItemsDetail(This.aItems, item.tag === 'contentRepository');
                            });
                        }
                    }
                }
            });
        },

        deleteTag : function (tagAndType) {
            var tag,
                item,
                This = this;

            for (var i = 0; i < this.aItems.length; i++) {
                tag = tagAndType.split(',');
                item = {
                    contextItemName:this.aItems[i].contextItemName,
                    tag:this.aItems[i].tag,
                    name:this.aItems[i].name,
                    tags: [{ // we send delta, so only one here
                        text: tag[0],
                        type: tag[1],
                        blacklist: true
                    }]
                };

                portalServer.updateItem(item, function(){
                    notify('Tag has been deleted' );
                    This.loadItemsDetail(This.aItems, item.tag === 'contentRepository');
                });
            }
        },

        _saveItemsTags: function (newItem, callback) {
            portalServer.updateItem(newItem, callback);
        },


        //  ----------------------------------------------------------------
        //  Properties
        //  ----------------------------------------------------------------

        drawProperties: function (json) {

            if (!json.name) {
                return;
            }
            var s, n, props,
                validValue = function(val){
                    return (val && val != "undefined" ? val : "");
                },
                encodedContextItemName = encodeURIComponent(json.contextItemName);

            json.propertiesArray = [];

            for (n in json.properties) {
                props = json.properties[n];
                props.actionAllowed = true;
                if (props.itemName != '***' && json.name != props.itemName) {
                    props.className = 'ex-tr-inherited';
                    props.inherited = true; //add
                    props.actionAllowed = false;
                    props.value = validValue(props.value);
                }
                json.propertiesArray.push(props);
                json.showManageable = props.manageable !== null;
            }

            json.icon0 = getIcon(json) + ' ';
            jsonTag = json.tag === 'contentRepository' ? 'contentrepositorie': json.tag;

            json.link = json.name && json.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+jsonTag+'s/'+json.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:5px"></a>' :
                    json.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+json.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:5px"></a>' : '';

            json.xml = json.name && json.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+jsonTag+'s/'+json.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:6px"></a>':
                    json.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+json.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:6px"></a>' : '';

            s = view.applyJSON('props', json);

            jQuery('.bd-tabs > .bd-tab2', this.dom).html(s);
        },

        _saveItemsProperties : function (newItem, aItems, callback) {
            var This = this,
                orgItem = aItems.shift(),
                item = {},
                p,
                pNew,
                pOrg,
                inherited,
                updated;

            item.name = orgItem.name;
            item.tag = orgItem.tag;
            item.contextItemName = orgItem.contextItemName;
            item.parentItemName = orgItem.parentItemName;

            item.properties = {};

            for (var n in newItem.properties) {
                p = {'name':n};
                pNew = newItem.properties[n];
                pOrg = orgItem.properties[n];
                pOrg.manageable = pOrg.manageable || '';
                inherited = pOrg.itemName != '***' && pOrg.itemName != item.name;
                updated = false;
                if (pOrg) {

                    if(pNew.manageable!=pOrg.manageable.toString() || pNew.label!=pOrg.label ||
                        pNew.value!=(typeof pOrg.value === 'undefined' ? '' : pOrg.value).toString() || pNew.type!=pOrg.type || pNew.viewHint!=pOrg.viewHint){
                        updated = true;

                        (pNew.label == '***')? p.label=pOrg.label : p.label=pNew.label;
                        (pNew.value == '***')? p.value=pOrg.value : p.value=pNew.value;
                        (pNew.type == '***')? p.type=pOrg.type : p.type=pNew.type;
                        (pNew.manageable == '***')? p.manageable=pOrg.manageable : p.manageable=pNew.manageable;
                        (pNew.viewHint == '***')? p.viewHint=pOrg.viewHint : p.viewHint=pNew.viewHint;

                        if (!p.type) {
                            if (p.value == pOrg.value) delete p.value;
                        }
                    }

                }
                // don't try to update inherited props that are not manageable
                if(!pOrg || (!(inherited && pOrg.manageable === false))){
                    if(updated) item.properties[n] = p;
                }
            }

            portalServer.updateItem(item, function (item, xml) {
                if (aItems.length)
                    This._saveItemsProperties(newItem, aItems, callback)
                else
                    callback();
            });
        },

        //  ----------------------------------------------------------------
        //  Rights
        //  ----------------------------------------------------------------
        drawRights : function () {

        },

        showRights : function (item) {
            var This = this,
                isContentRepository = This.aItems.slice(0)[0].tag === 'contentRepository';

            // feature item has no permissons
            if(This.aItems.slice(0)[0].tag === 'feature') {
                $('.bd-tabs > .bd-tab3', this.dom)[0].innerHTML = '';
                return;
            }

            portalServer.loadItem(portalServer.itemObject(item.contextItemName, item.tag, item.name), function (item, xml) {
                var item = portalServer.itemXMLDOC2JSON(xml),
                    url = portalServer.serverURL,
                    portalName = (item.tag == 'portal') ? item.name : item.contextItemName,
                    elm,
                    s;

                    url = (item.tag === 'template') ? url.substring(0, url.length - 1) : url + 'portals/' + portalName;

                    if(item.tag !== 'portal' && item.tag !=='contentRepository') {
                        url += '/' + item.tag + 's/' + item.name;
                    }

                    // construct url to get portal repository rights
                    if(item.tag === 'contentRepository') {
                        url += '/contentrepositories/' + item.name;
                    }

                    // construct url to get contenttype rights
                    if(item.tag === 'contenttypes') {
                        url += '/contenttypes/' + item.name;
                    }

                    jQueryRestAjax(url+'/rights.xml',null,'GET',function (r){

                        if(r) {

                            var data = genericXML2JSON(r),
                                right = {},
                                r,
                                s;

                            for (var i=0;i<data.rights[0].itemRight.length;i++) {
                                r = data.rights[0].itemRight[i];
                                s = r.sid[0];
                                if (s.indexOf('group')==0) {    // strip 'group_'
                                    s = s.substring(6);
                                    if(!right.group)right.group = {};
                                    if(!right.group[s])right.group[s] = {properties:{}};
                                    right.group[s].securityProfile = r.securityProfile[0]
                                }
                                else {  // strip 'role_'
                                    s = s.substring(5);
                                    if(!right.role)right.role = {};
                                    right.role = s;
                                }
                            }

                            //Process rights on properties
                            if (data.rights[0].propertyRight) {
                                for (var i=0;i<data.rights[0].propertyRight.length;i++) {
                                    r = data.rights[0].propertyRight[i];
                                    s = r.sid[0];
                                    if (s.indexOf('group')==0) {   // strip 'group_'
                                        s = s.substring(6);
                                        if(!right.group)right.group = {};
                                        if(!right.group[s])right.group[s] = {properties:{}};
                                        right.group[s].properties[r.name] = r.securityProfile[0]
                                    }
                                    else { // strip 'role_'
                                        s = s.substring(5);
                                        if(!right.role)right.role = {};
                                        right.role = s;
                                    }
                                }
                            }

                            This.rightsImplShow(item, right);
                        } else {
                            elm = $('.bd-tabs > .bd-tab3', this.dom)[0],
                            itemTag = item.tag === 'contentRepository' ? 'contentrepositorie': item.tag;
                            var encodedContextItemName = encodeURIComponent(item.contextItemName);

                            s = '';
                            s += '<div class="bc-bar">';
                            s += '<div class="ex-panel-h-label rights-label-permissions ex-panel-h-south-label">';
                            s += getIcon(itemTag) + ' ';
                            s += item.name;
                            s += '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'"target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>';
                            s += '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>';
                            s += ' </div>';
                            s += '</div>';
                            s += '<div class="ex-properties-form" style="padding-left:8px">No permissions for selected item</div>'
                            elm.innerHTML = s;
                        }
                    });

            }, false, 1000, null, isContentRepository);

        },

        rights: {
            'NONE': 'None',
            'ADMIN': 'Administrator',
            'CREATOR': 'Creator',
            'COLLABORATOR': 'Collaborator',
            'CONTRIBUTOR': 'Contributor',
            'CONSUMER': 'Consumer'
        },
        rightsFullName: {
            'None': 'NONE',
            'Administrator': 'ADMIN',
            'Creator': 'CREATOR',
            'Collaborator': 'COLLABORATOR',
            'Contributor': 'CONTRIBUTOR',
            'Consumer': 'CONSUMER'
        },

        rightsImplShow: function(item, right) {

            if (!item.name) {
                return;
            }
            var This = this,
                contextItemName = item.contextItemName,
                tag = item.tag,
                name = item.name,
                inherited,
                s = '',
                n,
                p,
                sp,
                elm,
                encodedContextItemName = encodeURIComponent(item.contextItemName);

            s += '<div class="bc-bar">';
            s += '<div class="ex-panel-h-label rights-label-permissions ex-panel-h-south-label">';
            itemTag = item.tag === 'contentRepository' ? 'contentrepositorie': item.tag;

            s += getIcon(item) + ' ';
            s += item.name;
            s += item.name && item.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'"target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' :
                    item.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+item.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' : '';

            s +=  item.name && item.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>':
                    item.tag === 'portal' ?
                '<a href="'+portalServer.serverURL+'portals/'+item.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>' : '';

            s += ' </div>';
            s += '<div form="form1">';
            s += '  <button class="bd-refreshButton bd-button bd-button-text refresh-btn-details " type="button">Refresh</button>';
            s += '  <button class="bd-cancelButton bd-button bd-button-text cancel-btn-details" type="button form="form1"">Cancel</button>';
            s += '  <button class="bd-saveButton bd-button bd-green bd-roundcorner-3 save-btn-details" type="submit" form="form1">Save</button>';
            s += '</div>';
            s += '</div>'
            s += '<form class="ex-permissions-form" id="form1">';
            s += '<table class="ex-list-table"><tbody>';
            for(var i = 0; i < This.aGroups.length; i++) {
                s += '<tr class="ex-list-tr"><td class="ex-list-td" style="width:120px; padding-left:14px">';
                n = This.aGroups[i].name;
                s += n+'</td><td class="ex-list-td ex-list-td-a" style="width:360px;">';
                s += This.renderSelect(This.aGroups[i].name,(right && right.group && right.group[n])? right.group[n].securityProfile:null);

                s += '<table class="ex-list-table"><tbody>';
                for(var m in item.properties) {
                    p = item.properties[m];
                    inherited = (p.itemName != '***' && name != p.itemName);

                    s += '<tr class="ex-list-tr' + (inherited ? 'ex-tr-inherited' : '')+ '"><td class="ex-list-td">';
                    s += p.name+'</td><td class="ex-list-td">';
                    sp = '';
                    if (right.group[n] && right.group[n].properties[p.name])
                        sp = right.group[n].properties[p.name];

                    s += inherited ? 'inherited property' : This.renderSelect(n, sp, p.name);
                    s += '</td></tr>';
                }
                s += '</tbody></table>';
                s += '</td><td class="ex-list-td" ></td></tr>';
            }

            s += '<tr><td><br/></td><td></td></tr>';
            s += '</tbody></table>';
            s += '</form>';

            elm = $('.bd-tabs > .bd-tab3', this.dom)[0];
            elm.innerHTML = s;

            $('FORM',elm).bind('submit',function (ev) {
                var r, sel, sp, pn, s, url;
                if (ev.originalEvent && ev.originalEvent.explicitOriginalTarget && ev.originalEvent.explicitOriginalTarget.value == 'Cancel') {
                    return false;
                }

                r = [];
                for (var i=0;i<this.elements.length;i++) {
                    sel = this.elements[i];
                    if(sel.name) {
                        sp = sel.options[sel.selectedIndex];
                        pn = sel.getAttribute('propertyName');

                        if (sp.value != 'None' || !pn)
                            r.push({'sid':'group_'+sel.name,'securityProfile':sp.value,'propertyName':pn});
                    }
                }

                s = '<?xml version="1.0" encoding="utf-8"?>';
                s += '<rights>';
                for (var i = 0, len = r.length; i < len; i++) {
                    if (!r[i].propertyName) {
                        s += '<itemRight>';
                        s += '<securityProfile>'+This.rightsFullName[r[i].securityProfile]+'</securityProfile>';
                        s += '<sid>'+r[i].sid+'</sid>';
                        s += '</itemRight>';
                    }
                }

                for (i = 0, len = r.length; i < len; i++) {
                    if (r[i].propertyName) {
                        s += '<propertyRight name="'+r[i].propertyName+'">';
                        s += '<securityProfile>'+This.rightsFullName[r[i].securityProfile]+'</securityProfile>';
                        s += '<sid>'+r[i].sid+'</sid>';
                        s += '</propertyRight>';
                    }
                }

                s += '</rights>';

                url = portalServer.serverURL+'portals/'+name+'';

                if(item.tag == 'portal') {
                    url = portalServer.serverURL+'portals/'+name+'';
                }
                else{
                    if(item.tag == 'contentRepository') {
                        url = portalServer.serverURL+'portals/'+contextItemName+'/contentrepositories/'+name+'';
                    } else {
                        url = portalServer.serverURL+'portals/'+contextItemName+'/'+tag+'s/'+name+'';
                    }
                }
                jQueryRestAjax(url+'/rights.xml',s,'PUT',function (r){
                });

                return false;
            });
        },

        renderSelect : function (name, r ,sProp) {
            var s = '';
            s += '<select name="'+name+'"';
            if(sProp)
                s += ' propertyName="'+sProp+'"';
            s += '>';

            for(var n in this.rights) {
                s += '<option name="'+n+'" ';
                if (n==r)s += 'selected="selected"';
                s += '>'+this.rights[n]+'</option>';
            }
            s += '</select>';
            return s;
        },

        //  ----------------------------------------------------------------
        //  Inheritance
        //  ----------------------------------------------------------------
        drawInheritance : function (item) {
            var This = this;
            this._loadInheritance(item, [], function (aRes) {

                var s = '',
                    elm = $('.bd-tabs > .bd-tab4', this.dom)[0],
                    encodedContextItemName = encodeURIComponent(item.contextItemName);

                if(aRes[0].extendedItemName === '' || aRes[0].extendedItemName === undefined || item.extendedItemName === '') {

                    s += '<div class="bc-bar">';
                    s += '<div class="ex-panel-h-label rights-label-permissions ex-panel-h-south-label">';
                    s += getIcon(item) + ' ';
                    s += item.name;
                    if(item.contextItemName !== '[BBHOST]') {
                      s += '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'"target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>';
                      s += '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>';
                    }
                    s += ' </div>';
                    s += '<div>';
                    s += '  <button class="bd-refreshButton bd-button bd-button-text refresh-btn-details " type="button">Refresh</button>';
                    s += '</div>';
                    s += '</div>';
                    s += '<div class="ex-properties-form" style="padding-left:8px">No inheritance for selected item</div>'
                    elm.innerHTML = s;

                } else {

                    s += '<div class="bc-bar">';
                    s += '<div class="ex-panel-h-label ex-label-inheritance ex-panel-h-south-label">';
                    itemTag = item.tag === 'contentRepository' ? 'contentrepositorie': item.tag;

                    s += getIcon(item) + ' ';
                    s += item.name;
                    s += item.name && item.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' :
                            item.tag === 'portal' ?
                        '<a href="'+portalServer.serverURL+'portals/'+item.name+'" target="_blank" class="bd-icn bd-preview-link" style="margin-left:10px"></a>' : '';

                    s += item.name && item.contextItemName !== '[BBHOST]' ? '<a href="'+portalServer.serverURL+'portals/'+encodedContextItemName+'/'+itemTag+'s/'+item.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>':
                            item.tag === 'portal' ?
                        '<a href="'+portalServer.serverURL+'portals/'+item.name+'.xml" target="_blank" class="bd-icn bd-xml" style="margin-left:10px"></a>' : '';

                    s +=' </div>';
                    s += '<div>';
                    s += '  <button class="bd-refreshButton bd-button bd-button-text refresh-btn-details " type="button">Refresh</button>';
                    s += '</div>';
                    s += '</div>';
                    s += '<form class="ex-details-form">';
                    s += '<ul>';
                    for (var i = 0; i < aRes.length; i++) {
                        s += '<li style="padding-left:14px; padding-bottom:10px">'+aRes[i].extendedItemName+'</li>';
                    }
                    s += '</ul>';
                    s += '</form>';


                    elm = $('.bd-tabs > .bd-tab4', This.dom)[0];
                    elm.innerHTML = s;
                }
            })
        },

        _loadInheritance : function (item, aRes, callback) {
            var This = this;
            portalServer.loadItem(item, function (item, xml) {
                var json = portalServer.itemXMLDOC2JSON(xml);
                aRes.push(json);
                callback(aRes);
            })
        },

        addPreference : function () {
            var be = parent.be, bc = parent.bc, This = this;
            var content = be.utils.processHTMLTemplateByUrl(be.contextRoot + '/static/Explorer/create-preference.html');
            var $form = bc.component.modalform({
                width: '530px',
                title: 'Add property',
                content: content,
                ok: 'Save',
                cancel: 'Cancel',
                okCallback: function($form){

                    var name  = $('[name="preference-name"]', $form).val().trim(),
                        value = $('[name="preference-value"]', $form).val().trim(),
                        type  = $('[name="preference-type"]', $form).val().trim(),
                        label = $('[name="preference-label"]', $form).val().trim(),
                        manageable = $('[name="preference-manageable"]', $form).val().trim(),
                        viewHint = $('[name="preference-viewHint"]', $form).val().trim(),
                        item;

                    if($('.ex-properties-form input[name="l-'+name+'"]').length) {
                        notify(
                            'An Item with the given name: ' + name + ' already exists.',
                            true,
                            {
                                title: 'CONFLICT',
                                closeIcon: false
                            }
                        );
                        return false;
                    }

                    if(name && type) {

                        for (var i=0;i<This.aItems.length;i++) {
                            item = {
                                contextItemName:This.aItems[i].contextItemName,
                                tag:This.aItems[i].tag,
                                name:This.aItems[i].name,
                                properties : {
                                    n:{
                                        name:name,
                                        value:value,
                                        type:type,
                                        label:label,
                                        viewHint:viewHint,
                                        manageable:manageable,
                                        itemName:This.aItems[i].name
                                    }
                                }
                            };
                            item.OLDparentItemName = true;  // force NO catalog
                            portalServer.updateItem(item, function(){
                                This.loadItemsDetail(This.aItems, item.tag === 'contentRepository');
                                notify('Property has been added' );
                            });
                        }
                    }
                }
            });
            $form.find("li:last-child").css({"clear": "both"});
        },
        removePreference : function (rItem, propertyName)  {
            var This = this;

            var propObject = {
                name: rItem.name,
                contextItemName: rItem.contextItemName,
                tag: rItem.tag,
                OLDparentItemName : true,
                properties: []
            };

            propObject.properties[propertyName] = {
                name:propertyName,
                deleted:true
            };

            portalServer.updateItem(propObject, function() {
                notify('Property has been deleted' );
                This.loadItemsDetail(This.aItems, propObject.tag === 'contentRepository');
            });

        }
    });

    var genericXML2JSON = function(xml, dataFormatter) {
        // Create the return object
        var obj = {},
            attribute,
            item,
            nodeName,
            item2,
            nodeName2,
            childOjb;

        if (xml.nodeType == 1) { // element
            // do attributes
            if (xml.attributes.length > 0) {
                for (var j = 0; j < xml.attributes.length; j++) {
                    attribute = xml.attributes.item(j);
                    obj[attribute.nodeName] = attribute.nodeValue;
                }
            }
        }
        else if (xml.nodeType == 3) { // text
            obj = xml.nodeValue;
        }

        // do children
        if (xml.hasChildNodes()) {
            for(var i = 0; i < xml.childNodes.length; i++) {
                item = xml.childNodes.item(i);
                nodeName = item.nodeName;
                if(nodeName == "#text") {
                    obj = item.nodeValue;
                    break;
                }
                else if(nodeName == "properties") {
                    obj["properties"] = {};
                    for(var p = 0; p < item.childNodes.length; p++) {
                        item2 = item.childNodes.item(p);
                        nodeName2 = item2.getAttribute("name");
                        childOjb =  genericXML2JSON(item2, dataFormatter);
                        obj["properties"][nodeName2] = childOjb;
                    }
                    if(dataFormatter != null)
                        dataFormatter(obj);
                } else {
                    if (!obj[nodeName]) obj[nodeName] = [];
                    obj[nodeName].push(genericXML2JSON(item, dataFormatter));
                }
            }
        }
        return obj;
    };


//  ----------------------------------------------------------------
    function jQueryRestAjax(url, data, method, callback, errcallback) {
//  ----------------------------------------------------------------
        return be.utils.ajax({
            type: method,
            url: url,
            cache: false,
            data: data,
            dataType: "xml",
            contentType: 'application/xml',
            success: callback,
            error: errcallback
        });
    }

    var buildUI = function (root) {
        exporerDV = new ExplorerDomView();
        var elm = exporerDV.getAttachableDOM();
        root.appendChild(elm);
        d1.resolve();
        return exporerDV;
    };

    this.buildUI = buildUI;

}(jQuery));