<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:ns2="http://ns.backbase.com/"
	version="2.0">

	<xsl:output method="html"  encoding="UTF-8"/>
<!--
	<xsl:output method="xml" omit-xml-declaration="yes" xindent="no" cdata-section-elements="" encoding="UTF-8"/>
	<ul class="bc-tree-ul" >
		<li class="bc-tree-li">
			<div class="bc-tree-div">
				<span class="bc-tree-plus"></span>
				<img class="bc-tree-img" alt="" src="folderClosed.png">
				<a class="bc-tree-a">Marketing</a>
			</div>
			<ul>...
			</ul>
		</li>
	</ul>
-->

<!--
	<xsl:template match="/*">
		<ul class="bc-tree-ul">
			<xsl:apply-templates select="portal | page | container | widget | template"  mode="inner" />
		</ul>
	</xsl:template>
-->

	<xsl:template match="/portals">
		<ul class="bc-tree-ul">
			<li class="bc-tree-li" contextItemName="[BBHOST]" tag="portal" xname="Portals">

				<div class="bc-tree-div">
					<div class="bc-tree-selector">&#160;</div><!-- add white space for Opera 11 -->
					<span class="bc-tree-minus">&#160;</span>
					<img class="bc-tree-img">
							<xsl:attribute name="src">
								<xsl:value-of select="concat(@contextRoot,'media/icon_widget.png')"/>
							</xsl:attribute>
						</img>
					<a class="bc-tree-a">Portals</a>
				</div>
				<ul class="bc-tree-ul"><xsl:apply-templates select="." mode="inner" />

					<li class="bc-tree-li" mode="catalog">
						<xsl:attribute name="contextItemName">
							<xsl:value-of select="'[BBHOST]'"/>
						</xsl:attribute>
						<xsl:attribute name="tag">
							<xsl:value-of select="'portal'"/>
						</xsl:attribute>
						<xsl:attribute name="name">
							<xsl:value-of select="'[BBHOST]'"/>
						</xsl:attribute>
						<xsl:attribute name="parentItemName">
							<xsl:value-of select="parentItemName/text()"/>
						</xsl:attribute>
						<div class="bc-tree-div">
							<div class="bc-tree-selector">&#160;</div>
							<span class="bc-tree-plus">&#160;</span>
							<img class="bc-tree-img">
								<xsl:attribute name="src">
									<xsl:value-of select="concat(//@contextRoot,'media/icon_catalog.png')"/>
								</xsl:attribute>
							</img>
							<a class="bc-tree-a">Catalog</a>
						</div>
					</li>
					<li class="bc-tree-li" mode="templates">
						<xsl:attribute name="contextItemName">
							<xsl:value-of select="'[BBHOST]'"/>
						</xsl:attribute>
						<xsl:attribute name="tag">
							<xsl:value-of select="'portal'"/>
						</xsl:attribute>
						<xsl:attribute name="name">
							<xsl:value-of select="'[BBHOST]'"/>
						</xsl:attribute>
						<xsl:attribute name="parentItemName">
							<xsl:value-of select="parentItemName/text()"/>
						</xsl:attribute>
						<div class="bc-tree-div">
							<div class="bc-tree-selector">&#160;</div>
							<span class="bc-tree-plus">&#160;</span>
							<img class="bc-tree-img">
								<xsl:attribute name="src">
									<xsl:value-of select="concat(//@contextRoot, 'media/icon_template.png')"/>
								</xsl:attribute>
							</img>
							<a class="bc-tree-a">Templates</a>
						</div>
					</li>
<!--
					<li class="bc-tree-li" mode="trash">
						<div class="bc-tree-div">
							<div class="bc-tree-selector">&#160;</div>
							<span class="bc-tree-plus">&#160;</span>
							<img class="bc-tree-img" xsrc="../..../..media/icon_template.png" />
							<a class="bc-tree-a">Trash</a>
						</div>
					</li>
-->
				</ul>
			</li>
		</ul>
	</xsl:template>

	<xsl:template match="/pages | /catalog | /links">
		<ul class="bc-tree-ul"><xsl:apply-templates select="." mode="inner" /></ul>
	</xsl:template>


	<xsl:template match="/portal | /page | /container | /widget">
		<xsl:apply-templates select="." mode="inner" />
	</xsl:template>


	<xsl:template match="children" mode="kids">
		<ul class="bc-tree-ul"><xsl:apply-templates select="portal | page | container | widget | template"  mode="inner" /></ul>
	</xsl:template>

	<xsl:template match="portal" mode="inner">
		<li class="bc-tree-li">
			<xsl:attribute name="contextItemName">
				<xsl:value-of select="contextItemName/text()"/>
			</xsl:attribute>
			<xsl:attribute name="tag">
				<xsl:value-of select="name()"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="name/text()"/>
			</xsl:attribute>
			<xsl:attribute name="parentItemName">
				<xsl:value-of select="parentItemName/text()"/>
			</xsl:attribute>

			<div class="bc-tree-div">
				<div class="bc-tree-selector">&#160;</div>
				<span class="bc-tree-minus">&#160;</span>
				<img class="bc-tree-img">
					<xsl:attribute name="src">
						<xsl:value-of select="concat(//@contextRoot,'media/icon_',name(),'.png')"/>
					</xsl:attribute>
				</img>
				<a class="bc-tree-a">
					<xsl:value-of select="name"/>
				</a>
			</div>
			<ul class="bc-tree-ul">
				<li class="bc-tree-li" mode="items">
					<xsl:attribute name="contextItemName">
						<xsl:value-of select="contextItemName/text()"/>
					</xsl:attribute>
					<xsl:attribute name="tag">
						<xsl:value-of select="name()"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="name/text()"/>
					</xsl:attribute>
					<xsl:attribute name="parentItemName">
						<xsl:value-of select="parentItemName/text()"/>
					</xsl:attribute>
					<div class="bc-tree-div">
						<div class="bc-tree-selector">&#160;</div>
						<span class="bc-tree-plus">&#160;</span>
						<img class="bc-tree-img">
							<xsl:attribute name="src">
								<xsl:value-of select="concat(//@contextRoot, 'media/icon_items.png')"/>
							</xsl:attribute>
						</img>
						<a class="bc-tree-a">Items</a>
					</div>
				</li>
				<li class="bc-tree-li" mode="catalog">
					<xsl:attribute name="contextItemName">
						<xsl:value-of select="name/text()"/>
					</xsl:attribute>
					<xsl:attribute name="tag">
						<xsl:value-of select="name()"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="name/text()"/>
					</xsl:attribute>
					<xsl:attribute name="parentItemName">
						<xsl:value-of select="parentItemName/text()"/>
					</xsl:attribute>
					<div class="bc-tree-div">
						<div class="bc-tree-selector">&#160;</div>
						<span class="bc-tree-plus">&#160;</span>
						<img class="bc-tree-img">
							<xsl:attribute name="src">
								<xsl:value-of select="concat(//@contextRoot,'media/icon_catalog.png')"/>
							</xsl:attribute>
						</img>
						<a class="bc-tree-a">Catalog</a>
					</div>
				</li>
<!--
				<li class="bc-tree-li" mode="templates">
					<xsl:attribute name="contextItemName">
						<xsl:value-of select="contextItemName/text()"/>
					</xsl:attribute>
					<xsl:attribute name="tag">
						<xsl:value-of select="name()"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="name/text()"/>
					</xsl:attribute>
					<xsl:attribute name="parentItemName">
						<xsl:value-of select="parentItemName/text()"/>
					</xsl:attribute>
					<div class="bc-tree-div">
						<div class="bc-tree-selector">&#160;</div>
						<span class="bc-tree-plus">&#160;</span>
						<img class="bc-tree-img" src="../..media/icon_template.png" />
						<a class="bc-tree-a">Templates</a>
					</div>
				</li>
-->
				<li class="bc-tree-li" mode="templates">
					<xsl:attribute name="contextItemName">
						<xsl:value-of select="contextItemName/text()"/>
					</xsl:attribute>
					<xsl:attribute name="tag">
						<xsl:value-of select="name()"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="name/text()"/>
					</xsl:attribute>
					<xsl:attribute name="parentItemName">
						<xsl:value-of select="parentItemName/text()"/>
					</xsl:attribute>
					<div class="bc-tree-div">
						<div class="bc-tree-selector">&#160;</div>
						<span class="bc-tree-plus">&#160;</span>
						<img class="bc-tree-img">
							<xsl:attribute name="src">
								<xsl:value-of select="concat(//@contextRoot, 'media/icon_template.png')"/>
							</xsl:attribute>
						</img>
						<a class="bc-tree-a">Templates</a>
					</div>
				</li>

				<li class="bc-tree-li" mode="link">
					<xsl:attribute name="contextItemName">
						<xsl:value-of select="contextItemName/text()"/>
					</xsl:attribute>
					<xsl:attribute name="tag">
						<xsl:value-of select="name()"/>
					</xsl:attribute>
					<xsl:attribute name="name">
						<xsl:value-of select="name/text()"/>
					</xsl:attribute>
					<xsl:attribute name="parentItemName">
						<xsl:value-of select="parentItemName/text()"/>
					</xsl:attribute>
					<div class="bc-tree-div">
						<div class="bc-tree-selector">&#160;</div>
						<span class="bc-tree-plus">&#160;</span>
						<img class="bc-tree-img">
							<xsl:attribute name="src">
								<xsl:value-of select="concat(//@contextRoot, 'media/icon_link.png')"/>
							</xsl:attribute>
						</img>
						<a class="bc-tree-a">Links</a>
					</div>
				</li>


			</ul>
		</li>
	</xsl:template>

<!--
<xsl:template match="page | container | widget | template" mode="inner">
-->

	<xsl:template match="page | container" mode="inner">
		<li class="bc-tree-li">
			<xsl:attribute name="contextItemName">
				<xsl:value-of select="contextItemName/text()"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="name/text()"/>
			</xsl:attribute>
			<xsl:attribute name="tag">
				<xsl:value-of select="name()"/>
			</xsl:attribute>
			<xsl:attribute name="parentItemName">
				<xsl:value-of select="parentItemName/text()"/>
			</xsl:attribute>
			<div class="bc-tree-div">
				<div class="bc-tree-selector">&#160;</div>
				<span class="bc-tree-minus">&#160;</span>
				<img class="bc-tree-img">
					<xsl:attribute name="src">
						<xsl:value-of select="concat(//@contextRoot,'media/icon_',name(),'.png')"/>
					</xsl:attribute>
				</img>
				<a class="bc-tree-a">
                    <xsl:attribute name="title">
    					<xsl:value-of select="name"/>
                    </xsl:attribute>
                    <xsl:value-of select="properties/property[@name = 'title']/value/text()"/>
				</a>
			</div>
			<xsl:apply-templates select="children"  mode="kids" />
		</li>
	</xsl:template>

	<xsl:template match="widget" mode="inner">
	</xsl:template>






	<xsl:template match="/templates">

		<ul class="bc-tree-ul">
			<li class="bc-tree-li">
				<xsl:attribute name="contextItemName">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="tag">
					<xsl:value-of select="'template'"/>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="parentItemName">
					<xsl:value-of select="parentItemName/text()"/>
				</xsl:attribute>
				<xsl:attribute name="filter">
					<xsl:value-of select="'subType(eq)PAGE'"/>
				</xsl:attribute>
				<div class="bc-tree-div">
					<div class="bc-tree-selector">&#160;</div>
					<span class="bc-tree-minus">&#160;</span>
					<img class="bc-tree-img">
						<xsl:attribute name="src">
							<xsl:value-of select="concat(//@contextRoot,'media/icon_','template','.png')"/>
						</xsl:attribute>
					</img>
					<a class="bc-tree-a">
						Page
					</a>
				</div>
				<ul class="bc-tree-ul">
					<xsl:for-each select="template">
						<xsl:if test="type/text() = 'PAGE'" >
							<xsl:apply-templates select="."  mode="template" />
						</xsl:if>
					</xsl:for-each>
				</ul>
			</li>
			<li class="bc-tree-li">
				<xsl:attribute name="contextItemName">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="tag">
					<xsl:value-of select="'template'"/>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="parentItemName">
					<xsl:value-of select="parentItemName/text()"/>
				</xsl:attribute>
				<xsl:attribute name="filter">
					<xsl:value-of select="'subType(eq)CONTAINER'"/>
				</xsl:attribute>
				<div class="bc-tree-div">
					<div class="bc-tree-selector">&#160;</div>
					<span class="bc-tree-minus">&#160;</span>
					<img class="bc-tree-img">
						<xsl:attribute name="src">
							<xsl:value-of select="concat(//@contextRoot,'media/icon_','template','.png')"/>
						</xsl:attribute>
					</img>
					<a class="bc-tree-a">
						Container
					</a>
				</div>
				<ul class="bc-tree-ul">
					<xsl:for-each select="template">
						<xsl:if test="type/text() = 'CONTAINER'" >
							<xsl:apply-templates select="."  mode="template" />
						</xsl:if>
					</xsl:for-each>
				</ul>
			</li>
			<li class="bc-tree-li">
				<xsl:attribute name="contextItemName">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="tag">
					<xsl:value-of select="'template'"/>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="parentItemName">
					<xsl:value-of select="parentItemName/text()"/>
				</xsl:attribute>
				<xsl:attribute name="filter">
					<xsl:value-of select="'subType(eq)WIDGET'"/>
				</xsl:attribute>
				<div class="bc-tree-div">
					<div class="bc-tree-selector">&#160;</div>
					<span class="bc-tree-minus">&#160;</span>
					<img class="bc-tree-img">
						<xsl:attribute name="src">
							<xsl:value-of select="concat(//@contextRoot,'media/icon_','template','.png')"/>
						</xsl:attribute>
					</img>
					<a class="bc-tree-a">
						Widget
					</a>
				</div>
				<ul class="bc-tree-ul">
					<xsl:for-each select="template">
						<xsl:if test="type/text() = 'WIDGET'" >
							<xsl:apply-templates select="."  mode="template" />
						</xsl:if>
					</xsl:for-each>
				</ul>
			</li>
			<li class="bc-tree-li">
				<xsl:attribute name="contextItemName">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="tag">
					<xsl:value-of select="'template'"/>
				</xsl:attribute>
				<xsl:attribute name="name">
					<xsl:value-of select="'[BBHOST]'"/>
				</xsl:attribute>
				<xsl:attribute name="parentItemName">
					<xsl:value-of select="parentItemName/text()"/>
				</xsl:attribute>
				<xsl:attribute name="filter">
					<xsl:value-of select="'subType(eq)INCLUDE'"/>
				</xsl:attribute>
				<div class="bc-tree-div">
					<div class="bc-tree-selector">&#160;</div>
					<span class="bc-tree-minus">&#160;</span>
					<img class="bc-tree-img">
						<xsl:attribute name="src">
							<xsl:value-of select="concat(//@contextRoot,'media/icon_','template','.png')"/>
						</xsl:attribute>
					</img>
					<a class="bc-tree-a">
						Include
					</a>
				</div>
				<ul class="bc-tree-ul">
					<xsl:for-each select="template">
						<xsl:if test="type/text() = 'INCLUDE'" >
							<xsl:apply-templates select="."  mode="template" />
						</xsl:if>
					</xsl:for-each>
				</ul>
			</li>
		</ul>
	</xsl:template>

	<xsl:template match="link" mode="inner">
		<li class="bc-tree-li">
			<xsl:attribute name="contextItemName">
				<xsl:value-of select="contextItemName/text()"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="name/text()"/>
			</xsl:attribute>
			<xsl:attribute name="tag">
				<xsl:value-of select="name()"/>
			</xsl:attribute>
			<xsl:attribute name="parentItemName">
				<xsl:value-of select="parentItemName/text()"/>
			</xsl:attribute>
			<div class="bc-tree-div">
				<div class="bc-tree-selector">&#160;</div>
				<span class="bc-tree-minus">&#160;</span>
				<img class="bc-tree-img">
					<xsl:attribute name="src">
						<xsl:value-of select="concat(//@contextRoot,'media/icon_',name(),'.png')"/>
					</xsl:attribute>
				</img>
				<a class="bc-tree-a">
                    <xsl:attribute name="title">
    					<xsl:value-of select="name"/>
                    </xsl:attribute>
                    <xsl:value-of select="properties/property[@name = 'title']/value/text()"/>
				</a>
			</div>
			<xsl:apply-templates select="children"  mode="kids" />
		</li>
	</xsl:template>

	<xsl:template match="*" mode="template">
		<li class="bc-tree-li">
			<xsl:attribute name="contextItemName">
				<xsl:value-of select="contextItemName/text()"/>
			</xsl:attribute>
			<xsl:attribute name="name">
				<xsl:value-of select="name/text()"/>
			</xsl:attribute>
			<xsl:attribute name="tag">
				<xsl:value-of select="name()"/>
			</xsl:attribute>
			<xsl:attribute name="parentItemName">
				<xsl:value-of select="parentItemName/text()"/>
			</xsl:attribute>
			<div class="bc-tree-div">
				<div class="bc-tree-selector">&#160;</div>
				<span class="bc-tree-minus">&#160;</span>
				<img class="bc-tree-img">
					<xsl:attribute name="src">
						<xsl:value-of select="concat(//@contextRoot,'media/icon_',name(),'.png')"/>
					</xsl:attribute>
				</img>
				<a class="bc-tree-a">
					<xsl:value-of select="name"/>
				</a>
			</div>
			<xsl:apply-templates select="children"  mode="kids" />
		</li>
	</xsl:template>



</xsl:stylesheet>