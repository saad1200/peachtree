b$.module("bd.widgets.Explorer", function() {
   this.Dashboard = function(oGadgetBody, sGadgetUrl) {
      
      jQuery('.bd-widgetContent', oGadgetBody).html('Developer Tools');
   
   };

   this.Maximized = function(oGadgetBody, sGadgetUrl){

      var $oGadgetBody = jQuery(oGadgetBody);
      if($.browser.msie && $.browser.version < 8)
      {
         $oGadgetBody.html("The Portal Explorer tool is not supported in Internet Explorer.");
      }
      else{
         var customStyle = "width: 100%;";
         var $htmlContent = jQuery("<iframe id='ExplorerContainer' height='97%' src='"+ bd.contextRoot + "/static/Explorer/dashboard.html' scrolling='no' style='"+customStyle+"' frameborder='0' seamless='seamless'/>");
         $oGadgetBody.append($htmlContent);
      }
//      $oGadgetBody.addClass('expl-Widget');
//      $oGadgetBody.html('<div id="expl-Widget" class="expl-Widget">');
//      window.myExplorer = buildUI(oGadgetBody);
   };
});