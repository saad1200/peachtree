    /**
	Copyright Backbase R&D 2011
	--------------------------------
	Backbase Portal 5
	portalServer.raw-json-api.js
	version 1.00
	@author : G. Kaandorp
**/

//	----------------------------------------------------------------
(function () {
	//'use strict';

//	----------------------------------------------------------------
	var Observable = b$.require('b$.mvc.Observable');

/*
	// ----------------------------------------------------------------
	var portalServer = new PortalServer();
	// ----------------------------------------------------------------
	portalServer.addObserver('itemModified',function (item, ev) {
//		console.info('itemModified');
//		console.dir(item);
//		window.explorerView.pagesPagedList.requestPage();
		window.exact.refresh();
		window.explorerView.detailModel.load(item.contextItemName, item.tag, item.name);
	});

*/



	var RENAME_CHARS = '0123456789abcdef';

//	----------------------------------------------------------------
	var PortalServer = Observable.extend(function (url){
//	----------------------------------------------------------------
		Observable.call(this);
		if(url) this.serverURL = url;
	},{
		serverURL : (window.contextRoot ? window.contextRoot : '') + "/",

		itemObject : function (contextItemName, tag, name, extendedItemName, parentItemName, properties, itemObj, filter) {
			if (!itemObj) itemObj = {};
			itemObj.contextItemName = contextItemName;
			itemObj.tag = tag;
			itemObj.name = name;
			itemObj.extendedItemName = extendedItemName;
			itemObj.parentItemName = parentItemName;
			itemObj.properties = properties;
			itemObj.filter = filter;
			return itemObj;
		},
		propertyObject : function (name, value, label, type, viewHint, manageable) {
			var propObj = {};
			propObj.name = name;
			propObj.value = value;
			propObj.label = label;
			propObj.type = type;
			propObj.viewHint = viewHint;
            propObj.manageable = manageable;
			return propObj;
		},
		simpleRenamer : function (name){
			var n2 = name;
			if(name.indexOf('::')!=-1){
				n2 = name.substr(0, name.indexOf('::'));
			}
			n2 =  n2 +'::';
			for (var i=0;i<8;i++) {
				n2 += RENAME_CHARS[Math.round(Math.random()*15)];
			}
			return n2;
		},

		copyItem : function (itemObj, callback) {
			var This = this;
			this.loadItem(itemObj, function (item, xml) {
				var item = This.itemXMLDOC2JSON(xml);
				item.name = This.simpleRenamer(item.name);
				if (itemObj.parentItemName) item.parentItemName = itemObj.parentItemName;
				delete item.children;

				for (var n in item.properties) {	// set properties...
					var props = item.properties[n]
					if (itemObj.name == props.itemName)
						props.itemName = item.name;
				}
				This.saveItemByObject(item, true, callback);
			});
		},

		newItem : function (itemObj, callback, errCallback) {
			this.saveItemByObject(itemObj, true, callback, errCallback);
		},
		updateItem : function (item, callback, errCallback) {
			this.saveItemByObject(item, false, callback, errCallback);

		},
		loadItem : function (item, callback, pc, ps, off, isContentRepository) {

			if (!item)return;
            ps = ps || 1000;
			var url = this.serverURL;

			if (item.tag == 'portal') {
			}
			else if (item.tag == 'template') {
			}
			else if (item.contextItemName == '[BBHOST]') {
			}
			else {
                if(item.tag !== 'contentRepository') {
                    url += 'portals/'+item.contextItemName+'/';
                }
			}

            if(isContentRepository) {

                if(item.name && item.tag === 'contentRepository') {
                    url += 'portals/'+item.contextItemName + '/contentrepositories/' + item.name + '.xml';
                }

            } else {

                if (item.contextItemName == '[BBHOST]') {
                    if (item.tag == 'portal' || item.tag == 'template') {
                        url += item.tag + 's';
                    } else {
                        // check for contentRepository Tag
                        if (item.tag === 'contentRepository') {
                            url += 'portals/' + item.contextItemName + '/contentrepositories';
                        } else {
                            url += 'catalog';
                        }
                    }
                } else if (item.tag === 'contentRepository') {
                    url += 'portals/' + item.contextItemName + '/contentrepositories';
                } else {
                    if (item.tag == 'link') {
                        url += item.name ? 'links' : 'pagemanagement/links';
                    }
                    else if (item.tag == 'catalog')
                        url += 'catalog';
                    else {
                        if (item.tag !== 'contentRepository') {
                            url += item.tag + 's';
                        }
                    }
                }

                if (item.name && item.tag !== 'contentRepository') {
                    url += '/' + item.name;
                }

                if (item.tag !== 'contentRepository') {
                    url += '.xml';
                }
            }

            url += '?ps=' + ps + '&';

            // -- filter ------
            if(item.filter){
                url += 'f=type(eq)' + item.filter + '&';
            }

            if (item.tag === 'link') {
                if(!item.name){
                    url += 'f=parentItemName(eq)' + (item.parentItemName || item.contextItemName) + '&';
                }
                url += 'depth=1&';
            } else {
                if(pc){
                    url += 'depth=-1&';
                }else{
                    url += 'pc=false&';
                }
            }

            if(off) url += 'of=' + off + '&';

            // ditch trailing ampersand
            url = url.substring(0, url.length - 1);

            var This = this;
			jQueryRestAjax(url, '', 'GET', function (res){
				This.notifyObservers('itemLoaded', item, res);
				if (callback) callback(item, res);
			});
		},
		saveItemByObject : function (item, bNew, callback, errCallback) {

			var CATALOG_URL = this.serverURL + 'portals/{PORTAL_NAME}/{ITEM_TYPE}/{ITEM_NAME}.xml',
				SERVER_ITEM_CREATION_URL = this.serverURL + 'catalog.xml',
				PORTAL_ITEM_CREATION_URL = this.serverURL + 'portals/{PORTAL_NAME}/catalog.xml',
				PORTAL_UPDATE_URL = this.serverURL + 'portals/{PORTAL_NAME}.xml',
				LINK_CREATE_URL = this.serverURL + 'portals/{PORTAL_NAME}/{XML_NAME}s.xml',
				XML_POST_DATA = this.itemJSON2XML(item),
				REQUEST_METHOD = bNew ? 'POST' : 'PUT',
				REQUEST_URL = '',
				This = this;

			if(bNew) {

				if(item.tag ==='template') {
					REQUEST_URL = this.serverURL + 'templates.xml';
				} else {

					if(item.contextItemName === '[BBHOST]') {

		               // prevent links creation operation for server catalog
		               // links only can be created in existing portal.
		               if(item.tag === 'link') {
		                   bc.component.notify({
		                       uid: '99456780',
		                       icon: 'error',
		                       message: 'Link creation not allowed in server catalog'
		                   });
		                   return;
		                }

						REQUEST_URL = SERVER_ITEM_CREATION_URL;
					} else {

						if(item.tag === 'link') {
							LINK_CREATE_URL = LINK_CREATE_URL
													.replace('{PORTAL_NAME}', item.contextItemName)
													.replace('{XML_NAME}', item.tag);
							REQUEST_URL = LINK_CREATE_URL;				
						} else {
							PORTAL_ITEM_CREATION_URL = PORTAL_ITEM_CREATION_URL.replace('{PORTAL_NAME}', item.contextItemName);
							REQUEST_URL = PORTAL_ITEM_CREATION_URL;
						}
					}

					if(item.tag !== 'link' && item.tag !== 'template') {
						XML_POST_DATA = '<catalog>' + XML_POST_DATA + '</catalog>';
					}
				}

			} else {

				if(item.tag === 'template') {
					REQUEST_URL = this.serverURL + 'templates.xml';
				} else {
					if(item.tag === 'portal') {
						REQUEST_URL = PORTAL_UPDATE_URL.replace('{PORTAL_NAME}', item.name);
					} else {

						CATALOG_URL = CATALOG_URL.replace('{PORTAL_NAME}', item.contextItemName)
												 .replace('{ITEM_NAME}', (!bNew && item.orgName) ? item.orgName : item.name);

					    if(item.tag === 'contentRepository') {
		                   CATALOG_URL = CATALOG_URL.replace('{ITEM_TYPE}', 'contentrepositories');
		                } else if(item.tag === 'contenttype') {
		               	   CATALOG_URL = CATALOG_URL.replace('{ITEM_TYPE}', 'contenttypes');
		                } else {
			  			   CATALOG_URL = CATALOG_URL.replace('{ITEM_TYPE}', item.tag + 's');
		                }

		                REQUEST_URL = CATALOG_URL;
					}
				}
			}

			XML_POST_DATA = '<?xml version="1.0" encoding="UTF-8"?>' + XML_POST_DATA;

            if(item.tag === 'feature') {
               bc.component.notify({
                        uid: '99802994432',
                        icon: 'error',
                        message: 'Feature item cannot be modified'
                });
                return; 
            }

			jQueryRestAjax(REQUEST_URL, XML_POST_DATA, REQUEST_METHOD, function (){
				This.notifyObservers('itemModified', item);
				if (callback) callback(item);
			}, function (err) {

                // template update in server catalog is not allowed
                if(item.tag === 'template' && err.responseText) {
                    bc.component.notify({
                        uid: '99802934432',
                        icon: 'error',
                        message: 'Template can\'t be changed'
                    });
                    return;
                }

                //server error messages handler
                if(err && err.responseText) {
                    bc.component.notify({
                        uid: '990012382945',
                        icon: 'error',
                        message: $(err.responseText).find('message')[0].innerHTML
                    });
                } else {
                    // notify error in case drag is performed over item
                    // from same item type
                    bc.component.notify({
                        uid: '123156552233',
                        icon: 'error',
                        message: 'Drag over item from same type is not allowed'
                    });
                }
            });
		},
		deleteItem : function (itemObj, callback) {
			if (itemObj.tag == 'portal') {
				var url = this.serverURL;
			}
			else if (itemObj.tag == 'template') {
				var url = this.serverURL;
			}
			else {
				var url = this.serverURL+'portals/'+itemObj.contextItemName+'/';
			}

            if(itemObj.tag !== 'contentRepository') {
                url += itemObj.tag +'s/'+itemObj.name+'.xml'
            }

            // construct url to delete portal repository
            if(itemObj.tag === 'contentRepository') {
                url += 'contentrepositories/'+itemObj.name+'.xml'
            }

			jQueryRestAjax(url, null, 'DELETE', function (){
//				window.exact.refresh();
//				This.notifyObservers('itemRemoved', tag, name, contextItemName));
				if(callback)callback(itemObj);
			});
		},



		doRecursiveSave : function (item, bNew) {
			this._doRecursiveSave([item], bNew);
		},
		_doRecursiveSave : function (ac, bNew) {
			var item = ac.pop();
			if (item) {
//				if (model instanceof BaseContainer) {
				if (item.children) {
					item.children.forEach( function (o) {
						ac.push(o);
					}, this);
				}
				var This = this;
				item.OLDparentItemName = true;	// force bypass auto catalog save...
				portalServer.saveItemByObject(item, bNew, function () {
					This._doRecursiveSave(ac, bNew)
				}, function (err) {
				})
			}
		},

		itemJSON2XML : function (item) {
			var xml = '';
			xml += '<'+item.tag+'>';
			xml += '<name>'+item.name+'</name>';
			if(item.contextItemName)				xml+= '<contextItemName>'+item.contextItemName+'</contextItemName>';
			if(item.extendedItemName)				xml+= '<extendedItemName>'+item.extendedItemName+'</extendedItemName>';
	//		if(item.parentItemName != undefined)	xml+= '<parentItemName>'+item.parentItemName+'</parentItemName>';
			if(item.parentItemName)					xml+= '<parentItemName>'+item.parentItemName+'</parentItemName>';				// !!! How to delete parentItemName????
			if(item.securityProfile && item.securityProfile !== 'undefined') xml+= '<securityProfile>'+item.securityProfile+'</securityProfile>';

			if(item.type) xml+= '<type>'+item.type+'</type>';// For TEMPLATES only!!!!

			if (item.properties) {
				xml += '<properties>';

				for (var n in item.properties) {
					if (item.properties.hasOwnProperty(n)){
						var p = item.properties[n];


//						if (p.value ==  undefined) p.value = '';

						// Detect inheritance
						if (!p.itemName || p.itemName == item.name) {
							xml += '<property name="'+p.name+'"';
							if (p.viewHint)xml += ' viewHint="'+p.viewHint+'"';
							if (p.manageable)xml += ' manageable="'+p.manageable+'"';
							if(p.label) xml += ' label="'+p.label+'" ';
							if(p.deleted) xml += ' markedForDeletion="true"'; //' deleted="true"';
							xml += '>';

							if (p.value != undefined) {
								p.value = this.encodeXML(p.value);
								xml += '<value';
								if (p.type)xml += ' type="'+p.type+'"';
								xml += '>'+p.value+'</value>';
							}
							else if (p.type) {
								xml += '<value type="'+p.type+'"/>';
							}
							xml += '</property>';
						}
//						else
//							myconsole.log(p.name +' comes from '+p.itemName)
					}
				}
				xml += '</properties>';
			}

			if (item.tags){
				xml += '<tags>';

				for (var n = 0, l = item.tags.length; n < l; n++) {
					var p = item.tags[n];
					xml += '<tag type="'+p.type+'"' + (p.blacklist ? ' blacklist="true"' : '') + '>' + p.text + '</tag>';
				}
				xml += '</tags>';
			}

			xml += '</'+item.tag+'>';
			return xml;
		},


		itemXMLDOC2JSON : function (xml) {
			if (!xml) return null;
			var c = xml.firstChild;
			if (typeof c.setAttribute !== 'function'){
				c = c.nextSibling;
			}
			var item = this.itemXML2JSON(c);
			return item;
		},

		itemXML2JSON : function (xml) {
			if (xml.tagName == 'catalog') return this.itemXML2JSON(xml.firstChild)


			var item = {};

			item.tag = xml.tagName;
			item.contextItemName = '';
			item.extendedItemName = '';
			item.parentItemName = '';

			var c = xml.firstChild;
			while(c) {
				switch(c.tagName) {
					case 'name':
						item.name = c.textContent || c.text;
						break;
					case 'type':
						item.type = c.textContent || c.text;
						break;
					case 'contextItemName':
						item.contextItemName = c.textContent || c.text;
						break;
					case 'extendedItemName':
						item.extendedItemName = c.textContent || c.text;
						break;
					case 'parentItemName':
						item.parentItemName = c.textContent || c.text;
						break;
					case 'securityProfile':
						item.securityProfile = c.textContent || c.text;
						break;
					case 'properties':
						item.properties = {};

						var c2 = c.firstChild;
						while(c2) {
							switch(c2.tagName) {
								case 'property':
									var props = {};
									props.name = c2.getAttribute('name');
									props.itemName = c2.getAttribute('itemName');

									props.label = c2.getAttribute('label');
									if (!props.label) props.label = '';
	//								props.readonly = '';
	//								if (c2.getAttribute('readonly'))props.readonly = c2.getAttribute('readonly');
									props.viewHint = '';
									if (c2.getAttribute('viewHint'))props.viewHint = c2.getAttribute('viewHint');
									props.manageable = c2.getAttribute('manageable') && c2.getAttribute('manageable') === 'true';

									var c3 = c2.firstChild;
									while(c3) {
										switch(c3.tagName) {
											case 'value':
												props.value = c3.textContent || c3.text;
												props.value = this.decodeXML(props.value);
												props.type = c3.getAttribute('type');
												if (!props.type) props.type = '';
												break;
										}
										c3 = c3.nextSibling;
									}
									item.properties[props.name] = props;
									break;
							}
							c2 = c2.nextSibling;
						}
						break;
					case 'children':
						item.children = [];
						var c2 = c.firstChild;
						while(c2) {
							if(c2.nodeType == 1) {
								item.children.push(this.itemXML2JSON(c2));
							}
							c2 = c2.nextSibling;
						}
						break;
					case 'tags':
						item.tags = [];
						var c2 = c.firstChild;
						while(c2) {
							if(c2.nodeType == 1) {
								var tag = {};
								tag.text = c2.textContent || c2.text;
								tag.type = c2.getAttribute('type');
								item.tags.push(tag);
							}
							c2 = c2.nextSibling;
						}
						break;
				}
				c = c.nextSibling;
			}
			return item;
		},

		encodeXML: function(str){
			return typeof str === 'string' ? str.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;') : str;
		},

		decodeXML: function(str){
			return typeof str === 'string' ? str.replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/&lt;/g, '<').replace(/&gt;/g, '>') : str;
		},

        loadContentRepositories: function(portalName) {
            // TO DO LOAD ALL REPOSITORIES,
            // FIRST PREPARE GET PARAMS.

            var url = this.serverURL,
                This = this;

            // resolve url to get content repository


            jQueryRestAjax(url, '', 'GET', function (res){
                This.notifyObservers('itemLoaded', item, res);
                if (callback) callback(item, res);
            });
        }

	});



//	----------------------------------------------------------------
	function jQueryRestAjax(url, data, method, callback, errcallback) {
//	----------------------------------------------------------------
        var be = be || top.be;
		return be.utils.ajax({
//		return $.ajax({
			type: method,
			url: url,
			cache: false,
			data: data,
			dataType: "xml",
			contentType: 'application/xml',
			success: callback,
			error: errcallback
		});
	}


	this.PortalServer_rawjsonapi = PortalServer;

}());
