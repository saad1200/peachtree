/*
 *  ----------------------------------------------------------------
 *  Copyright Backbase b.v. 2003/2013
 *  All rights reserved.
 *  ----------------------------------------------------------------
 *  Version 5.5
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : promise.groups.js
 * ----------------------------------------------------------------
 * Handles the Groups REST API promises
 * ----------------------------------------------------------------
 * Description:
 * This object is only used to return REST API promises.
 * It is also used as a  documentation file
 * ----------------------------------------------------------------
 *
 * Usage :
 * <pre>
 *  Promise.groups([params])
 *    .then(successCallback)
 *    .fail(errorCallback)
 *
 * //  or use it nested
 *
 *  $.when( promise1, promise2 )
*       .then( successCallback, successCallback );
 * </pre>
 *
 */

(function() {
    'use strict';
/*----------------------------------------------------------------*/
/* Private
/*----------------------------------------------------------------*/
    var baseUrl = this._baseUrl;
/*----------------------------------------------------------------*/
/* Public
/*----------------------------------------------------------------*/
    this.groups = function(data, onBefore, onAfter) {

        var config = {
            url : baseUrl + '/groups',
            data: data
        };
        return this._makeRequest(config);
    };


}).apply(be.promise);

