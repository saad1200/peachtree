/*
 *  ----------------------------------------------------------------
 *  Copyright Backbase b.v. 2003/2013
 *  All rights reserved.
 *  ----------------------------------------------------------------
 *  Version 5.5
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : promise.pages.js
 * ----------------------------------------------------------------
 * Handles the Pages REST API promises
 * ----------------------------------------------------------------
 * Description:
 * This object is only used to return REST API promises.
 * It is also used as a  documentation file
 * ----------------------------------------------------------------
 *
 * Usage :
 * <pre>
 *  Promise.pages([params])
 *    .then(successCallback)
 *    .fail(errorCallback)
 *
 * //  or use it nested
 *
 *  $.when( promise1, promise2 )
*       .then( successCallback, successCallback );
 * </pre>
 *
 */

(function() {

    'use strict';
/*----------------------------------------------------------------*/
/* Private
/*----------------------------------------------------------------*/
    var baseUrl = this._baseUrl;
    //basic validation methods can be improved further
    var validatePage = function(pageName) {
        if(typeof pageName === 'undefined' || typeof pageName !== 'string') {
          throw new Error('You must provide a pageName!');
        }
    };


/*----------------------------------------------------------------*/
/* Public
/*----------------------------------------------------------------*/
    /**
     * [pages description]
     */
    this.pages = function(data, onBefore, onAfter) {
        var config = {
            url: this._getPortalBaseUrl() + '/pages',
            data: data
        };
        return this._makeRequest(config);
    };

     /**
     * [addPage description]
     */
     var addPage = function(type){
        return function addPage (data, onBefore, onAfter) {
            var config = {
                url: this._getPortalBaseUrl() + '/' + type,
                data: data,
                type: 'POST'
            };
            return this._makeRequest(config);
        };
    };


     /**
     * [updatePage description]
     */
    var updatePage = function(type){

        return function updatePage(pageName, data, onBefore, onAfter) {

            validatePage(pageName);

            var httpConfig = {
                url: this._getPortalBaseUrl() + '/' + type + '/' + pageName,
                data: data,
                type: 'PUT',
                before: onBefore,
                after: onAfter
            };
            return this._makeRequest(httpConfig);
        };
    };


    /**
     * [deletePage description]
     * DELETE: /pages/{pageName} -- delete page from the Enterprise Catalog
     * DELETE: /portals/{portalName}/pages/{pageName}  -- delete page from the Portal Catalog
     *
     * [deleteMasterPage description]
     * DELETE: /masterpages/{pageName} -- delete from the Enterprise Catalog
     * DELETE: /portals/{portalName}/masterpages/{pageName}  -- delete from the Portal Catalog
     */

    var deletePage = function(type){

        return function deletePage (pageName, portalName, onBefore, onAfter) {

            validatePage(pageName);

            var httpConfig = {
                url: baseUrl + (portalName ? '/portals/' + portalName : '') + '/' + type + '/' + pageName,
                type: 'DELETE',
                before: onBefore,
                after: onAfter
            };
            return this._makeRequest(httpConfig);
        };
    };

    this.addPage = addPage('pages');
    this.addMasterPage = addPage('masterpages');

    this.updatePage = updatePage('pages');
    this.updateMasterPage = updatePage('masterpages');

    this.deletePage = deletePage('pages');
    this.deleteMasterPage = deletePage('masterpages');

}).apply(be.promise);


