/*
 *  ----------------------------------------------------------------
 *  Copyright Backbase b.v. 2003/2013
 *  All rights reserved.
 *  ----------------------------------------------------------------
 *  Version 5.5
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : promise.links.js
 * ----------------------------------------------------------------
 * Handles the Links REST API promises
 * ----------------------------------------------------------------
 * Description:
 * This object is only used to return REST API promises.
 * It is also used as a  documentation file
 * ----------------------------------------------------------------
 * Usage :
 * <pre>
 *  Promise.links([params])
 *    .then(successCallback)
 *    .fail(errorCallback)
 *
 * //  or use it nested
 *
 *  $.when( promise1, promise2 )
*       .then( successCallback, successCallback );
 * </pre>
 *
 */

(function() {

    'use strict';
/*----------------------------------------------------------------*/
/* Private
/*----------------------------------------------------------------*/
    //basic validation methods can be improved further
    var validateLink = function(link) {
        if(typeof link === 'undefined' || typeof link !== 'string') {
          throw new Error('You must provide a link!');
        }
    };

    var validateUUID = function(uuid) {
        if(typeof uuid === 'undefined' || typeof uuid !== 'string') {
          throw new Error('You must provide an uuid!');
        }
    };

/*----------------------------------------------------------------*/
/* Public
/*----------------------------------------------------------------*/
    /**
     * [links description]
     * @param  {[type]} data     [description]
     */
    this.links = function(data, onBefore, onAfter) {

        var config = {
            url : this._getPortalBaseUrl() + '/links',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [link description]
     * @param  {[type]} link     [description]
     */
    this.link = function(link, data, onBefore, onAfter) {
        validateLink(link);

        var config = {
            url : this._getPortalBaseUrl() + '/links/' + link,
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [postLink description]
     */
    this.addLink = function(data, onBefore, onAfter) {

        var config = {
            url : this._getPortalBaseUrl() + '/links',
            type : 'POST',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [editLink description]
     * @param  {[type]} link     [description]
     */
    this.editLink = function(link, data, onBefore, onAfter) {

        validateLink(link);

        var config = {
            url : this._getPortalBaseUrl() + '/links/' + link,
            type : 'PUT',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [deleteLink description]
     */
    this.deleteLink = function(data, onBefore, onAfter) {

        var config = {
            url : this._getPortalBaseUrl() + '/delete/links',
            type : 'DELETE',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [duplicateLink description]
     */
    this.duplicateLink = function(link, data, onBefore, onAfter) {

        validateLink(link);

        var config = {
            url : this._getPortalBaseUrl() + '/links/' + link + '/duplicate',
            type : 'POST',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [duplicateItem description]
     */
    this.duplicateItem = function(itemName, itemType, data, onBefore, onAfter){

        var baseUrl = this._getPortalBaseUrl();

        var url = baseUrl == bd.contextRoot ?
            baseUrl + '/' + itemType + 's/' + itemName + '/duplicate' :
            baseUrl + '/items/' + itemName + '/replicate';

        var config = {
            url : decodeURIComponent(url),
            type : 'POST',
            data: data,
            params: {
                includeSelf: true
            },
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [pagemanagementLinks description]
     */
    this.pagemanagementLinks = function(data,onBefore, onAfter) {

        var config = {
            url : this._getPortalBaseUrl() + '/pagemanagement/links',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [pagemanagementSearchLinks description]
     */
    this.pagemanagementSearchLinks = function(data,onBefore, onAfter) {

        var config = {
            url : this._getPortalBaseUrl() + '/pagemanagement/search/links',
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [editPagemanagementLink description]
     */
    this.editPagemanagementLink = function(link, data,onBefore, onAfter) {

        validateLink(link);

        var config = {
            url : this._getPortalBaseUrl() + '/pagemanagement/links/' + link,
            data: data,
            type: 'PUT',
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };
    /**
     * [deletePagemanagementLink description]
     */
    this.deletePagemanagementLink = function(link, data,onBefore, onAfter) {

        validateLink(link);

        var config = {
            url : this._getPortalBaseUrl() + '/pagemanagement/links/' + link,
            data: data,
            type: 'DELETE',
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [linkTree description]
     * @param  {[type]} uuid     [description]
     */
    this.linkTree = function(uuid, data,onBefore, onAfter) {

        validateLink(uuid);

         var config = {
            url : this._getPortalBaseUrl() + '/linktree/' + uuid,
            data: data,
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };
    /**
     * [linkRequest description]
     */
    this.linkRequest = function(data,onBefore, onAfter) {

         var config = {
            url : this._getPortalBaseUrl() + '/links/request',
            data: data,
            type: 'POST',
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };

    /**
     * [deletion check description]
     * @param  {[type]} uuid     [description]
     */
    this.deletionCheck = function(uuid, onBefore, onAfter) {
        validateUUID(uuid);

        var config = {
            url : this._getPortalBaseUrl() + '/pagemanagement/links/delete/check',
            data : {'uuid' : uuid},
            before: onBefore,
            after: onAfter
        };

        return this._makeRequest(config);
    };


}).apply(be.promise);

