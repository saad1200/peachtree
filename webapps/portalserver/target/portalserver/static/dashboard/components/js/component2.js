if (!window.bc) window.bc = {};

bc.component2 = (function() {
	
	var htmlPath = '/static/dashboard/components/html/test/';
	
	bd.contextRoot = bd.contextRoot || top.bd.contextRoot;
	
	htmlPath = bd.contextRoot + htmlPath;

    function scrollPos(el){
        var scroll = {
            top     : 0,
            left    : 0
        }
        while(el && el.offsetParent){
            scroll.top  += el.offsetParent.scrollTop || 0;
            scroll.left += el.offsetParent.scrollLeft || 0;
            el = el.offsetParent;
        }

        return scroll;
    }

	// TOOLTIP
	var tooltip = function(params){

		var template    = be.utils.processHTMLTemplateByUrl(htmlPath + 'tooltip2.html', params);
		var $template   = $(template);
        var $tooltip    = $(".bc-tooltip", $template).remove();
        var tip, w, h, x, y, defaultMessage = params.message;

        var reFlow = function(){
            $tooltip.toggleClass("bc-tooltip-flip-h", x > w);
            $tooltip.toggleClass("bc-tooltip-flip-v", y < h);
        };

        $template.mouseover(function(e){

            var $target = $(e.currentTarget);
			tip         = $(this).parent().attr('title') || defaultMessage;

			$(this).parent().attr('title', '');
            $(".bc-tooltip-body", $tooltip).text(tip);
			$(this).append($tooltip);

            w = $tooltip.outerWidth();
            h = $tooltip.outerHeight();

			$tooltip.css('opacity','0');
			$tooltip.fadeTo('1', 0.9);

            x = $(e.target).offset().left - scrollPos(e.target).left;
            y = $(e.target).offset().top - scrollPos(e.target).top;

            reFlow();

		}).mousemove(function(e){
            // reFlow();
		}).mouseout(function(){
			$(this).parent().attr('title', tip);
            tip = '';
			$tooltip.remove();
		});

		// return as variable or auto-append to target
		if (params.target) {
			$(params.target).append($template);
		}
		return $template[0]; // returns a DOM node
	};

	// public functions
	return {
		tooltip         : tooltip
	}
})();

