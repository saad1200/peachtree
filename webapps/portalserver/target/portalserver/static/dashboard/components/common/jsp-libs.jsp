<%-- Copyright © 2011 Backbase B.V. --%>
<%@page import="com.backbase.portal.foundation.presentation.util.BuildConfigUtils"%>
<%@page import="com.backbase.portal.foundation.business.utils.context.ThreadLocalRequestContext"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<%String buildVersion = BuildConfigUtils.getBuildVersion();%>
<c:choose>
	<c:when test="${devmode}">
		<!-- YUI3 CSS Resets and Grids -->
		<link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/portalclient/css/reset.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/backbase.com.2012.components/css/grids.css" />

		<!-- Component-specific styles -->
		<link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/backbase.com.2012.components/css/components.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/backbase.com.2012.components/css/icons.css" />
		<!-- <link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/backbase.com.2012.components/css/icons2.css" /> -->

		<script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery.min.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery-migrate.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/lib/jquery-ui-1.9.2.custom.min.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/lib/jquery.validate-1.8.1.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/lib/modernizr.js"></script>

		<!-- scripts for component page  DO NOT INCLUDE -->
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/ext-libs/date.format.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/common-utils.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/css.escape.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/lib/mustache.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.components/js/components.js"></script>
        <script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.components/js/components.search.js"></script>
	</c:when>
	<c:otherwise>
		<!-- style for component page layout  DO NOT INCLUDE -->
		<link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/portalclient/css/reset.css" />
		<link rel="stylesheet" type="text/css" media="all" href="<%=portalContextRoot%>/static/dashboard/build/dashboard-all.min.css" />

	    <script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery.min.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery-migrate.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/portalclient/js/libs/mustache.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/lib/modernizr.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/ext-libs/date.format.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/common-utils.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/css.escape.js"></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.components/js/components.js"></script>
        <script type="text/javascript" src="<%=portalContextRoot%>/static/backbase.com.2012.components/js/components.search.js"></script>
		<!-- <script type="text/javascript" src="<%=portalContextRoot%>/static/conf/global-constants.js"></script> -->
		<script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/build/dashboard-lib.min.js"></script>
	</c:otherwise>
</c:choose>

<link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
<link rel="stylesheet" type="text/css" media="all" href="css/reset.css" />
<script type="text/javascript" src="js/scripts.js"></script>
