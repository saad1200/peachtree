<%@page import="com.backbase.portal.foundation.presentation.util.UiFormatterUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.text.*,java.util.*" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Backbase Components</title>
<script type="text/javascript">
    var bd = bd || {};
    bd.contextRoot = "<%=portalContextRoot%>";
    if(window.bc == null) window.bc = {};
    bc.dateTimeFormat = <%=UiFormatterUtil.getLocaleDateTimeFormat(request)%>;
</script>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<jsp:include page="common/jsp-libs.jsp"/>
</head>

<body>
	<div class="cc-header"></div>
	<div class="cc-portalManagement cc-popupForm">
		<div class="cc-mainTitle">Portal Manager Style Guide</div>
		<div style="width: auto; background-color: #eee; margin: 1em 0 1em 0; padding: 1em">
			Wiki: <a href="https://backbase.atlassian.net/wiki/display/development/Live+Styleguide#LiveStyleguide-WayWeWork">How do I use this</a>? <br /> Wiki: What will be added? <br />
		</div>
		<div class="cc-tabs">
			<div class="cc-columnHeader cc-tabLabels">
				<div class="cc-tabLabel cc-tab1" for=".cc-tab1">Dropdowns</div>
				<div class="cc-tabLabel cc-tab7" for=".cc-tab7">Modals</div>
				<div class="cc-tabLabel cc-tab3" for=".cc-tab3">Buttons</div>
				<div class="cc-tabLabel cc-tab6" for=".cc-tab6">Panels</div>
				<div class="cc-tabLabel cc-tab2" for=".cc-tab2">Form Components</div>
				<div class="cc-tabLabel cc-tab4" for=".cc-tab4">Icons</div>
				<div class="cc-tabLabel cc-tab5" for=".cc-tab5">Layouts</div>
				<div class="cc-tabLabel cc-tab8" for=".cc-tab8">Application Patterns</div>
				<div class="cc-tabLabel cc-tab9" for=".cc-tab9">Utility Functions</div>
				<div class="cc-tabLabel cc-tab10" for=".cc-tab10">UI Patterns</div>
			</div>
		</div>
	</div>
	<div class="cc-footer">
		<span class="cc-leftInline">Support@backbase.com</span>
	</div>
</body>
</html>