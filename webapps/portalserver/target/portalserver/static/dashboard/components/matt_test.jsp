<!DOCTYPE html>
<%@ page session="false"%>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>Backbase Components</title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />

<jsp:include page="common/jsp-libs.jsp"/>

<!-- Testing Files -->
<link rel="stylesheet" type="text/css" media="all" href="css/components2.css" />
<script type="text/javascript" src="js/component2.js"></script>



<style type="text/css">

</style>
<script type="text/javascript">
    $(function(){
        bc.component2.tooltip({
            target  : '.bc-i',
            color   : 'blue',  // green, grey
            message : 'This is a tooltip example'
        });
    });
</script>
</head>

<body class="dashboard">
<ul class="bc-audit-data bc-panel-hoverlist">
	<li class="bc-g">
		<div class="bc-1-8">
			<p class="bc-i" title="1) Column 1 and some extra text to see how this thing grows">Static</p>
		</div>
		<div class="bc-1-8">
			<p class="bc-i">Static</p>
		</div>
		<div class="bc-1-8">
			<p class="bc-i">Static</p>
		</div>
		<div class="bc-1-8">
			<p class="bc-i">Static</p>
		</div>
		<div class="bc-1-2">
			<p class="bc-i">This is a fluid column with exceeding width for test purposes</p>
		</div>
	</li>
</ul>

<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i" title="2) Column 1 and some extra text to see how this thing grows">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i" title="This is a tooltip example">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i" title="3) Column 1 and some extra text to see how this thing grows">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i" title="This is a tooltip example">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i" title="This is a tooltip example">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
<ul class="bc-audit-data bc-panel-hoverlist bc-g">
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-8">
        <p class="bc-i">Column n</p>
    </li>
    <li class="bc-1-2">
        <p class="bc-i">Column 5</p>
    </li>
</ul>
</body>
</html>