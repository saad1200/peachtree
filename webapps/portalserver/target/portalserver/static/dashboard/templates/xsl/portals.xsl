<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright © 2011 Backbase B.V. -->
<xsl:stylesheet xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:ns2="http://ns.backbase.com/"
	version="2.0">

	<xsl:output method="html" omit-xml-declaration="yes" indent="no"
		cdata-section-elements="" encoding="UTF-8" />


	<xsl:param name="contextRoot" />
	
	<xsl:template match="/portals">
		<xsl:apply-templates select="portal" />
	</xsl:template>
	
	<xsl:template match="portal">
		<xsl:variable name="title" select="properties/property[@name='Title']/value"/>
			<div title="{$title}" portalname="{name}" portaltitle="{$title}" devicetarget="{properties/property[@name='TargetedDevice']/value}">
				<xsl:attribute name="class">
					<xsl:text>bd-portalListItem bd-roundcorner-5 bd-shadow</xsl:text>
				</xsl:attribute>
						
				<div class="bd-thumbnail bd-shadow">
					
					<!-- TODO: fix URLS, maybe when we move away from xslt -->
					<div class="bd-thumbnailLabel">
					<xsl:choose>
						<xsl:when test="properties/property[@name='thumbnail-url']">
							<xsl:attribute name="style">
								padding-top: 60px; background:url('<xsl:value-of select="$contextRoot"/><xsl:value-of select="substring(properties/property[@name='thumbnail-url']/value, 15)" />') repeat scroll 0 0 #888888;
							</xsl:attribute>
						</xsl:when>
						<xsl:otherwise>insert portal image</xsl:otherwise>
					</xsl:choose>
					</div>
				</div>
	
				<div class="bd-portalTitle">
					<a href="javascript:;" id="portalItem-{name}"><div></div>
						<xsl:choose>
							<xsl:when test="string-length($title) &gt; 20">
								<xsl:value-of select="substring($title, 0, 20)" />
								<xsl:text>...</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="$title" />
							</xsl:otherwise>
						</xsl:choose>
					</a>
				</div>
			</div>
	</xsl:template>
</xsl:stylesheet>