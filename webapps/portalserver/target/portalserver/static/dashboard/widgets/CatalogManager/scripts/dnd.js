/**
 * Copyright (c) 2015 Backbase B.V.
 *
 * Sugar on top of nativeDnD module
 *
 * Singleton which returns one NativeDnD instance which could be used by several SPI modules
 * First call will create an instance and cache options as defaults
 *
 */

define([
    'backbase.com.2014.components/scripts/nativeDnD'
], function(NativeDnD) {
    'use strict';

    var nativeDnD;

    /**
     * Set options for DND
     * @param {[type]}
     */
    function set (options) {
        if (nativeDnD) {
            nativeDnD.reset(options ? options : nativeDnD.defaults);
        } else {
            nativeDnD = new NativeDnD.NativeDnD(options);
            nativeDnD.defaults = options;
        }
        return nativeDnD;
    }

    return {
        set: set
    };

});

