/* globals jQuery, be, bd, bc, b$, Mustache */
/**
 * Copyright (c) 2013 Backbase B.V.
 */
define([
    'dashboard/widgets/PortalMgmt/scripts/portal-export',
    'backbase.com.2014.components/scripts/filePicker',
    'backbase.com.2014.components/scripts/formDataManager',
    'zenith/utils',
    'zenith/http/portals',
    './scripts/dnd',
    './scripts/utils',
    './scripts/settings'
], function(portalExport, FilePicker, FormDataManager, utils, portals, nativeDnD, cmUtils, cmItemSettings) {
    'use strict';

    bd.portal = b$.portal || {};
    bd.portal.config = bd.portal.config || { serverRoot: bd.contextRoot };
    bd.portal.portalName = bd.portal.portalName || bd.portalName;
    bd.oPortalClient = bd.oPortalClient || {};
    bd.oPortalClient.params = {
        contextRoot: bd.portal.config.serverRoot,
        portalType: 'dashboard'
    };

    bd.CatalogManager = (function($) {

        var itemsPerRequest = 30, // bd.uiEditingOptions.catalog.itemsPerRequest
            dbLock = false, // indicates we're still waiting for server results
            offset = 0,
            $catalogMain,
            itemData = {},
            canExportItems,
            cannotExportItemsMsg;

        var API_SERVER_CATALOG_READ = bd.contextRoot + '/catalog.xml';
        var API_SERVER_CATALOG_DELETE = bd.contextRoot + '/catalog/delete';
        var API_IMPORT = bd.contextRoot + '/import/package';
        var API_SERVER_CATALOG_TAGS = bd.contextRoot + '/tags/catalog?ps=-1';
        var API_CAN_EXPORT = bd.contextRoot + '/catalog/export/config';

        // generated dynamically based on selected portal
        var API_PORTAL_CATALOG_TAGS,
            API_PORTAL_CATALOG_READ,
            API_PORTAL_CATALOG_DELETE;

        var TXT_CATALOG_FILTER_ALL = 'All Items',
            TXT_CATALOG_FILTER_WIDGETS = 'Widgets',
            TXT_CATALOG_FILTER_MASTER_PAGES = 'Master pages',
            TXT_CATALOG_FILTER_CONTAINERS = 'Layouts',
            TXT_CATALOG_FILTER_CONTENTTYPES = 'Content types',
            TXT_CATALOG_FILTER_TEMPLATES = 'Templates',
            TXT_CATALOG_FILTER_FEATURES = 'Features',

            TXT_CATALOG_SORT_TITLE = 'Title',
            TXT_CATALOG_SORT_TYPE = 'Type',
            TXT_CATALOG_SORT_DATE = 'Creation Date',

            TXT_CATALOG_SELECT_ALL = 'All on this page',
            TXT_CATALOG_SELECT_NONE = 'None',

            TXT_CATALOG_REMOVE_TITLE = {
                'enterprise': function(itemsLength){
                    if (itemsLength > 1) {
                        return 'REMOVE ' + itemsLength + ' ITEMS FROM ENTERPRISE CATALOG?';
                    }
                    return 'REMOVE ITEM FROM ENTERPRISE CATALOG?';
                },
                'portal': function(itemsLength){
                    if (itemsLength > 1) {
                        return 'REMOVE ' + itemsLength + ' ITEMS FROM ' + cmUtils.shrink(bd.selectedPortalName, 7).toUpperCase() + ' PORTAL CATALOG?';
                    }
                    return 'REMOVE ITEM FROM ' + cmUtils.shrink(bd.selectedPortalName, 7).toUpperCase() + ' PORTAL CATALOG?';
                }
            },

            TXT_CATALOG_REMOVE_TEXT = {
                'enterprise': 'This action cannot be undone.<br/>Are you sure you want to continue?',
                'portal': 'This will also remove all instances from the portal pages.<br/>This cannot be undone.'
            },

            TXT_CATALOG_DELETE_OK = 'REMOVE',
            TXT_CATALOG_DELETE_CANCEL = 'CANCEL',

            // TRASH

            TXT_CATALOG_DELETE_SUCCESS = 'Items were successfully removed',
            TXT_CATALOG_DELETE_ERROR = 'Something went wrong',


            TXT_CATALOG_IMPORT_TITLE = 'Item import',
            TXT_CATALOG_IMPORT_UNSUPPORTED_FILE = 'Unsupported file type',
            TXT_CATALOG_IMPORT_MISSING_FILE = 'Please, select file you want to import',
            TXT_CATALOG_IMPORT_PROGRESS = 'Import in progress',
            TXT_CATALOG_IMPORT_SUCCESS = 'Item was successfully imported',
            TXT_CATALOG_IMPORT_FAILURE = 'Import failed',
            TXT_CATALOG_IMPORT_OK = 'OK',
            TXT_CATALOG_IMPORT_CANCEL = 'CANCEL',

            ALLOWED_IMPORT_PACKAGE_EXT = ['zip'],

            TXT_CATALOG_DUPLICATE_ITEM = 'The {{0}} "{{1}}" was successfully duplicated';




        var isPortalCatalog = false;
        var loadMore = false;
        // var notification       = null;

        var filterType, filterTitle, filterTags;
        var filters = [];
        var sortField,
            sortDir = be.utils.cookie(bd.portal.portalName + '_sortDir') || 'dsc';

        var importData = {};
        var serverCatalog = [];

        var txtCatalogFilter = {
            container: TXT_CATALOG_FILTER_CONTAINERS,
            page: TXT_CATALOG_FILTER_MASTER_PAGES,
            template: TXT_CATALOG_FILTER_TEMPLATES,
            contenttype: TXT_CATALOG_FILTER_CONTENTTYPES,
            widget: TXT_CATALOG_FILTER_WIDGETS,
            all: TXT_CATALOG_FILTER_ALL,
            feature: TXT_CATALOG_FILTER_FEATURES
        };
        var supportedItemTypes = {
            server: [
                'container',
                'page',
                'template',
                'contenttype',
                'widget',
                'feature'
            ],
            portal: [
                'container',
                'page',
                'contenttype',
                'widget'
            ]
        };

        var searchTimeout, pagingTimeout;

        var $body, $loaderTile, $tagsArea, $progressNotify = $('');

        var uploadUrl,
            dialogActive = false,
            formDataManager; // for nativeDnD (for example in showImportDialog()...)



        var cancelImport = function() {
            $body.removeClass('bd-import-active');
        };

        var updateCatalogMainMarginTop = function() {
            $catalogMain.css({
                marginTop: $body.hasClass('bd-catalog-tags-active') ? $tagsArea[0].offsetHeight : 'auto'
            });
        };

        var checkIfItemsExportingIsAllowed = function () {
            var dfd = new $.Deferred();

            be.utils.ajax({
                url: API_CAN_EXPORT,
                cache: false,
                encodeURI: false,
                error: function (jqXHR, exception) {
                    cannotExportItemsMsg = exception;
                },
                complete: function (jqXHR) {
                    canExportItems = jqXHR.status === 204;
                    dfd.resolve();
                }
            });

            return dfd.promise();
        };

        var updateTags = function() {
            var url = isPortalCatalog ? API_PORTAL_CATALOG_TAGS : API_SERVER_CATALOG_TAGS;
            if (filterType.value){
                url += '&f=type(eq)' + filterType.value.toUpperCase();
            }
            be.utils.ajax({
                url: url,
                dataType: 'xml',
                cache: false,
                encodeURI: false,
                success: function(xml) {
                    var tplData = {
                        tags: []
                    };
                    var $xml = $(xml);
                    $xml.find('> tags > tag').each(function(idx, item) {
                        var $item = $(item);
                        tplData.tags.push({
                            tag: $item.text(),
                            type: $item.attr('type')
                        });
                    });

                    tplData.tags.sort(function(a, b) {
                        if (a.tag > b.tag) return 1;
                        if (a.tag < b.tag) return -1;
                        return 0;
                    });
                    var htmlContent = cmUtils.getHtmlTemplate('tag.html', tplData);
                    $tagsArea.html(htmlContent);
                    updateCatalogMainMarginTop();
                },
                error: function() {
                    console.log('ERROR - unable load tags');
                }
            });
        };

        var applyFilter = function(filterObj, value) {
            var idx = filters.indexOf(filterObj);
            if (idx !== -1) {
                filters.splice(idx, 1);
            }
            if (value) {
                filterObj.value = String(value);
                filters.push(filterObj);
            } else {
                filterObj.value = value;
            }
            if (!filterTags.value){
                updateTags();
            }
        };


        var getItemModel = function(itemName) {
            return serverCatalog.filter(function(item) {
                return item.name === itemName;
            })[0];
        };

        var convertFilterToRegex = function (filter) {
            // These characters need to be escaped for the RegExp: \ ^ $ * + ? . ( ) | { } [ ]
            var escapedSearchFilter = filter.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
            return new RegExp('(' + escapedSearchFilter + ')', 'i');
        };

        /**
         * Highlight matching string in items title during search
         *
         * @return {void}
         */
        var highlightSearchResult = function() {
            $('.bd-catalog-item', $body).each(function(idx, item) {
                var $item = $(item),
                    itemName = String($item.data('name')),
                    itemModel = getItemModel(itemName),
                    re;

                if (filterTitle.value) {
                    re = convertFilterToRegex(filterTitle.value);
                }
                if (!itemModel || !itemName.match(re) && !(itemModel.title && itemModel.title.match(re)) || !filterTitle.value) {
                    if ($item.data('originalTitle') !== undefined) {
                        $item.find('.bc-tile-title').text(Mustache.to_html('{{text}}', {
                            text: $item.data('originalTitle')
                        }));
                    }
                } else {
                    $item.data('originalTitle', itemModel.title);
                    $item.find('.bc-tile-title').html(Mustache.to_html('{{text}}', {
                        text: itemModel.title || itemModel.name
                    }).replace(re, '<span class="bd-catalog-search-highlight">$1</span>'));
                }
            });
        };


        var hideLoadingTile = function() {
            if ($loaderTile) {
                $loaderTile.hide();
            }
        };


        var showLoadingTile = function() {
            if ($loaderTile) {
                $loaderTile.appendTo($catalogMain).show();
            } else {
                var tpl = '<div class="bd-catalog-loading-icon"><div class="bd-catalog-item-thumbnail" style="height:100%;background: transparent url(' + bd.contextRoot + '/static/backbase.com.2012.components/media/loading_icon_32.png) no-repeat center"s></div></div>';
                $loaderTile = $(tpl);
                $catalogMain = $catalogMain || $('.bd-catalog-main', $body);
                $catalogMain.append($loaderTile);
            }
        };


        var loadItems = function(callback, forceRefresh, autoSelectItem, cacheKillerFor) {
            if(dbLock) return;
            if(forceRefresh){
                loadMore = false; // no new page, but fresh page 1
            }
            var catalogUrl,
                nrOfItemsOnPage = $('.bd-catalog-item:visible', $body).length,
                filter = '',
                sortField = be.utils.cookie(bd.portal.portalName + '_sortField') || 'createdTimestamp';

            showLoadingTile();

            if(filterType.value){
                filter = '&f=type(eq)' + filterType.value.toUpperCase();
            }
            catalogUrl = (isPortalCatalog ? API_PORTAL_CATALOG_READ : API_SERVER_CATALOG_READ) + '?' +
                'ps=' + itemsPerRequest +
                 filter +
                '&s=' + sortField + '(' + sortDir + ')';

            if (filterTitle.value) {
                catalogUrl += '&f=property.title(like)' + encodeURIComponent(filterTitle.value);
            }


            if (filterTags.value) {
                catalogUrl += '&f=tag.name(eq)' + filterTags.value;
            }

            if (!dbLock && loadMore){
                offset = nrOfItemsOnPage;
                catalogUrl += '&of=' + offset;
            }

            dbLock = true;
            be.utils.ajax({
                url: catalogUrl,
                dataType: 'xml',
                cache: false,
                encodeURI: false,
                success: function(xml) {
                    if (!loadMore) {
                        serverCatalog = [];
                        itemData = {};
                    }
                    var items = [],
                        $xml = $(xml),
                        availableItems = isPortalCatalog ? supportedItemTypes.portal : supportedItemTypes.server,
                        availableItemsSelector = availableItems.map(function(item){
                            return '>catalog>' + item;
                        }).join(',');

                    $xml.find(availableItemsSelector).each(function() {
                        var item = cmUtils.parseItem(this, isPortalCatalog, cacheKillerFor);

                        $(item.tags).each(function(index, tag) {
                            if(tag.name.toLowerCase() === 'cxpmanager') {
                                item.hasCXPTag = true;
                            }
                            if(tag.name.toLowerCase() === 'hide-in-catalog' || tag.name.toLowerCase() === 'hideincatalog') {
                                item.hasHiddenTag = true;
                            }
                        });

                        items.push(item);
                        itemData[item.uuid] = {
                            title: item.title,
                            type: item.type,
                            name: item.name
                        };
                    });
                    serverCatalog = serverCatalog.concat(items);
                    serverCatalog = utils.uniq(serverCatalog, false, function(item){ return item.uuid; });

                    var delayedSuccess = false;
                    if (callback) {
                        delayedSuccess = callback(items, autoSelectItem);
                    }

                    if (!delayedSuccess){
                        loadMore = false;

                        hideLoadingTile();
                        highlightSearchResult();

                        dbLock = false;
                    }
                },
                error: function() {
                    loadMore = false;
                    hideLoadingTile();
                    dbLock = false;
                }
            });
        };


        var cancelSelection = function() {
            $('.bd-catalog-item', $body).removeClass('bc-s-selected bc-s-first');
            $('.bc-s-active, .bc-s-multi', $body).removeClass('bc-s-active bc-s-multi');
        };


        var renderItemList = function(items, autoSelectItem) {
            var $item;
            if (items && items.length !== undefined) {
                var itemsList = cmUtils.getHtmlTemplate('itemList.html', {
                    items: items,
                    exportConfig: {
                        canExportItems: canExportItems,
                        cannotExportItemsMsg: cannotExportItemsMsg
                    }
                });

                if (loadMore) {
                    var $itemsList = $(itemsList);
                    $itemsList.each(function(idx, item) {
                        $item = $(item);
                        if ($item.hasClass('bd-catalog-item')) {
                            var itemName = $item.data('name');
                            if ($('.bd-catalog-item[data-name="' + itemName + '"]', $body).length === 0) {
                                $catalogMain.append($item);
                            }
                        }
                    });
                } else {
                    $catalogMain.html(itemsList);
                    cancelSelection();
                }

                if (autoSelectItem){
                    $item = $catalogMain.find('.bd-catalog-item[data-name=' + autoSelectItem + ']');
                    if (!$item.length){
                        dbLock = false;
                        loadMore = true;
                        loadItems(renderItemList, false, autoSelectItem);
                        return true;
                    } else {
                        $item.addClass('bc-s-selected');
                        $item[0].scrollIntoView();
                        return false;
                    }
                }
            }
        };


        var showItemSettings = function(itemName, itemType, extendedItemName){
            return cmItemSettings.show(itemName, itemType, isPortalCatalog, $body, serverCatalog, renderItemList, loadItems, updateTags, extendedItemName);
        };



        /* filePicker Import */
        // for import dialog only
        var filePicker; // this is the instance of the file-upload fallback (later on in upload modal)
        var createFilePicker = function(options) {
            return new FilePicker.FilePicker({
                dropZone: options.dropZone || '.bd-fileupload-area',
                inputName: options.inputName || 'file',
                inputLabelClass: 'bd-input-trigger-label',
                immediateUpload: false,
                uploadUrl: options.uploadUrl,
                openCallback: options.openCallback,
                responseCallback: options.responseCallback
            });
        };


        /* D&D Import */
        // for whole body and import dialog
        var importDialogDropCallback = function(e) {
            var allowedFiles = this.options.allowedFileTypes;

            importData = e.dataTransfer; // booooh, global variable
            if (!importData.files || !importData.files.length) { // whatever
                cmUtils.notify(TXT_CATALOG_IMPORT_MISSING_FILE, 'error');
                cancelImport();
                return false;
            } else if (!importData.files[0].name.match( new RegExp('\\.(?:' + allowedFiles + ')$', 'i') )) {
                cmUtils.notify(TXT_CATALOG_IMPORT_UNSUPPORTED_FILE);
                cancelImport();
                return false;
            }

            formDataManager.addItems(importData);
            if (!dialogActive) {
                showImportDialog();
            } else {
                filePicker.removeDetachedForm();
            }

            cancelImport();

            $('.bd-fileupload-area', this.options.dropZone).addClass('bd-added-file').
            find('.bd-has-file .bd-file-text').text(importData.files[importData.files.length - 1].name);
            return true;
        };

        var activateImport = function() {
            $body.removeClass('bd-catalog-tags-active');
            if (!dialogActive) {
                $body.addClass('bd-import-active');
            }
        };

        var overCallback = function(e) {
            activateImport();
        };

        var leaveWindowCallback = function(e) {
            cancelImport();
        };

        var nativeDnDOptions = {
            canvas: 'body',
            dropZone: '.bd-catalog-import-drop',
            allowedFileTypes: ALLOWED_IMPORT_PACKAGE_EXT.join('|'),
            overCallback: overCallback,
            dropCallback: importDialogDropCallback,
            leaveWindowCallback: leaveWindowCallback,
            allowMultipleFiles: false
        };

        nativeDnD.set(nativeDnDOptions);


        var scrollHandler = function() {
            clearTimeout(pagingTimeout);
            pagingTimeout = setTimeout(function() {
                var container = $catalogMain[0],
                    containerHeight = container.scrollHeight,
                    containerScroll = container.scrollTop,
                    visibleHeight = container.clientHeight,
                    listItemHeight = $('.bd-catalog-item', $body).height();
                if (containerHeight - containerScroll - visibleHeight < listItemHeight * 1.5) {
                    loadMore = true;
                    loadItems(renderItemList);
                }
            }, 1000);
        };


        /**
         * Filter items by title
         * @return {void}
         */
        var filterItems = function(newSearch) {

            $('.bd-catalog-item', $body).each(function(idx, item) {
                var $item = $(item),
                    itemName = String($item.data('name')),
                    itemType = String($item.data('type')),
                    itemModel = getItemModel(itemName),
                    filtered = false,
                    re, currentFilter = null;

                var filterTag = function(tag) {
                    return tag.name === currentFilter.value;
                };

                for (var i = 0; i < filters.length; i++) {
                    currentFilter = filters[i];
                    switch (currentFilter.type) {
                        case 'type':
                            filtered = filtered || !itemModel || itemType !== currentFilter.value;
                            break;
                        case 'title':
                            re = re || convertFilterToRegex(currentFilter.value);
                            filtered = filtered || !itemModel || !(itemModel.title && itemModel.title.match(re));
                            break;
                        case 'tag':
                            filtered = filtered || !itemModel || itemModel.tags.filter(filterTag).length === 0;
                            break;
                        default:
                            break;
                    }
                }

                filtered ? $item.hide() : $item.css({ display: '' });

            });

            if (filters.length !== 0) {
                highlightSearchResult();
                if(!newSearch){
                    scrollHandler();
                }else{
                    offset = 0;
                    loadMore = true;
                    loadItems(renderItemList);
                }
            } else {
                loadItems(renderItemList, true);
                updateTags();
            }
        };

        var toggleFilterTag = function() {
            $body.toggleClass('bd-catalog-tags-active').removeClass('bd-import-active');
            updateCatalogMainMarginTop();
            if (!$body.hasClass('bd-catalog-tags-active')) {
                applyFilter(filterTags, null);
                filterItems();
            }
        };



        /**
         * Shows/hides batcn action toolbar based on selection as well as number of active/inactive items in selection
         *
         * @return {void}
         */
        var batchActionToolbar = function() {
            var $selection = $('.bc-s-selected', $body);
            $('.bd-catalog-selection-count', $body).text($selection.length);

            if ($selection.length > 1) {
                $('.bd-catalog-inline-confirm', $body).remove();
            }
        };

        /**
         * Handler for "Select" dropdown. Based on user search marks all/none/active/inactive items as selected.
         * @param  {String} selectSet - one of 'all', 'none', 'active', 'inactive'
         * @return {void}
         */
        var selectItems = function(selectSet) {
            $('.bd-catalog-item', $body).removeClass('bc-s-selected');
            switch (selectSet) {
                case 'all':
                    $('.bd-catalog-item:visible', $body).addClass('bc-s-selected').parent().addClass('bc-s-active');
                    break;
                case 'none':
                    $('.bd-catalog-item', $body).removeClass('bc-s-selected').parent().removeClass('bc-s-active');
                    break;
                default:
                    break;
            }
            bc.component.checkSelectedItems($body, $('.bc-s-wrapper', $body));
            batchActionToolbar();
        };



        var getSelectedItems = function() {
            var selected = [];
            $('.bc-s-selected', $body).each(function(idx, item) {
                selected.push(String($(item).data('name')));
            });
            return selected;
        };



        var actionConfirm = function(config) {
            var items = config.itemName;
            if (typeof items === 'string') {
                var $item = $('.bd-catalog-item[data-name="' + items + '"]', $body);

                var confirm = cmUtils.getHtmlTemplate('confirm.html', config);
                var $confirm = $item.append(confirm).find('.bd-catalog-inline-confirm');

                $confirm.on('click', function(evt) {
                    evt.stopPropagation();
                });

                $confirm.on('click', '> .buttons > .ok', function() {
                    $confirm.off().remove();
                    if (config.okCallback) {
                        config.okCallback();
                    }
                });

                $confirm.on('click', '> .buttons > .cancel', function() {
                    $confirm.off().remove();
                    if (config.cancelCallback) {
                        config.cancelCallback();
                    }
                });
            } else {
                be.utils.confirm({
                    title: config.title,
                    message: config.text,
                    okBtnText: config.okLabel,
                    cancelBtnText: config.cancelLabel,
                    yesCallback: function() {
                        if (config.okCallback) {
                            config.okCallback();
                        }
                    },
                    noCallback: function() {
                        if (config.cancelCallback) {
                            config.cancelCallback();
                        }
                    }
                });
            }
        };


        var removeItemFromCatalog = function(itemName, callback) {
            actionConfirm({
                itemName: itemName,
                title: TXT_CATALOG_REMOVE_TITLE[isPortalCatalog ? 'portal' : 'enterprise'](itemName instanceof Array ? itemName.length : 1),
                text: TXT_CATALOG_REMOVE_TEXT[isPortalCatalog ? 'portal' : 'enterprise'],
                okLabel: TXT_CATALOG_DELETE_OK,
                cancelLabel: TXT_CATALOG_DELETE_CANCEL,
                okCallback: function() {
                    function refresh (msg) {
                        return function() {
                            if(typeof msg === 'object'){
                                bc.component.notify(msg);
                            }
                            loadItems(renderItemList, true);
                            if (callback) {
                                callback(itemName);
                            }
                        };
                    }
                    var itemsToRemove = [];
                    var pagesToRemove = []; // Master pages should be removed differently

                    var onSuccess = refresh({
                        uid: '04018',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: TXT_CATALOG_DELETE_SUCCESS
                    });

                    var onError = function(xhr, textStatus) {
                        if(!textStatus) {
                            textStatus = xhr && xhr.errorMessage || TXT_CATALOG_DELETE_ERROR;
                        }
                        refresh({
                            uid: '04019',
                            icon: 'error', // checkbox, attention, error, loading
                            message: textStatus
                        })();
                    };

                    if (typeof itemName === 'string') {
                        itemName = [itemName];
                    }

                    for (var i = 0, l = itemName.length; i < l; i++) {
                        var type = $('.bd-catalog-item[data-name="' + itemName[i] + '"]').data('type');
                        if (type === 'page') {
                            pagesToRemove.push(itemName[i]);
                        } else {
                            itemsToRemove.push({
                                name: itemName[i],
                                type: type
                            });
                        }
                    }

                    if (itemsToRemove.length){

                        var xmlText = cmUtils.getXmlTemplate('portalCatalogItem.xml', {
                            items: itemsToRemove,
                            deleteAction: true,
                            isCatalogOperation: true
                        });

                        be.utils.ajax({
                            url: isPortalCatalog ? API_PORTAL_CATALOG_DELETE : API_SERVER_CATALOG_DELETE,
                            type: 'POST',
                            data: xmlText,
                            encodeURI: false,
                            success: onSuccess,
                            error: onError
                        });
                    } else if (pagesToRemove.length) {
                        utils.forEach(pagesToRemove, function(name){
                            be.promise.deleteMasterPage(name, isPortalCatalog ? bd.selectedPortalName : undefined)
                                .then(utils.throttle(onSuccess, 200), onError); // small delay requred
                        });
                    }
                }
            });
        };


        var exportItem = function(itemName, itemType, modelOnly) {
            portalExport.exportItem(itemName, itemType, modelOnly, isPortalCatalog ? bd.selectedPortalName : undefined);
        };

        var duplicateItem = function(itemId){
            var data = itemData[itemId];

            be.promise.duplicateItem(data.name, data.type).done(function(ret){
                cmUtils.notify(cmUtils.applyTxtTpl(TXT_CATALOG_DUPLICATE_ITEM, [data.type, data.title]), 'checkbox');
                var itemName = ret[data.type].name;
                loadItems(renderItemList, true, itemName);
            });
        };

        var cleanupFormComponents = function() {
            importData = {};
            cancelImport();
            nativeDnD.set();
            dialogActive = false;
        };

        var showImportDialog = function () {
            dialogActive = true;

            var file = importData && importData.files && importData.files[importData.files.length - 1];

            portals.get({ps: '-1'}).then(function(portalsData) {

                var htmlContent = cmUtils.getHtmlTemplate('importDialog.html', {
                        fileName: file ? file.name : '',
                        portals: portalsData,
                        isIE: $.browser.msie,
                        isPortalCatalog: isPortalCatalog
                    });

                var handleImportResponse = function (doc) {
                    var $responseXML = $(doc),
                        $level = $responseXML.find('level'),
                        $message = $responseXML.find('message'),
                        messageText = $message.text(),
                        messageOptions;

                    $progressNotify.click(); // get rid of 'IMPORT IN PROGRESS' -- global var (within bd.CatalogManager)

                    if ($level.length > 0 && $message.length > 0) {
                        switch($level.text().toUpperCase()) {
                            case 'INFO':
                                if (messageText.toUpperCase() === 'SUCCESS') {
                                    cmUtils.notify(TXT_CATALOG_IMPORT_SUCCESS, 'checkbox');
                                    loadItems(renderItemList);
                                }
                                break;
                            case 'ERROR':
                                //error message could be too long and make illusion of stuck popup
                                //in this case we manually limit message delay to 5 seconds
                                messageOptions = (messageText && messageText.length > 70) ? {delay: 5000} : {};
                                cmUtils.notify(messageText, 'error', messageOptions);
                                break;
                        }
                        return;
                    }

                    //in case of unexpected response
                    cmUtils.notify(TXT_CATALOG_IMPORT_FAILURE, 'error');
                };

                var $importWidgetForm = bc.component.modalform({
                    uid: '04012',
                    width: '465px',
                    title: TXT_CATALOG_IMPORT_TITLE,
                    cls: 'bd-catalog-import-dialog',
                    content: htmlContent,
                    ok: TXT_CATALOG_IMPORT_OK,
                    okDisabled: !file,
                    cancel: TXT_CATALOG_IMPORT_CANCEL,
                    okCallback: function($form) {
                        var $manualUploadForm = $('form[target="uploadHelperIFrame"]');
                        // collect input toggles
                        var params = [];
                        if (!isPortalCatalog) {
                            if ($('.bd-catalog-import-toggle-portals input[type="checkbox"]', $form)[0].checked) {
                                $('.bd-catalog-import-dialog .bd-catalog-import-toggle-portal input[type="checkbox"]', $form).
                                each(function(idx, item) {
                                    var portalName;
                                    if (item.checked) {
                                        portalName = $(item).parents('.bd-catalog-import-toggle-portal').data('portal');
                                        // in case we're sending the helper form...
                                        $manualUploadForm.append('<input type="hidden" name="p" value="' + portalName + '">');
                                        // ... otherwhise we push it to the formData
                                        params.push({
                                            name: 'p',
                                            value: portalName
                                        });
                                    }
                                });
                            }
                        }

                        if (!$manualUploadForm.length && formDataManager.isEmpty()) { // pressed ok but no file selected
                            cmUtils.notify(TXT_CATALOG_IMPORT_MISSING_FILE, 'error');
                            return false;
                        } else { // pressed ok and send selected files
                            $progressNotify = $(cmUtils.notify(TXT_CATALOG_IMPORT_PROGRESS, 'loading', {delay: -1}).$template[0]);

                            if ($manualUploadForm.length) { // uload package via iFrame
                                be.utils.appendXSRFTagTo($manualUploadForm[0]);
                                $manualUploadForm[0].submit();
                            } else { // upload via formDataManager
                                var name = be.utils.getXSRFRequestHeaderName();
                                var token = be.utils.getXSRFCookie();
                                params.push({ name: name, value: token });
                                formDataManager.addItems(params);
                                formDataManager.sendForm(handleImportResponse, //success
                                    function(){ //unexpected error
                                        $progressNotify.click(); // get rid of 'IMPORT IN PROGRESS'
                                        cmUtils.notify(TXT_CATALOG_IMPORT_FAILURE, 'error');
                                    });
                            }

                            cleanupFormComponents();
                        }
                    },
                    cancelCallback: function() {
                        cleanupFormComponents();
                    }
                });

                // --- prepare the manual upload form and make nativeDnD interact with it (as drop zone)
                nativeDnD.set({
                    canvas: 'body',
                    dropZone: '.bd-fileupload-area',
                    allowedFileTypes: ALLOWED_IMPORT_PACKAGE_EXT.join('|'),
                    overCallback: overCallback,
                    dropCallback: function (e) { importDialogDropCallback.call(this, e) && $importWidgetForm.api.okBtnEnable(); },
                    leaveWindowCallback: leaveWindowCallback,
                    allowMultipleFiles: false
                });

                if (filePicker) { // get rid of previous one
                    filePicker.destroy();
                }

                filePicker = createFilePicker({
                    dropZone: '.bd-fileupload-area',
                    uploadUrl: uploadUrl,
                    inputName: 'package',
                    openCallback: function(e) {
                        var fileName = e.target.value,
                            $dropZone = this.options.$dropZone;

                        if (!fileName.match( new RegExp('\\.(?:' + ALLOWED_IMPORT_PACKAGE_EXT.join('|') + ')$', 'i') )) {
                            cmUtils.notify(TXT_CATALOG_IMPORT_UNSUPPORTED_FILE);
                            this.removeDetachedForm();
                            return false;
                        }

                        $dropZone.addClass('bd-added-file');
                        fileName = fileName.split(/(?:\\|\/)/);
                        fileName = fileName[fileName.length - 1];
                        $('.bd-has-file .bd-file-text', $dropZone).text(fileName);
                        formDataManager.reset();
                        $importWidgetForm.api.okBtnEnable();
                    },
                    responseCallback: handleImportResponse
                });
                // ---

                bc.component.toggleSwitch({
                    target: $('.bd-catalog-import-toggle-portals', $importWidgetForm),
                    uid: '04014',
                    checked: isPortalCatalog,
                    callback: function($toggle, $input) {
                        var $portalsList = $('.bd-catalog-portals-list', $importWidgetForm);
                        if ($input[0].checked) {
                            $portalsList.show();
                            if (!$portalsList.data('initiated')) {
                                $('.bd-catalog-import-toggle-portal', $portalsList).each(function(idx, item) {
                                    var dashboard = item.getAttribute('data-portal') === 'dashboard';
                                    bc.component.toggleSwitch({
                                        target: $(item),
                                        uid: '04015',
                                        checked: !dashboard,
                                        name: $(item).data('portal'),
                                        callback: function($pToggle, $pInput) {}
                                    });
                                });
                                $portalsList.data('initiated', true);
                            }
                        } else {
                            $portalsList.hide();
                        }
                    }
                });
            });
        };

        var itemClickHandler = function(evt) {
            var $target = $(evt.target),
                $item = $target.hasClass('bd-catalog-item') ? $target : $target.parents('.bd-catalog-item');

            if ($target.hasClass('bd-catalog-item-toolbar') || $target.parents('.bd-catalog-item-toolbar').length > 0) {
                evt.stopPropagation();
                cancelSelection();
                var actionType = $target.parent().attr('class').replace(/^.*bd-catalog-item-([^\s]+).*$/, '$1');
                switch (actionType) {
                    case 'settings':
                        showItemSettings(String($item.data('name')), $item.data('type'),  $item.data('extendedItemName'));
                        break;
                    case 'remove':
                        removeItemFromCatalog(String($item.data('name')));
                        break;
                    case 'duplicate':
                        duplicateItem($item.data('id'));
                        break;
                    case 'export':
                        if (canExportItems) {
                            exportItem(String($item.data('name')), $item.data('type'), !getItemModel(String($item.data('name'))).isExportable);
                        }
                        break;
                    default:
                        break;
                }

            } else if ($target.hasClass('bd-catalog-item') || $target.parents('.bd-catalog-item').length > 0) {
                cancelImport();
            }
            setTimeout(function() { // make sure batchActionToolbar is executed _after_ multiselect assignments
                batchActionToolbar();
            }, 1);
        };

        var checkIfItemExistsInPortalCatalog = function(extendedItemName, callback){
            be.utils.ajax({
                    url: API_PORTAL_CATALOG_READ + '?f=extendedItemName(eq)' + encodeURIComponent(extendedItemName),
                    dataType: 'xml',
                    cache: false,
                    encodeURI: false,
                    success: function(xml) {

                        var $xml = $(xml),
                            $items = $xml.find('catalog>');

                        if (callback) {
                            callback($items.length > 0);
                        }

                    }
            });
        };

        var addServerCatalogItem = function(data){
            checkIfItemExistsInPortalCatalog(data.extendedItemName, function(existInPortalCatalog) {
                var payload;
                var newItemName = data.name = existInPortalCatalog ? (data.extendedItemName + '_' + data.guid) : data.extendedItemName;
                payload = cmUtils.getXmlTemplate('addCatalogItem.xml', data);

                var sUrl = (data.type == 'page') ? (be.contextRoot + '/portals/' + bd.selectedPortalName + '/masterpages/extend') : (be.contextRoot + '/portals/' + bd.selectedPortalName + '/catalog.xml');
                be.utils.ajax({
                    url: sUrl,
                    data: payload,
                    type: 'POST',
                    encodeURI: false,
                    success: function() {
                        cmUtils.notify('the item was successfully added to the portal catalog', 'checkbox');
                        loadItems(renderItemList, true, newItemName);
                    }
                });
            });

        };

        var imageErrorHandler = function(imgEl){
            jQuery(imgEl).parents('.bd-catalog-item').addClass('bd-catalog-default-icon');
        };

        var initToolbars = function() {
            /* Item types filter */
            var applyTypeFilter = function(id){
                $catalogMain.scrollTop(0);
                applyFilter(filterType, id);
                filterItems(true);
            };
            var availableItems = isPortalCatalog ? supportedItemTypes.portal : supportedItemTypes.server;
            var options = availableItems.map(function(id){
                return {
                    name: txtCatalogFilter[id],
                    handler: function() {
                        applyTypeFilter(id);
                    }
                };
            });
            options.push({
                name: TXT_CATALOG_FILTER_ALL,
                value: 'all',
                handler: function() {
                    applyTypeFilter(null);
                }
            });

            bc.component.dropdown({
                target: $('.bd-catalog-item-types', $body),
                label: TXT_CATALOG_FILTER_ALL,
                uid: '04001',
                customHandlers: true,
                options: options
            });

            /* Sort dropdown */
            bc.component.dropdown({
                target: $('.bd-catalog-sort', $body),
                label: be.utils.cookie(bd.portal.portalName + '_sortFieldLabel') || TXT_CATALOG_SORT_DATE,
                uid: '04002',
                customHandlers: true,
                options: [{
                    name: TXT_CATALOG_SORT_TITLE,
                    handler: function() {
                        sortField = 'property.title';
                        be.utils.cookie(bd.portal.portalName + '_sortField', sortField);
                        be.utils.cookie(bd.portal.portalName + '_sortFieldLabel', TXT_CATALOG_SORT_TITLE);
                        loadItems(renderItemList);
                    }
                }, {
                    name: TXT_CATALOG_SORT_TYPE,
                    handler: function() {
                        sortField = 'type';
                        be.utils.cookie(bd.portal.portalName + '_sortField', sortField);
                        be.utils.cookie(bd.portal.portalName + '_sortFieldLabel', TXT_CATALOG_SORT_TYPE);
                        loadItems(renderItemList);
                    }
                }, {
                    name: TXT_CATALOG_SORT_DATE,
                    handler: function() {
                        sortField = 'createdTimestamp';
                        be.utils.cookie(bd.portal.portalName + '_sortField', sortField);
                        be.utils.cookie(bd.portal.portalName + '_sortFieldLabel', TXT_CATALOG_SORT_DATE);
                        loadItems(renderItemList);
                    }
                }]
            });

            if (sortDir === 'asc') {
                $('.bd-catalog-sort-dir > i').toggleClass('bi-pm-sort-asc bi-pm-sort-desc');
            }

            $('.bd-catalog-sort-dir', $body).off().on('click', function() {
                var $btn = $(this);
                $('> i', $btn).toggleClass('bi-pm-sort-asc bi-pm-sort-desc');
                sortDir = $('> i', $btn).hasClass('bi-pm-sort-desc') ? 'dsc' : 'asc';
                be.utils.cookie(bd.portal.portalName + '_sortDir', sortDir);
                loadItems(renderItemList);
            });

            /* Select actions dropdown */
            bc.component.dropdown({
                target: $('.bd-catalog-select', $body),
                label: 'SELECT',
                uid: '04003',
                customHandlers: true,
                staticLabel: true,
                options: [{
                    name: TXT_CATALOG_SELECT_ALL,
                    handler: function() {
                        selectItems('all');
                        bc.keepSelection = true;
                        batchActionToolbar();
                    }
                }, {
                    name: TXT_CATALOG_SELECT_NONE,
                    handler: function() {
                        selectItems('none');
                        bc.keepSelection = true;
                        batchActionToolbar();
                    }
                }]
            });

            /* Import button */
            $('.bd-catalog-import', $body).off().on('click', function() {
                cancelSelection();
                showImportDialog();
            });

            /* Create button */
            $('.bd-catalog-create', $body).off().on('click', function() {
                showItemSettings('', 'widget');
            });

            /* tags button */
            $tagsArea = $('.bd-catalog-tags-area', $body);
            $('.bd-catalog-tags-btn', $body).off().on('click', toggleFilterTag);
            $body.on('click', '.bd-catalog-tag-name', function(evt) {
                var $tag = $(evt.target);
                if ($tag.hasClass('bd-catalog-tag-filter-active')) {
                    $tag.removeClass('bd-catalog-tag-filter-active');
                    applyFilter(filterTags, null);
                } else {
                    $tag.addClass('bd-catalog-tag-filter-active').siblings().removeClass('bd-catalog-tag-filter-active');
                    applyFilter(filterTags, $tag.data('tag'));
                }

                filterItems(true);
            });

            /* Search box */
            $('.bd-catalog-search-box', $body).off().on('keyup', function() {
                clearTimeout(searchTimeout);
                var $searchBox = $(this);
                applyFilter(filterTitle, $searchBox.val());

                filterItems(true);
            });

            /* Cancel search button */
            $('.bd-catalog-search-cancel', $body).off().on('click', function() {

                $('.bd-catalog-item-types ul.bc-selectbox', $body)[0].dropdownOptions.find('a[data-value="all"]').trigger('click');
                applyFilter(filterType, null);

                $('.bd-catalog-search-box', $body).val('');
                applyFilter(filterTitle, null);

                filterItems();
            });

            /* Batch export button */
            $('.bd-catalog-export', $body).off().on('click', function(evt) {
                evt.stopPropagation();
                // var itemsToExport = getSelectedItems();
                cmUtils.notify('Batch export: coming soon');
            });

            /* Batch remove button */
            $('.bd-catalog-remove', $body).off().on('click', function(evt) {
                evt.stopPropagation();
                var itemsToRemove = getSelectedItems();
                removeItemFromCatalog(itemsToRemove);
            });

            $(window).off('resize.tagsPanel').on('resize.tagsPanel', function(){
                if ($body.hasClass('bd-catalog-tags-active')) {
                    updateCatalogMainMarginTop();
                }
            });
        };


        var renderUI = function(oGadgetBody, sGadgetUrl, portalCatalog) {

            $body = $(oGadgetBody);

            filters = [];

            filterType = {
                type: 'type'
            };
            filterTitle = {
                type: 'title'
            };
            filterTags = {
                type: 'tag'
            };
            isPortalCatalog = portalCatalog;

            uploadUrl = API_IMPORT + (isPortalCatalog ? '/' + bd.selectedPortalName : '');

            formDataManager = new FormDataManager.FormDataManager({
                url: uploadUrl,
                fileInputName: 'package',
                allowMultipleFiles: false
            });

            /* Init API URLs */
            if (isPortalCatalog) {
                API_PORTAL_CATALOG_READ = bd.contextRoot + '/portals/' + bd.selectedPortalName + '/catalog.xml';
                API_PORTAL_CATALOG_DELETE = bd.contextRoot + '/portals/' + bd.selectedPortalName + '/catalog/delete';
                API_PORTAL_CATALOG_TAGS = bd.contextRoot + '/portals/' + bd.selectedPortalName + '/tags/catalog?ps=-1';

                var $title = $('.bd-pageTitle', $body);
                $title.text($title.text() + ' | ' + bd.selectedPortalTitle);
            }

            checkIfItemsExportingIsAllowed().then(function () {
                loadItems(renderItemList);
                updateTags();
                initToolbars();
            });

            $catalogMain = $('.bd-catalog-main', $body);
            $catalogMain
                .off().on('click', '.bd-catalog-item', itemClickHandler)
                .on('scroll', scrollHandler);
        };



        // public functions
        return {
            renderUI: renderUI,
            urlID: 'server-catalog',
            addServerCatalogItem: addServerCatalogItem,
            imageErrorHandler: imageErrorHandler
        };
    }(jQuery));

    b$.module('bd.widgets.ServerCatalog', function() {
        this.Maximized = bd.CatalogManager.renderUI;
    });
    b$.module('bd.widgets.PortalCatalog', function() {
        this.Maximized = bd.CatalogManager.renderUI;
    });

    return bd.CatalogManager;
});

