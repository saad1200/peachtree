/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : app.js
 *  Description: Enterprize/Portal Catalog Manager
 *
 *  ----------------------------------------------------------------
 */
define([
    'zenith/promise',
    'zenith/http/portals',
    'zenith/core/errors-handler',
    'angular',
    'backbase.com.2012.sabre/widgets/CatalogBrowser/items-library',
    'dashboard/widgets/CatalogManager/CatalogManager',
    'jquery',
    'css!backbase.com.2012.sabre/widgets/CatalogBrowser/items-library.css',
    'backbase.com.2014.components/modules/fileupload/scripts/app'
], function(Promise, httpPortals, error, angular, itemsLibrary, CatalogManager, $) {

    'use strict';

    /*----------------------------------------------------------------*/
    /* Start the widget
    /*----------------------------------------------------------------*/
     var Runner = function($rootScope, Widget) {

        // Get url2state property
        var portalName = Widget.getPreference('portalName'),
            portalDetails;

        if (portalName && portalName !=='_'){

            portalDetails = httpPortals.portalDetails(portalName);

            $rootScope.isPortalCatalog = true;
            Promise.all(portalDetails)
                .then(function(portal) {
                    if(portal) {

                        // legacy
                        bd.selectedPortalName = portalName;
                        bd.selectedPortalTitle = portal.getProperty('title');
                        bd.selectedPortalOptimizedFor = portal.getProperty('DefaultDevice');
                        bd.selectedPortalTargetDevice = portal.getProperty('TargetedDevice');

                        // if this is the combined view (portal/server catalog):
                        // load portal catalog. itemsLibrary will be server catalog
                        var isCustomCatalog = typeof bd.widgets !== 'undefined' && typeof bd.widgets.PortalCatalog !== 'undefined';
                        if(isCustomCatalog){
                            CatalogManager.renderUI(Widget.body, undefined, true);
                        }
                        $(Widget.body).find('.pageDesignerWrapper').show();
                        itemsLibrary.makeLibraryWidget(Widget, isCustomCatalog);
                    }

                }, function(err) {
                    error.trigger(err);
                });
        } else {
            CatalogManager.renderUI(Widget.body, undefined, false);
        }

        //
    };

    return angular.module('CatalogManager', ['bbFileupload'])
        .run(['$rootScope', 'Widget', Runner]);

});
