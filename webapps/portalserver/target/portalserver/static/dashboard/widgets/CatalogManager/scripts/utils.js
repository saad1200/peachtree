/* globals be, bc, bd, $, Mustache */
/**
 * Copyright (c) 2013 Backbase B.V.
 */
define(function(require, exports, module) {
    'use strict';


    var TPL_HTML = be.contextRoot + '/static/dashboard/templates/html/CatalogManager/';
    var TPL_XML = be.contextRoot + '/static/dashboard/templates/xml/CatalogManager/';

    var defaultIcon = 'bi-pm-nav-page';
    var iconMap = {
        page: 'bi-pm-nav-master-page',
        widget: 'bi-pm-diamond',
        contenttype: 'bi-pm-structuredcontent-type',
        container: 'bi-pm-layout',
        feature: 'bi-pm-sharedfeatures',
        template: 'bi-pm-stamp',
        templatepage: 'bi-pm-page-stamp',
        templatelink: 'bi-pm-link-stamp',
        templatewidget: 'bi-pm-diamond-stamp',
        templatecontainer: 'bi-pm-layout-stamp'
    };

    function getHtmlTemplate (path, tplData) {
        return be.utils.processXMLTemplateByUrl(TPL_HTML + path, tplData);
    }

    function getXmlTemplate (path, tplData) {
        return be.utils.processXMLTemplateByUrl(TPL_XML + path, tplData);
    }

    function applyTxtTpl (tpl, data) {
        if (tpl && data) {
            return Mustache.to_html(tpl, data);
        }
    }

    function shrink(str, length){
        if (str.length > length) {
            return str.substr(0, length - 1) + '...';
        }
        return str;
    }


    function createUUID () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.
            replace(/[xy]/g, function(c) {
                var r = Math.random() * 16|0;

                return (c === 'x' ? r : (r&0x3|0x8)).toString(16);
            });
    }

    function notify (message, icon, additionalOptions) {
        return bc.component.notify({
            uid: createUUID(),
            icon: icon || 'attention',
            message: message
        });
    }



    function parseItem (itemXml, isPortalCatalog, cacheKillerFor) {
        var $item = $(itemXml),
            props = {},
            $props = $('> properties > property', $item),
            item = {};

        item.properties = [];

        $props.each(function(a, b){

            var $this = $(this),
                tmp = {},
                name = $this.attr('name'),
                value = $('value', $this);

            props[name] = value.text();

            if (['title', 'src', 'defaultStartFile', 'thumbnailUrl'].indexOf(name) === -1) {
                tmp.propName = name;
                tmp.propTitle = $this.attr('label') || name;
                tmp.propLabel = $this.attr('label');
                tmp.propValue = value.text();
                tmp.propType = value.attr('type');
                tmp.propViewHint = $this.attr('viewHint');
                tmp.propManageable = $this.attr('manageable');
                item.properties.push(tmp);
            }

        });

        item.type = itemXml.tagName;
        item.itemType = $('> type', $item).text().toLowerCase();
        item.ucType = item.type.toUpperCase();
        item.name = $('> name', $item).text();
        item.contextItemName = $('> contextItemName', $item).text();
        item.createdBy = $('> createdBy', $item).text();
        item.createdTimestamp = bd.date.formatDateTime($('> createdTimestamp', $item).text());
        item.lastModifiedBy = $('> lastModifiedBy', $item).text();
        item.lastModifiedTimestamp = bd.date.formatDateTime($('> lastModifiedTimestamp', $item).text());
        item.securityProfile = $('> securityProfile', $item).text();
        item.publishState = $('> publishState', $item).text();
        item.uuid = $('> uuid', $item).text();
        item.lockState = $('> lockState', $item).text();
        item.extendedItemName = $('> extendedItemName', $item).text();

        item.isExportable = item.ucType !== 'CONTAINER' || props.config;
        item.src = props.src;
        item.defaultStartFile = props.defaultStartFile;
        item.thumbnailUrl = props.thumbnailUrl;

        item.title = props.title;
        item.parsedThumbnailUrl = be.utils.replaceParams(props.thumbnailUrl, bd.oPortalClient.params) +
                                        (item.name === cacheKillerFor ? '?' + Math.random() : '');
        item.tags = [];
        item.notInPortalCatalog = item.contextItemName !== bd.selectedPortalName;

        $item.find('> tags > tag').each(function() {
            var $tag = $(this),
                tag = {};
            tag.type = $tag.attr('type');
            tag.name = $tag.text();
            if (tag.name === 'deprecated') {
                item.deprecated = true;
            }
            if (tag.name === 'cxpManager') {
                item.cxpManager = true;
            }
            item.tags.push(tag);
        });

        item.isWidget = item.type === 'widget';
        item.isTypedef = item.type === 'contenttype';

        item.icon = iconMap[item.type + item.itemType] ||iconMap[item.type] || defaultIcon;

        return item;
    }




    module.exports = {
        notify: notify,
        shrink: shrink,
        parseItem: parseItem,
        applyTxtTpl: applyTxtTpl,
        getXmlTemplate: getXmlTemplate,
        getHtmlTemplate: getHtmlTemplate
    };

});
