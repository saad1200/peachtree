/* globals $, jQuery, be, bd, bc, b$ */
/**
 * Copyright (c) 2015 Backbase B.V.
 */
define([
    'backbase.com.2014.components/scripts/filePicker',
    'backbase.com.2014.components/scripts/formDataManager',
    './utils',
    './dnd',
    'showdown',
    'xss'
], function(FilePicker, FormDataManager, cmUtils, nativeDnD, showDown, filterXSS) {
    'use strict';

    var TXT_CATALOG_THUMBNAIL_UNSUPPORTED_FILE = 'Unsupported file type. Please use JPG, JPEG, PNG, SVG or GIF files only.';
    var ALLOWED_THUMBNAIL_EXT = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

    var TXT_CATALOG_CREATE_SUCCESS = '{{0}} has been created successfully';
    var TXT_CATALOG_UPDATE_SUCCESS = '{{0}} has been updated successfully';

    // var TXT_CATALOG_IMPORT_SUCCESS = 'Item was successfully imported';
    var TXT_UPLOAD_FAILURE = 'Resource upload failed';
    var TXT_UPLOAD_MISSING_FILE = 'Please, select file you want to upload';
    var TXT_UPLOAD_PROGRESS = 'Resource uploading in progress';

    // old api: /bb-admin-api/resources
    var API_BASE_PATH = bd.contextRoot + '/bb-admin-api';
    var API_UPLOAD = API_BASE_PATH + '/itemresource';

    var importData = {};


    var $progressNotify;
    var uploadTimeout;

    function getItemTypeCaption(itemType) {
        var itemTypeCaption;
        switch (itemType) {
            case 'contenttype':
                itemTypeCaption = 'Content Type';
                break;
            case 'page':
                itemTypeCaption = 'Master Page';
                break;
            case 'container':
                itemTypeCaption = 'Layout';
                break;
            default:
                itemTypeCaption = itemType;
                break;
        }
        return itemTypeCaption;
    }

    function hideUploadProgress () {
        window.clearTimeout(uploadTimeout);
        if ($progressNotify && $progressNotify.length) {
            $progressNotify.click();
        }
        $progressNotify = null;
    }

    function showUploadProgress () {
        uploadTimeout = window.setTimeout(function(){
            $progressNotify = $(cmUtils.notify(TXT_UPLOAD_PROGRESS, 'loading', {delay: -1}).$template[0]);
        }, 2000);
    }


    /* filePicker Import */
    // for import dialog only
    var filePicker; // this is the instance of the file-upload fallback (later on in upload modal)
    var createFilePicker = function(options) {
        return new FilePicker.FilePicker({
            dropZone: options.dropZone || '.bd-fileupload-area',
            inputName: options.inputName || 'file',
            inputLabelClass: 'bd-input-trigger-label',
            immediateUpload: false,
            uploadUrl: options.uploadUrl,
            openCallback: options.openCallback,
            responseCallback: options.responseCallback
        });
    };



    var showItemSettings = function(itemName, itemType, isPortalCatalog, $body, serverCatalog, renderItemList, loadItems, updateTags, extendedItemName, readmeHTML) {

        var cancelImport = function() {
            $body.removeClass('bd-import-active');
        };

        var cleanupFormComponents = function() {
            importData = {};
            cancelImport();
            nativeDnD.set();
        };

        var overCallback = function(e) {
        };

        var leaveWindowCallback = function(e) {
            cancelImport();
        };



        itemName = String(itemName);

        var itemModel = serverCatalog.filter(function(item) {
            return item.name === itemName;
        })[0] || {
            isWidget: true,
            isNewWidget: true,
            thumbnailUrl: bd.contextRoot + '/static/dashboard/media/icons/icons-widgets_01-default.png'
        };

        itemModel.hasReadme = typeof readmeHTML !== 'undefined';

        itemModel.readmeHTML = readmeHTML;

        itemModel.thumbnailUrl = itemModel.thumbnailUrl ? itemModel.thumbnailUrl.trim() : '';

        itemModel.tagsList = itemModel.tags ? itemModel.tags.filter(function(item){
            return item.type === 'regular';
        }).map(function(item) {
            return item.name;
        }).join(', ') : '';

        itemModel.sectionTagsList = itemModel.tags ? itemModel.tags.filter(function(item){
            return item.type === 'section';
        }).map(function(item) {
            return item.name;
        }).join(', ') : '';

        itemModel.thumbnailUploadAPI = API_UPLOAD;

        var htmlContent = cmUtils.getHtmlTemplate('itemSettingsEdit.html', itemModel);

        var submitDialogForm = function($form) {
            var tagNameToObject = function(tag) {
                if (tag) {
                    return {
                        value: tag
                    };
                } else {
                    return null;
                }
            };
            var data = be.utils.getTemplateDataFromForm($form),
                xmlText, tplData;

            var parameterName = be.utils.getXSRFCookieName();
            delete data[parameterName];

            if (itemModel.isNewWidget) {
                tplData = data;
                tplData.name = new Date().getTime();
                tplData.tags = tplData.tags.split(',').map(tagNameToObject);
                if (tplData.tags.length > 0 && tplData.tags[0] !== null) {
                    tplData.hasTags = true;
                }
            } else {
                tplData = {
                    name: itemName,
                    type: itemType,
                    properties: []
                };
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        if (key === 'tags') {
                            tplData.tags = bd.getTagsDeltaModel($form[0].tags);
                            if (tplData.tags.length > 0 && tplData.tags[0] !== null && tplData.tags[0].value) {
                                tplData.hasTags = true;
                            }
                        } else if (key === 'sectionTags') {
                            tplData.sectionTags = bd.getTagsDeltaModel($form[0].sectionTags);
                            for (var i = 0, l = tplData.sectionTags.length; i < l; i++){
                                tplData.sectionTags[i].value = tplData.sectionTags[i].value.toLowerCase();
                            }
                            if (tplData.sectionTags.length > 0 && tplData.sectionTags[0] !== null && tplData.sectionTags[0].value) {
                                tplData.hasTags = true;
                            }
                        } else {
                            tplData.properties.push({
                                name: key,
                                value: data[key].replace(/^\s+|\s+$/g, ''),
                                type: $form.find('input[name="' + key + '"]').data('type') || 'string',
                                label: $form.find('input[name="' + key + '"]').data('label') || '',
                                viewHint: $form.find('input[name="' + key + '"]').data('viewhint') || '',
                                manageable: $form.find('input[name="' + key + '"]').data('manageable') || 'true'
                            });
                        }
                    }
                }
            }

            if (itemModel.isNewWidget) {
                xmlText = cmUtils.getXmlTemplate('serverCatalogItem.xml', tplData);
            } else {
                xmlText = cmUtils.getXmlTemplate('portalCatalogItem.xml', {
                    items: [tplData],
                    updateAction: true,
                    isCatalogOperation: itemModel.notInPortalCatalog
                });
            }
            var url = API_BASE_PATH + '/catalog.xml';
            if (isPortalCatalog && !itemModel.isCatalogOperation) {
                if(itemModel.isNewWidget){
                    url = API_BASE_PATH + '/portals/' + bd.selectedPortalName + '/catalog.xml';
                }else{
                    url = API_BASE_PATH + '/portals/' + bd.selectedPortalName + '/' + itemModel.type + 's/' + itemModel.name;
                }
            }

            be.utils.ajax({
                url: url,
                data: xmlText,
                type: itemModel.isNewWidget ? 'POST' : 'PUT',
                encodeURI: false,
                success: function() {
                    cmUtils.notify(
                        cmUtils.applyTxtTpl(itemModel.isNewWidget ?
                            TXT_CATALOG_CREATE_SUCCESS :
                            TXT_CATALOG_UPDATE_SUCCESS,
                            [getItemTypeCaption(itemModel.type)]
                        ), 'checkbox');

                    loadItems(renderItemList, null, null, itemName);
                    updateTags();
                }
            });
            cleanupFormComponents();
        };

        var itemTitle = itemModel.title ? itemModel.title : itemName;
        // --- prepare the manual upload form and make nativeDnD
        // --- interact with it (as drop zone) for the image upload

        var imageFormDataManager = new FormDataManager.FormDataManager({
            url: API_UPLOAD,
            allowMultipleFiles: false
        });



        function uploadThumbNail ($form) {
            var $imageInput = $('#thumbnailUrl', $form);
            var $manualUploadForm = $('form[target="uploadHelperIFrame"]');


            if (!importData.files) {
                submitDialogForm($form);
                return;
            }
            // finally upload....
            if (!$manualUploadForm.length && imageFormDataManager.isEmpty()) { // pressed ok but no file selected
                cmUtils.notify(TXT_UPLOAD_MISSING_FILE, 'error');
                return;
            } else { // pressed ok and send selected files

                // show dialog if slow connection
                showUploadProgress();

                if ($manualUploadForm.length) { // uload package via iFrame
                    $manualUploadForm.find('iframe').one('load', function() {
                        submitDialogForm($form);
                        loadItems(renderItemList);
                    });
                    $manualUploadForm[0].submit();
                } else { // upload via formDataManager

                    imageFormDataManager.sendForm(function(newUrl) { // success
                        hideUploadProgress();
                        // cmUtils.notify(TXT_CATALOG_IMPORT_SUCCESS, 'checkbox');

                        if(newUrl){
                            $imageInput.val(newUrl);
                        }

                        submitDialogForm($form);
                    }, function(){ // error
                        hideUploadProgress();
                        cmUtils.notify(TXT_UPLOAD_FAILURE, 'error');
                    });
                }

                cleanupFormComponents();
            }
        }


        var $html = bc.component.modalform({
            width: '450px',
            title: itemName ? itemType + ' settings: ' + itemTitle : 'New widget',
            uid: '04001',
            cls: 'bd-catalog-item-settings-form',
            content: htmlContent,
            ok: 'Save',
            cancel: 'Cancel',
            okCallback: function($form) {
                uploadThumbNail($form); // will call submitDialogForm on sucess!??
            },

            cancelCallback: function() {
                cleanupFormComponents();
            }
        });



        function showFile (file) {
            var fileReader = window.FileReader ? new FileReader() : undefined;

            if (fileReader && file) {
                fileReader.onload = function (e) {
                     $('.bd-catalog-settings-icon-preview', $html).attr('src', e.target.result);
                };
                fileReader.readAsDataURL(file);
            }
        }

        function imageUploadDropCallback (e) { // for nativeDnD
            var allowedFiles = this.options.allowedFileTypes;
                // $previewImage = $('.bd-catalog-settings-icon-preview', $html);

            if (e.dataTransfer.files[0].name.match( new RegExp('\\.(?:' + allowedFiles + ')$', 'i') )) {
                importData = e.dataTransfer;
                imageFormDataManager.addItems(importData);
                // show image that has been dropped
                showFile(importData.files[importData.files.length - 1]);
                cancelImport();
                filePicker.removeDetachedForm();
            } else {
                cmUtils.notify(TXT_CATALOG_THUMBNAIL_UNSUPPORTED_FILE);
                cancelImport();
            }
        }


        nativeDnD.set({
            canvas: 'body',
            dropZone: '.bd-fileupload-area',
            allowedFileTypes: ALLOWED_THUMBNAIL_EXT.join('|'),
            overCallback: overCallback,
            dropCallback: imageUploadDropCallback,
            leaveWindowCallback: leaveWindowCallback,
            allowMultipleFiles: false
        }).options.$dropZone.addClass('bd-added-file'); // ...because there is always an image...

        // -- reset file picker part
        if (filePicker) { // get rid of previous one
            filePicker.destroy();
        }
        filePicker = createFilePicker({
            dropZone: '.bd-fileupload-area',
            uploadUrl: API_UPLOAD,
            expectedResponse: /[a-zA-Z\/]/,
            openCallback: function(event) {
                var fileInput = this.$detachedForm.find('input[type="file"]')[0],
                    files = fileInput ? fileInput.files : undefined,
                    fileName = files[0] ? files[0].name || '' : '';

                if (!fileName.match( new RegExp('\\.(?:' + ALLOWED_THUMBNAIL_EXT.join('|') + ')$', 'i') )) {
                    cmUtils.notify(TXT_CATALOG_THUMBNAIL_UNSUPPORTED_FILE);
                    this.removeDetachedForm();
                    return false;
                }
                importData = { // fake the importData model...
                    files: [{name: fileName}]
                };

                showFile(files[0]);
                imageFormDataManager.reset();
            },
            responseCallback: function(html) {
                var newUrl = $(html).text();
                hideUploadProgress();

                if (newUrl.match(/[a-zA-Z\/]/)) {
                    // cmUtils.notify(TXT_CATALOG_IMPORT_SUCCESS, 'checkbox');
                    loadItems(renderItemList);
                    $('#thumbnailUrl', $html).val(newUrl);
                } else {
                    cmUtils.notify(TXT_UPLOAD_FAILURE, 'error');
                }
            }
        });

        // bc.component.toggleSwitch({ // switch between specify path/upload (image)
        //     target: $('.bd-catalog-path-upload-toggle', $html),
        //     uid: '04001',
        //     callback: function($toggle, $input) {
        //         var $upload = $html.find('.bd-catalog-settings-icon-upload'),
        //             $path = $html.find('.bd-catalog-settings-icon-path');

        //         if ($input[0].checked) {
        //             $path.show();
        //             $upload.hide();
        //         } else {
        //             $path.hide();
        //             $upload.show();
        //         }
        //     }
        // });


        var tabs = $('.bc-tab-labels .bc-tab-label', $html);
        tabs.click(function(evt) {
            var $tab = $(this);
            $('.bc-tab-content', $html).hide();
            $('.bc-tab-content' + $tab.data('for'), $html).show();
        });

        if (!isPortalCatalog) {
            bc.component.radioButton({
                target: '.bd-catalog-widget-backbase',
                color: 'white',
                uid: '04028',
                name: 'bd-catalog-widget-type',
                value: 'backbase',
                label: 'Backbase',
                checked: true,
                handler: function() {
                    $('.bd-catalog-settings-src', $html).attr('name', 'src');
                    $('.bd-catalog-settings-icon-preview', $html).attr('src', bd.contextRoot + '/static/dashboard/media/icons/icons-widgets_01-default.png');
                    $('.bd-catalog-settings-icon-value', $html).val(bd.contextRoot + '/static/dashboard/media/icons/icons-widgets_01-default.png');
                    $('.bd-catalog-settings-template', $html).val('Standard_Widget');
                }
            });
            bc.component.radioButton({
                target: '.bd-catalog-widget-w3c',
                color: 'white',
                uid: '04029',
                name: 'bd-catalog-widget-type',
                value: 'w3c',
                label: 'W3C',
                handler: function() {
                    $('.bd-catalog-settings-src', $html).attr('name', 'defaultStartFile');
                    $('.bd-catalog-settings-icon-preview', $html).attr('src', bd.contextRoot + '/static/backbase.com.2012.aurora/media/icons/icons-pmwidgets_w3c.png');
                    $('.bd-catalog-settings-icon-value', $html).val(bd.contextRoot + '/static/backbase.com.2012.aurora/media/icons/icons-pmwidgets_w3c.png');
                    $('.bd-catalog-settings-template', $html).val('W3C_Widget');
                }
            });
        }

        var $itemIcon = $('.bd-catalog-settings-icon', $html),
            $itemIconEdit = $('.bd-catalog-settings-icon-edit', $html);

        setTimeout(function() {
            if ($('#itemTags', $html).length > 0) {
                bd.initTags($('#itemTags', $html));
            }
            if ($('#sectionTags', $html).length > 0) {
                bd.initTags($('#sectionTags', $html));
            }
        }, 100);


        $itemIconEdit.on('click', '.bd-catalog-settings-icon-save', function(evt) {
            evt.preventDefault();
            var newVal = $('input[type="text"]', $itemIconEdit).val().replace(/^\s+|\s+$/, '');
            $('img', $itemIcon).attr('src', be.utils.replaceParams(newVal, bd.oPortalClient.params) + '?_=' + Math.random());
            $itemIcon.show();
            $itemIconEdit.hide();
        });

        $itemIconEdit.on('click', '.bd-catalog-settings-icon-cancel', function(evt) {
            evt.preventDefault();
            $('input[type="text"]', $itemIconEdit).val($('img', $itemIcon).attr('src'));
            $itemIcon.show();
            $itemIconEdit.hide();
        });
    };

    /**
     *    If deferred resolved empty means that we do not show the Readme tab in settings.
     *    If deferred resolved with null we have a proper type of item to show the Readme type, but README.md was not found.
     *
     *    @param {string} itemName - Name of the item
     *    @param {string} itemType - Type of the item
     *    @param {string} extendedItemName - Name of the item from which it extended
     *    @returns {object} jQuery Deferred object
     * */
    function fetchReadme(itemName, itemType, extendedItemName) {
        if (itemType !== 'widget') return $.Deferred().resolve();

        function loadReadme(itemName) {
            return be.utils.loadTemplateByUrl(
                be.contextRoot + '/static/widgets/[BBHOST]/' + itemName + '/README.md',
                true
            );
        }

        return loadReadme(itemName)
            .then(undefined, function() {
               return !extendedItemName ? $.Deferred().reject() : loadReadme(extendedItemName);
            })
            .then(function(markdown){
                return filterXSS((new showDown.Converter()).makeHtml(markdown));
            });
    }

    return {
        show: function(itemName, itemType, isPortalCatalog, $body, serverCatalog, renderItemList, loadItems, updateTags, extendedItemName) {
            var args = Array.prototype.slice.call(arguments);
            var self = this;
            fetchReadme(itemName, itemType, extendedItemName)
                .done(function(readmeHTML) {
                    args = args.concat(readmeHTML);
                    showItemSettings.apply(self, args);
                })
                .fail(function() {
                    args = args.concat(null);
                    showItemSettings.apply(self, args);
                    console.warn('README.md was not found in the root of widget folder');
                });
        }
    };
});
