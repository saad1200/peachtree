/**
 * Copyright � 2011-2013 Backbase B.V.
 */
b$.module("bd.widgets.GroupMgmt", function () {
    'use strict';
    var DataSource = b$.require('bd.DataSource'),
        PagedList = b$.require('bd.PagedList'),
        emptyString = '',
        noop = function (){};

    function applyManagementPermission(element) {
        if (!bd.uiEditingOptions.usersAndGroups.allowManagement) {
            var managedElements = [];
            if (element) {
                managedElements = jQuery(element).find('.bd-managedElement');
            } else {
                managedElements = jQuery('.bd-managedElement');
            }
            jQuery.each(managedElements, function (index, element) {
                jQuery(element).remove();
            });
            return false;
        }
        return true;
    }

    this.Maximized = function (oGadgetBody) {

        var widgetContentWrapper = jQuery('.bd-groupManagement', oGadgetBody)[0],
            selectedGroupName = emptyString,
            selectedGroupRole = emptyString,
            selectedUserName = emptyString,
            selectedUserRole = emptyString,
            groupSelectorAction = emptyString,      //variable indicate group selector action
            bucketSize = 0,
            adminUser = 'admin',                    //define the name of super admin and manager
            managerUser = 'manager',
            adminGroup = 'admin',                   //define the name of super admin and manager group
            managerGroup = 'manager',
            sys2sysGroup = 'sys2sys',
            $editUserContainer = jQuery('.bd-editUserFormContainer', oGadgetBody),
            userPageList = new PageList(),          //pagination for user
            groupPageList = new PageList(),
            $column2,
            loadUsersList,
            loadGroupsDetails,
            reloadGroupDetails,
            $groupColumn,
            $groupViewport,
            $groupDetailsColumn,
            $groupDetailsViewport,
            groupList,
            groupOnSelect,
            openGroupCreationPanel,
            closeGroupCreationPanel,
            groupCreation,
            $groupEditingFormContainer,
            closeGroupEditingForm,
            groupEditing,
            $column3,
            $userColumn,
            $userViewport,
            $userDetailsColumn,
            $userDetailsViewport,
            userGroupList,
            userDetailList,
            selectUser,
            unSelectUser,
            userOnSelect,
            userList,
            selectAllUsers,
            unselectAllUsers,
            openUserCreationPanel,
            closeUserCreationPanel,
            userCreation,
            $passwordNew = null,    // User edition
            $passwordAgain = null,
            $trPassword = null,
            $trPasswordNew = null,
            $trPasswordAgain = null,
            closeUserEditingForm,
            userEditSubmition,
            $groupSelectorPane,
            groupSelectorDS,
            userGroupsDS,
            anim = null,
            openGroupSelectorPanel,
            groupSelectorPanelRender,
            groupSelectorPanelInit,
            submitNewGroupSetting,
            saveGroupSelection,
            checkUserRole,
            $bucketColumn,
            $bucketViewport,
            refreshBucketState,
            addToBucket,
            removeFromBucket,
            updateUserStateInBucket,
            activateTab,
            deActivateTab,
            column3Action,
            dynamicHideSelectedUserTab,
            showEditPassword,
            hideEditPassword,
            resetPasswordField,
            formValidation,
            popupForm,
            xmlToJson,
            getData;

        /*
         * Column 2
         */
        $column2 = jQuery('.bd-column2Container', widgetContentWrapper);

        $column2.undelegate('.bd-columnHeader .bd-tab1', 'click').delegate('.bd-columnHeader .bd-tab1', 'click', function () {
            // show user list
            loadUsersList();
            closeGroupEditingForm();
        });

        $column2.undelegate('.bd-columnHeader .bd-tab2', 'click').delegate('.bd-columnHeader .bd-tab2', 'click', function () {
            // load group details
            loadGroupsDetails();
            closeUserEditingForm();
        });

        loadUsersList = function () {
            // load the list of users
            var url = '',
                groupInfoTag = $column2.find('.bd-tabLabel.bd-tab2'),
                selectAll = jQuery('.bd-selectall-list .bc-checkbox-select', $column2)[0],
                selectAllDropdown,
                ddoptions;

            if (!selectAll) {
                selectAllDropdown = bc.component.dropdown({
                    target: '.bd-selectall-list',
                    type: 'all-select',
                    uid: '4356'
                });

                ddoptions = selectAllDropdown[0].dropdownOptions;

                ddoptions.delegate('.bc-selectall', 'click', selectAllUsers);
                ddoptions.delegate('.bc-selectnone', 'click', unselectAllUsers);

                jQuery('.bc-custom-checkbox').click(function () {
                    var callback = jQuery(this).hasClass('c_on') ? selectAllUsers : unselectAllUsers;
                    callback();
                });

                ddoptions.delegate('input', 'change', function () {
                    var callback = this.checked ? selectAllUsers : unselectAllUsers;
                    callback();
                });
            }

            if (selectedGroupName === 'AllUsers') {
                url = '/users.xml?s=username(asc)&of=' + userPageList.getOffSet() + '&ps=' + userPageList.getPageSize();
                //hide the group info tag
                groupInfoTag.hide();
                //hide the "add user" button for "all users" group
                $column2.find('.bd-openPaneButton').hide();
            } else {
                url = '/groups/' + selectedGroupName + '/users.xml?s=username(asc)&of=' + userPageList.getOffSet() + '&ps=' + userPageList.getPageSize();
                groupInfoTag.show();
                //show the "add user" button
                $column2.find('.bd-openPaneButton').show();
            }
            jQuery('#addUserTag-Wrapper').show();

            userList(url);
            activateTab('1', $column2);

            column3Action('show');

        };

        loadGroupsDetails = function () {
            var reloadGroupDetailsDone = function () {
                activateTab('2', $column2);
                applyManagementPermission($groupDetailsViewport);
                jQuery('#addUserTag-Wrapper').hide();
                column3Action('hide');
            };
            if (selectedGroupName === 'AllUsers') {
                loadUsersList();
            } else {
                reloadGroupDetails('groupDetails', $groupDetailsViewport).done(reloadGroupDetailsDone);
            }
        };

        reloadGroupDetails = function (templateName, viewPoint) {

            var callback = function (json) {
                var id = json.group.id || '',
                    roles = {
                        'USER': 'None',
                        'MANAGER': 'Can manage portals',
                        'ADMIN': 'Can create portals & manage global settings',
                        'SYS2SYS': 'Not applicable'
                    },
                    description = typeof json.group.description === 'object' ? ' ' : json.group.description,
                    templateData = {
                        id: id,
                        name: json.group.name,
                        role: json.group.role,
                        isAdminGroup: selectedGroupName === adminGroup,
                        isSys2sysGroup: selectedGroupName === sys2sysGroup,
                        roleText: roles[json.group.role],
                        description: description
                    },
                    htmlContent = be.utils.processHTMLTemplate("groups/" + templateName, templateData);

                viewPoint.html(htmlContent);

                if ((templateName === 'editGroup') && (!templateData.isSys2sysGroup) && (!templateData.isAdminGroup)) {
                    bc.component.dropdown({
                        target: '.bd-group-edit-dropdown',
                        selected: templateData.role,
                        label: templateData.roleText,
                        forlabel: 'bd-groupForm-role-dropdownbox',
                        uid: '5436',
                        options: [
                            {name: 'None', value: 'USER'},
                            {name: 'Can manage portals', value: 'MANAGER'},
                            {name: 'Not applicable', value: 'SYS2SYS'}
                        ]
                    });
                }
            };

            return getData('/groups/' + selectedGroupName + '.xml', callback);

        };
        /*
         *  Groups
         */
        $groupColumn = jQuery('.bd-groups', widgetContentWrapper);
        $groupViewport = $groupColumn.find('.bd-viewportWrapper');
        $groupDetailsColumn = jQuery('.bd-groupDetails', widgetContentWrapper);
        $groupDetailsViewport = $groupDetailsColumn.find('.bd-groupDetailsViewport');

        //list the all groups on the first clomun
        groupList = function (param) {
            var url,
                callback = function (json) {
                    var $listDataholder,
                        params,
                        groups,
                        htmlContent = '',
                        insertAllUsersGroup,
                        node,
                        i;

                    applyManagementPermission(widgetContentWrapper); //turn off any management features if management is not allowed

                    $listDataholder = $groupViewport.find('.bd-listDataholder');

                    params = {
                        obj: json,
                        rootnode: 'groups',
                        childnode: 'group'
                    };

                    groups = groupPageList.pageSizeControl(params, $groupColumn);
                    for (i = 0; node = groups[i]; i++) {
                        htmlContent += '<a class="bd-listItem bd-autoTest-groupList-' + node.name + '" itemid="' + node.name + '" itemrole="' + node.role + '"> ' + node.name + ' </a>';
                        node = groups[i];
                    }
                    if (param && param.loadMore) {
                        $listDataholder.append(htmlContent);
                    } else {
                        $listDataholder.html(htmlContent);
                    }

                    insertAllUsersGroup = $listDataholder.find('a[itemid="AllUsers"]');
                    if (!insertAllUsersGroup || insertAllUsersGroup.length <= 0) {
                        $listDataholder.prepend('<a class="bd-listItem bd-autoTest-groupList-AllUsers" itemid="AllUsers"><i> All Users </i></a>');
                    }

                    jQuery('a', $listDataholder).unbind('click').click(function () {
                        groupOnSelect(this);
                    });
                };

            if (!param || !param.loadMore) {
                groupPageList.setOffSet(0);
            }

            url = '/groups.xml?s=name(asc)&of=' + groupPageList.getOffSet() + '&ps=' + groupPageList.getPageSize();
            getData(url, callback);
        };

        //trigger when a group is selected
        groupOnSelect = function (obj) {
            var groupInfoTag;
            selectedGroupName = jQuery(obj).attr('itemid');
            selectedGroupRole = jQuery(obj).attr('itemrole');

            jQuery(obj).addClass('bd-selected');
            jQuery(obj).siblings().removeClass('bd-selected');
            userPageList.setOffSet(0);

            groupInfoTag = $column2.find('.bd-tabLabel.bd-tab2');

            if (selectedGroupName) {
                if (selectedGroupName === adminGroup || selectedGroupName === managerGroup) {
                    $groupDetailsColumn.find('.bd-deleteButton').hide();
                } else {
                    $groupDetailsColumn.find('.bd-deleteButton').show();
                }

                // hide column2 tabs
                $column2.find('.bd-columnHeader .bd-content').show();

                // unselect current user
                unSelectUser();

                //PMFIVE-485: keep tab focus
                if (groupInfoTag.hasClass('bd-activeTab')) {
                    loadGroupsDetails();
                } else {
                    loadUsersList();
                }

                closeGroupEditingForm();
                $groupDetailsColumn.find('.bd-buttons').show();

            } else {
                $column2.find('.bd-columnHeader .bd-content').hide();
                $column2.find('.bd-columnEditor').hide();
            }
        };

        // group deletion
        $groupDetailsColumn
            .undelegate('.bd-deleteButton', 'click')
            .delegate('.bd-deleteButton', 'click', function () {

                var deleteGroup = function () {
                    be.utils.ajax({
                        url: bd.contextRoot + '/groups/' + selectedGroupName + '.xml',
                        success: function () {
                            groupList();

                            $groupViewport.find('a[itemid="' + selectedGroupName + '"]').removeClass('bd-selected');

                            selectedGroupName = null;

                            deActivateTab('2', $column2);
                            $column2.find('.bd-columnHeader .bd-content').hide();
                            unSelectUser();
                            bc.component.notify({
                                uid: '1123',
                                icon: 'checkbox', // checkbox, attention, error, loading
                                message: 'The group has been deleted.'
                            });
                        },
                        type: "DELETE"
                    });
                };

                be.utils.confirm({
                    title: 'Remove Group',
                    message: "This will remove the " + selectedGroupRole + " group, '" + selectedGroupName + "', which cannot be undone. Group users will not be deleted. Are you sure you want to continue?",
                    yesCallback: deleteGroup
                });
            });

        // Group creation
        // open/close create panel
        openGroupCreationPanel = function () {

            var prepareForm = function ($form) {//group creating submit validation
                var validateOpts = {
                    rules: {
                        name: {
                            validTitle: true
                        }
                    },
                    messages: {
                        name: {
                            validTitle: "Group name can not contain any special characters"
                        }

                    }
                };
                $form.attr({
                    action: 'groups',
                    method: 'post',
                    autocomplete: 'off'
                });

                formValidation($form, validateOpts);

                // Focus first input field on open
                $form.find('input:first').focus();
            };

            popupForm({
                templateName: 'createGroup',
                templateData: null,
                prepareForm: prepareForm,
                form: {
                    width: '650px',
                    height: '300px',
                    id: 'groupCreateForm',
                    ok: 'Save',
                    cancel: 'Cancel',
                    content: '',
                    title: 'NEW GROUP',
                    uid: 'groups',
                    cls: 'bd-tCont-presetSelect-form',
                    okCallback: groupCreation
                }
            });
        };

        closeGroupCreationPanel = function () {
            be.closeCurrentDialog();
        };

        $groupColumn
            .undelegate('.bd-openPaneButton', 'click')
            .delegate('.bd-openPaneButton', 'click', openGroupCreationPanel);

        //for creating new group
        groupCreation = function (form) {
            //init the form data
            var role = jQuery('select', form).attr('value'),
                templateData = {
                    name: form.name.value,
                    description: form.description.value,
                    role: role
                },
                successCallback = function () { // handle group creation submit
                    bd.Permission.setDefaultGroupPermission(templateData);
                    groupList(); //refresh the group list
                    closeGroupCreationPanel();
                    bc.component.notify({
                        uid: '1113',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: 'Group was created successfully.'
                    });
                },
                xmlContent = be.utils.processXMLTemplate("groups/createGroup", templateData);   //render the xml with the form data

            //sumbit the data
            return be.utils.ajax({
                url: bd.contextRoot + "/groups.xml",
                data: xmlContent,
                success: successCallback,
                error: function (data) {
                    var validator = jQuery('#groupCreateForm').validate();
                    if (data.responseText.indexOf("already exists") !== -1) {
                        validator.showErrors({"name": "This group name is already in use"});
                    }
                    if (data.responseText.indexOf("150 characters") !== -1) {
                        validator.showErrors({"description": "Please enter no more than 149 characters."});
                    }
                },
                type: "POST"
            });
        };

        // Group edition
        $groupEditingFormContainer = $groupDetailsColumn.find('.bd-editGroupFormContainer');


        $groupDetailsColumn
            .undelegate('.bd-editDetails', 'click')
            .delegate('.bd-editDetails', 'click', function () {
                var reloadGroupDetailsDone = function () {
                    $groupDetailsViewport.hide();
                    $groupEditingFormContainer.show();

                    //add the validation for group editing
                    var validateOpts = {
                        rules: {
                            name: {
                                validTitle: true
                            }
                        },
                        messages: {
                            name: {
                                validTitle: "Group name can not contain any special characters"
                            }

                        },
                        submitHandler: function (form) {
                            groupEditing(form);
                        }
                    };
                    formValidation('groupEditForm', validateOpts);
                    jQuery('#div-editGroupTitle').hide();
                    jQuery('.bd-group-det-btn').hide();
                    /*add*/
                };
                reloadGroupDetails('editGroup', $groupEditingFormContainer).done(reloadGroupDetailsDone);
            });


        // close group editing form and restore details view
        closeGroupEditingForm = function () {
            jQuery('#div-editGroupTitle').hide();
            jQuery('.bd-group-det-btn').show();

            $groupEditingFormContainer.hide();

            $groupDetailsViewport.show();
            $groupDetailsColumn.find('.bd-buttons').show();
        };

        // close on cancel click
        $groupEditingFormContainer.delegate('.bd-cancelButton', 'click', closeGroupEditingForm);

        //for group detail editing
        groupEditing = function (form) {

            var role = jQuery('select', form).attr('value'),
                templateData = {
                    id: form.id.value,
                    name: form.name.value,
                    description: form.description.value,
                    role: role
                },
                groupEditingErrorCallback = function (jqXHR) {
                    var validator = jQuery('#groupEditForm').validate(),
                        messages,
                        i,
                        lenMessages,
                        messageHtml = '';
                    if (jqXHR.responseText.indexOf("150 characters") !== -1) {
                        validator.showErrors({"description": "Please enter no more than 149 characters."});
                        return false;
                    }

                    messages = jQuery(jqXHR.responseXML).find("message");

                    if (messages != null) {
                        lenMessages = messages.length;
                        for (i = 0; i < lenMessages; i++) {
                            messageHtml += "<p>" + messages[i].textContent + "</p>";
                        }
                        if (messageHtml.length > 0) {
                            be.utils.alert({
                                title: "Update Failed",
                                message: messageHtml,
                                closeIcon: false
                            });
                        }
                    } else {
                        //TODO: output message somehow
                        validator.showErrors({"name": "This group name is already in use"});
                    }
                },
                reloadGroupDetailsDone = function () {
                    closeGroupEditingForm();
                    bc.component.notify({
                        uid: '9988',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: 'Group details updated successfully.'
                    });
                    //bd.operationCompleteMsg("Group details updated successfully.");
                },
                groupEditingSuccessCallback = function () {
                    /* PSFIVE-274, Workaround */
                    bd.Permission.setDefaultGroupPermission(templateData);
                    var newGroupName = form.name.value;
                    if (newGroupName !== selectedGroupName) {
                        selectedGroupName = newGroupName;
                        groupList();
                    }

                    reloadGroupDetails('groupDetails', $groupDetailsViewport).done(reloadGroupDetailsDone);
                },
                xmlContent = be.utils.processXMLTemplate("groups/updateGroup", templateData); //render the xml with the form data
            //submit the data
            be.utils.ajax({
                url: bd.contextRoot + "/groups/" + selectedGroupName + ".xml",
                data: xmlContent,
                success: groupEditingSuccessCallback,
                error: groupEditingErrorCallback,
                type: 'PUT'
            });

            return false;
        };

        /*
         * Column 3
         */

        $column3 = jQuery('.bd-column3Container', widgetContentWrapper);
        $column3
            .on('click', '.bd-columnHeader .bd-tab1', function () {
                // show user details
                selectUser();
            })
            .on('click', '.bd-columnHeader .bd-tab2', function () {
                // show bucket list
                activateTab('2', $column3);
            });

        /*
         * Users
         */
        $userColumn = jQuery('.bd-users', widgetContentWrapper);
        $userViewport = $userColumn.find('.bd-viewport');

        $userDetailsColumn = jQuery('.bd-userDetails', widgetContentWrapper);
        $userDetailsViewport = $userDetailsColumn.find('.bd-viewport');

        //userGroupsAddObserverChange
        userGroupList = function () {
            var callback = function (json) {
                var groups, groupStr, i, htmlContent;
                if (json.groups) {
                    if (json.groups.group) {
                        groups = json.groups.group;
                        groupStr = '';
                        if (groups.length) {
                            selectedUserRole = groups[0].role;
                            for (i = 0; i < groups.length; i++) {
                                groupStr += groups[i].name + ', ';
                            }
                            groupStr = groupStr.substring(0, groupStr.length - 2);
                        } else {
                            selectedUserRole = groups.role;
                            groupStr = groups.name;
                        }

                        htmlContent = '<span>' + groupStr + '</span>';

                        $userDetailsViewport.find('.bd-userDetailsGroupList').html(htmlContent);

                    } else {
                        selectedUserRole = '';
                    }
                }
            };

            applyManagementPermission($userDetailsViewport);

            getData('/users/' + selectedUserName + '/groups.xml', callback);
        };

        //userDetailsAddObserverChange
        userDetailList = function () {
            var callback = function (json) {
                var user = json.user,
                    templateData = {
                        username: user.username,
                        enabled: user.enabled === 'true' ? 'Yes' : 'No'
                    },
                    htmlContent = be.utils.processHTMLTemplate("groups/usersDetails", templateData);
                $userDetailsViewport.html(htmlContent);

                userGroupList();

                //TODO better prevention, change this when removing xsl
                //PMFIVE-449, Users from 'admin' group shouldn't have a possibility to delete themselves from Groups.
                //and you can't delete yourself as a logged in user.
                if (selectedUserName === adminUser || selectedUserName === managerUser || bd.loggedInUserId === selectedUserName) {
                    $userDetailsColumn.find('.bd-deleteButton').hide();
                    // comment out for now, hide the edit button from those user
                    $userDetailsColumn.find('.bd-editGroups').hide();
                } else {
                    $userDetailsColumn.find('.bd-deleteButton').show();
                    //comment out for now, hide the edit button from those user
                    $userDetailsColumn.find('.bd-editGroups').show();
                }
            };

            getData('/users/' + selectedUserName + '.xml', callback);
        };

        selectUser = function () {
            if (selectedUserName) {
                $column3.find('.bd-columnHeader .bd-content').show();

                userDetailList();

                activateTab('1', $column3);
                closeUserEditingForm();
            }
        };

        unSelectUser = function () {
            if (selectedUserName) {

                // if bucket contains selected user
                if (bucketSize) {
                    activateTab('2', $column3);
                } else {
                    // hide the whole user info tab
                    deActivateTab('1', $column3);

                    // hide tab buttons
                    $column3.find('.bd-columnHeader .bd-content').hide();
                }

                selectedUserName = null;
            }
        };

        //addObserverSelect
        //trigger when a user is selected
        userOnSelect = function (obj) {

            selectedUserName = jQuery(obj).attr('itemid');
            jQuery(obj).addClass('bd-selected');
            jQuery(obj).siblings().removeClass('bd-selected');

            if (selectedUserName) {
                selectUser();
            } else {
                $column3.find('.bd-columnHeader .bd-content').hide();
            }
            //hide the selected user tab dynamically
            dynamicHideSelectedUserTab();
        };

        //addObserverChange
        //list all the user in the clomun 2
        userList = function (urls, param) {
            var url,
                callback = function (json) {
                    var params, users, i, len, roleName, templateData, htmlContent, tmpUserList;
                    // show column sections depending on user selection
                    $userColumn.find('.bd-columnEditor').show();
                    $userColumn.find('.bd-columnHeader .bd-content').show();

                    params = {
                        obj: json,
                        rootnode: 'users',
                        childnode: 'user'
                    };

                    users = userPageList.pageSizeControl(params, $userColumn);
                    if (users.length) {
                        for (i = 0, len = users.length; i < len; i++) {
                            roleName = '';
                            if (users[i].groups.group) {
                                if (users[i].groups.group.length) {
                                    roleName = users[i].groups.group[0].role;
                                } else {
                                    roleName = users[i].groups.group.role;
                                }
                            } else {
                                roleName = null;
                            }
                            users[i].role = roleName;
                        }
                    } else {
                        if (users.totalSize !== "0") {
                            if (users.groups.group.length) {
                                roleName = users.groups.group[0].role;
                            } else {
                                roleName = users.groups.group.role;
                            }
                        }
                        users.role = roleName;
                    }

                    templateData = {
                        users: users.totalSize === "0" ? false : users
                    };

                    htmlContent = be.utils.processHTMLTemplate("groups/users", templateData);
                    if (param && param.loadMore) {
                        $userViewport.find('.bd-listDataholder').append(htmlContent);
                    } else {
                        $userViewport.find('.bd-listDataholder').html(htmlContent);
                        $userViewport.find('.bd-listItem[itemid="' + selectedUserName + '"]').addClass('bd-selected');
                    }
                    // check if user is present in the bucket and update check box state accordingly
                    tmpUserList = templateData.users;
                    if (templateData.users !== false && !templateData.users.length) {
                        tmpUserList = [templateData.users];
                    }

                    jQuery('a', $userViewport).each(function (idx) {
                        if (jQuery('[itemid="' + this.getAttribute('itemid') + '"]', $bucketViewport).length) {
                            jQuery('.bd-listCheckbox', this)[0].checked = true;
                        }
                        if (tmpUserList.length && tmpUserList[idx]) {
                            if (!tmpUserList[idx].groups) {
                                tmpUserList[idx].groups = [];
                            }
                            if (!tmpUserList[idx].groups.group) {
                                tmpUserList[idx].groups.group = [];
                            } else if (!tmpUserList[idx].groups.group.length &&
                                tmpUserList[idx].groups.group.id
                            ) {
                                tmpUserList[idx].groups.group = [tmpUserList[idx].groups.group];
                            }
                            this.groups = tmpUserList[idx].groups.group;
                        }
                    }).unbind('click').click(function () {
                        userOnSelect(this);
                    });

                    //issue PMFIVE-331
                    dynamicHideSelectedUserTab();
                    closeUserEditingForm();

                    applyManagementPermission($userViewport.find('.bd-listDataholder')[0]);
                };

            if (!param || !param.loadMore) {
                userPageList.setOffSet(0);
            }

            url = urls || '/groups/' + selectedGroupName + '/users.xml?s=username(asc)&of=' + userPageList.getOffSet() + '&ps=' + userPageList.getPageSize();

            if (selectedGroupName === "AllUsers") {
                url = '/users.xml?s=username(asc)&of=' + userPageList.getOffSet() + '&ps=' + userPageList.getPageSize();
            }

            getData(url, callback);
        };

        // select all users from the list
        selectAllUsers = function () {
            jQuery(this).parents('.bd-dropdowncheckbox').find('.bd-checkbox').each(function () {
                this.checked = true;
            });
            selectedUserName = null;
            jQuery('.bd-listCheckbox', $userViewport).each(function () {
                this.checked = true;
                updateUserStateInBucket.call(this);
            });
        };

        // unselect all users from the list
        unselectAllUsers = function () {
            jQuery(this).parents('.bd-dropdowncheckbox').find('.bd-checkbox').each(function () {
                this.checked = false;
            });
            selectedUserName = null;
            jQuery('.bd-listCheckbox', $userViewport).each(function () {
                this.checked = false;
                updateUserStateInBucket.call(this);
            });
        };


        // User creation
        // open/close creation panel
        openUserCreationPanel = function () {
            var prepareForm = function (dialog) {
                //add validation for the user creation form submitting
                //group creating submit validate
                var validateOpts = {
                    rules: {
                        username: {
                            validTitle: true
                        },
                        passwordAgain: {
                            equalTo: "#userCreateForm_password"
                        }
                    },
                    messages: {
                        username: {
                            validTitle: "Username can not contain any special characters"
                        },
                        passwordAgain: {
                            equalTo: "Please enter the same password as above"
                        }
                    }
                };

                formValidation(dialog, validateOpts);

                dialog.attr({
                    action: 'users',
                    method: 'post',
                    autocomplete: "off"
                });

                bd.enableSwitch("activateNewUser");

                // Focus first input field on open
                dialog.find('input:first').focus();
            };

            popupForm({
                templateName: 'createUser',
                templateData: {
                    groupname: selectedGroupName,
                    //leave the group description empty now
                    groupdescription: ''
                },
                prepareForm: prepareForm,
                form: {
                    width: '650px',
                    height: '300px',
                    ok: 'Save',
                    cancel: 'Cancel',
                    content: '',
                    id: 'userCreateForm',
                    title: 'NEW USER',
                    uid: 'groups',
                    cls: 'bd-tCont-presetSelect-form',
                    disableValidation: true,
                    okCallback: userCreation
                }
            });

        };

        closeUserCreationPanel = function () {
            be.closeCurrentDialog();
        };
        $userColumn.undelegate('.bd-openPaneButton', 'click').delegate('.bd-openPaneButton', 'click', openUserCreationPanel);


        //for creating new user
        userCreation = function (form) {
            if (!$(form).valid()) {
                return false;
            }

            var userCreatedErrorCallback = function () {
                    var validator = jQuery('#userCreateForm').validate();
                    validator.showErrors({"username": "This username is already in use"});
                },
                userCreatedSuccessCallback = function () {  // handle user creation submit
                    closeUserCreationPanel();
                    bucketSize = 0;
                    selectedUserName = null;
                    userList(null);
                    bc.component.notify({
                        uid: '9975',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: 'User was created successfully.'
                    });
                    deActivateTab('2', $column3);
                    $column3.find('.bd-tabLabel.bd-tab2').hide();
                },
                templateData = {    //init the form data
                    username: form.username.value,
                    password: form.password.value,
                    enabled: String(form.enabled.checked),
                    groupname: form.groupname.value,
                    //leave group description empty now
                    groupdescription: ''
                },
                xmlContent = be.utils.processXMLTemplate("groups/createUser", templateData);    //render the xml with the form data

            //sumbit the data
            return be.utils.ajax({
                url: bd.contextRoot + "/users.xml",
                data: xmlContent,
                success: userCreatedSuccessCallback,
                error: userCreatedErrorCallback,
                type: "POST"
            });
            //hide the 3rd column when add the new user
            deActivateTab('1', $column3);
        };

        // user deletion

        $userDetailsColumn.undelegate('.bd-deleteButton', 'click').delegate('.bd-deleteButton', 'click', function () {
            var deleteUser = function () {
                be.utils.ajax({
                    url: bd.contextRoot + '/users/' + selectedUserName + '.xml',
                    success: function () {
                        userList(null);
                        //issue PMFIVE-524, workaround
                        var tmpSelectedUserName = selectedUserName;
                        unSelectUser();
                        removeFromBucket(tmpSelectedUserName);
                    },
                    type: "DELETE"
                });
            };

            be.utils.confirm({
                title: 'Remove User',
                message: "This will remove the user and cannot be undone. Are you sure you want to continue?",
                yesCallback: deleteUser
            });
        });

        //open the user edit form

        $userDetailsColumn.undelegate('.bd-editDetails', 'click').delegate('.bd-editDetails', 'click', function () {
            var url = '/users/' + selectedUserName + '.xml',
                editUserFormRender_step2 = function () {
                    var validateOpts;
                    //init the password field variable
                    $passwordNew = $editUserContainer.find('.bd-userEditForm-passwordNew');
                    $passwordAgain = $editUserContainer.find('.bd-userEditForm-passwordAgain');
                    $trPassword = $editUserContainer.find('.bd-userEditForm-trPassword');
                    $trPasswordNew = $editUserContainer.find('.bd-userEditForm-trPasswordNew');
                    $trPasswordAgain = $editUserContainer.find('.bd-userEditForm-trPasswordAgain');

                    //user edit form validation
                    validateOpts = {
                        rules: {
                            username: {
                                validTitle: true
                            },
                            passwordAgain: {
                                equalTo: "#userEditForm_passwordNew"
                            }
                        },
                        messages: {
                            username: {
                                validTitle: "Username can not contain any special characters"
                            },
                            passwordAgain: {
                                equalTo: "Please enter the same password as above"
                            }
                        },
                        submitHandler: function (form) {
                            $editUserContainer.find('.bd-userEditForm-password').val($passwordNew.val());
                            userEditSubmition(form);
                        }
                    };
                    formValidation('userEditForm', validateOpts);

                    //hide the "password" and "password again" field
                    hideEditPassword();

                    //hide the viewpoint
                    $userDetailsViewport.hide();
                    $userDetailsColumn.find('.bd-buttons').show();
                    bd.enableSwitch("activateExistingUser");

                    //show the user edit form
                    $editUserContainer.show();

                    jQuery('#div-editUserTitle').hide();
                    jQuery('.bd-btns-hide').hide();
                    /*change*/
                },
                editUserFormRender = function (json) {
                    var username = json.user.username,
                        userEnabled = (json.user.enabled === 'true'),
                        isLoggedInUserInAdminGroup = (selectedGroupName === adminGroup),
                        templateData = {
                            action: 'users/' + selectedUserName,
                            id: json.user.id,
                            username: username,
                            //password : userJsonData['password'],
                            enabled: (userEnabled ? 'checked' : ''),
                            //for deactivating admin themselves
                            activeStatus: (userEnabled ? 'Yes' : 'No'),
                            activeRight: isLoggedInUserInAdminGroup, // only admins can change active state
                            // for password changing
                            editPasswordButton: ((isLoggedInUserInAdminGroup && username !== adminUser) ||
                                bd.loggedInUserId === username) // user can change own password, admins can change anyone's
                        },
                        htmlContent = be.utils.processHTMLTemplate("groups/editUser", templateData);
                    $editUserContainer.html(htmlContent);
                    editUserFormRender_step2();
                };

            getData(url, editUserFormRender);

        });

        // close user editing form and restore details view
        closeUserEditingForm = function () {
            $editUserContainer.hide();
            jQuery('#div-editUserTitle').hide();
            $userDetailsViewport.show();
            $userDetailsColumn.find('.bd-buttons').show();
        };

        // close on cancel click
        $editUserContainer.delegate('.bd-cancelButton', 'click', closeUserEditingForm);

        // for user edition submition
        userEditSubmition = function (form) {
            var templateData = {
                    id: form.id.value,
                    username: form.username.value,
                    password: form.password.value,
                    enabled: (form.enabled.checked === true ? 'true' : 'false')
                },
                xmlContent = be.utils.processXMLTemplate("groups/updateUser", templateData),
                userEditedErrorCallback = function (jqXHR) {
                    // TODO: better notification
                    if (jqXHR.responseText.indexOf("validation.user.noGroups, parameters:{}") > -1) {
                        be.utils.alert({message: "User update failed because this user is not in any groups."});
                    } else {
                        userDetailList();
                        closeUserEditingForm();
                        bc.component.notify({
                            uid: '1112',
                            icon: 'checkbox', // checkbox, attention, error, loading
                            message: 'User has been updated successfully.'
                        });
                    }
                },
                userEditedSuccessCallback = function () {
                    // a workaround for now
                    var newUserName = form.username.value;
                    if (newUserName !== selectedUserName) {
                        selectedUserName = newUserName;
                        userList(null);
                    }

                    userDetailList();
                    closeUserEditingForm();
                    bc.component.notify({
                        uid: '1008',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: 'User has been updated successfully.'
                    });
                };

            be.utils.ajax({
                url: bd.contextRoot + "/users/" + form.username.value + ".xml",
                data: xmlContent,
                success: userEditedSuccessCallback,
                error: userEditedErrorCallback,
                type: "PUT"
            });

            return false;
        };


        /* Groups selector pane */
        $groupSelectorPane = jQuery('.bd-groupsSelectorPane', widgetContentWrapper);
        //init two datasource
        groupSelectorDS = new DataSource();
        userGroupsDS = new DataSource();

        //function to open/show the group selector panel
        openGroupSelectorPanel = function (obj) {
            var pos, top, left;
            //sort the groups by the group name
            groupSelectorDS.setURL('groups.xml?s=name(asc)');
            groupSelectorDS.setPageSize(100);
            groupSelectorDS.requestPage();

            ///for groupSelectorPane position
            pos = jQuery(obj).offset();
            top = pos.top + 20;
            left = pos.left;

            anim = {
                css: {
                    opacity: 0.2,
                    top: top,
                    left: left
                },
                open: {
                    opacity: 1,
                    top: '+=10'
                },
                openDuration: 200,
                openEasing: 'swing',
                close: {
                    opacity: 0.2,
                    top: '+=10'
                },
                closeDuration: 200,
                closeEasing: 'swing'
            };
        };

        groupSelectorDS.addObserver('change', function () {
            //move the functionailty out and place in a function for re-use purpose
            groupSelectorPanelInit(groupSelectorDS.getJSON());
        });

        // function used for rendering the group selector panel
        groupSelectorPanelRender = function (jsonObj, action, userGroupsList) {
            var groups = jsonObj.groups.group,
                groupSelectorTitle = '',
                groupSelectorSaveButton = '',
                i,
                j,
                len;
            if (groups == null) {
                groups = [];
            } else if (groups.constructor !== Array) {
                groups = [groups];
            }

            for (i = groups.length - 1; i >= 0; i--) {
                groups[i].name = groups[i].name['#text'];
                groups[i].role = groups[i].role['#text'];
                groups[i].descr = groups[i].description && groups[i].description['#text'];
            }
            // do a correct rendering according to the action
            if (groupSelectorAction === 'add') {
                groupSelectorTitle = 'Add selection to the following groups';
                groupSelectorSaveButton = 'Save';
            } else if (groupSelectorAction === 'edit') {
                groupSelectorTitle = 'Group Membership for ' + selectedUserName;
                groupSelectorSaveButton = 'Save';
            } else if (groupSelectorAction === 'remove') {
                groupSelectorTitle = 'Remove selection from the following groups';
                groupSelectorSaveButton = 'Remove';
            }
            if (groups.length && userGroupsList.length) {
                for (i = 0, len = groups.length; i < len; i++) {
                    for (j = userGroupsList.length - 1; j >= 0; j--) {
                        if (groups[i].name === userGroupsList[j].name) {
                            groups[i].isChecked = true;
                            userGroupsList.splice(j, 1);
                            break;
                        }
                    }
                }
            }
            $groupSelectorPane = popupForm({
                templateName: 'groupselector',
                templateData: {
                    groupSelectorTitle: groupSelectorTitle,
                    groupSelectorSaveButton: groupSelectorSaveButton,
                    groups: groups
                },
                form: {
                    width: '650px',
                    height: '300px',
                    ok: 'Save',
                    cancel: 'Cancel',
                    content: '',
                    title: groupSelectorTitle,
                    uid: 'groups',
                    cls: 'bd-tCont-presetSelect-form',
                    okCallback: saveGroupSelection
                }
            });

            if (groupSelectorAction === 'edit') {
                $groupSelectorPane.find('.bd-listCheckbox').change(function () {
                    checkUserRole(this);
                });
            }
        };

        // init the checkbox and render the group selector panel
        groupSelectorPanelInit = function (jsonObj) {
            var groupsList = [];
            //init an empty array for holding the selected user
            userGroupsDS.userList = [];
            //if only single user, do a single push, oterhwise push all the selected user to the array
            if (groupSelectorAction === 'edit') {
                userGroupsDS.userList.push({userName: selectedUserName, userRole: selectedUserRole});
            } else {
                if (bucketSize > 0) {
                    $bucketViewport.find('div.bd-listItem').each(function () {
                        if (this.groups) {
                            groupsList = groupsList.concat(this.groups);
                        }
                        userGroupsDS.userList.push({
                            userName: jQuery(this).attr('itemid'),
                            userRole: jQuery(this).attr('itemrole')
                        });
                    });
                }
            }

            groupSelectorPanelRender(jsonObj, groupSelectorAction, groupsList);

            //init the checkbox status that inside the group selector panel
            if (groupSelectorAction === 'edit') {
                jQuery.each(userGroupsDS.userList, function () {
                    if (this.userName === adminUser || this.userName === managerUser) {
                        $groupSelectorPane.find('[itemid="' + this.userName + '"] input')[0].disabled = true;
                    }
                    userGroupsDS.setURL('users/' + this.userName + '/groups.xml');
                    userGroupsDS.requestPage();
                });
            }

        };

        //add observer for initing the checkbox status that inside the group selector panel
        userGroupsDS.addObserver('change', function () {

            var i, groupName, groups = userGroupsDS.getJSON().groups.group;
            if (groups == null) {
                groups = [];
            } else if (groups.constructor !== Array) {
                groups = [groups];
            }

            //check the checkbox for all the groups that the user belong to
            groupSelectorDS.currentSelectedGroups = [];

            for (i = 0; i < groups.length; i++) {
                groupName = groups[i].name['#text'];
                if (groupName) {
                    $groupSelectorPane.find('[itemid="' + groupName + '"]').each(function () {
                        jQuery('.bd-listCheckbox', this)[0].checked = true;
                    });
                }
            }

            //save all the checked checkbox - all the selected groups before editing group
            $groupSelectorPane.find('.bd-listItem .bd-listCheckbox:checked').each(function () {
                groupSelectorDS.currentSelectedGroups.push(jQuery(this).val());
            });

        });

        //for sumbitting the new group setting for the user
        submitNewGroupSetting = function (userListObj, jsonObj) {
            var tmpJsonObj = jsonObj.groups.slice(0),
                request = bd.contextRoot + '/users/' + userListObj.userName + '/groups',
                deleteGroupList = {},
                errorMsg = '',
                removeCount = 0,
                addToGroup,
                removeFromGroup,
                updateBucketItem;

            /** private functions ***START*** **/
            /** add to group */
            addToGroup = function () {
                var successCallback, xmlContent;
                deleteGroupList.groups = [];
                // handle group addition submit
                successCallback = function () {
                    if (groupSelectorAction === 'edit') {
                        userGroupList();
                    }
                    updateBucketItem(userListObj.userName, tmpJsonObj);
                    bc.component.notify({
                        uid: '9186',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: 'Group memberships have been updated.'
                    });
                    //bd.operationCompleteMsg("Group memberships have been updated.");
                };

                //render the xml with the form data
                xmlContent = be.utils.processXMLTemplate("groups/updateUserGroups", {"groups": tmpJsonObj});
                //sumbit the data
                be.utils.ajax({
                    url: request + '.xml',
                    data: xmlContent,
                    success: successCallback,
                    type: "POST",
                    dataType: 'xml'
                });

            };

            /** remove from group function for REMOVE */
            removeFromGroup = function (params) {
                var deleteGroupArray = null,
                    deleteSuccessCallback = function (groupName) {
                        //empty
                        updateBucketItem(userListObj.userName, groupName, true);
                    },
                    i,
                    l,
                    c,
                    defaultParams;

                if (params != null) {
                    deleteGroupArray = params.deleteGroupArray;
                } else {
                    deleteGroupArray = tmpJsonObj;
                }

                defaultParams = {
                    title: 'COULD NOT REMOVE USER FROM GROUP',
                    message: ' The following users could not be removed from their group:<br/>' +
                    '- user "manager" (from the group "manager")<br/>' +
                    '- user "admin" (from the group "admin")',
                    closeIcon: true,
                    uid: '8172',
                    respondToEscKey: true,
                    buttons: [{
                        title: 'OK',
                        type: 'white',
                        callback: noop
                    }]
                };

                function getAjaxConfig(groupName) {
                    return {
                        url: request + '/' + groupName + '.xml',
                        success: function () {
                            deleteSuccessCallback(groupName);
                        },
                        type: "DELETE"
                    };
                }

                //check the admin and manager
                for (i = 0, l = deleteGroupArray.length; i < l; i++) {
                    c = deleteGroupArray[i];
                    if ((c.groupName === adminGroup && userListObj.userName === adminUser) ||
                        (c.groupName === managerGroup && userListObj.userName === managerUser)   //check if the super-admin and super-manager is removing from thier group
                    ) {
                        bc.component.dialog(defaultParams);
                        //break;
                    } else {
                        be.utils.ajax(getAjaxConfig(c.groupName));
                    }
                }

            };

            updateBucketItem = function (currentUser, groupList, isDelete) {
                //userGroupList();
                var $updatedUser, i, len, currentUserGroup;
                $updatedUser = $bucketViewport.find('.bd-listItem[itemid="' + currentUser + '"]');
                len = groupList.length;
                if (!$updatedUser.length) {
                    return;
                }
                if (!isDelete) {
                    $updatedUser[0].groups = [];
                    for (i = 0; i < len; i++) {
                        $updatedUser[0].groups.push({
                            description: groupList[i].groupDescription,
                            name: groupList[i].groupName,
                            role: groupList[i].groupRole
                        });
                    }
                } else {
                    for (i = $updatedUser[0].groups.length - 1; i >= 0; i--) {
                        currentUserGroup = $updatedUser[0].groups[i];
                        if (currentUserGroup.name === groupList) {
                            $updatedUser[0].groups.splice(i, 1);
                            break;
                        }
                    }
                }
            };

            /** private functions ***END*** **/

            //do the correct action
            if (groupSelectorAction === 'add' || groupSelectorAction === 'edit') {
                addToGroup();
            } else if (groupSelectorAction === 'remove') {
                removeFromGroup();
            }

            return errorMsg;
        };

        //user edit group button

        $userDetailsColumn.undelegate('.bd-editGroups', 'click').delegate('.bd-editGroups', 'click', function () {

            //move the functionailty out and place in a function for re-use purpose
            groupSelectorAction = 'edit';
            openGroupSelectorPanel(this);
        });


        saveGroupSelection = function (form) {
            var $form = jQuery(form),
                groupListJson = {},
                errorMsg = '',
                checkedRoles = {};
            groupListJson.groups = [];
            groupListJson.allGroups = [];

            $form.find('.bd-listItem .bd-listCheckbox').each(function () {
                var $this = jQuery(this);
                // add only new groups
                if (this.checked) {
                    groupListJson.groups.push({
                        'groupName': $this.val(),
                        'groupRole': $this.attr('itemrole'),
                        'groupDescription': $this.siblings('[name="description"]').val()
                    });
                }
                groupListJson.allGroups.push({
                    'groupName': $this.val(),
                    'groupRole': $this.attr('itemrole'),
                    'groupDescription': $this.siblings('[name="description"]').val()
                });
            });

            if (!groupListJson.groups || groupListJson.groups.length <= 0) {
                be.utils.alert({message: 'User must belong to at least one group.'});
                return;
            }

            be.closeCurrentDialog();

            //move the functionailty out and place in a function for re-use purpose
            jQuery.each(userGroupsDS.userList, function () {
                errorMsg += submitNewGroupSetting(this, groupListJson) + '<br/>';
            });
            //loadGroupsDetails();
            userList(null);

            if (errorMsg.replace(/<br\/>/gi, '') !== '') {
                be.utils.alert({message: errorMsg});
            }
        };


        /**
         * Uer Role Checking
         */
        checkUserRole = function (checkedGroup) {
            if (selectedUserRole !== jQuery(checkedGroup).attr('itemrole')) {
                $groupSelectorPane.find('.bd-listCheckbox').attr('checked', false);
                jQuery(checkedGroup).attr('checked', true);
                selectedUserRole = jQuery(checkedGroup).attr('itemrole');
            }
        };

        /*
         * User bucket
         */
        $bucketColumn = jQuery('.bd-bucket', widgetContentWrapper);
        $bucketViewport = $bucketColumn.find('.bd-viewport');

        //bind click event for the "add to Groups" button in the "selected users" tab

        $bucketColumn.undelegate('.bd-addToGroups', 'click').delegate('.bd-addToGroups', 'click', function () {
            groupSelectorAction = 'add';
            openGroupSelectorPanel(this);
        });

        //bind click event for the "remove From Groups" button in the "selected users" tab

        $bucketColumn.undelegate('.bd-removeFromGroups', 'click').delegate('.bd-removeFromGroups', 'click', function () {
            groupSelectorAction = 'remove';
            openGroupSelectorPanel(this);
        });

        // set handler to clear the bucket
        $bucketColumn.delegate('.bd-clearBucket', 'click', function () {
            bucketSize = 0;
            $bucketViewport.html(''); // clear bucket html
            jQuery('.bd-listCheckbox', $userViewport).each(function () {
                this.checked = false;
            }); // deselect all user in user list

            refreshBucketState();
        });

        // set handler to delete users from the bucket
        $bucketViewport.delegate('.bd-listItem .bd-icon', 'click', function () {
            var item = this.parentNode;
            removeFromBucket(item.getAttribute('itemid'));
        });

        refreshBucketState = function () {

            bucketSize = bucketSize < 0 ? 0 : bucketSize;

            if (bucketSize > 0) {
                $bucketColumn.find('.bd-columnEditor').show();
            } else {
                $bucketColumn.find('.bd-columnEditor').hide();
                //issuee PMFIVE-520
                //No user details are displayed after removing the users from the list of selected users.
                selectUser();
            }

            $column3.find('.bd-columnHeader .bd-content').show();
            $column3.find('.bd-usersSelected').html(bucketSize);

            //hide the selected user tab dynamically
            dynamicHideSelectedUserTab();
        };

        addToBucket = function (sUserName, sUserRole, groups) {
            // add selected user to the bucket
            //var newUser = ['<div class="bd-listItem" itemid="', sUserName, '" itemrole="', sUserRole, '"><div class="bd-text-overFlow">', sUserName, '</div></div>'].join('');
            //$bucketViewport[0].innerHTML = $bucketViewport[0].innerHTML + newUser;
            var $newUser = jQuery('<div class="bd-listItem" itemid="' + sUserName + '" itemrole="' + sUserRole + '"><div class="bd-text-overFlow">' + sUserName + '</div></div>');
            $newUser[0].groups = groups;
            $bucketViewport.append($newUser);
            bucketSize++;
            refreshBucketState();
        };

        removeFromBucket = function (sUserName) {
            jQuery('[itemId="' + sUserName + '"]', $bucketViewport).remove();

            // try to uncheck user item in selection list
            jQuery('[itemid="' + sUserName + '"] input', $userViewport).each(function () {
                this.checked = false;

                // uncheck "select all" button
                $userColumn.find('.bc-selectbox input')[0].checked = false;
            });

            bucketSize--;
            refreshBucketState();
        };

        updateUserStateInBucket = function (event) {

            var itemId = this.parentNode.getAttribute('itemid'),
                $foundUserInBucket = jQuery('[itemid="' + itemId + '"]', $bucketViewport);

            if (this.checked) {

                if ($foundUserInBucket.length === 0) {
                    addToBucket(this.parentNode.getAttribute('username'), this.parentNode.getAttribute('itemrole'), this.parentNode.groups);
                }
            } else {
                // remove unchecked user from the bucket
                removeFromBucket(itemId);
            }

            if (event) {
                // stop propagation as we do not want default selec to happen on the user item (also switching tabs)
                event.stopPropagation();
            }

            // but we want the user to be selected anyway so we fake the pagedList selection
            $userViewport.find('.bd-selected').removeClass('bd-selected');
            if (selectedUserName) {
                selectedUserName = itemId;
                jQuery(this.parentNode).addClass('bd-selected');
            }
            // and finaly we show the bucket
            //hide the selected user tab dynamically
            if (bucketSize > 0) {
                activateTab('2', $column3);
                $column3.find('.bd-tabLabel.bd-tab2').show();
            } else {
                deActivateTab('2', $column3);
            }

        };

        // set handler to add users to the bucket when selecting their checkbox
        $userViewport.delegate('.bd-listCheckbox', 'change', updateUserStateInBucket);
        //removed the quick search function

        /*
         *  generic
         */
        activateTab = function (sTabNumber, oContext) {
            jQuery('.bd-tabLabel, .bd-tab', oContext).removeClass('bd-activeTab');
            jQuery('.bd-tab' + sTabNumber, oContext).addClass('bd-activeTab');
        };

        deActivateTab = function (sTabNumber, oContext) {
            jQuery('.bd-tab' + sTabNumber, oContext).removeClass('bd-activeTab');
        };

        column3Action = function (action) {
            var $headerTab = $column3.find('.bd-columnHeader'),
                $bodyTab = $column3.find('.bd-groupMgmt-column3Body');
            if (action === 'show') {
                $headerTab.find('.bd-tab1').removeClass('bd-hideTab');
                $headerTab.find('.bd-tab2').removeClass('bd-hideTab');
                $bodyTab.find('.bd-details').removeClass('bd-hideTab');
                $bodyTab.find('.bd-bucket').removeClass('bd-hideTab');
            } else {
                $headerTab.find('.bd-tab1').addClass('bd-hideTab');
                $headerTab.find('.bd-tab2').addClass('bd-hideTab');
                $bodyTab.find('.bd-details').addClass('bd-hideTab');
                $bodyTab.find('.bd-bucket').addClass('bd-hideTab');
            }

        };

        //dynamically hide the selected user tab
        dynamicHideSelectedUserTab = function () {
            if (bucketSize <= 0) {
                deActivateTab('2', $column3);
                $column3.find('.bd-tabLabel.bd-tab2').hide();
            } else {
                activateTab('2', $column3);
                $column3.find('.bd-tabLabel.bd-tab2').show();
            }

            if (selectedUserName) {
                $column3.find('.bd-tabLabel.bd-tab1').show();
            } else {
                $column3.find('.bd-tabLabel.bd-tab1').hide();
            }
        };


        //TODO: no more hard coded with html id
        //show the "password" and "password again" field in the userDetails tab
        showEditPassword = function () {

            if (!$passwordNew || !$passwordAgain) {
                $passwordNew = $editUserContainer.find('.bd-userEditForm-passwordNew');
                $passwordAgain = $editUserContainer.find('.bd-userEditForm-passwordAgain');
                $trPassword = $editUserContainer.find('.bd-userEditForm-trPassword');
                $trPasswordNew = $editUserContainer.find('.bd-userEditForm-trPasswordNew');
                $trPasswordAgain = $editUserContainer.find('.bd-userEditForm-trPasswordAgain');
            }

            $passwordNew.addClass('required');
            $passwordAgain.addClass('required');
            resetPasswordField();

            $trPassword.hide();
            $trPasswordNew.show();
            $trPasswordAgain.show();
        };

        //bind the click event to the editPasswordButton
        $editUserContainer.delegate('.bd-userEditForm-editPasswordButton', 'click', function (e) {
            e.preventDefault();
            showEditPassword();
        });

        //bind the click event to the cancel edit password button
        $editUserContainer.delegate('.bd-userEditForm-passwordCancelButton', 'click', function (e) {
            e.preventDefault();
            hideEditPassword();
        });

        //hide the "password" and "password again" field in the userDetails tab
        hideEditPassword = function () {
            //TODO batter coding
            //hide the jquery validate error msg
            $passwordNew.removeClass('required valid error');
            $passwordAgain.removeClass('required valid error');
            $editUserContainer.find('label[for="userEditForm_passwordAgain"],label[for="userEditForm_passwordNew"] ').remove();
            //PMFIVE-490 prevent submiting the encrypted password
            resetPasswordField();

            $trPassword.show();
            $trPasswordNew.hide();
            $trPasswordAgain.hide();
        };

        //clear the password error msg
        resetPasswordField = function () {
            $passwordNew.val('');
            $passwordAgain.val('');
        };


        //wrapper for jquery validate, do some own modification
        formValidation = function ($objForm, validateOptions) {
            var validator;
            if (typeof $objForm === 'string') {
                $objForm = jQuery('#' + $objForm);
            }
            be.utils.addCustomMethodToValidator(['validTitle']);
            validator = window.validator = $objForm.validate(validateOptions);
            validator.resetForm();
            $objForm.find('.valid').removeClass('valid');
        };

        /**
         *  utilities function
         *
         */
        popupForm = function (config) {
            var modal, proxyOkCallback;
            config.form.content = be.utils.processHTMLTemplate('groups/' + config.templateName, config.templateData);
            proxyOkCallback = config.form.okCallback;
            config.form.okCallback = function ($form) {
                return proxyOkCallback($form[0]);
            };

            modal = bc.component.modalform(config.form);

            if (config.prepareForm) {
                config.prepareForm(modal.find('form'));
            }

            if (config.templateName === 'createGroup') {
                bc.component.dropdown({
                    target: '.bd-groupForm-dropdownbox',
                    label: 'None',
                    forlabel: 'bd-groupForm-role-dropdownbox',
                    uid: '5436',
                    options: [
                        {name: 'None', value: 'USER'},
                        {name: 'Can manage portals', value: 'MANAGER'}
                    ]
                });
            }
            return modal;
        };

        // wrapper of bd.xmlToJson function
        xmlToJson = function (xml) {
            return bd.xmlToJson({xml: xml});
        };

        //for pagination
        function PageList() {
            //ps: page size, how many items will be showed per page
            var ps = 50,
                of = 0,
                totalSize = null,
                sliceArray = function (array) {
                    return array.slice(0, ps);
                };

            this.getPageSize = function () {
                return ps;
            };
            this.getOffSet = function () {
                return of;
            };
            this.setPageSize = function (PS) {
                ps = PS;
            };

            this.setOffSet = function (OF) {
                of = OF;
            };

            this.pageSizeControl = function (json, viewpoint) {
                var returnedJson = null, rootNode, childNode;
                if (json.obj[json.rootnode]) {
                    totalSize = Number(json.obj[json.rootnode].totalSize);
                    rootNode = json.obj[json.rootnode];
                    childNode = rootNode[json.childnode];
                    if (childNode) {
                        if (childNode.length > ps || (ps + of) < totalSize) {
                            returnedJson = sliceArray(childNode);
                            viewpoint.find('.bd-listLoadMoreButton').removeClass('bd-disabled');
                        } else {
                            returnedJson = childNode;
                            viewpoint.find('.bd-listLoadMoreButton').addClass('bd-disabled');
                        }
                    } else {
                        returnedJson = rootNode;
                        viewpoint.find('.bd-listLoadMoreButton').addClass('bd-disabled');
                    }
                }
                if (returnedJson.id && returnedJson.name) { //TODO: a batter way to check when only one user/group returned
                    returnedJson = [returnedJson];
                }
                return returnedJson;
            };

            this.loadMore = function (callback) {
                of += ps;
                if (callback) {
                    callback();
                }
            };
        }

        //get the xml data and return a json obj
        getData = function (url, callback) {
            return be.utils.ajax({
                url: bd.contextRoot + url,
                dataType: 'xml',
                success: function (data) {
                    var json = xmlToJson(data);
                    if (callback) {
                        callback(json);
                    }
                },
                cache: false,
                type: "GET"
            });
        };

        groupList(); // this will trigger the first request from construtor to list the group
        $groupColumn.delegate('.bd-listLoadMoreButton', 'click', function () {
            groupPageList.loadMore(function () {
                groupList({loadMore: true});
            });

        });
        $userColumn.delegate('.bd-listLoadMoreButton', 'click', function () {
            userPageList.loadMore(function () {
                userList(null, {loadMore: true});

            });

        });
    };

    this.Dashboard = function (oGadgetBody) {
        var groupDS, userDS;
        jQuery('.bd-widgetContent', oGadgetBody).html('<div class="bd-groups"></div><div class="bd-users"></div>');

        groupDS = new DataSource({
            'restURL': 'groups.xml' // this will trigger the first request from construtor
        });

        groupDS.addObserver('pagechange', function () {
            jQuery('.bd-groups', oGadgetBody).html('Groups <span class="bd-outcome-right">' + groupDS.getTotalRecordCount() + '</span>');
        });

        userDS = new DataSource({
            'restURL': 'users.xml' // this will trigger the first request from construtor
        });

        userDS.addObserver('pagechange', function () {
            jQuery('.bd-users', oGadgetBody).html('Users <span class="bd-outcome-right">' + userDS.getTotalRecordCount() + '</span>');
        });
    };

    this.startWidget = function (oWidget) {
        this.Maximized(oWidget.body.firstChild);
    };

});
