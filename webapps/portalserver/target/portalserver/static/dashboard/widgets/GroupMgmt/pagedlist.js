/**
 * Copyright © 2011 Backbase B.V.
 * This file provides XSLT paging support
 */

b$.module('bd', function() {

	var Class = b$.require('b$.Class');
	var Model = b$.require('b$.mvc.Model');
	var DataSource = b$.require('bd.DataSource');

	/*
	 * PagedList will try to map to a defined markup structure and CSS classnames
	 * within the provided viewport
	 * 
	 * [viewport]
	 * 	<div class="bd-listDataholder"></div>
	 *  <div class="bd-listPreviousPageButton"></div>	 
	 *  <div class="bd-listNextPageButton"></div>
	 *  <div class="bd-listPageDots"></div> // a template to clone navigation dots 
	 * [/viewport]
	 * 
	 * Extra Options (compared to datasource) are:
	 * 
	 * - keepContent: a boolean to specify new content should be added to already loaded content (useful for animation)
	 * 
	 */
	
	var PagedList = this.PagedList = DataSource.extend(function(oOptions) {

		this._$viewport = jQuery(oOptions['viewport']);
		this._keepContent = oOptions['keepContent'] || false;		
				
		// set a new viewport for the datasource
		oOptions['viewport'] = this._$viewport.find('.bd-listDataholder')[0];
		
		// call Super Class
		DataSource.call(this, oOptions);
				
		this._selectedItem = null;
		this._selectedItemId = null;
				
		if (this._$viewport.length == 0) {
			throw('PagedList constructor needs a viewport node option.');
		}

		var that = this;

		this._$viewport.delegate('.bd-listLoadMoreButton', 'click', function() {
			that.requestNextPage(/* keep content */ true); // always keep content for the "loadMore" button
		});

		this._$viewport.delegate('.bd-listNextPageButton', 'click', function() {
			that.requestNextPage(that._keepContent);
		});		
		
		this._$viewport.delegate('.bd-listPreviousPageButton', 'click', function() {
			that.requestPreviousPage(that._keepContent);
		});
		
		this._$viewport.delegate('.bd-listPageDots', 'click', function() {
			that.setPage(parseInt(this.getAttribute('page')));
			that.requestPage(that._keepContent);
		});			

		// add observers on the datasource
		this.addObserver('pagechange', this.checkPaging, this);

		// set handler to select a group
		this._$viewport.delegate('.bd-listItem', 'click', function() {
			that.selectItem(this);
		});
	});
	
	PagedList.prototype.checkPaging = function() {
		//check for remaing record and total record
		if (!this.getRemainingRecordCount() || this.getTotalRecordCount() <= 0) {
			this._$viewport.find('.bd-listLoadMoreButton').addClass('bd-disabled');
			this._$viewport.find('.bd-listNextPageButton').addClass('bd-disabled bd-listNextPageButton-disabled');
		}
		else {
			this._$viewport.find('.bd-listLoadMoreButton').removeClass('bd-disabled');
			this._$viewport.find('.bd-listNextPageButton').removeClass('bd-disabled bd-listNextPageButton-disabled');
		}
		
		if (this._offset > 0) {
			this._$viewport.find('.bd-listPreviousPageButton').removeClass('bd-disabled bd-listPreviousPageButton-disabled');
		}
		else {
			this._$viewport.find('.bd-listPreviousPageButton').addClass('bd-disabled bd-listPreviousPageButton-disabled');
		}

		var totalPages = this.getTotalPageCount();
		var $dots = this._$viewport.find('.bd-listPageDots');
		var currentPage = this.getCurrentPageNumber();
		
		
		// we need at least one dot to start working with
		if ($dots.length) {
			
			// only one page
			if (totalPages == 1) {
				$dots.hide();
			}
			else {
				// walk over the list of dots
				var maxLength = Math.max($dots.length, totalPages);
				for (var i = 0; i < maxLength; i++) {
					
					if (i >= $dots.length) {
						// create a new dot by cloning the first one
						$newDot = jQuery($dots[0]).clone().insertAfter($dots[i - 1]);
						//$dots.add($newDot); // not working !!????
						$dots = this._$viewport.find('.bd-listPageDots');
					}
	
					$dot = jQuery($dots[i]);
					
					if (i > totalPages-1) {
						$dot.remove();
					}
					else {
						$dot.attr('page', i);
						$dot.show();
						if (i == currentPage) {
							$dot.addClass('bd-active');
						}
						else {
							$dot.removeClass('bd-active');
						}
					}			
				} 
			}
		}
	};
	
	PagedList.prototype.selectItemById = function(sId) {
		var item = this._$viewport.find('a[itemid="' + sId + '"]')[0];
		this.selectItem(item);
	};	
	
	PagedList.prototype.selectItem = function(oItem) {

		// unselect any selected group
		this._$viewport.find('a[itemid="' + this._selectedItemId + '"]').removeClass('bd-selected');
/*
		// if no item provided, try to get the first one
		if (!oItem) {
			oItem = this._$viewport.find('.bd-listItem')[0] || null;	
		}
*/
		if (oItem) {
			// select clicked group
			this._$viewport.find(oItem).addClass('bd-selected');					
			this._selectedItem = oItem;
			this._selectedItemId = oItem.getAttribute('itemid');
		}
		else {
			// nothing to select
			this._selectedItem = null;
			this._selectedItemId = null;			
		}
		
		this.notifyObservers('select');
	};
	
	PagedList.prototype.unSelectItem = function(oItem) {
		this.selectItem(null);
	};	
	
	/*
	 * Getters
	 */
	PagedList.prototype.getSelectItem = function() {
		return this._selectedItem;
	};
	
	PagedList.prototype.getSelectItemId = function() {
		return this._selectedItemId;
	};
});
