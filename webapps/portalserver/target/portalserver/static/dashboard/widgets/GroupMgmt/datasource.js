/**
 * Copyright � 2011 Backbase B.V.
 */
b$.module('bd', function() {

var Class = b$.require('b$.Class');
var Model = b$.require('b$.mvc.Model');
var Observable = b$.require('b$.mvc.Observable');

// extend jQuery to handle a full REST protocol
function jQueryRestAjax(url, data, method, callback) {
	
	// be.utils.ajax is a wrapper built to handle Spring Security session expiration
	// we want all requests to go through this in order to redirect to login page when session expired
	
	//return $.ajax({
	return be.utils.ajax({
	type: method,
	url: url,
	contentType: 'text/xml',
	cache: false,
	data: data,
	processData: false,
	dataType: "xml",
	success: callback});
}

var Dashboard = this.Dashboard = Class.extend(function(oOptions) {
	oOptions = oOptions || {};
	this.clientSideXSL = true;
	//this.baseURL = bd.contextRoot + '/'; //TODO: do better than that

//	this.errorMessages = {
//		'requestFailed': 'An error occured: the request could not be completed.'
//	};
});

var dashboard = new Dashboard();

// an editable list constructor
var DataSource = this.DataSource = Observable.extend(function(oOptions) {
	Observable.call(this);

	// prevent null pointer on oOptions
	oOptions = oOptions || {};

	this._loadedPages = {}; // to keep track of page caching
	this._allLoaded = false;
	this._xslCallbackQueue = [];
	this._totalSize = 0;
	this._currentSize = 0;
	this._html = null;
	this._xml = null;

	// initialize datasource with options or default settings

	// Client-side paging works around the server limitation for returning totalSize value
	this._clientSidePaging = oOptions['clientSidePaging'] || false;
	this._preRenderAll = oOptions['preRenderAll'] || false;
	this._useXSL = oOptions['xslURL'] != undefined;
	this.setPageSize(oOptions['pageSize'] || 5);
	this.setURL(oOptions['restURL'] || '');
	this.setOffset(oOptions['offset'] || 0);
	this.setFilter(oOptions['filterField'] || '', oOptions['filterValue'] || '', oOptions['filterOperator'] || '');
	this.setSorting(oOptions['sortingField'] || '', oOptions['sortingOrder'] || '');
	this.setViewport(oOptions['viewport']);

	if (dashboard.clientSideXSL && this._useXSL) {
		this.loadXSL(oOptions['xslURL']);
	}

	this.setXSLParams(oOptions['xslParams']);

	// if a rest URL is already provided, request content immedialty
	if (oOptions['restURL']) {
		this.requestPage();
	}
});

/*
 * Setters
 */
DataSource.prototype.setViewport = function(oViewport) {
	this._viewport = oViewport || document.createDocumentFragment(); // if not viewport, store returned HTML in a fragment
};

// set the REST API URL
DataSource.prototype.setURL = function(sURL) {
	this._URL = sURL;
};

DataSource.prototype.setPageSize = function(iPageSize) {
	this._pageSize = iPageSize;
	this.setOffset();
};

// set the paging object
DataSource.prototype.setPage = function(iPage) {
	this.setOffset(iPage * this._pageSize);
};

// set the paging object
DataSource.prototype.setOffset = function(iOffset) {

	// keep some value for observers
	this._lastPage = this._page || 0;
	this._lastOffset = this._offset || 0;

	if (iOffset != undefined) {
		this._offset = iOffset;
	}
	else {
		this._offset = this._offset || 0;
	}

	this._page = this.getCurrentPageNumber();
	var pageSize = this._pageSize;
	var offset = this._offset;

	// force a full request to buffer response
	if (this._clientSidePaging) {
		offset = 0;
		pageSize = 1000;
	}

	this._paging = [
		'of=',
		offset,
		'&ps=',
		pageSize
		].join('');
};

// set the filtering object (search feature)
DataSource.prototype.setFilter = function(sField, sValue, /* optional */ sOperator) {

	// set filter only if both field and value are provided
	if (sField && sValue) {
		this.filter = [
			'f=',
			sField,
			'(', sOperator || 'eq', ')',
			sValue
			].join('');
	}
	else {
		this.filter = '';
	}
};

// set the list sorting object
DataSource.prototype.setSorting = function(sField, sOrder) {
	if (sField && sOrder) {
		this.sorting = [
			's=',
			sField,
			'(', sOrder || 'asc', ')'
			].join('');
	}
	else {
		this.sorting = '';
	}
};

/*
 * Getters
 */
DataSource.prototype.getLastOffset = function() {
	return this._lastOffset;
};

DataSource.prototype.getOffset = function() {
	return this._offset;
};

DataSource.prototype.getLastPage = function() {
	return this._lastPage;
};

DataSource.prototype.getPage = function() {
	return this._page;
};

DataSource.prototype.getHTML = function() {
	return this._html;
};

DataSource.prototype.getXML = function() {
	return this._xml;
};

// parse and returns the json string received in the API response
DataSource.prototype.getJSON = function() {
	return this._json;
};

DataSource.prototype.getCurrentRecordCount = function() {
	return this._currentSize;
};

DataSource.prototype.getTotalRecordCount = function() {
	return this._totalSize;
};

DataSource.prototype.getRemainingRecordCount = function() {
	if(!this._totalSize) return 1;			// Temporary exception
	var records = (this._totalSize - (this._offset + this._pageSize));
	return records > 0 ? records : 0;
};

DataSource.prototype.getTotalPageCount = function() {
	if(!this._totalSize) return this._page +2;			// Temporary exception
	return Math.ceil(this._totalSize / this._pageSize);
};

DataSource.prototype.getCurrentPageNumber = function() {
	return this._offset / this._pageSize;
};

DataSource.prototype.setXSLParams = function(oXslParams) {
	this._XSLParameters = oXslParams || {};
};

DataSource.prototype.loadXSL = function(xslFileURL) {

	var that = this;
	var callbackFunction = function(sData) {
		that._XSL = sData;

		for (var i = that._xslCallbackQueue.length; i > 0; i--) {
			var callback = that._xslCallbackQueue.pop();
			try {
				callback.apply(that);
			}
			catch(e) {
				// do nothing, we just do not want to break the queue
				console.log(callback);
				console.dir(e);
			}
		}
	}

	$.get(xslFileURL, null, callbackFunction, 'xml');
};

DataSource.prototype.waitForXSL = function(fCallback) {

	// if XSL is not ran client side or if the XSL file is already loaded or we do not need XSL transform at all
	if (!dashboard.clientSideXSL || this._XSL || !this._useXSL) {
		fCallback();
	}
	else {
		this._xslCallbackQueue.push(fCallback);
	}
};

// build the full REST request
DataSource.prototype.buildRequestURL = function() {

	var request = this._URL + '?' + this._paging;

	if (this.filter) {
		request += '&' + this.filter;
	}

	if (this.sorting) {
		request += '&' + this.sorting;
	}

	return request;
};

DataSource.prototype.render = function(sHTML) {
	this._viewport.innerHTML = sHTML;
};

DataSource.prototype.requestPreviousPage = function(bKeepContent) {
	if (this._page > 0) {
		this.setPage(this._page - 1);
		this.requestPage(bKeepContent);
	}
};

DataSource.prototype.requestNextPage = function(bKeepContent) {
	if (this._page < this.getTotalPageCount() - 1) {
		this.setPage(this._page + 1);
		this.requestPage(bKeepContent);
	}
};

DataSource.prototype.refreshPage = function(bKeepContent) {

	// clear the buffered XML
	if (this._clientSidePaging) {
		this._aXMLPages = null;
	}

	// clear the whole content cache
	this._allLoaded = false;

	if (bKeepContent) {
		// set a temporary new page size to reload all pages in one request
		var currentOffset = this._offset;
		var currentPageSize = this._pageSize;
		this.setPageSize((currentOffset * currentPageSize) || currentPageSize);
	}
	else {
		// clear the page caching flags
		this._loadedPages = {};
	}

	// request data from page 0
	this.setOffset(0);

	// request the page refresh and clear existing content
	// page index caching should not be affected by this
	this.requestPage();

	if (bKeepContent) {
		// restore previous paging state
		this._offset = currentOffset;
		this._pageSize = currentPageSize;
	}
};

DataSource.prototype.bufferXMLpages = function(oXML) {

	this._aXMLPages = [];

	var rootNode = oXML.lastChild;
	var nbChildren = rootNode.childNodes.length;

	// fix paging values
	rootNode.setAttribute('totalSize', nbChildren);

	var children = rootNode.childNodes;

	var nbPages = nbChildren / this._pageSize;

	for (var p = 0; p <= nbPages; p++) {
		var offset = p * this._pageSize;
		var upLimit = offset + this._pageSize;
		var sXml = '<' + rootNode.nodeName + ' totalSize="' + nbChildren + '">';

		for (var i = offset; i < upLimit && i < nbChildren; i++) {
			sXml += serializeXML(children[i]);
		}
		sXml += '</' + rootNode.nodeName + '>';
		this._aXMLPages.push($.parseXML(sXml));
	}
};

DataSource.prototype.requestPage = function(bKeepContent) {

	var that = this;

	var request = this.buildRequestURL();

	if (bKeepContent && this._loadedPages[this._page] || this._allLoaded) {
		// this page has already been loaded
		that.notifyObservers('pagechange'); // just notify of the page change
		return;
	}

	// set caching flag for this page
	this._loadedPages[this._page] = true;
	
	// set caching for the whole content when required
	if (this._preRenderAll) {
		this._allLoaded = true;
	}

	// response handler
	var onResponseFunction = function(oXML, sError) {
		var html = '';
		var recordCount = that._pageSize; // not accurate but better than nothing as default value

		if (oXML) {

			// get frame size
			var rootNode = oXML.lastChild;
			if (rootNode) {
				
				// when using client side paging, "totalSize" attribute will be set by bufferXMLpages
				that._totalSize = parseInt(rootNode.getAttribute('totalSize') || 0);
				recordCount = rootNode.childNodes.length;
				that.notifyObservers('pagechange');
			}

			that._xml = oXML;
			that._json = xmlToJson(oXML);

			// apply XSL transform
			if (that._useXSL) {
				that._html = applyXSL(oXML, that._XSL, that._XSLParameters);
			}
			else {
				that._html = oXML.documentElement.textContent;
			}
		}

		if (sError != 'success') { //jQuery returns 'success' as a second argument by defaut
			that._html = sError;
		}

		if (that._viewport) {

			if (!bKeepContent) {
				// just replace content
				that._currentSize = recordCount;
				that._viewport.innerHTML = that._html;
			}
			else {
				// keep content
				that._currentSize += recordCount;
				that._viewport.innerHTML += that._html;
			}
		}

		that.notifyObservers('change');
	};

	var requestFunction;

	if (this._clientSidePaging) {
		if (this._aXMLPages) {
			// call the response function using buffered XML pages
			requestFunction = function() {
				onResponseFunction(that._aXMLPages[that._page], 'success');
			};
		}
		else {

			var bufferResponseCallback = function(oXML) {
				if (oXML) {
					that.bufferXMLpages(oXML);
					if (that._preRenderAll) {
						onResponseFunction(oXML, 'success');
					}
					else {
						onResponseFunction(that._aXMLPages[that._page], 'success');
					}
				}
			};

			// send a request for all content to be buffered
			requestFunction = function() {
				that.sendRequest(request, null, 'get', bufferResponseCallback, function(sMessage) {
					onResponseFunction(null, sMessage);
				});
			};
		}
	}
	else {
		// send regular request for paged content
		requestFunction = function() {
			that.sendRequest(request, null, 'get', onResponseFunction, function(sMessage) {
				onResponseFunction(null, sMessage);
			});
		};
	}

	// delay in case of missing XSL
	this.waitForXSL(requestFunction);
};

DataSource.prototype.sendRequest = function(sUrl, sXml, sMethod, successCallback, failureCallback) {

	var request = bd.contextRoot + '/' + sUrl;

	if (sXml)
		sXml = addXMLHeader(sXml);

	jQueryRestAjax(request, sXml, sMethod, successCallback);
//	.error(function(jqXHR) {
//
//		jqXHR = jqXHR || {};
//
//		// jQuery sometimes fails to parse response in IE, especially for REST HTTP code 201, 204, etc.
//		// TODO: investigate on why the response fails to be parsed (Location header? Empty response body?, etc.)
//		// here we check the HTTP status code again and force a call to the successCallBack
//		if(jqXHR.status && (jqXHR.status == 201 || jqXHR.status == 204 || jqXHR.status == 304)) {
//			if (jqXHR.statusText == 'parsererror') {
//				successCallback(jqXHR.responseText, jqXHR.statusText, jqXHR);
//				return;
//			}
//		}
//
//		if (failureCallback) {
//			var sMessage = jqXHR.responseText || dashboard.errorMessages['requestFailed'];
//			failureCallback(sMessage);
//		}
//	});
};

DataSource.prototype.submitForm = function(oForm, successCallback, failureCallback) {

	var url = oForm.getAttribute('action');
	var method = oForm.getAttribute('method').toLowerCase();

	var xml = form2XML(oForm);

	this.sendRequest(url, xml, method, successCallback, failureCallback);
};

DataSource.prototype.requestDelete = function(url, successCallback, failureCallback) {
	this.sendRequest(url, null, 'delete', successCallback, failureCallback);
};

var form2XML = function(oForm) {
	var json = form2Json(oForm);
	var xml = xmlizeJson(json);
	return xml;
};
/*
var form2Json2 = function(oElement, jSon) {

	var localName = oElement.getAttribute('name');

	// no named descendents
	if (jQuery(oElement).has('[name]').length == 0) {
		var value;
		switch(oElement.getAttribute('type')) {
			case 'checkbox': value = oElement.value || oElement.checked;
							 break;
			default: value = oElement.value || '';
		}

		jSon[localName] = {
			'xns:attributes': oElement.getAttribute('xns:attributes') || '',
			'xns:value': value
		};
	}
	else {

		jSon = jSon || {};

		var newRecord = {
			'xns:attributes': oElement.getAttribute('xns:attributes') || '',
			'xns:value': ''
		};

		// named children: get their value
		var children = jQuery(oElement).children('[name]').each(function() {

			var childName = this.getAttribute('name');

			jSon[localName] = jSon[localName] || newRecord;

			// if some siblings have the same name: it's an array
			// discard all differently named siblings as json can not handle that
			var $siblings = jQuery(this).siblings('[name="' + childName + '"]');
			if ($siblings.length) {
				jSon[localName]['xns:value'] = jSon[localName]['xns:value'] || [];
				jSon[localName]['xns:value'].push(form2Json2(this));
			}
			else {
				// it's an object
				jSon[localName]['xns:value'] = jSon[localName]['xns:value'] || {};
				form2Json2(this, jSon[localName]['xns:value']);
			}
		});

		// not named children: keep walking
//		var children = jQuery(oElement).children().not('[name]').each(function() {
//			jSon[localName] = form2Json2(this);
//		});

	}

	return jSon;
};
*/

var form2Json = function(oForm) {

	var jSon = {};

	var getJasonPath = function(element, value) {

		var currentNode = jSon;
		var $path =  $.merge(jQuery(element), jQuery(element).parents('[name]'));

		for (var i = $path.length - 1; i >= 0; i--) {
			//var pathName = $path[i].getAttribute('name'); // IE returns the input with name="name" instead of the form's attribute
			var pathName = $path[i].getAttributeNode('name').value;
			//prevent an empty node submitted in the xml, this will create a non-well formated xml error
			if(pathName != ''){
				if (!currentNode[pathName]) {
	
					var xnsValue = {};
					if (i == 0) {
						xnsValue = value;
					}
	
					currentNode[pathName] = {
						'xns:attributes': $path[i].getAttribute('xns:attributes') || '',
						'xns:value': xnsValue
					};
	
	/*
					//var xmlResponseNSPrefix = element.lookupPrefix('http://rns.backbase.com/') + ':';
					var xmlResponseNSPrefix = 'rns:';
	
					for (var j = 0, l = element.attributes.length; j < l; j++) {
						var qualifiedName = element.attributes.item(j).name.split(xmlResponseNSPrefix);
						if (qualifiedName.length > 1) {
							var localName = qualifiedName[1];
							currentNode[pathName]['attributes'] = currentNode[pathName]['attributes'] || ' ';
							currentNode[pathName]['attributes'] += [localName, '=', element.attributes.item(j).value, ' '].join('');
						}
					}
	*/
				}
	
				currentNode = currentNode[pathName]['xns:value'];
			}
		}

		return currentNode;
	};

	var namedElements = jQuery('[name]', oForm);

	for (var i = 0, l = namedElements.length; i < l; i++) {

		var element = namedElements[i];

		if (jQuery(element).has('[name]').length == 0) {

			var value;
			switch(element.getAttribute('type')) {
				case 'checkbox': value = element.checked;
								 break;
				default: value = element.value || '';
				
						 // force special characters stripping before submitting
						 // TODO: user should be prompted by client side validation instead.
						 // An agreement has to be reached on validation (PSFIVE-423)
						 value = be.utils.formatAllowedName(value);
			}			
			
			var subJsonValue = getJasonPath(element, value);
		}
	}

	return jSon;
};

// UTILITIES
var addXMLHeader = function(sXml) {
	return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + sXml;
};

var xmlizeJson = function(oJson) {

	var sXml = '';

	if (typeof oJson == 'object') {
		if (oJson.constructor === Array) {
			for (var j = 0; j < oJson.length; j++) {
				sXml += xmlizeJson(oJson[j])
			}
		}
		else {
			for (var i in oJson) {
				if (i != 'xns:value' && i != 'xns:attributes') {
					sXml += '<' + i + ' ' + oJson[i]['xns:attributes'] + '>';
					sXml += xmlizeJson(oJson[i]['xns:value']);
					sXml += '</' + i + '>';
				}
			}
		}
	}
	else {
		sXml += be.utils.xmlEscape(oJson + ''); // let JS convert to string
	}

	return sXml;
};

var xmlToJson = function(xml) {

  // Create the return object
  var obj = {};

  if (xml.nodeType == 1) { // element
    // do attributes
    if (xml.attributes.length > 0) {
    obj["@attributes"] = {};
      for (var j = 0; j < xml.attributes.length; j++) {
        var attribute = xml.attributes.item(j);
        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
      }
    }
  } else if (xml.nodeType == 3) { // text
    obj = xml.nodeValue;
  }

  // do children
  if (xml.hasChildNodes()) {
    for(var i = 0; i < xml.childNodes.length; i++) {
      var item = xml.childNodes.item(i);
      var nodeName = item.nodeName;
      if (typeof(obj[nodeName]) == "undefined") {
        obj[nodeName] = xmlToJson(item);
      } else {
        if (typeof(obj[nodeName].length) == "undefined") {
          var old = obj[nodeName];
          obj[nodeName] = [];
          obj[nodeName].push(old);
        }
        obj[nodeName].push(xmlToJson(item));
      }
    }
  }
  return obj;
};

var applyXSL = function(sXML, sXSL, oXslParams) {
	try {

		var oProcessor = new XSLTProcessor();

		oProcessor.importStylesheet(sXSL);

		for (var i in oXslParams) {
			var oXslParam = (typeof oXslParams[i] === 'function' ? oXslParams[i]() : oXslParams[i]);
			if (oXslParam !== undefined) {
				oProcessor.setParameter('', i, oXslParam);
			}
		}

		var oResult;

		if (b$.browser.ie) {

			result = oProcessor.transformToFragment(sXML, document);
			bd.fragPlaceHolde = bd.fragPlaceHolde || document.createElement('div');
			bd.fragPlaceHolde.appendChild(result);
			result = bd.fragPlaceHolde.innerHTML;
			bd.fragPlaceHolde.innerHTML = '';
			return result;
		}
		else {
			result = oProcessor.transformToDocument(sXML);
			return (new XMLSerializer()).serializeToString(result) || result.xml;
		}
	}
	catch(e) {
		console.dir(e);
		throw(e);
	}
};

var serializeXML = function(oXML) {
    if (oXML.xml)
	    return oXML.xml;
    else
        return new XMLSerializer().serializeToString(oXML);
};
});