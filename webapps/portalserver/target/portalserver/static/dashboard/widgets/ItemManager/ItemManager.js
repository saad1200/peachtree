/**
 * Copyright � 2011 Backbase B.V.
 */
bd.ItemManager = (function() {

   var currentSelectedWidget = null;
   var currentSelectedItem = null;

   // var URI = b$.require('b$.uri.URI');
   // private functions
   /**
    * This function will add a widget to the server catalog.
    */
   var saveNewWidget = function(formObj, widgetType, event) {
      try {
         // Get data from a HTML input form into JSON object
         var templateData = be.utils.getTemplateDataFromForm("#newWidgetForm");

         //modify the widgetTags from string to array
     	//create a mustache.js-friendly list
     	var tagsArray = [];
     	if (templateData.widgetTags.length > 0) {
     		var rawTags = templateData.widgetTags.split(",");
     		for (var i = 0; i < rawTags.length; i++) {
     			if (jQuery.type(rawTags[i]) == 'string') {
     				if (rawTags[i].length > 0)
     				tagsArray.push({tag: rawTags[i] });
     			}
     		}
     	}
     	templateData.hasTags = tagsArray.length > 0;
     	templateData.tags = tagsArray;

         var widgetType = templateData.widgetType;
         // TODO: find a unique ID generator
         templateData.widgetId = new Date().getTime();

         // Converts JSON object to XML
         var catalogWidgetXML = be.utils.processXMLTemplate("itemMngmt/CatalogWidget-" + widgetType, templateData);
         var url = bd.contextRoot + "/catalog.xml";

         // Send data to the server
         be.utils.ajax({
            url: url,
            data: catalogWidgetXML,
            type: "POST",
            success: function() {

               // confirm to the end user
               bc.component.notify({
					uid: '0292',
					icon: 'checkbox', // checkbox, attention, error, loading
					message: 'Widget added successfully.'
				});
               //bd.operationCompleteMsg("Widget added successfully.");
               var oGadgetBody = jQuery(".bd-widget-ItemManagerContainer")[0];

               // close the form
               cancelCreateForm();

               // re-render anything that is needed
               renderUI(oGadgetBody, null, true);
            }
         });
      } catch (exception) {
         console.log("bd.ItemManagerClass.saveNewWidget" + exception);
      }
      return false;
   };

   var saveEditWidget = function(formObj) {
      try {
         var templateData = be.utils.getTemplateDataFromForm("#editWidgetForm");

		//modify the widgetTags from string to array
		//create a mustache.js-friendly list
		var tagsArray = [];
		if (templateData.widgetTags.length > 0) {
			var rawTags = templateData.widgetTags.split(",");
			for (var i = 0; i < rawTags.length; i++) {
				if (jQuery.type(rawTags[i]) == 'string') {
					if (rawTags[i].length > 0)
						tagsArray.push({
							tag: rawTags[i].replace(/^\s+|\s+$/, "")
						});
				}
			}
		}
		templateData.hasTags = tagsArray.length > 0;
		templateData.tags = tagsArray;

		templateData.isEdit = true;
		templateData.modifiedProp = createXmlProp(currentSelectedItem.catalog.widget.properties, templateData);
		//console.log(templateData.modifiedProp);

         var widgetType = templateData.widgetType;
         var catalogWidgetXML = be.utils.processXMLTemplate("itemMngmt/CatalogWidget-" + widgetType, templateData);
         //console.log(templateData)
         //console.log(catalogWidgetXML)
         var url = bd.contextRoot + "/catalog.xml";
         be.utils.ajax({
            url: url,
            data: catalogWidgetXML,
            type: "PUT",
            success: function() {
				bc.component.notify({
					uid: '1209',
					icon: 'checkbox', // checkbox, attention, error, loading
					message: 'Widget has been updated successfully'
				});
               //bd.operationCompleteMsg("Widget has been updated successfully");
               var oGadgetBody = jQuery(".bd-widget-ItemManagerContainer")[0];
               renderUI(oGadgetBody, null, true);
            }
         });
      } catch (exception) {
         console.log("bd.ItemManagerClass.saveNewWidget" + exception);
      }
      return false;
   };

   var showDetails = function(formObj, widgetId) {
      be.utils.ajax({
         url: bd.contextRoot + "/catalog/" + widgetId,
         dataType: "xml",
         success: function(responseDataXML) {
            try {
               // unselect other elements and select this one
               jQuery(".bd-widgetItemHolder").removeClass('bd-widgetItemHolder-current');
               jQuery(formObj).addClass('bd-widgetItemHolder-current');
               var responseDataJson = bd.xmlToJson({
                  xml: responseDataXML,
                  dataFormatter: function(item) {
                     {
                        if(item.properties['thumbnailUrl'] != null){
                           item.properties['thumbnailUrl'].value = be.utils.replaceParams(item.properties['thumbnailUrl'].value,
                              bd.oPortalClient.params);
                           item.properties.thumbnailUrl = item.properties['thumbnailUrl'];
                        }
                        //if (typeof item.properties['Tags'].value === 'object') {
                        //   item.properties['Tags'].value = "";
                        if (typeof item.tag === 'object') {
                           item.tag = "";
                        }
                     }
                  }
               });
               currentSelectedItem = responseDataJson;
               var props = responseDataJson.catalog.widget.propertiesArray;
               var viewableProperties = [];
               for (var i in props) {
            	   if (props[i].hasOwnProperty("name") && props[i].hasOwnProperty("value") && props[i].value != null
            			   && typeof props[i].value !== 'object') {
            		   viewableProperties.push(props[i]);
            	   }
               }

                var rawTags = responseDataJson.catalog.widget.tagArray;
		    	var tags = [];
		    	for (var i=0; i < rawTags.length; i++) {
		    		var t = rawTags[i];
		    		if (jQuery.type(t) == 'string') {
		    				tags.push(t);
		    		}
		    	}
		    	var tags = tags.join(", ");


               var templateData = {
                  widgetData: responseDataJson.catalog.widget,
                  viewableProperties: viewableProperties,
                  tags: tags
               };

               var htmlContent = be.utils.processHTMLTemplate("ItemManager/widgetDetails", templateData);
               var jWidgetDetails = jQuery("#widgetDetailsContainer");
               jWidgetDetails.html(htmlContent);
               jWidgetDetails.show();
               var template = templateData.widgetData.properties.TemplateName.value;
               if (template == "Standard_Widget") {
                  jQuery("#editWidgetDefUrl").attr("value", templateData.widgetData.properties.src.value);
               } else if (template == "W3C_Widget") {
                  jQuery("#editWidgetDefUrl").attr("value", templateData.widgetData.properties.defaultStartFile.value);
               }
               editWidget(templateData.widgetData);
               jQuery("#resetButton").click(function() {
                  showDetails(formObj, widgetId);
               });
            } catch (exception) {
               currentSelectedItem = null;
               console.log("bd.ItemManagerClass.showDetails" + exception);
            }
         }
      });
   };

   var editWidget = function(widgetData) {
      be.utils.domReady("#widgetTagsDetails", function(widgetTagsInput) {
         bd.initTags("#widgetTagsDetails");
      });
      editIcon(widgetData);
      be.utils.addCustomMethodToValidator(['validTitle']);
      jQuery('#editWidgetForm').validate({
  		rules: {
  			widgetTitle: {
				validTitle: true
			}
		},
         submitHandler: saveEditWidget
      });
      return false;
   };

   var editIcon = function(widgetData) {
      var id = 'id' + new Date().getTime();
      jQuery("#icon-edit-button")
            .click(
                  function() {
                     var preIconURL = jQuery("#iconImage").attr("src");
                     var template = '<div class="bd-inPlaceEditorPlus" id="'
                           + id
                           + '"><input size="200" value="'
                           + ((widgetData.properties.thumbnailUrl != null) ? widgetData.properties.thumbnailUrl.value : "")
                           + '"/><button class="bd-button bd-roundcorner-3">Ok</button><button class="bd-buttonText">Cancel</button></div>';
                     jQuery("#icon-edit-button").hide();
                     jQuery("#icon-edit-button").before(template);
                     var editor = jQuery('#' + id);
                     var input = jQuery('input', editor).select();
                     var ok = jQuery('.bd-button', editor);
                     var cancel = jQuery('.bd-buttonText', editor);

                     ok.bind('click', function(e) {
                        jQuery("#iconImage").attr("src", input.val());
                        jQuery("#icon-edit-button").show();
                        jQuery("#iconInput").attr("value", input.val());
                        editor.remove();
                     });
                     cancel.bind('click', function(e) {
                        jQuery("#icon-edit-button").show();
                        jQuery("#iconImage").attr("src", preIconURL);
                        jQuery("#iconInput").attr("value", input.val());
                        editor.remove();
                     });
                  });
   };

   // end of private functions

   /**
    * This function will popup a create widget form.
    */
   var addWidget = function(formObj, widgetType, event) {
      var templateData = {
         widgetType: widgetType
      };
      var htmlContent = be.utils.processHTMLTemplate("ItemManager/createWidget-" + widgetType, templateData);

      be.openDialog({
         htmlContent: htmlContent,
         event: event,
         // add standAlone parameter for add-widget form
         standAlone: true,
         height: "400",
         small: true
      });
      be.utils.addCustomMethodToValidator(['validTitle']);
      jQuery('#newWidgetForm').validate({
  		rules: {
  			widgetTitle: {
				validTitle: true
			}
		},
         submitHandler: saveNewWidget
      });

      be.utils.domReady("#widgetTagsInput", function(widgetTagsInput) {
         bd.initTags("#widgetTagsInput");
      });

      return false;
   };

   var selectWidget = function(formObj, widgetId) {
      currentSelectedWidget = widgetId;
      bd.ItemManager.selectedWidget = currentSelectedWidget;
      showDetails(formObj, widgetId);
      return false;
   };

   var cancelCreateForm = function(formObj) {
      be.closeCurrentDialog();
      return false;
   };

   var removeWidget = function(formObj, softDelete) {

		deleteWidget = function(){
			var widgetId = currentSelectedWidget;
			be.utils.ajax({
				url: bd.contextRoot + "/catalog/" + widgetId + "?soft=" + softDelete,
				type: "DELETE",
				success: function() {
					bc.component.notify({
						uid: '9256',
						icon: 'checkbox', // checkbox, attention, error, loading
						message: 'Widget has been removed successfully'
					});
					//bd.operationCompleteMsg("Widget has been removed successfully");
					var oGadgetBody = jQuery(".bd-widget-ItemManagerContainer")[0];
					renderUI(oGadgetBody, null, true);
				}
			});
		};

		if (currentSelectedWidget) {
			be.utils.confirm({
				title: 'REMOVE WIDGET',
				message: "This will remove the widget and cannot be undone. Are you sure you want to continue?",
				yesCallback: deleteWidget
			});
		} else {
			//be.utils.alert({
			//	message: 'Nothing selected'
			//});
		}

	  /*var answer = confirm("Are you sure you want to delete this widget?");
      if (answer) deleteWidget();*/
   };

   var importWidget = function(){
       bd.portalExport.importItem("widget", function(){
           var oGadgetBody = jQuery(".bd-widget-ItemManagerContainer")[0];
           renderUI(oGadgetBody, null, true);
       });
   };

   var renderUI = function(oGadgetBody, sGadgetUrl) {
       var successCallback = function(serverCatalogXmlDoc){
          // convert data to json
          var serverCatalogJsonData = bd.xmlToJson({
             xml: serverCatalogXmlDoc,
             dataFormatter: function(item) {

                // handle any special formatting
                if (bd.oPortalClient != null && bd.oPortalClient.params != null && item.properties['thumbnailUrl'] != null) {

                   // console.log("before: " +
                   // item.properties['thumbnailUrl'].value);
                   item.properties['thumbnailUrl'].value = be.utils.replaceParams(item.properties['thumbnailUrl'].value, bd.oPortalClient.params);
                   item.properties.thumbnailUrl = item.properties['thumbnailUrl'];
                   // item.properties['thumbnailUrl'].value = new
                   // URI(item.properties['thumbnailUrl']).resolve(new
                   // URI(portal.config.resourceRoot)).toString();
                   // console.log("after: " +
                   // item.properties['thumbnailUrl'].value);
                }
             }
          });

          if (serverCatalogJsonData.catalog == null || serverCatalogJsonData.catalog.widget == null
                || serverCatalogJsonData.catalog.widget.length == null) {
             serverCatalogJsonData.catalog.widget = [];
          }

          var templateData = {
             serverCatalogWidgets: serverCatalogJsonData.catalog.widget,
             portalTitle: bd.selectedPortalTitle,
             contextRoot: bd.contextRoot
          };

          // generate HTML via html template from json data
          var $htmlContent = be.utils.processHTMLTemplate("ItemManager/ItemManagerView", templateData);

          // add to the DOM
          $(oGadgetBody).html($htmlContent);

          var removeWidget = bc.component.dropdown({
                target: $('.bd-itemMnt-header-buttons', oGadgetBody)[0],
                label: 'Actions',
                uid: '4567',
                type: 'action-icon',
                staticLabel: true,
                options: [
                    {name:'Remove selected item from the server', classname: 'bd-removeSelectedItem', onclick: 'return bd.ItemManager.removeWidget(this, true)'},
                    {name:'Export selected item', classname: 'bd-exportSelectedItem', onclick: 'return bd.portalExport.exportItem(bd.ItemManager.selectedWidget, "widget")'},
                    {name:'Import item', classname: 'bd-importItem', onclick: 'return bd.ItemManager.importWidget()'}
                ]
            });

            bc.component.dropdown({
                target: $('.bd-itemMnt-header-buttons', oGadgetBody)[0],
                type: 'add-select',
                uid: '7282',
                staticLabel: true,
                options: [
                    {name:'Add Backbase Widget', onclick: 'return bd.ItemManager.addWidget(this, "backbase", event);'},
                    {name:'Add W3C Widget', onclick: 'return bd.ItemManager.addWidget(this, "w3c", event);'}
                ]
            });



          // var $oGadgetBody = jQuery(oGadgetBody);
          // $oGadgetBody.delegate('.addWidgetButton', 'click',
          // handleAddWidgetButton);

    /*
          var pageTemplatesUrl = bd.contextRoot + "/templates.xml?s=lastModifiedTimestamp(dsc)&f=subType(eq)PAGE";
          be.utils.ajax({
             url: pageTemplatesUrl,
             dataType: "xml",
             cache: false,
             success: function(responseDocument){
                console.log(responseDocument);
                jQuery('.bd-pageTemplateCount', oGadgetBody).html("("+responseDocument.documentElement.getAttribute("totalSize")+")");
             }
          });

          var layoutsUrl = bd.contextRoot + "/catalog.xml?s=lastModifiedTimestamp(dsc)&f=type(eq)CONTAINER";
          be.utils.ajax({
             url: pageTemplatesUrl,
             dataType: "xml",
             cache: false,
             success: function(responseDocument){
                console.log(responseDocument);
                jQuery('.bd-layoutCount', oGadgetBody).html("("+responseDocument.documentElement.getAttribute("totalSize")+")");
             }
          });
      */
       };
      // generate the URL
      var globalServerCatalogUrl = bd.contextRoot + "/catalog.xml?ps=100&s=lastModifiedTimestamp(dsc)&f=type(eq)WIDGET";
      // Get xml data from the server
      be.utils.ajax({
         url: globalServerCatalogUrl,
         dataType: "xml",
		 cache: false,
		 success: successCallback
      });

      bd.ItemManager.selectedWidget = null;
   };

   var createXmlProp = function(jsonProp, formProp){
	   var xmlStr = '';

	   var getXml = function(prop, value){
		   var attrList = ["label", "name", "viewHint", "manageable"];
		   var xmlAttr = '', tmpXmlStr = '';
		   for(var i=0; i < attrList.length; i++){
			   var attr = '';
			   if(prop[attrList[i]]){
				   attr = attrList[i] + '="' + prop[attrList[i]] + '"';
			   }
			   xmlAttr += attr + ' ';
		   }
		   xmlAttr = $.trim(xmlAttr);
		   tmpXmlStr += '<property ' + xmlAttr + '>';

		   if(prop["type"]){
			   tmpXmlStr += '<value type="'+prop["type"]+'">'+prop["value"]+'</value>';
		   }else{
			   tmpXmlStr += '<value>'+ value +'</value>';
		   }

		   tmpXmlStr += '</property>';

		   return tmpXmlStr;
	   };

	   var propList = {
			   "title" : "widgetTitle",
			   "thumbnailUrl" : "widgetThumbNailUrl",
			   "src" : "widgetDefUrl",
			   "defaultStartFile" : "widgetDefUrl"
	   };

	   for(var key in propList){
		   if(jsonProp[key]){
			   var value = propList[key];
			   xmlStr += getXml( jsonProp[key], formProp[value] );
		   }
	   }

	   xmlStr = '<properties>' + xmlStr + '</properties>';

	   return xmlStr;
   };



   // public functions
   return {
      renderUI: renderUI,
      removeWidget: removeWidget,
      addWidget: addWidget,
      importWidget: importWidget,
      selectWidget: selectWidget,
      cancelCreateForm: cancelCreateForm,
      selectedWidget: null
   };
}());

b$.module("bd.widgets.ItemManager", function() {
   this.Maximized = bd.ItemManager.renderUI;
});
