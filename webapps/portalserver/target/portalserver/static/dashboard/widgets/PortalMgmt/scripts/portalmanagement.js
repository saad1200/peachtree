/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : portalmanagement.js
 *  Description:
 *
 *  ----------------------------------------------------------------
 */

/* globals define */

// b$.module("bd.widgets.PortalMgmtGadget", function() {

// TODO: finish AMDifying / get rid of bd dependencies

define([
    'dashboard/widgets/PortalMgmt/scripts/portal-export',
    'angular',
    'zenith/utils',
    'zenith/utils/string',
    'zenith/http/portals'
], function(portalExport, angular, utils, string, httpPortals) {

    'use strict';

    var that = this || {},
        bd = window.bd || {},
        widgetBody;

   /** from form data */
    var archivePortal = function (portalName, callback) {
        portalName = portalName || jQuery('#listOfPortals').val();
        var archivePortalFinish = function() {
            be.utils.ajax({
                url : bd.contextRoot + "/portals/" + portalName + "/deletePortal", // new API
                type : "DELETE"
            }).then(function (){
                if (bd.closeLauncher) {
                    bd.closeLauncher();
                    bd.selectedPortalName = 'dashboard';
                    bc.component.notify({
                        uid: '1998',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: portalName + ' was deleted successfully.'
                    });
                    if (typeof callback == 'function') {
                        callback();
                    }
                } else {
                    var qString = location.search ? location.search : '';
                    location.href = bd.contextRoot + '/portals/dashboard' + qString;
                }
            }, function(data){
                if (data && data.documentElement && data.documentElement.nodeName == "parsererror") {
                    console.log("API Error: " + data.documentElement.firstChild.nodeValue);
                } else if(data && data.responseText) {
                    console.log(data.responseText);
                }
            });
        };

        be.utils.confirm({
            title: 'REMOVE PORTAL',
            message: "This will remove the portal and all its pages and cannot be undone. Are you sure you want to continue?",
            yesCallback: archivePortalFinish
        });

        return false;
   };

   if(bd.test)
      bd.test.deletePortal = archivePortal;



    var getPortalDetails = function(){
        return httpPortals.portalDetails(bd.selectedPortalName, null, true).then(function(portalDataJson){
            that.portalDataJson = portalDataJson;
            renderPortalDetails();
            return portalDataJson;
        });
    };



	var renderPortalDetails = function() {
		var templateData = that.portalDataJson;

		var properties = templateData.properties;
		var portalType = properties.TargetedDevice.value;


		bd.typeOfTags = [];
		if(properties["TypeOfTags"] && properties["TypeOfTags"].value != null){
			try{
				bd.typeOfTags = properties["TypeOfTags"].value.split(",");
			}catch(e){console.log(e);}
		}
		templateData.isDashboard = (templateData.name == 'dashboard');
		bd.mobileDrawer = false;
		if(!templateData.isDashboard){
			bd.mobileDrawer = portalType;
		}
		if(templateData.tag != null && templateData.tag != false)
		{
			templateData.tag = (typeof templateData.tag == 'object' ? false : templateData.tag.replace(/,/g, ', '));
		}

		if (properties.DefaultLandingPage.value != null && properties.DefaultLandingPage.value.indexOf('http://') == 0) {
			templateData.onClick = templateData.linkText = "window.open('" + properties.DefaultLandingPage.value + "')";
		} else {
			templateData.onClick = "window.open('" + be.utils.getURLRoot() + bd.contextRoot + "/portals/" + templateData.name + "')";
			templateData.linkText = bd.contextRoot + '/portals/' +
				(templateData.name.length > 14 ? templateData.name.substring(0, 14) + '...' : templateData.name);
		}

        if(properties.publishChains){
            if(properties.publishChains.value === null) properties.publishChains.value = '';
            properties.publishChains.text = properties.publishChains.value.replace(/;/g, ', ');
        }

        be.utils.processHTMLTemplate('portals/portalDetails', templateData, function(data){
            that.$portalDetailsViewport.html(data);
        });
	};






	var startPortalManagement = function(oWidgetBody, portal) {

        // Legacy
        bd.selectedPortalName = portal.name;
        bd.selectedPortalTitle = portal.getProperty('title');
        bd.selectedPortalOptimizedFor = portal.getProperty('DefaultDevice');
        bd.selectedPortalTargetDevice = portal.getProperty('TargetedDevice');
        widgetBody = oWidgetBody;



		var $portalDetailsViewport = jQuery('.bd-portalDetailsViewport', oWidgetBody);
		that.$portalDetailsViewport = $portalDetailsViewport;

		var $portalSpecialPagesViewport = jQuery('.bd-specialPagesViewport', oWidgetBody);
		that.$portalSpecialPagesViewport = $portalSpecialPagesViewport;

		getPortalDetails();


		var specialPageList = ['DefaultLandingPage','LoginPage','LogoutPage','ErrorPage','AccessDeniedPage','AuthenticationFailurePage'];

		if(bd.designMode === "null" || bd.designMode === false) {
			// view permissions
			jQuery('.bd-tabLabels', oWidgetBody).delegate('.bd-tab2', 'click', function() {
				var params = {
					portalName: bd.selectedPortalName,
					itemName: bd.selectedPortalName,
					itemType: 'portal',
					context: bd.contextRoot + "/portals/" + bd.selectedPortalName,
					domParent: ".portalPermissions",
					hideCloseButtons : true,
					closeCallback: function(){
						bd.closeLauncher();
					}};
				bd.Permission.loadPermissionsAndGroups(params);
			});


			// Special Pages -- this should probably get it's own js file
			jQuery('.bd-tabLabels', oWidgetBody).delegate('.bd-tab3', 'click', function() {
				getSpecialPageData("portals/portalSpecialPages", true);
			});

            /* special pages edit button click */
            var editSPClick = function(){
 				getSpecialPageData("portals/editPortalSpecialPages", true, _editSPClick);
            };
            var _editSPClick = function(){
				be.utils.addCustomMethodToValidator(['specialPageUrlSpecialChar', 'defaultLandingPageUrlSpecialChar']);
				var rules = {};
				for(var i=0, node; node = specialPageList[i]; i++){
					if(node === 'DefaultLandingPage') {
						rules[node] = {
								defaultLandingPageUrlSpecialChar : true
						}
					} else {
						rules[node] = {
								specialPageUrlSpecialChar : true
						}
					}
				}
				jQuery('#bd-portalSpecialPages-updateForm').validate({
					rules: rules,
					submitHandler : updateSpecialPage
				});

				//cancel the editing
				be.closeCurrentDialogCallback = function(){
					jQuery(oWidgetBody).find('.bd-specialPageSettingsCancel').trigger('click');
				};
            };
			jQuery(oWidgetBody).delegate('.bd-specialPagesEditButton', 'click', editSPClick);

            var SpecialPageDataCache;

			var getSpecialPageData = function(filePath, reload, callback) {
				var SpecialPageData = {};
				if(SpecialPageDataCache && !reload){
					SpecialPageData = SpecialPageDataCache;
                    getSpecialPageData2(filePath);
				}else{
					be.utils.ajax({
						url : bd.contextRoot + '/portals/' + bd.selectedPortalName + '.xml?pc=false',//do not include children
						dataType: "xml",
						cache: false,
						type: "GET",
						success : function(responseDataXML){
							var specialPageData = bd.xmlToJson({xml: responseDataXML});
							SpecialPageDataCache = specialPageData;
							getSpecialPageData2(filePath, callback);
						}
					});
				}
            };
			var getSpecialPageData2 = function(filePath, callback) {

                var SpecialPageData = SpecialPageDataCache;

                for( var key in SpecialPageData.portal.properties){
					var property = SpecialPageData.portal.properties[key];
					if(property.value){
						if(property.value.type){
							property.value = null;
						}
					}
				}

				var htmlContent = be.utils.processHTMLTemplate(filePath, SpecialPageData.portal);
				$portalSpecialPagesViewport.html(htmlContent);

				if(filePath == 'portals/editPortalSpecialPages') {
                    var properties = SpecialPageData.portal.properties;
				}

				jQuery(oWidgetBody).delegate('.bd-specialPagesCloseButton', 'click', function() {
					bd.closeLauncher();
				});

                if(callback){
                    callback();
                }
			};

			jQuery(oWidgetBody).delegate('.bd-specialPageSettingsCancel', 'click', function() {
				getSpecialPageData("portals/portalSpecialPages");
			});

			var updateSpecialPage = function() {
                 var   templateData = {
    					name: bd.selectedPortalName,
    					otherSpecialPage : []
    				};

				for(var i=0, len=specialPageList.length; i < len; i++){
					var specialPageName = specialPageList[i];
					var $specialPageHTML = jQuery('.bd-special-'+specialPageName, $portalSpecialPagesViewport);
					var value = jQuery.trim($specialPageHTML.val());
					if(value != $specialPageHTML.attr('oldvalue')){
						templateData.otherSpecialPage.push( {specialPageName : specialPageName, specialPageValue : value} );
					}
				}

				var xmlContent = be.utils.processXMLTemplate("updateSpecialPages", templateData);

				be.utils.ajax(
				{
					url : bd.contextRoot + "/portals/" + bd.selectedPortalName + ".xml",
					data : xmlContent,
					success : function(data){
						getSpecialPageData("portals/portalSpecialPages", true);
						bc.component.notify({
							uid: '6283',
							icon: 'checkbox', // checkbox, attention, error, loading
							message: 'Special pages updated successfully.'
						});
					},
					contentType : "text/xml",
					type : "PUT"
				});
				return false;

			};
		}

		var showEditForm = function() {

            var portalDetails = that.portalDataJson,
                portalName = bd.selectedPortalName,
                portalTitle = bd.selectedPortalTitle,
                OptimizedFor = bd.selectedPortalOptimizedFor  || 'mixed',
                portalTags = '',
                targetDevice = bd.selectedPortalTargetDevice;

            if (jQuery.type(portalDetails.tagArray) == 'array') {
                portalTags = portalDetails.tagArray.join(',');
            }

			var templateData = {
				contextRoot : bd.contextRoot,
				portalName : portalName,
				portalTitle : portalTitle,
				portalTitleFormatted : be.utils.formatAllowedName(portalTitle),
				tags : '', //disable tags for portal
                urlRoot : be.utils.getURLRoot(),
                thumbnailUrl : portalDetails.thumbnailUrl,
				thumbnailSrc : portalDetails.thumbnailSrc
			};

			var isMobile, isTablet;

			if(OptimizedFor == "mobile"){
				isMobile = true;
			}
			if(OptimizedFor == "tablet"){
				isTablet = true;
			}
			var htmlContent = be.utils.processHTMLTemplate("portals/editPortal", templateData);

			var tabletDropdown = bc.component.dropdown({
				type: 'device-select',
				device: 'tablet',
				deviceName: function() {
					if (isTablet) {
						return targetDevice;
					} else {
						return 'Tablet';
					};
				},
				options: bd.uiEditingOptions.portalDevices.tablet
			});
			var mobileDropdown = bc.component.dropdown({
				type: 'device-select',
				device: 'mobile',
				deviceName: function() {
					if (isMobile) {
						return targetDevice
					} else {
						return 'Mobile';
					};
				},
				options: bd.uiEditingOptions.portalDevices.mobile
			});

			var mobileOptions = mobileDropdown[0].dropdownOptions;
			var tabletOptions = tabletDropdown[0].dropdownOptions;


            // $portalDetailsViewport.html(htmlContent);

            // injecting angular app into existing app
            // TODO: cleanup after app refactoring
            utils.inject(htmlContent, function(elem){
                $portalDetailsViewport.html(elem);
            }, widgetBody);



			$('#OptimizedForView', $portalDetailsViewport).append(tabletDropdown);
			$('#OptimizedForView', $portalDetailsViewport).append(mobileDropdown);

			$('a',tabletOptions).click( function() {
				updateOptimized(this, tabletDropdown);
			});
			$('a',mobileOptions).click( function() {
				updateOptimized(this, mobileDropdown);
			});

			var updateOptimized = function(This, parentDropdown) {
				var targetDevice = $('.bc-device-name', This).text();
				if (targetDevice.toLowerCase() == 'all tablet devices') {
					targetDevice = 'Tablet';
				} else if (targetDevice.toLowerCase() == 'all mobile devices') {
					targetDevice = 'Mobile';
				}
				$('.bd-optimizedFor-label', parentDropdown).text(targetDevice);
                bd.syncOptimizedForView(parentDropdown.attr('optimizedfor'));
			};

			$('#OptimizedForView', $portalDetailsViewport).delegate('.bd-optimizedFor', 'click', function(e) {
				bd.syncOptimizedForView(jQuery(this).attr('optimizedfor'));
			});
			//bd.initTags("#portalTagsInput"); //disable tags for portal
			bd.syncOptimizedForView(OptimizedFor);


            // publish chain
            fetchPublishChainConfig().then(
                function(options, portalPubChains) {
                    publishChainDD($portalDetailsViewport, options, portalPubChains);
                },
                function(err) {
                    console.log(err);
                }
            );


			var updatePortal = function(formObj) {
				try {
					bd.updatePortal(formObj);
				} catch(exception) {
					console.log(exception);
				}
				return false;
			};

			be.utils.addCustomMethodToValidator(['validTitle']);
			jQuery('#newPortalForm').validate({
				rules: {
					portalTitle: {
						validTitle: true
					}
				},
				messages: {
					portalTitle: {
						validTitle: "Portal title can not contain any special characters"
					}

				},
				submitHandler : updatePortal
			});

			bd.initEditable(jQuery('#newPortalForm'));

			return false;
		};

		var showExportForm = function(portalName, portalTitle, OptimizedFor, portalTags, targetDevice){
			portalExport.exportItem(portalName, 'portal');
		};



        var fetchPublishChainConfig = function() {
            var defer = jQuery.Deferred();
            be.utils.ajax({
                url : bd.contextRoot + "/orchestrator/configuration.xml" ,
                success : function(data){
                    var options = [],
                        properties = that.portalDataJson.properties,
                        portalPubChains = properties.publishChains && properties.publishChains.value ? properties.publishChains.value.split(';') : [],
                        i,l,c,
                        portalPubChainsMap = {};

                    for(i = 0, l = portalPubChains.length; i < l; i++){
                        c = portalPubChains[i];
                        portalPubChainsMap[c] = true;
                    }

                    jQuery(data).find('publishChainName').each(function(idx,elm) {
                        var text = jQuery(elm).text();
                        options.push({
                            name : text,
                            value : text,
                            checked: !!portalPubChainsMap[text]
                        });
                    });
                    defer.resolve(options, portalPubChains);
                },
                error: function(err) {
                    defer.reject(err);
                },
                contentType : "text/xml",
                type : "GET"
            });

            return defer.promise();
        };

        var publishChainDD = function($viewport, optData, portalPubChains) {
            var $target = $viewport.find('.bd-publishchain-dd').empty();

            var publishChain =  bc.component.dropdown({
                    type: 'multi-select',
                    staticLabel: true,
                    uid: "723641230",
                    target: $target,
                    label: 'Loading',
                    options: optData
                }),
                publishChainOpt = publishChain[0].dropdownOptions,
                updateLabel = function() {
                    var checked = publishChainOpt.find('input[type="checkbox"]:checked'),
                        label = '';
                    if(!optData || !optData.length){
                        label = 'No publish chain available';
                        publishChain.addClass('bd-groups-empty');
                    } else{
                        if(checked.length < 1){
                            label = 'Select a publish chain';
                        }else if(checked.length === 1){
                            label = checked.length + ' chain';
                        }else{
                            label = checked.length + ' chains';
                        }
                        publishChain.removeClass('bd-groups-empty');
                    }
                    publishChain.children('.bc-label').text(label);
                };

            jQuery('a, .bc-custom-checkbox',publishChainOpt).click( updateLabel );

            publishChain.data('portalPubChains' , portalPubChains.sort());
            //publishChainOpt.find('input[type="checkbox"]').hide()

            updateLabel();

        };


        // Delegate button action


        // Edit portal
        $portalDetailsViewport.on('click', '.bd-editButton', showEditForm);


        // Delete portal
        $portalDetailsViewport.delegate('.bd-removeButton', 'click', function() {
            archivePortal(bd.selectedPortalName);
        });

        // Export
        // jQuery(oWidgetBody).on('mouseover', '.bd-exportButton', bc.component.moTooltip);
        bc.component.setTooltip('.bd-exportButton', {
            context: oWidgetBody
        });

        $portalDetailsViewport.delegate('.bd-exportButton', 'click', function() {
            showExportForm(bd.selectedPortalName);
        });

        // duplicate portal
        var duplicatePortal = function(){
            bc.component.modalform({
                width: '310px',
                title: 'Duplicate portal',
                uid: 'duplicate_portal_modal',
                cls: 'bd-duplicate',
                content: '<fieldset><label>New name</label><input type="text" class="bd-new-portal-name required" maxlength="45" /></fieldset>',
                ok: 'Duplicate',
                cancel: 'Cancel',
                okCallback: function ($form) {
                    var formPortalName = $('.bd-new-portal-name', $form).val(),
                        newPortalName = string.slugify(formPortalName)
                            .replace(string.regex('stripSpecialCharsTitle'), '')
                            .replace(/\/+/g, '-');

                    if (encodeURIComponent(newPortalName).length < 100)
                    {
                    	be.utils.ajax({
	                        url : bd.contextRoot + '/portals/' + bd.selectedPortalName + '/duplicate.xml?targetName=' + newPortalName,
	                        cache: false,
	                        type: 'POST',
                            skipTimeout:true,
	                        success : function(responseDataXML){
	                            bc.component.notify({
	                                uid: 'duplicate_portal_success',
	                        		icon: 'checkbox', // checkbox, attention, error, loading
	                                message: 'Portal duplicated'
	                            });
	                            bd.refreshPortalList();
	                        }
	                    });
                    } else {
                        bc.component.notify({
	                        uid: 'encodedPortalNameMaxLenghtExceeded',
	                        icon: 'error', // checkbox, attention, error, loading
	                        message: 'Encoded portal name has exceeded maximum length of 100 characters.'
	                    });
	                    $('.bd-new-portal-name', $form).removeClass('valid error').addClass('error');
	                    return false;
                    }
                }
            });
        };
		$portalDetailsViewport.delegate('.bd-duplicateButton', 'click', duplicatePortal);

        // Cancel editing
        $portalDetailsViewport.delegate('.bd-cancelEditPortal', 'click', renderPortalDetails );


	};



    return {
        Start: startPortalManagement,
        refreshPortalDetails: getPortalDetails
    };

});



