/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : portal-export.js
 *  Description:
 *
 *  Provides portal export functionality
 *  @return {Object} Set of exported public methods
 *  @example
 *  bd.portalExport.exportPortal(portalName);
 *  ----------------------------------------------------------------
 */

/* globals define */


define(function(require, exports, module){

	'use strict';


	/**
	 * Definition of string constants used in module
	 * @type {String}
	 */
	/* API URLs */
	var EXPORT_API_CREATE = bd.contextRoot + '/orchestrator/export/exportrequests',
		EXPORT_API_DOWNLOAD = bd.contextRoot + '/orchestrator/export/files/',
		EXPORT_API_DIRECT = bd.contextRoot + '/export/package/',

		/* Text definition */
		TXT_MAP = {
			EXPORT_PORTAL_TXT_SUCCESS: 'Portal export is ready!',
			EXPORT_PORTAL_TXT_HELP: 'The exported zip file contains the portal Model (portal settings, permissions, pages, links, layouts and widgets) and portal Content (images and copy residing in the portal repository). It does not include resources.',
			EXPORT_PORTAL_TXT_TIPS: 'Notes:<ul><li>To import this portal on another environment, place the package file in the standard import folder, or the auto import folder.</li><li>Note that the Application configuration and the WAR with JSP-templates and other resources are not included. Deploy those artifacts separately.</li><li>The maximum size is 10000 model items.</li><li>Note that this prepares for a full deployment, not an incremental deployment.</li></ul>',
			EXPORT_PORTAL_TXT_NOTIFICATION: 'Exporting portal...',
			EXPORT_PORTAL_TXT_TITLE: 'export portal',

			/* Item export */
			EXPORT_WIDGET_TXT_HELP: "You can export single widget as a package containing all related resources, such as stylesheets, images and javascripts.",
			EXPORT_WIDGET_TXT_TIPS: "",
			EXPORT_WIDGET_TXT_TITLE: "export widget",
			EXPORT_WIDGET_TXT_NOTIFICATION: "Exporting widget...",

			EXPORT_CONTAINER_TXT_HELP: "You can export single layout as a package containing all related resources, such as stylesheets, images and javascripts.",
			EXPORT_CONTAINER_TXT_HELP_MODEL: "You can export single layout as a package. Resources for this layout are not included in the package.",
			EXPORT_CONTAINER_TXT_TIPS: "",
			EXPORT_CONTAINER_TXT_TITLE: "export layout",
			EXPORT_CONTAINER_TXT_NOTIFICATION: "Exporting layout...",

			EXPORT_CONTENTTYPE_TXT_HELP: "You can export structured content type definition as a package containing all necessary resources",
			EXPORT_CONTENTTYPE_TXT_TIPS: "",
			EXPORT_CONTENTTYPE_TXT_TITLE: "export type definition",
			EXPORT_CONTENTTYPE_TXT_NOTIFICATION: "Exporting type definition...",

			EXPORT_PAGE_TXT_HELP: "You can export master page as a package containing all necessary resources",
			EXPORT_PAGE_TXT_TIPS: "",
			EXPORT_PAGE_TXT_TITLE: "export master page",
			EXPORT_PAGE_TXT_NOTIFICATION: "Exporting master page...",

			EXPORT_TEMPLATE_TXT_HELP: "You can export template as a package containing all necessary resources",
			EXPORT_TEMPLATE_TXT_TIPS: "",
			EXPORT_TEMPLATE_TXT_TITLE: "export template",
			EXPORT_TEMPLATE_TXT_NOTIFICATION: "Exporting template...",

			EXPORT_FEATURE_TXT_HELP: "You can export feature as a package containing all necessary resources",
			EXPORT_FEATURE_TXT_TIPS: "",
			EXPORT_FEATURE_TXT_TITLE: "export feature",
			EXPORT_FEATURE_TXT_NOTIFICATION: "Exporting feature...",

			EXPORT_ITEM_TXT_FAILURE: 'Export failed!',

			/* Button captions */
			EXPORT_BTN_OK: 'OK',
			EXPORT_BTN_OR: 'OR',
			EXPORT_BTN_CLOSE: 'Close',
			EXPORT_BTN_DOWNLOAD: 'Download',
			EXPORT_BTN_EXPORT: 'Export',
			EXPORT_BTN_CANCEL: 'Cancel',

			/* Import */
			IMPORT_PORTAL_TXT_TITLE: 'import portal',
			IMPORT_WIDGET_TXT_TITLE: 'import widget',
			IMPORT_TXT_TIPS: 'Click to select file or drop it in area below',
			IMPORT_TXT_TIPS_IE: 'Select file',
			IMPORT_TXT_DROP: 'Drop file here',
			IMPORT_BTN_IMPORT: 'Import',
			IMPORT_BTN_CANCEL: 'Cancel',

			IMPORT_TXT_NOTIFICATION: 'Import in progress',
			IMPORT_TXT_NO_FILE: 'You must select file before import',
			IMPORT_TXT_SUCCESS_TITLE: 'Import succeeded',

			IMPORT_PORTAL_TXT_SUCCESS_TEXT: 'Portal imported successfully',
			IMPORT_WIDGET_TXT_SUCCESS_TEXT: 'Widget imported successfully',

			IMPORT_TXT_FAIL_TITLE: 'Import failed',
			IMPORT_TXT_FAIL_TEXT: 'Something went wrong'
		},

		/* Error messages */
		EXPORT_API_ERROR_CODES = {
			'*': 'Unknown server error',
			INTERNAL_SERVER_ERROR: 'Unknown server error',
			ERROR_EXPORTING_CONTENT: 'Error exporting content',
			ERROR_ACCESSING_ARCHIVE: 'Error accessing archive',
			ERROR_CREATING_PORTAL_SERVER_BUNDLE: 'Error creating portal server bundle',
			ERROR_EXPORTING_FROM_PORTAL_SERVER: 'Error exporting from portal server',
			ERROR_CREATING_EXPORT_ARCHIVE: 'Error creating export archive',
			FILE_NOT_FOUND: 'File not found',
			EXPORT_TOO_BIG: 'Too many items to export',
			ERROR_CREATING_TEMPLATE_EXPORT: 'Error exporting templates'
		},

		IMPORT_API_URL = bd.contextRoot + '/orchestrator/import/upload',

		IMPORT_API_ERROR_CODES = {
			'*': 'Unknown server error',
			INTERNAL_SERVER_ERROR: 'Unknown server error',
			ERROR_INVALID_ARCHIVE: 'Invalid portal archive',
			ERROR_IMPORTING_CONTENT: 'Error importing content',
			ERROR_READING_PORTAL_SERVER_BUNDLE: 'Error creating portal server bundle',
			ERROR_IMPORTING_TO_PORTAL_SERVER: 'Error importing to portal server',
			ERROR_UPLOADING: 'Error uploading',
			ERROR_MISSING_RESOURCES: 'Some portal resources missing (showing only first 10)',
			PORTALSERVER_CONNECTION_EXCEPTION: 'Portal server seems to be down'
		},

		EXPORT_TYPE = null,

		/**
		 * Download file by provided url
		 * @param  {String} url - Export package url
		 * @private
		 */
		downloadPackage = function (url) {
			var iframe, hiddenIFrameID = 'hiddenDownloader';
			iframe = document.getElementById(hiddenIFrameID);
			if (iframe === null) {
				iframe = document.createElement('iframe');
				iframe.id = hiddenIFrameID;
				iframe.style.display = 'none';
				document.body.appendChild(iframe);
			}
			iframe.src = url;
		},

		/**
		 * Export success callback
		 * @param  {XMLString} data - Response from RESTful API
		 * @private
		 */
		onExportSuccess = function (data, sItemType) {
			var $oData = jQuery(data),
				$tData = $oData.find('exportResponse'),
				$data = $tData.length ? $tData : $oData,
				tplData = {
					archiveLocation: be.utils.getNodeText('> archiveLocation', $data),
					archiveSize: be.utils.getNodeText('> archiveSize', $data),
					logfileLocation: be.utils.getNodeText('> logFileLocation', $data),
					identifier: be.utils.getNodeText('> identifier', $data)
				},
				str = be.utils.processHTMLTemplate("export/exportResponse", tplData);
			bc.component.dialog({
				title: TXT_MAP["EXPORT_" + sItemType.toUpperCase() + "_TXT_SUCCESS"],
				message: str,
				buttons: [{
						title: TXT_MAP.EXPORT_BTN_DOWNLOAD,
						type: 'white',
						callback: function () {
							downloadPackage(EXPORT_API_DOWNLOAD + tplData.identifier);
						}
				}, {
						title: TXT_MAP.EXPORT_BTN_OR,
						textOnly: true
				}, {
						title: TXT_MAP.EXPORT_BTN_CLOSE,
						type: 'text',
						callback: function () {}
				}]
			});

            // reenable session timeout
            bd.observer.notifyObserver(bd.notifications.session.TIMEOUT_OVERRIDE, {on:false});
		},

		/**
		 * Export failure callback
		 * @param  {Object} error
		 * @private
		 */
		onExportFailure = function (error, sItemType) {
			var message = be.utils.getNodeText('message', jQuery(error.responseXML));
			bc.component.dialog({
				title: TXT_MAP.EXPORT_ITEM_TXT_FAILURE,
				message: EXPORT_API_ERROR_CODES[message] || message,
				buttons: [{
						title: TXT_MAP.EXPORT_BTN_OK,
						type: 'white',
						style: {
							width: '100px'
						},
						callback: function () {}
				}]
			});

            // reenable session timeout
            bd.observer.notifyObserver(bd.notifications.session.TIMEOUT_OVERRIDE, {on:false});
		},

		/**
		 * Sends "create export" request
		 * @param  {String} sPortalName
		 * @param  {Boolean} bIncludeContent
		 * @param  {Boolean} bIncludeGroups
		 * @private
		 */
		requestPortalExport = function (sItemName, sItemType, bIncludeContent, bIncludeGroups, bIncludeResources, sPortalName) {
			var tplData = {
				itemName: sItemName,
				itemType: sItemType,
				includeContent: bIncludeContent,
				includeGroups: bIncludeGroups,
				includeResources: bIncludeResources,
				contextItemName: sPortalName ? sPortalName : '[BBHOST]'
			},
				xmlContent = be.utils.processXMLTemplate(sItemType + 'Export', tplData);

            // disable session timeout
            bd.observer.notifyObserver(bd.notifications.session.TIMEOUT_OVERRIDE, {on:true});

			be.utils.ajax({
				url: EXPORT_API_CREATE,
				data: xmlContent,
				success: function (data) {
					onExportSuccess(data, sItemType);
				},
				error: function (data) {
					onExportFailure(data, sItemType);
				},
				contentType: "text/xml",
				type: "POST",
				dataType: "xml",
                skipTimeout:true
			});
		},

		performDirectExport = function (itemName, portalName) {
			var	contextItem = portalName ? portalName + '/' : '',
				exportUrl = EXPORT_API_DIRECT + contextItem + itemName;

			downloadPackage(exportUrl);
		},


		/**
		 * Displays export dialog
		 * @param  {String} portalName - name of portal to export
		 * @public
		 */
		exportItem = function (itemName, itemType, modelOnly, portalName) {
			var isPortalExport = itemType && itemType.toLowerCase() === 'portal',
				templateName = isPortalExport ? 'portalForm' : 'genericForm',
				htmlContent = be.utils.loadTemplate('export/' + templateName, 'html', false),
				$html = jQuery(htmlContent),
				helpContainer = $html.find('.bd-export-help'),
				tipsContainer = $html.find('.bd-export-tips'),
				contentToggle = null,
				groupsToggle = null,
				resourcesToggle = null;


			/* init hints */
			bc.component.hint({
				target: helpContainer,
				message: TXT_MAP["EXPORT_" + itemType.toUpperCase() + "_TXT_HELP" + (modelOnly ? '_MODEL' : '')]
			});
			bc.component.hint({
				target: tipsContainer,
				message: TXT_MAP["EXPORT_" + itemType.toUpperCase() + "_TXT_TIPS"]
			});

			htmlContent = $html.get(0).outerHTML;

			$html = bc.component.modalform({
				width: '450px',
				title: TXT_MAP["EXPORT_" + itemType.toUpperCase() + "_TXT_TITLE"] + ': ' + itemName,
				uid: '1230',
				cls: 'bd-export',
				content: htmlContent,
				ok: TXT_MAP.EXPORT_BTN_EXPORT,
				cancel: TXT_MAP.EXPORT_BTN_CANCEL,
				okCallback: function ($form) {
					bc.component.notify({
						icon: 'loading',
						message: TXT_MAP["EXPORT_" + itemType.toUpperCase() + "_TXT_NOTIFICATION"]
					});
					if (isPortalExport) {
						requestPortalExport(itemName, itemType, !! $form.find('.bd-export-toggle-content').find('input[type="checkbox"]').attr('checked'), !! $form.find('.bd-export-toggle-groups').find('input[type="checkbox"]').attr('checked'), !! $form.find('.bd-export-toggle-resources').find('input[type="checkbox"]').attr('checked'), portalName);	
					} else {
						performDirectExport(itemName, portalName);
					}
				},
				cancelCallback: function () {}
			});


			contentToggle = $html.find('.bd-export-toggle-content');
			groupsToggle = $html.find('.bd-export-toggle-groups');
			resourcesToggle = $html.find('.bd-export-toggle-resources');
			bc.component.toggleSwitch({
				target: contentToggle,
				checked: true
			});
			bc.component.toggleSwitch({
				target: groupsToggle,
				checked: true
			});
			bc.component.toggleSwitch({
				target: resourcesToggle,
				checked: true
			});
		},

		trunkatePath = function(path, length, separator){
			if (path.length <= length){
				return path;
			}
			separator = separator || '/.../';
			var splPath = path.split('/');
			var filename = splPath[splPath.length-1];
			var remaining = length - filename.length - separator.length;

			return path.substr(0, remaining) + separator + filename;
		},

		importErrorReport = function($data){
			// console.log($data);
			var tplData = {
				missingResources: []
			};
			$data.find('importValidatorResponse > resources > resource').each(function(idx, res){
				var $res = jQuery(res);
				var path = $res.attr('path');
				var trunkated = trunkatePath(path);
				tplData.missingResources.push({
					path: path,
					pathShort: trunkatePath(path, 50, '/.../')
				});
			});

			return be.utils.processHTMLTemplate('export/missingResources', tplData);
		},

		/**
		 * Displays import dialog
		 */
		importItem = function (itemType, callback) {

			itemType = itemType || 'portal';

			var tplData = {
				dropZoneText: jQuery.browser.msie ? '' : TXT_MAP.IMPORT_TXT_DROP
			};
			var htmlContent = be.utils.processHTMLTemplate("portals/importForm", tplData),
				$html = jQuery(htmlContent),
				helpContainer = $html.find('.bd-export-help'),
				tipsContainer = $html.find('.filename'),
				contentToggle = null,
				groupsToggle = null,
				importData = {},
				currentStep = 0,
				importStatusTitle = null,
				importStatusMessage = null,
				importStatus = null,
				notification = null,
				$uploadContainer;

			bc.component.hint({
				target: tipsContainer,
				message: jQuery.browser.msie ? TXT_MAP.IMPORT_TXT_TIPS_IE : TXT_MAP.IMPORT_TXT_TIPS
			});

			htmlContent = $html.get(0).outerHTML;

			$html = bc.component.modalform({
				width: '450px',
				title: TXT_MAP['IMPORT_' + itemType.toUpperCase() + '_TXT_TITLE'],
				cls: 'bd-export',
				content: htmlContent,
				ok: TXT_MAP.IMPORT_BTN_IMPORT,
				cancel: TXT_MAP.IMPORT_BTN_CANCEL,
				okCallback: function ($form) {
					if (importData.fileName && importData.data) {
						notification = bc.component.notify({
							icon: 'loading',
							message: TXT_MAP.IMPORT_TXT_NOTIFICATION,
							delay: -1
						});
						importData.data.submit();
					} else {
						bc.component.notify({
							icon: 'error',
							message: TXT_MAP.IMPORT_TXT_NO_FILE
						});
						return false;
					}
				},
				cancelCallback: function () {}
			});

			$uploadContainer = jQuery('.bd-import-uploadForm', $html);

			if (!jQuery.browser.msie) {
				$uploadContainer.bind('click', function () {
					jQuery('.bd-import-uploadForm-uploadField', $html).trigger('click');
				});
			}

			be.utils.appendXSRFTagTo($uploadContainer);

			$uploadContainer.fileupload({
				type: 'POST',
				fileInput: jQuery('.bd-import-uploadForm-uploadField'),
				url: IMPORT_API_URL,
				dropZone: $uploadContainer,
				forceIframeTransport: true,
				singleFileUploads: true,
				// replaceFileInput: false,
				add: function (e, data) {
					importData.data = data;
					importData.fileName = data.files[0].name;
					$html.find('.bd-import-uploadForm span').text(importData.fileName);
					$html.find('.bd-import-uploadForm').addClass('file-selected');
				},
				done: function (e, data) {
					if (data.result.find('importErrorMessage').length > 0){
						importStatus = 'error';
						importStatusTitle = TXT_MAP.IMPORT_TXT_FAIL_TITLE;
						if (data.result.find('importErrorMessage > importValidatorResponse').length > 0){
							var $msg = data.result.find('importErrorMessage > message');
							importStatusMessage = ($msg.length > 0 ? $msg.text() : IMPORT_API_ERROR_CODES['ERROR_MISSING_RESOURCES']) + importErrorReport(data.result);
						}
					} else if (data.result.find('errorMessage').length > 0){
						importStatus = 'error';
						importStatusTitle = TXT_MAP.IMPORT_TXT_FAIL_TITLE;
						importStatusMessage = data.result.find('errorMessage > message').text();
					} else {
						importStatus = 'ok';
						importStatusTitle = TXT_MAP.IMPORT_TXT_SUCCESS_TITLE;
						importStatusMessage = TXT_MAP['IMPORT_' + itemType.toUpperCase() + '_TXT_SUCCESS_TEXT'];
					}
				},
				fail: function (e, data) {
					var errorCode = be.utils.getNodeText('message', jQuery(data.jqXHR.responseXML));
					importStatus = 'error';
					importStatusTitle = TXT_MAP.IMPORT_TXT_FAIL_TITLE;
					importStatusMessage = IMPORT_API_ERROR_CODES[errorCode] || IMPORT_API_ERROR_CODES['*'];
				},
				always: function (e, data) {
					notification.hide();
					bc.component.dialog({
						title: importStatusTitle,
						message: importStatusMessage,
						status: importStatus,
						buttons: [{
							title: TXT_MAP.EXPORT_BTN_OK,
							type: 'white',
							style: {
								width: '100px'
							},
							callback: function () {
								if (typeof callback === 'function') {
									callback();
								}
							}
						}]
					});
				}

			});
		};


	module.exports = {
		exportItem: exportItem,
		importItem: importItem
	};


});