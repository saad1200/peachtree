/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : app.js
 *  Description: Portal Details Migrations
 *
 *  ----------------------------------------------------------------
 */

/* globals define */

define([
    'zenith/promise',
    'zenith/http/portals',
    'zenith/core/errors-handler',
    'angular',
    'dashboard/widgets/PortalMgmt/scripts/portalmanagement',
    'backbase.com.2014.components/modules/thumbnail/scripts/app',
    'backbase.com.2014.components/scripts/modal',
    'backbase.com.2014.components/scripts/learnmore',
    'dashboard/widgets/PortalMgmt/scripts/portal-mnt'
], function(Promise, httpPortals, error, angular, pm, Thumbnail) {

    'use strict';

    var name = 'cxpPortalManagement';

    var deps = [
        Thumbnail.name
    ];

    /*----------------------------------------------------------------*/
    /* Main App
    /*----------------------------------------------------------------*/
     var Runner = function($rootScope, Widget) {

        // Get url2state property
        var portalName = Widget.getPreference('portalName'),
            portalDetails;

        if (portalName && portalName !='_'){

            portalDetails = httpPortals.portalDetails(portalName);

            $rootScope.isPortalCatalog = true;
            Promise.all(portalDetails)
                .then(function(portal) {
                    if(portal) {
                        pm.Start(Widget.body, portal);
                    }

                    $rootScope.$broadcast('portaldata:available');

                }, function(err) {
                    error.trigger(err);
                });
        } else {
            error.trigger(new Error('Portal not found'));
        }

    };

    return angular.module(name, deps)
        .run(['$rootScope', 'Widget', Runner]);
});