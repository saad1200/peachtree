/**
 *  ----------------------------------------------------------------
 *  Copyright © 2003/2014 Backbase B.V.
 *  ----------------------------------------------------------------
 *  Author : Backbase R&D - Amsterdam - New York
 *  Filename : portal-mnt.js
 *  Description:
 *
 *  Create a paged list of portal
 *  ----------------------------------------------------------------
 */

/* globals define */


 // TODO: refactor all this


define([
    'jquery',
    'dashboard/widgets/PortalMgmt/scripts/portalmanagement'
], function($, pm) {
    'use strict';

    var MAXIMUM_PORTAL_TITLE_LENGTH = 32;

    var buildThumbnailUrl = function (properties) {
        if (properties.hasOwnProperty('thumbnailUrl')) {
            return  properties.thumbnailUrl.value.toString().replace('$(contextRoot)', bd.contextRoot);
        }
        return null;
    };

    var formatPortalsData = function(json){

        //Render Portal List:
        var data = [],
            portals = (json.portals.portal instanceof Array) ? json.portals : { portal: [json.portals.portal ]}, // handle n or 1 results
            pageCount = 0;

        for(var i = 0; i < portals.portal.length; i++){

            if (portals.portal[i]['securityProfile'] == 'CONSUMER' && portals.portal[i]['name'] == 'dashboard'){
                continue;
            }
            var current = portals.portal[i];
            var properties = current.properties;

            if(properties['title'] === null || properties['title'].value === ""){
               properties['title'] = {value: "No title"};
            }
            if(properties['DefaultDevice'] === null || properties['DefaultDevice'].value === ""){
               properties['DefaultDevice'] = {value: "NONE"};
            }
            data.push({
                securityProfile:    current['securityProfile'],
                title:              properties['title'].value,
                portalName:         current['name'],
                deviceTarget:       properties.hasOwnProperty('TargetedDevice') ? properties['TargetedDevice'].value : '',
                defaultDevice:      properties['DefaultDevice'].value,
                thumbnailUrl:       buildThumbnailUrl(properties),
                truncatedTitle:     (properties['title'].value !== null) ? be.utils.truncateText(properties['title'].value, MAXIMUM_PORTAL_TITLE_LENGTH) : properties['title']['itemName'],
                thumbnailLabel:     'insert portal image'
            });

            pageCount++;
        }
        return data;
    };

    var onError = function(xhr){
        var msg = be.utils.getErrorText(xhr);
        bc.component.notify({
            uid: '292822',
            icon: 'error', // checkbox, attention, error, loading
            message: msg
        });
        if (typeof callback === 'function') {
            callback.call(this, arguments);
        }
    };

    bd.createPortalList = function() {

        var portalList = function() {

            var NUMBER_OF_ITEMS_PER_PAGE = 3;
            var PORTAL_ITEM_TEMPLATE = "portals/portalListView";
            var dotTemplate = '<span class="bd-listPageDots">.</span>';
            var $portalListViewport = $('.bd-portalList');
            var $listContainer = $($portalListViewport).find('.bd-listDataholder')[0];
            var $dotsContainer = $($portalListViewport).find('.bd-listPageDotsContainer')[0];
            var $portalSearchOverlay = $('<div class="bd-portalSearchOverlay"></div>').appendTo($portalListViewport);
            var portalSearchQuery = '', portalSearchTimeout, portalSearchTimeoutHide;
            var currentPage = 0;
            var hasRight = false;
            var hasLeft = false;
            var totalPages = 0;
            var portalsData;

            var xmlPortalList_responseHandler = function(response, ShouldBindUi) {
                var jsonResponse = bd.xmlToJson({ xml: response }),
                    templateData, pageCount;

                if (jsonResponse === null) return;

                if(jsonResponse.portals.portal == undefined) {
                    //FIXME: remove this hard coded url
                    //if it is not logged in, redirect to the default login page
                    //window.location = bd.contextRoot + "/portals/dashboard/";
                    window.location.reload(true);
                }


                portalsData = formatPortalsData(jsonResponse);
                templateData = portalsData;
                pageCount = portalsData.length;

                // Matt: make copy of portals list available globally
                bd.portals = templateData.concat([]);
                if(!bd.portals.length){
                    $('.bd-listNextPageButton', $portalListViewport).remove();
                }
                totalPages = Math.ceil(pageCount / NUMBER_OF_ITEMS_PER_PAGE);
                var renderedHtml = be.utils.processHTMLTemplate(PORTAL_ITEM_TEMPLATE, { portals: templateData });
                $($listContainer).html(renderedHtml);
                //Render Pagination Dots:
                var $container = $($dotsContainer);
                if (templateData.length > 3) {
                    setVisibility($dotsContainer, true);
                    for (i = 0; i < totalPages; i++) {
                        var newDot = $(dotTemplate);
                        newDot.attr("page", i);
                        newDot.css('cursor', 'pointer');
                        if (currentPage == i) {
                            newDot.addClass('bd-active');
                        }
                        newDot.appendTo($container);
                    }
                } else {
                    $container.html("");
                }
                animatePageScroll();

                // Bind UI:
                if (ShouldBindUi) {
                    bindUi();
                }
            };

            var bindUi = function() {
                $portalListViewport.delegate('.bd-listNextPageButton', 'click', function() {
                    if (currentPage < totalPages - 1) {
                        currentPage++;
                        animatePageScroll();
                    }
                });
                $portalListViewport.delegate('.bd-listPreviousPageButton', 'click', function() {
                    if (currentPage > 0) {
                        currentPage--;
                        animatePageScroll();
                    }
                });
                $portalListViewport.delegate('.bd-listPageDots', 'click', function() {
                    currentPage = Number($(this).attr('page'));
                    animatePageScroll();
                });
                $portalListViewport.delegate('.bd-portalListItem', 'click', function(event) {
                    bd.selectedPortalName = $(this).attr('portalname');
                    bd.selectedPortalTitle = $(this).attr('portaltitle');
                    bd.selectedPortalOptimizedFor = $(this).attr('defaultDevice');
                    bd.selectedPortalTargetDevice = $(this).attr('devicetarget');
                    bd.loadPage('launcher',event);
                });

                $portalListViewport.on('mousewheel DOMMouseScroll', function(event){
                    event.preventDefault();
                    event.stopPropagation();
                    animateItemScroll(event.originalEvent.wheelDelta || -event.originalEvent.detail);
                });

                /**
                 * Quick search for portals
                 * @easteregg
                 */
                $('body').on('keypress', function(event){
                    if (be.dialogContainersStack.length === 0 && $('.bc-modal-cover').length === 0){
                        if (!event.ctrlKey && !event.metaKey && event.which > 46){
                            clearTimeout(portalSearchTimeout);
                            clearTimeout(portalSearchTimeoutHide);
                            var userInput = String.fromCharCode(event.which);
                            portalSearchQuery += userInput;

                            $portalSearchOverlay.text(portalSearchQuery);

                            $portalSearchOverlay.css({
                                opacity: 0.8,
                                marginLeft: -$portalSearchOverlay.width() / 2,
                                zIndex: 999,
                                visibility: 'visible'
                            });

                            portalSearchTimeout = setTimeout(function(){
                                var re = new RegExp(portalSearchQuery, "i");
                                for (var i = 0, l = bd.portals.length; i < l; i++){
                                    if (re.test(bd.portals[i].title)){
                                        var nPage = Math.floor(i / NUMBER_OF_ITEMS_PER_PAGE);
                                        setPage(nPage);
                                        animatePageScroll();
                                        $('.bd-portalListItem[portalname="' + bd.portals[i].portalName + '"]', $portalListViewport)
                                            .addClass('bd-portalSelected')
                                            .siblings().removeClass('bd-portalSelected');
                                        break;
                                    }
                                }
                            }, 100);

                            portalSearchTimeoutHide = setTimeout(function(){
                                $portalSearchOverlay.css({
                                    opacity: 0
                                });
                                portalSearchTimeoutHide = setTimeout(function(){
                                    $portalSearchOverlay.css({
                                        zIndex: -1,
                                        visibility: 'hidden'
                                    });
                                }, 100);
                                portalSearchQuery = '';
                            }, 500);
                        } else if (event.which === 13){
                             $('.bd-portalListItem.bd-portalSelected', $portalListViewport).trigger('click');
                        }
                    }
                });
                $('body').on('click', function(){
                    if (be.dialogContainersStack.length === 0 && $('.bc-modal-cover').length === 0){
                        $('.bd-portalListItem.bd-portalSelected', $portalListViewport).removeClass('bd-portalSelected');
                    }
                });
            };

            var scrolling = false;
            var itemWidth = 285, leftAmount = 855, offset = 10;

            var animatePageScroll = function() {
                scrolling = true;
                $($listContainer).animate({left: (- currentPage * leftAmount - offset) + 'px'}, {duration: 500, complete: function(){
                    scrolling = false;
                }});
                var dots = $($dotsContainer).find('.bd-listPageDots');
                $.each(dots, function(index, value){
                    if (index === currentPage) {
                        $(value).addClass('bd-active');
                    } else {
                        $(value).removeClass('bd-active');
                    }
                });
                toggleArrowButtons();
            };

            var animateItemScroll = function(delta){
                if (!scrolling){
                    scrolling = true;
                    if (delta < 0){
                        $('.bd-listNextPageButton', $portalListViewport).trigger('click');
                    } else if (delta > 0){
                        $('.bd-listPreviousPageButton', $portalListViewport).trigger('click');
                    }
                    setTimeout(function(){
                        scrolling = false;
                    }, 500);
                }
            };

            var setVisibility = function(element, show, delegatingElement, delegate, eventType, callback ) {
                if (element) {
                    if (show) {
                        $(element).removeClass('bd-hideElement');
                        if (delegatingElement) {
                            $(delegatingElement).delegate(delegate, eventType, callback);
                        }
                    } else {
                        $(element).addClass('bd-hideElement');
                        if (delegatingElement) {
                            $(delegatingElement).undelegate(delegate, eventType, callback);
                        }
                    }
                }
            };

            var toggleArrowButtons = function() {
                var nextButtonSelector = '.bd-listNextPageButton';
                var prevButtonSelector = '.bd-listPreviousPageButton';
                var nextButton = $($portalListViewport).find(nextButtonSelector)[0];
                var prevButton = $($portalListViewport).find(prevButtonSelector)[0];
                if (hasRight || hasLeft){
                    setVisibility(nextButton, hasRight);
                    setVisibility(prevButton, hasLeft);
                    hasRight = false;
                    hasLeft = false;
                    return;
                }
                if (totalPages == 1) {
                    setVisibility(nextButton, false);
                    setVisibility(prevButton, false);
                } else {
                    if (currentPage === 0) {
                        setVisibility(nextButton, true);
                        setVisibility(prevButton, false);
                    } else if (currentPage == totalPages - 1) {
                        setVisibility(nextButton, false);
                        setVisibility(prevButton, true);
                    } else {
                        setVisibility(nextButton, true);
                        setVisibility(prevButton, true);
                    }
                }

            };

            var buildRestQueryUrl = function() {
                var sortParam = 'lastModifiedTimestamp(dsc)';
                var portalSize = '1000';
                return  bd.contextRoot + '/portals.xml' + '?s=' + sortParam + '&ps=' + portalSize;
            };

            var setPage = function(newPage) {
                currentPage = newPage;
            };

            var loadPortals = function(ShouldBindUi) {
                ShouldBindUi = !(ShouldBindUi || false);
                $($dotsContainer).html("");
                var url = buildRestQueryUrl();
                be.utils.ajax({
                      url: url,
                      data: null,
                      cache: false,
                      success: function(response) {
                            xmlPortalList_responseHandler(response, ShouldBindUi);
                        },
                      dataType: "xml",
                      accept: "application/xml;charset=UTF-8"
                    });
            };

            var refreshPortals = function() {
                $($listContainer).html("");
                loadPortals(true);
            };

            return {
                loadPortals: loadPortals,
                refreshPortals: refreshPortals,
                setPage: setPage
            };
        }();

        // *********
        // These functions may be causing a UI error with the operationCompleteMsg
        // cannot animate containers while these are executing

        bd.portalList = portalList;
        portalList.loadPortals();

    };

    /* Refresh the portal list */
    bd.refreshPortalList = function(bRewind) {

        if(bd.portalList){
            bd.portalList.setPage(0);
            bd.portalList.refreshPortals();
        }
    };


    bd.cancelCreatePortal = function() {
        try {
            $('#portalDetailsArea').hide();
            $('.bd-portalDetailsViewport').show();
            return be.closeCurrentDialog();
        } catch(e) {
            console.log(e);
        }
    };

    bd.syncOptimizedForView = function(OptimizedFor) {
        if ($('#OptimizedForView .bd-selected').attr('optimizedFor') == 'tablet') {
            $('#OptimizedForView .bd-selected .bd-optimizedFor-label').text('Tablet');
        } else if ($('#OptimizedForView .bd-selected').attr('optimizedFor') == 'mobile') {
            $('#OptimizedForView .bd-selected .bd-optimizedFor-label').text('Mobile');
        }
        $('#OptimizedForView .bd-selected').removeClass('bd-selected');
        $('#OptimizedForView .bd-optimizedFor-' + OptimizedFor).addClass('bd-selected');
        $("#OptimizedFor").val(OptimizedFor);
    };

    bd.createNewPortal = function(event) {

        if(bd.designMode != "null")
            return false;

        var templateData = {
            contextRoot : bd.contextRoot,
            urlRoot : be.utils.getURLRoot(),
            mobile : bd.uiEditingOptions.portalDevices.mobile,
            tablet : bd.uiEditingOptions.portalDevices.tablet
        };
        var htmlContent = $(be.utils.processHTMLTemplate("portals/createPortal", templateData));

        var $optimizedView = $('#OptimizedForView', htmlContent);

        var tabletDropdown = bc.component.dropdown({
            type: 'device-select',
            device: 'tablet',
            deviceName: 'Tablet',
            options: bd.uiEditingOptions.portalDevices.tablet
        });
        var mobileDropdown = bc.component.dropdown({
            type: 'device-select',
            device: 'mobile',
            deviceName: 'Mobile',
            options: bd.uiEditingOptions.portalDevices.mobile
        });

        var mobileOptions = mobileDropdown[0].dropdownOptions;
        var tabletOptions = tabletDropdown[0].dropdownOptions;

        $('#OptimizedForView', htmlContent).append(tabletDropdown);
        $('#OptimizedForView', htmlContent).append(mobileDropdown);

        be.openDialog({
            event: event,
            htmlContent: htmlContent,
            standAlone: true,
            className: 'bd-createPortalDialog',
            small: true,
            headerCloseIcon:false,
            containerMargins: {
                mLeft: 330,
                mTop: 200
            }
        });

        $('a',tabletOptions).click( function() {
            updateOptimized(this, tabletDropdown);
        });
        $('a',mobileOptions).click( function() {
            updateOptimized(this, mobileDropdown);
        });

        var updateOptimized = function(This, parentDropdown) {
            var targetDevice = $('.bc-device-name', This).text();
            if (targetDevice.toLowerCase() == 'all tablet devices') {
                targetDevice = 'Tablet';
            } else if (targetDevice.toLowerCase() == 'all mobile devices') {
                targetDevice = 'Mobile';
            }
            $('.bd-optimizedFor-label', parentDropdown).text(targetDevice);
        };

        $('#portalTitle').focus();

        $optimizedView.delegate('.bd-optimizedFor', 'click', function(e) {
            bd.syncOptimizedForView($(this).attr('optimizedfor'));
        });

        bd.syncOptimizedForView($optimizedView.children('.bd-optimizedFor-mixed').attr("optimizedfor"));

    //  setTimeout(function() {
    //  bd.operationCompleteMsg("Creating Portal ...");
    //}, 2000);
        var onCreating = false;  //prevent mutiple clicks on the save button.
        var saveNewPortal = function(formObj) {
    //        bd.cancelCreatePortal();
            try {
                var name = formObj.portalName.value;
                if(onCreating) return;
                onCreating = true;
                bd.savePortalFromForm(formObj, 'POST', function(data){
                    onCreating = false;
                });
            } catch(exception) {
                console.log(exception);
            }
            return false;
        };

        //      $.validator.addMethod("portalTitle", function(value, element, params) {
        //        return this.optional(element) || (value.contains("'") ||  value.contains('"'));
        //       }, $.format("Single quotes and double quotes are not allowed."));


        be.utils.addCustomMethodToValidator(['validTitle']);
        $('#newPortalForm').validate({
            rules: {
                portalTitle: {
                    validTitle: true
                }
            },
            messages: {
                portalTitle: {
                    validTitle: "Portal title can not contain any special characters"
                }

            },
            submitHandler:
                function(data){
                    // timeout to make sure portal title and portal path are synced before we save
                    setTimeout(function(){
                        saveNewPortal(data);
                    }, 100);
                }
        });

        bd.initEditable($('#newPortalForm'));

        bd.enableSwitch($("#addAllWidgets"));


        return false;
    };


    /** from form data */
    bd.savePortalFromForm = function(formObj, httpMethod, callback) {

        // console.log(formObj);
        var title = be.utils.formatAllowedTitle(formObj.portalTitle.value);
        var name = formObj.portalName.value;
        var thumbnailUrl = formObj.thumbnailUrl.value;
        var portalTags = null;//formObj.portalTags.value;
        var addAllWidgets = false;
        if(formObj.addAllWidgets && formObj.addAllWidgets.checked)
            addAllWidgets = true;

        var OptimizedFor = $('#OptimizedFor').val();
        var oldPortalName = null;

        if(formObj.oldPortalName){
            oldPortalName = formObj.oldPortalName.value;
        }

        var pubChainDD = $('.bd-publishchain-dd').find('.bc-multiselect'),
            pubChains = [], pubChainValue = '', oldPubChainValue = pubChainDD.data('portalPubChains'), isChanged = false;
        if(pubChainDD.length){
            $(pubChainDD[0].dropdownOptions).find('a.bc-dropdown-item')
                .each(function(idx, elm) {
                    if($(elm).find('input[type="checkbox"]:checked').length){
                        pubChains.push($(elm).attr('data-value'));
                    }
                });
            if(pubChains.length === oldPubChainValue.length){
                var i,l,c, tmp = (pubChains.slice(0)).sort();
                for(i = 0, l = oldPubChainValue.length; i < l; i++){
                    c = oldPubChainValue[i];
                    if(oldPubChainValue[i] !==  tmp[i]){
                        isChanged = true;
                        break;
                    }
                }
            }else{
                isChanged = true;
            }

            pubChainValue = pubChains.join(';');
        }

        var save = function(){
            bd.savePortalWithInputData(title, name, oldPortalName, addAllWidgets, OptimizedFor, httpMethod, portalTags, callback, pubChainValue, thumbnailUrl);
        };

        if(!isChanged){
            save();
        }else{
            be.utils.confirm({
                title: 'UPDATE PORTAL',
                message: "You are about to change the publication end point. When you change the publication end point, the status of all published items will be set to 'not published'.",
                okBtnText: 'Save',
                yesCallback: save
            });
        }

    };

    /** no form data parsing */
    bd.savePortalWithInputData = function(title, name, oldPortalName, addAllWidgets, OptimizedFor, httpMethod, portalTags, callback, pubChainValue, thumbnailUrl) {
        var selectedTargetDevice = OptimizedFor;
        if(OptimizedFor == "tablet" || OptimizedFor == "mobile"){
            selectedTargetDevice = $('#OptimizedForView').find('*[optimizedFor='+OptimizedFor+']').children('.bd-optimizedFor-label').text();
            if (selectedTargetDevice == "Tablet usage") {
                selectedTargetDevice = "Tablet";
            } else if (selectedTargetDevice == "Mobile usage") {
                selectedTargetDevice = "Mobile";
            }
        }

        var templateData = {
            name : name,
            title : title,
            OptimizedFor : OptimizedFor,
            targetDevice: selectedTargetDevice,
            thumbnailUrl: thumbnailUrl,
            hasTags: false, //disabled tags
            tags: [], //disabled tags
            hasPublishchain : typeof pubChainValue == 'string' || pubChainValue instanceof String ,
            publishChains : pubChainValue
        };

        var XMLTemplateName = 'createPortal';
        var xmlContent = "";
        var url = bd.contextRoot + "/portals.xml";
        if(httpMethod == "PUT"){
            url = bd.contextRoot + "/portals/" + oldPortalName + ".xml";
            XMLTemplateName = 'updatePortal';
            xmlContent = be.utils.processXMLTemplate(XMLTemplateName, templateData);
        } else if (httpMethod == "POST") {
            url = bd.contextRoot + "/portals/"+name+"/createPortal?title="+title+"&targetDevice="+selectedTargetDevice+"&optimizedFor="+OptimizedFor+"&addAllWidgets="+addAllWidgets;
        }

        var onSuccess = function(data){
            var $portalDialog = $('#portalLauncher-tabs');
            $('#bd-saveButton', $portalDialog).attr('disabled', 'true');
            $('.bd-cancelButton', $portalDialog).attr('disabled', 'true');
            $('.bd-popupForm-closeButton', $portalDialog).attr('onclick', '');
            // temporary settimeout to fix portal creation message bug
            //  --the actual cause has something to do with the UI getting stuck when refreshPortalList() is called below
            // to not display the "complete message" (which would remove the "Creating Portal" message)

            if(httpMethod == "POST"){
                bc.component.notify({
                    uid: '0012',
                    icon: 'loading', // checkbox, attention, error, loading
                 // delay: 10000,
                    message: 'Creating ' + title
                });
            }

            //bd.operationCompleteMsg("Creating " + title, 'loadingIcon');
            setTimeout( function() {
                if(httpMethod == "POST") {
                    bd.cancelCreatePortal();
                    if (typeof callback === 'function') {
                        callback.call(this, arguments);
                    }
                    bc.component.notify({
                        uid: '2928',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: title + ' was created successfully.'
                    });

                    pm.refreshPortalDetails();

                } else {
                    // portal data is kept in global space, so we have to keep track of that
                    bd.selectedPortalName = name;
                    bd.selectedPortalTitle = title;
                    bd.selectedPortalOptimizedFor = OptimizedFor;
                    bd.selectedPortalTargetDevice = selectedTargetDevice;

                    pm.refreshPortalDetails();

                    bc.component.notify({
                        uid: '2928',
                        icon: 'checkbox', // checkbox, attention, error, loading
                        message: title + ' was updated successfully.'
                    });
                }
            }, 400);

        };
        be.utils.ajax({
            url : url,
            data : xmlContent,
            success : onSuccess,
            error: onError,
            type : httpMethod
        });

        return false;
    };

    bd.updatePortal = function(formObj) {
        bd.savePortalFromForm(formObj, 'PUT');
    };

    bd.updatePortalName = function(inputObj) {
        var newValue = be.utils.removeNonAlphaNumericChars(inputObj.value);
        inputObj.form.portalName.value = newValue;
        inputObj.form.portalPath.value = bd.contextRoot + "/" + newValue + "/pages/index";
        return false;
    };

    // Add page
    bd.addPage = function(templateData, portalName, callback) {
        var xmlContent = be.utils.processHTMLTemplateByUrl(be.contextRoot + "/static/backbase.com.2012.nexus/widgets/PageMgmtMVC/xml/pages/createPage.html", templateData);
        be.utils.ajax({
            url : bd.contextRoot + "/portals/" + portalName + "/pages.xml",
            data : xmlContent,
            success : callback,
            type : "POST"
        });
        return false;
    };

    // Add link
    bd.addLink = function(templateData, callback, errorCallback) {
        var xmlContent = be.utils.processHTMLTemplateByUrl(be.contextRoot + "/static/backbase.com.2012.nexus/widgets/PageMgmtMVC/xml/links/createLink.html", templateData);
        be.utils.ajax({
            url : bd.contextRoot + "/portals/" + templateData.portalName + "/links.xml",
            data : xmlContent,
            success : function(data){
                if(callback) callback(data);
            },
            error: errorCallback,
            type : "POST"
        });
        return false;
    };

    // delete link
    bd.deleteLink = function(linkName, portalName, callback, errorCallback) {
        be.utils.ajax({
            url : bd.contextRoot + "/portals/" + portalName + "/links/" + linkName + ".xml",
            success : callback,
            error: errorCallback,
            type : "DELETE"
        });
        return false;
    };

    // create basic portal
    bd.createPortal = function(portalName, portalTitle, OptimizedFor, addAllWidgets, callback, errorCallback) {
        var url = bd.contextRoot + "/portals/"+portalName+"/createPortal?title="+
                portalTitle+"&targetDevice="+OptimizedFor+"&optimizedFor="+OptimizedFor+"&addAllWidgets="+addAllWidgets;
        be.utils.ajax({
            url : url,
            success : function(data){
                callback(data);
            },
            error : function(data) {
                errorCallback(data);
            },
            type : 'POST'
        });
        return false;
    };

    // delete portal
    bd.deletePortal = function(portalName, callback, errorCallback) {
        be.utils.ajax({
            url : bd.contextRoot + "/portals/" + portalName + ".xml",
            success : callback,
            error: errorCallback,
            type : "DELETE"
        });
        return false;
    };

    bd.getPortalsModel = function(){
        var sortParam = 'lastModifiedTimestamp(dsc)',
            portalSize = '1000',
            url =  bd.contextRoot + '/portals.xml' + '?s=' + sortParam + '&ps=' + portalSize,
            dfd = new $.Deferred(),
            promise = dfd.promise();

        var onPortals = function(data){
            var json = bd.xmlToJson({ xml: data });
            if (json === null) return;
            bd.portalsModel = formatPortalsData(json);
            dfd.resolve(bd.portalsModel);
        };

        be.utils.ajax({
            url: url,
            data: null,
            cache: false,
            dataType: "xml",
            accept: "application/xml;charset=UTF-8"
        }).done(onPortals);

        return promise;
    };

    return {};
});
