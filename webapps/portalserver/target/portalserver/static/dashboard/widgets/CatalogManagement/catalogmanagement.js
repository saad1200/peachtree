/**
 * Copyright © 2013 Backbase B.V.
 */
b$.module("bd.widgets.CatalogManagement", function() {
	'use strict';

	var renderUI = function(oWidget){
		var $widget = jQuery(oWidget).html('<div class="bd-widgetContent"></div>'),
			oContainer = $widget.find(".bd-widgetContent");
		bd.CatalogManagement.renderUI(oContainer, null);
	};

	var renderDashboard = function(oWidget){
		var $widget = jQuery(oWidget)
			.html(
				'<div class="bd-widgetHeader bd-widget-CatalogManagement">' +
					'<div class="bd-widgetIcon"></div>' +
					'<div class="bd-widgetTitle">Catalog</div>' +
				'</div>' + 
				(bd.designMode != 'null' ? '<div class="bd-iconEditWidgetProperties">edit</div>' : '') +
				'<div class="bd-widgetContent"></div>'
			),
			oContainer = $widget.find(".bd-widgetContent");
	
		bd.CatalogManagement.renderDashboard(oContainer, null);
	};

	this.Maximized = renderUI;
	this.Dashboard = renderDashboard;
});
