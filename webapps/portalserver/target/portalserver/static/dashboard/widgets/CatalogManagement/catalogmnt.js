/**
  * Copyright � 2011 Backbase B.V.
 */
bd.CatalogManagement = (function() {
	var oWidgetBody;

	// private functions

	// get variables for current tab
	var getCurrentTabInfo = function(activeTab) {
		//var oGadgetBody = jQuery('.bd-catalogMnt-container', );
		var activeTab = activeTab || oWidgetBody.find('.bd-tabs > .bd-activeTab');
		var itemPathname = jQuery(activeTab).attr('itemtype');
		var itemType = itemPathname.toUpperCase();
		return {
			itemType : itemType,
			activeTab : activeTab
		}
	};

	/**
	* this function calculates values for Dashboard tile
	*/
	// Anyway not to hardcode these?
	var renderDashboard = function(oGadgetBody) {
        getServerCatalogItems('WIDGET', '', function(serverCatalogWidgets){
            getPortalCatalogItems('WIDGET', '', function(portalCatalogWidgets){
                getServerCatalogItems('CONTAINER', '', function(serverCatalogLayouts){
                    getPortalCatalogItems('CONTAINER', '', function(portalCatalogLayouts){
                        jQuery(oGadgetBody).html('Widgets<span class="bd-outcome-right">' + portalCatalogWidgets.length + '/' + serverCatalogWidgets.length + '</span><br>Layouts<span class="bd-outcome-right">' + portalCatalogLayouts.length + '/' + serverCatalogLayouts.length + '</span>');
                    });
                });
            });
        });
    };

	/**
	* this function creates a new server catalog list with items that only match
	* the portalcatalog list.
	*/
	var removeExisting = function(serverCatalog, portalCatalog, portalName) {
		var newServerCatalog = [];
		for ( var i = 0; i < serverCatalog.length; i++) {
			var itemFound = false;
			var serverCatalogItem = serverCatalog[i];

			// replaces parameters in the strings like %(contextRoot)
		    if (bd.oPortalClient != null && bd.oPortalClient.params != null && serverCatalogItem.properties['thumbnailUrl'] != null) {
		    	serverCatalogItem.properties['thumbnailUrl'].value = be.utils.replaceParams(serverCatalogItem.properties['thumbnailUrl'].value, bd.oPortalClient.params);
		    	serverCatalogItem.properties.thumbnailUrl = serverCatalogItem.properties['thumbnailUrl'];
		    }
		    for ( var p = 0; p < portalCatalog.length; p++) {
		    	var portalCatalogItem = portalCatalog[p];
		    	var serverCatalogItemName = serverCatalogItem.name + "_" + portalName;
		    	var portalCatalogItemName = portalCatalogItem.extendedItemName+"_"+portalCatalogItem.contextItemName;
		    	if (serverCatalogItemName == portalCatalogItemName) {
		    		itemFound = true;
		    		break;
		    	}
		    }
		    if (itemFound) {
		    	serverCatalogItem.checked = "checked=\"checked\"";
		    	serverCatalogItem.isChecked = true;
		    	newServerCatalog.push(serverCatalogItem);
		    }
		}
		return newServerCatalog;
	};

	/**
	* this function adds a "checked" attribute all items in the server catalog
	* that match the portal catalog list
	*/
	var addCheckedAttribute = function(serverCatalog, portalCatalog, portalName) {
		for ( var i = 0; i < serverCatalog.length; i++) {
			var serverCatalogItem = serverCatalog[i];
			serverCatalogItem.checked = "";
			serverCatalogItem.isChecked = false;

			// replaces parameters in the strings like %(contextRoot)
			if (bd.oPortalClient != null && bd.oPortalClient.params != null && serverCatalogItem.properties['thumbnailUrl'] != null) {
				serverCatalogItem.properties['thumbnailUrl'].value = be.utils.replaceParams(serverCatalogItem.properties['thumbnailUrl'].value, bd.oPortalClient.params);
				serverCatalogItem.properties.thumbnailUrl = serverCatalogItem.properties['thumbnailUrl'];
			}

			for ( var p = 0; p < portalCatalog.length; p++) {
				var portalCatalogItem = portalCatalog[p];
	            var serverCatalogItemName = serverCatalogItem.name + "_" + portalName;
	            var portalCatalogItemName = portalCatalogItem.extendedItemName+"_"+portalCatalogItem.contextItemName;
	            if (serverCatalogItemName == portalCatalogItemName) {
	               serverCatalogItem.checked = "checked=\"checked\"";
	               serverCatalogItem.isChecked = true;
	               break;
	            }
			}
		}
		return serverCatalog;
	};

	/**
	* get all available items, "addUrl" used for filtering
	*/

	var getServerCatalogItems = function(itemType, addUrl, callback) {
		addUrl = addUrl || '';
		var globalServerCatalogUrl = bd.contextRoot + "/catalog.xml?s=lastModifiedTimestamp(dsc)&f=type(eq)" + itemType;
		globalServerCatalogUrl = globalServerCatalogUrl + "&ps=" + bd.uiEditingOptions.catalog.itemsPerRequest;
		be.utils.ajax({
			url: globalServerCatalogUrl + addUrl,
			cache: false,
			dataType: "xml",
			success: function(serverCatalogXmlDoc){
        		var serverCatalogJsonData = bd.xmlToJson({xml: serverCatalogXmlDoc});
        		var itemPath = itemType.toLowerCase();
        		var serverCatalogItems = serverCatalogJsonData.catalog? serverCatalogJsonData.catalog[itemPath]:[];
        		if (serverCatalogJsonData.catalog == null) {
        			serverCatalogItems = [];
        		} else if (serverCatalogItems && serverCatalogItems.length == null) {
        			serverCatalogItems = [ serverCatalogItems ];
        		}
        		if(callback) callback(serverCatalogItems? serverCatalogItems : []);
			}
		});

	};

	/**
	*   get items assigned to catalog
	*/

	var getPortalCatalogItems = function(itemType, portalName, callback) {

		portalName = portalName || bd.selectedPortalName;
		var portalCatalogUrl = bd.contextRoot + "/portals/" + portalName + "/catalog.xml?s=lastModifiedTimestamp(dsc)&f=type(eq)" + itemType + "&ps=" + bd.uiEditingOptions.catalog.itemsPerRequest;
		be.utils.ajax({
			url: portalCatalogUrl,
			cache: false,
			dataType: "xml",
			success: function(portalCatalogXmlDoc){
        		var portalCatalogJsonData = bd.xmlToJson({
        			xml: portalCatalogXmlDoc
        		});
        		var itemPath = itemType.toLowerCase();
        		var portalCatalogItems = portalCatalogJsonData.catalog[itemPath] || [];
        		if (portalCatalogJsonData.catalog == null || portalCatalogItems == null)
        			portalCatalogItems = [];
        		else if (portalCatalogItems.length == null)
        			portalCatalogItems = [ portalCatalogItems ];
        		if(callback)callback(portalCatalogItems);
			}
		});

	};

	var getTags = function(itemType, portalName, callback){
    	var tUrl = bd.contextRoot+'/tags/catalog?f=type(eq)'+itemType;
    	var tags = null;
    	be.utils.ajax({
    		url: tUrl,
    		dataType:"xml",
    		success:function(xmlData){
    			tags = bd.xmlToJson({xml: xmlData}).tagArray;
    			if(callback)callback(tags);
    		}
    	});
    	//return tags;
	};
	// end of private functions

	/**
    * this function renders the UI
    */

	var renderUI = function(oGadgetBody, sGadgetUrl, itemType, displayPortalCatalogItemsOnly, activeTab) {
		//console.log(1);
		var validation = "none", // set validation message to display:none
			addUrl = '',
			filters = null;

		oWidgetBody = jQuery(oGadgetBody);

		// on first load only, render outer page container
		if (itemType == null) {
			itemType = 'WIDGET';
			addUrl = '';

			templateData = {
				portalTitle: bd.selectedPortalTitle,
				contextRoot: bd.contextRoot
			};

			// render outer container
			var htmlContent = be.utils.processHTMLTemplate("catalogMgmntView", templateData);
			oWidgetBody.html(htmlContent);

			// define tab click
			jQuery('.bd-tabLabels', oGadgetBody).undelegate('.bd-tabLabel', 'click').delegate('.bd-tabLabel', 'click', function() {
				var currentTab = jQuery(this).attr('for');
				currentTab = jQuery(currentTab, oWidgetBody);
				var itemType = getCurrentTabInfo(currentTab).itemType;
				renderUI(oGadgetBody, sGadgetUrl, itemType, displayPortalCatalogItemsOnly, currentTab);
			});
		}

		// Render Page Content
		var inputID = "#catalogFilterInput-" + itemType;
		if (jQuery(inputID, oWidgetBody).length > 0){
			if(jQuery(inputID, oWidgetBody).data("tokenInputObject") === undefined){
				return;
			}
			filters = jQuery(inputID, oWidgetBody).tokenInput("get");
		}
		// get current tab values
		var tabVars = getCurrentTabInfo(activeTab),
			activeTab = tabVars.activeTab,
			itemType = tabVars.itemType;

		if (filters != null && filters.length != null && filters.length > 0) {
			var subURL = "";
			for ( var i = 0; filters != null && filters.length != null && i < filters.length; i++) {
				validation = /^[^\r\f\n\t\"\'\\\/&%$#@!~]*$/.test(filters[i].name);
				if(validation == true){
					subURL += "&f=tag.name(like)" + filters[i].name.substring(0, 10);
					validation = "none";
				} else {
					validation = "block";
				}
			}
			addUrl += subURL;
		}

		jQuery(".token-input-dropdown", oWidgetBody).remove();

		// get catalog and portal items from server
		getServerCatalogItems(itemType, addUrl, function(serverCatalogItems){
    		getPortalCatalogItems(itemType, bd.selectedPortalName, function(portalCatalogItems){
        		getTags(itemType, bd.selectedPortalName, function(getPortalTags){
        		    var isAddToken = true;
                     var tagListerner = function(activeTab, itemType){
                            jQuery(activeTab).undelegate('.bd-catalogMnt-tag', 'click').delegate('.bd-catalogMnt-tag', 'click', function(e){
                                var tag = jQuery(this).text() == "All Tags"? null : jQuery(this).text();
                                validation = /^[^\r\f\n\t\"\'\\\/&%$#@!~]*$/.test(tag);
                                if(validation == true){
                                    var inputID = "#catalogFilterInput-" + itemType,
										inputField = jQuery(inputID, oWidgetBody);
                                    if (tag != null){
                                        isAddToken = false;
                                        inputField.data("tokenInputObject").clear();
                                        isAddToken = true;
                                        inputField.tokenInput("add", {id: tag, name: tag});
                                    }else{
                                        inputField.data("tokenInputObject").clear();
                                    }
                                }
                        });
                    };
                    // initialize search box
            		be.utils.domReady(inputID, function(catalogFilterInput) {
                        var updateCatalogView = function() {
            	        // fixing event queue bug in FF (PMFIVE-519)
                            if(isAddToken){
                				setTimeout(function() {
                					jQuery(".token-input-dropdown", oWidgetBody).remove();
                					toggleSelection(jQuery(".displayPortalCatalogWidgetToggle")[0], itemType);
                				}, 10);
            				}
            			};

            			var prePopulate = [];
            			var validation;
            			for ( var i = 0; filters != null && filters.length != null && i < filters.length; i++){
            				validation = /^[^\r\f\n\t\"\'\\\/&%$#@!~]*$/.test(filters[i].name);
            				if(validation == true){
            					prePopulate.push({
            						id: filters[i].name,
            						name: filters[i].name
            					});
            				}
            			}
            			var options = {
            	            // theme: "bb",
            	            minChars: 2,
            	            tokenLimit: 5,
            	            tokenValue: 'name',
            	            prePopulate: prePopulate,
            	            preventDuplicates: true,
            	            onAdd: updateCatalogView,
            	            hintText: "Search By Tag",
            	            onDelete: updateCatalogView,
            				onReady: function(){
            					bd.initTagsHightlight(catalogFilterInput);
            				},
            	            classes: {
            				   tokenList: 'token-input-list token-input-list-filter',
            	               dropdown: 'token-input-dropdown token-input-dropdown-filter'
            	            }
            			};
            			catalogFilterInput.tokenInput(bd.contextRoot + "/json/tags.jsp?portalName=" + bd.selectedPortalName + "&", options);
            		});


            		// display all items
            		if (!displayPortalCatalogItemsOnly){
            			serverCatalogItems = addCheckedAttribute(serverCatalogItems, portalCatalogItems, bd.selectedPortalName);
                    } else {
                    // display only selected items
                       serverCatalogItems = removeExisting(serverCatalogItems, portalCatalogItems, bd.selectedPortalName);
                    }

            		// create items array
                    for (var i=0, n=serverCatalogItems.length; i<n; i++) {
                    	var itemsObj = serverCatalogItems[i];
                    	if (itemsObj.tagArray != undefined && itemsObj.tagArray.length > 0) {
                    		var keyedArray = [];
                    		for (var j=0, len = itemsObj.tagArray.length; j < len; j++) {
                    			if (itemsObj.tagArray[j] != null && jQuery.type(itemsObj.tagArray[j]) == "string")
                    				keyedArray.push(itemsObj.tagArray[j]);
                    		}
            	      		if (keyedArray.length > 0) {
            	      			keyedArray.sort();
            	      			itemsObj.tags = be.utils.truncateText(keyedArray.join(", "), 60);
            	      			itemsObj.hasTags = true;
            	      		} else {
            	      			itemsObj.hasTags = false;
            	      		}
                    	}
                    }

                    // render page data
                    var itemsName = itemType.toLowerCase();
                    var itemsData = {
                    	isLength: function() {
                    		return serverCatalogItems.length;
                    	},
                    	itemType : itemType,
                    	itemsName : itemsName,
                    	error: validation,
                    	serverCatalogItems: serverCatalogItems,
                    	tagArray: getPortalTags,
                    	showOnlySelected: (displayPortalCatalogItemsOnly == true ? "checked=checked" : "")
                    }

                    var itemsContent = be.utils.processHTMLTemplate("catalog/itemsView", itemsData);

                    activeTab.html(itemsContent);

                    bc.component.dropdown({
            			target: '.bd-catalog-actions',
            			label: 'Actions',
            			uid: '9875',
            			staticLabel: true,
            			options: [
            				{name:'Add all '  + itemsName + 's to catalog', href: "#", classname: 'bd-addAllWidgets', onclick: 'return bd.CatalogManagement.registerItemsToCatalogAction("'+ itemType + '")'},
            				{name:'Remove all '  + itemsName + 's from catalog', href: "#", classname: 'bd-removeAllWidgets', onclick: 'return bd.CatalogManagement.removeItemsFromCatalogAction("'+ itemType + '")'}
            			]
            		});
                    tagListerner(activeTab, itemType);
                });
            });
        });
	};

	var registerItemToCatalog = function(itemType, portalName, ItemId, isInheritance) {

		var renderReturnData = function(itemType, data, textStatus, xhr) {
			var itemPathName = itemType.toLowerCase();
			var itemPath = "/catalog/" + itemPathName;
			var itemObjs = be.utils.xpathNodes(data, itemPath);
			getPortalCatalogItems(itemType, '', function(portalCatalogItems){
    			for ( var i = 0; i < itemObjs.length; i++) {
    				var itemName = be.utils.xpathSingleNode(itemObjs[i], "name/text()");
    	            if (ItemId == itemName) {
    	            	postItem(itemObjs[i], itemPathName, portalName, portalCatalogItems, isInheritance);
    	            	break;
    	            }
    			}
    			bc.component.notify({
    				uid: '3456',
    				icon: 'checkbox', // checkbox, attention, error, loading
    				message: 'The catalog for this portal has been updated.'
    			});
    			//bd.operationCompleteMsg("The catalog for this portal has been updated.");
			});
		};

		var sUrl = bd.contextRoot + "/catalog.xml?ps=" + bd.uiEditingOptions.catalog.itemsPerRequest + "&s=lastModifiedTimestamp(dsc)&f=type(eq)" + itemType;

		be.utils.ajax({
			url: sUrl,
			dataType: "xml",
			success: function(responseHTML){
				renderReturnData(itemType, responseHTML);
			}
		});
	};

	var registerItemsToCatalogAction = function(itemType) {
		var itemPathName = itemType.toLowerCase();
		registerToCatalog(itemType, bd.selectedPortalName, true, false, function(){
    		toggleSelection(jQuery(".displayPortalCatalogWidgetToggle")[0], itemType);
    		bc.component.notify({
    			uid: '3457',
    			icon: 'checkbox', // checkbox, attention, error, loading
    			message: 'The catalog for this portal has been updated.'
    		});
		});
		//bd.operationCompleteMsg("The catalog for this portal has been updated.");
	};

	var removeItemFromPortalCatalog = function(itemId, callback) {
		be.utils.ajax({
			url: bd.contextRoot + "/portals/" + bd.selectedPortalName + "/catalog/" + itemId,
			type: "DELETE",
			success: function() {
				bc.component.notify({
					uid: '3458',
					icon: 'checkbox', // checkbox, attention, error, loading
					message: 'The catalog for this portal has been updated.'
				});
				if(callback)callback();
			},
			error:function (xhr, ajaxOptions, thrownError){
				bc.component.notify({
					uid: '3426',
					icon: 'error', // checkbox, attention, error, loading
					message: 'Error: The widget does not exist on the portal server'
				});
				if(callback)callback();
				//bd.operationCompleteMsg("Error: The widget does not exist on the portal server", "errorIcon");
			}
		});
	};

	var removeItemsFromCatalogAction = function(itemType) {

		var renderReturnData = function(itemType, data, textStatus, xhr) {
			var itemPathName = itemType.toLowerCase();
			var itemObjs = be.utils.xpathNodes(data, "/catalog/"+itemPathName+"/name/text()");
			for ( var i = 0; i < itemObjs.length; i++){
			    if(i == itemObjs.length-1){
                    removeItemFromPortalCatalog(itemObjs[i].nodeValue, function(){
                        toggleSelection(jQuery(".displayPortalCatalogWidgetToggle")[0], itemType);
                        bc.component.notify({
                            uid: '4465',
                            icon: 'checkbox', // checkbox, attention, error, loading
                            message: 'The catalog for this portal has been updated.'
                        });
                    });
                }else{
                    removeItemFromPortalCatalog(itemObjs[i].nodeValue);
                }
			}
		};
		var sUrl = "/portals/" + bd.selectedPortalName + "/catalog.xml?ps=" + bd.uiEditingOptions.catalog.itemsPerRequest + "&s=lastModifiedTimestamp(dsc)&f=type(eq)" + itemType;
		be.utils.ajax({
			url: bd.contextRoot + sUrl,
			dataType: "xml",
			success: function(responseHTML){
				renderReturnData(itemType, responseHTML);
			}
		});
	};

	var toggleSelection = function(selectObj, itemType) {
		var activeTab = getCurrentTabInfo().activeTab;
		if (selectObj.checked) {
			renderUI(oWidgetBody, null, itemType, true, activeTab);
		} else {
			renderUI(oWidgetBody, null, itemType, null, activeTab);
		}
	};

	var toggleRegistration = function(itemType, element, selectObj) {
		var portalName = bd.selectedPortalName;
		var activeTab = getCurrentTabInfo().activeTab;
		var ItemId = selectObj;
		var checkBoxId = "#select" + selectObj;
		var itemTypeName = itemType === 'WIDGET' ? 'widget' : 'layout';
		if (jQuery(checkBoxId).is(':checked')) {
			be.utils.confirm({
				title: 'REMOVE ' + itemTypeName.toUpperCase() + ' FROM THIS PORTAL',
				message: 'Are you sure you want to remove this ' + itemTypeName + ' from this portal?<br/>This will also remove all instances from the portal pages.<br/>This cannot be undone.',
				okBtnText: 'Continue',
				cancelBtnText: 'Cancel',
				yesCallback: function(){
					getPortalCatalogItems(itemType, portalName, function(portalCatalogItems){
    					portalCatalogItems.forEach(function(oWidget) {
    						if (oWidget.extendedItemName == ItemId) removeItemFromPortalCatalog(oWidget.name, function(){
            					jQuery(element).addClass("bd-catalogMnt-widget-selected");
            					jQuery(checkBoxId).removeAttr("checked");
            					toggleSelection(jQuery(".displayPortalCatalogWidgetToggle", activeTab)[0], itemType);
    						});
    					});
					});
				},
				noCallback: function(){}
			});
//find widget that is extended from WidgetId and remove it.
//TODO: right now it makes an extra request. We should store portal catalog somewhere to access it without server calls.

		} else {
			be.utils.confirm({
				title: 'INSTALL ' + itemTypeName.toUpperCase() + ' TO THIS PORTAL',
				message: 'Are you sure you want to make this ' + itemTypeName + ' available to this portal?<br/>Note that if you want to publish, you have to install the item on those environments as well.<br/><br/>You can find the ' + itemTypeName + ' in the ' + itemTypeName + ' catalog (in Pages).',
				okBtnText: 'Continue',
				cancelBtnText: 'Cancel',
				yesCallback: function(){
					registerItemToCatalog(itemType, portalName, ItemId, true);
					//registerItemToCatalog(itemType, portalName, ItemId);
					jQuery(element).addClass("bd-catalogMnt-widget-selected");
					jQuery(checkBoxId).attr("checked", "checked");
				},
				noCallback: function(){}
			});
		}
	};

	var checkRegistration = function(selectObj) {
		if (selectObj.checked) {
			selectObj.checked = false;
		} else {
			selectObj.checked = true;
		}
	};

	var postItem = function(item, itemType, portalName, portalCatalogItems, isInheritance, callback) {
		try {
			var currentItemName = be.utils.xpathSingleNode(item, "name/text()");
			var newItemName = currentItemName;

			if (!isItemExist(newItemName, portalCatalogItems)) {

				// This is manually copying properties from the server catalog item to the portal catalog item instead of inheritance.
				if(isInheritance){
					var itemPropertiesText = '';
				}else{
					var itemProperties = be.utils.xpathNodes(item, "properties")[0];
					var itemPropertiesText = be.utils.xml2text(itemProperties);
				}
                var currentItemTags = be.utils.xpathNodes(item, "tags")[0];
                var currentItemTagsText = be.utils.xml2text(currentItemTags);

                var xmlContent = "<catalog><" + itemType + "><name>" + newItemName + "</name>" +
	               "<extendedItemName>["+currentItemName+"]</extendedItemName>" + itemPropertiesText +
	               ((currentItemTags == null) ?  "" : currentItemTagsText)
	                + "</" + itemType + "></catalog>";

	            var url = bd.contextRoot + "/portals/" + portalName + "/catalog.xml";
	            be.utils.ajax({
	               url: url,
	               data: xmlContent,
	               success: function(){if(callback != null) callback(currentItemName);},
	               type: "POST",
	               error:function (xhr, ajaxOptions, thrownError){
	                   var message = 'The item already exists on the portal server.';
	                   if(xhr.status == 404) message = currentItemName+" doesn't exist on the server catalog."
                        bc.component.notify({
    						uid: '9901',
    						icon: 'error', // checkbox, attention, error, loading
    						message: message
    					});
					   if(callback)callback(null);
	              	// bd.operationCompleteMsg("The item already exists on the portal server.", "errorIcon");
	               }
	            });
			}else{
			    if(callback != null) callback(null);
			}
		} catch (exception) {
			console.log(exception);
		}
	};

	var isItemExist = function(itemName, portalCatalogItems) {
		var exist = false;
		if (portalCatalogItems != null) {
			for ( var j = 0; j < portalCatalogItems.length; j++) {
				if (itemName == portalCatalogItems[j].name) {
					exist = true;
					break;
				}
			}
		}
		return exist;
	}

	var registerPagesToCatalog = function(portalName, showMsg, isInheritance, callback) {
		registerToCatalog('PAGE', portalName, showMsg, isInheritance, callback);
	};

	var registerToCatalog = function(itemType, portalName, showMsg, isInheritance, callback) {
		var renderReturnData = function(itemType, data, callback, textStatus, xhr) {
			var itemPathName = itemType.toLowerCase();
			var itemObjs = be.utils.xpathNodes(data, "/catalog/" + itemPathName);
			getPortalCatalogItems(itemType, portalName, function(portalCatalogItems){
			    var i = 0, postedItem = [],
			    _loopPostItem = function(itemName){
			        if(itemName)postedItem.push(itemName);
                    i++;
                    if(i < itemObjs.length){
                        postItem(itemObjs[i], itemPathName, portalName, portalCatalogItems, isInheritance, _loopPostItem);
                    }else{
                        if(callback)callback(postedItem);
                        }
			    };
                postItem(itemObjs[i], itemPathName, portalName, portalCatalogItems, isInheritance, _loopPostItem);
			});
		};

		var url = bd.contextRoot + "/catalog.xml?s=lastModifiedTimestamp(dsc)&f=type(eq)" + itemType + "&ps=" + bd.uiEditingOptions.catalog.itemsPerRequest;

		be.utils.ajax({
			url: url,
			dataType: "xml",
			success: function(responseHTML){
				renderReturnData(itemType, responseHTML, callback);
			}
		});
	};


   //public functions
	return {
		renderUI: renderUI,
		renderDashboard: renderDashboard,
//		preview: preview,
		registerItemToCatalog: registerItemToCatalog,
		registerPagesToCatalog:registerPagesToCatalog,
		registerToCatalog: registerToCatalog,
		postItem: postItem,
		toggleSelection: toggleSelection,
		toggleRegistration: toggleRegistration,
		checkRegistration:checkRegistration,
		removeItemsFromCatalogAction: removeItemsFromCatalogAction,
		registerItemsToCatalogAction: registerItemsToCatalogAction,
		removeItemFromPortalCatalog: removeItemFromPortalCatalog
	};

}());
