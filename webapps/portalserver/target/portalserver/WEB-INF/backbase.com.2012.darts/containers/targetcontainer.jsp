<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="b" uri="http://www.backbase.com/taglib" %>
<%@ taglib prefix="esapi" uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<%@page import="com.backbase.portal.foundation.domain.conceptual.Item,
                com.backbase.portal.foundation.domain.model.BaseContainer,
                com.backbase.portal.foundation.domain.comparators.ItemOrderComparator,
                java.util.Collections" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <title>Target Container Contents</title>

	<script src="<%=portalContextRoot%>/static/backbase.com.2012.aurora/js/aurora.containers.js" type="text/javascript"></script>
	<script src="<%=portalContextRoot%>/static/backbase.com.2012.darts/js/targetingContainer.js" type="text/javascript"></script>
</head>
<body>

	<%
       BaseContainer item = (BaseContainer) request.getAttribute("item");
       Collections.sort(item.getChildren(), ItemOrderComparator.INSTANCE);
    %>
<div class="bp-container bp-tCont bp-tContFn-main bp-ui-dragRoot" data-pid="<esapi:encodeForHTMLAttribute>${item.name}</esapi:encodeForHTMLAttribute>">
	<div class="bp-tCont-body bp-area bp-tContFn-body">
		<c:forEach items="${item.children}" var="child">
			<b:include src="${child}"/>
		</c:forEach>
	</div>
</div>

</body>
</html>
