<%--
Copyright © 2011 Backbase B.V.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.springframework.security.web.savedrequest.*" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<%String buildVersion = com.backbase.portal.foundation.presentation.util.BuildConfigUtils.getBuildVersion();%>
<!DOCTYPE html>
<html>
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link rel="stylesheet" type="text/css" href="<%=portalContextRoot%>/static/dashboard/build/dashboard-all.min.css?v=<%=buildVersion%>" />
      <link rel="shortcut icon" href="<%=portalContextRoot%>/static/dashboard/media/favicon.ico" />
    </head>
    <body class="dashboard">
        <div class="bd-errorContainer">
            <div style='background: url("../static/dashboard/media/icon_error_sign.png") no-repeat scroll 30px 30px;'>
				<div class="bd-errorMsg" style='text-align: left; padding-left:120px;'>
				    <p class="bd-errorTitle">Server Error - Testing Page</p> 
				    <p class="bd-errorDescription">An unexpected error has occurred with the server.</p>
				    <a href="login.jsp">Return to login page.</a>
				</div>
			</div>

        </div>
    </body>
</html>