<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="esapi" uri="http://www.owasp.org/index.php/Category:OWASP_Enterprise_Security_API" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.springframework.security.web.savedrequest.*" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<%String buildVersion = com.backbase.portal.foundation.presentation.util.BuildConfigUtils.getBuildVersion();%>
<!DOCTYPE html>
<html>
	<head>
        <title>CXP Manager - Login</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" type="text/css" href="<%=portalContextRoot%>/static/dashboard/build/dashboard-all.min.css?v=<%=buildVersion%>" />
        <link rel="shortcut icon" href="<%=portalContextRoot%>/static/dashboard/media/favicon.ico" />
        <script type="text/javascript">
            window.bd = window.bd || {};
            window.bd.contextRoot = '<%=portalContextRoot%>';
        </script>
        <script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery.min.js" ></script>
		<script type="text/javascript" src="<%=portalContextRoot%>/static/ext-lib/jquery-migrate.js" ></script>
        <script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/login/jquery.cookie.js"></script>
        <script type="text/javascript" src="<%=portalContextRoot%>/static/dashboard/js/login/loginDashboard.js"></script>
	</head>
	<body class="dashboard bd-login-dashboard" onload="bd.initLoginPage()">
		<div class="bd-login-dashboard-bg"></div>
		<div class="bd-loginContainer-dashboard">
            <div class="bd-loginContainer-dashboard-bg"></div>
			<div class="bd-loginContainer-dashboard-white-bg"></div>
			<form action="<%=portalContextRoot%>/j_spring_security_check" method="POST" name="f" class="bd-loginForm-dashboard">
			     <div class="bd-login-container">
			         <div class="bd-login-header">
			         </div>

			         <div class="bd-login-body">
			             <div class="bd-login-input-area">
			                 <div class="bd-login-input-username">
				                 <div class="icon-box">
									 <div class="bi bi-pm-user"></div>
								 </div>
			                     <input type="text" name="j_username" autocapitalize="off" id="j_username" placeholder="Username" class="bd-login-input bd-login-username bd-autoTest-login-username" autofocus />
		                     </div>

			                 <div class="bd-login-input-password">
								<div class="icon-box">
									<div class="bi bi-pm-lock"></div>
								</div>
                                 <c:if test="${not empty param.redirect}">
								    <input type="hidden" name="redirect" value="<esapi:encodeForHTMLAttribute><esapi:encodeForURL>${param.redirect}</esapi:encodeForURL></esapi:encodeForHTMLAttribute>"/>
                                 </c:if>
			                     <input type="password" name="j_password" id="j_password"  placeholder="Password" class="bd-login-input bd-login-password bd-autoTest-login-password"/>

			                     <input type="hidden" name="${BBXSRF.parameterName}" value="${BBXSRF.token}" />
		                     </div>

			             </div>
			                 <c:if test="${param.login_error eq 'accessdenied'}">
								 <div class="bd-login-error-msg-area">
			                         <p class="bd-error-message-dashboard bd-login-error">The requested page requires authorization. Please login with a valid Username and Password.</p>
								 </div>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'failure'}">
								 <div class="bd-login-error-msg-area">
			                         <p class="bd-error-message-dashboard bd-login-error">The Username or Password you entered is not correct. Please try again.</p>
								 </div>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'logout'}">
								 <div class="bd-login-error-msg-area">
			                         <p class="bd-error-message-dashboard">You have successfully logged out. Please re-enter your Username and Password to log in.</p>
								 </div>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'timeOut'}">
								 <div class="bd-login-error-msg-area">
			                         <p class="bd-error-message-dashboard">Your session has timed out. Please re-enter your Username and Password to log in.</p>
								 </div>
			                 </c:if>
			                 <c:if test="${param.login_error eq 'invalidDestination'}">
								 <div class="bd-login-error-msg-area">
			                         <p class="bd-error-message-dashboard bd-login-error">This user has no permissions to access this page. Please check the URL and try again.</p>
								 </div>
			                 </c:if>
                         </div>
                         <div class="bd-login-button-area">
                             <button data-cid="bc-btn-primary-login" type="submit"  class="bc-button bc-primary bd-login-button">Log in</button>
                         </div>

                     </div>
			     </div>
			</form>
		</div>
	</body>
</html>
