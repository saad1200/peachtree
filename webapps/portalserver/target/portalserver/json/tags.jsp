<%--
 Copyright © 2011 Backbase B.V.
  --%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.util.regex.Pattern"%>
<%@ page language="java" import="java.util.regex.Matcher"%>
<%@ page language="java" import="com.backbase.portal.foundation.domain.model.*" %>
<%@ page language="java" import="com.backbase.portal.foundation.domain.model.Filter" %>
<%@ page language="java" import="com.backbase.portal.foundation.business.service.*" %>
<%@ page language="java" import="com.backbase.portal.foundation.domain.conceptual.Tag" %>
<%@ page language="java" import="org.springframework.context.*"%>
<%@ page language="java" import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page language="java" import="java.util.List,java.util.Iterator,java.util.ArrayList" %>
<%@ page language="java" import="java.util.regex.Pattern"%>
<%@ page language="java" import="java.util.regex.Matcher"%>
<%@ page import="com.backbase.portal.foundation.commons.utils.ItemUtils" %>
<%@ page import="java.net.URLDecoder" %>
<%@ page import="java.nio.charset.StandardCharsets" %>
<%@ page session="false"%>
<%String portalContextRoot = request.getContextPath();%>
<%


		ApplicationContext context =  WebApplicationContextUtils.getWebApplicationContext(application);
		TagBusinessService tagBusinessService = (TagBusinessService) context.getBean("tagBusinessService");

		String defaultPortalName = "dashboard";

		String portalName = defaultPortalName;
		String queryString = "";
		String typeString = null;

		try {
			portalName = request.getParameter("portalName");
			if ("undefined".equals(portalName)) {
				portalName = "dashboard";
			} else {
                portalName = URLDecoder.decode(portalName, StandardCharsets.UTF_8.toString());
            }
			queryString = request.getParameter("q").replaceAll("\\+","%20");
			if (request.getParameter("type") != null && !request.getParameter("type").equals("")) {
				typeString = request.getParameter("type");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		 //^*()+`=[]{};:",.<>?
		Pattern p = Pattern.compile(ItemUtils.STRING_WHITELIST_PATTERN, Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(queryString);
		boolean b = m.find();

		if (b) {

			//build sort and filter arrays:
			SorterDefinition[] sorterDefinitions = new SorterDefinition[] {
					new SorterDefinition("name", SorterDefinition.SortingParameter.ASC)
			};

			FilterDefinition[] filterDefinitions = new FilterDefinition[] {
					new FieldFilterDefinition("name", FilterDefinition.ComparisonParameter.LIKE, queryString)
			};

			//build filter and sorter options:
			Filter filter = new Filter(filterDefinitions, Integer.valueOf(0), Integer.valueOf(100));
			Sorter sorter = new Sorter(sorterDefinitions);

			if (typeString != null) {
				filter.addFilterDefinition(new FieldFilterDefinition("type", FilterDefinition.ComparisonParameter.EQUALS, typeString));
			}

			List<Tag> tags = tagBusinessService.get(portalName, sorter, filter);

			Iterator iterator = tags.iterator();
			List<String> tagList = new ArrayList<String>();

			while (iterator.hasNext()) {
				Tag tag = (Tag) iterator.next();
				tagList.add(tag.getName());
			}

			//append query string
			if (!tagList.contains(queryString)) {
				tagList.add(queryString);
			}

			//build the json string:
			int count = 0;
			int size = tagList.size();

			StringBuilder sb = new StringBuilder("[");

			for (String tag : tagList) {
				sb.append("{\"id\":\"" + Math.abs(tag.hashCode()) + "\",\"name\":\"" + tag + "\"}");
				count++;
				if (count < size) {
					sb.append(",");
				};
			}

			sb.append("]");

            // Normalize JSON before sending it in response
            pageContext.getOut().write(sb.toString().replace("<", "\u003c"));
		} else {
            pageContext.getOut().write("[]");
		}
%>