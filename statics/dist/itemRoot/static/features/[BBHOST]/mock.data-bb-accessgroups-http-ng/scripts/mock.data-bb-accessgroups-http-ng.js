(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-accessgroups-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-accessgroups-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-accessgroups-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.accessGroupsDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbAccessgroupsHttp = __webpack_require__(3);
	
	var _dataBbAccessgroupsHttp2 = _interopRequireDefault(_dataBbAccessgroupsHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint-disable */
	var accessGroupsDataModuleKey = 'data-bb-accessgroups-http-ng';
	
	var accessGroupsDataKey = exports.accessGroupsDataKey = 'data-bb-accessgroups-http-ng:accessGroupsData';
	
	exports.default = _vendorBbAngular2.default.module(accessGroupsDataModuleKey, []).provider(accessGroupsDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbAccessgroupsHttp2.default)(config)]
	  };
	}]).name;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	/* eslint-disable */
	exports.default = function (conf) {
	  return function (Promise) {
	    var _plugins;
	
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    var state = {
	      "{version}/accessgroups/data": [{
	        "dataAccessGroupId": "00001",
	        "name": "Account group 1",
	        "description": "Simple account group",
	        "legalEntityId": "001",
	        "type": "arrangements",
	        "items": ["ac1"]
	      }, {
	        "dataAccessGroupId": "00002",
	        "name": "Account group 2",
	        "description": "Simple account group",
	        "legalEntityId": "001",
	        "type": "arrangements",
	        "items": ["ac1", "ac2"]
	      }],
	      "{version}/accessgroups/function": [{
	        "functionAccessGroupId": "00001",
	        "name": "Administrator",
	        "description": "Administers the corporate users.",
	        "legalEntityId": "001",
	        "permissions": [{
	          "functionId": "1",
	          "assignedPrivileges": [{
	            "privilege": "execute"
	          }]
	        }]
	      }, {
	        "functionAccessGroupId": "00002",
	        "name": "Payment",
	        "description": "Domestic payments",
	        "legalEntityId": "001",
	        "permissions": [{
	          "functionId": "2",
	          "assignedPrivileges": [{
	            "privilege": "execute"
	          }, {
	            "privilege": "create"
	          }, {
	            "privilege": "read"
	          }, {
	            "privilege": "update"
	          }]
	        }]
	      }],
	      "{version}/accessgroups/config/functions": [{
	        "functionId": "1",
	        "functionCode": "list.accounts",
	        "resource": "Account",
	        "name": "List",
	        "privileges": [{
	          "privilege": "execute"
	        }, {
	          "privilege": "read"
	        }]
	      }, {
	        "functionId": "2",
	        "functionCode": "domestic.payments",
	        "resource": "Payments",
	        "name": "Domestic",
	        "privileges": [{
	          "privilege": "execute"
	        }, {
	          "privilege": "create"
	        }, {
	          "privilege": "read"
	        }, {
	          "privilege": "update"
	        }, {
	          "privilege": "delete"
	        }, {
	          "privilege": "verify"
	        }, {
	          "privilege": "approve"
	        }]
	      }],
	      "{version}/accessgroups/users": [{
	        "id": "1",
	        "serviceAgreementId": "SA",
	        "userId": "0001",
	        "dataAccessGroupsByFunctionAccessGroup": [{
	          "functionAccessGroupId": "00001",
	          "dataAccessGroupIds": ["00001", "00002"]
	        }, {
	          "functionAccessGroupId": "00003",
	          "dataAccessGroupIds": ["00001", "00003"]
	        }]
	      }, {
	        "id": "2",
	        "serviceAgreementId": "SA",
	        "userId": "0002",
	        "dataAccessGroupsByFunctionAccessGroup": [{
	          "functionAccessGroupId": "00001",
	          "dataAccessGroupIds": ["00001", "00002"]
	        }, {
	          "functionAccessGroupId": "00003",
	          "dataAccessGroupIds": ["00001", "00003"]
	        }]
	      }],
	      "{version}/accessgroups/serviceagreements": [{
	        "id": "0001",
	        "name": "Broker deal 1",
	        "description": "Agreement between Backbase and Apple",
	        "creatorLegalEntity": "001",
	        "isMaster": false,
	        "participants": ["LE-0001", "LE-002"],
	        "providers": [{
	          "id": "001",
	          "users": ["001", "002"],
	          "admins": ["001"]
	        }, {
	          "id": "003",
	          "users": ["003", "004"],
	          "admins": ["003"]
	        }],
	        "consumers": [{
	          "id": "001",
	          "dataAccessGroupFunctionAccessGroupPairs": [{
	            "functionAccessGroupId": "FAG1",
	            "dataAccessGroupId": "DAG1"
	          }, {
	            "functionAccessGroupId": "FAG2",
	            "dataAccessGroupId": "DAG2"
	          }],
	          "admins": ["001"]
	        }, {
	          "id": "003",
	          "dataAccessGroupFunctionAccessGroupPairs": [{
	            "functionAccessGroupId": "FAG1",
	            "dataAccessGroupId": "DAG1"
	          }, {
	            "functionAccessGroupId": "FAG2",
	            "dataAccessGroupId": "DAG2"
	          }],
	          "admins": ["003"]
	        }]
	      }, {
	        "id": "0002",
	        "name": "Broker deal 2",
	        "description": "Agreement between Backbase and HP",
	        "creatorLegalEntity": "001",
	        "isMaster": false,
	        "participants": ["LE-0001", "LE-002"],
	        "providers": [{
	          "id": "001",
	          "users": ["001", "002"],
	          "admins": ["001"]
	        }, {
	          "id": "003",
	          "users": ["003", "004"],
	          "admins": ["003"]
	        }],
	        "consumers": [{
	          "id": "001",
	          "dataAccessGroupFunctionAccessGroupPairs": [{
	            "functionAccessGroupId": "FAG1",
	            "dataAccessGroupId": "DAG1"
	          }, {
	            "functionAccessGroupId": "FAG2",
	            "dataAccessGroupId": "DAG2"
	          }],
	          "admins": ["001"]
	        }, {
	          "id": "003",
	          "dataAccessGroupFunctionAccessGroupPairs": [{
	            "functionAccessGroupId": "FAG1",
	            "dataAccessGroupId": "DAG1"
	          }, {
	            "functionAccessGroupId": "FAG2",
	            "dataAccessGroupId": "DAG2"
	          }],
	          "admins": ["003"]
	        }]
	      }],
	      "{version}/accessgroups/users/privileges": [{
	        "privilege": "execute"
	      }, {
	        "privilege": "read"
	      }],
	      "{version}/accessgroups/users/privileges/arrangements": [{
	        "arrangementId": "1",
	        "privileges": [{
	          "privilege": "view"
	        }]
	      }, {
	        "arrangementId": "2",
	        "privileges": [{
	          "privilege": "view"
	        }]
	      }],
	      "{version}/accessgroups/users/permissions/summary": [{
	        "resource": "Contacts",
	        "function": "Contacts",
	        "permissions": {
	          "view": true,
	          "edit": true
	        }
	      }, {
	        "resource": "Payments",
	        "function": "Domestic",
	        "permissions": {
	          "execute": true,
	          "view": true,
	          "approve": true
	        }
	      }, {
	        "resource": "Entitlements",
	        "function": "ManageDAG",
	        "permissions": {
	          "view": true,
	          "edit": true,
	          "create": true,
	          "delete": true
	        }
	      }],
	      "{version}/accessgroups/serviceagreements/admins/me": [{
	        "id": "SA001",
	        "name": "Service Agreement 1",
	        "description": "Service Agreement between Backbase and Endava",
	        "isMaster": false,
	        "roles": ["provider", "consumer"]
	      }, {
	        "id": "SA002",
	        "name": "Service Agreement 2",
	        "description": "Service Agreement 2",
	        "isMaster": false,
	        "roles": ["provider"]
	      }, {
	        "id": "SA003",
	        "name": "Master Service Agreement",
	        "description": "Master Service Agreement",
	        "isMaster": true,
	        "roles": []
	      }],
	      "{version}/accessgroups/serviceagreements/{id}/users": [{
	        "id": "001",
	        "externalId": "U0000011",
	        "legalEntityId": "012",
	        "firstName": "David"
	      }, {
	        "id": "002",
	        "externalId": "U0000012",
	        "legalEntityId": "012",
	        "firstName": "Paul"
	      }, {
	        "id": "003",
	        "externalId": "U0000013",
	        "legalEntityId": "012",
	        "firstName": "Wayne"
	      }],
	      "{version}/accessgroups/serviceagreements/{id}/functions": [{
	        "functionAccessGroupId": "00001",
	        "name": "Administrator",
	        "description": "Administers the corporate users.",
	        "legalEntityId": "001",
	        "permissions": [{
	          "functionId": "1",
	          "assignedPrivileges": [{
	            "privilege": "execute"
	          }]
	        }]
	      }, {
	        "functionAccessGroupId": "00002",
	        "name": "Payment",
	        "description": "Domestic payments",
	        "legalEntityId": "001",
	        "permissions": [{
	          "functionId": "2",
	          "assignedPrivileges": [{
	            "privilege": "execute"
	          }, {
	            "privilege": "create"
	          }, {
	            "privilege": "read"
	          }, {
	            "privilege": "update"
	          }]
	        }]
	      }],
	      "{version}/accessgroups/serviceagreements/{id}/functions/{functionAccessGroupId}/data": [{
	        "dataAccessGroupId": "00001",
	        "name": "Account group 1",
	        "description": "Simple account group",
	        "legalEntityId": "001",
	        "type": "arrangements",
	        "items": ["ac1"]
	      }, {
	        "dataAccessGroupId": "00002",
	        "name": "Account group 2",
	        "description": "Simple account group",
	        "legalEntityId": "001",
	        "type": "arrangements",
	        "items": ["ac1", "ac2"]
	      }]
	    };
	
	    var responses = {
	
	      getAccessgroupsData: [{ "status": 200, "data": [{ "dataAccessGroupId": "00001", "name": "Account group 1", "description": "Simple account group", "legalEntityId": "001", "type": "arrangements", "items": ["ac1"] }, { "dataAccessGroupId": "00002", "name": "Account group 2", "description": "Simple account group", "legalEntityId": "001", "type": "arrangements", "items": ["ac1", "ac2"] }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      postAccessgroupsDataRecord: [{ "status": 201, "data": { "id": "001" } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsFunction: [{ "status": 200, "data": [{ "functionAccessGroupId": "00001", "name": "Administrator", "description": "Administers the corporate users.", "legalEntityId": "001", "permissions": [{ "functionId": "1", "assignedPrivileges": [{ "privilege": "execute" }] }] }, { "functionAccessGroupId": "00002", "name": "Payment", "description": "Domestic payments", "legalEntityId": "001", "permissions": [{ "functionId": "2", "assignedPrivileges": [{ "privilege": "execute" }, { "privilege": "create" }, { "privilege": "read" }, { "privilege": "update" }] }] }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      postAccessgroupsFunctionRecord: [{ "status": 201, "data": { "id": "001" } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsConfigFunctions: [{ "status": 200, "data": [{ "functionId": "1", "functionCode": "list.accounts", "resource": "Account", "name": "List", "privileges": [{ "privilege": "execute" }, { "privilege": "read" }] }, { "functionId": "2", "functionCode": "domestic.payments", "resource": "Payments", "name": "Domestic", "privileges": [{ "privilege": "execute" }, { "privilege": "create" }, { "privilege": "read" }, { "privilege": "update" }, { "privilege": "delete" }, { "privilege": "verify" }, { "privilege": "approve" }] }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      postAccessgroupsUsersRecord: [{ "status": 201, "data": { "id": "001" } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsUsers: [{ "status": 200, "data": [{ "id": "1", "serviceAgreementId": "SA", "userId": "0001", "dataAccessGroupsByFunctionAccessGroup": [{ "functionAccessGroupId": "00001", "dataAccessGroupIds": ["00001", "00002"] }, { "functionAccessGroupId": "00003", "dataAccessGroupIds": ["00001", "00003"] }] }, { "id": "2", "serviceAgreementId": "SA", "userId": "0002", "dataAccessGroupsByFunctionAccessGroup": [{ "functionAccessGroupId": "00001", "dataAccessGroupIds": ["00001", "00002"] }, { "functionAccessGroupId": "00003", "dataAccessGroupIds": ["00001", "00003"] }] }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      postAccessgroupsServiceagreementsRecord: [{ "status": 201, "data": { "id": "001" } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreements: [{ "status": 200, "data": [{ "id": "0001", "name": "Broker deal 1", "description": "Agreement between Backbase and Apple", "creatorLegalEntity": "001", "isMaster": false, "participants": ["LE-0001", "LE-002"], "providers": [{ "id": "001", "users": ["001", "002"], "admins": ["001"] }, { "id": "003", "users": ["003", "004"], "admins": ["003"] }], "consumers": [{ "id": "001", "dataAccessGroupFunctionAccessGroupPairs": [{ "functionAccessGroupId": "FAG1", "dataAccessGroupId": "DAG1" }, { "functionAccessGroupId": "FAG2", "dataAccessGroupId": "DAG2" }], "admins": ["001"] }, { "id": "003", "dataAccessGroupFunctionAccessGroupPairs": [{ "functionAccessGroupId": "FAG1", "dataAccessGroupId": "DAG1" }, { "functionAccessGroupId": "FAG2", "dataAccessGroupId": "DAG2" }], "admins": ["003"] }] }, { "id": "0002", "name": "Broker deal 2", "description": "Agreement between Backbase and HP", "creatorLegalEntity": "001", "isMaster": false, "participants": ["LE-0001", "LE-002"], "providers": [{ "id": "001", "users": ["001", "002"], "admins": ["001"] }, { "id": "003", "users": ["003", "004"], "admins": ["003"] }], "consumers": [{ "id": "001", "dataAccessGroupFunctionAccessGroupPairs": [{ "functionAccessGroupId": "FAG1", "dataAccessGroupId": "DAG1" }, { "functionAccessGroupId": "FAG2", "dataAccessGroupId": "DAG2" }], "admins": ["001"] }, { "id": "003", "dataAccessGroupFunctionAccessGroupPairs": [{ "functionAccessGroupId": "FAG1", "dataAccessGroupId": "DAG1" }, { "functionAccessGroupId": "FAG2", "dataAccessGroupId": "DAG2" }], "admins": ["003"] }] }] }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsDataRecord: [{ "status": 200, "data": { "dataAccessGroupId": "00001", "name": "Account group 1", "description": "Simple account group", "legalEntityId": "001", "type": "arrangements", "items": ["000001", "000002"] } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      putAccessgroupsDataRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      deleteAccessgroupsDataRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsFunctionRecord: [{ "status": 200, "data": { "functionAccessGroupId": "00001", "name": "Inputer", "description": "Some description for inputer", "legalEntityId": "001", "permissions": [{ "functionId": "1", "assignedPrivileges": [{ "privilege": "view" }, { "privilege": "execute" }] }, { "functionId": "2", "assignedPrivileges": [{ "privilege": "view", "limits": [{ "limitType": "daily", "amount": 500 }, { "limitType": "upper", "amount": 200000 }, { "limitType": "weekly", "amount": 20000 }] }] }] } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      putAccessgroupsFunctionRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      deleteAccessgroupsFunctionRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsUsersRecord: [{ "status": 200, "data": { "id": "2", "serviceAgreementId": "SA", "userId": "0001", "dataAccessGroupsByFunctionAccessGroup": [{ "functionAccessGroupId": "00001", "dataAccessGroupIds": ["00001", "00002"] }, { "functionAccessGroupId": "00003", "dataAccessGroupIds": ["00001", "00003"] }] } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsUsersPrivileges: [{ "status": 200, "data": [{ "privilege": "execute" }, { "privilege": "read" }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsUsersPermissions: [{ "status": 200, "data": null }, { "status": 403, "data": null }],
	
	      postAccessgroupsUsersFunctionRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      postAccessgroupsUsersDataRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsUsersPrivilegesArrangements: [{ "status": 200, "data": [{ "arrangementId": "1", "privileges": [{ "privilege": "view" }] }, { "arrangementId": "2", "privileges": [{ "privilege": "view" }] }] }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 404, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsUsersPermissionsSummary: [{ "status": 200, "data": [{ "resource": "Contacts", "function": "Contacts", "permissions": { "view": true, "edit": true } }, { "resource": "Payments", "function": "Domestic", "permissions": { "execute": true, "view": true, "approve": true } }, { "resource": "Entitlements", "function": "ManageDAG", "permissions": { "view": true, "edit": true, "create": true, "delete": true } }] }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreementsRecord: [{ "status": 200, "data": { "id": "0001", "name": "Broker deal 1", "description": "Agreement between Backbase and Apple", "creatorLegalEntity": "001", "isMaster": false, "participants": ["LE-0001", "LE-002"], "providers": [{ "id": "001", "users": ["001", "002"], "admins": ["001"] }, { "id": "003", "users": ["003", "004"], "admins": ["003"] }], "consumers": [{ "id": "001", "dataAccessGroupFunctionAccessGroupPairs": [{ "functionAccessGroupId": "FAG1", "dataAccessGroupId": "DAG1" }, { "functionAccessGroupId": "FAG2", "dataAccessGroupId": "DAG2" }], "admins": ["001"] }, { "id": "003", "dataAccessGroupFunctionAccessGroupPairs": [{ "functionAccessGroupId": "FAG1", "dataAccessGroupId": "DAG1" }, { "functionAccessGroupId": "FAG2", "dataAccessGroupId": "DAG2" }], "admins": ["003"] }] } }, { "status": 404, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreementsRoles: [{ "status": 200, "data": { "roles": ["provider", "consumer"] } }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreementsAdminsMe: [{ "status": 200, "data": [{ "id": "SA001", "name": "Service Agreement 1", "description": "Service Agreement between Backbase and Endava", "isMaster": false, "roles": ["provider", "consumer"] }, { "id": "SA002", "name": "Service Agreement 2", "description": "Service Agreement 2", "isMaster": false, "roles": ["provider"] }, { "id": "SA003", "name": "Master Service Agreement", "description": "Master Service Agreement", "isMaster": true, "roles": [] }] }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      putAccessgroupsServiceagreementsAdminsRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreementsUsers: [{ "status": 200, "data": [{ "id": "001", "externalId": "U0000011", "legalEntityId": "012", "firstName": "David" }, { "id": "002", "externalId": "U0000012", "legalEntityId": "012", "firstName": "Paul" }, { "id": "003", "externalId": "U0000013", "legalEntityId": "012", "firstName": "Wayne" }] }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      putAccessgroupsServiceagreementsUsersRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreementsFunctions: [{ "status": 200, "data": [{ "functionAccessGroupId": "00001", "name": "Administrator", "description": "Administers the corporate users.", "legalEntityId": "001", "permissions": [{ "functionId": "1", "assignedPrivileges": [{ "privilege": "execute" }] }] }, { "functionAccessGroupId": "00002", "name": "Payment", "description": "Domestic payments", "legalEntityId": "001", "permissions": [{ "functionId": "2", "assignedPrivileges": [{ "privilege": "execute" }, { "privilege": "create" }, { "privilege": "read" }, { "privilege": "update" }] }] }] }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      deleteAccessgroupsServiceagreementsUsersRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 500, "data": null }],
	
	      putAccessgroupsServiceagreementsConsumersFunctionsRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      deleteAccessgroupsServiceagreementsConsumersFunctionsRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      putAccessgroupsServiceagreementsConsumersFunctionsDataRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getAccessgroupsServiceagreementsFunctionsData: [{ "status": 200, "data": [{ "dataAccessGroupId": "00001", "name": "Account group 1", "description": "Simple account group", "legalEntityId": "001", "type": "arrangements", "items": ["ac1"] }, { "dataAccessGroupId": "00002", "name": "Account group 2", "description": "Simple account group", "legalEntityId": "001", "type": "arrangements", "items": ["ac1", "ac2"] }] }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 500, "data": null }]
	
	    };
	
	    var DEFAULT_MOCK = {
	      data: {},
	      status: 200,
	      headers: function headers(header) {
	        return header === 'content-type' && this.data ? 'application/json' : null;
	      },
	      config: {},
	      statusText: 'OK'
	    };
	
	    var getResponse = function getResponse(method, status) {
	      var response = (responses[method] || []).find(function (response) {
	        return response.status === status;
	      });
	      return Object.assign({}, DEFAULT_MOCK, response);
	    };
	
	    var PLUGINS_ALL = '__all__';
	
	    var plugins = (_plugins = {}, _defineProperty(_plugins, PLUGINS_ALL, []), _defineProperty(_plugins, 'getAccessgroupsData', []), _defineProperty(_plugins, 'postAccessgroupsDataRecord', []), _defineProperty(_plugins, 'getAccessgroupsFunction', []), _defineProperty(_plugins, 'postAccessgroupsFunctionRecord', []), _defineProperty(_plugins, 'getAccessgroupsConfigFunctions', []), _defineProperty(_plugins, 'postAccessgroupsUsersRecord', []), _defineProperty(_plugins, 'getAccessgroupsUsers', []), _defineProperty(_plugins, 'postAccessgroupsServiceagreementsRecord', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreements', []), _defineProperty(_plugins, 'getAccessgroupsDataRecord', []), _defineProperty(_plugins, 'putAccessgroupsDataRecord', []), _defineProperty(_plugins, 'deleteAccessgroupsDataRecord', []), _defineProperty(_plugins, 'getAccessgroupsFunctionRecord', []), _defineProperty(_plugins, 'putAccessgroupsFunctionRecord', []), _defineProperty(_plugins, 'deleteAccessgroupsFunctionRecord', []), _defineProperty(_plugins, 'getAccessgroupsUsersRecord', []), _defineProperty(_plugins, 'getAccessgroupsUsersPrivileges', []), _defineProperty(_plugins, 'getAccessgroupsUsersPermissions', []), _defineProperty(_plugins, 'postAccessgroupsUsersFunctionRecord', []), _defineProperty(_plugins, 'postAccessgroupsUsersDataRecord', []), _defineProperty(_plugins, 'getAccessgroupsUsersPrivilegesArrangements', []), _defineProperty(_plugins, 'getAccessgroupsUsersPermissionsSummary', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreementsRecord', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreementsRoles', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreementsAdminsMe', []), _defineProperty(_plugins, 'putAccessgroupsServiceagreementsAdminsRecord', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreementsUsers', []), _defineProperty(_plugins, 'putAccessgroupsServiceagreementsUsersRecord', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreementsFunctions', []), _defineProperty(_plugins, 'deleteAccessgroupsServiceagreementsUsersRecord', []), _defineProperty(_plugins, 'putAccessgroupsServiceagreementsConsumersFunctionsRecord', []), _defineProperty(_plugins, 'deleteAccessgroupsServiceagreementsConsumersFunctionsRecord', []), _defineProperty(_plugins, 'putAccessgroupsServiceagreementsConsumersFunctionsDataRecord', []), _defineProperty(_plugins, 'deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord', []), _defineProperty(_plugins, 'getAccessgroupsServiceagreementsFunctionsData', []), _plugins);
	
	    var pluginMocks = function pluginMocks(method, args, uri) {
	      var methodPlugins = plugins[method] || [];
	      var commonPlugins = plugins[PLUGINS_ALL] || [];
	      var allPlugins = methodPlugins.concat(commonPlugins);
	
	      return function (initialResult) {
	        return allPlugins.reduce(function (result, plugin) {
	          return result.then(function (nextResult) {
	            return plugin(nextResult, args, uri, method);
	          });
	        }, Promise.resolve(initialResult));
	      };
	    };
	
	    var handleError = function handleError(method) {
	      return function (error) {
	        // If error object is one of the error responses, assume it returned intentionally from one of the plugins
	        var isIntendedError = error && error.status && error.status >= 400;
	        var response = isIntendedError ? error : getResponse(method, 500);
	
	        console.log(method + ' request rejected because of ', error);
	        return Promise.reject(response);
	      };
	    };
	
	    function getAccessgroupsData(params) {
	      var url = '' + baseUri + version + '/accessgroups/data';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsData', 200)).then(pluginMocks('getAccessgroupsData', [params], '{version}/accessgroups/data')).catch(handleError('getAccessgroupsData'));
	    }
	
	    function postAccessgroupsDataRecord(data) {
	      var url = '' + baseUri + version + '/accessgroups/data';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postAccessgroupsDataRecord', 201)).then(pluginMocks('postAccessgroupsDataRecord', [data], '{version}/accessgroups/data')).catch(handleError('postAccessgroupsDataRecord'));
	    }
	
	    function getAccessgroupsFunction(params) {
	      var url = '' + baseUri + version + '/accessgroups/function';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsFunction', 200)).then(pluginMocks('getAccessgroupsFunction', [params], '{version}/accessgroups/function')).catch(handleError('getAccessgroupsFunction'));
	    }
	
	    function postAccessgroupsFunctionRecord(data) {
	      var url = '' + baseUri + version + '/accessgroups/function';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postAccessgroupsFunctionRecord', 201)).then(pluginMocks('postAccessgroupsFunctionRecord', [data], '{version}/accessgroups/function')).catch(handleError('postAccessgroupsFunctionRecord'));
	    }
	
	    function getAccessgroupsConfigFunctions(params) {
	      var url = '' + baseUri + version + '/accessgroups/config/functions';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsConfigFunctions', 200)).then(pluginMocks('getAccessgroupsConfigFunctions', [params], '{version}/accessgroups/config/functions')).catch(handleError('getAccessgroupsConfigFunctions'));
	    }
	
	    function postAccessgroupsUsersRecord(data) {
	      var url = '' + baseUri + version + '/accessgroups/users';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postAccessgroupsUsersRecord', 201)).then(pluginMocks('postAccessgroupsUsersRecord', [data], '{version}/accessgroups/users')).catch(handleError('postAccessgroupsUsersRecord'));
	    }
	
	    function getAccessgroupsUsers(params) {
	      var url = '' + baseUri + version + '/accessgroups/users';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsUsers', 200)).then(pluginMocks('getAccessgroupsUsers', [params], '{version}/accessgroups/users')).catch(handleError('getAccessgroupsUsers'));
	    }
	
	    function postAccessgroupsServiceagreementsRecord(data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postAccessgroupsServiceagreementsRecord', 201)).then(pluginMocks('postAccessgroupsServiceagreementsRecord', [data], '{version}/accessgroups/serviceagreements')).catch(handleError('postAccessgroupsServiceagreementsRecord'));
	    }
	
	    function getAccessgroupsServiceagreements(params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreements', 200)).then(pluginMocks('getAccessgroupsServiceagreements', [params], '{version}/accessgroups/serviceagreements')).catch(handleError('getAccessgroupsServiceagreements'));
	    }
	
	    function getAccessgroupsDataRecord(id, params) {
	      var url = '' + baseUri + version + '/accessgroups/data/' + id;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsDataRecord', 200)).then(pluginMocks('getAccessgroupsDataRecord', [id, params], '{version}/accessgroups/data/{id}')).catch(handleError('getAccessgroupsDataRecord'));
	    }
	
	    function putAccessgroupsDataRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/data/' + id;
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putAccessgroupsDataRecord', 200)).then(pluginMocks('putAccessgroupsDataRecord', [id, data], '{version}/accessgroups/data/{id}')).catch(handleError('putAccessgroupsDataRecord'));
	    }
	
	    function deleteAccessgroupsDataRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/data/' + id;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('deleteAccessgroupsDataRecord', 200)).then(pluginMocks('deleteAccessgroupsDataRecord', [id, data], '{version}/accessgroups/data/{id}')).catch(handleError('deleteAccessgroupsDataRecord'));
	    }
	
	    function getAccessgroupsFunctionRecord(id, params) {
	      var url = '' + baseUri + version + '/accessgroups/function/' + id;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsFunctionRecord', 200)).then(pluginMocks('getAccessgroupsFunctionRecord', [id, params], '{version}/accessgroups/function/{id}')).catch(handleError('getAccessgroupsFunctionRecord'));
	    }
	
	    function putAccessgroupsFunctionRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/function/' + id;
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putAccessgroupsFunctionRecord', 200)).then(pluginMocks('putAccessgroupsFunctionRecord', [id, data], '{version}/accessgroups/function/{id}')).catch(handleError('putAccessgroupsFunctionRecord'));
	    }
	
	    function deleteAccessgroupsFunctionRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/function/' + id;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('deleteAccessgroupsFunctionRecord', 200)).then(pluginMocks('deleteAccessgroupsFunctionRecord', [id, data], '{version}/accessgroups/function/{id}')).catch(handleError('deleteAccessgroupsFunctionRecord'));
	    }
	
	    function getAccessgroupsUsersRecord(id, params) {
	      var url = '' + baseUri + version + '/accessgroups/users/' + id;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsUsersRecord', 200)).then(pluginMocks('getAccessgroupsUsersRecord', [id, params], '{version}/accessgroups/users/{id}')).catch(handleError('getAccessgroupsUsersRecord'));
	    }
	
	    function getAccessgroupsUsersPrivileges(params) {
	      var url = '' + baseUri + version + '/accessgroups/users/privileges';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsUsersPrivileges', 200)).then(pluginMocks('getAccessgroupsUsersPrivileges', [params], '{version}/accessgroups/users/privileges')).catch(handleError('getAccessgroupsUsersPrivileges'));
	    }
	
	    function getAccessgroupsUsersPermissions(params) {
	      var url = '' + baseUri + version + '/accessgroups/users/permissions';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsUsersPermissions', 200)).then(pluginMocks('getAccessgroupsUsersPermissions', [params], '{version}/accessgroups/users/permissions')).catch(handleError('getAccessgroupsUsersPermissions'));
	    }
	
	    function postAccessgroupsUsersFunctionRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/users/' + id + '/function';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postAccessgroupsUsersFunctionRecord', 200)).then(pluginMocks('postAccessgroupsUsersFunctionRecord', [id, data], '{version}/accessgroups/users/{id}/function')).catch(handleError('postAccessgroupsUsersFunctionRecord'));
	    }
	
	    function postAccessgroupsUsersDataRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/users/' + id + '/data';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postAccessgroupsUsersDataRecord', 200)).then(pluginMocks('postAccessgroupsUsersDataRecord', [id, data], '{version}/accessgroups/users/{id}/data')).catch(handleError('postAccessgroupsUsersDataRecord'));
	    }
	
	    function getAccessgroupsUsersPrivilegesArrangements(params) {
	      var url = '' + baseUri + version + '/accessgroups/users/privileges/arrangements';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsUsersPrivilegesArrangements', 200)).then(pluginMocks('getAccessgroupsUsersPrivilegesArrangements', [params], '{version}/accessgroups/users/privileges/arrangements')).catch(handleError('getAccessgroupsUsersPrivilegesArrangements'));
	    }
	
	    function getAccessgroupsUsersPermissionsSummary(params) {
	      var url = '' + baseUri + version + '/accessgroups/users/permissions/summary';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsUsersPermissionsSummary', 200)).then(pluginMocks('getAccessgroupsUsersPermissionsSummary', [params], '{version}/accessgroups/users/permissions/summary')).catch(handleError('getAccessgroupsUsersPermissionsSummary'));
	    }
	
	    function getAccessgroupsServiceagreementsRecord(serviceAgreementId, params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + serviceAgreementId;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreementsRecord', 200)).then(pluginMocks('getAccessgroupsServiceagreementsRecord', [serviceAgreementId, params], '{version}/accessgroups/serviceagreements/{serviceAgreementId}')).catch(handleError('getAccessgroupsServiceagreementsRecord'));
	    }
	
	    function getAccessgroupsServiceagreementsRoles(params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/roles';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreementsRoles', 200)).then(pluginMocks('getAccessgroupsServiceagreementsRoles', [params], '{version}/accessgroups/serviceagreements/roles')).catch(handleError('getAccessgroupsServiceagreementsRoles'));
	    }
	
	    function getAccessgroupsServiceagreementsAdminsMe(params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/admins/me';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreementsAdminsMe', 200)).then(pluginMocks('getAccessgroupsServiceagreementsAdminsMe', [params], '{version}/accessgroups/serviceagreements/admins/me')).catch(handleError('getAccessgroupsServiceagreementsAdminsMe'));
	    }
	
	    function putAccessgroupsServiceagreementsAdminsRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/admins';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putAccessgroupsServiceagreementsAdminsRecord', 200)).then(pluginMocks('putAccessgroupsServiceagreementsAdminsRecord', [id, data], '{version}/accessgroups/serviceagreements/{id}/admins')).catch(handleError('putAccessgroupsServiceagreementsAdminsRecord'));
	    }
	
	    function getAccessgroupsServiceagreementsUsers(id, params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/users';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreementsUsers', 200)).then(pluginMocks('getAccessgroupsServiceagreementsUsers', [id, params], '{version}/accessgroups/serviceagreements/{id}/users')).catch(handleError('getAccessgroupsServiceagreementsUsers'));
	    }
	
	    function putAccessgroupsServiceagreementsUsersRecord(id, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/users';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putAccessgroupsServiceagreementsUsersRecord', 200)).then(pluginMocks('putAccessgroupsServiceagreementsUsersRecord', [id, data], '{version}/accessgroups/serviceagreements/{id}/users')).catch(handleError('putAccessgroupsServiceagreementsUsersRecord'));
	    }
	
	    function getAccessgroupsServiceagreementsFunctions(id, params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/functions';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreementsFunctions', 200)).then(pluginMocks('getAccessgroupsServiceagreementsFunctions', [id, params], '{version}/accessgroups/serviceagreements/{id}/functions')).catch(handleError('getAccessgroupsServiceagreementsFunctions'));
	    }
	
	    function deleteAccessgroupsServiceagreementsUsersRecord(id, userId, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/users/' + userId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('deleteAccessgroupsServiceagreementsUsersRecord', 200)).then(pluginMocks('deleteAccessgroupsServiceagreementsUsersRecord', [id, userId, data], '{version}/accessgroups/serviceagreements/{id}/users/{userId}')).catch(handleError('deleteAccessgroupsServiceagreementsUsersRecord'));
	    }
	
	    function putAccessgroupsServiceagreementsConsumersFunctionsRecord(id, consumerId, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/consumers/' + consumerId + '/functions';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putAccessgroupsServiceagreementsConsumersFunctionsRecord', 200)).then(pluginMocks('putAccessgroupsServiceagreementsConsumersFunctionsRecord', [id, consumerId, data], '{version}/accessgroups/serviceagreements/{id}/consumers/{consumerId}/functions')).catch(handleError('putAccessgroupsServiceagreementsConsumersFunctionsRecord'));
	    }
	
	    function deleteAccessgroupsServiceagreementsConsumersFunctionsRecord(id, consumerId, functionAccessGroupId, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/consumers/' + consumerId + '/functions/' + functionAccessGroupId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('deleteAccessgroupsServiceagreementsConsumersFunctionsRecord', 200)).then(pluginMocks('deleteAccessgroupsServiceagreementsConsumersFunctionsRecord', [id, consumerId, functionAccessGroupId, data], '{version}/accessgroups/serviceagreements/{id}/consumers/{consumerId}/functions/{functionAccessGroupId}')).catch(handleError('deleteAccessgroupsServiceagreementsConsumersFunctionsRecord'));
	    }
	
	    function putAccessgroupsServiceagreementsConsumersFunctionsDataRecord(id, consumerId, functionAccessGroupId, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/consumers/' + consumerId + '/functions/' + functionAccessGroupId + '/data';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putAccessgroupsServiceagreementsConsumersFunctionsDataRecord', 200)).then(pluginMocks('putAccessgroupsServiceagreementsConsumersFunctionsDataRecord', [id, consumerId, functionAccessGroupId, data], '{version}/accessgroups/serviceagreements/{id}/consumers/{consumerId}/functions/{functionAccessGroupId}/data')).catch(handleError('putAccessgroupsServiceagreementsConsumersFunctionsDataRecord'));
	    }
	
	    function deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord(id, consumerId, functionAccessGroupId, dataAccessGroupId, data) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/consumers/' + consumerId + '/functions/' + functionAccessGroupId + '/data/' + dataAccessGroupId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord', 200)).then(pluginMocks('deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord', [id, consumerId, functionAccessGroupId, dataAccessGroupId, data], '{version}/accessgroups/serviceagreements/{id}/consumers/{consumerId}/functions/{functionAccessGroupId}/data/{dataAccessGroupId}')).catch(handleError('deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord'));
	    }
	
	    function getAccessgroupsServiceagreementsFunctionsData(id, functionAccessGroupId, params) {
	      var url = '' + baseUri + version + '/accessgroups/serviceagreements/' + id + '/functions/' + functionAccessGroupId + '/data';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccessgroupsServiceagreementsFunctionsData', 200)).then(pluginMocks('getAccessgroupsServiceagreementsFunctionsData', [id, functionAccessGroupId, params], '{version}/accessgroups/serviceagreements/{id}/functions/{functionAccessGroupId}/data')).catch(handleError('getAccessgroupsServiceagreementsFunctionsData'));
	    }
	
	    schemas.postAccessgroupsDataRecord = { "properties": { "name": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "description": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "legalEntityId": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "type": { "type": "string", "required": true }, "items": { "type": "array", "items": { "properties": {} }, "required": true } } };
	
	    schemas.postAccessgroupsFunctionRecord = { "properties": { "name": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "description": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "legalEntityId": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "permissions": { "type": "array", "items": { "properties": { "functionId": { "type": "string", "required": true }, "assignedPrivileges": { "type": "array", "items": { "properties": { "privilege": { "type": "string", "required": true }, "limits": { "type": "array", "items": { "properties": { "limitType": { "enum": ["daily", "weekly", "monthly", "upper"], "required": true }, "amount": { "type": "number", "required": false } } }, "required": false } } }, "required": true } } }, "required": false } } };
	
	    schemas.postAccessgroupsUsersRecord = { "properties": { "userId": { "type": "string", "minLength": 1, "pattern": "^\\S+$", "required": true }, "serviceAgreementId": { "type": "string", "required": false } } };
	
	    schemas.postAccessgroupsServiceagreementsRecord = { "properties": { "name": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "description": { "type": "string", "minLength": 1, "pattern": "^\\S(.*(\\S))?$", "required": true }, "providers": { "type": "array", "items": { "properties": {} }, "required": true }, "consumers": { "type": "array", "items": { "properties": {} }, "required": true } } };
	
	    schemas.putAccessgroupsDataRecord = { "properties": { "dataAccessGroupId": { "type": "string", "required": true } } };
	
	    schemas.putAccessgroupsFunctionRecord = { "properties": { "functionAccessGroupId": { "type": "string", "required": true } } };
	
	    schemas.postAccessgroupsUsersFunctionRecord = { "properties": { "functionAccessGroupIds": { "type": "array", "items": { "properties": {} }, "minItems": 1, "required": true }, "revoke": { "type": "boolean", "required": true } } };
	
	    schemas.postAccessgroupsUsersDataRecord = { "properties": { "functionAccessGroupId": { "type": "string", "required": true }, "dataAccessGroupIds": { "type": "array", "items": { "properties": {} }, "minItems": 1, "required": true }, "revoke": { "type": "boolean", "required": true } } };
	
	    schemas.putAccessgroupsServiceagreementsAdminsRecord = { "properties": { "consumers": { "type": "array", "items": { "properties": { "id": { "type": "string", "required": true }, "admins": { "type": "array", "items": { "properties": {} }, "uniqueItems": true, "required": true } } }, "minItems": 1, "required": false }, "providers": { "type": "array", "items": { "properties": { "id": { "type": "string", "required": true }, "admins": { "type": "array", "items": { "properties": {} }, "uniqueItems": true, "required": true } } }, "minItems": 1, "required": false } } };
	
	    schemas.putAccessgroupsServiceagreementsUsersRecord = { "properties": { "id": { "type": "string", "required": true } } };
	
	    schemas.putAccessgroupsServiceagreementsConsumersFunctionsRecord = { "properties": { "functionAccessGroupIds": { "type": "array", "items": { "properties": {} }, "minItems": 1, "uniqueItems": true, "required": true } } };
	
	    schemas.putAccessgroupsServiceagreementsConsumersFunctionsDataRecord = { "properties": { "dataAccessGroupIds": { "type": "array", "items": { "properties": {} }, "minItems": 1, "uniqueItems": true, "required": true } } };
	
	    return {
	
	      getAccessgroupsData: getAccessgroupsData,
	
	      postAccessgroupsDataRecord: postAccessgroupsDataRecord,
	
	      getAccessgroupsFunction: getAccessgroupsFunction,
	
	      postAccessgroupsFunctionRecord: postAccessgroupsFunctionRecord,
	
	      getAccessgroupsConfigFunctions: getAccessgroupsConfigFunctions,
	
	      postAccessgroupsUsersRecord: postAccessgroupsUsersRecord,
	
	      getAccessgroupsUsers: getAccessgroupsUsers,
	
	      postAccessgroupsServiceagreementsRecord: postAccessgroupsServiceagreementsRecord,
	
	      getAccessgroupsServiceagreements: getAccessgroupsServiceagreements,
	
	      getAccessgroupsDataRecord: getAccessgroupsDataRecord,
	
	      putAccessgroupsDataRecord: putAccessgroupsDataRecord,
	
	      deleteAccessgroupsDataRecord: deleteAccessgroupsDataRecord,
	
	      getAccessgroupsFunctionRecord: getAccessgroupsFunctionRecord,
	
	      putAccessgroupsFunctionRecord: putAccessgroupsFunctionRecord,
	
	      deleteAccessgroupsFunctionRecord: deleteAccessgroupsFunctionRecord,
	
	      getAccessgroupsUsersRecord: getAccessgroupsUsersRecord,
	
	      getAccessgroupsUsersPrivileges: getAccessgroupsUsersPrivileges,
	
	      getAccessgroupsUsersPermissions: getAccessgroupsUsersPermissions,
	
	      postAccessgroupsUsersFunctionRecord: postAccessgroupsUsersFunctionRecord,
	
	      postAccessgroupsUsersDataRecord: postAccessgroupsUsersDataRecord,
	
	      getAccessgroupsUsersPrivilegesArrangements: getAccessgroupsUsersPrivilegesArrangements,
	
	      getAccessgroupsUsersPermissionsSummary: getAccessgroupsUsersPermissionsSummary,
	
	      getAccessgroupsServiceagreementsRecord: getAccessgroupsServiceagreementsRecord,
	
	      getAccessgroupsServiceagreementsRoles: getAccessgroupsServiceagreementsRoles,
	
	      getAccessgroupsServiceagreementsAdminsMe: getAccessgroupsServiceagreementsAdminsMe,
	
	      putAccessgroupsServiceagreementsAdminsRecord: putAccessgroupsServiceagreementsAdminsRecord,
	
	      getAccessgroupsServiceagreementsUsers: getAccessgroupsServiceagreementsUsers,
	
	      putAccessgroupsServiceagreementsUsersRecord: putAccessgroupsServiceagreementsUsersRecord,
	
	      getAccessgroupsServiceagreementsFunctions: getAccessgroupsServiceagreementsFunctions,
	
	      deleteAccessgroupsServiceagreementsUsersRecord: deleteAccessgroupsServiceagreementsUsersRecord,
	
	      putAccessgroupsServiceagreementsConsumersFunctionsRecord: putAccessgroupsServiceagreementsConsumersFunctionsRecord,
	
	      deleteAccessgroupsServiceagreementsConsumersFunctionsRecord: deleteAccessgroupsServiceagreementsConsumersFunctionsRecord,
	
	      putAccessgroupsServiceagreementsConsumersFunctionsDataRecord: putAccessgroupsServiceagreementsConsumersFunctionsDataRecord,
	
	      deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord: deleteAccessgroupsServiceagreementsConsumersFunctionsDataRecord,
	
	      getAccessgroupsServiceagreementsFunctionsData: getAccessgroupsServiceagreementsFunctionsData,
	
	      schemas: schemas
	    };
	  };
	};

/***/ })
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-accessgroups-http-ng.js.map