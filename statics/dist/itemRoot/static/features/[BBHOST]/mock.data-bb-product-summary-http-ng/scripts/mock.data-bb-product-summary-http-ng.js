(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-product-summary-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-product-summary-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-product-summary-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.productSummaryDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbProductSummaryHttp = __webpack_require__(3);
	
	var _dataBbProductSummaryHttp2 = _interopRequireDefault(_dataBbProductSummaryHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module data-bb-product-summary-http-ng
	 *
	 * @description A data module for accessing the Product Summary REST API
	 *
	 * @returns {String} `data-bb-product-summary-http-ng`
	 * @example
	 * import productSummaryDataModuleKey, {
	 *   productSummaryDataKey,
	 * } from 'data-bb-product-summary-http-ng';
	 */
	
	var productSummaryDataModuleKey = 'data-bb-product-summary-http-ng';
	/**
	 * @name productSummaryDataKey
	 * @type {string}
	 * @description Angular dependency injection key for the Product Summary data service
	 */
	var productSummaryDataKey = exports.productSummaryDataKey = 'data-bb-product-summary-http-ng:productSummaryData';
	/**
	 * @name default
	 * @type {string}
	 * @description Angular dependency injection module key
	 */
	exports.default = _vendorBbAngular2.default.module(productSummaryDataModuleKey, [])
	
	/**
	 * @constructor ProductSummaryData
	 * @type {object}
	 *
	 * @description Public api for service data-bb-product-summary-http
	 *
	 */
	.provider(productSummaryDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  /**
	   * @name ProductSummaryDataProvider
	   * @type {object}
	   * @description
	   * Data service that can be configured with custom base URI.
	   *
	   * @example
	   * angular.module(...)
	   *   .config(['data-bb-product-summary-http-ng:productSummaryDataProvider',
	   *     (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *       });
	   */
	  return {
	    /**
	     * @name ProductSummaryDataProvider#setBaseUri
	     * @type {function}
	     * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
	     */
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	
	    /**
	     * @name ProductSummaryDataProvider#$get
	     * @type {function}
	     * @return {object} An instance of the service
	     */
	    $get: ['$q',
	    /* into */
	    (0, _dataBbProductSummaryHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _mocks = __webpack_require__(4);
	
	var _mocks2 = _interopRequireDefault(_mocks);
	
	var _filterable = __webpack_require__(6);
	
	var _filterable2 = _interopRequireDefault(_filterable);
	
	var _sortable = __webpack_require__(7);
	
	var _sortable2 = _interopRequireDefault(_sortable);
	
	var _pageable = __webpack_require__(8);
	
	var _pageable2 = _interopRequireDefault(_pageable);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint-disable */
	exports.default = function (conf) {
	  return function (Promise) {
	    /**
	     * @description
	     * Emulate delay for mocks
	     *
	     * @param milliseconds
	     * @return {Function}
	     */
	    var delay = function delay(milliseconds) {
	      return function (result) {
	        return new Promise(function (resolve) {
	          setTimeout(function () {
	            resolve(result);
	          }, milliseconds);
	        });
	      };
	    };
	
	    var config = {
	      endpoint: conf.baseUri + 'v2'
	    };
	    /**
	     * @description
	     * JSON schema data. Keys of the object are names of the POST and PUT methods listed below.
	     *
	     * @name ProductSummaryData#schemas
	     * @type {object}
	     */
	    var schemas = {};
	
	    /*
	     * @name parse
	     * @type {function}
	     * @private
	     * @description Should be overitten by transformRespone on a project level
	     */
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers
	      };
	    }
	
	    var DEFAULT_RESPONSE = {
	      data: {},
	      status: 200,
	      headers: function headers() {
	        return null;
	      },
	      config: {},
	      statusText: 'OK'
	    };
	
	    function getResponse(responses, statusCode) {
	      var response = void 0;
	      if (statusCode) {
	        response = responses.find(function (resp) {
	          return resp.status === statusCode;
	        });
	      }
	
	      if (!response) {
	        // Find the happy response.
	        response = responses.find(function (resp) {
	          return resp.status.indexOf('2') === 0;
	        });
	      }
	
	      return response ? Object.assign({}, DEFAULT_RESPONSE, response) : DEFAULT_RESPONSE;
	    }
	
	    /**
	     * @name ProductSummaryData#getProductsummary
	     * @type {function}
	     * @description Perform a GET request to the URI.Retrieve list of products summaries.
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * productSummaryData
	     *  .getProductsummary(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getProductsummary(data) {
	      var url = config.endpoint + '/productsummary';
	
	      var responses = [{
	        "status": "200",
	        "data": {
	          "aggregatedBalance": {
	            "value": "126692.78",
	            "currency": "EUR"
	          },
	          "currentAccounts": {
	            "name": "Current Account",
	            "products": [{
	              "id": "1cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Mr and Mrs J. Smith",
	              "alias": "Our joined account",
	              "bookedBalance": "1000.00",
	              "availableBalance": "1500.00",
	              "creditLimit": "500.00",
	              "currency": "EUR",
	              "IBAN": "DE69759506070806035216",
	              "BBAN": "FR596129172765GE6UQ2U8TYD56",
	              "externalTransferAllowed": true,
	              "urgentTransferAllowed": true,
	              "crossCurrencyAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "1000.00",
	              "currency": "EUR"
	            }
	          },
	          "savingsAccounts": {
	            "name": "Savings Account",
	            "products": [{
	              "id": "3cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Smith Bonus Savings",
	              "productKindName": "Savings Account",
	              "productTypeName": "productTypeName",
	              "bookedBalance": "5500.00",
	              "availableBalance": "3500.00",
	              "accruedInterest": "36.16",
	              "currency": "EUR",
	              "BBAN": "22-44-33-1556",
	              "IBAN": "NL81TRIO0212471066",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "5500.00",
	              "currency": "EUR"
	            }
	          },
	          "termDeposits": {
	            "name": "Term Deposit",
	            "products": [{
	              "id": "4cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "3 Month Deposit",
	              "productNumber": "5fbd5aec-5e37-4ceb-95e0-7787ca42886f",
	              "principalAmount": "10000.00",
	              "accruedInterest": "12.32",
	              "availableBalance": "323.07",
	              "currency": "EUR",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": false
	            }, {
	              "id": "8cdb2224-8926-4b4d-a99f-1c9dfbbb4610",
	              "name": "Sera Bank Deposite Account",
	              "productNumber": "f76a73c7-111b-4292-bdb4-7dcd357c150b",
	              "principalAmount": "12567.50",
	              "accruedInterest": "0.14",
	              "availableBalance": "45886.39",
	              "currency": "EUR",
	              "externalTransferAllowed": true,
	              "crossCurrencyAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "10000.00",
	              "currency": "EUR"
	            }
	          },
	          "loans": {
	            "name": "Loan",
	            "products": [{
	              "id": "5cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Short term variable interest",
	              "productNumber": "79f5a3ac-e505-41d7-88e9-7568fe4c13c3",
	              "bookedBalance": "150000.00",
	              "availableBalance": "-94.57",
	              "principalAmount": "200000.00",
	              "currency": "EUR",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": false
	            }],
	            "aggregatedBalance": {
	              "value": "150000.00",
	              "currency": "EUR"
	            }
	          },
	          "creditCards": {
	            "name": "Credit Card",
	            "products": [{
	              "id": "6cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Jason Smith",
	              "IBAN": "PK76FBGRHWSUBJF4QBMQ9FV0",
	              "externalTransferAllowed": true,
	              "crossCurrencyAllowed": true,
	              "productKindName": "Credit Card",
	              "productTypeName": "productTypeName",
	              "currency": "EUR",
	              "bookedBalance": "1250.00",
	              "availableBalance": "0.00",
	              "creditLimit": "5000.00",
	              "number": "1234",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "1250.00",
	              "currency": "EUR"
	            }
	          },
	          "debitCards": {
	            "name": "Debit Card",
	            "products": [{
	              "id": "7cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Mr J. Smith",
	              "productNumber": "bdce84a2-adf3-45c6-9765-07bcfc14e597",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": false,
	              "productKindName": "Debit Card",
	              "productTypeName": "productTypeName",
	              "currency": "UAH",
	              "availableBalance": "179.12",
	              "number": "1234",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "1250.00",
	              "currency": "EUR"
	            }
	          },
	          "investmentAccounts": {
	            "name": "Investment Account",
	            "products": [{
	              "id": "8cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Mr J. Smith Execution only portfolio",
	              "IBAN": "SI65413524760527800",
	              "BBAN": "RO72KWANERSHGW0Y3DGC3P9X",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": false,
	              "productKindName": "Investment Account",
	              "productTypeName": "productTypeName",
	              "availableBalance": "-1759.45",
	              "currency": "EUR",
	              "currentInvestmentValue": "75789.00",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "75789.00",
	              "currency": "EUR"
	            }
	          }
	        }
	      }, {
	        "status": "400"
	      }, {
	        "status": "500"
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    /**
	     * @name ProductSummaryData#getProductsummaryDebitaccounts
	     * @type {function}
	     * @description Perform a GET request to the URI.All accounts available to transfer from
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * productSummaryData
	     *  .getProductsummaryDebitaccounts(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getProductsummaryDebitaccounts(data) {
	      var url = config.endpoint + '/productsummary/debitaccounts';
	
	      var responses = [{
	        "status": "200",
	        "data": {
	          "currentAccounts": {
	            "name": "Current Account",
	            "products": [{
	              "id": "1cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Standard Current Account",
	              "externalTransferAllowed": true,
	              "crossCurrencyAllowed": true,
	              "productKindName": "Current Account",
	              "productTypeName": "Product type name",
	              "alias": "Standard Current Account Alias",
	              "bookedBalance": "12345.67",
	              "availableBalance": "13345.67",
	              "creditLimit": "500.00",
	              "IBAN": "NL66INGB0280680451",
	              "BBAN": "280680451",
	              "currency": "USD",
	              "urgentTransferAllowed": true
	            }, {
	              "id": "2cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "USD Current Account",
	              "externalTransferAllowed": true,
	              "crossCurrencyAllowed": true,
	              "productKindName": "Current Account",
	              "productTypeName": "Product type name",
	              "alias": "USD Current Account Alias",
	              "bookedBalance": "12345.67",
	              "availableBalance": "13345.67",
	              "creditLimit": "500.00",
	              "IBAN": "NL66INGB0280680451",
	              "BBAN": "280680452",
	              "currency": "USD",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "26691.34",
	              "currency": "USD"
	            }
	          },
	          "savingsAccounts": {
	            "name": "Savings Account",
	            "products": [{
	              "id": "3cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Junior's New Computer",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": false,
	              "productKindName": "Savings Account",
	              "productTypeName": "Product type name",
	              "bookedBalance": "12345.67",
	              "accruedInterest": "13345.67",
	              "BBAN": "280680453",
	              "IBAN": "NL81TRIO0212471066",
	              "currency": "PLN",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "13345.67",
	              "currency": "PLN"
	            }
	          }
	        }
	      }, {
	        "status": "400"
	      }, {
	        "status": "500"
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    /**
	     * @name ProductSummaryData#getProductsummaryCreditaccounts
	     * @type {function}
	     * @description Perform a GET request to the URI.All accounts available for transfer to
	     * @param {?object} data - optional configuration object
	     * @param {?string} data.debitAccountId - The debit account id to filter with
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * productSummaryData
	     *  .getProductsummaryCreditaccounts(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getProductsummaryCreditaccounts(data) {
	      var url = config.endpoint + '/productsummary/creditaccounts';
	
	      var responses = [{
	        "status": "200",
	        "data": {
	          "currentAccounts": {
	            "name": "Current Account",
	            "products": [{
	              "id": "1cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Standard Current Account",
	              "externalTransferAllowed": true,
	              "crossCurrencyAllowed": true,
	              "productKindName": "Current Account",
	              "productTypeName": "Product type name",
	              "alias": "Standard Current Account Alias",
	              "bookedBalance": "12345.67",
	              "availableBalance": "13345.67",
	              "creditLimit": "500.00",
	              "IBAN": "NL66INGB0280680451",
	              "BBAN": "280680451",
	              "currency": "USD",
	              "urgentTransferAllowed": true
	            }, {
	              "id": "2cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "USD Current Account",
	              "externalTransferAllowed": true,
	              "crossCurrencyAllowed": true,
	              "productKindName": "Current Account",
	              "productTypeName": "Product type name",
	              "alias": "USD Current Account Alias",
	              "bookedBalance": "12345.67",
	              "availableBalance": "13345.67",
	              "creditLimit": "500.00",
	              "IBAN": "NL66INGB0280680451",
	              "BBAN": "280680452",
	              "currency": "USD",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "26691.34",
	              "currency": "USD"
	            }
	          },
	          "savingsAccounts": {
	            "name": "Savings Account",
	            "products": [{
	              "id": "3cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	              "name": "Junior's New Computer",
	              "externalTransferAllowed": false,
	              "crossCurrencyAllowed": false,
	              "productKindName": "Savings Account",
	              "productTypeName": "Product type name",
	              "bookedBalance": "12345.67",
	              "accruedInterest": "13345.67",
	              "BBAN": "280680453",
	              "IBAN": "NL81TRIO0212471066",
	              "currency": "PLN",
	              "urgentTransferAllowed": true
	            }],
	            "aggregatedBalance": {
	              "value": "13345.67",
	              "currency": "PLN"
	            }
	          }
	        }
	      }, {
	        "status": "400"
	      }, {
	        "status": "500"
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    /**
	     * @name ProductSummaryData#getProductsummaryArrangements
	     * @type {function}
	     * @description Perform a GET request to the URI.Retrieve list of products summaries, flat structure.
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * productSummaryData
	     *  .getProductsummaryArrangements(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getProductsummaryArrangements(data) {
	      var rawProductSummary = _mocks2.default.productSummaryGet;
	      var filteredProductSummary = (0, _filterable2.default)(rawProductSummary, data);
	      var sortedProductSummary = (0, _sortable2.default)(filteredProductSummary, data);
	      var pagedProductSummary = (0, _pageable2.default)(sortedProductSummary, data);
	
	      var responses = [{
	        status: '200',
	        headers: function headers(header) {
	          switch (header) {
	            case 'x-total-count':
	              return filteredProductSummary.length;
	            default:
	              return null;
	          }
	        },
	        data: pagedProductSummary
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    return {
	      getProductsummary: getProductsummary,
	      getProductsummaryDebitaccounts: getProductsummaryDebitaccounts,
	      getProductsummaryCreditaccounts: getProductsummaryCreditaccounts,
	      getProductsummaryArrangements: getProductsummaryArrangements,
	      schemas: schemas
	    };
	  };
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _productSummaryRecordGet = __webpack_require__(5);
	
	var _productSummaryRecordGet2 = _interopRequireDefault(_productSummaryRecordGet);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  productSummaryGet: _productSummaryRecordGet2.default
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/* eslint-disable */
	exports.default = [{
	  "id": "1cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "Mr and Mrs J. Smith",
	  "alias": "Our joined account",
	  "externalArrangementId": "bcf10f4d-4b2f-4413-9bab-31ff693608b5",
	  "externalLegalEntityId": "27263171-94d5-4d82-975c-61d8c41644d0",
	  "externalProductId": "fade7867-533e-465e-90cb-e41675c54400",
	  "productKindName": "Current Account",
	  "bookedBalance": "1000.00",
	  "availableBalance": "1500.00",
	  "creditLimit": "442.12",
	  "IBAN": "FR188388353832IH3CAWFYXAA36",
	  "BBAN": "FR596129172765GE6UQ2U8TYD56",
	  "currency": "EUR",
	  "externalTransferAllowed": true,
	  "accruedInterest": "0.54",
	  "principalAmount": "620.54",
	  "currentInvestmentValue": 0.16,
	  "legalEntityId": "47dbac45-a297-47d5-9d11-c83b7c9eed91",
	  "productId": "36c8fc42-ec97-4f83-8a7c-d622625007f3",
	  "productTypeName": "Current Account",
	  "BIC": "AABAFI22"
	}, {
	  "id": "3cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "Smith Bonus Savings",
	  "productKindName": "Savings Account",
	  "productTypeName": "productTypeName",
	  "bookedBalance": "5500.00",
	  "availableBalance": "3500.00",
	  "accruedInterest": "36.16",
	  "currency": "EUR",
	  "BBAN": "22-44-33-1556",
	  "IBAN": "NL81TRIO0212471066",
	  "crossCurrencyAllowed": true,
	  "externalArrangementId": "a3f8defe-f447-4475-b5c1-d3971ec7a273",
	  "externalLegalEntityId": "db792960-9574-434c-9fa9-9c3c82229472",
	  "externalProductId": "df40193b-22a9-4dd6-ae25-db68cd5bbd47",
	  "creditLimit": "267.41",
	  "externalTransferAllowed": false,
	  "principalAmount": "375.73",
	  "currentInvestmentValue": 0.06,
	  "legalEntityId": "b6c52e85-c9a7-404d-84b1-ffd8d9007f40",
	  "productId": "ffb7b827-33b2-4c93-83ad-41511f788a56",
	  "productNumber": "ffdd939c-ac4a-4441-ae47-70a7259899e7",
	  "BIC": "OKOYFIHH"
	}, {
	  "id": "4cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "3 Month Deposit",
	  "principalAmount": "10000.00",
	  "accruedInterest": "12.32",
	  "currency": "EUR",
	  "externalTransferAllowed": false,
	  "crossCurrencyAllowed": false,
	  "externalArrangementId": "bf0ad157-96e6-4d92-8cdb-a642d74f5ea5",
	  "externalLegalEntityId": "48dd9c4a-c47b-429a-9b86-e1f7f98ba531",
	  "externalProductId": "19d57714-52c5-4c3c-8172-a70a2f13779d",
	  "productKindName": "Investment Account",
	  "bookedBalance": "631.37",
	  "availableBalance": "323.07",
	  "creditLimit": "177.37",
	  "currentInvestmentValue": 0.88,
	  "legalEntityId": "078335f8-5695-45ed-807c-b87cd7e7b03b",
	  "productId": "89d58325-9cc4-4012-bf5e-7cb1110eb746",
	  "productNumber": "5fbd5aec-5e37-4ceb-95e0-7787ca42886f",
	  "productTypeName": "Investment Account",
	  "BIC": "HANDFIHH"
	}, {
	  "id": "5cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "Short term variable interest",
	  "externalArrangementId": "828ea599-d4e1-42c1-a64b-e91d58291017",
	  "externalLegalEntityId": "21dec57a-86c0-4089-90e4-b9ef2d0ad6d4",
	  "externalProductId": "e526eabc-a616-4842-af79-7d93570a5815",
	  "productKindName": "Loan",
	  "bookedBalance": "150000.00",
	  "principalAmount": "200000.00",
	  "currency": "EUR",
	  "availableBalance": "-94.57",
	  "creditLimit": "414.66",
	  "externalTransferAllowed": false,
	  "accruedInterest": "0.61",
	  "currentInvestmentValue": 0.79,
	  "legalEntityId": "ff1cc415-2af5-4b3e-8aa7-4275b72beb41",
	  "productId": "717d27e1-2002-4ac3-aa74-1e2df4038c2d",
	  "productNumber": "79f5a3ac-e505-41d7-88e9-7568fe4c13c3",
	  "productTypeName": "Loan",
	  "BIC": "OKOYFIHH"
	}, {
	  "id": "6cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "Jason Smith",
	  "externalArrangementId": "72ab334c-9a5f-4864-a476-f3250d606e5e",
	  "externalLegalEntityId": "b410db32-02a8-4e5c-b373-964714050246",
	  "externalProductId": "a5bf8a16-118e-4bd3-9d57-f20e23677d00",
	  "productKindName": "Current Account",
	  "currency": "EUR",
	  "bookedBalance": "1250.00",
	  "availableBalance": "0.00",
	  "creditLimit": "5000.00",
	  "IBAN": "PK76FBGRHWSUBJF4QBMQ9FV0",
	  "externalTransferAllowed": false,
	  "accruedInterest": "0.69",
	  "principalAmount": "466.83",
	  "currentInvestmentValue": 0.33,
	  "legalEntityId": "e13d23a0-4131-4676-a043-58d020304c93",
	  "productId": "2cf3c2c0-8ad7-482b-9cb1-a0cf9224a5dd",
	  "productTypeName": "Current Account",
	  "BIC": "AABAFI22"
	}, {
	  "id": "7cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "Mr J. Smith",
	  "externalArrangementId": "2719ec5a-6f94-4d4f-a9fe-4826864e621f",
	  "externalLegalEntityId": "88a0d084-7c75-4f85-8e97-ad8b918ba9c4",
	  "externalProductId": "8c78bf93-0835-485f-979c-56c84fb1c0df",
	  "productKindName": "Loan",
	  "bookedBalance": "651.24",
	  "availableBalance": "179.12",
	  "creditLimit": "672.22",
	  "currency": "UAH",
	  "externalTransferAllowed": true,
	  "accruedInterest": "0.75",
	  "principalAmount": "590.94",
	  "currentInvestmentValue": 0.73,
	  "legalEntityId": "f8e6b90a-799e-4f73-8384-560e977f8444",
	  "productId": "72f00772-77bc-45ea-b204-9b879739d054",
	  "productNumber": "bdce84a2-adf3-45c6-9765-07bcfc14e597",
	  "productTypeName": "Loan",
	  "BIC": "OKOYFIHH"
	}, {
	  "id": "8cdb2224-8926-4b4d-a99f-1c9dfbbb4699",
	  "name": "Mr J. Smith Execution only portfolio",
	  "externalArrangementId": "09fde03f-fa65-4053-b485-d97212e7ecdc",
	  "externalLegalEntityId": "1bf55edd-9272-453d-86f9-94c6f8f17769",
	  "externalProductId": "1b2a4a9c-3f62-45b4-9ba3-786253b6b86e",
	  "productKindName": "Current Account",
	  "bookedBalance": "263.85",
	  "availableBalance": "-1759.45",
	  "creditLimit": "319.98",
	  "IBAN": "SI65413524760527800",
	  "BBAN": "RO72KWANERSHGW0Y3DGC3P9X",
	  "currency": "EUR",
	  "externalTransferAllowed": true,
	  "accruedInterest": "0.79",
	  "principalAmount": "978.98",
	  "currentInvestmentValue": "75789.00",
	  "legalEntityId": "8c13402f-f37a-4f82-a1fe-ae6789fad3f4",
	  "productId": "1eb6d1c8-090a-413e-99aa-b43112bd5148",
	  "productTypeName": "Current Account",
	  "BIC": "AABAFI22"
	}, {
	  "id": "8cdb2224-8926-4b4d-a99f-1c9dfbbb4610",
	  "name": "Sera Bank Deposite Account",
	  "externalArrangementId": "828ea599-d4e1-42c1-a64b-e91d58291088",
	  "externalLegalEntityId": "21dec57a-86c0-4089-90e4-b9ef2d0ad6d75",
	  "externalProductId": "e526eabc-a616-4842-af79-7d93570a5886",
	  "productKindName": "Term Deposit",
	  "bookedBalance": 45912.39,
	  "availableBalance": 45886.39,
	  "creditLimit": 100000,
	  "IBAN": "FR596129172765GE6UQ2U8TYD72",
	  "BBAN": "RO72KWANERSHGW0Y3DGC3P9X",
	  "currency": "EUR",
	  "externalTransferAllowed": true,
	  "crossCurrencyAllowed": true,
	  "accruedInterest": 0.14,
	  "principalAmount": 12567.50,
	  "currentInvestmentValue": "9826781.00",
	  "legalEntityId": "8c13402f-f37a-4f82-a1fe-ae6789fad3f4",
	  "productId": "1eb6d1c8-090a-413e-99aa-b43112bd5148",
	  "productNumber": "f76a73c7-111b-4292-bdb4-7dcd357c150b",
	  "productTypeName": "Term Deposit",
	  "BIC": "KNABNL2H"
	}];

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var filterable = function filterable(collection, params) {
	  // Filter by productKindName
	  var filtered = params.productKindName ? collection.filter(function (item) {
	    return item.productKindName === params.productKindName;
	  }) : collection;
	
	  return filtered;
	};
	
	exports.default = filterable;

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var I = function I(value) {
	  return value;
	};
	var sortableValue = {
	  name: I,
	  number: function number(key, item) {
	    return item.IBAN || item.BBAN || item.productNumber;
	  },
	  BIC: I,
	  productKindName: I,
	  currency: I,
	  bookedBalance: function bookedBalance(value, item) {
	    return parseInt(value, 10);
	  },
	  availableBalance: function availableBalance(value, item) {
	    return parseInt(value, 10);
	  }
	};
	
	var sortable = function sortable(collection, data) {
	  var key = data && data.orderBy || 'name';
	  var direction = data && data.direction || 'DESC';
	
	  return collection.slice(0).sort(function (a, b) {
	    var dir = direction === 'ASC' ? 1 : -1;
	    if (sortableValue[key](a[key], a) > sortableValue[key](b[key], b)) {
	      return dir * 1;
	    } else if (sortableValue[key](a[key], a) < sortableValue[key](b[key], b)) {
	      return dir * -1;
	    }
	
	    return 0;
	  });
	};
	
	exports.default = sortable;

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var pageable = function pageable(collection, data) {
	  var from = parseInt(data && data.from, 10) || 0;
	  var size = parseInt(data && data.size, 10) || 10;
	  var startIndex = from * size;
	  var endIndex = startIndex + size;
	
	  return collection.slice(startIndex, endIndex);
	};
	
	exports.default = pageable;

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-product-summary-http-ng.js.map