(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-action-recipes-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-action-recipes-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-action-recipes-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.actionRecipesDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbActionRecipesHttp = __webpack_require__(3);
	
	var _dataBbActionRecipesHttp2 = _interopRequireDefault(_dataBbActionRecipesHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var actionRecipesDataModuleKey = 'data-bb-action-recipes-http-ng';
	
	var actionRecipesDataKey = exports.actionRecipesDataKey = 'data-bb-action-recipes-http-ng:actionRecipesData';
	
	exports.default = _vendorBbAngular2.default.module(actionRecipesDataModuleKey, []).provider(actionRecipesDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbActionRecipesHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (conf) {
	  return function (Promise) {
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers,
	        status: res.status,
	        statusText: res.statusText
	      };
	    }
	
	    function getActionRecipeSpecifications(params) {
	      var url = baseUri + '/' + version + '/action-recipe-specifications';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "actionRecipeSpecifications": [{ "id": "5", "name": "New Transaction", "userDefinable": true, "type": "newTransaction", "triggerDefinition": { "selectors": [{ "path": "transaction.arrangementId", "type": "string" }], "filter": [{ "path": "transaction.amount", "type": "number" }] }, "actions": [{ "type": "notification", "templateId": "1" }] }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getActionRecipes(params) {
	      var url = baseUri + '/' + version + '/action-recipes';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "actionRecipes": [{ "id": "1", "name": "Transaction > 100 EUR", "active": true, "specificationId": "5", "userId": "kjordan", "trigger": { "selectors": [{ "path": "transaction.arrangementId", "value": "123456" }], "filter": { "and": [{ "gt": [{ "pathValue": ["transaction.amount"] }, 100] }, { "or": [{ "eq": [{ "pathValue": ["transaction.creditDebitIndicator"] }, "CRDT"] }, { "eq": [{ "pathValue": ["transaction.creditDebitIndicator"] }, "DBIT"] }] }] } }, "actions": [{ "type": "notification" }] }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postActionRecipesRecord(data) {
	      var url = baseUri + '/' + version + '/action-recipes';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getActionRecipesRecord(recipeId, params) {
	      var url = baseUri + '/' + version + '/action-recipes/' + recipeId;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function putActionRecipesRecord(recipeId, data) {
	      var url = baseUri + '/' + version + '/action-recipes/' + recipeId;
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function deleteActionRecipesRecord(recipeId, data) {
	      var url = baseUri + '/' + version + '/action-recipes/' + recipeId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postActionRecipesDeactivationRequestRecord(recipeId, data) {
	      var url = baseUri + '/' + version + '/action-recipes/' + recipeId + '/deactivation-request';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postActionRecipesActivationRequestRecord(recipeId, data) {
	      var url = baseUri + '/' + version + '/action-recipes/' + recipeId + '/activation-request';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    schemas.postActionRecipesRecord = { "properties": { "name": { "type": "string", "required": true }, "specificationId": { "type": "string", "required": true }, "active": { "type": "boolean", "required": true }, "trigger": { "type": "object", "properties": { "selectors": { "type": "array", "items": { "properties": { "path": { "type": "string", "required": false }, "value": { "type": "string", "required": false } } }, "required": false }, "filter": { "type": "object", "properties": {}, "required": false } }, "required": true }, "actions": { "type": "array", "items": { "properties": { "type": { "type": "string", "required": false } } }, "required": true } } };
	
	    schemas.putActionRecipesRecord = { "properties": { "name": { "type": "string", "required": true }, "specificationId": { "type": "string", "required": true }, "active": { "type": "boolean", "required": true }, "trigger": { "type": "object", "properties": { "selectors": { "type": "array", "items": { "properties": { "path": { "type": "string", "required": false }, "value": { "type": "string", "required": false } } }, "required": false }, "filter": { "type": "object", "properties": {}, "required": false } }, "required": false }, "actions": { "type": "array", "items": { "properties": { "type": { "type": "string", "required": false } } }, "required": false } } };
	
	    return {
	
	      getActionRecipeSpecifications: getActionRecipeSpecifications,
	
	      getActionRecipes: getActionRecipes,
	
	      postActionRecipesRecord: postActionRecipesRecord,
	
	      getActionRecipesRecord: getActionRecipesRecord,
	
	      putActionRecipesRecord: putActionRecipesRecord,
	
	      deleteActionRecipesRecord: deleteActionRecipesRecord,
	
	      postActionRecipesDeactivationRequestRecord: postActionRecipesDeactivationRequestRecord,
	
	      postActionRecipesActivationRequestRecord: postActionRecipesActivationRequestRecord,
	
	      schemas: schemas
	    };
	  };
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-action-recipes-http-ng.js.map