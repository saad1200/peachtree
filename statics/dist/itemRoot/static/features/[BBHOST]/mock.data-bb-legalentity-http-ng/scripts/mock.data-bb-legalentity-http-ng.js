(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-legalentity-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-legalentity-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-legalentity-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.legalEntityDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbLegalentityHttp = __webpack_require__(3);
	
	var _dataBbLegalentityHttp2 = _interopRequireDefault(_dataBbLegalentityHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var legalEntityDataModuleKey = 'data-bb-legalentity-http-ng';
	
	var legalEntityDataKey = exports.legalEntityDataKey = 'data-bb-legalentity-http-ng:legalEntityData';
	
	exports.default = _vendorBbAngular2.default.module(legalEntityDataModuleKey, []).provider(legalEntityDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbLegalentityHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (conf) {
	  return function (Promise) {
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers,
	        status: res.status,
	        statusText: res.statusText
	      };
	    }
	
	    function getLegalentities(params) {
	      var url = '' + baseUri + version + '/legalentities';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": [{ "id": "1", "externalId": "ext1", "name": "Endava", "parentId": "", "isParent": true }, { "id": "2", "externalId": "ext1", "name": "Apple", "parentId": "ext1", "isParent": false }, { "id": "3", "externalId": "ext1", "name": "John Smith", "parentId": "1", "isParent": true }, { "id": "4", "externalId": "ext2", "name": "Marin Trpenovski", "parentId": "1", "isParent": false }, { "id": "5", "externalId": "ext2", "name": "Daniel Eftimov", "parentId": "3", "isParent": false }], "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postLegalentitiesRecord(data) {
	      var url = '' + baseUri + version + '/legalentities';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "id": "0955e686-d31e-4216-b3dd-5d66161d536d" }, "status": 201, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getLegalentitiesRecord(legalEntityId, params) {
	      var url = '' + baseUri + version + '/legalentities/' + legalEntityId;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "id": "1", "externalId": "ext1", "name": "Endava", "parentId": "", "isParent": false }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getLegalentitiesSubEntities(params) {
	      var url = '' + baseUri + version + '/legalentities/sub-entities';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": [{ "id": "1", "externalId": "ext1", "name": "Endava", "parentId": "", "isParent": true }, { "id": "2", "externalId": "ext1", "name": "Apple", "parentId": "ext1", "isParent": false }, { "id": "3", "externalId": "ext1", "name": "John Smith", "parentId": "1", "isParent": true }, { "id": "4", "externalId": "ext2", "name": "Marin Trpenovski", "parentId": "1", "isParent": false }, { "id": "5", "externalId": "ext2", "name": "Daniel Eftimov", "parentId": "3", "isParent": false }], "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getLegalentitiesExternalRecord(externalId, params) {
	      var url = '' + baseUri + version + '/legalentities/external/' + externalId;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "id": "1", "externalId": "ext1", "name": "Endava", "parentId": "", "isParent": true }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getLegalentitiesServiceagreementsMaster(legalEntityId, params) {
	      var url = '' + baseUri + version + '/legalentities/' + legalEntityId + '/serviceagreements/master';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "id": "0001", "name": "Broker deal 1", "description": "Agreement between Backbase and Apple", "creatorLegalEntity": "001", "isMaster": false, "participants": ["LE-0001", "LE-002"], "providers": [{ "id": "001", "users": ["001", "002"], "admins": ["001"] }, { "id": "003", "users": ["003", "004"], "admins": ["003"] }], "consumers": [{ "id": "001", "dataAccessGroupFunctionAccessGroupPairs": ["FAG1-DAG1", "FAG2-DAG2"], "admins": ["001"] }, { "id": "003", "dataAccessGroupFunctionAccessGroupPairs": ["FAG3-DAG3", "FAG1-DAG1"], "admins": ["003"] }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    schemas.postLegalentitiesRecord = { "properties": { "externalId": { "type": "string", "required": true }, "name": { "type": "string", "required": true }, "parentExternalId": { "type": "string", "required": false } } };
	
	    return {
	
	      getLegalentities: getLegalentities,
	
	      postLegalentitiesRecord: postLegalentitiesRecord,
	
	      getLegalentitiesRecord: getLegalentitiesRecord,
	
	      getLegalentitiesSubEntities: getLegalentitiesSubEntities,
	
	      getLegalentitiesExternalRecord: getLegalentitiesExternalRecord,
	
	      getLegalentitiesServiceagreementsMaster: getLegalentitiesServiceagreementsMaster,
	
	      schemas: schemas
	    };
	  };
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-legalentity-http-ng.js.map