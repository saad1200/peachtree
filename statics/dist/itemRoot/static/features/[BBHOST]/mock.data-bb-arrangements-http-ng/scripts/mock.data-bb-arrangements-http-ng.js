(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-arrangements-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-arrangements-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-arrangements-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.arrangementsDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbArrangementsHttp = __webpack_require__(3);
	
	var _dataBbArrangementsHttp2 = _interopRequireDefault(_dataBbArrangementsHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint-disable */
	var arrangementsDataModuleKey = 'data-bb-arrangements-http-ng';
	
	var arrangementsDataKey = exports.arrangementsDataKey = 'data-bb-arrangements-http-ng:arrangementsData';
	
	exports.default = _vendorBbAngular2.default.module(arrangementsDataModuleKey, []).provider(arrangementsDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbArrangementsHttp2.default)(config)]
	  };
	}]).name;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	/* eslint-disable */
	exports.default = function (conf) {
	  return function (Promise) {
	    var _plugins;
	
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    var state = {};
	
	    var responses = {
	
	      postArrangementsRecord: [{ "status": 201, "data": { "id": "729190df-a421-4937-94fd-5e1a3da132cc" } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      putArrangementsRecord: [{ "status": 204, "data": null }, { "status": 400, "data": null }, { "status": 401, "data": null }, { "status": 403, "data": null }, { "status": 404, "data": null }, { "status": 500, "data": null }],
	
	      patchArrangementsRecord: [{ "status": 204, "data": null }, { "status": 400, "data": null }, { "status": 401, "data": null }, { "status": 403, "data": null }, { "status": 404, "data": null }, { "status": 500, "data": null }],
	
	      getArrangementsInternalRecord: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getArrangementsRecord: [{ "status": 200, "data": { "externalArrangementId": "kjh4567-asd1-11e7-b114-b2f933d50000", "externalLegalEntityId": "COOOOO1", "externalProductId": "1234567", "name": "name", "alias": "Secret account", "bookedBalance": 100.1, "availableBalance": 100.2, "creditLimit": 100.3, "IBAN": "MKIBAN", "BBAN": "BBAN", "currency": "EUR", "externalTransferAllowed": true, "urgentTransferAllowed": false, "accruedInterest": 2.2, "PANSuffix": "PANSuffix", "principalAmount": 100.4, "currentInvestmentValue": 100.5, "legalEntityId": "legalEntityId", "productId": "productId", "BIC": "BICExample123", "bankBranchCode": "bankBranchCode1" } }, { "status": 400, "data": null }, { "status": 404, "data": null }, { "status": 500, "data": null }],
	
	      getAccountsBalance: [{ "status": 200, "data": null }, { "status": 400, "data": null }, { "status": 404, "data": null }, { "status": 500, "data": null }]
	
	    };
	
	    var DEFAULT_MOCK = {
	      data: {},
	      status: 200,
	      headers: function headers(header) {
	        return header === 'content-type' && this.data ? 'application/json' : null;
	      },
	      config: {},
	      statusText: 'OK'
	    };
	
	    var getResponse = function getResponse(method, status) {
	      var response = (responses[method] || []).find(function (response) {
	        return response.status === status;
	      });
	      return Object.assign({}, DEFAULT_MOCK, response);
	    };
	
	    var PLUGINS_ALL = '__all__';
	
	    var plugins = (_plugins = {}, _defineProperty(_plugins, PLUGINS_ALL, []), _defineProperty(_plugins, 'postArrangementsRecord', []), _defineProperty(_plugins, 'putArrangementsRecord', []), _defineProperty(_plugins, 'patchArrangementsRecord', []), _defineProperty(_plugins, 'getArrangementsInternalRecord', []), _defineProperty(_plugins, 'getArrangementsRecord', []), _defineProperty(_plugins, 'getAccountsBalance', []), _plugins);
	
	    var pluginMocks = function pluginMocks(method, args, uri) {
	      var methodPlugins = plugins[method] || [];
	      var commonPlugins = plugins[PLUGINS_ALL] || [];
	      var allPlugins = methodPlugins.concat(commonPlugins);
	
	      return function (initialResult) {
	        return allPlugins.reduce(function (result, plugin) {
	          return result.then(function (nextResult) {
	            return plugin(nextResult, args, uri, method);
	          });
	        }, Promise.resolve(initialResult));
	      };
	    };
	
	    var handleError = function handleError(method) {
	      return function (error) {
	        // If error object is one of the error responses, assume it returned intentionally from one of the plugins
	        var isIntendedError = error && error.status && error.status >= 400;
	        var response = isIntendedError ? error : getResponse(method, 500);
	
	        console.log(method + ' request rejected because of ', error);
	        return Promise.reject(response);
	      };
	    };
	
	    function postArrangementsRecord(data) {
	      var url = '' + baseUri + version + '/arrangements';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postArrangementsRecord', 201)).then(pluginMocks('postArrangementsRecord', [data], '{version}/arrangements')).catch(handleError('postArrangementsRecord'));
	    }
	
	    function putArrangementsRecord(data) {
	      var url = '' + baseUri + version + '/arrangements';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putArrangementsRecord', 204)).then(pluginMocks('putArrangementsRecord', [data], '{version}/arrangements')).catch(handleError('putArrangementsRecord'));
	    }
	
	    function patchArrangementsRecord(data) {
	      var url = '' + baseUri + version + '/arrangements';
	      var mocking = {
	        method: 'PATCH',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('patchArrangementsRecord', 204)).then(pluginMocks('patchArrangementsRecord', [data], '{version}/arrangements')).catch(handleError('patchArrangementsRecord'));
	    }
	
	    function getArrangementsInternalRecord(externalId, params) {
	      var url = '' + baseUri + version + '/arrangements/internal/' + externalId;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getArrangementsInternalRecord', 200)).then(pluginMocks('getArrangementsInternalRecord', [externalId, params], '{version}/arrangements/internal/{externalId}')).catch(handleError('getArrangementsInternalRecord'));
	    }
	
	    function getArrangementsRecord(id, params) {
	      var url = '' + baseUri + version + '/arrangements/' + id;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getArrangementsRecord', 200)).then(pluginMocks('getArrangementsRecord', [id, params], '{version}/arrangements/{id}')).catch(handleError('getArrangementsRecord'));
	    }
	
	    function getAccountsBalance(params) {
	      var url = '' + baseUri + version + '/accounts/balance';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getAccountsBalance', 200)).then(pluginMocks('getAccountsBalance', [params], '{version}/accounts/balance')).catch(handleError('getAccountsBalance'));
	    }
	
	    schemas.postArrangementsRecord = { "properties": { "externalLegalEntityId": { "type": "string", "pattern": "^[a-zA-Z0-9_.-]*$", "required": true }, "externalProductId": { "type": "string", "pattern": "^[a-zA-Z0-9_.-]*$", "required": true }, "alias": { "type": "string", "maxLength": 64, "required": false }, "legalEntityId": { "type": "string", "pattern": "^[a-zA-Z0-9_.-]*$", "required": true }, "productId": { "type": "string", "pattern": "^[a-zA-Z0-9_.-]*$", "required": false } } };
	
	    schemas.putArrangementsRecord = { "properties": { "name": { "type": "string", "pattern": "^[a-zA-Z0-9 _.-]*$", "required": false }, "bookedBalance": { "type": "number", "required": false }, "availableBalance": { "type": "number", "required": false }, "creditLimit": { "type": "number", "required": false }, "IBAN": { "type": "string", "maxLength": 34, "pattern": "^(AF|AX|AL|DZ|AS|AD|AO|AI|AQ|AG|AR|AM|AW|AU|AT|AZ|BS|BH|BD|BB|BY|BE|BZ|BJ|BM|BT|BO|BQ|BA|BW|BV|BR|IO|BN|BG|BF|BI|KH|CM|CA|CV|KY|CF|TD|CL|CN|CX|CC|CO|KM|CG|CD|CK|CR|CI|HR|CU|CW|CY|CZ|DK|DJ|DM|DO|EC|EG|SV|GQ|ER|EE|ET|FK|FO|FJ|FI|FR|GF|PF|TF|GA|GM|GE|DE|GH|GI|GR|GL|GD|GP|GU|GT|GG|GN|GW|GY|HT|HM|VA|HN|HK|HU|IS|IN|ID|IR|IQ|IE|IM|IL|IT|JM|JP|JE|JO|KZ|KE|KI|KP|KR|KW|KG|LA|LV|LB|LS|LR|LY|LI|LT|LU|MO|MK|MG|MW|MY|MV|ML|MT|MH|MQ|MR|MU|YT|MX|FM|MD|MC|MN|ME|MS|MA|MZ|MM|NA|NR|NP|NL|NC|NZ|NI|NE|NG|NU|NF|MP|NO|OM|PK|PW|PS|PA|PG|PY|PE|PH|PN|PL|PT|PR|QA|RE|RO|RU|RW|BL|SH|KN|LC|MF|PM|VC|WS|SM|ST|SA|SN|RS|SC|SL|SG|SX|SK|SI|SB|SO|ZA|GS|SS|ES|LK|SD|SR|SJ|SZ|SE|CH|SY|TW|TJ|TZ|TH|TL|TG|TK|TO|TT|TN|TR|TM|TC|TV|UG|UA|AE|GB|US|UM|UY|UZ|VU|VE|VN|VG|VI|WF|EH|YE|ZM|ZW)[a-zA-Z0-9_.-]*", "required": false }, "BBAN": { "type": "string", "maxLength": 30, "required": false }, "currency": { "enum": ["AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BHD", "BIF", "BMD", "BND", "BOB", "BOV", "BRL", "BSD", "BTN", "BWP", "BYN", "BZD", "CAD", "CDF", "CHE", "CHW", "CLF", "CLP", "CNY", "COP", "COU", "CRC", "CUC", "CUP", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ERN", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GHS", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "IQD", "IRR", "ISK", "JMD", "JOD", "JPY", "KES", "KGS", "KHR", "KMF", "KPW", "KWD", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LYD", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MXV", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "OMR", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SDG", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "SSP", "STD", "SVC", "SYP", "SZL", "THB", "TJS", "TMT", "TND", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "USN", "UYI", "UYU", "UZS", "VEF", "VND", "VUV", "WST", "YER", "ZAR", "ZMW", "ZWL"], "required": true }, "externalTransferAllowed": { "type": "boolean", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false }, "accruedInterest": { "type": "number", "required": false }, "number": { "type": "string", "maxLength": 4, "required": false }, "principalAmount": { "type": "number", "required": false }, "currentInvestmentValue": { "type": "number", "minimum": 0, "required": false }, "productNumber": { "type": "string", "required": false }, "BIC": { "type": "string", "required": false }, "bankBranchCode": { "type": "string", "required": false }, "externalArrangementId": { "type": "string", "pattern": "^[a-zA-Z0-9_.-]*$", "required": true } } };
	
	    return {
	
	      postArrangementsRecord: postArrangementsRecord,
	
	      putArrangementsRecord: putArrangementsRecord,
	
	      patchArrangementsRecord: patchArrangementsRecord,
	
	      getArrangementsInternalRecord: getArrangementsInternalRecord,
	
	      getArrangementsRecord: getArrangementsRecord,
	
	      getAccountsBalance: getAccountsBalance,
	
	      schemas: schemas
	    };
	  };
	};

/***/ })
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-arrangements-http-ng.js.map