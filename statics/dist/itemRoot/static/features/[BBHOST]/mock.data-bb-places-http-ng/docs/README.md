# data-bb-places-http-ng

A data module for accessing the Places REST API

## Imports

* vendor-bb-angular

---

## Exports

### *default*

Angular dependency injection module key

**Type:** *string*

### *placesDataKey*

Angular dependency injection key for the Places data service

**Type:** *string*


---

## Example

```javascript
import placesDataModuleKey, {
  placesDataKey,
} from 'data-bb-places-http-ng';
```

---

## PlacesData

Public api for service data-bb-places-http

## schemas

JSON schema data. Keys of the object are names of the POST and PUT methods listed below.

### *#getPlaces(data)*

Perform a GET request to the URI.Retrieve list of all places.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | ?object | optional configuration object |
| data.latitude | ?number | Latitude for current location. Should be used with longitude and radius params to return places available in specified radius. |
| data.longitude | ?number | Longitude for current location. Should be used with latitude and radius params to return places available in specified radius. |
| data.radius | ?integer | Search radius (distance in KM) |

##### Returns

Promise.&lt;object&gt; - *A promise resolving to object with headers and data keys*

## Example

```javascript
placesData
 .getPlaces(data)
 .then(function(result){
   console.log(result)
 });
```

---

## PlacesDataProvider

Data service that can be configured with custom base URI.

### *#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | string | Base URI which will be the prefix for all HTTP requests |

### *#$get()*


##### Returns

object - *An instance of the service*

## Example

```javascript
angular.module(...)
  .config(['data-bb-places-http-ng:placesDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });
```
