(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-places-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-places-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-places-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.placesDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbPlacesHttp = __webpack_require__(3);
	
	var _dataBbPlacesHttp2 = _interopRequireDefault(_dataBbPlacesHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module data-bb-places-http-ng
	 *
	 * @description A data module for accessing the Places REST API
	 *
	 * @returns {String} `data-bb-places-http-ng`
	 * @example
	 * import placesDataModuleKey, {
	 *   placesDataKey,
	 * } from 'data-bb-places-http-ng';
	 */
	
	var placesDataModuleKey = 'data-bb-places-http-ng';
	/**
	 * @name placesDataKey
	 * @type {string}
	 * @description Angular dependency injection key for the Places data service
	 */
	var placesDataKey = exports.placesDataKey = 'data-bb-places-http-ng:placesData';
	/**
	 * @name default
	 * @type {string}
	 * @description Angular dependency injection module key
	 */
	exports.default = _vendorBbAngular2.default.module(placesDataModuleKey, [])
	
	/**
	 * @constructor PlacesData
	 * @type {object}
	 *
	 * @description Public api for service data-bb-places-http
	 *
	 */
	.provider(placesDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  /**
	   * @name PlacesDataProvider
	   * @type {object}
	   * @description
	   * Data service that can be configured with custom base URI.
	   *
	   * @example
	   * angular.module(...)
	   *   .config(['data-bb-places-http-ng:placesDataProvider',
	   *     (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *       });
	   */
	  return {
	    /**
	     * @name PlacesDataProvider#setBaseUri
	     * @type {function}
	     * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
	     */
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	
	    /**
	     * @name PlacesDataProvider#$get
	     * @type {function}
	     * @return {object} An instance of the service
	     */
	    $get: ['$q',
	    /* into */
	    (0, _dataBbPlacesHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _mocks = __webpack_require__(4);
	
	var _mocks2 = _interopRequireDefault(_mocks);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function () {
	  return function (Promise) {
	    /**
	     * @description
	     * JSON schema data. Keys of the object are names of the POST and PUT methods listed below.
	     *
	     * @name PlacesData#schemas
	     * @type {object}
	     */
	    var schemas = {};
	
	    var bounds = {};
	
	    /*
	     * @name parse
	     * @type {function}
	     * @private
	     * @description Should be overitten by transformRespone on a project level
	     */
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers
	      };
	    }
	
	    /**
	     * @name isInBounds
	     * @description Checks if location is in defined bounds
	     * @type {function}
	     * @private
	     *
	     * @param  {Object} item location object
	     * @return {Boolean} Returns true if location is in defined bounds, false otherwise
	     */
	    function isInBounds(item) {
	      return item.latitude < bounds.northEast.lat() && item.latitude > bounds.southWest.lat() && item.longitude < bounds.northEast.lng() && item.longitude > bounds.southWest.lng();
	    }
	
	    /**
	     * @name getBounds
	     * @description Calculates north-east and south-west coordinates
	     * @type {function}
	     * @private
	     *
	     * @param  {object} params Should contain center point coordinates and radius
	     * @return {object} Returns object that contains north-east and south-west coordinates
	     */
	    function getBounds(params) {
	      var $maps = google.maps;
	      var center = new $maps.LatLng({
	        lat: params.latitude,
	        lng: params.longitude
	      });
	      // convert radius from km to meters
	      var radius = params.radius * 1000;
	
	      return {
	        northEast: $maps.geometry.spherical.computeOffset(center, radius * Math.sqrt(2), 45),
	        southWest: $maps.geometry.spherical.computeOffset(center, radius * Math.sqrt(2), 225)
	      };
	    }
	
	    /*
	     * @name inBoundsFilter
	     * @type {function}
	     * @private
	     * @description Filter out locations that are not in defined bounds
	     *
	     * @Return {object}
	     */
	    function inBoundsFilter(res) {
	      return Object.assign(res, { data: res.data.filter(isInBounds) });
	    }
	
	    var DEFAULT_RESPONSE = {
	      data: {},
	      status: 200,
	      headers: function headers() {
	        return null;
	      },
	      config: {},
	      statusText: 'OK'
	    };
	
	    function getResponse(responses, statusCode) {
	      var response = void 0;
	      if (statusCode) {
	        response = responses.find(function (resp) {
	          return resp.status === statusCode;
	        });
	      }
	
	      if (!response) {
	        // Find the happy response.
	        response = responses.find(function (resp) {
	          return resp.status.indexOf('2') === 0;
	        });
	      }
	
	      return response ? Object.assign({}, DEFAULT_RESPONSE, response) : DEFAULT_RESPONSE;
	    }
	
	    /**
	     * @name PlacesData#getPlaces
	     * @type {function}
	     * @description Perform a GET request to the URI.Retrieve list of all places.
	     * @param {?object} data - optional configuration object
	     * @param {?number} data.latitude - Latitude for current location.
	     * Should be used with longitude and radius params to return places available in specified radius.
	     * @param {?number} data.longitude - Longitude for current location. Should be used with latitude
	     * and radius params to return places available in specified radius.
	     * @param {?integer} data.radius - Search radius (distance in KM)
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * placesData
	     *  .getPlaces(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getPlaces(data) {
	      // calculate bounds based on params
	      bounds = getBounds(data);
	
	      var responses = [{
	        status: '200',
	        data: _mocks2.default.placesGet
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	
	      var response = getResponse(responses);
	
	      return Promise.resolve(response).then(parse).then(inBoundsFilter);
	    }
	
	    return {
	      getPlaces: getPlaces,
	      schemas: schemas
	    };
	  };
	}; /* global google */

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _placesGet = __webpack_require__(5);
	
	var _placesGet2 = _interopRequireDefault(_placesGet);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  placesGet: _placesGet2.default
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = [{
	  id: '32c91a0b-199e-4275-bbe9-7097af217a7a',
	  name: 'Branch 1',
	  address: {
	    addressLine1: 'Plantage Middenlaan 43',
	    postalCode: '1018 DC',
	    country: 'The Netherlands'
	  },
	  latitude: 52.366886,
	  longitude: 4.915426,
	  placeType: 'branch'
	}, {
	  id: '12c91a0b-122e-4275-bbe9-7097af217a7a',
	  name: 'Branch Noord',
	  address: {
	    addressLine1: 'Distelweg',
	    addressLine2: 'Amsterdam',
	    postalCode: '1031 EM',
	    country: 'The Netherlands'
	  },
	  latitude: 52.390953,
	  longitude: 4.909631,
	  placeType: 'branch'
	}, {
	  id: '53sffs0b-122e-4275-bbe9-7097af217a7a',
	  name: 'Branch Centre',
	  address: {
	    addressLine1: 'Prins Hendrikkade 21',
	    addressLine2: 'Amsterdam',
	    postalCode: '1012 TM',
	    country: 'The Netherlands'
	  },
	  latitude: 52.378972,
	  longitude: 4.895883,
	  placeType: 'branch'
	}, {
	  id: '283fbe06-5184-41e2-b79e-4b80173afe57',
	  name: 'Branch 2',
	  address: {
	    addressLine1: 'John Doe',
	    addressLine2: '10 10th Street',
	    addressLine3: 'Suite 325',
	    postalCode: '10013',
	    country: 'USA'
	  },
	  latitude: 40.71816,
	  longitude: -74.00085,
	  placeType: 'branch'
	}, {
	  id: 'c447b1f5-993e-4a31-b7be-c7a89f69b831',
	  name: 'Branch 3',
	  address: {
	    addressLine1: 'Ardenham Court',
	    addressLine2: 'app 6',
	    addressLine3: '1 Oxford Road',
	    postalCode: 'HP19 8HT',
	    country: 'United Kingdom'
	  },
	  latitude: 40.72847,
	  longitude: -74.0054,
	  placeType: 'branch'
	}, {
	  id: 'f7ccb9d0-1810-48e7-952b-5c8a663915f0',
	  name: 'Branch 4',
	  address: {
	    addressLine1: 'MONKEY MAN GROUP',
	    addressLine2: 'Suite 150',
	    addressLine3: 'Floor 3  120 Wandaloo ESP',
	    postalCode: '3184',
	    country: 'Australia'
	  },
	  latitude: 40.72872,
	  longitude: -74.00524,
	  placeType: 'branch'
	}, {
	  id: '6c8c9efd-a243-48b9-a81b-29ab1ea8f550',
	  name: 'Branch 5',
	  address: {
	    addressLine1: 'INIT building',
	    addressLine2: 'Jacob Bontiusplaats 9',
	    postalCode: '1018 LL',
	    country: 'The Netherlands'
	  },
	  latitude: 40.72152,
	  longitude: -73.99574,
	  placeType: 'branch'
	}, {
	  id: '0f1e2a3a-cf3e-4b18-86b4-4f1919b745bf',
	  name: 'Branch 6',
	  address: {
	    addressLine1: '195 Grand St',
	    postalCode: '10013',
	    country: 'USA'
	  },
	  latitude: 40.71909,
	  longitude: -73.9971,
	  placeType: 'branch'
	}, {
	  id: 'a956dbc7-a84d-46fe-91c3-bc40561ffdee',
	  name: 'XCEL FCU',
	  address: {
	    addressLine1: 'Jane Doe',
	    addressLine2: '26 Federal Plaza',
	    postalCode: '10278',
	    country: 'USA'
	  },
	  latitude: 40.71558,
	  longitude: -74.00417,
	  placeType: 'branch'
	}, {
	  id: 'd6cdb083-ea55-4760-9ca7-10b1ce0792fd',
	  name: 'Justice FCU',
	  address: {
	    addressLine1: '26 Federal Plaza',
	    postalCode: '10278',
	    country: 'USA'
	  },
	  latitude: 40.71558,
	  longitude: -74.00417,
	  placeType: 'branch'
	}, {
	  id: '40c154d3-344b-48c9-966d-23aaf7369c1f',
	  name: '7-ELEVEN',
	  address: {
	    addressLine1: '135 W 3rd St',
	    postalCode: '10012',
	    country: 'USA'
	  },
	  latitude: 40.73101,
	  longitude: -74.00079,
	  placeType: 'branch'
	}, {
	  id: '82cd9c3b-2c91-4631-9b25-71281179fa0d',
	  name: 'Chrystie Garage',
	  address: {
	    addressLine1: '89 Chrysie St',
	    addressLine2: 'p.o. box 213',
	    postalCode: '10002',
	    country: 'USA'
	  },
	  latitude: 40.7177,
	  longitude: -73.99442,
	  placeType: 'branch'
	}, {
	  id: '0edb7a6e-8969-493e-9787-90fe967843ce',
	  name: 'Gryfice',
	  address: {
	    addressLine1: 'Akacjowa',
	    addressLine2: 'Gryfice',
	    country: 'Poland'
	  },
	  latitude: 53.9127245,
	  longitude: 15.2098384,
	  placeType: 'branch'
	}, {
	  id: '0edb7a6e-8969-493e-9787-90fe967843ce',
	  name: 'Łochów',
	  address: {
	    addressLine1: 'Aleja Łochowska',
	    addressLine2: 'Łochów',
	    country: 'Poland'
	  },
	  latitude: 52.532436,
	  longitude: 21.686002,
	  placeType: 'branch'
	}];

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-places-http-ng.js.map