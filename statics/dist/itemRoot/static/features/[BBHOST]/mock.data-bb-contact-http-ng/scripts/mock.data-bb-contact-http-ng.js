(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-contact-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-contact-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-contact-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.contactDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbContactHttp = __webpack_require__(3);
	
	var _dataBbContactHttp2 = _interopRequireDefault(_dataBbContactHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module data-bb-contact-http-ng
	 *
	 * @description A data module for accessing the Contact REST API
	 *
	 * @returns {String} `data-bb-contact-http-ng`
	 * @example
	 * import contactDataModuleKey, {
	 *   contactDataKey,
	 * } from 'data-bb-contact-http-ng';
	 */
	
	var contactDataModuleKey = 'data-bb-contact-http-ng';
	/**
	 * @name contactDataKey
	 * @type {string}
	 * @description Angular dependency injection key for the Contact data service
	 */
	var contactDataKey = exports.contactDataKey = 'data-bb-contact-http-ng:contactData';
	/**
	 * @name default
	 * @type {string}
	 * @description Angular dependency injection module key
	 */
	exports.default = _vendorBbAngular2.default.module(contactDataModuleKey, [])
	
	/**
	 * @constructor ContactData
	 * @type {object}
	 *
	 * @description Public api for service data-bb-contact-http
	 *
	 */
	.provider(contactDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  /**
	   * @name ContactDataProvider
	   * @type {object}
	   * @description
	   * Data service that can be configured with custom base URI.
	   *
	   * @example
	   * angular.module(...)
	   *   .config(['data-bb-contact-http-ng:contactDataProvider',
	   *     (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *       });
	   */
	  return {
	    /**
	     * @name ContactDataProvider#setBaseUri
	     * @type {function}
	     * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
	     */
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	
	    /**
	     * @name ContactDataProvider#$get
	     * @type {function}
	     * @return {object} An instance of the service
	     */
	    $get: ['$q',
	    /* into */
	    (0, _dataBbContactHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _mocks = __webpack_require__(4);
	
	var _mocks2 = _interopRequireDefault(_mocks);
	
	var _searchable = __webpack_require__(8);
	
	var _searchable2 = _interopRequireDefault(_searchable);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint max-len: ["error", { "ignoreComments": true }] */
	exports.default = function () {
	  return function (Promise) {
	    /**
	     * @description
	     * Emulate delay for mocks
	     *
	     * @param milliseconds
	     * @return {Function}
	     */
	    var delay = function delay(milliseconds) {
	      return function (result) {
	        return new Promise(function (resolve) {
	          setTimeout(function () {
	            resolve(result);
	          }, milliseconds);
	        });
	      };
	    };
	
	    /**
	     * @description
	     * JSON schema data. Keys of the object are names of the POST and PUT methods listed below.
	     *
	     * @name ContactData#schemas
	     * @type {object}
	     */
	    var schemas = {};
	
	    /*
	     * @name parse
	     * @type {function}
	     * @private
	     * @description Should be overitten by transformRespone on a project level
	     */
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers
	      };
	    }
	
	    var DEFAULT_RESPONSE = {
	      data: {},
	      status: 200,
	      headers: function headers() {
	        return null;
	      },
	      config: {},
	      statusText: 'OK'
	    };
	
	    function getResponse(responses, statusCode) {
	      var response = void 0;
	      if (statusCode) {
	        response = responses.find(function (resp) {
	          return resp.status === statusCode;
	        });
	      }
	
	      if (!response) {
	        // Find the happy response.
	        response = responses.find(function (resp) {
	          return resp.status.indexOf('2') === 0;
	        });
	      }
	
	      return response ? Object.assign({}, DEFAULT_RESPONSE, response) : DEFAULT_RESPONSE;
	    }
	
	    /**
	     * @name ContactData#getContacts
	     * @type {function}
	     * @description Perform a GET request to the URI.Retrieve list of all contacts.
	     * @param {?object} data - optional configuration object
	     * @param {?integer} data.from - Skip over a page of elements by specifying a start value for the query
	     * @param {?string} data.cursor - Alternative 'from' pointer, pointing to entry instead of page.
	     * @param {?integer} data.size - Limit the number of elements on the response.
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactData
	     *  .getContacts(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getContacts(data) {
	      var rawContacts = _mocks2.default.contactsGet;
	      var searchedContacts = (0, _searchable2.default)(rawContacts, data);
	      var from = parseInt(data && data.from, 10) || 0;
	      var size = parseInt(data && data.size, 10) || 20;
	      var startIndex = from * size;
	      var endIndex = startIndex + size;
	
	      var responses = [{
	        status: '200',
	        headers: function headers(header) {
	          switch (header) {
	            case 'x-total-count':
	              return searchedContacts.length;
	            default:
	              return null;
	          }
	        },
	        data: searchedContacts.slice(startIndex, endIndex)
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    /**
	     * @name ContactData#getApprovals
	     * @type {function}
	     * @description Perform a GET request to the URI.Retrieve list of contacts to be authorized.
	     * @param {?object} data - optional configuration object
	     * @param {?integer} data.from - Skip over a page of elements by specifying a start value for the query
	     * @param {?string} data.cursor - Alternative 'from' pointer, pointing to entry instead of page.
	     * @param {?integer} data.size - Limit the number of elements on the response.
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactData
	     *  .getApprovals(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getApprovals(data) {
	      var rawContacts = _mocks2.default.authorizationsGet || [];
	      var from = parseInt(data && data.from, 10) || 0;
	      var size = parseInt(data && data.size, 10) || 20;
	      var startIndex = from * size;
	      var endIndex = startIndex + size;
	
	      var responses = [{
	        status: '200',
	        headers: function headers(header) {
	          switch (header) {
	            case 'x-total-count':
	              return rawContacts.length;
	            default:
	              return null;
	          }
	        },
	        data: rawContacts.slice(startIndex, endIndex)
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    /**
	     * @name ContactsData#postApprovalsApprovalRecordsRecord
	     * @type {function}
	     * @description Perform a POST request to the URI.Authorize contact.
	     * @param {string} contactID
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactsData
	     *  .postApprovalsApprovalRecordsRecord(contactID, data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function postApprovalsApprovalRecordsRecord(contactID) {
	      var responses = [{
	        status: '204'
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	
	      var contacts = _mocks2.default.authorizationsGet;
	      var contactIndex = contacts.findIndex(function (cont) {
	        return cont.id === contactID;
	      });
	
	      var contact = contacts[contactIndex];
	      var statusCode = !contact || Math.random() < 0.2 ? '400' : '204';
	      var response = getResponse(responses, statusCode);
	
	      return Promise.resolve(response).then(function (res) {
	        if (res.status === '204') {
	          // Remove contact from authorization collection
	          contacts.splice(contactIndex, 1);
	        } else {
	          throw new Error('Unexpected error');
	        }
	
	        return res;
	      }).then(parse);
	    }
	
	    /**
	     * @name ContactsData#postApprovalsRejectionRecordsRecord
	     * @type {function}
	     * @description Perform a POST request to the URI.Reject contact.
	     * @param {string} contactID
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactsData
	     *  .postApprovalsRejectionRecordsRecord(contactID, data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function postApprovalsRejectionRecordsRecord(contactID) {
	      var responses = [{
	        status: '204'
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	
	      var contacts = _mocks2.default.authorizationsGet;
	      var contactIndex = contacts.findIndex(function (cont) {
	        return cont.id === contactID;
	      });
	
	      var contact = contacts[contactIndex];
	      var statusCode = !contact || Math.random() < 0.2 ? '400' : '204';
	      var response = getResponse(responses, statusCode);
	
	      return Promise.resolve(response).then(function (res) {
	        if (res.status === '204') {
	          // Remove contact from authorization collection
	          contacts.splice(contactIndex, 1);
	        } else {
	          throw new Error('Unexpected error');
	        }
	
	        return res;
	      }).then(parse);
	    }
	
	    var isChallengeRequest = function isChallengeRequest(data) {
	      return data.name.toLowerCase().trim() === 'step up';
	    };
	    var isAuthorizedChallengeRequest = function isAuthorizedChallengeRequest(headers) {
	      var REQUEST_CHALLENGE_ATTR = 'X-MFA';
	      return headers && REQUEST_CHALLENGE_ATTR in headers && headers[REQUEST_CHALLENGE_ATTR] === 'sms challenge="12345"';
	    };
	
	    var getChallengeStatusCode = function getChallengeStatusCode(headers, data) {
	      var status = void 0;
	
	      if (isChallengeRequest(data)) {
	        if (isAuthorizedChallengeRequest(headers)) {
	          status = '201';
	        } else {
	          status = '401';
	        }
	      } else {
	        status = '201';
	      }
	      return status;
	    };
	
	    var challengeResponses = function challengeResponses(responses, data) {
	      if (isChallengeRequest(data)) {
	        Object.assign(responses.find(function (response) {
	          return response.status === '401';
	        }), {
	          headers: function headers(header) {
	            switch (header) {
	              case 'WWW-Authenticate':
	                return 'sms challenge="", pki challenge="Z8nlwZe0daUNWCWIbfJe3"';
	              default:
	                return '';
	            }
	          }
	        });
	      }
	
	      return responses;
	    };
	
	    /**
	     * @name ContactData#postContactsRecord
	     * @type {function}
	     * @description Perform a POST request to the URI.Create a new contact
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactData
	     *  .postContactsRecord(data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function postContactsRecord(data, headers) {
	      var responses = [{
	        status: '201',
	        data: {
	          id: '0955e686-d31e-4216-b3dd-5d66161d536d'
	        }
	      }, {
	        status: '401',
	        data: {
	          message: 'Unathorized'
	        }
	      }];
	      var response = getResponse(challengeResponses(responses, data), getChallengeStatusCode(headers, data));
	
	      return new Promise(function (resolved, rejected) {
	        if (response.status === '401') {
	          rejected(response);
	        } else {
	          var randId = Math.floor(Math.random() * 100);
	          var contact = Object.assign(data, {
	            id: '0855e686-d31e-4216-b3dd-5d66161d536d' + randId
	          });
	          _mocks2.default.contactsGet.push(contact);
	          resolved(parse(response));
	        }
	      });
	    }
	
	    schemas.postContactsRecord = {
	      $schema: 'http://json-schema.org/draft-04/schema#',
	      type: 'object',
	      description: 'Contact',
	      properties: {
	        name: {
	          type: 'string',
	          description: 'The name of a contact',
	          maxLength: 70,
	          required: true
	        },
	        alias: {
	          type: 'string',
	          description: 'Name will be displayed if alias field is not filled',
	          maxLength: 70
	        },
	        category: {
	          type: 'string',
	          description: 'Category to which the contact belongs to like employee, supplier.'
	        },
	        contactPerson: {
	          type: 'string',
	          description: 'Contact person of the contact',
	          maxLength: 70
	        },
	        phoneNumber: {
	          type: 'string',
	          description: 'Contact phone number of the contact'
	        },
	        emailId: {
	          type: 'string',
	          description: 'Email-id of the contact',
	          format: 'email'
	        },
	        addressLine1: {
	          type: 'string',
	          description: 'Address line 1'
	        },
	        addressLine2: {
	          type: 'string',
	          description: 'Address line 2'
	        },
	        addressLine3: {
	          type: 'string',
	          description: 'Address line 3'
	        },
	        addressLine4: {
	          type: 'string',
	          description: 'Address line 4'
	        },
	        country: {
	          type: 'string',
	          description: 'ISO Country Code',
	          minLength: 2,
	          maxLength: 2
	        },
	        accounts: {
	          type: 'array',
	          items: {
	            type: 'object',
	            properties: {
	              name: {
	                type: 'string',
	                description: 'The name of an account',
	                maxLength: 70
	              },
	              alias: {
	                type: 'string',
	                description: 'Name will be displayed if alias field is not filled',
	                maxLength: 70
	              },
	              accountNumber: {
	                type: 'string',
	                description: 'Either of Account Number or IBAN is mandatory'
	              },
	              IBAN: {
	                type: 'string',
	                description: 'Either of Account Number or IBAN is mandatory'
	              },
	              BIC: {
	                type: 'string'
	              },
	              bankCode: {
	                type: 'string'
	              },
	              bankName: {
	                type: 'string'
	              },
	              bankAddressLine1: {
	                type: 'string'
	              },
	              bankAddressLine2: {
	                type: 'string'
	              },
	              bankAddressLine3: {
	                type: 'string'
	              },
	              bankAddressLine4: {
	                type: 'string'
	              },
	              bankCountry: {
	                type: 'string',
	                description: 'ISO Country Code',
	                minLength: 2,
	                maxLength: 2
	              }
	            }
	          },
	          minItems: 1
	        }
	      },
	      required: ['name', 'accounts']
	    };
	
	    /**
	     * @name ContactData#getContactsRecord
	     * @type {function}
	     * @description Perform a GET request to the URI.Get a single contact by ID
	     * @param {string} contactID
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactData
	     *  .getContactsRecord(contactID, data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function getContactsRecord() {
	      var responses = [{
	        status: '200',
	        data: {
	          id: '0855e686-d31e-4216-b3dd-5d66161d536d',
	          name: 'John Doe',
	          alias: 'John',
	          category: 'Employee',
	          contactPerson: 'Jane Doe',
	          phoneNumber: '055512345678',
	          emailId: 'john@example.com',
	          addressLine1: 'Jacob Bontiusplaats 9',
	          addressLine2: '1018 LL',
	          addressLine3: 'Amsterdam',
	          addressLine4: 'the Netherlands',
	          country: 'NL',
	          accounts: [{
	            name: 'Saving account',
	            alias: 'Savings',
	            IBAN: 'FI21 1234 5600 0007 85',
	            bankName: 'Test Bank',
	            bankAddressLine1: 'Jodenbreestraat 96',
	            bankAddressLine2: '1011NS',
	            bankAddressLine3: 'Amsterdam',
	            bankAddressLine4: 'the Netherlands',
	            bankCountry: 'NL'
	          }]
	        }
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(parse);
	    }
	
	    /**
	     * @name ContactData#putContactsRecord
	     * @type {function}
	     * @description Perform a PUT request to the URI.Update a single contact by ID
	     * @param {string} contactID
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactData
	     *  .putContactsRecord(contactID, data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function putContactsRecord(id, data, headers) {
	      var responses = [{
	        status: '202',
	        data: {
	          id: '0855e686-d31e-4216-b3dd-5d66161d536d',
	          name: 'John Doe',
	          alias: 'John',
	          category: 'Employee',
	          contactPerson: 'Jane Doe',
	          phoneNumber: '055512345678',
	          emailId: 'john@example.com',
	          addressLine1: 'Jacob Bontiusplaats 9',
	          addressLine2: '1018 LL',
	          addressLine3: 'Amsterdam',
	          addressLine4: 'the Netherlands',
	          country: 'NL',
	          accounts: [{
	            name: 'Saving account',
	            alias: 'Savings',
	            IBAN: 'FI21 1234 5600 0007 85',
	            bankName: 'Test Bank',
	            bankAddressLine1: 'Jodenbreestraat 96',
	            bankAddressLine2: '1011NS',
	            bankAddressLine3: 'Amsterdam',
	            bankAddressLine4: 'the Netherlands',
	            bankCountry: 'NL'
	          }]
	        }
	      }, {
	        status: '401',
	        data: {
	          message: 'Unauthorized'
	        }
	      }];
	
	      var response = getResponse(challengeResponses(responses, data), getChallengeStatusCode(headers, data));
	
	      return new Promise(function (resolved, rejected) {
	        if (response.status === '401') {
	          rejected(response);
	        } else {
	          Object.assign(_mocks2.default.contactsGet.find(function (contact) {
	            return contact.id === id;
	          }), data);
	          resolved(parse(response));
	        }
	      });
	    }
	
	    schemas.putContactsRecord = {
	      $schema: 'http://json-schema.org/draft-04/schema#',
	      type: 'object',
	      description: 'Contact',
	      properties: {
	        name: {
	          type: 'string',
	          description: 'The name of a contact',
	          maxLength: 70,
	          required: true
	        },
	        alias: {
	          type: 'string',
	          description: 'Name will be displayed if alias field is not filled',
	          maxLength: 70
	        },
	        category: {
	          type: 'string',
	          description: 'Category to which the contact belongs to like employee, supplier.'
	        },
	        contactPerson: {
	          type: 'string',
	          description: 'Contact person of the contact',
	          maxLength: 70
	        },
	        phoneNumber: {
	          type: 'string',
	          description: 'Contact phone number of the contact'
	        },
	        emailId: {
	          type: 'string',
	          description: 'Email-id of the contact',
	          format: 'email'
	        },
	        addressLine1: {
	          type: 'string',
	          description: 'Address line 1'
	        },
	        addressLine2: {
	          type: 'string',
	          description: 'Address line 2'
	        },
	        addressLine3: {
	          type: 'string',
	          description: 'Address line 3'
	        },
	        addressLine4: {
	          type: 'string',
	          description: 'Address line 4'
	        },
	        country: {
	          type: 'string',
	          description: 'ISO Country Code',
	          minLength: 2,
	          maxLength: 2
	        },
	        accounts: {
	          type: 'array',
	          items: {
	            type: 'object',
	            properties: {
	              name: {
	                type: 'string',
	                description: 'The name of an account',
	                maxLength: 70
	              },
	              alias: {
	                type: 'string',
	                description: 'Name will be displayed if alias field is not filled',
	                maxLength: 70
	              },
	              accountNumber: {
	                type: 'string',
	                description: 'Either of Account Number or IBAN is mandatory'
	              },
	              IBAN: {
	                type: 'string',
	                description: 'Either of Account Number or IBAN is mandatory'
	              },
	              BIC: {
	                type: 'string'
	              },
	              bankCode: {
	                type: 'string'
	              },
	              bankName: {
	                type: 'string'
	              },
	              bankAddressLine1: {
	                type: 'string'
	              },
	              bankAddressLine2: {
	                type: 'string'
	              },
	              bankAddressLine3: {
	                type: 'string'
	              },
	              bankAddressLine4: {
	                type: 'string'
	              },
	              bankCountry: {
	                type: 'string',
	                description: 'ISO Country Code',
	                minLength: 2,
	                maxLength: 2
	              }
	            }
	          },
	          minItems: 1
	        }
	      },
	      required: ['name', 'accounts']
	    };
	
	    /**
	     * @name ContactData#deleteContactsRecord
	     * @type {function}
	     * @description Perform a DELETE request to the URI.Delete a single contact by ID
	     * @param {string} contactID
	     * @param {?object} data - optional configuration object
	     * @returns {Promise.<object>} A promise resolving to object with headers and data keys
	     *
	     * @example
	     * contactData
	     *  .deleteContactsRecord(contactID, data)
	     *  .then(function(result){
	     *    console.log(result)
	     *  });
	     */
	    function deleteContactsRecord() {
	      var responses = [{
	        status: '202'
	      }, {
	        status: '422',
	        data: {
	          message: 'Entity could not be processed'
	        }
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(parse);
	    }
	
	    function getApprovalsMe(data) {
	      var rawContacts = _mocks2.default.requestsGet || [];
	      var from = parseInt(data && data.from, 10) || 0;
	      var size = parseInt(data && data.size, 10) || 20;
	      var startIndex = from * size;
	      var endIndex = startIndex + size;
	
	      var responses = [{
	        status: '200',
	        headers: function headers(header) {
	          switch (header) {
	            case 'x-total-count':
	              return rawContacts.length;
	            default:
	              return null;
	          }
	        },
	        data: rawContacts.slice(startIndex, endIndex)
	      }, {
	        status: '400'
	      }, {
	        status: '500'
	      }];
	      var response = getResponse(responses);
	      return Promise.resolve(response).then(delay(1000)).then(parse);
	    }
	
	    return {
	      getContacts: getContacts,
	      postContactsRecord: postContactsRecord,
	
	      getApprovals: getApprovals,
	
	      getContactsRecord: getContactsRecord,
	      putContactsRecord: putContactsRecord,
	      deleteContactsRecord: deleteContactsRecord,
	
	      postApprovalsApprovalRecordsRecord: postApprovalsApprovalRecordsRecord,
	
	      postApprovalsRejectionRecordsRecord: postApprovalsRejectionRecordsRecord,
	
	      getApprovalsMe: getApprovalsMe,
	
	      schemas: schemas
	    };
	  };
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _contactsGet = __webpack_require__(5);
	
	var _contactsGet2 = _interopRequireDefault(_contactsGet);
	
	var _authorizationsGet = __webpack_require__(6);
	
	var _authorizationsGet2 = _interopRequireDefault(_authorizationsGet);
	
	var _requestsGet = __webpack_require__(7);
	
	var _requestsGet2 = _interopRequireDefault(_requestsGet);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  contactsGet: _contactsGet2.default,
	  authorizationsGet: _authorizationsGet2.default,
	  requestsGet: _requestsGet2.default
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/* eslint-disable */
	exports.default = [{
	  "id": "0855e686-d31e-4216-b3dd-5d66161d536d",
	  "name": "John Doe",
	  "alias": "John",
	  "category": "Employee",
	  "contactPerson": "Jane Doe",
	  "phoneNumber": "055512345678",
	  "emailId": "john@example.com",
	  "addressLine1": "Jacob Bontiusplaats 9",
	  "addressLine2": "1018 LL",
	  "addressLine3": "Amsterdam",
	  "addressLine4": "the Netherlands",
	  "country": "NL",
	  "accounts": [{
	    "name": "Saving account",
	    "alias": "Savings",
	    "IBAN": "FI2112345600000785",
	    "bankName": "Test Bank",
	    "bankAddressLine1": "Jodenbreestraat 96",
	    "bankAddressLine2": "1011NS",
	    "bankAddressLine3": "Amsterdam",
	    "bankAddressLine4": "the Netherlands",
	    "bankCountry": "NL"
	  }, {
	    "name": "Debit account",
	    "alias": "Debit",
	    "IBAN": "FI21 1234 5600 0007 86",
	    "bankName": "Test Bank",
	    "bankAddressLine1": "Jodenbreestraat 96",
	    "bankAddressLine2": "1011NS",
	    "bankAddressLine3": "Amsterdam",
	    "bankAddressLine4": "the Netherlands",
	    "bankCountry": "NL"
	  }]
	}, {
	  "id": "0855e686-d31e-4216-b3dd-5d66161d536e",
	  "name": "Jane Doe",
	  "alias": "Jane",
	  "category": "Manager",
	  "contactPerson": "John Doe",
	  "phoneNumber": "055512345678",
	  "emailId": "jane@example.com",
	  "addressLine1": "Jacob Bontiusplaats 9",
	  "addressLine2": "1018 LL",
	  "addressLine3": "Amsterdam",
	  "addressLine4": "the Netherlands",
	  "country": "NL",
	  "accounts": [{
	    "name": "Saving account",
	    "alias": "Savings",
	    "IBAN": "FI21 1234 5600 0007 85",
	    "bankName": "Test Bank",
	    "bankAddressLine1": "Jodenbreestraat 96",
	    "bankAddressLine2": "1011NS",
	    "bankAddressLine3": "Amsterdam",
	    "bankAddressLine4": "the Netherlands",
	    "bankCountry": "NL"
	  }]
	}];

/***/ },
/* 6 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/* eslint-disable */
	exports.default = [{
	  "id": 101,
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-02-15T09:30:10Z",
	  "action": "CREATE",
	  "status": "PENDING",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d536d",
	    "name": "Augustus Pollak",
	    "alias": "Augustus",
	    "category": "Employee",
	    "contactPerson": "Jane Doe",
	    "phoneNumber": "055512345777",
	    "emailId": "augustus@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 102,
	  "creatorBbId": "john.doe@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "action": "CREATE",
	  "status": "PENDING",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d5621",
	    "name": "Brad Nelson",
	    "alias": "Brad",
	    "category": "Employee",
	    "contactPerson": "Jane Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "brad.nelson@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 103,
	  "action": "DELETE",
	  "status": "PENDING",
	  "creatorBbId": "alex.nord@example",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d5361",
	    "name": "John Smith",
	    "alias": "Johny",
	    "category": "Manager",
	    "contactPerson": "John Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "john.smith@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 104,
	  "action": "UPDATE",
	  "status": "PENDING",
	  "creatorBbId": "alex.nood@example.com",
	  "creationTime": "2017-03-01T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d536e",
	    "name": "Nina Smith",
	    "alias": "Jane",
	    "category": "Manager",
	    "contactPerson": "John Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "jane@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 105,
	  "action": "UPDATE",
	  "status": "PENDING",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d536f",
	    "name": "Nora Algol",
	    "alias": "Nora",
	    "category": "Employee",
	    "contactPerson": "John Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "jane@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}];

/***/ },
/* 7 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/* eslint-disable */
	exports.default = [{
	  "id": 201,
	  "status": "REJECTED",
	  "action": "DELETE",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d5321",
	    "name": "Alexander Hayne",
	    "alias": "Alex",
	    "category": "HR",
	    "contactPerson": "Jane Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "a.hayne@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 202,
	  "status": "PENDING",
	  "action": "CREATE",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-02-15T09:30:10Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d536d",
	    "name": "Augustus Pollak",
	    "alias": "Augustus",
	    "category": "Employee",
	    "contactPerson": "Jane Doe",
	    "phoneNumber": "055512345777",
	    "emailId": "augustus@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 203,
	  "status": "PENDING",
	  "action": "CREATE",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d5621",
	    "name": "Brad Nelson",
	    "alias": "Brad",
	    "category": "Employee",
	    "contactPerson": "Jane Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "brad.nelson@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 204,
	  "status": "REJECTED",
	  "action": "UPDATE",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d1621",
	    "name": "Jadine Komplarens",
	    "alias": "Jadine",
	    "category": "Employee",
	    "contactPerson": "Jane Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "jad.komplares@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 205,
	  "status": "PENDING",
	  "action": "DELETE",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d5361",
	    "name": "John Smith",
	    "alias": "Johny",
	    "category": "Manager",
	    "contactPerson": "John Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "john.smith@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": "206",
	  "status": "PENDING",
	  "action": "UPDATE",
	  "creatorBbId": "alex.nood@example.org",
	  "creationTime": "2017-03-01T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d536e",
	    "name": "Nina Smith",
	    "alias": "Jane",
	    "category": "Manager",
	    "contactPerson": "John Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "jane@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}, {
	  "id": 208,
	  "status": "PENDING",
	  "action": "UPDATE",
	  "creatorBbId": "alex.nord@example.com",
	  "creationTime": "2017-03-02T11:30:00Z",
	  "data": {
	    "id": "0855e686-d31e-4216-b3dd-5d66161d536f",
	    "name": "Nora Algol",
	    "alias": "Nora",
	    "category": "Employee",
	    "contactPerson": "John Doe",
	    "phoneNumber": "055512345678",
	    "emailId": "jane@example.com",
	    "addressLine1": "Jacob Bontiusplaats 9",
	    "addressLine2": "1018 LL",
	    "addressLine3": "Amsterdam",
	    "addressLine4": "the Netherlands",
	    "country": "NL",
	    "accounts": [{
	      "name": "Saving account",
	      "alias": "Savings",
	      "IBAN": "FI2112345600000785",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }, {
	      "name": "Debit account",
	      "alias": "Debit",
	      "IBAN": "FI2112345600000786",
	      "bankName": "Test Bank",
	      "bankAddressLine1": "Jodenbreestraat 96",
	      "bankAddressLine2": "1011NS",
	      "bankAddressLine3": "Amsterdam",
	      "bankAddressLine4": "the Netherlands",
	      "bankCountry": "NL"
	    }],
	    "accessContextScope": "USER"
	  }
	}];

/***/ },
/* 8 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	// Helper: search collection
	var lookupLike = function lookupLike(collection, keysArr, query) {
	  return collection.filter(function (item) {
	    var found = false;
	
	    keysArr.forEach(function (keyItem) {
	      if (item[keyItem] && item[keyItem].toLowerCase().indexOf(query.toLowerCase()) !== -1) {
	        found = true;
	      }
	    });
	
	    return found;
	  });
	};
	
	// Search collection by list of keys with query
	var searchable = function searchable(collection, data) {
	  var query = data && data.query || false;
	  var lookupBy = function lookupBy(keys) {
	    return lookupLike(collection, keys, query);
	  };
	
	  // No search query provided
	  if (!query) {
	    return collection;
	  }
	
	  // Search
	  return lookupBy(['name', 'alias']);
	};
	
	exports.default = searchable;

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-contact-http-ng.js.map