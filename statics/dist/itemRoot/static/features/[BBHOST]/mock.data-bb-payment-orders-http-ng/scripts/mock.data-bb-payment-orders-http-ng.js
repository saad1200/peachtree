(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-payment-orders-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-payment-orders-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-payment-orders-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.paymentOrdersDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbPaymentOrdersHttp = __webpack_require__(3);
	
	var _dataBbPaymentOrdersHttp2 = _interopRequireDefault(_dataBbPaymentOrdersHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint-disable */
	var paymentOrdersDataModuleKey = 'data-bb-payment-orders-http-ng';
	
	var paymentOrdersDataKey = exports.paymentOrdersDataKey = 'data-bb-payment-orders-http-ng:paymentOrdersData';
	
	exports.default = _vendorBbAngular2.default.module(paymentOrdersDataModuleKey, []).provider(paymentOrdersDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbPaymentOrdersHttp2.default)(config)]
	  };
	}]).name;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	/* eslint-disable */
	exports.default = function (conf) {
	  return function (Promise) {
	    var _plugins;
	
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    var state = {
	      "{version}/payment-orders": [{
	        "id": "7d34169c-6714-11e7-907b-a6006ad3dba0",
	        "status": "ENTERED",
	        "debtorAccount": {
	          "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc",
	          "identification": {
	            "identification": "NL32RBOS0608833921"
	          }
	        },
	        "debtor": {
	          "name": "Credit Account",
	          "postalAddress": {
	            "addressLine": "Jacob Bontiusplaats 9, 1018LL, Amsterdam"
	          }
	        },
	        "instructionPriority": "HIGH",
	        "requestedExecutionDate": "2017-08-16T09:31:10Z",
	        "paymentMode": "SINGLE",
	        "creditTransferTransactionInformation": [{
	          "endToEndIdentification": "Some reference",
	          "instructedAmount": {
	            "amount": "749.99",
	            "currencyCode": "EUR"
	          },
	          "creditorAccount": {
	            "identification": {
	              "identification": "MR1300020001010000123456753",
	              "schemeName": "IBAN"
	            }
	          },
	          "creditor": {
	            "name": "John Smith"
	          }
	        }]
	      }, {
	        "id": "7d341a2a-6714-11e7-907b-a6006ad3dba0",
	        "status": "PROCESSED",
	        "bankStatus": "FINISHED",
	        "debtorAccount": {
	          "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc",
	          "identification": {
	            "identification": "NL18INGB0664923638",
	            "schemeName": "IBAN"
	          },
	          "name": "Credit Account"
	        },
	        "debtor": {
	          "name": "Random Account",
	          "postalAddress": {
	            "country": "NL"
	          }
	        },
	        "batchBooking": true,
	        "instructionPriority": "NORM",
	        "requestedExecutionDate": "2017-08-11T09:30:10Z",
	        "paymentMode": "RECURRING",
	        "schedule": {
	          "transferFrequency": "MONTHLY",
	          "on": 1,
	          "startDate": "2017-08-12T10:01:22Z",
	          "every": 1,
	          "nextExecutionDate": "2017-08-15T11:23:01Z"
	        },
	        "creditTransferTransactionInformation": [{
	          "instructedAmount": {
	            "amount": "100.00",
	            "currencyCode": "EUR"
	          },
	          "creditorAccount": {
	            "identification": {
	              "identification": "SK7463897688561500690656",
	              "schemeName": "IBAN"
	            }
	          },
	          "creditor": {
	            "name": "Jack Yellow"
	          },
	          "remittanceInformation": {
	            "unstructured": "Salary"
	          }
	        }]
	      }, {
	        "id": "7d341c28-6714-11e7-907b-a6006ad3dba0",
	        "status": "REJECTED",
	        "bankStatus": "ACCOUNT_BLOCKED",
	        "reasonCode": "AB01",
	        "reasonText": "Account blocked",
	        "errorDescription": "Blocked by bank.",
	        "debtorAccount": {
	          "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc",
	          "identification": {
	            "identification": "NL53RABO0309349755",
	            "schemeName": "IBAN"
	          },
	          "name": "Unidentified Account"
	        },
	        "instructionPriority": "NORM",
	        "requestedExecutionDate": "2017-07-16T19:20:10Z",
	        "creditTransferTransactionInformation": [{
	          "instructedAmount": {
	            "amount": "5000.55",
	            "currencyCode": "EUR"
	          },
	          "creditorAccount": {
	            "identification": {
	              "identification": "FR708933019952AUNHQNQ0KZ"
	            },
	            "name": "ABN Amro"
	          },
	          "creditor": {
	            "name": "Backbase",
	            "postalAddress": {
	              "addressLine": "Jacob Bontiusplaats 9, 1018LL, Amsterdam",
	              "country": "NL"
	            }
	          },
	          "remittanceInformation": {
	            "unstructured": "Return a debt"
	          }
	        }]
	      }, {
	        "id": "7d34215a-6714-11e7-907b-a6006ad3dba0",
	        "status": "ACCEPTED",
	        "bankStatus": "ACCEPTEDTECHNICALVALIDATION",
	        "reasonCode": "AC01",
	        "reasonText": "Accepted by bank",
	        "debtorAccount": {
	          "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc",
	          "identification": {
	            "identification": "NL24ABNA0531787108",
	            "schemeName": "IBAN"
	          }
	        },
	        "batchBooking": false,
	        "requestedExecutionDate": "2017-07-23T03:30:10Z",
	        "paymentMode": "SINGLE",
	        "creditTransferTransactionInformation": [{
	          "endToEndIdentification": "Some reference",
	          "instructedAmount": {
	            "amount": "749.99",
	            "currencyCode": "EUR"
	          },
	          "creditorAccount": {
	            "identification": {
	              "identification": "NL25TRIO0253844321"
	            }
	          },
	          "creditor": {
	            "name": "Delivery Service"
	          }
	        }]
	      }],
	      "{version}/payment-orders/currencies": [{
	        "code": "EUR"
	      }, {
	        "code": "USD"
	      }, {
	        "code": "GBP"
	      }, {
	        "code": "ISK"
	      }, {
	        "code": "KWD"
	      }]
	    };
	
	    var responses = {
	
	      getPaymentOrders: [{ "status": 200, "data": [{ "id": "7d34169c-6714-11e7-907b-a6006ad3dba0", "status": "ENTERED", "debtorAccount": { "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc", "identification": { "identification": "NL32RBOS0608833921" } }, "debtor": { "name": "Credit Account", "postalAddress": { "addressLine": "Jacob Bontiusplaats 9, 1018LL, Amsterdam" } }, "instructionPriority": "HIGH", "requestedExecutionDate": "2017-08-16T09:31:10Z", "paymentMode": "SINGLE", "creditTransferTransactionInformation": [{ "endToEndIdentification": "Some reference", "instructedAmount": { "amount": "749.99", "currencyCode": "EUR" }, "creditorAccount": { "identification": { "identification": "MR1300020001010000123456753", "schemeName": "IBAN" } }, "creditor": { "name": "John Smith" } }] }, { "id": "7d341a2a-6714-11e7-907b-a6006ad3dba0", "status": "PROCESSED", "bankStatus": "FINISHED", "debtorAccount": { "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc", "identification": { "identification": "NL18INGB0664923638", "schemeName": "IBAN" }, "name": "Credit Account" }, "debtor": { "name": "Random Account", "postalAddress": { "country": "NL" } }, "batchBooking": true, "instructionPriority": "NORM", "requestedExecutionDate": "2017-08-11T09:30:10Z", "paymentMode": "RECURRING", "schedule": { "transferFrequency": "MONTHLY", "on": 1, "startDate": "2017-08-12T10:01:22Z", "every": 1, "nextExecutionDate": "2017-08-15T11:23:01Z" }, "creditTransferTransactionInformation": [{ "instructedAmount": { "amount": "100.00", "currencyCode": "EUR" }, "creditorAccount": { "identification": { "identification": "SK7463897688561500690656", "schemeName": "IBAN" } }, "creditor": { "name": "Jack Yellow" }, "remittanceInformation": { "unstructured": "Salary" } }] }, { "id": "7d341c28-6714-11e7-907b-a6006ad3dba0", "status": "REJECTED", "bankStatus": "ACCOUNT_BLOCKED", "reasonCode": "AB01", "reasonText": "Account blocked", "errorDescription": "Blocked by bank.", "debtorAccount": { "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc", "identification": { "identification": "NL53RABO0309349755", "schemeName": "IBAN" }, "name": "Unidentified Account" }, "instructionPriority": "NORM", "requestedExecutionDate": "2017-07-16T19:20:10Z", "creditTransferTransactionInformation": [{ "instructedAmount": { "amount": "5000.55", "currencyCode": "EUR" }, "creditorAccount": { "identification": { "identification": "FR708933019952AUNHQNQ0KZ" }, "name": "ABN Amro" }, "creditor": { "name": "Backbase", "postalAddress": { "addressLine": "Jacob Bontiusplaats 9, 1018LL, Amsterdam", "country": "NL" } }, "remittanceInformation": { "unstructured": "Return a debt" } }] }, { "id": "7d34215a-6714-11e7-907b-a6006ad3dba0", "status": "ACCEPTED", "bankStatus": "ACCEPTEDTECHNICALVALIDATION", "reasonCode": "AC01", "reasonText": "Accepted by bank", "debtorAccount": { "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc", "identification": { "identification": "NL24ABNA0531787108", "schemeName": "IBAN" } }, "batchBooking": false, "requestedExecutionDate": "2017-07-23T03:30:10Z", "paymentMode": "SINGLE", "creditTransferTransactionInformation": [{ "endToEndIdentification": "Some reference", "instructedAmount": { "amount": "749.99", "currencyCode": "EUR" }, "creditorAccount": { "identification": { "identification": "NL25TRIO0253844321" } }, "creditor": { "name": "Delivery Service" } }] }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      postPaymentOrdersRecord: [{ "status": 201, "data": { "id": "af2599ef-759a-4b78-8e67-4949055a532b", "status": "ENTERED" } }, { "status": 202, "data": { "id": "af2599ef-759a-4b78-8e67-4949055a532b", "status": "ENTERED" } }, { "status": 400, "data": null }, { "status": 403, "data": { "message": "Limits has been breached, check the report for more details", "payment": { "id": "7d341c28-6714-11e7-907b-a6006ad3dba0", "status": "PENDING", "debtorAccount": { "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc", "identification": { "identification": "NL53RABO0309349755", "schemeName": "IBAN" }, "name": "Unidentified Account" }, "instructionPriority": "NORM", "requestedExecutionDate": "2017-07-16T19:20:10Z", "creditTransferTransactionInformation": [{ "name": "Jack Jackson", "instructedAmount": { "amount": "5000.55", "currencyCode": "EUR" }, "creditorAccount": { "identification": { "identification": "FR708933019952AUNHQNQ0KZ" }, "name": "ABN Amro" }, "creditor": { "name": "Backbase", "postalAddress": { "addressLine": "Jacob Bontiusplaats 9, 1018LL, Amsterdam", "country": "NL" } }, "remittanceInformation": { "unstructured": "Return a debt" } }] }, "checkTime": "2017-01-31T12:12:12Z", "breachReport": [{ "limitedEntity": [{ "ref": "1234567-12312-123123", "type": "Service Agreement", "description": "Kuhic, Gislason and Kemmer. SERVICE AGREEMENT" }], "shadow": false, "currency": "EUR", "user-BBID": "oleksii", "breachInfo": [{ "breachType": "THRESHOLD", "timeframe": { "period": "daily", "startTime": "2017-01-31T00:00:00Z", "endTime": "2017-01-31T23:59:59Z" }, "currentConsumption": "250.0", "currentThreshold": "499.9" }, { "breachType": "CONSUMPTION", "timeframe": { "period": "monthly", "startTime": "2017-01-01T00:00:00Z", "endTime": "2017-01-31T23:59:59Z" }, "currentConsumption": "9950.0", "currentThreshold": "10000.0" }] }, { "user-BBID": "oleksii", "shadow": false, "currency": "EUR", "breachInfo": [{ "breachType": "CONSUMPTION", "timeframe": { "period": "daily", "startTime": "2017-01-31T00:00:00Z", "endTime": "2017-01-31T23:59:59Z" }, "currentConsumption": "500.0", "currentThreshold": "1000.0" }] }, { "limitedEntity": [{ "ref": "1234567-12312-123123", "type": "Function Access Group", "description": "Payments approvers" }, { "ref": "1234567-12312-123123", "type": "Function", "description": "Domestic payments" }, { "ref": "Approve", "type": "Privilege", "description": "Approve" }], "shadow": false, "currency": "EUR", "breachInfo": [{ "breachType": "THRESHOLD", "timeframe": { "period": "daily", "startTime": "2017-01-31T00:00:00Z", "endTime": "2017-01-31T23:59:59Z" }, "currentConsumption": "250.0", "currentThreshold": "499.9" }] }, { "limitedEntity": [{ "ref": "1234567-12312-123123", "type": "Legal Entity", "description": "Kuhic, Gislason and Kemmer" }], "shadow": false, "currency": "EUR", "breachInfo": [{ "breachType": "CONSUMPTION", "timeframe": { "period": "quarterly", "startTime": "2017-01-01T00:00:00Z", "endTime": "2017-03-31T23:59:59Z" }, "currentConsumption": "99950.0", "currentThreshold": "100000.0" }] }] } }, { "status": 500, "data": null }],
	
	      getPaymentOrdersRecord: [{ "status": 200, "data": { "id": "7d341c28-6714-11e7-907b-a6006ad3dba0", "status": "ACCEPTED", "bankStatus": "ACCEPTEDTECHNICALVALIDATION", "reasonCode": "AC01", "reasonText": "Accepted by bank", "debtorAccount": { "arrangementId": "729190df-a421-4937-94fd-5e1a3da132cc", "identification": { "identification": "NL53RABO0309349755", "schemeName": "IBAN" }, "name": "Unidentified Account" }, "instructionPriority": "NORM", "requestedExecutionDate": "2017-07-16T19:20:10Z", "creditTransferTransactionInformation": [{ "instructedAmount": { "amount": "5000.55", "currencyCode": "EUR" }, "creditorAccount": { "identification": { "identification": "FR708933019952AUNHQNQ0KZ" }, "name": "ABN Amro" }, "creditor": { "name": "Backbase", "postalAddress": { "addressLine": "Jacob Bontiusplaats 9, 1018LL, Amsterdam", "country": "NL" } }, "remittanceInformation": { "unstructured": "Return a debt" } }] } }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 404, "data": null }, { "status": 405, "data": null }, { "status": 500, "data": null }],
	
	      deletePaymentOrdersRecord: [{ "status": 204, "data": null }, { "status": 400, "data": null }, { "status": 403, "data": null }, { "status": 404, "data": null }, { "status": 405, "data": null }, { "status": 500, "data": null }],
	
	      putPaymentOrdersUpdateStatusRecord: [{ "status": 200, "data": { "bankReferenceId": "12312312", "status": "ACCEPTED", "bankStatus": "ACCEPTEDTECHNICALVALIDATION", "reasonCode": "AC01", "reasonText": "Accepted by bank", "reasonDescription": "Accepted by bank." } }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getPaymentOrdersCurrencies: [{ "status": 200, "data": [{ "code": "EUR" }, { "code": "USD" }, { "code": "GBP" }, { "code": "ISK" }, { "code": "KWD" }] }, { "status": 400, "data": null }, { "status": 500, "data": null }],
	
	      getPaymentOrdersRate: [{ "status": 200, "data": { "rate": 1.07 } }, { "status": 400, "data": null }, { "status": 500, "data": null }]
	
	    };
	
	    var DEFAULT_MOCK = {
	      data: {},
	      status: 200,
	      headers: function headers(header) {
	        return header === 'content-type' && this.data ? 'application/json' : null;
	      },
	      config: {},
	      statusText: 'OK'
	    };
	
	    var getResponse = function getResponse(method, status) {
	      var response = (responses[method] || []).find(function (response) {
	        return response.status === status;
	      });
	      return Object.assign({}, DEFAULT_MOCK, response);
	    };
	
	    var PLUGINS_ALL = '__all__';
	
	    var plugins = (_plugins = {}, _defineProperty(_plugins, PLUGINS_ALL, []), _defineProperty(_plugins, 'getPaymentOrders', []), _defineProperty(_plugins, 'postPaymentOrdersRecord', []), _defineProperty(_plugins, 'getPaymentOrdersRecord', []), _defineProperty(_plugins, 'deletePaymentOrdersRecord', []), _defineProperty(_plugins, 'putPaymentOrdersUpdateStatusRecord', []), _defineProperty(_plugins, 'getPaymentOrdersCurrencies', []), _defineProperty(_plugins, 'getPaymentOrdersRate', []), _plugins);
	
	    var pluginMocks = function pluginMocks(method, args, uri) {
	      var methodPlugins = plugins[method] || [];
	      var commonPlugins = plugins[PLUGINS_ALL] || [];
	      var allPlugins = methodPlugins.concat(commonPlugins);
	
	      return function (initialResult) {
	        return allPlugins.reduce(function (result, plugin) {
	          return result.then(function (nextResult) {
	            return plugin(nextResult, args, uri, method);
	          });
	        }, Promise.resolve(initialResult));
	      };
	    };
	
	    var handleError = function handleError(method) {
	      return function (error) {
	        // If error object is one of the error responses, assume it returned intentionally from one of the plugins
	        var isIntendedError = error && error.status && error.status >= 400;
	        var response = isIntendedError ? error : getResponse(method, 500);
	
	        console.log(method + ' request rejected because of ', error);
	        return Promise.reject(response);
	      };
	    };
	
	    function getPaymentOrders(params) {
	      var url = '' + baseUri + version + '/payment-orders';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getPaymentOrders', 200)).then(pluginMocks('getPaymentOrders', [params], '{version}/payment-orders')).catch(handleError('getPaymentOrders'));
	    }
	
	    function postPaymentOrdersRecord(data) {
	      var url = '' + baseUri + version + '/payment-orders';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('postPaymentOrdersRecord', 201)).then(pluginMocks('postPaymentOrdersRecord', [data], '{version}/payment-orders')).catch(handleError('postPaymentOrdersRecord'));
	    }
	
	    function getPaymentOrdersRecord(paymentOrderId, params) {
	      var url = '' + baseUri + version + '/payment-orders/' + paymentOrderId;
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getPaymentOrdersRecord', 200)).then(pluginMocks('getPaymentOrdersRecord', [paymentOrderId, params], '{version}/payment-orders/{paymentOrderId}')).catch(handleError('getPaymentOrdersRecord'));
	    }
	
	    function deletePaymentOrdersRecord(paymentOrderId, data) {
	      var url = '' + baseUri + version + '/payment-orders/' + paymentOrderId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('deletePaymentOrdersRecord', 204)).then(pluginMocks('deletePaymentOrdersRecord', [paymentOrderId, data], '{version}/payment-orders/{paymentOrderId}')).catch(handleError('deletePaymentOrdersRecord'));
	    }
	
	    function putPaymentOrdersUpdateStatusRecord(data) {
	      var url = '' + baseUri + version + '/payment-orders/update-status';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('putPaymentOrdersUpdateStatusRecord', 200)).then(pluginMocks('putPaymentOrdersUpdateStatusRecord', [data], '{version}/payment-orders/update-status')).catch(handleError('putPaymentOrdersUpdateStatusRecord'));
	    }
	
	    function getPaymentOrdersCurrencies(params) {
	      var url = '' + baseUri + version + '/payment-orders/currencies';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getPaymentOrdersCurrencies', 200)).then(pluginMocks('getPaymentOrdersCurrencies', [params], '{version}/payment-orders/currencies')).catch(handleError('getPaymentOrdersCurrencies'));
	    }
	
	    function getPaymentOrdersRate(params) {
	      var url = '' + baseUri + version + '/payment-orders/rate';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	
	      console.log('Mocking request with', mocking);
	      return Promise.resolve(getResponse('getPaymentOrdersRate', 200)).then(pluginMocks('getPaymentOrdersRate', [params], '{version}/payment-orders/rate')).catch(handleError('getPaymentOrdersRate'));
	    }
	
	    schemas.postPaymentOrdersRecord = { "properties": { "debtor": { "type": "object", "properties": { "name": { "type": "string", "maxLength": 140, "required": true }, "postalAddress": { "type": "object", "properties": { "addressLine": { "type": "string", "required": false }, "country": { "type": "string", "required": false } }, "required": false } }, "required": false }, "debtorAccount": { "type": "object", "properties": { "arrangementId": { "type": "string", "minLength": 1, "required": true } }, "required": true }, "batchBooking": { "type": "boolean", "default": false, "required": false }, "instructionPriority": { "type": "string", "enum": ["NORM", "HIGH"], "default": "NORM", "required": false }, "requestedExecutionDate": { "type": "string", "format": "date-time", "required": true }, "paymentMode": { "type": "string", "enum": ["SINGLE", "RECURRING"], "default": "SINGLE", "required": false }, "schedule": { "type": "object", "properties": { "nonWorkingDayExecutionStrategy": { "type": "string", "enum": ["BEFORE", "AFTER", "NONE"], "required": false }, "transferFrequency": { "type": "string", "enum": ["ONCE", "DAILY", "WEEKLY", "MONTHLY", "QUARTERLY", "YEARLY"], "required": true }, "on": { "type": "integer", "required": true }, "startDate": { "type": "string", "format": "date-time", "required": true }, "endDate": { "type": "string", "format": "date-time", "required": false }, "repeat": { "type": "integer", "required": false }, "every": { "type": "integer", "enum": [1, 2], "required": true }, "nextExecutionDate": { "type": "string", "format": "date-time", "required": false } }, "required": false }, "creditTransferTransactionInformation": { "type": "array", "items": { "properties": { "instructedAmount": { "type": "object", "properties": { "amount": { "type": "string", "required": true }, "currencyCode": { "type": "string", "pattern": "^[A-Z]{3}$", "required": true } }, "required": true }, "creditor": { "type": "object", "properties": { "name": { "type": "string", "maxLength": 140, "required": true }, "postalAddress": { "type": "object", "properties": { "addressLine": { "type": "string", "required": false }, "country": { "type": "string", "required": false } }, "required": false } }, "required": true }, "creditorAccount": { "type": "object", "properties": { "identification": { "type": "object", "properties": { "schemeName": { "type": "string", "maxLength": 35, "required": false }, "identification": { "type": "string", "maxLength": 34, "required": true } }, "required": true }, "name": { "type": "string", "maxLength": 140, "required": false } }, "required": true }, "remittanceInformation": { "type": "object", "properties": { "structured": { "type": "object", "properties": { "reference": { "type": "string", "required": false }, "code": { "type": "string", "required": false }, "proprietary": { "type": "string", "required": false } }, "required": false }, "unstructured": { "type": "string", "required": false } }, "required": false }, "endToEndIdentification": { "type": "string", "maxLength": 35, "required": false } } }, "minItems": 1, "required": true } } };
	
	    schemas.putPaymentOrdersUpdateStatusRecord = { "properties": { "bankReferenceId": { "type": "string", "required": true }, "status": { "type": "string", "enum": ["ENTERED", "ACCEPTED", "PROCESSED", "REJECTED"], "required": true }, "bankStatus": { "type": "string", "maxLength": 35, "required": true }, "reasonCode": { "type": "string", "maxLength": 4, "required": false }, "reasonText": { "type": "string", "maxLength": 35, "required": false }, "errorDescription": { "type": "string", "maxLength": 105, "required": false } } };
	
	    return {
	
	      getPaymentOrders: getPaymentOrders,
	
	      postPaymentOrdersRecord: postPaymentOrdersRecord,
	
	      getPaymentOrdersRecord: getPaymentOrdersRecord,
	
	      deletePaymentOrdersRecord: deletePaymentOrdersRecord,
	
	      putPaymentOrdersUpdateStatusRecord: putPaymentOrdersUpdateStatusRecord,
	
	      getPaymentOrdersCurrencies: getPaymentOrdersCurrencies,
	
	      getPaymentOrdersRate: getPaymentOrdersRate,
	
	      schemas: schemas
	    };
	  };
	};

/***/ })
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-payment-orders-http-ng.js.map