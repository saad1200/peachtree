(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-messaging-service-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-messaging-service-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-messaging-service-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.messagingServiceDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbMessagingServiceHttp = __webpack_require__(3);
	
	var _dataBbMessagingServiceHttp2 = _interopRequireDefault(_dataBbMessagingServiceHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var messagingServiceDataModuleKey = 'data-bb-messaging-service-http-ng';
	
	var messagingServiceDataKey = exports.messagingServiceDataKey = 'data-bb-messaging-service-http-ng:messagingServiceData';
	
	exports.default = _vendorBbAngular2.default.module(messagingServiceDataModuleKey, []).provider(messagingServiceDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbMessagingServiceHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (conf) {
	  return function (Promise) {
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers,
	        status: res.status,
	        statusText: res.statusText
	      };
	    }
	
	    function getMessageCenterUsersUnreadConversationCount(userId, params) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/unread-conversation-count';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "unreadCount": 654 }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getMessageCenterUsersRecipients(userId, params) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/recipients';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "addresses": ["test address", "test address2"] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getMessageCenterUsersDrafts(userId, params) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/drafts';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "drafts": [{ "id": "79754562-57f6-4cf7-81dc-ec757e4d4c04", "body": "This is a first test message", "subject": "Test", "category": "Loans", "sender": "testuser1", "recipients": ["CN=testuser2, DC=backbase, OU=TEST", "CN=testuser3, DC=backbase, OU=TEST"], "important": true, "updatedDate": "2017-01-17T13:16:21.116Z" }, { "id": "7a31a0f4-2b3f-4160-8ca4-e54b8f6df377", "body": "This is a second test message", "subject": "Test", "category": "Loans", "sender": "testuser1", "recipients": ["CN=testuser2, DC=backbase, OU=TEST"], "updatedDate": "2017-01-16T15:28:26.852Z" }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postMessageCenterUsersDraftsRecord(userId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/drafts';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "id": "79754562-57f6-4cf7-81dc-ec757e4d4c04" }, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getMessageCenterUsersConversations(userId, params) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "totalCount": 100, "conversations": [{ "id": "bacec2dd-dc04-452c-bdf0-147143757fa0", "otherUser": "CN=Steve Oliver, DC=backbase, OU=TEST", "category": "Loans", "body": "Hello, I would like to apply for a loan for my car and I need to know when I receive the money when I apply now. I have saved the application with ID #223957. Can you please check? Gr. Steve", "subject": "Date when I can receive the money", "containsUnread": false, "important": true, "numberOfMessages": 1, "timestamp": "2017-01-16T15:48:59.792Z" }, { "id": "37d07329-c2c2-4842-8625-6f447904b942", "otherUser": "CN=Peter Smith, DC=backbase, OU=TEST", "category": "Mortgage application", "body": "Dear reader,\n\nMy application for my mortgage failed. I filled it in with ID #67235456, but I got a message that my application failed. Can you check what is the problem?\n\nKind Regards,\n\nPeter", "subject": "Application for my mortgage failed", "containsUnread": false, "numberOfMessages": 1, "timestamp": "2017-01-16T15:45:47.303Z" }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function putMessageCenterUsersDraftsRecord(userId, draftId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/drafts/' + draftId;
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function deleteMessageCenterUsersDraftsRecord(userId, draftId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/drafts/' + draftId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postMessageCenterUsersDraftsSendDraftRequestRecord(userId, draftId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/drafts/' + draftId + '/send-draft-request';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function deleteMessageCenterUsersConversationsRecord(userId, conversationId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postMessageCenterUsersConversationsArchiveConversationRequestRecord(userId, conversationId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId + '/archive-conversation-request';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getMessageCenterUsersConversationsDrafts(userId, conversationId, params) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId + '/drafts';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "drafts": [{ "id": "e92bbe7d-10cf-4d7c-b7b5-eadb7632812a", "body": "Thank you for your message.", "subject": "Application for my mortgage failed", "category": "Mortgage application", "sender": "Peter Smith", "recipients": ["CN=Peter Smith, DC=backbase, OU=TEST"], "updatedDate": "2017-01-17T13:45:03.564Z" }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postMessageCenterUsersConversationsDraftsRecord(userId, conversationId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId + '/drafts';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "id": "e92bbe7d-10cf-4d7c-b7b5-eadb7632812a" }, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function getMessageCenterUsersConversationsMessages(userId, conversationId, params) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId + '/messages';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": { "messages": [{ "id": "e485c749-61f6-4d53-b6ba-a790dffaea08", "subject": "Application for my mortgage failed", "body": "Dear reader,\n\nMy application for my mortgage failed. I filled it in with ID #67235456, but I got a message that my application failed. Can you check what is the problem?\n\nKind Regards,\n\nPeter ", "category": "Mortgage application", "status": "READ", "sender": "CN=Peter, DC=backbase, OU=TEST", "deliveredOn": "2017-01-16T15:48:59.792Z", "important": false }] }, "status": 200, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function putMessageCenterUsersConversationsDraftsRecord(userId, conversationId, draftId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId + '/drafts/' + draftId;
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postMessageCenterUsersConversationsMessagesReadMessageRequestRecord(userId, conversationId, messageId, data) {
	      var url = baseUri + '/' + version + '/message-center/users/' + userId + '/conversations/' + conversationId + '/messages/' + messageId + '/read-message-request';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { "data": {}, "status": 202, "headers": null, "config": {}, "statusText": "OK" };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    schemas.postMessageCenterUsersDraftsRecord = { "properties": { "body": { "type": "string", "required": false }, "subject": { "type": "string", "required": false }, "category": { "type": "string", "required": false }, "recipients": { "type": "array", "items": { "properties": {} }, "required": false }, "important": { "type": "boolean", "required": false } } };
	
	    schemas.putMessageCenterUsersDraftsRecord = { "properties": { "body": { "type": "string", "required": false }, "subject": { "type": "string", "required": false }, "category": { "type": "string", "required": false }, "recipients": { "type": "array", "items": { "properties": {} }, "required": false }, "important": { "type": "boolean", "required": false } } };
	
	    schemas.postMessageCenterUsersDraftsSendDraftRequestRecord = { "properties": { "body": { "type": "string", "required": false } } };
	
	    schemas.postMessageCenterUsersConversationsDraftsRecord = { "properties": { "body": { "type": "string", "required": false } } };
	
	    schemas.putMessageCenterUsersConversationsDraftsRecord = { "properties": { "body": { "type": "string", "required": false } } };
	
	    return {
	
	      getMessageCenterUsersUnreadConversationCount: getMessageCenterUsersUnreadConversationCount,
	
	      getMessageCenterUsersRecipients: getMessageCenterUsersRecipients,
	
	      getMessageCenterUsersDrafts: getMessageCenterUsersDrafts,
	
	      postMessageCenterUsersDraftsRecord: postMessageCenterUsersDraftsRecord,
	
	      getMessageCenterUsersConversations: getMessageCenterUsersConversations,
	
	      putMessageCenterUsersDraftsRecord: putMessageCenterUsersDraftsRecord,
	
	      deleteMessageCenterUsersDraftsRecord: deleteMessageCenterUsersDraftsRecord,
	
	      postMessageCenterUsersDraftsSendDraftRequestRecord: postMessageCenterUsersDraftsSendDraftRequestRecord,
	
	      deleteMessageCenterUsersConversationsRecord: deleteMessageCenterUsersConversationsRecord,
	
	      postMessageCenterUsersConversationsArchiveConversationRequestRecord: postMessageCenterUsersConversationsArchiveConversationRequestRecord,
	
	      getMessageCenterUsersConversationsDrafts: getMessageCenterUsersConversationsDrafts,
	
	      postMessageCenterUsersConversationsDraftsRecord: postMessageCenterUsersConversationsDraftsRecord,
	
	      getMessageCenterUsersConversationsMessages: getMessageCenterUsersConversationsMessages,
	
	      putMessageCenterUsersConversationsDraftsRecord: putMessageCenterUsersConversationsDraftsRecord,
	
	      postMessageCenterUsersConversationsMessagesReadMessageRequestRecord: postMessageCenterUsersConversationsMessagesReadMessageRequestRecord,
	
	      schemas: schemas
	    };
	  };
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-messaging-service-http-ng.js.map