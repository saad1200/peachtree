(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("mock.data-bb-notifications-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["mock.data-bb-notifications-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["mock.data-bb-notifications-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.notificationsDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbNotificationsHttp = __webpack_require__(3);
	
	var _dataBbNotificationsHttp2 = _interopRequireDefault(_dataBbNotificationsHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var notificationsDataModuleKey = 'data-bb-notifications-http-ng';
	
	var notificationsDataKey = exports.notificationsDataKey = 'data-bb-notifications-http-ng:notificationsData';
	
	exports.default = _vendorBbAngular2.default.module(notificationsDataModuleKey, []).provider(notificationsDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  return {
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	    $get: ['$q',
	    /* into */
	    (0, _dataBbNotificationsHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _mocks = __webpack_require__(4);
	
	var _mocks2 = _interopRequireDefault(_mocks);
	
	var _pageable = __webpack_require__(8);
	
	var _pageable2 = _interopRequireDefault(_pageable);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = function (conf) {
	  return function (Promise) {
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    var schemas = {};
	
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers,
	        status: res.status,
	        statusText: res.statusText,
	        cursor: res.cursor
	      };
	    }
	
	    /**
	     * @name NotificationsData#getNotifications
	     * @type {Function}
	     *
	     * @description
	     * Get all notifications for current user -
	     * Returns data as NotificationsData.Notifications on success or
	     * NotificationsData.BadRequest|NotificationsData.Forbidden|
	     * NotificationsData.InternalServerError on error
	     *
	     * @param {?Object} params Map of query parameters.
	     * @param {?string} params.cursor As an alternative for specifying
	     *  'from' this allows to point to the record to start the selection from.
	     *  Eg: 1483006260. (defaults to null)
	     * @param {?number} params.from Skip over a page of elements
	     *  by specifying a start value for the query. Eg: 20. (defaults to 0)
	     * @param {?number} params.size Limit the number
	     *  of elements on the response. Eg: 80. (defaults to 10)
	     * @param {?string} params.fromDate Date from which
	     *  the notifications should be retrieved. Eg: 2017-02-12T14:15:12+00:00.
	     * @param {?string} params.toDate Date to which the notifications should be retrieved.
	     *  Eg: 2017-04-11T15:14:33+00:00.
	     * @param {?string} params.levels Array of severity levels notifications should be filtered by.
	     * @param {?string} params.read Fetch only read or not read notifications. (defaults to null)
	     * @param {?string} params.originTerm A sequense of symbols/words entered by user.
	     * @param {?string} params.messageTerm A sequense of symbols/words entered by user.
	     *
	     * @returns {Promise.<Response>} A promise resolving to response object
	     *
	     * @example
	     * notificationsData
	     *  .getNotifications(params)
	     *  .then(function(result){
	       *    console.log('headers', result.headers)
	       *    console.log('data', result.data);
	       *  });
	     */
	    function getNotifications(params) {
	      var url = '' + baseUri + version + '/notifications';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { data: _mocks2.default.notificationsGetResponse,
	        status: 200,
	        headers: null,
	        config: {},
	        statusText: 'OK' };
	      var pagedNotificationsData = (0, _pageable2.default)(mockResponse.data, params);
	
	      return Promise.resolve({ data: pagedNotificationsData })
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers(header) {
	            switch (header) {
	              case 'x-total-count':
	                return mockResponse.data.length;
	              case 'x-cursor':
	                return (0, _pageable.getPointer)() < mockResponse.data.length ? (0, _pageable.getPointer)() : null;
	              default:
	                return null;
	            }
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    function postNotificationsRecord(data) {
	      var url = '' + baseUri + version + '/notifications';
	      var mocking = {
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { data: _mocks2.default.notificationsPostResponse,
	        status: 201,
	        headers: null,
	        config: {},
	        statusText: 'OK' };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	     * @name NotificationsData#getNotificationsUnreadCount
	     * @type {Function}
	     *
	     * @description
	     * get request - Returns data as
	     *  NotificationsData.UnreadNotificationsCount on success or
	     *  NotificationsData.BadRequest|NotificationsData.Forbidden|
	     *  NotificationsData.InternalServerError on error
	     *
	     * @param {Object} params Map of query parameters.
	     *
	     * @returns {Promise.<Response>} A promise resolving to response object
	     *
	     * @example
	     * notificationsData
	     *  .getNotificationsUnreadCount(params)
	     *  .then(function(result){
	       *    console.log('headers', result.headers)
	       *    console.log('data', result.data);
	       *  });
	     */
	    function getNotificationsUnreadCount(params) {
	      var url = '' + baseUri + version + '/notifications/unread-count';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { data: { unread: 123 },
	        status: 200,
	        headers: null,
	        config: {},
	        statusText: 'OK' };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	     * @name NotificationsData#getNotificationsStream
	     * @type {Function}
	     *
	     * @description
	     * Retrieve latest unread notifications for current user -
	     *  Returns data as NotificationsData.NotificationsStream on
	     *  success or NotificationsData.BadRequest|NotificationsData.Forbidden|
	     *  NotificationsData.InternalServerError on error
	     *
	     * @param {?Object} params Map of query parameters.
	     * @param {?number} params.interval Age of notifications that will be
	     *  retrieved from stream (milliseconds). Eg: 15000. (defaults to 30000)
	     *
	     * @returns {Promise.<Response>} A promise resolving to response object
	     *
	     * @example
	     * notificationsData
	     *  .getNotificationsStream(params)
	     *  .then(function(result){
	       *    console.log('headers', result.headers)
	       *    console.log('data', result.data);
	       *  });
	     */
	    function getNotificationsStream(params) {
	      var url = '' + baseUri + version + '/notifications/stream';
	      var mocking = {
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { data: _mocks2.default.notificationsStreamGetResponse,
	        status: 200,
	        headers: null,
	        config: {},
	        statusText: 'OK' };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	     * @name NotificationsData#deleteNotificationsRecord
	     * @type {Function}
	     *
	     * @description
	     * Delete the notification with the specified id - Returns data as
	     *  NotificationsData.Forbidden|NotificationsData.NotFound|
	     *  NotificationsData.InternalServerError on error
	     *
	     * @param {string} id
	     * @param {?Object} data Data to be sent as the request message data.
	     *
	     * @returns {Promise.<Response>} A promise resolving to response object
	     *
	     * @example
	     * notificationsData
	     *  .deleteNotificationsRecord(id, data)
	     *  .then(function(result){
	       *    console.log('headers', result.headers)
	       *    console.log('data', result.data);
	       *  });
	     */
	    function deleteNotificationsRecord(id, data) {
	      var url = '' + baseUri + version + '/notifications/' + id;
	      var mocking = {
	        method: 'DELETE',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { data: {}, status: 200, headers: null, config: {}, statusText: 'OK' };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	     * @name NotificationsData#putNotificationsReadRecord
	     * @type {Function}
	     *
	     * @description Mark notification as read/unread - Returns data as
	     * NotificationsData.BadRequest|NotificationsData.Forbidden|NotificationsData.NotFound|
	     * NotificationsData.UnprocessableEntity|NotificationsData.InternalServerError on error
	     *
	     * @param {string} id
	     * @param {NotificationsData.ChangeAcknowledgementCommand} data
	     *  Data to be sent as the request message data.
	     *
	     * @returns {Promise.<Response>} A promise resolving to response object
	     *
	     * @example
	     * notificationsData
	     *  .putNotificationsReadRecord(id, data)
	     *  .then(function(result){
	       *    console.log('headers', result.headers)
	       *    console.log('data', result.data);
	       *  });
	     */
	    function putNotificationsReadRecord(id, data) {
	      var url = '' + baseUri + version + '/notifications/' + id + '/read';
	      var mocking = {
	        method: 'PUT',
	        url: url,
	
	        data: data
	
	      };
	      console.log('Mocking request with', mocking);
	      var mockResponse = { data: {}, status: 200, headers: null, config: {}, statusText: 'OK' };
	      return Promise.resolve(mockResponse)
	      // ng headers is a function
	      .then(function (mock) {
	        return Object.assign({}, mock, { headers: function headers() {
	            return mock.headers;
	          } });
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    schemas.postNotificationsRecord = { properties: {
	        recipients: {
	          type: 'array',
	          items: {
	            properties: {
	              userId: {
	                type: 'string',
	                minLength: 1,
	                required: true } } },
	          required: false },
	        legalEntities: {
	          type: 'array',
	          items: {
	            properties: {
	              leId: {
	                type: 'string',
	                minLength: 1,
	                required: true } } },
	          required: false },
	        title: {
	          type: 'string',
	          required: false },
	        message: {
	          type: 'string',
	          minLength: 1,
	          required: true },
	        level: {
	          type: 'string',
	          enum: ['ALERT', 'WARNING', 'SUCCESS', 'INFO'],
	          required: true },
	        targetGroup: {
	          type: 'string',
	          enum: ['GLOBAL', 'CUSTOMER', 'USER'],
	          required: true },
	        link: { type: 'string', required: false },
	        validFrom: { type: 'string', format: 'date-time', required: false },
	        expiresOn: { type: 'string', format: 'date-time', required: false },
	        origin: { type: 'string', minLength: 1, required: true } } };
	
	    schemas.putNotificationsReadRecord = { properties: {
	        read: { type: 'boolean', required: true } } };
	
	    /**
	     * @typedef Response
	     * @type {Object}
	     *
	     * @property {Object} data See method descriptions for possible return types
	     * @property {Function} headers Getter headers function
	     */
	    return {
	
	      getNotifications: getNotifications,
	
	      postNotificationsRecord: postNotificationsRecord,
	
	      getNotificationsUnreadCount: getNotificationsUnreadCount,
	
	      getNotificationsStream: getNotificationsStream,
	
	      deleteNotificationsRecord: deleteNotificationsRecord,
	
	      putNotificationsReadRecord: putNotificationsReadRecord,
	
	      schemas: schemas
	    };
	  };
	};

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _notificationsGetResponse = __webpack_require__(5);
	
	var _notificationsGetResponse2 = _interopRequireDefault(_notificationsGetResponse);
	
	var _notificationsStreamGetResponse = __webpack_require__(6);
	
	var _notificationsStreamGetResponse2 = _interopRequireDefault(_notificationsStreamGetResponse);
	
	var _notificationsPostResponse = __webpack_require__(7);
	
	var _notificationsPostResponse2 = _interopRequireDefault(_notificationsPostResponse);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	exports.default = {
	  notificationsGetResponse: _notificationsGetResponse2.default,
	  notificationsStreamGetResponse: _notificationsStreamGetResponse2.default,
	  notificationsPostResponse: _notificationsPostResponse2.default
	};

/***/ },
/* 5 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = [{ id: '1234-5678-9012-3432',
	  title: 'Payment completed',
	  message: 'Your payment to John Doe was completed successfully.',
	  level: 'SUCCESS',
	  createdOn: '2017-10-04T14:54:36Z',
	  read: false,
	  origin: 'Transactions' }, { id: '1234-5678-9012-3433',
	  title: 'Payment rejected',
	  message: 'http://support.dashboard.backbase.com Your payment to A. Smith was rejected due to insufficient ' + 'fonds on you current account.',
	  level: 'ALERT',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  read: false,
	  origin: 'Transactions' }, { id: '1234-5678-9012-3434',
	  title: 'Scheduled payment alert',
	  message: 'Don`t forget: 50 euro will be withdrawn from the account on 2018-10-04',
	  level: 'INFO',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2018-10-04T14:54:36Z',
	  read: true,
	  origin: 'Security' }, { id: '1234-5678-9012-3437',
	  title: 'http://support.dashboard.backbase.com Updates',
	  message: 'You are using an old version of application. Please update it ' + 'as soon as possible. To get the new version click the link.',
	  level: 'WARNING',
	  createdOn: '2017-10-05T12:13:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2018-11-22T14:54:36Z',
	  read: true,
	  origin: 'Security' }, { id: '1234-5678-9012-3435',
	  title: 'Planned maintenance',
	  message: 'The server will be down at October 4 due to some problems you ' + 'should not care about. Sorry for inconvenience. For more information check the link below.',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  validFrom: '2018-10-04T14:54:36Z',
	  expiresOn: '2019-10-04T14:54:36Z',
	  read: false,
	  origin: 'Maintenance' }, { id: '1234-5678-9012-3436',
	  title: 'Payment completed',
	  message: 'long word ' + ': 1.12.123.1234.12345.123456.1234567.12345678.123456789.1234567890' + ' Your payment to John Doe was completed successfully.',
	  level: 'INFO',
	  createdOn: '2017-10-04T14:54:36Z',
	  read: false,
	  origin: 'Transactions' }, { id: '1234-5678-9012-3445',
	  title: 'Payment rejected because of the very very long reason which I wouldn`t describe',
	  message: 'Your payment to A. Smith was rejected due to insufficient ' + 'fonds on you current account. For additional information please click the link below',
	  level: 'ALERT',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  read: false,
	  origin: 'Transactions' }, { id: '1234-5678-9012-3438',
	  title: 'Scheduled payment alert',
	  message: 'Don`t forget: 50 euro will be withdrawn from the account on 2018-10-04',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2018-10-04T14:54:36Z',
	  read: true,
	  origin: 'Security' }, { id: '1234-5678-9012-3439',
	  title: 'Updates',
	  message: 'You are using an old version of application. Please update it as ' + 'soon as possible. To get the new version click the link.',
	  level: 'WARNING',
	  createdOn: '2017-10-05T12:13:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2018-11-22T14:54:36Z',
	  read: true,
	  origin: 'Security' }, { id: '1234-5678-9012-3440',
	  title: 'Planned maintenance',
	  message: 'The server will be down at October 4 due to some problems you ' + 'should not care about. Sorry for inconvenience. For more information check the link below.',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  validFrom: '2018-10-04T14:54:36Z',
	  expiresOn: '2019-10-04T14:54:36Z',
	  read: false,
	  origin: 'Maintenance' }, { id: '1234-5678-9012-3441',
	  title: 'Payment completed',
	  message: 'Your payment to John Doe was completed successfully.',
	  level: 'INFO',
	  createdOn: '2017-10-04T14:54:36Z',
	  read: false,
	  origin: 'Transactions' }, { id: '1234-5678-9012-3442',
	  title: 'Payment rejected',
	  message: 'Your payment to A. Smith was rejected due to insufficient ' + 'fonds on you current account. For additional information please click the link below',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  read: false,
	  origin: 'Transactions' }, { id: '1234-5678-9012-3443',
	  title: 'Scheduled payment alert',
	  message: 'Don`t forget: 50 euro will be withdrawn from the account on 2018-10-04',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2018-10-04T14:54:36Z',
	  read: true,
	  origin: 'Security' }, { id: '1234-5678-9012-3444',
	  title: 'Updates',
	  message: 'You are using an old version of application. Please update it' + ' as soon as possible. To get the new version click the link.',
	  level: 'WARNING',
	  createdOn: '2017-10-05T12:13:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2018-11-22T14:54:36Z',
	  read: true,
	  origin: 'Security' }, { id: '1234-5678-9012-3446',
	  title: 'Planned maintenance',
	  message: 'The server will be down at October 4 due to some problems you ' + 'should not care about. Sorry for inconvenience. For more information check the link below.',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  validFrom: '2018-10-04T14:54:36Z',
	  expiresOn: '2019-10-04T14:54:36Z',
	  read: false,
	  origin: 'Maintenance' }];

/***/ },
/* 6 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = [{ id: '1234-5678-9021',
	  title: 'Message without link',
	  message: 'Message without link',
	  level: 'INFO',
	  createdOn: '2017-10-04T14:54:36Z' }, { id: '1234-5678-9012',
	  title: 'Message with link',
	  message: 'Message with link',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com' }, { id: '1234-5678-9013',
	  title: 'Sticky message',
	  message: 'Sticky message',
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  link: 'http://support.dashboard.backbase.com',
	  expiresOn: '2017-11-04T14:54:36Z' }, { id: '1234-5678-9014',
	  title: "Message shown in future (unless it's past November 2016 :)",
	  message: "Message shown in future (unless it's past November 2016 :)",
	  level: 'WARNING',
	  createdOn: '2017-10-04T14:54:36Z',
	  ink: 'http://support.dashboard.backbase.com',
	  validFrom: '2018-10-04T14:54:36Z',
	  expiresOn: '2019-11-04T14:54:36Z' }];

/***/ },
/* 7 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = [{
	  id: '1234-5678-9012-3432'
	}, {
	  id: '1234-5678-9012-3433'
	}, {
	  id: '1234-5678-9012-3434'
	}, {
	  id: '1234-5678-9012-3435'
	}];

/***/ },
/* 8 */
/***/ function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var endIndex = 0;
	
	var pageable = function pageable(collection, data) {
	  var size = parseInt(data && data.size, 10) || 10;
	  var from = parseInt(data && data.from, 10) || 0;
	  var cursor = parseInt(data && data.cursor, 10) || 0;
	
	  if (cursor) {
	    from = cursor / size;
	  }
	
	  var startIndex = from * size;
	  endIndex = startIndex + size;
	
	  return collection.slice(startIndex, endIndex);
	};
	
	var getPointer = exports.getPointer = function getPointer() {
	  return endIndex;
	};
	
	exports.default = pageable;

/***/ }
/******/ ])
});
;
//# sourceMappingURL=mock.data-bb-notifications-http-ng.js.map