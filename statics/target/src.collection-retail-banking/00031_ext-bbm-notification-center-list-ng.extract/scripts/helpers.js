import { TimePeriod } from 'ui-bb-date-label-filter-ng';

const LevelIconClass = {
  ALERT: 'fa-exclamation-circle',
  WARNING: 'fa-exclamation-triangle',
  INFO: 'fa-info-circle',
  SUCCESS: 'fa-check-circle',
};

const DateLabelKey = {
  [TimePeriod.NOW]: 'calendar.label.now',
  [TimePeriod.TODAY]: 'calendar.label.today',
  [TimePeriod.YESTERDAY]: 'calendar.label.yesterday',
};

export default ({ $filter }) => {
  /**
   * The standard ISO-8601 supports the following formats for time offsets:
   * ±[hh]:[mm], ±[hh][mm], or ±[hh]
   * However iOS does support only ±[hh]:[mm] format.
   * Thus we make sure that the given date string has the following
   * variation of the ISO-8601 standard:
   * "YYYY-MM-DDThh:mm:ss.SSS±hh:mm"
   * @name normalizeDate
   * @inner
   * @param dateStr
   */
  const normalizeDate = dateStr => {
    const filteredDate = $filter('date')(dateStr, 'yyyy-MM-ddTHH:mm:ss.sssZ');
    return filteredDate.replace(/(\d{2}):?(\d{2})$/, '$1:$2');
  };

  return {
    getLevelIcon: (level) => LevelIconClass[level],

    getDateLabel: (notification) => {
      const date = normalizeDate(notification.createdOn);
      let labelKey;
      let resultDateLabel;

      if (!notification.isOpen) {
        labelKey = DateLabelKey[$filter('dateLabel')(date)];
        if (labelKey) {
          if (labelKey === DateLabelKey[TimePeriod.NOW]) {
            resultDateLabel = $filter('i18n')(labelKey);
          } else {
            resultDateLabel = $filter('i18n')(labelKey) +
              $filter('i18n')('calendar.label.at') +
              $filter('date')(date, 'hh:mm');
          }
        }
      }

      return labelKey ? resultDateLabel : $filter('date')(date, 'd MMMM yyyy');
    },
  };
};
