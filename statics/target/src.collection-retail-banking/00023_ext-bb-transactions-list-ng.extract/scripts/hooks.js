/* eslint-disable import/prefer-default-export */
import debitCreditSign from './debit-credit-sign';
import setType from './set-type';

/**
 * @name defaultSortableColumn
 *
 * @description
 * defaultSortableColumn default hook
 *
 * @type {function}
 * @returns {string}
 */
export const defaultSortableColumn = () => 'bookingDate';

/**
 * @name defaultSortableDirection
 *
 * @description
 * defaultSortableDirection default hook
 *
 * @type {function}
 * @returns {string}
 */
export const defaultSortableDirection = () => 'DESC';

/**
 * @name defaultPaginationType
 * @type {function}
 * @description
 * defaultPaginationType default hook for setting load-more or pagination as default
 *
 * @param {string} pagination type
 * @returns {?string}
 */
export const defaultPaginationType = () => 'load-more';

/**
 * @name isTransactionsShown
 *
 * @description
 * Hook for checking if the transactions should be visible
 *
 * @type {function}
 * @returns {boolean}
 */
export const isTransactionsShown = () => false;

/**
 * @name processTransactions
 * @description
 * Hook for process transactions
 *
 * Add debitCreditSign property to transaction
 *
 * Add customType field to transaction
 *
 * Sort transactions collection based on valueDate (descending)
 *
 * @type {function}
 * @param {object[]} transactions The source transactions from the widget controller
 * @returns {object} Processed transaction array
 */
export const processTransactions = (transactions) =>
  transactions
    .map(debitCreditSign)
    .map(setType)
    .reduce((groupedByDate, transaction) => {
      const datesObject = groupedByDate;

      if (datesObject[transaction.bookingDate]) {
        let clearToAdd = true;

        datesObject[transaction.bookingDate].forEach((element) => {
          if (element.id === transaction.id) {
            clearToAdd = false;
          }
        });

        if (clearToAdd) {
          datesObject[transaction.bookingDate].push(transaction);
        }
      } else {
        datesObject[transaction.bookingDate] = [transaction];
      }

      return datesObject;
    }, {});
