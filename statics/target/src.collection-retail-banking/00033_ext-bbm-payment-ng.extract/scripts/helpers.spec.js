import extHelpers from './helpers';

describe('helpers', function() {
  const obj = {
    publish: () => {},
  };
  const ctrl = {
    storePayment: () => {},
    makePayment: () => {},
    setSaveContactFlag: () => {},
    storeSaveContactFlag: () => {},
    canSaveNewContact: () => Promise.resolve(),
    paymentPreferences: { reviewStep: true },
    payment: {
      from: null,
      to: null,
    },
    saveNewContact: false,
  };
  const paymentForm = {
    $setUntouched: () => {},
    $invalid: true
  };
  let helper;

  beforeEach(() => {
    helper = extHelpers(obj);
    spyOn(ctrl, 'canSaveNewContact');
    spyOn(ctrl, 'setSaveContactFlag');
  });

  describe('onPaymentContinue', function() {
    it('should store payment on review step', () => {
      spyOn(ctrl, 'storePayment');

      helper.onPaymentContinue(ctrl, paymentForm);

      expect(ctrl.storePayment).toHaveBeenCalled();
    });

    it('should store the saveNewContact flag on the review step', () => {
      spyOn(ctrl, 'storeSaveContactFlag');

      helper.onPaymentContinue(ctrl, paymentForm);

      expect(ctrl.storeSaveContactFlag).toHaveBeenCalled();
    });

    it('should make a payment', () => {
      spyOn(ctrl, 'makePayment');

      ctrl.paymentPreferences.reviewStep = false;
      helper.onPaymentContinue(ctrl, paymentForm);

      expect(ctrl.makePayment).toHaveBeenCalled();
    });

    it('should set the form in untouched state', () => {
      spyOn(paymentForm, '$setUntouched');

      helper.onPaymentContinue(ctrl, paymentForm);

      expect(paymentForm.$setUntouched).toHaveBeenCalled();
    });
  });

  describe('invalidate', function() {
    it('should return true on invalid form', () => {
      const invalid =helper.invalidate(ctrl.payment, paymentForm);

      expect(invalid).toBeTruthy();
    });

    it('should return false on valid form', () => {
      ctrl.payment.from = {};
      paymentForm.$invalid = false;
      const invalid =helper.invalidate(ctrl.payment, paymentForm);

      expect(invalid).toBeFalsy();
    });
  });

  describe('canSaveNewContact', function() {
    it('should return false if beneficiary is null', () => {
      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeFalse();
      expect(ctrl.setSaveContactFlag).toHaveBeenCalledWith(false);
    });

    it('should return false if beneficiary is name is empty', () => {
      ctrl.payment.to = {
        name: '',
        identifier: 'NL91 ABNA 0417 1643 00',
      };

      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeFalse();
      expect(ctrl.setSaveContactFlag).toHaveBeenCalledWith(false);
    });

    it('should return false if beneficiary is iban is undefined', () => {
      ctrl.payment.to = {
        name: 'Henk Bruin',
        identifier: undefined,
      };

      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeFalse();
      expect(ctrl.setSaveContactFlag).toHaveBeenCalledWith(false);
    });

    it('should return false if beneficiary is iban is empty', () => {
      ctrl.payment.to = {
        name: 'Henk Bruin',
        identifier: '',
      };

      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeFalse();
      expect(ctrl.setSaveContactFlag).toHaveBeenCalledWith(false);
    });

    it('should return false if beneficiary exists in the internal accounts list', () => {
      ctrl.accountsTo = [
        {
          name: 'Henk Bruin',
          identifier: 'NL91 ABNA 0417 1643 00'
        }
      ];

      ctrl.payment.to = {
        name: 'Henk Bruin',
        identifier: 'NL91 ABNA 0417 1643 00',
      };

      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeFalse();
      expect(ctrl.setSaveContactFlag).toHaveBeenCalledWith(false);
    });

    it('should return false if beneficiary exists in the accounts list contacts', () => {
      ctrl.accountsTo = [
        {
          name: 'Henk Bruin',
          identifier: 'NL91 ABNA 0417 1643 00'
        },
        {
          contacts: [{
            name: 'Sara Cerveza',
            identifier: 'ES91 2100 0418 4502 0005 1332'
          }]
        }
      ];

      ctrl.payment.to = {
        name: 'Sara Cerveza',
        identifier: 'ES91 2100 0418 4502 0005 1332',
      };

      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeFalse();
      expect(ctrl.setSaveContactFlag).toHaveBeenCalledWith(false);
    });

    it('should return true if beneficiary does not exist in the list of acounts', () => {
      ctrl.accountsTo = [
        {
          name: 'Henk Bruin',
          identifier: 'NL91 ABNA 0417 1643 00'
        },
        {
          contacts: [{
            name: 'Sara Cerveza',
            identifier: 'ES91 2100 0418 4502 0005 1332'
          }]
        }
      ];

      ctrl.payment.to = {
        name: 'Jean-Paul Dubois',
        identifier: 'FR14 2004 1010 0505 0001 3M02 606',
      };

      const canSaveNewContact = helper.canSaveNewContact(ctrl);

      expect(canSaveNewContact).toBeTrue();
      expect(ctrl.setSaveContactFlag).not.toHaveBeenCalled();
    });
  });
});
