/**
 * @name Helpers
 * @type {object}
 *
 * @description
 * Helpers for ext-bbm-payment-ng
 */

/**
 * Widget events enum
 * @type {object}
 */
const Event = {
  PAYMENT_REVIEW_STEP: 'bb.event.payment.review.step',
};

/**
 * @name isBeneficiaryComplete
 * @type {function}
 *
 * @description
 * Checks if all the required fields for a beneficiary have been filled
 *
 * @param {object} beneficiary - Beneficiary data object
 * @returns {boolean}
 * @inner
 */
const isBeneficiaryComplete = (beneficiary) => Boolean(
  beneficiary && beneficiary.name && beneficiary.identifier
);

/**
 * @name isInternalAccount
 * @type {function}
 *
 * @description
 * Checks if the given account is an internal account
 *
 * @param {object} beneficiary - Beneficiary data object
 * @returns {boolean}
 * @inner
 */
const isInternalAccount = (account) => Boolean(
  account.name && account.identifier
);

/**
 * @name isSameAccount
 * @type {function}
 *
 * @description
 * Checks if given account A matches given account B
 *
 * @param {object} accountA - Data object accountA
 * @param {object} accountB - Data object accountB
 * @returns {boolean}
 * @inner
 */
const isSameAccount = (accountA, accountB) => Boolean(
  accountA.name === accountB.name &&
  accountA.identifier === accountB.identifier
);

/**
 * @name isExistingAccount
 * @type {function}
 *
 * @description
 * Checks if the given accounts list contains the given account
 *
 * @param {object} accounts - Data object with accounts
 * @param {object} account - Data object account
 * @returns {boolean}
 * @inner
 */
const isExistingAccount = (accounts, account) => (
  accounts.some(item => {
    if (isInternalAccount(item)) {
      return isSameAccount(item, account);
    }

    return (item.contacts || []).some(contact => isSameAccount(contact, account));
  })
);

export default ({ publish }) => ({
  /**
   * @name Helpers#onPaymentContinue
   * @type {function}
   *
   * @description
   * Helper to reset the payment model, and update accounts and currency lists
   *
   * @param {object} $ctrl Instance of the angular widget controller
   * @param {object} paymentForm Instance of the angular form
   * @returns {void}
   */
  onPaymentContinue: ($ctrl, paymentForm) => {
    const { reviewStep } = $ctrl.paymentPreferences;

    paymentForm.$setUntouched();
    if (reviewStep) {
      $ctrl.storePayment($ctrl.payment);
      $ctrl.storeSaveContactFlag();
      publish(Event.PAYMENT_REVIEW_STEP, $ctrl.payment);
    } else {
      $ctrl.makePayment($ctrl.payment);
    }
  },

  /**
   * @name Helpers#invalidate
   * @type {function}
   *
   * @description
   * Helper to invalidate the payment form
   *
   * @param {Payment} payment Payment object
   * @param {object} paymentForm Instance of the angular form
   * @returns {boolean} True if the form is valid, false otherwise
   */
  invalidate: (payment, paymentForm) => {
    if (!payment.from || paymentForm.$invalid) {
      return true;
    }

    return false;
  },

  /**
   * @name Helpers#onPaymentFromAccountsClick
   * @type {function}
   *
   * @description
   * Helper to process reaction on debit accounts type selection
   *
   * @param {object} $ctrl Widget controller
   * @returns {Promise<any>} Promise which is resolved once the click is processed,
   *   or rejected in case of errors
   */
  onPaymentFromAccountsClick: $ctrl => $ctrl.processSelectProductType(true),

  /**
   * @name Helpers#onPaymentToAccountsClick
   * @type {function}
   *
   * @description
   * Helper to process reaction on credit accounts type selection
   *
   * @param {object} $ctrl Widget controller
   * @returns {Promise<any>} Promise, which is resolved once the click is processed,
   *   or rejected in case of error
   */
  onPaymentToAccountsClick: $ctrl => $ctrl.processSelectProductType(false),

  /**
   * @name Helpers#canSaveNewContact
   * @type {function}
   *
   * @description
   * Checks if layout should show 'save beneficiary' switcher
   *
   * @param {object} $ctrl PaymentController
   * @returns {boolean}
   */
  canSaveNewContact: ($ctrl) => {
    const accounts = $ctrl.accountsTo || []; // to cover null arg issue
    const beneficiary = $ctrl.payment.to;

    const canBeSaved = isBeneficiaryComplete(beneficiary) &&
      !isExistingAccount(accounts, beneficiary);

    // reset switcher before any further appearance
    if (!canBeSaved) {
      $ctrl.setSaveContactFlag(false);
    }

    return canBeSaved;
  },
});
