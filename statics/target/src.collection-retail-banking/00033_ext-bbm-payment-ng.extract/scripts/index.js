/**
 * @module ext-bbm-payment-ng
 *
 * @description
 * Mobile extension for a payment form in the Payment widget.
 *
 * @example
 * <!-- Contact widget model.xml -->
 * <property name="extension" viewHint="text-input,admin">
 *   <value type="string">ext-bbm-payment-ng</value>
 * </property>
 */
import uiBbmCurrencyInputNgKey from 'ui-bbm-currency-input-ng';
import uiBbmBeneficiarySelectNgKey from 'ui-bbm-beneficiary-select-ng';
import uiBbIbanKey from 'ui-bb-iban-ng';
import uiBbI18nNgKey from 'ui-bb-i18n-ng';
import uiBbFormatAmount from 'ui-bb-format-amount';
import uiBbTextNg from 'ui-bb-text-ng';
import vendorBbAngularNgMessagesKey from 'vendor-bb-angular-ng-messages';
import ngSanitizeKey from 'vendor-bb-angular-sanitize';

import * as extHooks from './hooks';
import extEvents from './events';
import extHelpers from './helpers';

import '../styles/index.css';

export const hooks = extHooks;
export const events = extEvents;
export const helpers = extHelpers;

export const dependencyKeys = [
  uiBbmCurrencyInputNgKey,
  uiBbmBeneficiarySelectNgKey,
  uiBbIbanKey,
  uiBbI18nNgKey,
  uiBbFormatAmount,
  uiBbTextNg,
  vendorBbAngularNgMessagesKey,
  ngSanitizeKey,
];
