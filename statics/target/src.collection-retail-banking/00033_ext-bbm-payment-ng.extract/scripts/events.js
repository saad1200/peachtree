// Temporary disable capsInNew until it fixed in lib-bbm-plugins
/* eslint new-cap: ["error", { "capIsNew": false }] */
import plugins from 'lib-bbm-plugins';

const Event = {
  ACCOUNT_FROM_LOAD: 'bb.event.account.from.load',
  ACCOUNT_FROM_DONE: 'bb.event.account.from.done',
  ACCOUNT_FROM_FAILED: 'bb.event.account.from.failed',
  ACCOUNT_TO_LOAD: 'bb.event.account.to.load',
  ACCOUNT_TO_DONE: 'bb.event.account.to.done',
  ACCOUNT_TO_FAILED: 'bb.event.account.to.failed',
  PAYMENT_START: 'bb.event.payment.start',
  PAYMENT_DONE: 'bb.event.payment.done',
  PAYMENT_FAILED: 'bb.event.payment.failed',
};

export default ({ $filter }) => {
  const i18n = $filter('i18n');
  return {
    [Event.ACCOUNT_FROM_LOAD]() {
      plugins.ActivityIndicator().show(i18n('message.payment.account.from.load'));
    },

    [Event.ACCOUNT_FROM_DONE]() {
      plugins.ActivityIndicator().hide();
    },

    [Event.ACCOUNT_FROM_FAILED]() {
      plugins.ActivityIndicator().hide();
      plugins.Snackbar().error(i18n('message.payment.account.from.failed'));
    },

    [Event.ACCOUNT_TO_LOAD]() {
      plugins.ActivityIndicator().show(i18n('message.payment.account.to.load'));
    },

    [Event.ACCOUNT_TO_DONE]() {
      plugins.ActivityIndicator().hide();
    },

    [Event.ACCOUNT_TO_FAILED]() {
      plugins.ActivityIndicator().hide();
      plugins.Snackbar().error(i18n('message.payment.account.to.failed'));
    },

    [Event.PAYMENT_START]() {
      plugins.ActivityIndicator().show(i18n('message.payment.start'));
    },

    [Event.PAYMENT_DONE]() {
      plugins.ActivityIndicator().hide();
      plugins.Snackbar().success(i18n('message.payment.done'));
    },

    [Event.PAYMENT_FAILED]() {
      plugins.ActivityIndicator().hide();
      plugins.Snackbar().error(i18n('message.payment.failed'));
    },
  };
};
