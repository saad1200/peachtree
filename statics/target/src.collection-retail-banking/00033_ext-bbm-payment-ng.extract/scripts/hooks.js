/* eslint-disable import/prefer-default-export */
/* eslint no-unused-vars: ["error", { "args": "none" }] */

/**
 * @name Hooks
 * @type {object}
 *
 * @description
 * Hooks for widget-bb-payment-ng
 */

/**
 * @name isExternalTransferAllowed
 * @type {function}
 *
 * @param {Account} debitAccount
 * @returns {boolean}
 * @inner
 */
const isExternalTransferAllowed = (debitAccount) => (
  !debitAccount.id || debitAccount.externalTransferAllowed
);

/**
 * @name wait
 * @type {function}
 * @param {number} delay
 * @returns {promise}
 * @inner
 */
const wait = (delay) => new Promise(resolve => setTimeout(resolve, delay));

/**
 * @name Hooks#processAccountsTo
 * @type {function}
 *
 * @description
 * Hook for processing account list in 'to' field (credit).
 * Assigned to [$ctrl.accountsTo]{@link PaymentController#AccountView}
 *
 * @param {Account} debitAccount Selected debit account (can be null)
 * @param {function} getCreditAccounts Function to retrieve all credit accounts
 * @param {function} getExternalAccounts Function to retrieve all external contacts
 * formatted like Product kind
 * @returns {Promise<array>} Promise that retrieves array of accounts.
 */
export function processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts) {
  return wait(300)
    .then(() => getCreditAccounts(debitAccount.id || null))
    .then(accounts => {
      if (isExternalTransferAllowed(debitAccount)) {
        return getExternalAccounts()
          .then(contacts => [
            ...accounts,
            ...contacts,
          ]);
      }
      return accounts;
    });
}
