import * as extHooks from './hooks';

describe('ext-bbm-select-product-ng:hooks', () => {
  describe('processAccountsTo', () => {
    const { processAccountsTo } = extHooks;

    const contactA = { id: 'contact-a', name: 'Brechtje Mien' };
    const contactB = { id: 'contact-b', name: 'Brigitta Sabien' };
    const contactC = { id: 'contact-c', name: 'Rien Filibert' };
    const contactD = { id: 'contact-d', name: 'Sofie Claudia' };

    const contacts = [
      contactA,
      contactB,
      contactC,
      contactD,
    ];

    const creditAccounts = [
      { id: 'credit-account-1' },
      { id: 'credit-account-2' },
      { id: 'credit-account-3' },
    ];

    const getCreditAccounts = jasmine.createSpy('getCreditAccounts').and
      .returnValue(Promise.resolve(creditAccounts));

    const getExternalAccounts = jasmine.createSpy('getExternalAccounts').and
      .returnValue(Promise.resolve(contacts));

    it('should return credit accounts if external transfer is not allowed', (done) => {
      const debitAccount = {
        id: 'debit-account-1',
        externalTransferAllowed: false,
      };

      processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts)
        .then((accounts) => {
          expect(accounts).toEqual(creditAccounts);
          done();
        })
        .catch(done.fail);
    });

    it('should return credit accounts and contacts if external transfer is allowed', (done) => {
      const debitAccount = {
        id: 'debit-account-1',
        externalTransferAllowed: true,
      };

      processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts)
        .then((accounts) => {
          const expectedContacts = [
            contactA,
            contactB,
            contactC,
            contactD,
          ];

          expect(accounts).toEqual([
            ...creditAccounts,
            ...expectedContacts
          ]);

          done();
        })
        .catch(done.fail);
    });
  });
});
