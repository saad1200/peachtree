/* global expect, describe, beforeEach, jasmine, it, fail, spyOn */
/* eslint new-cap: ["error", { "capIsNew": false }] */

import plugins from 'lib-bbm-plugins';
import extEvents from './events';

describe('ext-bbm-contact-form-ng:events', () => {
  let event;

  beforeEach(() => {
    const mock = {
      $filter: () => {
        return (key) => {
          switch (key) {
            case 'message.payment.account.from.load' :
              return 'Loading accounts';
            case 'message.payment.account.from.failed' :
              return 'Not able to load accounts, please try again.';
            case 'message.payment.account.to.load' :
              return 'Loading beneficiaries';
            case 'message.payment.account.to.failed' :
              return 'Not able to load beneficiaries, please try again.';
            case 'message.payment.start' :
              return 'Sending payment';
            case 'message.payment.done' :
              return 'Payment successfully submitted';
            case 'message.payment.failed' :
              return 'Not able to process payment, please try again.';
          }
        }
      }
    };

    event = extEvents(mock);

    const activityIndicator = jasmine.createSpyObj('activityIndicator', ['show', 'hide']);
    activityIndicator.show.and.returnValue(Promise.resolve({}));
    activityIndicator.hide.and.returnValue(Promise.resolve({}));

    const snackbar = jasmine.createSpyObj('snackbar', ['error', 'hide', 'success']);
    snackbar.error.and.returnValue(Promise.resolve({}));

    spyOn(plugins, 'ActivityIndicator').and.returnValue(activityIndicator);
    spyOn(plugins, 'Snackbar').and.returnValue(snackbar);
  });

  describe('accountFromLoad', () => {
    it('should show activity indicator', () => {
      event['bb.event.account.from.load']();
      expect(plugins.ActivityIndicator().show).toHaveBeenCalledWith('Loading accounts');
    });
  });

  describe('accountFromDone', () => {
    it('should hide activity indicator', () => {
      event['bb.event.account.from.done']();
      expect(plugins.ActivityIndicator().hide).toHaveBeenCalled();
    });
  });

  describe('accountFromFail', () => {
    it('should hide activity indicator', () => {
      event['bb.event.account.from.failed']();
      expect(plugins.ActivityIndicator().hide).toHaveBeenCalled();
    });

    it('should show error notification', () => {
      event['bb.event.account.from.failed']();
      expect(plugins.Snackbar().error).toHaveBeenCalledWith('Not able to load accounts, please try again.');
    });
  });

  describe('accountToLoad', () => {
    it('should show activity indicator', () => {
      event['bb.event.account.to.load']();
      expect(plugins.ActivityIndicator().show).toHaveBeenCalledWith('Loading beneficiaries');
    });
  });

  describe('accountToDone', () => {
    it('should hide activity indicator', () => {
      event['bb.event.account.to.done']();
      expect(plugins.ActivityIndicator().hide).toHaveBeenCalled();
    });
  });

  describe('accountToFail', () => {
    it('should hide activity indicator', () => {
      event['bb.event.account.to.failed']();
      expect(plugins.ActivityIndicator().hide).toHaveBeenCalled();
    });

    it('should show error notification', () => {
      event['bb.event.account.to.failed']();
      expect(plugins.Snackbar().error).toHaveBeenCalledWith('Not able to load beneficiaries, please try again.');
    });
  });

  describe('PAYMENT_START', () => {
    it('should show activity indicator', () => {
      event['bb.event.payment.start']();
      expect(plugins.ActivityIndicator().show).toHaveBeenCalledWith('Sending payment');
    });
  });

  describe('PAYMENT_DONE', () => {
    it('should hide activity indicator', () => {
      event['bb.event.payment.done']();
      expect(plugins.ActivityIndicator().hide).toHaveBeenCalled();
    });

    it('should show success notification', () => {
      event['bb.event.payment.done']();
      expect(plugins.Snackbar().success).toHaveBeenCalledWith('Payment successfully submitted');
    });
  });

  describe('PAYMENT_FAILED', () => {
    it('should hide activity indicator', () => {
      event['bb.event.payment.failed']();
      expect(plugins.ActivityIndicator().hide).toHaveBeenCalled();
    });

    it('should show error notification', () => {
      event['bb.event.payment.failed']();
      expect(plugins.Snackbar().error).toHaveBeenCalledWith('Not able to process payment, please try again.');
    });
  });
});
