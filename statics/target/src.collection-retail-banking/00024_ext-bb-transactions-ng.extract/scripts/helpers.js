import { negativeSignKey } from './debit-credit-sign';
import CATEGORY_CLASS_PREFIX, {
  uncategorizedIconClass as DEFAULT_ICON_CLASS,
} from './constants';

/**
 * @description
 * Based on credit/debit indicator, put right sign on the transaction amount
 *
 * @name getSignedAmount
 * @type {function}
 * @param {object} transaction Transaction object
 * @returns {number} Signed amount
 */
export const getSignedAmount = (transaction) =>
  transaction.amount * (transaction.creditDebitIndicator === negativeSignKey ? -1 : 1);

/**
 * @description
 * Checkes if actual pagination type matches the one, defined in properties
 *
 * @public
 * @name isPaginationTypeMatch
 * @type {function}
 * @param {function} $ctrl      Current controller
 * @param {string} type         Description of pagination type (pagination, load-more)
 *
 * @returns {boolean}
 */
export const isPaginationTypeMatch = ($ctrl, type) =>
  $ctrl.state.pageParams.paginationType === type;

/**
 * @description
 * Converts transaction category name into category CSS icon class
 *
 * @name getCategoryIconClass
 * @type {function}
 * @param {string} transactionCategory Transaction category
 * @returns {string}
 */
export const getCategoryIconClass = (transactionCategory) =>
  `${CATEGORY_CLASS_PREFIX}${transactionCategory.toLowerCase()
    .replace(/\W/g, '-').replace(/-{2,}/g, '-') || DEFAULT_ICON_CLASS}`;
