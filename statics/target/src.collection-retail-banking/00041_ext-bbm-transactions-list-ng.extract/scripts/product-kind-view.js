
const maskCardNumber = (suffix) => {
  if (!suffix) {
    return '';
  }
  return `XXXX-XXXX-XXXX-${suffix}`;
};

const defaultViewModel = (product) => ({
  id: product.id,
  name: product.name,
});

const viewModelFactories = {
  currentAccounts: (product) => ({
    id: product.id,
    name: product.name,
    identifier: product.IBAN || product.BBAN,
    primaryValue: product.bookedBalance,
    secondaryValue: product.availableBalance,
    secondaryLabel: 'label.availableBalance',
    tertiaryValue: product.creditLimit,
    tertiaryLabel: 'label.creditLimit',
    currency: product.currency,
  }),

  savingsAccounts: (product) => ({
    id: product.id,
    name: product.name,
    identifier: product.BBAN || product.IBAN,
    primaryValue: product.bookedBalance,
    secondaryValue: product.accruedInterest,
    secondaryLabel: 'label.accruedInterestAmount',
    currency: product.currency,
  }),

  termDeposits: (product) => ({
    id: product.id,
    name: product.name,
    primaryValue: product.principalAmount,
    secondaryValue: product.accruedInterest,
    secondaryLabel: 'label.accruedInterestAmount',
    currency: product.currency,
  }),

  creditCards: (product) => ({
    id: product.id,
    name: product.name,
    identifier: maskCardNumber(product.number),
    primaryValue: product.bookedBalance,
    secondaryValue: product.creditLimit,
    secondaryLabel: 'label.creditLimit',
    tertiaryValue: product.availableBalance,
    tertiaryLabel: 'label.availableBalance',
    currency: product.currency,
  }),

  debitCards: (product) => ({
    id: product.id,
    name: product.name,
    identifier: maskCardNumber(product.number),
  }),

  loans: (product) => ({
    id: product.id,
    name: product.name,
    primaryValue: product.bookedBalance,
    currency: product.currency,
  }),

  investmentAccounts: (product) => ({
    id: product.id,
    name: product.name,
    primaryValue: product.currentInvestmentValue,
    currency: product.currency,
  }),
};

/**
 * Prepare the fields of a Product into a form ready for display to the User
 *
 * @param {object} product The source Product from the API
 * @type {function}
 * @returns {ProductView}
 * @inner
 */
export default (product) => {
  const isProcessedProduct = {}.hasOwnProperty.call(product, 'identifier') ||
    {}.hasOwnProperty.call(product, 'primaryValue');

  if (isProcessedProduct) {
    return product;
  }

  const kind = product.kind;
  if (!{}.hasOwnProperty.call(viewModelFactories, kind)) {
    throw new TypeError(`Unhandled product kind: ${kind}`);
  }

  return viewModelFactories[kind](product) || defaultViewModel(product);
};


/**
 * @typedef ProductView
 * @type {object}
 * @property {string} id The internal Product Identifier
 * @property {string} name The product's name, suitable for display to users
 * @property {?string} identifier The identifier of the Product from the user's perspective
 * @property {?string} primaryValue The most important associated value to be displayed
 * @property {?string} secondaryValue A secondary associated value to be displayed
 * @property {?string} secondaryLabel A label to describe the secondary value
 */
