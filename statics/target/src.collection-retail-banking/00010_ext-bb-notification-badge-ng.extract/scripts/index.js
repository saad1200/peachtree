/**
 * @module ext-bb-notification-badge-ng
 *
 * @description
 * Default extension for notifications badge.
 *
 * @example
 * <!-- widget's model.xml -->
 * <property name="extension" viewHint="text-input,admin">
 *  <value type="string">ext-bb-notification-badge-ng</value>
 * </property>
 */
import uibPopoverKey from 'vendor-bb-uib-popover';
import i18nKey from 'ui-bb-i18n-ng';
import uiBbDateLabelFilterKey from 'ui-bb-date-label-filter-ng';
import uiBbMessageNgKey from 'ui-bb-message-ng';
import uiBbLoadingIndicatorKey from 'ui-bb-loading-indicator-ng';
import uiBbLoadMoreKey from 'ui-bb-load-more-ng';
import uiBbInlineStatusKey from 'ui-bb-inline-status-ng';
import uiBbSubstituteErrorNgKey from 'ui-bb-substitute-error-ng';
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';
import uiBbConfirmKey from 'ui-bb-confirm-ng';


import extHelpers from './helpers';

export const hooks = {};
export const helpers = extHelpers;

export const dependencyKeys = [
  uibPopoverKey,
  i18nKey,
  uiBbDateLabelFilterKey,
  uiBbMessageNgKey,
  uiBbLoadingIndicatorKey,
  uiBbLoadMoreKey,
  uiBbInlineStatusKey,
  uiBbSubstituteErrorNgKey,
  ngAriaModuleKey,
  uiBbConfirmKey,
];
