export const Operator = {
  greaterThan: 'gt',
  lessThan: 'lt',
  equals: 'eq',
  greaterOrEqual: 'gte',
  lessOrEqual: 'lte',
};

export default {
  Operator,
};
