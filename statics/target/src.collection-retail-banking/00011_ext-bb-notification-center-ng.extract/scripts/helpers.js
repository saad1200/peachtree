import { TimePeriod } from 'ui-bb-date-label-filter-ng';

const dateLabelKeys = {
  [TimePeriod.NOW]: 'calendar.label.now',
  [TimePeriod.TODAY]: 'calendar.label.today',
  [TimePeriod.YESTERDAY]: 'calendar.label.yesterday',
};

export default ({ widget, $filter }) => ({
  isExpired: expiresOn => Date.parse(expiresOn) < Date.now(),
  loadMoreButton: widget.getBooleanPreference('loadMoreButton'),
  itemsPerPage: widget.getLongPreference('itemsPerPage'),
  maximumNumberOfPages: widget.getLongPreference('maximumNumberOfPages'),
  markButtonClass: (isRead) => (isRead ? 'fa-eye-slash' : 'fa-eye'),
  getEmptyMessage: (isFilterApplied) =>
    $filter('i18n')(`notification.message.${isFilterApplied ? 'not.found' : 'empty'}`),
  getDateLabel: (item) => {
    const date = item.validFrom || item.createdOn;
    let labelKey;
    if (!item.isOpen) {
      labelKey = dateLabelKeys[$filter('dateLabel')(date)];
    }
    return labelKey ? $filter('i18n')(labelKey) : $filter('date')(date, 'medium');
  },
  /* global window */
  /* eslint no-param-reassign: ["error", { "props": false }] */
  toggleCollapsed: (item) => {
    if (!window.getSelection().toString()) {
      item.isOpen = !item.isOpen;
    }
  },
});
