import helpers from './helpers';

describe('helpers', () => {

  let helpersInstance;

  // Filter mock
  const filters = {
    'dateLabel': date => date,
    'i18n': key => {
      const keys = {
        'calendar.label.now': 'Now',
        'calendar.label.today': 'Today',
        'calendar.label.yesterday': 'Yesterday',
      };
      return keys[key] || null;
    },
    'date': (date, format) => format === 'medium' ? date.getTime() : null
  };

  // Filter mock
  const $filter = filter => ((...args) => {
    return filters[filter].apply(null, args);
  });

  beforeEach(() => {
    const widget = {
      getBooleanPreference: val => val,
      getLongPreference: val => val,
    };

    helpersInstance = helpers({ widget, $filter });
  });

  describe('loadMoreButton', () => {

    it('should set state of loadMoreButton option from widget preference', () => {
      expect(helpersInstance.loadMoreButton).toEqual('loadMoreButton');
    });

  });

  describe('itemsPerPage', () => {

    it('should set count of itemsPerPage option from widget preference', () => {
      expect(helpersInstance.itemsPerPage).toEqual('itemsPerPage');
    });

  });

  describe('markButtonClass', () => {

    it('should return fa-eye-slash class', () => {
      expect(helpersInstance.markButtonClass(true)).toEqual('fa-eye-slash');
    });

    it('should return fa-eye class', () => {
      expect(helpersInstance.markButtonClass(false)).toEqual('fa-eye');
    });

  });

  describe('getDateLabel', () => {

    it('should call i18n', () => {
      expect(helpersInstance.getDateLabel({validFrom: 'now'})).toEqual('Now');
      expect(helpersInstance.getDateLabel({validFrom: 'today'})).toEqual('Today');
      expect(helpersInstance.getDateLabel({createdOn: 'yesterday'})).toEqual('Yesterday');
      expect(helpersInstance.getDateLabel({createdOn: {getTime: () => '10/10/10'}})).toEqual('10/10/10');
    });

    it('should call date filter', () => {
      const createdOn = new Date();
      expect(helpersInstance.getDateLabel({isOpen: true, createdOn})).toEqual(createdOn.getTime());
    });

  });

  describe('toggleCollapsed', () => {

    it('should expand item', () => {
      const item = { isOpen: false };

      spyOn(window, 'getSelection').and.callFake(() => '');
      helpersInstance.toggleCollapsed(item);

      expect(item.isOpen).toBe(true);
    });

    it('should not expand item', () => {
      const item = { isOpen: false };

      spyOn(window, 'getSelection').and.callFake(() => 'selected');
      helpersInstance.toggleCollapsed(item);

      expect(item.isOpen).toBe(false);
    });

  });

});