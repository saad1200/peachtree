/**
 * @module ext-bb-notification-center-ng
 *
 * @description
 * Default extension for notifications center.
 *
 * @example
 * <!-- widget's model.xml -->
 * <property name="extension" viewHint="text-input,admin">
 *  <value type="string">ext-bb-notification-center-ng</value>
 * </property>
 */
import uiBbPaginationKey from 'vendor-bb-uib-pagination';
import uiBbMessageNgKey from 'ui-bb-message-ng';
import uiBbLoadMoreKey from 'ui-bb-load-more-ng';
import uiBbConfirmKey from 'ui-bb-confirm-ng';
import i18nKey from 'ui-bb-i18n-ng';
import uiBbSubstituteErrorNgKey from 'ui-bb-substitute-error-ng';
import uiBbInlineStatusKey from 'ui-bb-inline-status-ng';
import uiBbNotificationStripeKey from 'ui-bb-notification-stripe-ng';
import uiBbLoadingIndicatorKey from 'ui-bb-loading-indicator-ng';
import uiBbDateLabelFilterKey from 'ui-bb-date-label-filter-ng';
import uiBbNotificationsFilterKey from 'ui-bb-notifications-filter-ng';
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

import '../styles/index.scss';

import extHelpers from './helpers';

export const helpers = extHelpers;

export const dependencyKeys = [
  uiBbPaginationKey,
  uiBbMessageNgKey,
  uiBbLoadMoreKey,
  uiBbConfirmKey,
  i18nKey,
  uiBbSubstituteErrorNgKey,
  uiBbInlineStatusKey,
  uiBbNotificationStripeKey,
  uiBbLoadingIndicatorKey,
  uiBbDateLabelFilterKey,
  uiBbNotificationsFilterKey,
  ngAriaModuleKey,
];
