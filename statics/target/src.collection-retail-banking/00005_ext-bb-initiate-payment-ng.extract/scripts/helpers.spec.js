import { transferFrequencies } from './constants';
import helpersGenerator from './helpers';

describe('ui-bb-initiate-payment-ng::helpers', () => {
  const $filter = (filter) => {
    switch (filter) {
      case 'i18n':
        return key => `i18n-${key}`;
      case 'date':
        return date => date.toString();
    }
  };
  const helpers = helpersGenerator({ $filter });

  it('should contain static properties', () => {
    expect(helpers.minOccurrences).toBeNumber();
    expect(helpers.maxOccurences).toBeNumber();
  });

  describe('showCrossCurrencyMessage()', () => {
    it('should return messages if ctrl has rate and from account and amount defined', () => {
      const ctrl = {
        rate: true,
        payment: {
          from: {
            currency: 1,
          },
          amount: {
            value: '1.00',
          },
        },
        paymentPreferences: {
          showExchangeRate: true,
        },
      };

      const result = helpers.showCrossCurrencyMessage(ctrl);

      expect(result).toBeObject();
      expect(result['cross-currency']).toEqual(true);
    });

    it('should return null if showExchangeRate is false', () => {
      const ctrl = {
        rate: true,
        payment: {
          from: {
            currency: 1,
          },
          amount: {
            value: '1.00',
          },
        },
        paymentPreferences: {
          showExchangeRate: false,
        },
      };

      expect(helpers.showCrossCurrencyMessage(ctrl)).toEqual(null);
    });

    it('should return null if ctrl has rate or from account or amount not defined', () => {
      const ctrl = {
        rate: false,
        payment: {
          from: true,
          amount: {
            value: true,
          },
        },
        paymentPreferences: {
          showExchangeRate: true,
        },
      };

      expect(helpers.showCrossCurrencyMessage(ctrl)).toEqual(null);

      ctrl.rate = true;
      ctrl.payment.from = false;
      expect(helpers.showCrossCurrencyMessage(ctrl)).toEqual(null);

      ctrl.payment.from = true;
      ctrl.payment.amount.value = false;
      expect(helpers.showCrossCurrencyMessage(ctrl)).toEqual(null);
    });

    it('should return null if ctrl from account currency and amount have same currency', () => {
      const ctrl = {
        rate: true,
        payment: {
          from: {
            currency: 'test',
          },
          amount: {
            value: true,
            currency: 'test',
          },
        },
        paymentPreferences: {
          showExchangeRate: true,
        },
      };

      expect(helpers.showCrossCurrencyMessage(ctrl)).toEqual(null);
    });
  });

  describe('getAccounts()', () => {
    it('should return initial accounts if "search" is specified', () => {
      const result = helpers.getAccounts('1', [1, 2, 3]);

      expect(result).toBeNonEmptyArray();
      expect(result[1]).toEqual(2);
    });

    it('should return empty array if accounts undefined', () => {
      expect(helpers.getAccounts('', [])).toBeEmptyArray();
    });

    it('should return limited groups array if accounts defined', () => {
      const internalItem = {};
      const externalItem = { external: true };

      const internals = [];
      const externals = [];
      internals.length = externals.length = 10;
      internals.fill(internalItem);
      externals.fill(externalItem);

      const result = helpers.getAccounts('', [].concat(internals, externals));

      expect(result).toBeNonEmptyArray();
      expect(result.length).toEqual(14);
      expect(result[0]).toEqual({ group: 'i18n-ui-bb-credit-suggest-ng.group.internal' });
      expect(result[3]).toEqual({ more: 'i18n-ui-bb-credit-suggest-ng.group.internal' });
      expect(result[4]).toEqual({ group: 'i18n-ui-bb-credit-suggest-ng.group.external', external: true });
      expect(result[13]).toEqual({ external: true });
    });

    it('should return whole accounts list if group was expanded', () => {
      const internalItem = {};
      const externalItem = { external: true };

      const internals = [];
      const externals = [];
      internals.length = externals.length = 10;
      internals.fill(internalItem);
      externals.fill(externalItem);

      const given = [].concat(internals, externals);

      let result = helpers.getAccounts('', given);

      expect(result).toBeNonEmptyArray();
      expect(result.length).toEqual(14);

      helpers.toggleCreditSuggestGroup(
        { stopPropagation: () => { } },
        { more: 'i18n-ui-bb-credit-suggest-ng.group.internal' },
        { open: () => {} },
      );

      result = helpers.getAccounts('', given);
      expect(result).toBeNonEmptyArray();
      expect(result.length).toEqual(20);
      expect(result[9]).toEqual({ less: 'i18n-ui-bb-credit-suggest-ng.group.internal' });
    });
  });

  describe('canSelectUrgentPayment()', () => {
    it('should return correct value depending on control properties', () => {
      const ctrl = {
        payment: {},
        paymentPreferences: {},
      };
      
      expect(helpers.canSelectUrgentPayment(ctrl)).toBeFalse();

      ctrl.payment.from = {};
      expect(helpers.canSelectUrgentPayment(ctrl)).toBeFalse();

      ctrl.paymentPreferences.urgent = {};
      expect(helpers.canSelectUrgentPayment(ctrl)).toBeFalse();

      ctrl.payment.from = { urgentTransferAllowed: true };
      ctrl.paymentPreferences.urgent = false;
      expect(helpers.canSelectUrgentPayment(ctrl)).toBeFalse();

      ctrl.payment.from = { urgentTransferAllowed: true };
      ctrl.paymentPreferences.urgent = true;
      expect(helpers.canSelectUrgentPayment(ctrl)).toBeTrue();
    });
  });

  describe('getScheduleText()', () => {
    const EndingType = {
      AFTER: 'after',
      NEVER: 'never',
      ON: 'on',
    };
    const singleTransfer = {
      value: 'once',
      name: 'Once'
    };

    let payment;
    let dailyFrequency;
    let ctrl;

    beforeEach(() => {
      payment = {
        schedule: {
          startDate: new Date(),
        },
        endingType: EndingType.NEVER,
      };

      dailyFrequency = transferFrequencies[0];

      ctrl = {
        EndingType,
        payment,
        singleTransfer,
      };
    });

    it('should return "Once - Today" if we are dealing with single payment and executing it today', () => {
      payment.schedule.transferFrequency = singleTransfer;

      expect(helpers.getScheduleText(ctrl)).toEqual('i18n-Once - i18n-form.schedule.today');
    });

    it('should return "Daily - Starting today, x times" if "after" option selected', () => {
      payment.schedule.transferFrequency = dailyFrequency;
      payment.endingType = EndingType.AFTER;
      payment.schedule.repeat = 3;

      expect(helpers.getScheduleText(ctrl)).toEqual('i18n-form.schedule.frequency.daily - i18n-form.schedule.starting i18n-form.schedule.today, 3 i18n-form.schedule.repeat.count');
    });

    it('should return "Daily - Starting today" if "never" option selected', () => {
      payment.schedule.transferFrequency = dailyFrequency;
      payment.endingType = EndingType.NEVER;

      expect(helpers.getScheduleText(ctrl)).toEqual('i18n-form.schedule.frequency.daily - i18n-form.schedule.starting i18n-form.schedule.today');
    });

    it('should return "Daily - Starting today, until date" if "on" option selected', () => {
      payment.schedule.transferFrequency = dailyFrequency;
      payment.endingType = EndingType.ON;
      payment.schedule.endDate = new Date();

      const expected = `i18n-form.schedule.frequency.daily - i18n-form.schedule.starting i18n-form.schedule.today, i18n-form.schedule.until ${payment.schedule.endDate}`;
      expect(helpers.getScheduleText(ctrl)).toEqual(expected);
    });
  });

  describe('getFrequencies()', () => {
    it('should return frequencies depending on controller properties', () => {
      const ctrl = {
        singleTransfer: 123,
        paymentPreferences: {},
      };
      
      let result = helpers.getFrequencies(ctrl);
      expect(result).toBeNonEmptyArray();
      expect(result.length).toEqual(1);
      expect(result[0]).toEqual(123);

      ctrl.paymentPreferences.recurring = true;
      result = helpers.getFrequencies(ctrl);
      expect(result).toBeNonEmptyArray();
      expect(result.length).toEqual(6);
      expect(result[0]).toEqual(123);
      expect(result[3].value).toEqual('MONTHLY');
    });
  });

  describe('resetPayment()', () => {
    it('should reset payment and change step', done => {
      const scope = { step: 'edit' };
      const ctrl = {
        resetPayment: jasmine.createSpy('makePayment').and.returnValue(Promise.resolve()),
      };

      helpers.resetPayment(ctrl, scope)
        .then(() => {
          expect(ctrl.resetPayment).toHaveBeenCalled();
          expect(scope.step).toEqual('form');
        })
        .then(done)
        .catch(done.fail);
    });
  });
  
  describe('makePayment()', () => {
    it('should make payment and change step on success', done => {
      const scope = { step: 'edit' };
      const ctrl = {
        makePayment: jasmine.createSpy('makePayment').and.returnValue(Promise.resolve()),
      };

      helpers.makePayment(ctrl, scope)
        .then(() => {
          expect(ctrl.makePayment).toHaveBeenCalled();
          expect(scope.step).toEqual('confirmation');
        })
        .then(done)
        .catch(done.fail);
    });

    it('should make payment and do not change step on failure', done => {
      const scope = { step: 'edit' };
      const ctrl = {
        makePayment: jasmine.createSpy('makePayment').and.returnValue(Promise.reject()),
      };

      helpers.makePayment(ctrl, scope)
        .then(done.fail)
        .catch(() => {
          expect(ctrl.makePayment).toHaveBeenCalled();
          expect(scope.step).toEqual('edit');
        })
        .then(done);
    });
  });
  
  describe('toggleCreditSuggestGroup()', () => {
    it('should call open controller method', () => {
      const event = jasmine.createSpyObj(['stopPropagation']);
      const ctrl = jasmine.createSpyObj(['open']);

      helpers.toggleCreditSuggestGroup(event, {}, ctrl);
      expect(event.stopPropagation).toHaveBeenCalled();
      expect(ctrl.open).toHaveBeenCalled();
    });
  });
  
  describe('onAccountChange()', () => {
    it('should call account change callback and digest scope', done => {
      const scope = {
        $digest: jasmine.createSpy('$digest'),
      };
      const ctrl = {
        onAccountFromChange: jasmine.createSpy('onAccountFromChange').and.returnValue(Promise.resolve()),
      };

      helpers.onAccountChange(ctrl, scope)
        .then(() => {
          expect(ctrl.onAccountFromChange).toHaveBeenCalled();
          expect(scope.$digest).toHaveBeenCalled();
        })
        .then(done)
        .catch(done.fail);
    });

    it('should call account change callback and not digest scope on failure', done => {
      const scope = {
        $digest: jasmine.createSpy('$digest'),
      };
      const ctrl = {
        onAccountFromChange: jasmine.createSpy('onAccountFromChange').and.returnValue(Promise.reject()),
      };

      helpers.onAccountChange(ctrl, scope)
        .then(done.fail)
        .catch(() => {
          expect(ctrl.onAccountFromChange).toHaveBeenCalled();
          expect(scope.$digest).not.toHaveBeenCalled();
        })
        .then(done);
    });
  });
});
