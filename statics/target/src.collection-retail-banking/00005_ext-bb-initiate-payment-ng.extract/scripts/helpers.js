import { transferFrequencies } from './constants';

const crossCurrencyMessages = { 'cross-currency': true };
const minOccurrences = 2;
const maxOccurences = 200;
const expanded = {};
const groupItemsNumber = 4;

/**
 * @description
 * Returns cross currency messages if they should be shown
 *
 * @name showCrossCurrencyMessage
 * @type {function}
 * @param {object} controller Widget controller
 * @returns {object} Cross currency messages
 */
const showCrossCurrencyMessage = $ctrl => {
  const isRateVisible = $ctrl.paymentPreferences.showExchangeRate && $ctrl.rate &&
    $ctrl.payment.from && parseFloat($ctrl.payment.amount.value);
  const isDifferentCurrencies = $ctrl.payment.amount.currency !== $ctrl.payment.from.currency;

  return isRateVisible && isDifferentCurrencies ? crossCurrencyMessages : null;
};

/**
 * @description
 * Checks if layout should show 'Urgent payment' switcher
 *
 * @name canSelectUrgentPayment
 * @type {function}
 * @param {object} $ctrl Widget controller
 * @returns {boolean} True if urget payment switchet should be shown, false otherwise
 */
const canSelectUrgentPayment = $ctrl => {
  const debitAccount = $ctrl.payment.from || {};
  return !!($ctrl.paymentPreferences.urgent && debitAccount && debitAccount.urgentTransferAllowed);
};

/**
 * @description
 * Resets payment order
 *
 * @name resetPayment
 * @type {function}
 * @param {object} $ctrl Widget controller
 * @param {object} scope Template scope
 *
 * @returns {Promise<any>}
 */
const resetPayment = ($ctrl, scope) => $ctrl.resetPayment().then(() => {
  Object.assign(scope, { step: 'form' });
});

/**
 * @description
 * Makes new payment request and changes step on success
 *
 * @name makePayment
 * @type {function}
 * @param {object} $ctrl Payment controller
 * @param {object} scope Parent ng scope
 * @returns {object} Payment request Promise
 */
const makePayment = ($ctrl, scope) => $ctrl.makePayment($ctrl.payment)
  .then(() => {
    Object.assign(scope, { step: 'confirmation' });
  });

/**
 * @description
 * Handler for Account From change action
 *
 * @name onAccountChange
 * @type {function}
 * @param {object} $ctrl Payment controller
 * @param {object} scope Parent ng scope
 * @returns {object} A Promise object
 */
const onAccountChange = ($ctrl, scope) => $ctrl.onAccountFromChange()
  .then(() => (scope.$digest()));

/**
 * @description
 * Toggle group in credit suggest component
 *
 * @name toggleCreditSuggestGroup
 * @type {function}
 * @param {object} event Event object
 * @param {object} model
 * @param {object} ctrl Widget's controller
 */
const toggleCreditSuggestGroup = (event, model, ctrl) => {
  // Prevent suggestion list to be closed
  event.stopPropagation();

  expanded[model.more || model.less] = !!model.more;

  ctrl.open();
};

const helpers = (context) => {
  const i18nFilter = context.$filter('i18n');
  const dateFilter = context.$filter('date');
  const GroupName = {
    INTERNAL: i18nFilter('ui-bb-credit-suggest-ng.group.internal'),
    EXTERNAL: i18nFilter('ui-bb-credit-suggest-ng.group.external'),
  };

  /**
   * @description
   * Transforms accounts list to custom structure
   *
   * @name getAccounts
   * @type {function}
   * @param {string} search
   * @param {array<object>} accounts
   * @returns {array<object>} View accounts
   */
  const getAccounts = (search, accounts) => {
    if (search || !accounts) {
      return [].concat(accounts || []);
    }

    const accountsGroups = {
      [GroupName.INTERNAL]: accounts.filter(account => !account.external),
      [GroupName.EXTERNAL]: accounts.filter(account => account.external),
    };

    if (accountsGroups[GroupName.EXTERNAL].length === 0) {
      return [].concat(accounts || []);
    }

    // add group names
    Object.keys(accountsGroups)
      .forEach(groupName => {
        const group = accountsGroups[groupName];
        const firstAccount = group[0];
        if (firstAccount) {
          group[0] = Object.assign({
            group: groupName,
          }, firstAccount);
        }
      });

    const internalGroup = accountsGroups[GroupName.INTERNAL];
    let viewGroup;

    // add toggle to internal group
    if (expanded[GroupName.INTERNAL]) {
      viewGroup = internalGroup;

      const lastAccount = viewGroup[viewGroup.length - 1];
      if (lastAccount) {
        viewGroup[viewGroup.length - 1] = Object.assign({
          less: GroupName.INTERNAL,
        }, lastAccount);
      }
    } else {
      viewGroup = internalGroup.slice(0, groupItemsNumber);

      if (internalGroup.length > groupItemsNumber) {
        const lastAccount = viewGroup[viewGroup.length - 1];
        if (lastAccount) {
          viewGroup[viewGroup.length - 1] = Object.assign({
            more: GroupName.INTERNAL,
          }, lastAccount);
        }
      }
    }

    return [].concat(viewGroup, accountsGroups[GroupName.EXTERNAL]);
  };

  /**
   * @description
   * Compiles the scheduling description out of payment object params.
   *
   * In this process, following translation keys are being used:
   *
   * form.schedule.starting, for word "Starting"
   *
   * form.schedule.today, for word "Today"
   *
   * form.schedule.on, for word "On" (used before date to form "on 01.01.2017")
   *
   * form.schedule.until, for word "until" (used before date to form "until 01.01.2017")
   *
   * form.schedule.repeat.count, for word "times" (used after repeat count to form "5 times")
   *
   * and name of the transfer frequency set in constants file
   *
   * @name getScheduleText
   * @type {function}
   * @param {object} $ctrl Widget controller
   * @returns {string} Compiled text that can be used as scheduling value in views
   */
  const getScheduleText = $ctrl => {
    const { payment, EndingType, singleTransfer } = $ctrl;
    const words = [];
    const today = (new Date()).setHours(0, 0, 0, 0);
    const startDate = new Date(payment.schedule.startDate);
    const isToday = startDate.setHours(0, 0, 0, 0) === today;
    const multipleOccurrences = payment.schedule.transferFrequency.value !== singleTransfer.value;
    const hasEnd = payment.endingType !== EndingType.NEVER;

    // add frequency
    words.push(i18nFilter(payment.schedule.transferFrequency.name));
    words.push('-');

    // if there are multiple occurrences, add word starting
    if (multipleOccurrences) {
      words.push(i18nFilter('form.schedule.starting'));
    }

    // if start date is today use word instead of date
    if (isToday) {
      let todayString = i18nFilter('form.schedule.today');
      // for multiple occurrences, there is a prefix word, so this on should be lowercase
      if (multipleOccurrences) {
        todayString = todayString.toLowerCase();
      }

      words.push(todayString);
    }

    // for single transfer in the future, we need prefix word on
    if (!multipleOccurrences && !isToday) {
      words.push(i18nFilter('form.schedule.on').toLowerCase());
    }

    if (!isToday) {
      words.push(dateFilter(payment.schedule.startDate));
    }

    const hasLimitedOccurences = hasEnd && multipleOccurrences;
    const isMinOccurences = !payment.schedule.repeat || payment.schedule.repeat < minOccurrences;

    // if there are limited number of occurrences,
    // add comma for the last word, so that there is no space between them
    if (hasLimitedOccurences && !(payment.endingType === EndingType.AFTER && isMinOccurences)) {
      words[words.length - 1] += ',';
    }

    if (hasEnd && multipleOccurrences) {
      // there is an end for this schedule
      // based on the type of ending construct words differently
      if (payment.endingType === EndingType.ON) {
        words.push(i18nFilter('form.schedule.until'));
        words.push(dateFilter(payment.schedule.endDate));
      } else if (payment.schedule.repeat >= minOccurrences) {
        words.push(payment.schedule.repeat);
        words.push(payment.schedule.repeat ? i18nFilter('form.schedule.repeat.count') : '');
      }
    }

    // output everything together
    return words.join(' ');
  };

  /**
   * @description
   * Returns frequencies for payment depending on occurences number
   *
   * @name getFrequencies
   * @type {function}
   * @param {object} $ctrl Widget controller
   * @returns {array<object>} Array of transfer frequencies
   */
  const getFrequencies = $ctrl => [$ctrl.singleTransfer].concat(
    $ctrl.paymentPreferences.recurring ? transferFrequencies : []);

  return {
    minOccurrences,
    maxOccurences,
    showCrossCurrencyMessage,
    getAccounts,
    canSelectUrgentPayment,
    getScheduleText,
    getFrequencies,
    resetPayment,
    makePayment,
    toggleCreditSuggestGroup,
    onAccountChange,
  };
};

export default helpers;
