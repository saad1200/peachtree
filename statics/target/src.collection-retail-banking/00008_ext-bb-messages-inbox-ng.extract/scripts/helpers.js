const helpers = {
  /**
   * Removes selected conversations and updates conversation list
   * @name removeSelectedConversations
   * @type {function}
   * @param  {Object} $ctrl The extension controller
   * @return {Promise}       Promise to be fulfilled after loading list
   */
  removeSelectedConversations: ($ctrl) =>
    $ctrl.removeSelectedItems()
      .then(() => {
        const { currentPage, totalCount } = $ctrl.state.currentFolder;
        const pageSize = $ctrl.config.pageSize;

        if (totalCount > 0) {
          const maxPage = Math.ceil(totalCount / pageSize);
          // only the last page can dissapear after conversations are deleted
          const newPage = Math.min(currentPage, maxPage);

          return $ctrl.loadPage($ctrl.state.currentView, newPage);
        }
        return Promise.resolve();
      }),
};

export default helpers;
