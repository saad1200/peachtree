/**
 * @module ext-bbm-contact-list-ng
 *
 * @description
 * Mobile extension for a contact list in the Contacts widget.
 *
 * @example
 * <!-- Contact widget model.xml -->
 * <property name="extension" viewHint="text-input,admin">
 *   <value type="string">ext-bbm-contact-list-ng</value>
 * </property>
 */
import uiBbAvatarKey from 'ui-bb-avatar-ng';
import uiBbmListKey from 'ui-bbm-list-ng';
import uiBbInlineStatusKey from 'ui-bb-inline-status-ng';
import i18nNgKey from 'ui-bb-i18n-ng';

import '../styles/index.css';
import * as extHooks from './hooks';

export const hooks = extHooks;

export const dependencyKeys = [
  uiBbAvatarKey,
  uiBbmListKey,
  uiBbInlineStatusKey,
  i18nNgKey,
];
