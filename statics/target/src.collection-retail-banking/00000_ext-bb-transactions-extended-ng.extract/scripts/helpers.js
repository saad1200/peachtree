import { negativeSignKey } from './debit-credit-sign';
import CATEGORY_CLASS_PREFIX, {
  uncategorizedIconClass as DEFAULT_ICON_CLASS,
} from './constants';

export const getSignedAmount = (transaction) =>
  transaction.amount * (transaction.creditDebitIndicator === negativeSignKey ? -1 : 1);

export const headers = [
  {
    label: 'details.label.date',
    class: 'col-sm-1',
    sortable: {
      key: 'bookingDate',
      direction: 'DESC',
    },
  },
  {
    label: 'details.label.category',
    class: 'col-sm-2',
    sortable: {
      key: 'category',
      direction: 'ASC',
    },
  },
  {
    label: 'details.label.description',
    class: 'col-sm-5',
    sortable: {
      key: 'description',
      direction: 'ASC',
    },
  },
  {
    label: 'details.label.creditAmount',
    class: 'col-sm-2 text-right',
    sortable: {
      key: 'amount',
      direction: 'ASC',
    },
  },
  {
    label: 'details.label.debitAmount',
    class: 'col-sm-2 text-right',
    sortable: {
      key: 'amount',
      direction: 'ASC',
    },
  },
];

export const isSortableActive = (sortable, sort) =>
  sortable.key === sort.orderBy;

/**
 * @description
 * Checks if actual pagination type matches the one, defined in properties
 *
 * @public
 * @name isPaginationTypeMatch
 * @type {function}
 * @param {function} $ctrl      Current controller
 * @param {string} type         Description of pagination type (pagination, load-more)
 *
 * @returns {boolean}
 */
export const isPaginationTypeMatch = ($ctrl, type) =>
  $ctrl.state.pageParams.paginationType === type;

/**
 * @description
 * Converts transaction category name into category CSS icon class
 *
 * @name getCategoryIconClass
 * @type {function}
 * @param {string} transactionCategory Transaction category
 * @returns {string}
 */
export const getCategoryIconClass = (transactionCategory) =>
  `${CATEGORY_CLASS_PREFIX}${transactionCategory.toLowerCase()
    .replace(/\W/g, '-').replace(/-{2,}/g, '-') || DEFAULT_ICON_CLASS}`;
