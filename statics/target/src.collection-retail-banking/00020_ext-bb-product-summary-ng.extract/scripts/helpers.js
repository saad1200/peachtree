const helpers = (context) => {
  const i18nFilter = context.$filter('i18n');
  const productAdditions = [];

  return {
    /**
     * @description
     * Builds AdditionalInfo array out of ProductView object.
     * Result can be passed to account card component
     *
     * @name buildAdditionalInfo
     * @type {function}
     * @param {ProductView} product
     * @returns {AdditionalInfo[]}
     */
    buildAdditionalInfo: product => {
      if (productAdditions[product.id]) {
        return productAdditions[product.id];
      }

      const additional = [];
      const pushAddition = (name, amount, currency) => {
        additional.push({
          name: i18nFilter(name),
          amount,
          currency,
        });
      };

      if (product.secondaryLabel || product.secondaryValue) {
        pushAddition(product.secondaryLabel, product.secondaryValue, product.currency);
      }

      if (product.tertiaryLabel || product.tertiaryValue) {
        pushAddition(product.tertiaryLabel, product.tertiaryValue, product.currency);
      }

      productAdditions[product.id] = additional;
      return additional;
    },
  };
};

export default helpers;

/**
 * @typedef {Object} AdditionalInfo
 * @property {?string} name Additional info label
 * @property {?number} amount Additional info row's amount
 * @property {?string} currency ISO currency code
 */
