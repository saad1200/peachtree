import getStyle from 'lib-bb-styles';
import { periodToDate, getDefaultPeriod } from './helpers';

/**
 * @name defaultPeriodStart
 * @type {function}
 *
 * @description
 * Sets period start property on init
 *
 * @returns {string} Start period string in format yyyy-mm-dd
 */
const defaultPeriodStart = () => periodToDate(getDefaultPeriod());

/**
 * @name defaultInterval
 * @type {function}
 *
 * @description
 * Sets interval property on init
 *
 * @param {Interval} interval Available intervals
 * @returns {string} One of the available intervals
 */
const defaultInterval = (interval) => interval.MONTH;

/**
 * @name processTurnoverSeries
 * @type {function}
 *
 * @description
 * Default hook for turnovers chart series object post processing
 *
 * @param {BBSeries} series chart series data
 * @param {Turnover} data original turnover object
 * @returns {TurnoversBBSeries} processed series
 */
const processTurnoverSeries = (series, data) => {
  series.datasets.forEach((dataset, index) => {
    const datasetColor = getStyle(`.chart-bar-dataset-${index}`, 'color');
    Object.assign(dataset, {
      backgroundColor: datasetColor,
      hoverBackgroundColor: datasetColor,
    });
  });

  return Object.assign(series, {
    datasets: series.datasets.reverse(),
    original: data,
    updated: true,
  });
};

/**
 * @name processLoadError
 * @type {function}
 *
 * @description
 * Overwrite the default hook and don't return passed error
 *
 * @param {error} The error passed
 * @returns {string} The actual error
 */
const processLoadError = () => null;

export default {
  processTurnoverSeries,
  defaultPeriodStart,
  defaultInterval,
  processLoadError,
};
