/**
 * @module ext-bb-turnovers-ng
 *
 * @description
 * Default extension for widget-bb-turnovers-ng
 *
 * @requires vendor-bb-angular-ng-aria
 * @requires ui-bb-i18n-ng
 * @requires ui-bb-substitute-error-ng
 * @requires ui-bb-dropdown-select
 * @requires ui-bb-format-amount
 * @requires ui-bb-chartjs-chart-bar-ng
 * @requires ui-bb-empty-state-ng
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';
import i18nNgKey from 'ui-bb-i18n-ng';
import uiBbSubstituteErrorNgKey from 'ui-bb-substitute-error-ng';
import uiBbDropdownSelect from 'ui-bb-dropdown-select';
import uiBbFormatAmount from 'ui-bb-format-amount';
import uiBbChartjsChartBarNgKey from 'ui-bb-chartjs-chart-bar-ng';
import uiBbEmptyStateKey from 'ui-bb-empty-state-ng';

import extHooks from './hooks';
import extHelpers from './helpers';

export const dependencyKeys = [
  i18nNgKey,
  ngAriaModuleKey,
  uiBbSubstituteErrorNgKey,
  uiBbDropdownSelect,
  uiBbFormatAmount,
  uiBbChartjsChartBarNgKey,
  uiBbEmptyStateKey,
];

export const hooks = extHooks;
export const helpers = extHelpers;
export const events = {};

/**
 * Turnover response object
 * @typedef {object} Turnover
 * @property {string} arrangementId Id of the arrangement this turnover belongs to
 * @property {string} intervalDuration Duration of intervals returned
 * @property {TurnoverItem[]} turnovers Array of turnover items
 */

/**
 * Turnover response item
 * @typedef {object} TurnoverItem
 * @property {string} intervalStartDate Date in ISO format (2016-06-01T16:41:41.090Z)
 * @property {object} debitAmount Debit amount object
 * @property {string} debitAmount.currencyCode Debit amount currency code (ISO)
 * @property {number} debitAmount.amount Debit amount value
 * @property {object} creditAmount Credit amount object
 * @property {string} creditAmount.currencyCode Credit amount currency code (ISO)
 * @property {number} creditAmount.amount Credit amount value
 * @property {object} balance Debit and credit difference object
 * @property {string} balance.currencyCode Debit and credit difference currency code (ISO)
 * @property {number} balance.amount Debit and credit difference value
 */

/**
 * BBSeries data object used to draw charts
 * @typedef {object} BBSeries
 * @property {string[]} labels Array of x axis labels
 * @property {Dataset[]} datasets Array of all y axis value datasets
 */

/**
 * Turnovers specific BBSeries object
 * @typedef {object} TurnoversBBSeries
 * @property {string[]} labels Array of x axis labels
 * @property {TurnoversDataset[]} datasets Array of all y axis value datasets
 * @property {Turnover} original Original turnover object
 * @property {boolean} updated Flag that signals that series are processed by hook
 */

/**
 * Dataset object for y axis data
 * @typedef {object} Dataset
 * @property {number[]} data Array of data points to be drawn for each label
 */

/**
 * Turnovers specific dataset object for y axis
 * @typedef {object} TurnoversDataset
 * @extends Dataset
 * @property {string} backgroundColor Background color of bars that represent this dataset
 * @property {string} hoverBackgroundColor Hover color of bars that represent this dataset
 */

/**
 * Settings object with options available for bar chart.
 * More info {@link http://www.chartjs.org/docs/latest/charts/bar.html}
 * @typedef {object} ChartjsSettings
 */
