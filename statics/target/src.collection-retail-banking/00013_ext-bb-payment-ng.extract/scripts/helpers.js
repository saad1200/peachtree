import transferFrequencies from './constants';

const helpers = (context) => {
  const dateFilter = context.$filter('date');
  const i18nFilter = context.$filter('i18n');
  const lowercaseFilter = context.$filter('lowercase');
  const expanded = {};
  const groupItemsNumber = 4;
  const groupNames = {
    INTERNAL: i18nFilter('ui-bb-credit-suggest-ng.group.internal'),
    EXTERNAL: i18nFilter('ui-bb-credit-suggest-ng.group.external'),
  };
  const crossCurrencyMessages = { 'cross-currency': true };

  const minOccurrences = 2;
  const maxOccurences = 200;

  const specialSymbols = "/-?:().,'+";
  const escapedSpecialSymbols = specialSymbols.split('').map((a) => `|\\${a}`).join('');
  const paymentReferencePattern = `^(\\w|\\d| ${escapedSpecialSymbols})*$`;

  return {
    minOccurrences,
    maxOccurences,
    paymentReferencePattern,

    /**
     * @description
     * Extend default single payment transfer frequency with extension specific frequencies
     *
     * @name extendFrequencies
     * @type {function}
     * @param {object} Single transfer object from widget controller
     * @param {object} Payment preferences obtained from widget
     * @returns {array} Array with all frequencies available
     */
    extendFrequencies: (singleTransfer, paymentPreferences) =>
      [singleTransfer].concat(paymentPreferences.recurring ? transferFrequencies : []),

    /**
     * @description
     * Compiles the scheduling description out of payment object params.
     *
     * In this process, following translation keys are being used:
     *
     * form.schedule.starting, for word "Starting"
     *
     * form.schedule.today, for word "Today"
     *
     * form.schedule.on, for word "On" (used before date to form "on 01.01.2017")
     *
     * form.schedule.until, for word "until" (used before date to form "until 01.01.2017")
     *
     * form.schedule.repeat.count, for word "times" (used after repeat count to form "5 times")
     *
     * and name of the transfer frequency set in constants file
     *
     * @name getScheduleText
     * @type {function}
     * @param {object} controller PaymentController
     * @returns {string} Compiled text that can be used as scheduling value in views
     */
    getScheduleText: ({
      payment,
      endingTypes,
      singleTransfer,
    }) => {
      const words = [];
      const now = new Date();
      const multipleOccurrences = payment.schedule.transferFrequency.value !== singleTransfer.value;
      const isToday = payment.schedule.startDate.setHours(0, 0, 0, 0) === now.setHours(0, 0, 0, 0);
      const hasEnd = payment.endingType !== endingTypes.NEVER;

      // add frequency
      words.push(i18nFilter(payment.schedule.transferFrequency.name));
      words.push('-');

      // if there are multiple occurrences, add word starting
      if (multipleOccurrences) {
        words.push(i18nFilter('form.schedule.starting'));
      }

      // if start date is today use word instead of date
      if (isToday) {
        let todayString = i18nFilter('form.schedule.today');
        // for multiple occurrences, there is a prefix word, so this on should be lowercase
        if (multipleOccurrences) {
          todayString = lowercaseFilter(todayString);
        }

        words.push(todayString);
      }

      // for single transfer in the future, we need prefix word on
      if (!multipleOccurrences && !isToday) {
        words.push(lowercaseFilter(i18nFilter('form.schedule.on')));
      }

      if (!isToday) {
        words.push(dateFilter(payment.schedule.startDate));
      }

      // if there are limited number of occurrences,
      // add comma for the last word, so that there is no space between them
      if (hasEnd && multipleOccurrences &&
        !(payment.endingType === endingTypes.AFTER &&
           (!payment.schedule.repeat || payment.schedule.repeat < minOccurrences))) {
        words[words.length - 1] += ',';
      }

      if (hasEnd && multipleOccurrences) {
        // there is an end for this schedule
        // based on the type of ending construct words differently
        if (payment.endingType === endingTypes.ON) {
          words.push(i18nFilter('form.schedule.until'));
          words.push(dateFilter(payment.schedule.endDate));
        } else if (payment.schedule.repeat >= minOccurrences) {
          words.push(payment.schedule.repeat);
          words.push(payment.schedule.repeat ? i18nFilter('form.schedule.repeat.count') : '');
        }
      }

      // output everything together
      return words.join(' ');
    },

    /**
     * @description
     * Toggle group in credit suggest component
     *
     * @name toggle
     * @type {function}
     * @param {object} event
     * @param {object} model
     * @param {object} controller
     */
    toggle: (event, model, { open }) => {
      // Prevent suggestion list to be closed
      event.stopPropagation();

      expanded[model.more || model.less] = !!model.more;

      open();
    },

    /**
     * @description
     * Transforms accounts list to custom structure
     *
     * @name getAccounts
     * @type {function}
     * @param {string} search
     * @param {array<object>} accounts
     * @returns {array<object>} viewAccounts
     */
    getAccounts: (search, accounts) => {
      if (search || !accounts) {
        return [].concat(accounts || []);
      }

      const accountsGroups = {
        [groupNames.INTERNAL]: accounts.filter(account => !account.external),
        [groupNames.EXTERNAL]: accounts.filter(account => account.external),
      };

      if (accountsGroups[groupNames.EXTERNAL].length === 0) {
        return [].concat(accounts || []);
      }

      // add group names
      const groups = Object.keys(accountsGroups);
      groups.forEach((groupName) => {
        const group = accountsGroups[groupName];
        const firstAccount = group[0];
        if (firstAccount) {
          group[0] = Object.assign({
            group: groupName,
          }, firstAccount);
        }
      });

      // add toggle to internal group
      const internalGroup = accountsGroups[groupNames.INTERNAL];
      let viewGroup;
      if (expanded[groupNames.INTERNAL]) {
        viewGroup = internalGroup;
        const lastAccount = viewGroup[viewGroup.length - 1];
        if (lastAccount) {
          viewGroup[viewGroup.length - 1] = Object.assign({
            less: groupNames.INTERNAL,
          }, lastAccount);
        }
      } else {
        viewGroup = internalGroup.slice(0, groupItemsNumber);
        if (internalGroup.length > groupItemsNumber) {
          const lastAccount = viewGroup[viewGroup.length - 1];
          if (lastAccount) {
            viewGroup[viewGroup.length - 1] = Object.assign({
              more: groupNames.INTERNAL,
            }, lastAccount);
          }
        }
      }

      return [].concat(viewGroup, accountsGroups[groupNames.EXTERNAL]);
    },

    /**
     * @description
     * If cross currency message should be shown
     *
     * @name showCrossCurrencyMessage
     * @type {function}
     * @param {object} controller
     * @returns {object} messages
     */
    showCrossCurrencyMessage: $ctrl => (
        (
          $ctrl.paymentPreferences.showExchangeRate &&
          $ctrl.rate &&
          $ctrl.payment.from &&
          parseFloat($ctrl.payment.amount.value) &&
          $ctrl.payment.amount.currency !== $ctrl.payment.from.currency
        ) ?
        crossCurrencyMessages :
        null
      ),

    /**
     * @description
     * Makes new payment request and changes step on success
     *
     * @name makePayment
     * @type {function}
     * @param {object} $ctrl Payment controller
     * @param {object} $parent Parent ng scope
     * @returns {object} Payment request Promise
     */
    makePayment: ($ctrl, $parent) => $ctrl.makePayment($ctrl.payment)
      .then(() => {
        // eslint-disable-next-line no-param-reassign
        $parent.step = 'confirmation';
      }),
  };
};

export default helpers;
