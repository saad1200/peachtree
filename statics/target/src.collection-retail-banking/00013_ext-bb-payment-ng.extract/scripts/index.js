/**
 * @module ext-bb-payment-ng
 *
 * @description
 * Payment default extension.
 *
 * @requires ui-bb-account-card
 * @requires ui-bb-account-selector
 * @requires ui-bb-confirm-ng
 * @requires ui-bb-dropdown-select
 * @requires ui-bb-substitute-error-ng
 * @requires vendor-bb-uib-alert
 * @requires vendor-bb-angular-ng-messages
 * @requires ui-bb-expandable-ng
 * @requires ui-bb-currency-input-ng
 * @requires ui-bb-calendar-popup-ng
 * @requires ui-bb-credit-suggest-ng
 * @requires ui-bb-switcher-ng
 * @requires ui-bb-loading-indicator-ng
 * @requires ui-bb-i18n-ng
 * @requires ui-bb-parent-responsiveness-ng
 * @requires vendor-bb-uib-popover
 * @requires vendor-bb-angular-ng-aria
 *
 * @example
 * <!-- payment widget model.xml -->
 * <property name="extension" viewHint="text-input,admin">
 *  <value type="string">ext-bb-payment-ng</value>
 * </property>
 *
 * Usage of ui-bb-account-card component in template
 *
 * <ui-bb-account-card
 *   account-name="$ctrl.payment.from.name"
 *   account-number="$ctrl.payment.from.identifier"
 *   amount="$ctrl.payment.from.amount"
 *   currency="$ctrl.payment.from.currency"
 *   show-avatar="true">
 * </ui-bb-account-card>
 *
 * Usage of ui-bb-account-selector component in template
 *
 * <ui-bb-account-selector
 *   ng-model="$ctrl.payment.from"
 *   accounts="$ctrl.accountsFrom"
 *   ng-change="$ctrl.onAccountFromChange()"
 *   custom-template-id="ui-bb-account-selector/option-template.htm"
 *   required>
 * </ui-bb-account-selector>
 *
 * where
 * accounts {array} List of accounts
 * ng-change {function} Method that will be executed when product selection is changed
 * custom-template-id {string} Id of the template that will be used for rendering
 * instead of default one
 *
 * Usage of ui-bb-currency-input-ng component in template
 *
 * <ui-bb-currency-input-ng
 *   max-length="6"
 *   decimal-max-length="2"
 *   placeholder="0"
 *   ng-model="$ctrl.payment.amount"
 *   currencies="$ctrl.currencies">
 * </ui-bb-currency-input-ng>
 *
 * where
 * max-length {number} Maximum number of digits allowed in whole part
 * decimal-max-length {number} Maximum number of digits allowed in decimal part
 * placeholder {string} Text to display for input's placeholder
 * currencies {string[]} List of currencies available
 *
 * Usage of ui-bb-credit-suggest-ng component in template
 *
 * <ui-bb-credit-suggest-ng
 *   name="credit"
 *   ng-model="$ctrl.payment.to"
 *   accounts="$ctrl.accountsTo"
 *   get-accounts="ext.helpers.getAccounts(search, accounts)"
 *   messages="{
 *     noResults: ('ui-bb-credit-suggest-ng.filter.noResults' | i18n),
 *   }"
 *   allow-external="$ctrl.payment.from.externalTransferAllowed"
 *   custom-template-id="ui-bb-credit-suggest-ng/template/option.html"
 *   required>
 * </ui-bb-credit-suggest-ng>
 *
 * where
 * accounts {array} List of accounts to filter and select with user input
 * get-accounts {function} External method for transform accounts array into custom structure
 * Can be defined in extensions helpers
 * messages {object} Localized messages
 * allow-external {boolean} Are external accounts included in list.
 * If not, IBAN field stays disabled
 * custom-template-id {string} Id of the template that will be used for rendering
 * instead of default one
 */
/* eslint-disable import/prefer-default-export */
import uiBbAccountCard from 'ui-bb-account-card';
import uiBbAccountSelector from 'ui-bb-account-selector';
import uiBbConfirmNgKey from 'ui-bb-confirm-ng';
import uiBbDropdownSelectKey from 'ui-bb-dropdown-select';
import uiBbSubstituteErrorNgKey from 'ui-bb-substitute-error-ng';
import vendorBbUibAlertKey from 'vendor-bb-uib-alert';
import vendorBbAngularNgMessagesKey from 'vendor-bb-angular-ng-messages';
import uiBbExpandableNg from 'ui-bb-expandable-ng';
import uiBbCurrencyInputNgKey from 'ui-bb-currency-input-ng';
import uiBbCalendarPopupNgKey from 'ui-bb-calendar-popup-ng';
import uiBbCreditSuggestNgKey from 'ui-bb-credit-suggest-ng';
import uiBbSwitcherNgKey from 'ui-bb-switcher-ng';
import uiBbLoadingIndicatorKey from 'ui-bb-loading-indicator-ng';
import i18nNgKey from 'ui-bb-i18n-ng';
import uiBbParentResponsivenessNg from 'ui-bb-parent-responsiveness-ng';
import uibPopoverKey from 'vendor-bb-uib-popover';
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';
import uiBbNumberInputNgKey from 'ui-bb-number-input-ng';

import extHelpers from './helpers';
import * as extHooks from './hooks';

import '../styles/index.css';

export const dependencyKeys = [
  uiBbAccountCard,
  uiBbAccountSelector,
  uiBbConfirmNgKey,
  uiBbDropdownSelectKey,
  uiBbSubstituteErrorNgKey,
  vendorBbUibAlertKey,
  vendorBbAngularNgMessagesKey,
  uiBbExpandableNg,
  uiBbCurrencyInputNgKey,
  uiBbCalendarPopupNgKey,
  uiBbCreditSuggestNgKey,
  uiBbSwitcherNgKey,
  uiBbLoadingIndicatorKey,
  i18nNgKey,
  uiBbParentResponsivenessNg,
  uibPopoverKey,
  ngAriaModuleKey,
  uiBbNumberInputNgKey,
];

export const helpers = extHelpers;
export const hooks = extHooks;
