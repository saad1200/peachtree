import helpersGenerator from './helpers';
import transferFrequencies from './constants';

describe('helpers', () => {
  const translation = 'translation';
  const lowercase = 'lowercase';
  const date = 'date';
  const $filter = (filter) => {
    switch (filter) {
      case 'i18n':
        return () => translation;
      case 'lowercase':
        return () => lowercase;
      case 'date':
      default:
        return () => date;
    }
  }

  const helpers = helpersGenerator({
    $filter,
  });

  let ctrl;
  let payment;
  let actual;
  let expected;
  let dailyFrequency;

  describe('getScheduleText() should return', () => {
    const endingTypes = {
      AFTER: 'after',
      NEVER: 'never',
      ON: 'on',
    };

    const singleTransfer = {
      value: 'once',
    };

    const now = new Date();

    beforeEach(() => {
      payment = {
        schedule: {
          startDate: now,
        },
        endingType: endingTypes.NEVER,
      };

      dailyFrequency = transferFrequencies[0];

      ctrl = {
        payment,
        endingTypes,
        singleTransfer
      };
    });

    it('"Once - Today" if we are dealing with single payment and executing it today', () => {
      payment.schedule.transferFrequency = singleTransfer;

      actual = helpers.getScheduleText(ctrl);
      expected = `${translation} - ${translation}`;

      expect(actual).toEqual(expected);
    });

    it('"Daily - Starting today, x times" if "after" option selected', () => {
      payment.schedule.transferFrequency = dailyFrequency;
      payment.endingType = endingTypes.AFTER;
      payment.schedule.repeat = 3;

      expected = `${translation} - ${translation} ${lowercase}, 3 ${translation}`;
      actual = helpers.getScheduleText(ctrl);

      expect(actual).toEqual(expected);
    });

    it('"Daily - Starting today" if "never" option selected', () => {
      payment.schedule.transferFrequency = dailyFrequency;
      payment.endingType = endingTypes.NEVER;

      expected = `${translation} - ${translation} ${lowercase}`;
      actual = helpers.getScheduleText(ctrl);

      expect(actual).toEqual(expected);
    });

    it('"Daily - Starting today, until date" if "on" option selected', () => {
      payment.schedule.transferFrequency = dailyFrequency;
      payment.endingType = endingTypes.ON;
      payment.schedule.endDate = new Date();

      expected = `${translation} - ${translation} ${lowercase}, ${translation} ${date}`;
      actual = helpers.getScheduleText(ctrl);

      expect(actual).toEqual(expected);
    });
  });

  describe('getAccounts() should return', () => {
    it('initial accounts if "search" is specified', () => {
      expected = [1, 2, 3];
      actual = helpers.getAccounts('1', expected);

      expect(actual).toEqual(expected);
    });

    it('empty array if accounts undefined', () => {
      expected = [];
      actual = helpers.getAccounts('', undefined);

      expect(actual).toEqual(expected);
    });

    it('limited groups array if accounts defined', () => {
      const internalItem = {};
      const externalItem = { external: true };

      const internals = [];
      const externals = [];
      internals.length = externals.length = 10;
      internals.fill(internalItem);
      externals.fill(externalItem);

      const given = [].concat(internals, externals);
      const getAccountsFilter = () => a => a;
      const getAccountsHelpers = helpersGenerator({
        $filter: getAccountsFilter,
      });

      expected = [];
      actual = getAccountsHelpers.getAccounts('', given);

      expect(actual.length).toEqual(14);
      expect(actual[0]).toEqual({ group: 'ui-bb-credit-suggest-ng.group.internal' });
      expect(actual[3]).toEqual({ more: 'ui-bb-credit-suggest-ng.group.internal' });
      expect(actual[4]).toEqual({ group: 'ui-bb-credit-suggest-ng.group.external', external: true });
      expect(actual[13]).toEqual({ external: true });
    });

    it('whole accounts list if group was expanded', () => {
      const internalItem = {};
      const externalItem = { external: true };

      const internals = [];
      const externals = [];
      internals.length = externals.length = 10;
      internals.fill(internalItem);
      externals.fill(externalItem);

      const given = [].concat(internals, externals);
      const getAccountsFilter = () => a => a;
      const getAccountsHelpers = helpersGenerator({
        $filter: getAccountsFilter,
      });

      expected = [];
      actual = getAccountsHelpers.getAccounts('', given);

      expect(actual.length).toEqual(14);

      getAccountsHelpers.toggle(
        { stopPropagation: () => { } },
        { more: 'ui-bb-credit-suggest-ng.group.internal' },
        { open: () => { } },
      );

      actual = getAccountsHelpers.getAccounts('', given);
      expect(actual.length).toEqual(20);
      expect(actual[9]).toEqual({ less: 'ui-bb-credit-suggest-ng.group.internal' });
    });
  });

  describe('showCrossCurrencyMessage() should return', () => {
    it('messages if ctrl has rate and from account and amount defined', () => {
      ctrl = {
        rate: true,
        payment: {
          from: {
            currency: 1,
          },
          amount: {
            value: '1.00',
          },
        },
        paymentPreferences: {
          showExchangeRate: true,
        },
      };

      actual = helpers.showCrossCurrencyMessage(ctrl);

      expect(actual['cross-currency']).toEqual(true);
    });

    it('null if showExchangeRate is false', () => {
      ctrl = {
        rate: true,
        payment: {
          from: {
            currency: 1,
          },
          amount: {
            value: '1.00',
          },
        },
        paymentPreferences: {
          showExchangeRate: false,
        },
      };

      actual = helpers.showCrossCurrencyMessage(ctrl);

      expect(actual).toEqual(null);
    });

    it('null if ctrl has rate or from account or amount not defined', () => {
      ctrl = {
        rate: true,
        payment: {
          from: true,
          amount: {
            value: true,
          },
        },
        paymentPreferences: {
          showExchangeRate: true,
        },
      };

      ctrl.rate = false;
      actual = helpers.showCrossCurrencyMessage(ctrl);
      expect(actual).toEqual(null);

      ctrl.rate = true;
      ctrl.payment.from = false;
      actual = helpers.showCrossCurrencyMessage(ctrl);
      expect(actual).toEqual(null);

      ctrl.payment.from = true;
      ctrl.payment.amount.value = false;
      actual = helpers.showCrossCurrencyMessage(ctrl);
      expect(actual).toEqual(null);
    });

    it('null if ctrl from account currency and amount have same currency', () => {
      ctrl = {
        rate: true,
        payment: {
          from: {
            currency: 'test',
          },
          amount: {
            value: true,
            currency: 'test',
          },
        },
        paymentPreferences: {
          showExchangeRate: true,
        },
      };

      actual = helpers.showCrossCurrencyMessage(ctrl);
      expect(actual).toEqual(null);
    });
  });

  describe('payment reference pattern', () => {
    const allSpecialSymbols = `!\"#$%&'()*+,-./:;<=>?[\\]^_{|}~`;
    const specialSymbols = "/-?:().,'+";
    const specialSymbolsArr = specialSymbols.split('');
    const regexp = new RegExp(helpers.paymentReferencePattern);

    it('should only allow letters, numbers and specified special symbols', () => {
      const letters = "abcdefghijklmnopqrstuvwxyz";
      const numbers = "0123456789";
      const allowedChars = letters.toUpperCase() + letters + numbers + specialSymbols;
      expect(regexp.test(allowedChars)).toBe(true);
    });

    it('should not allow other special symbols', () => {
      const disallowedSymbols = allSpecialSymbols.split('')
        .filter((c) => specialSymbolsArr.indexOf(c) < 0).join('');

      expect(disallowedSymbols.split('')
        .find((c) => regexp.test(disallowedSymbols))).toBeUndefined();
    });
  });

  describe('makePayment', () => {
    beforeEach(() => {
      ctrl = jasmine.createSpyObj(['makePayment']);
    });

    it('should make pament and change step on success', done => {
      const scope = {};
      ctrl.makePayment.and.returnValue(Promise.resolve());

      helpers.makePayment(ctrl, scope)
        .then(() => {
          expect(ctrl.makePayment).toHaveBeenCalled();
          expect(scope.step).toEqual('confirmation');
        })
        .then(done)
        .catch(done.fail);
    });

    it('should make pament and do not change step on failure', done => {
      const scope = {};
      ctrl.makePayment.and.returnValue(Promise.reject());

      helpers.makePayment(ctrl, scope)
        .then(done.fail)
        .catch(() => {
          expect(ctrl.makePayment).toHaveBeenCalled();
          expect(scope.step).toBeUndefined();
        })
        .then(done);
    });
  });
});
