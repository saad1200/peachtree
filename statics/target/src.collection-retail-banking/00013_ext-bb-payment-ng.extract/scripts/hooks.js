/**
 * @name processAccountsTo
 * @description
 * Hook for processing credit account list.
 *
 * @type {function}
 * @param debitAccount {ProductKind} Selected debit account (can be null)
 * @param getCreditAccounts {function} Function to retrieve all credit accounts
 * @param getExternalContacts {function} Function to retrieve all external contacts
 * formatted like Product kind
 * @returns {Promise.<any[]>} Promise that retrieves array of accounts.
 */
export function processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts) {
  return getCreditAccounts(debitAccount.id || null).then(accounts => {
    if (!debitAccount.id || debitAccount.externalTransferAllowed) {
      return getExternalAccounts().then(contacts =>
        accounts.concat(contacts)
      );
    }
    return accounts;
  });
}

/**
 * @name getRecurringTransactionDay
 * @type {function}
 *
 * @description
 * Denotes day on which transfer should be executed.
 *
 * For weekly it will be 1..7 indicating weekday.
 *
 * For monthly it will be 1..31 indicating day of month.
 *
 * For yearly it will be 1..12 indicating month of the year
 *
 * Assigned to [$ctrl.schedule.on]{@link PaymentController#makePayment}
 *
 * @param startDate {Date} Start date of recurring payment
 * @param transferFrequency {object} Recurring frequency
 * @returns {number}
 */
export function getRecurringTransactionDay(startDate, transferFrequency) {
  let transactionDay;
  switch (transferFrequency.value) {
    case 'YEARLY':
      transactionDay = startDate.getMonth() + 1;
      break;
    case 'MONTHLY':
      transactionDay = startDate.getDate();
      break;
    case 'WEEKLY':
    default:
      transactionDay = startDate.getDay() + 1;
      break;
  }

  return transactionDay;
}
