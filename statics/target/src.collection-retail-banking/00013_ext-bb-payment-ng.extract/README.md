# ext-bb-payment-ng

Default extension for widget-bb-payment-ng

## Special note according "Urgent" payments

In case a developer intends to utilize the __urgent__ payments functionality, the only step to be done is adding the switcher (or checkbox) to layout. Mentioned switcher should manage `payment.urgent` property like this:

```html
<div ng-if="$ctrl.canSelectUrgentPayment($ctrl.payment.from)" class="form-group">
  <div class="row-fluid">
    <div class="pull-left">{{ 'form.label.urgent' | i18n }}</div>
    <ui-bb-switcher-ng
      class="pull-left col-xs-offset-1"
      size="smaller"
      switcher="$ctrl.payment.urgent">
    </ui-bb-switcher-ng>
  </div>
  <div class="clearfix"></div>
</div>       
```

In above example `$ctrl.canSelectUrgentPayment($ctrl.payment.from)` is the function which is already resides in controller and checks if debit product supports urgent payments (arrives from backend inside product's object). Yet the `$ctrl.payment.urgent` is a reference to the property which should define if the user wants the the payment be "urgent". 

Other layout chunks then mentioned above are in the developer's discretion -- you can place it anywhere inside the payment form and decorate with other html (like help tooltips, etc.). 

