const resetFormState = form => {
  if (form) {
    form.$setUntouched();
    form.$setPristine();
  }
};

let selectedContact;

const helpers = (context) => {
  const filter = context.$filter;

  return {
    getSelectedContact: () => selectedContact,
    setSelectedContact: (contact) => {
      selectedContact = contact;
    },
    isFormPristine: (form) => !form || form.$pristine,
    cancelEditForm: (form) => resetFormState(form),
    saveContact: ($ctrl, form) => {
      const contact = $ctrl.state.contact.data;
      return $ctrl.saveContact(contact).then(() => resetFormState(form));
    },
    notificationMessage: (statusObject) => {
      let message = statusObject.text || '';
      if (statusObject.i18n) {
        message = filter('i18n')(statusObject.i18n);
      }

      return message;
    },
  };
};

export default helpers;
