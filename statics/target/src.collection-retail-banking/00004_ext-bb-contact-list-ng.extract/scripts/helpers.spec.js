/* global jasmine, describe, expect, fail, it */

import helpersGenerator from './helpers';

const setup = () => {
  const form = jasmine.createSpyObj('form', ['$setPristine', '$setUntouched']);
  const ctrl = jasmine.createSpyObj('$ctrl', ['saveContact']);
  ctrl.saveContact.and.returnValue(Promise.resolve({}));

  const translation = 'translation';
  const $filter = (filter) =>
    translation;

  const helpers = helpersGenerator({
    $filter,
  });

  return {
    form,
    ctrl,
    helpers,
  };
};

describe('isFormPristine', () => {
  it('should return true if form is not set yet', () => {
    const { helpers } = setup();
    const form = null;
    expect(helpers.isFormPristine(form)).toBeTrue();
  });

  it('should return true if form is pristine', () => {
    const { helpers } = setup();
    const form = { $pristine: true };
    expect(helpers.isFormPristine(form)).toBeTrue();
  });

  it('should return false if form is not pristine', () => {
    const { helpers } = setup();
    const form = { $pristine: false };
    expect(helpers.isFormPristine(form)).toBeFalse();
  })
});

describe('cancel edit form', () => {
  it('should reset form state', () => {
    const { ctrl, form, helpers } = setup();
    helpers.cancelEditForm(form);
    expect(form.$setUntouched).toHaveBeenCalled();
    expect(form.$setPristine).toHaveBeenCalled();
  });
});

describe('resets the form after saving', () => {
  it('when on the new contact page, creates a new contact', (done) => {
    const { ctrl, form, helpers } = setup();
    const contact = { id: 'arbContactId' };
    ctrl.state = {
      contact: {
        data: contact,
      },
    };
    helpers.saveContact(ctrl, form)
      .then(() => {
        expect(ctrl.saveContact).toHaveBeenCalledWith(contact);
        expect(form.$setUntouched).toHaveBeenCalled();
        expect(form.$setPristine).toHaveBeenCalled();
      })
      .then(done, fail);
  });
});
