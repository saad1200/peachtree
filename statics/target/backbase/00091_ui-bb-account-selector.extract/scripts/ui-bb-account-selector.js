(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("ui-bb-format-amount"), require("ui-bb-dropdown-select"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-account-selector", ["vendor-bb-angular", "ui-bb-format-amount", "ui-bb-dropdown-select"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-account-selector"] = factory(require("vendor-bb-angular"), require("ui-bb-format-amount"), require("ui-bb-dropdown-select"));
	else
		root["ui-bb-account-selector"] = factory(root["vendor-bb-angular"], root["ui-bb-format-amount"], root["ui-bb-dropdown-select"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_7__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(6);

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */,
/* 4 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _uiBbDropdownSelect = __webpack_require__(7);
	
	var _uiBbDropdownSelect2 = _interopRequireDefault(_uiBbDropdownSelect);
	
	var _uiBbFormatAmount = __webpack_require__(4);
	
	var _uiBbFormatAmount2 = _interopRequireDefault(_uiBbFormatAmount);
	
	var _selector = __webpack_require__(8);
	
	var _selector2 = _interopRequireDefault(_selector);
	
	__webpack_require__(9);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @name default
	 * @type {string}
	 * @description The angular module name
	 */
	exports.default = _vendorBbAngular2.default.module('ui-bb-account-selector', [_uiBbDropdownSelect2.default, _uiBbFormatAmount2.default]).component('uiBbAccountSelector', _selector2.default).name; /**
	                                                                                                                                                                                                     * @module ui-bb-account-selector
	                                                                                                                                                                                                     * @description
	                                                                                                                                                                                                     * UI component for selecting user account.
	                                                                                                                                                                                                     *
	                                                                                                                                                                                                     * @example
	                                                                                                                                                                                                     * // In an extension:
	                                                                                                                                                                                                     * // file: scripts/index.js
	                                                                                                                                                                                                     * import uiBbAccountSelector from 'ui-bb-account-selector';
	                                                                                                                                                                                                     *
	                                                                                                                                                                                                     * export const dependencyKeys = [
	                                                                                                                                                                                                     *   uiBbAccountSelector,
	                                                                                                                                                                                                     * ];
	                                                                                                                                                                                                     *
	                                                                                                                                                                                                     * // file: templates/template.ng.html
	                                                                                                                                                                                                     * <ui-bb-account-selector
	                                                                                                                                                                                                     *   ng-model="$ctrl.payment.from"
	                                                                                                                                                                                                     *   accounts="$ctrl.accountsList"
	                                                                                                                                                                                                     *   ng-change="$ctrl.onAccountChange()">
	                                                                                                                                                                                                     * </ui-bb-account-selector>
	                                                                                                                                                                                                     */

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_7__;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBbAccountSelector
	 * @type {object}
	 *
	 * @property {object} ng-model Account selector model
	 * @property {Array} accounts List of accounts
	 * @property {function} ng-change Callback function triggered when account is selected
	 * @property {boolean} select-all True if select all option is enabled
	 * @property {Labels} labels Object with labels
	 * @property {?string} select-all-template-id Enables select all option and contains template id
	 * @property {string} custom-template-id Id of the template that will be used for rendering
	 * instead of default ui-bb-account-selector/option-template.html template
	 */
	
	/**
	 * Labels type definition
	 * @typedef {Object} Labels
	 * @property {string}  allAccounts               - Label for all accounts
	 * @property {string}  accounts                  - Label for plural accounts
	 * @property {string}  account                   - Label for single accounts
	 */
	
	var selectAllTemplate = '\n  <a href="javascript:void(0)" tabindex="0" class="clearfix">\n    <div class="pull-left account-detail">\n      <div class="select-all-label" data-ng-bind="$option.label"></div>\n    </div>\n    <div class="pull-right text-right">\n      <div class="select-all-quantity" data-ng-bind="$option.quantity"></div>\n    </div>\n  </a>\n';
	
	var optionTemplate = '\n  <a href="javascript:void(0)" tabindex="0" class="clearfix">\n    <div class="bb-account-card-info">\n      <span class="bb-account-card-content-inline">\n        <div class="bb-account-card-name hover-color" data-ng-bind="$option.name"></div>\n        <div class="bb-account-card-number hover-color" data-ng-bind="$option.identifier"></div>\n      </span>\n      <span data-ng-hide="$option.hideAmounts"\n        class="bb-account-card-amount">\n        <span class="bb-account-card-additional-name text-muted sr-only hover-color">\n          Account amount\n        </span>\n        <ui-bb-format-amount\n          class="bb-account-card-additional-amount amount-regular-color hover-color"\n          data-amount="$option.amount"\n          data-currency="$option.currency"\n          data-wrap\n        ></ui-bb-format-amount>\n      </span>\n    </div>\n  </a>\n';
	
	var getSelectedTemplate = function getSelectedTemplate(optionTemplateName) {
	  var selectAllTemplateName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
	  return '\n  <span data-ng-if="$option.isSelectAll" data-ng-include="\'' + selectAllTemplateName + '\'"></span>\n  <span data-ng-if="!$option.isSelectAll" data-ng-include="\'' + optionTemplateName + '\'"></span>\n';
	};
	
	var uiBbAccountSelector = {
	  bindings: {
	    model: '=ngModel',
	    ngChange: '&',
	    accounts: '<',
	    selectAll: '<',
	    labels: '<',
	    customTemplateId: '@',
	    selectAllTemplateId: '@'
	  },
	  template: '\n    <ui-bb-dropdown-select\n      class="bb-account-selector"\n      data-ng-model="$ctrl.innerModel"\n      data-ng-change="$ctrl.onChange({ $item: $item.isSelectAll ? null : $item })"\n      data-template-url="{{ $ctrl.selectedTemplateName }}">\n      <ui-bb-dropdown-option\n        data-ng-if="$ctrl.selectAll"\n        data-option="$ctrl.allAccountsSelector"\n        data-template-url="{{ $ctrl.selectAllTemplateName }}">\n      </ui-bb-dropdown-option>\n      <ui-bb-dropdown-option\n        data-option="account"\n        data-template-url="{{ $ctrl.optionTemplateName }}"\n        data-ng-repeat="account in $ctrl.accounts">\n      </ui-bb-dropdown-option>\n    </ui-bb-dropdown-select>\n  ',
	  controller: ['$templateCache', '$element', '$attrs', '$scope', '$timeout', function controller($templateCache, $element, $attrs, $scope, $timeout) {
	    var self = this;
	    var defaultOptionTemplateName = 'ui-bb-account-selector/option-template.html';
	    var defaultSelectAllTemplateName = 'ui-bb-account-selector/select-all-template.html';
	    var selectedTemplateName = 'ui-bb-account-selector/selected-template.html';
	
	    var onChange = function onChange(item) {
	      $scope.$apply(function () {
	        self.model = self.innerModel.isSelectAll ? null : self.innerModel;
	      });
	      self.ngChange(item);
	    };
	
	    var $postLink = function $postLink() {
	      if (!self.attrs.placeholder) {
	        // eslint-disable-next-line max-len, no-console
	        console.warn('DEPRECATED - Hardcoded placeholder text (when account is not selected) will be removed in the next component major release. Use \'placeholder\' attribute instead.');
	      }
	
	      // TODO: Replace this deprecated section when component version will be bumped to v2.0.0
	      var placeholder = self.attrs.placeholder || 'Select account';
	      self.element.querySelector('span.placeholder').innerHTML = placeholder;
	    };
	
	    var $onInit = function $onInit() {
	      $templateCache.put(defaultOptionTemplateName, optionTemplate);
	      self.optionTemplateName = self.customTemplateId || defaultOptionTemplateName;
	      self.attrs = $attrs;
	      self.element = $element[0];
	
	      $timeout(function () {
	        self.innerModel = self.model;
	      });
	
	      if (self.selectAll) {
	        $templateCache.put(defaultSelectAllTemplateName, selectAllTemplate);
	        self.selectAllTemplateName = self.selectAllTemplateId || defaultSelectAllTemplateName;
	
	        var allAccountsSuffix = self.labels[self.accounts.length > 1 ? 'accounts' : 'account'];
	
	        self.allAccountsSelector = {
	          isSelectAll: true,
	          label: self.labels.allAccounts,
	          quantity: self.accounts.length + ' ' + allAccountsSuffix
	        };
	
	        self.innerModel = self.model || self.allAccountsSelector;
	
	        var selectedTemplate = getSelectedTemplate(self.optionTemplateName, self.selectAllTemplateName);
	        $templateCache.put(self.selectedTemplateName, selectedTemplate);
	      } else {
	        self.selectedTemplateName = self.optionTemplateName;
	      }
	    };
	
	    Object.assign(self, {
	      selectedTemplateName: selectedTemplateName,
	      onChange: onChange,
	      $onInit: $onInit,
	      $postLink: $postLink
	    });
	  }]
	};
	
	exports.default = uiBbAccountSelector;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(10);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(12)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./index.scss", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!../../node_modules/sass-loader/index.js!./index.scss");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(11)();
	// imports
	
	
	// module
	exports.push([module.id, "ui-bb-account-selector {\n  display: block; }\n", ""]);
	
	// exports


/***/ }),
/* 11 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ })
/******/ ])
});
;
//# sourceMappingURL=ui-bb-account-selector.js.map