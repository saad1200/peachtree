# ui-bb-account-selector


Version: **1.1.70**

UI component for selecting user account.

## Imports

* ui-bb-dropdown-select
* ui-bb-format-amount
* vendor-bb-angular

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbAccountSelector from 'ui-bb-account-selector';

export const dependencyKeys = [
  uiBbAccountSelector,
];

// file: templates/template.ng.html
<ui-bb-account-selector
  ng-model="$ctrl.payment.from"
  accounts="$ctrl.accountsList"
  ng-change="$ctrl.onAccountChange()">
</ui-bb-account-selector>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **Type Definitions**<br/>    <a href="#Labels">Labels</a><br/>

## Exports

### <a name="default"></a>*default*

The angular module name

**Type:** *String*


---

## uiBbAccountSelector


| Property | Type | Description |
| :-- | :-- | :-- |
| ng-model | Object | Account selector model |
| accounts | Array | List of accounts |
| ng-change | Function | Callback function triggered when account is selected |
| select-all | Boolean | True if select all option is enabled |
| labels | <a href="#Labels">Labels</a> | Object with labels |
| select-all-template-id | String (optional) | Enables select all option and contains template id |
| custom-template-id | String | Id of the template that will be used for rendering instead of default ui-bb-account-selector/option-template.html template |

## Type Definitions


### <a name="Labels"></a>*Labels*

Labels type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| allAccounts | String | Label for all accounts |
| accounts | String | Label for plural accounts |
| account | String | Label for single accounts |

---
