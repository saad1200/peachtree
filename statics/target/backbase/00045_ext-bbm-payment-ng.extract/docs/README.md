# ext-bbm-payment-ng


Version: **1.0.236**

Mobile extension for a payment form in the Payment widget.

## Imports

* ui-bb-format-amount
* ui-bb-i18n-ng
* ui-bb-iban-ng
* ui-bb-text-ng
* ui-bbm-beneficiary-select-ng
* ui-bbm-currency-input-ng
* vendor-bb-angular-ng-messages
* vendor-bb-angular-sanitize

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-payment-ng</value>
</property>
```

## Table of Contents
- **Helpers**<br/>    <a href="#Helpers#onPaymentContinue">#onPaymentContinue($ctrl, paymentForm)</a><br/>    <a href="#Helpers#invalidate">#invalidate(payment, paymentForm)</a><br/>    <a href="#Helpers#onPaymentFromAccountsClick">#onPaymentFromAccountsClick($ctrl)</a><br/>    <a href="#Helpers#onPaymentToAccountsClick">#onPaymentToAccountsClick($ctrl)</a><br/>    <a href="#Helpers#canSaveNewContact">#canSaveNewContact($ctrl)</a><br/>
- **Hooks**<br/>    <a href="#Hooks#processAccountsTo">#processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts)</a><br/>

---

## Helpers

Helpers for ext-bbm-payment-ng

### <a name="Helpers#onPaymentContinue"></a>*#onPaymentContinue($ctrl, paymentForm)*

Helper to reset the payment model, and update accounts and currency lists

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | Instance of the angular widget controller |
| paymentForm | Object | Instance of the angular form |

##### Returns

<a href="#void">void</a> - **

### <a name="Helpers#invalidate"></a>*#invalidate(payment, paymentForm)*

Helper to invalidate the payment form

| Parameter | Type | Description |
| :-- | :-- | :-- |
| payment | <a href="#Payment">Payment</a> | Payment object |
| paymentForm | Object | Instance of the angular form |

##### Returns

Boolean - *True if the form is valid, false otherwise*

### <a name="Helpers#onPaymentFromAccountsClick"></a>*#onPaymentFromAccountsClick($ctrl)*

Helper to process reaction on debit accounts type selection

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | Widget controller |

##### Returns

<a href="#Promise<any>">Promise<any></a> - *Promise which is resolved once the click is processed,
  or rejected in case of errors*

### <a name="Helpers#onPaymentToAccountsClick"></a>*#onPaymentToAccountsClick($ctrl)*

Helper to process reaction on credit accounts type selection

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | Widget controller |

##### Returns

<a href="#Promise<any>">Promise<any></a> - *Promise, which is resolved once the click is processed,
  or rejected in case of error*

### <a name="Helpers#canSaveNewContact"></a>*#canSaveNewContact($ctrl)*

Checks if layout should show 'save beneficiary' switcher

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | PaymentController |

##### Returns

Boolean - **

---

## Hooks

Hooks for widget-bb-payment-ng

### <a name="Hooks#processAccountsTo"></a>*#processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts)*

Hook for processing account list in 'to' field (credit).
Assigned to [$ctrl.accountsTo]<a href="#PaymentController#AccountView">PaymentController#AccountView</a>

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccount | <a href="#Account">Account</a> | Selected debit account (can be null) |
| getCreditAccounts | Function | Function to retrieve all credit accounts |
| getExternalAccounts | Function | Function to retrieve all external contacts formatted like Product kind |

##### Returns

<a href="#Promise<array>">Promise<array></a> - *Promise that retrieves array of accounts.*
