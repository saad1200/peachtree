(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular-ng-messages"), require("ui-bb-i18n-ng"), require("ui-bb-format-amount"), require("ui-bb-iban-ng"), require("ui-bb-text-ng"), require("ui-bbm-beneficiary-select-ng"), require("ui-bbm-currency-input-ng"), require("lib-bbm-plugins"), require("vendor-bb-angular-sanitize"));
	else if(typeof define === 'function' && define.amd)
		define("ext-bbm-payment-ng", ["vendor-bb-angular-ng-messages", "ui-bb-i18n-ng", "ui-bb-format-amount", "ui-bb-iban-ng", "ui-bb-text-ng", "ui-bbm-beneficiary-select-ng", "ui-bbm-currency-input-ng", "lib-bbm-plugins", "vendor-bb-angular-sanitize"], factory);
	else if(typeof exports === 'object')
		exports["ext-bbm-payment-ng"] = factory(require("vendor-bb-angular-ng-messages"), require("ui-bb-i18n-ng"), require("ui-bb-format-amount"), require("ui-bb-iban-ng"), require("ui-bb-text-ng"), require("ui-bbm-beneficiary-select-ng"), require("ui-bbm-currency-input-ng"), require("lib-bbm-plugins"), require("vendor-bb-angular-sanitize"));
	else
		root["ext-bbm-payment-ng"] = factory(root["vendor-bb-angular-ng-messages"], root["ui-bb-i18n-ng"], root["ui-bb-format-amount"], root["ui-bb-iban-ng"], root["ui-bb-text-ng"], root["ui-bbm-beneficiary-select-ng"], root["ui-bbm-currency-input-ng"], root["lib-bbm-plugins"], root["vendor-bb-angular-sanitize"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_28__, __WEBPACK_EXTERNAL_MODULE_29__, __WEBPACK_EXTERNAL_MODULE_30__, __WEBPACK_EXTERNAL_MODULE_31__, __WEBPACK_EXTERNAL_MODULE_32__, __WEBPACK_EXTERNAL_MODULE_37__, __WEBPACK_EXTERNAL_MODULE_42__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(41);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ }),
/* 27 */,
/* 28 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_28__;

/***/ }),
/* 29 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_29__;

/***/ }),
/* 30 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_30__;

/***/ }),
/* 31 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_31__;

/***/ }),
/* 32 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_32__;

/***/ }),
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_37__;

/***/ }),
/* 38 */,
/* 39 */,
/* 40 */,
/* 41 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.dependencyKeys = exports.helpers = exports.events = exports.hooks = undefined;
	
	var _uiBbmCurrencyInputNg = __webpack_require__(32);
	
	var _uiBbmCurrencyInputNg2 = _interopRequireDefault(_uiBbmCurrencyInputNg);
	
	var _uiBbmBeneficiarySelectNg = __webpack_require__(31);
	
	var _uiBbmBeneficiarySelectNg2 = _interopRequireDefault(_uiBbmBeneficiarySelectNg);
	
	var _uiBbIbanNg = __webpack_require__(29);
	
	var _uiBbIbanNg2 = _interopRequireDefault(_uiBbIbanNg);
	
	var _uiBbI18nNg = __webpack_require__(15);
	
	var _uiBbI18nNg2 = _interopRequireDefault(_uiBbI18nNg);
	
	var _uiBbFormatAmount = __webpack_require__(28);
	
	var _uiBbFormatAmount2 = _interopRequireDefault(_uiBbFormatAmount);
	
	var _uiBbTextNg = __webpack_require__(30);
	
	var _uiBbTextNg2 = _interopRequireDefault(_uiBbTextNg);
	
	var _vendorBbAngularNgMessages = __webpack_require__(8);
	
	var _vendorBbAngularNgMessages2 = _interopRequireDefault(_vendorBbAngularNgMessages);
	
	var _vendorBbAngularSanitize = __webpack_require__(42);
	
	var _vendorBbAngularSanitize2 = _interopRequireDefault(_vendorBbAngularSanitize);
	
	var _hooks = __webpack_require__(43);
	
	var extHooks = _interopRequireWildcard(_hooks);
	
	var _events = __webpack_require__(44);
	
	var _events2 = _interopRequireDefault(_events);
	
	var _helpers = __webpack_require__(45);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	__webpack_require__(46);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module ext-bbm-payment-ng
	 *
	 * @description
	 * Mobile extension for a payment form in the Payment widget.
	 *
	 * @example
	 * <!-- Contact widget model.xml -->
	 * <property name="extension" viewHint="text-input,admin">
	 *   <value type="string">ext-bbm-payment-ng</value>
	 * </property>
	 */
	var hooks = exports.hooks = extHooks;
	var events = exports.events = _events2.default;
	var helpers = exports.helpers = _helpers2.default;
	
	var dependencyKeys = exports.dependencyKeys = [_uiBbmCurrencyInputNg2.default, _uiBbmBeneficiarySelectNg2.default, _uiBbIbanNg2.default, _uiBbI18nNg2.default, _uiBbFormatAmount2.default, _uiBbTextNg2.default, _vendorBbAngularNgMessages2.default, _vendorBbAngularSanitize2.default];

/***/ }),
/* 42 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_42__;

/***/ }),
/* 43 */
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.processAccountsTo = processAccountsTo;
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	/* eslint-disable import/prefer-default-export */
	/* eslint no-unused-vars: ["error", { "args": "none" }] */
	
	/**
	 * @name Hooks
	 * @type {object}
	 *
	 * @description
	 * Hooks for widget-bb-payment-ng
	 */
	
	/**
	 * @name isExternalTransferAllowed
	 * @type {function}
	 *
	 * @param {Account} debitAccount
	 * @returns {boolean}
	 * @inner
	 */
	var isExternalTransferAllowed = function isExternalTransferAllowed(debitAccount) {
	  return !debitAccount.id || debitAccount.externalTransferAllowed;
	};
	
	/**
	 * @name wait
	 * @type {function}
	 * @param {number} delay
	 * @returns {promise}
	 * @inner
	 */
	var wait = function wait(delay) {
	  return new Promise(function (resolve) {
	    return setTimeout(resolve, delay);
	  });
	};
	
	/**
	 * @name Hooks#processAccountsTo
	 * @type {function}
	 *
	 * @description
	 * Hook for processing account list in 'to' field (credit).
	 * Assigned to [$ctrl.accountsTo]{@link PaymentController#AccountView}
	 *
	 * @param {Account} debitAccount Selected debit account (can be null)
	 * @param {function} getCreditAccounts Function to retrieve all credit accounts
	 * @param {function} getExternalAccounts Function to retrieve all external contacts
	 * formatted like Product kind
	 * @returns {Promise<array>} Promise that retrieves array of accounts.
	 */
	function processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts) {
	  return wait(300).then(function () {
	    return getCreditAccounts(debitAccount.id || null);
	  }).then(function (accounts) {
	    if (isExternalTransferAllowed(debitAccount)) {
	      return getExternalAccounts().then(function (contacts) {
	        return [].concat(_toConsumableArray(accounts), _toConsumableArray(contacts));
	      });
	    }
	    return accounts;
	  });
	}

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _libBbmPlugins = __webpack_require__(37);
	
	var _libBbmPlugins2 = _interopRequireDefault(_libBbmPlugins);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } // Temporary disable capsInNew until it fixed in lib-bbm-plugins
	/* eslint new-cap: ["error", { "capIsNew": false }] */
	
	
	var Event = {
	  ACCOUNT_FROM_LOAD: 'bb.event.account.from.load',
	  ACCOUNT_FROM_DONE: 'bb.event.account.from.done',
	  ACCOUNT_FROM_FAILED: 'bb.event.account.from.failed',
	  ACCOUNT_TO_LOAD: 'bb.event.account.to.load',
	  ACCOUNT_TO_DONE: 'bb.event.account.to.done',
	  ACCOUNT_TO_FAILED: 'bb.event.account.to.failed',
	  PAYMENT_START: 'bb.event.payment.start',
	  PAYMENT_DONE: 'bb.event.payment.done',
	  PAYMENT_FAILED: 'bb.event.payment.failed'
	};
	
	exports.default = function (_ref) {
	  var _ref2;
	
	  var $filter = _ref.$filter;
	
	  var i18n = $filter('i18n');
	  return _ref2 = {}, _defineProperty(_ref2, Event.ACCOUNT_FROM_LOAD, function () {
	    _libBbmPlugins2.default.ActivityIndicator().show(i18n('message.payment.account.from.load'));
	  }), _defineProperty(_ref2, Event.ACCOUNT_FROM_DONE, function () {
	    _libBbmPlugins2.default.ActivityIndicator().hide();
	  }), _defineProperty(_ref2, Event.ACCOUNT_FROM_FAILED, function () {
	    _libBbmPlugins2.default.ActivityIndicator().hide();
	    _libBbmPlugins2.default.Snackbar().error(i18n('message.payment.account.from.failed'));
	  }), _defineProperty(_ref2, Event.ACCOUNT_TO_LOAD, function () {
	    _libBbmPlugins2.default.ActivityIndicator().show(i18n('message.payment.account.to.load'));
	  }), _defineProperty(_ref2, Event.ACCOUNT_TO_DONE, function () {
	    _libBbmPlugins2.default.ActivityIndicator().hide();
	  }), _defineProperty(_ref2, Event.ACCOUNT_TO_FAILED, function () {
	    _libBbmPlugins2.default.ActivityIndicator().hide();
	    _libBbmPlugins2.default.Snackbar().error(i18n('message.payment.account.to.failed'));
	  }), _defineProperty(_ref2, Event.PAYMENT_START, function () {
	    _libBbmPlugins2.default.ActivityIndicator().show(i18n('message.payment.start'));
	  }), _defineProperty(_ref2, Event.PAYMENT_DONE, function () {
	    _libBbmPlugins2.default.ActivityIndicator().hide();
	    _libBbmPlugins2.default.Snackbar().success(i18n('message.payment.done'));
	  }), _defineProperty(_ref2, Event.PAYMENT_FAILED, function () {
	    _libBbmPlugins2.default.ActivityIndicator().hide();
	    _libBbmPlugins2.default.Snackbar().error(i18n('message.payment.failed'));
	  }), _ref2;
	};

/***/ }),
/* 45 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name Helpers
	 * @type {object}
	 *
	 * @description
	 * Helpers for ext-bbm-payment-ng
	 */
	
	/**
	 * Widget events enum
	 * @type {object}
	 */
	var Event = {
	  PAYMENT_REVIEW_STEP: 'bb.event.payment.review.step'
	};
	
	/**
	 * @name isBeneficiaryComplete
	 * @type {function}
	 *
	 * @description
	 * Checks if all the required fields for a beneficiary have been filled
	 *
	 * @param {object} beneficiary - Beneficiary data object
	 * @returns {boolean}
	 * @inner
	 */
	var isBeneficiaryComplete = function isBeneficiaryComplete(beneficiary) {
	  return Boolean(beneficiary && beneficiary.name && beneficiary.identifier);
	};
	
	/**
	 * @name isInternalAccount
	 * @type {function}
	 *
	 * @description
	 * Checks if the given account is an internal account
	 *
	 * @param {object} beneficiary - Beneficiary data object
	 * @returns {boolean}
	 * @inner
	 */
	var isInternalAccount = function isInternalAccount(account) {
	  return Boolean(account.name && account.identifier);
	};
	
	/**
	 * @name isSameAccount
	 * @type {function}
	 *
	 * @description
	 * Checks if given account A matches given account B
	 *
	 * @param {object} accountA - Data object accountA
	 * @param {object} accountB - Data object accountB
	 * @returns {boolean}
	 * @inner
	 */
	var isSameAccount = function isSameAccount(accountA, accountB) {
	  return Boolean(accountA.name === accountB.name && accountA.identifier === accountB.identifier);
	};
	
	/**
	 * @name isExistingAccount
	 * @type {function}
	 *
	 * @description
	 * Checks if the given accounts list contains the given account
	 *
	 * @param {object} accounts - Data object with accounts
	 * @param {object} account - Data object account
	 * @returns {boolean}
	 * @inner
	 */
	var isExistingAccount = function isExistingAccount(accounts, account) {
	  return accounts.some(function (item) {
	    if (isInternalAccount(item)) {
	      return isSameAccount(item, account);
	    }
	
	    return (item.contacts || []).some(function (contact) {
	      return isSameAccount(contact, account);
	    });
	  });
	};
	
	exports.default = function (_ref) {
	  var publish = _ref.publish;
	  return {
	    /**
	     * @name Helpers#onPaymentContinue
	     * @type {function}
	     *
	     * @description
	     * Helper to reset the payment model, and update accounts and currency lists
	     *
	     * @param {object} $ctrl Instance of the angular widget controller
	     * @param {object} paymentForm Instance of the angular form
	     * @returns {void}
	     */
	    onPaymentContinue: function onPaymentContinue($ctrl, paymentForm) {
	      var reviewStep = $ctrl.paymentPreferences.reviewStep;
	
	
	      paymentForm.$setUntouched();
	      if (reviewStep) {
	        $ctrl.storePayment($ctrl.payment);
	        $ctrl.storeSaveContactFlag();
	        publish(Event.PAYMENT_REVIEW_STEP, $ctrl.payment);
	      } else {
	        $ctrl.makePayment($ctrl.payment);
	      }
	    },
	
	    /**
	     * @name Helpers#invalidate
	     * @type {function}
	     *
	     * @description
	     * Helper to invalidate the payment form
	     *
	     * @param {Payment} payment Payment object
	     * @param {object} paymentForm Instance of the angular form
	     * @returns {boolean} True if the form is valid, false otherwise
	     */
	    invalidate: function invalidate(payment, paymentForm) {
	      if (!payment.from || paymentForm.$invalid) {
	        return true;
	      }
	
	      return false;
	    },
	
	    /**
	     * @name Helpers#onPaymentFromAccountsClick
	     * @type {function}
	     *
	     * @description
	     * Helper to process reaction on debit accounts type selection
	     *
	     * @param {object} $ctrl Widget controller
	     * @returns {Promise<any>} Promise which is resolved once the click is processed,
	     *   or rejected in case of errors
	     */
	    onPaymentFromAccountsClick: function onPaymentFromAccountsClick($ctrl) {
	      return $ctrl.processSelectProductType(true);
	    },
	
	    /**
	     * @name Helpers#onPaymentToAccountsClick
	     * @type {function}
	     *
	     * @description
	     * Helper to process reaction on credit accounts type selection
	     *
	     * @param {object} $ctrl Widget controller
	     * @returns {Promise<any>} Promise, which is resolved once the click is processed,
	     *   or rejected in case of error
	     */
	    onPaymentToAccountsClick: function onPaymentToAccountsClick($ctrl) {
	      return $ctrl.processSelectProductType(false);
	    },
	
	    /**
	     * @name Helpers#canSaveNewContact
	     * @type {function}
	     *
	     * @description
	     * Checks if layout should show 'save beneficiary' switcher
	     *
	     * @param {object} $ctrl PaymentController
	     * @returns {boolean}
	     */
	    canSaveNewContact: function canSaveNewContact($ctrl) {
	      var accounts = $ctrl.accountsTo || []; // to cover null arg issue
	      var beneficiary = $ctrl.payment.to;
	
	      var canBeSaved = isBeneficiaryComplete(beneficiary) && !isExistingAccount(accounts, beneficiary);
	
	      // reset switcher before any further appearance
	      if (!canBeSaved) {
	        $ctrl.setSaveContactFlag(false);
	      }
	
	      return canBeSaved;
	    }
	  };
	};

/***/ }),
/* 46 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(47);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!./index.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!./index.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 47 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(25)();
	// imports
	
	
	// module
	exports.push([module.id, ".ext-bbm-payment-ng {\n  position: relative;\n}\n\n.ext-bbm-payment-ng ui-bbm-beneficiary-select-ng {\n  min-height: 115px;\n}\n\n.ext-bbm-payment-ng .table-view-cell {\n  position: static;\n}\n\n.ext-bbm-payment-ng .ui-bbm-beneficiary-select--active {\n  min-height: 310px;\n}\n", ""]);
	
	// exports


/***/ })
/******/ ])
});
;
//# sourceMappingURL=ext-bbm-payment-ng.js.map