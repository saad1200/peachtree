# model-bb-spending-ng


Version: **0.1.33**

Model for widget-bb-spending-ng

## Imports

* data-bb-product-summary-http-ng
* data-bb-transactions-http-ng
* lib-bb-model-errors
* lib-bb-storage-ng
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
import modelSpendingModuleKey, { modelSpendingKey } from 'model-bb-spending-ng';

angular
  .module('ExampleModule', [
    modelSpendingModuleKey,
  ])
  .factory('someFactory', [
    modelSpendingKey,
    // into
    function someFactory(spendingModel) {
      // ...
    },
  ]);
```

## Table of Contents
- **model-bb-spending-ng**<br/>    <a href="#model-bb-spending-ngE_PARAMS">E_PARAMS</a><br/>
- **spendingModel**<br/>    <a href="#spendingModel#validateSpendingParameters">#validateSpendingParameters()</a><br/>    <a href="#spendingModel#loadSpending">#loadSpending(params)</a><br/>    <a href="#spendingModel#getProducts">#getProducts()</a><br/>    <a href="#spendingModel#getProductsArray">#getProductsArray(keepEmptyProducts)</a><br/>    <a href="#spendingModel#getSelectedProduct">#getSelectedProduct()</a><br/>    <a href="#spendingModel#setSelectedProduct">#setSelectedProduct(selectedProduct)</a><br/>    <a href="#spendingModel#transformToSeries">#transformToSeries(spendings)</a><br/>
- **Type Definitions**<br/>    <a href="#Spending">Spending</a><br/>    <a href="#SpendingItem">SpendingItem</a><br/>    <a href="#Amount">Amount</a><br/>    <a href="#ProductKinds">ProductKinds</a><br/>    <a href="#ProductKind">ProductKind</a><br/>    <a href="#Product">Product</a><br/>

---

## bbStorageKeys

bbStorage keys enum

---

## preferencesKeys

Preferences keys enum

---
### <a name="model-bb-spending-ngE_PARAMS"></a>*E_PARAMS*

Additional model error in case required parameters are missing

**Type:** *String*


---

## spendingModel


### <a name="spendingModel#validateSpendingParameters"></a>*#validateSpendingParameters()*

Checks if all required parameters are set

##### Returns

Promise of Object - *A Promise resolving to object with parameters.*

### <a name="spendingModel#loadSpending"></a>*#loadSpending(params)*

Load product spending.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Request parameters |

##### Returns

Promise of <a href="#Spending">Spending</a>, <a href="#ModelError">ModelError</a> - *A Promise with spending or error data*

### <a name="spendingModel#getProducts"></a>*#getProducts()*

Get products.

##### Returns

Promise of <a href="#ProductKinds">ProductKinds</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with ProductsKinds and TotalBalance.*

### <a name="spendingModel#getProductsArray"></a>*#getProductsArray(keepEmptyProducts)*

Get products.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| keepEmptyProducts | Boolean | defines if empty product kinds should be passed. |

##### Returns

Promise of <a href="#ProductKinds">ProductKinds</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to array with Products.*

### <a name="spendingModel#getSelectedProduct"></a>*#getSelectedProduct()*

Get current selected product.

##### Returns

Promise of <a href="#ProductKinds">ProductKinds</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to a selected product object.*

### <a name="spendingModel#setSelectedProduct"></a>*#setSelectedProduct(selectedProduct)*

Set selected product to the sorage

| Parameter | Type | Description |
| :-- | :-- | :-- |
| selectedProduct | <a href="#Product">Product</a> | The selected product value |

### <a name="spendingModel#transformToSeries"></a>*#transformToSeries(spendings)*

Transforms data into format suitable for chart UI components

| Parameter | Type | Description |
| :-- | :-- | :-- |
| spendings | <a href="#Spending">Spending</a> | Spending data |

##### Returns

<a href="#BBSeries">BBSeries</a> - *Data in format suitable for chart UI components*

## Type Definitions


### <a name="Spending"></a>*Spending*

Spending response object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| totalSpending | Object | Total spending object |
| totalSpending.amount | Number | Total spending value |
| totalSpending.currencyCode | String | Total spending currency code (ISO) |
| items | Array of <a href="#SpendingItem">SpendingItem</a> | Array of spending items |

### <a name="SpendingItem"></a>*SpendingItem*

Spending response item

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| category | String | Transactions category |
| totalAmount | Object | The total amount of the aggregated transactions by category |
| totalAmount.currencyCode | String | Total amount currency code (ISO) |
| totalAmount.amount | Number | Total amount value |
| trend | Number | Percentage value of the trend |
| portion | Number | Percentage of the total spending for a given period |

### <a name="Amount"></a>*Amount*

Amount object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String | Currency code |
| value | Number |  |

### <a name="ProductKinds"></a>*ProductKinds*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of products |
| productKinds | Array of <a href="#ProductKind">ProductKind</a> | Array of Products Kinds |

### <a name="ProductKind"></a>*ProductKind*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of product kind |
| name | <a href="#!string">!string</a> | Name of the product kind |
| products | Array of <a href="#Product">Product</a> | Array of associated products |

### <a name="Product"></a>*Product*

Product type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the Product |
| name | <a href="#!string">!string</a> | Name of the Product |
| kind | <a href="#!string">!string</a> | id of the ProductKind |
| alias | String | Alias of the Product |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | Currency code |
| PANSuffix | String | Primary Account Number Suffix |
| bookedBalance | String | Booked balance |
| availableBalance | String | Available balance |
| creditLimit | String | Credit limit |
| currentInvestmentValue | String | Current investment value |
| principalAmount | String | Principal amount |
| accruedInterest | String | Accrued interest |

---
