(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("data-bb-transactions-http-ng"), require("data-bb-product-summary-http-ng"), require("lib-bb-storage-ng"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"));
	else if(typeof define === 'function' && define.amd)
		define("model-bb-spending-ng", ["vendor-bb-angular", "data-bb-transactions-http-ng", "data-bb-product-summary-http-ng", "lib-bb-storage-ng", "lib-bb-widget-ng", "lib-bb-model-errors"], factory);
	else if(typeof exports === 'object')
		exports["model-bb-spending-ng"] = factory(require("vendor-bb-angular"), require("data-bb-transactions-http-ng"), require("data-bb-product-summary-http-ng"), require("lib-bb-storage-ng"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"));
	else
		root["model-bb-spending-ng"] = factory(root["vendor-bb-angular"], root["data-bb-transactions-http-ng"], root["data-bb-product-summary-http-ng"], root["lib-bb-storage-ng"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_14__, __WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_16__, __WEBPACK_EXTERNAL_MODULE_17__, __WEBPACK_EXTERNAL_MODULE_18__, __WEBPACK_EXTERNAL_MODULE_20__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(13);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.modelSpendingKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(14);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbTransactionsHttpNg = __webpack_require__(15);
	
	var _dataBbTransactionsHttpNg2 = _interopRequireDefault(_dataBbTransactionsHttpNg);
	
	var _dataBbProductSummaryHttpNg = __webpack_require__(16);
	
	var _dataBbProductSummaryHttpNg2 = _interopRequireDefault(_dataBbProductSummaryHttpNg);
	
	var _libBbStorageNg = __webpack_require__(17);
	
	var _libBbStorageNg2 = _interopRequireDefault(_libBbStorageNg);
	
	var _libBbWidgetNg = __webpack_require__(18);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _spending = __webpack_require__(19);
	
	var _spending2 = _interopRequireDefault(_spending);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module model-bb-spending-ng
	 *
	 * @description
	 * Model for widget-bb-spending-ng
	 *
	 * @example
	 * import modelSpendingModuleKey, { modelSpendingKey } from 'model-bb-spending-ng';
	 *
	 * angular
	 *   .module('ExampleModule', [
	 *     modelSpendingModuleKey,
	 *   ])
	 *   .factory('someFactory', [
	 *     modelSpendingKey,
	 *     // into
	 *     function someFactory(spendingModel) {
	 *       // ...
	 *     },
	 *   ]);
	 */
	var moduleKey = 'model-bb-spending-ng';
	var modelSpendingKey = exports.modelSpendingKey = moduleKey + ':model';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_dataBbTransactionsHttpNg2.default, _dataBbProductSummaryHttpNg2.default, _libBbStorageNg2.default, _libBbWidgetNg2.default]).factory(modelSpendingKey, ['$q', _dataBbTransactionsHttpNg.transactionsDataKey, _dataBbProductSummaryHttpNg.productSummaryDataKey, _libBbStorageNg.bbStorageServiceKey, _libBbWidgetNg.widgetKey,
	/* into */
	_spending2.default]).name;
	
	/**
	 * Spending response object
	 * @typedef {object} Spending
	 * @property {object} totalSpending Total spending object
	 * @property {number} totalSpending.amount Total spending value
	 * @property {string} totalSpending.currencyCode Total spending currency code (ISO)
	 * @property {SpendingItem[]} items Array of spending items
	 */
	
	/**
	 * Spending response item
	 * @typedef {object} SpendingItem
	 * @property {string} category Transactions category
	 * @property {object} totalAmount The total amount of the aggregated transactions by category
	 * @property {string} totalAmount.currencyCode Total amount currency code (ISO)
	 * @property {number} totalAmount.amount Total amount value
	 * @property {number} trend Percentage value of the trend
	 * @property {number} portion Percentage of the total spending for a given period
	 */
	
	/**
	 * Amount object
	 * @typedef {object} Amount
	 * @property {string} currency Currency code
	 * @property {number} value
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKinds
	 * @property {Amount} aggregatedBalance Total balance of products
	 * @property {ProductKind[]} productKinds Array of Products Kinds
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKind
	 * @property {Amount} aggregatedBalance Total balance of product kind
	 * @property {!string} name Name of the product kind
	 * @property {Product[]} products Array of associated products
	 */
	
	/**
	 * Product type definition
	 * @typedef {object} Product
	 * @property {!string} id id of the Product
	 * @property {!string} name Name of the Product
	 * @property {!string} kind id of the ProductKind
	 * @property {string} alias Alias of the Product
	 * @property {string} IBAN International Bank Account Number
	 * @property {string} BBAN Basic Bank Account Number
	 * @property {string} currency Currency code
	 * @property {string} PANSuffix Primary Account Number Suffix
	 * @property {string} bookedBalance Booked balance
	 * @property {string} availableBalance Available balance
	 * @property {string} creditLimit Credit limit
	 * @property {string} currentInvestmentValue Current investment value
	 * @property {string} principalAmount Principal amount
	 * @property {string} accruedInterest Accrued interest
	 */

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_14__;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_16__;

/***/ }),
/* 17 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_17__;

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_18__;

/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = spendingModel;
	
	var _libBbModelErrors = __webpack_require__(20);
	
	var _constants = __webpack_require__(21);
	
	/**
	 * Model factory for model-bb-spending-ng
	 *
	 * @inner
	 * @type {function}
	 * @param {Object} Promise An ES2015 compatible `Promise` object.
	 *
	 * @return {Object}
	 */
	function spendingModel(Promise, spendingData, productSummaryData, bbStorage, widgetInstance) {
	  /**
	   * @name spendingModel#FROM_STORAGE
	   * @inner
	   * @type {boolean}
	   *
	   * @description
	   * Defines if products are recieved from bb-storage or from API always
	   */
	  var FROM_STORAGE = widgetInstance.getBooleanPreference(_constants.preferencesKeys.IS_PRODUCTS_LIST_FROM_STORAGE);
	
	  /**
	   * @name spendingModel#IS_FIRST_PRODUCT_DEFAULT
	   * @inner
	   * @type {boolean}
	   *
	   * @description
	   * Defines if the first product is selected by default
	   */
	  var GET_FIRST_AS_DEFAULT = widgetInstance.getBooleanPreference(_constants.preferencesKeys.IS_FIRST_PRODUCT_DEFAULT);
	
	  /**
	   * @name spendingModel#productKindHasProducts
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Checks if product kind has any products
	   *
	   * @param {Object} productKind a product kind object
	   * @return {boolean} 'true' if has any products
	   */
	  var productKindHasProducts = function productKindHasProducts(productKind) {
	    return productKind.products && productKind.products.length;
	  };
	
	  /**
	   * @name spendingModel#validateSpendingParameters
	   * @type {function}
	   *
	   * @description
	   * Checks if all required parameters are set
	   *
	   * @returns {Promise.<object>}
	   * A Promise resolving to object with parameters.
	   */
	  var validateSpendingParameters = function validateSpendingParameters(params) {
	    if (params.arrangementId && params.periodStartDate && params.periodEndDate) {
	      return Promise.resolve(params);
	    }
	
	    return Promise.reject({
	      code: _constants.E_PARAMS
	    });
	  };
	
	  /**
	   * @name spendingModel#loadSpending
	   * @type {function}
	   *
	   * @description
	   * Load product spending.
	   *
	   * @param {object} params Request parameters
	   * @returns {Promise.<Spending, ModelError>} A Promise with spending or error data
	   */
	  var loadSpending = function loadSpending(params) {
	    return spendingData.getTransactionsSpending(params).then(function (response) {
	      return response.data;
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  };
	
	  /**
	   * @name spendingModel#loadProducts
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Load products from API.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var loadProducts = function loadProducts() {
	    return productSummaryData.getProductsummary().then(function (_ref) {
	      var data = _ref.data;
	
	      bbStorage.setItem(_constants.bbStorageKeys.PRODUCT_SUMMARY, data);
	      return data;
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  };
	
	  /**
	   * @name spendingModel#getProducts
	   * @type {function}
	   *
	   * @description
	   * Get products.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var getProducts = function getProducts() {
	    return bbStorage.getItem(_constants.bbStorageKeys.PRODUCT_SUMMARY).then(function (data) {
	      return FROM_STORAGE && data ? data : loadProducts();
	    });
	  };
	
	  /**
	   * @name spendingModel#getProductsArray
	   * @type {function}
	   *
	   * @description
	   * Get products.
	   *
	   * @param {boolean} keepEmptyProducts defines if empty product kinds should be passed.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to array with Products.
	   */
	  var getProductsArray = function getProductsArray() {
	    var keepEmptyProducts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	    return getProducts().then(function (data) {
	      return Object.keys(data).filter(function (kind) {
	        return keepEmptyProducts || productKindHasProducts(data[kind]);
	      }).reduce(function (products, kind) {
	        var extendedProducts = data[kind].products.map(function (product) {
	          return Object.assign(product, { kind: kind });
	        });
	        return products.concat(extendedProducts);
	      }, []);
	    });
	  };
	
	  /**
	   * @name spendingModel#getProductByID
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Get products kinds.
	   *
	   * @param {any} productId product ID to get a stored product record
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var getProductByID = function getProductByID(productId) {
	    return getProductsArray().then(function (products) {
	      var defaultProduct = GET_FIRST_AS_DEFAULT && products[0] ? products[0] : null;
	      return productId ? products.find(function (product) {
	        return product.id === productId;
	      }) : defaultProduct;
	    });
	  };
	
	  /**
	   * @name spendingModel#getSelectedProduct
	   * @type {function}
	   *
	   * @description
	   * Get current selected product.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to a selected product object.
	   */
	  var getSelectedProduct = function getSelectedProduct() {
	    return bbStorage.getItem(_constants.bbStorageKeys.PRODUCT_SELECTED).then(function (id) {
	      return getProductByID(id);
	    });
	  };
	
	  /**
	   * @name spendingModel#setSelectedProduct
	   * @type {function}
	   *
	   * @description
	   * Set selected product to the sorage
	   *
	   * @param {Product} selectedProduct The selected product value
	   */
	  var setSelectedProduct = function setSelectedProduct(selectedProduct) {
	    if (selectedProduct) {
	      bbStorage.setItem(_constants.bbStorageKeys.PRODUCT_SELECTED, selectedProduct.id);
	    } else {
	      bbStorage.removeItem(_constants.bbStorageKeys.PRODUCT_SELECTED);
	    }
	  };
	
	  /**
	   * @name spendingModel#transformToSeries
	   * @type {function}
	   *
	   * @description
	   * Transforms data into format suitable for chart UI components
	   *
	   * @param {Spending} spendings Spending data
	   * @returns {BBSeries} Data in format suitable for chart UI components
	   */
	  var transformToSeries = function transformToSeries(spendings) {
	    spendings.items.sort(function (a, b) {
	      return b.portion - a.portion;
	    });
	    return {
	      datasets: [{
	        data: spendings.items.map(function (item) {
	          return item.portion;
	        })
	      }],
	      labels: spendings.items.map(function (item) {
	        return item.category;
	      })
	    };
	  };
	
	  /**
	   * @name spendingModel
	   * @type {Object}
	   */
	  return {
	    E_PARAMS: _constants.E_PARAMS,
	    validateSpendingParameters: validateSpendingParameters,
	    loadSpending: loadSpending,
	    getProducts: getProducts,
	    getProductsArray: getProductsArray,
	    getSelectedProduct: getSelectedProduct,
	    setSelectedProduct: setSelectedProduct,
	    transformToSeries: transformToSeries
	  };
	}

/***/ }),
/* 20 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_20__;

/***/ }),
/* 21 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name bbStorageKeys
	 * @description
	 * bbStorage keys enum
	 * @type {object}
	 */
	var bbStorageKeys = exports.bbStorageKeys = {
	  PRODUCT_SELECTED: 'bb.product.selected',
	  PRODUCT_SUMMARY: 'bb.product.summary.data'
	};
	
	/**
	 * @name preferencesKeys
	 * @description
	 * Preferences keys enum
	 * @type {object}
	 */
	var preferencesKeys = exports.preferencesKeys = {
	  IS_FIRST_PRODUCT_DEFAULT: 'bb.spending.useFirstProductAsDefault',
	  IS_PRODUCTS_LIST_FROM_STORAGE: 'bb.spending.getProductsFromStorage'
	};
	
	/**
	 * @name E_PARAMS
	 * @description
	 * Additional model error in case required parameters are missing
	 * @type {string}
	 */
	var E_PARAMS = exports.E_PARAMS = 'error.load.params';

/***/ })
/******/ ])
});
;
//# sourceMappingURL=model-bb-spending-ng.js.map