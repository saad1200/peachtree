# ui-bbm-currency-input-ng


Version: **1.0.216**

Currency input UI component

## Imports

* vendor-bb-angular

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbmCurrencyInputNgKey from 'ui-bbm-currency-input-ng';

export const dependencyKeys = [
  uiBbmCurrencyInputNgKey,
];

// file: templates/template.ng.html
<ui-bbm-currency-input-ng
  max-length="6"
  decimal-max-length="2"
  ng-model="$ctrl.payment.amount"
  currency="$ctrl.currency">
</ui-bbm-currency-input-ng>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>

## Exports

### <a name="default"></a>*default*

The angular module name

**Type:** *String*


---

## uiBbmCurrencyInputNg


| Property | Type | Description |
| :-- | :-- | :-- |
| max-length | String | Maximum number of digits allowed in integer part |
| decimal-max-length | String | Maximum number of digits allowed in decimal part |
| ng-model | Object | Currency input model |
