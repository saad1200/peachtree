(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("model-bb-transactions-ng"), require("lib-bb-event-bus-ng"), require("lib-bb-intent-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-transactions-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "lib-bb-model-errors", "lib-bb-widget-extension-ng", "model-bb-transactions-ng", "lib-bb-event-bus-ng", "lib-bb-intent-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-transactions-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("model-bb-transactions-ng"), require("lib-bb-event-bus-ng"), require("lib-bb-intent-ng"));
	else
		root["widget-bb-transactions-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"], root["lib-bb-widget-extension-ng"], root["model-bb-transactions-ng"], root["lib-bb-event-bus-ng"], root["lib-bb-intent-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_63__, __WEBPACK_EXTERNAL_MODULE_68__, __WEBPACK_EXTERNAL_MODULE_83__, __WEBPACK_EXTERNAL_MODULE_84__, __WEBPACK_EXTERNAL_MODULE_85__, __WEBPACK_EXTERNAL_MODULE_86__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(82);

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),

/***/ 63:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_63__;

/***/ }),

/***/ 68:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_68__;

/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetExtensionNg = __webpack_require__(83);
	
	var _libBbWidgetExtensionNg2 = _interopRequireDefault(_libBbWidgetExtensionNg);
	
	var _libBbWidgetNg = __webpack_require__(63);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _modelBbTransactionsNg = __webpack_require__(84);
	
	var _libBbEventBusNg = __webpack_require__(85);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _libBbIntentNg = __webpack_require__(86);
	
	var _libBbIntentNg2 = _interopRequireDefault(_libBbIntentNg);
	
	var _controller = __webpack_require__(87);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	var _defaultHooks = __webpack_require__(89);
	
	var defaultHooks = _interopRequireWildcard(_defaultHooks);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module widget-bb-transactions-ng
	 *
	 * @description
	 * Transactions widget.
	 *
	 * @usage
	 * <div ng-controller="TransactionsController as $ctrl">
	 *   <ul>
	 *     <li ng-repeat="transaction in $ctrl.transactions">{{transaction.id}}</li>
	 *   </ul>
	 * </div>
	 */
	var hooksKey = 'widget-bb-transactions-ng:hooks';
	
	exports.default = _vendorBbAngular2.default.module('widget-bb-transactions-ng', [_modelBbTransactionsNg.modelTransactionsModuleKey, _libBbWidgetNg2.default, _libBbEventBusNg2.default, _libBbIntentNg2.default]).factory(hooksKey, (0, _libBbWidgetExtensionNg2.default)(defaultHooks)).controller('TransactionsController', [
	// dependencies to inject
	_modelBbTransactionsNg.modelTransactionsKey, hooksKey, _libBbWidgetNg.widgetKey, _libBbEventBusNg.eventBusKey, _libBbIntentNg.bbIntentKey, '$window', '$scope', _libBbWidgetNg.widgetKey, '$filter', '$window',
	/* into */
	_controller2.default]).name;

/***/ }),

/***/ 83:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_83__;

/***/ }),

/***/ 84:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_84__;

/***/ }),

/***/ 85:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_85__;

/***/ }),

/***/ 86:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_86__;

/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _errorMessages;
	
	exports.default = TransactionsController;
	
	var _libBbModelErrors = __webpack_require__(68);
	
	var _constants = __webpack_require__(88);
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var SEARCH_MIN_QUERY = 2;
	var SEARCH_INPUT_DEBOUNCE = 1000;
	
	var errorMessages = (_errorMessages = {}, _defineProperty(_errorMessages, _libBbModelErrors.E_AUTH, 'model.error.auth'), _defineProperty(_errorMessages, _libBbModelErrors.E_CONNECTIVITY, 'model.error.connectivity'), _defineProperty(_errorMessages, _libBbModelErrors.E_USER, 'model.error.user'), _defineProperty(_errorMessages, _libBbModelErrors.E_UNEXPECTED, 'model.error.unexpected'), _errorMessages);
	
	var searchActions = {
	  CANCEL: 'cancel',
	  INPUT: 'input'
	};
	
	var uiError = function uiError(messageMap, modelError) {
	  return {
	    message: messageMap[modelError.code]
	  };
	};
	
	var debounce = function debounce(func, wait, immediate) {
	  var timeout = void 0;
	  return function debouncedFn() {
	    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
	      args[_key] = arguments[_key];
	    }
	
	    var context = this;
	    var later = function later() {
	      timeout = null;
	      if (!immediate) {
	        func.apply(context, args);
	      }
	    };
	    var callNow = immediate && !timeout;
	    clearTimeout(timeout);
	    timeout = setTimeout(later, wait);
	    if (callNow) {
	      func.apply(context, args);
	    }
	  };
	};
	
	/**
	 * Defines the default pageSize for the direct debits page
	 * @type {int}
	 */
	var DEFAULT_PAGE_SIZE = 10;
	
	/**
	 * Defines the default maxNavPages for the accounts page
	 * as defined in the widget model.xml
	 * @type {int}
	 */
	var DEFAULT_MAX_NAV_PAGES = 3;
	
	/**
	 * Defines the default paginationType for the accounts page
	 * as defined in the widget model.xml
	 * @type {string}
	 */
	var DEFAULT_PAGINATION_TYPE = 'load-more';
	
	/**
	 * @name TransactionsController
	 * @ngkey TransactionsController
	 * @type {object}
	 * @description
	 * Transactions list controller
	 */
	function TransactionsController(model, hooks, widgetInstance, eventBus, bbIntent, window, scope, widget, filter, $window) {
	  /**
	   * @name errors
	   * @type {errors} errors - errors model
	   */
	  var errors = {
	    transactionsError: null,
	    clearError: function clearError(key) {
	      errors[key] = null;
	    }
	  };
	
	  /**
	   * @name pageSize
	   * @type {Number} pageSize
	   * @description
	   * Number of records to return per request
	   *
	   * The transactionListSize property is deprecated in favor of bb.transaction.pageSize
	   */
	  var pageSize = widgetInstance.getLongPreference('transactionListSize') || widgetInstance.getLongPreference('bb.transaction.pageSize') || DEFAULT_PAGE_SIZE;
	
	  /**
	   * @name categoryTransactionsIntent
	   * @type {String} categoryTransactionsIntent
	   * @description
	   * Intent event
	   */
	  var intentPreferencesArray = widgetInstance.getStringPreference('intents').split(',');
	
	  /**
	   * @name maxNavPages
	   * @type {Number} maxNaxPages
	   * @description
	   * Maximum number of navigation pages to display
	   */
	  var maxNavPages = widgetInstance.getLongPreference('bb.transaction.maxNavPages') || DEFAULT_MAX_NAV_PAGES;
	
	  /**
	   * @name paginationType
	   * @type {String}
	   * @description
	   * Type of pagination (pagination, load-more)
	   */
	  var paginationType = hooks.defaultPaginationType() || widgetInstance.getStringPreference('bb.transaction.paginationType') || DEFAULT_PAGINATION_TYPE;
	
	  /**
	   * @name dismissTime
	   * @type {number}
	   *
	   * @description
	   * Number of seconds to dismiss the notification
	   */
	  var dismissTime = Math.abs(widgetInstance.getLongPreference('notificationDismissTime')) || 3;
	
	  /**
	   * @name loadAllTransactionsInitially
	   * @type {boolean}
	   *
	   * @description
	   * Defines whether data should be shown if no account is selected
	   */
	  var loadAllTransactionsInitially = widgetInstance.getBooleanPreference('bb.transaction.loadAllTransactionsOnInit');
	
	  /**
	   * @name initLoad
	   * @type {boolean}
	   *
	   * @description
	   * Defines whether any data should be shown on widget init
	   */
	  var initLoad = widgetInstance.getBooleanPreference('bb.transaction.initLoad');
	
	  /**
	   * @name transactions
	   * @type {transactions} transactions - transactions model
	   */
	  var transactions = {
	    params: {
	      // Pagination params
	      size: pageSize,
	      from: 0
	    },
	    rawItems: [],
	    items: [],
	    loading: false,
	    loadedAll: false,
	    error: null,
	    showAll: hooks.isTransactionsShown()
	  };
	
	  var state = {
	    exportTransactionsFailed: false,
	    export: {
	      hasTodaysTransactions: false,
	      today: ''
	    },
	    get transactions() {
	      return transactions.items;
	    },
	    sort: {
	      orderBy: hooks.defaultSortableColumn(),
	      direction: hooks.defaultSortableDirection()
	    },
	    pageParams: {
	      from: 0,
	      size: pageSize,
	      currentPage: 1,
	      totalItems: 0,
	      maxNavPages: maxNavPages,
	      paginationType: paginationType
	    },
	    initialLoading: true
	  };
	
	  /**
	   * @name transactionsSearch
	   * @type {transactionsSearch} transactionsSearch - transactions model for search results
	   */
	  var transactionsSearch = {
	    params: {
	      // Pagination params
	      size: pageSize,
	      from: 0,
	      bookingDateLessThan: null,
	      bookingDateGreaterThan: null,
	      amountGreaterThan: null,
	      amountLessThan: null,
	      creditDebitIndicator: null
	    },
	    rawItems: [],
	    items: [],
	    loading: false,
	    loadedAll: false,
	    searching: false,
	    error: null
	  };
	
	  /**
	   * @name transaction
	   * @type {transaction} transaction - transaction model
	   */
	  var transaction = {
	    data: null
	  };
	
	  /**
	   * @name product
	   * @type {product} product - product model
	   */
	  var product = {
	    data: null
	  };
	
	  /**
	   * @name loadMorePromise
	   * @type {promise.<void>} loadMorePromise
	   */
	  var loadMorePromise = null;
	
	  /**
	   * @name searchMorePromise
	   * @type {promise.<void>} searchMorePromise
	   */
	  var searchMorePromise = null;
	
	  /**
	   * @description
	   * Merges new transactions with existing transactions
	   *
	   * @inner
	   * @name TransactionsController#append
	   * @type {function}
	   * @returns {array} merged array of old and new transactions
	   */
	  function append(newItems, existingItems) {
	    return [].concat(_toConsumableArray(existingItems), _toConsumableArray(newItems));
	  }
	
	  /**
	   * @description
	   * Replaces existing transactions with new transactions
	   *
	   * @inner
	   * @name TransactionsController#replace
	   * @type {function}
	   * @returns {array} new transactions
	   */
	  function replace(items) {
	    return items;
	  }
	
	  /**
	   * @description
	   * Searches by a given query
	   *
	   * @public
	   * @name TransactionsController#search
	   * @type {function}
	   * @param {string} query
	   * @param {function} [merge]
	   * @returns {promise.<array>}
	   */
	  function search(query) {
	    var merge = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : replace;
	
	    transactionsSearch.searching = true;
	    transactionsSearch.loading = true;
	
	    transactionsSearch.params.query = query;
	
	    return model.load(transactionsSearch.params).then(function (raw) {
	      if (transactionsSearch.searching && query === transactionsSearch.params.query) {
	        if (raw.data.length < transactionsSearch.params.size) {
	          transactionsSearch.loadedAll = true;
	        }
	
	        transactionsSearch.error = null;
	        transactionsSearch.loading = false;
	
	        transactionsSearch.rawItems = merge(raw.data, transactionsSearch.rawItems);
	        transactionsSearch.items = hooks.processTransactions(transactionsSearch.rawItems);
	        state.pageParams.totalItems = raw.totalCount || 0;
	      }
	      return transactionsSearch.items;
	    }).catch(function (err) {
	      transactionsSearch.error = err;
	      transactionsSearch.loading = false;
	
	      errors.transactionsError = uiError(errorMessages, err);
	      throw errors.transactionsError;
	    });
	  }
	
	  /**
	   * @description
	   * Searches by given filter parameters
	   *
	   * @public
	   * @name TransactionsController#applySearchFilter
	   * @type {function}
	   * @param {Object} params
	   * @param {function} [merge]
	   * @returns {promise.<array>}
	   */
	  function applySearchFilter(params) {
	    var merge = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : replace;
	
	    transactionsSearch.searching = true;
	    transactionsSearch.loading = true;
	
	    transactionsSearch.params.query = params.query;
	    transactionsSearch.params.bookingDateLessThan = params.toDate;
	    transactionsSearch.params.bookingDateGreaterThan = params.fromDate;
	    transactionsSearch.params.amountGreaterThan = params.amountFrom;
	    transactionsSearch.params.amountLessThan = params.amountTo;
	    transactionsSearch.params.creditDebitIndicator = params.creditDebitIndicator;
	
	    // Reset pagination on search
	    transactionsSearch.params.from = 0;
	    state.pageParams.currentPage = 1;
	
	    return model.load(transactionsSearch.params).then(function (raw) {
	      if (transactionsSearch.searching && params.query === transactionsSearch.params.query) {
	        if (raw.data.length < transactionsSearch.params.size) {
	          transactionsSearch.loadedAll = true;
	        }
	
	        transactionsSearch.error = null;
	        transactionsSearch.loading = false;
	
	        transactionsSearch.rawItems = merge(raw.data, transactionsSearch.rawItems);
	        transactionsSearch.items = hooks.processTransactions(transactionsSearch.rawItems);
	        state.pageParams.totalItems = raw.totalCount || 0;
	      }
	      return transactionsSearch.items;
	    }).catch(function (err) {
	      transactionsSearch.error = err;
	      transactionsSearch.loading = false;
	
	      errors.transactionsError = uiError(errorMessages, err);
	      throw errors.transactionsError;
	    });
	  }
	
	  /**
	   * @description
	   * Resets search
	   *
	   * @public
	   * @name TransactionsController#cancelSearch
	   * @type {function}
	   */
	  function cancelSearch() {
	    Object.assign(transactionsSearch, {
	      error: null,
	      items: [],
	      loading: false,
	      loadedAll: false,
	      rawItems: [],
	      searching: false
	    });
	
	    Object.assign(transactionsSearch.params, {
	      from: 0,
	      // Search filter additional params
	      bookingDateLessThan: null,
	      bookingDateGreaterThan: null,
	      amountGreaterThan: null,
	      amountLessThan: null,
	      creditDebitIndicator: null
	    });
	
	    Object.assign(errors, {
	      transactionsError: null
	    });
	  }
	
	  /**
	   * @description
	   * Loads more search results and appends them to the search result.
	   *
	   * @public
	   * @name TransactionsController#searchMore
	   * @type {function}
	   * @returns {promise.<array>} searchMorePromise
	   */
	  function searchMore() {
	    if (transactionsSearch.loading) {
	      return searchMorePromise;
	    }
	
	    transactionsSearch.params.from++;
	
	    searchMorePromise = search(transactionsSearch.params.query, append).then(function () {
	      searchMorePromise = null;
	    }).catch(function () {
	      transactionsSearch.params.from--;
	      searchMorePromise = null;
	    });
	
	    return searchMorePromise;
	  }
	
	  /**
	   * @description
	   * Sets selected product
	   *
	   * @inner
	   * @name TransactionsController#setupProductSelected
	   * @returns {promise.<object>}
	   */
	  function setupProductSelected() {
	    return model.getSelectedProduct(hooks.isDefaultProductFirst()).then(function (selectedProduct) {
	      product.data = hooks.processProductSelected(selectedProduct);
	      return product.data;
	    }).catch(function (error) {
	      errors.transactionsError = uiError(errorMessages, error);
	      throw errors.transactionsError;
	    });
	  }
	
	  /**
	   * @description
	   * Set initial params
	   *
	   * @inner
	   * @name TransactionsController#setRequestParams
	   * @param {object} params
	   */
	  function setRequestParams(params) {
	    // Configure additional params, f.e. set date interval, or transactions type to filter
	    var additionalParams = hooks.processRequestParams(params);
	    Object.assign(transactions.params, additionalParams);
	    transactionsSearch.params = Object.assign({}, transactions.params);
	  }
	
	  /**
	   * @description
	   * Handles combining transactions and transactionsSearch params
	   *
	   * @inner
	   * @name TransactionsController#combineParams
	   * @type {function}
	   * @param {object} params
	   * @returns {promise.<array>}
	   */
	  var combineParams = function combineParams() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    if (params.currentPage) {
	      Object.assign(transactionsSearch.params, {
	        // In BE services pagination starts from 0 page, but in bootstrap directive it's 1
	        from: params.currentPage - 1,
	        currentPage: params.currentPage
	      });
	    }
	
	    Object.assign(params, transactionsSearch.params);
	
	    if (state.sort.orderBy && state.sort.orderBy) {
	      Object.assign(params, {
	        orderBy: state.sort.orderBy,
	        direction: state.sort.direction
	      });
	    }
	
	    Object.assign(transactions.params, params);
	  };
	
	  /**
	   * @description
	   * Handles loading transactions
	   *
	   * @public
	   * @name TransactionsController#loadTransactions
	   * @type {function}
	   * @param {function} merge
	   * Defines how new items should be merged with current items
	   * @returns {promise.<array>}
	   */
	  function loadTransactions(merge) {
	    var transactionsListRef = transactionsSearch.searching ? transactionsSearch : transactions;
	    transactionsListRef.error = null;
	    transactionsListRef.loading = true;
	
	    return model.load(transactions.params).then(function (raw) {
	      if (raw.data.length < transactions.params.size) {
	        transactionsListRef.loadedAll = true;
	      }
	
	      transactionsListRef.error = null;
	      transactionsListRef.loading = false;
	
	      transactionsListRef.rawItems = merge(raw.data, transactionsListRef.rawItems);
	      state.pageParams.totalItems = raw.totalCount || 0;
	      state.pageParams.currentPage = transactions.params.currentPage || 1;
	
	      // process transactions, f.e. map them
	      transactionsListRef.items = hooks.processTransactions(transactionsListRef.rawItems);
	
	      return transactionsListRef.items;
	    }).catch(function (err) {
	      transactionsListRef.error = err;
	      transactionsListRef.loading = false;
	
	      errors.transactionsError = uiError(errorMessages, err);
	      throw errors.transactionsError;
	    });
	  }
	
	  /**
	   * @description
	   * Loads more transactions and append them to the transaction's list
	   *
	   * @public
	   * @name TransactionsController#loadMore
	   * @type {function}
	   * @returns {promise.<array>} loadMorePromise
	   * @param {function} done Callback function for `ui-bb-load-more-ng` component
	   */
	  function loadMore(done) {
	    if (transactions.loading || transactions.loadedAll || transactions.error) {
	      return loadMorePromise;
	    }
	
	    transactions.params.from++;
	
	    transactions.params = hooks.extendLoadMoreParams(transactions.params);
	
	    loadMorePromise = loadTransactions(append).then(function () {
	      loadMorePromise = null;
	      if (done) {
	        done();
	      }
	    }).catch(function () {
	      transactions.params.from--;
	      loadMorePromise = null;
	      if (done) {
	        done();
	      }
	    });
	
	    return loadMorePromise;
	  }
	
	  /**
	   * @description
	   * Change page of displayed accounts.
	   *
	   * @name AccountsOverviewController#changePage
	   * @type {function}
	   * @returns {Promise.<module:model-bb-product-summary-ng.Accounts, ModelError>}
	   * A Promise with loaded accounts
	   */
	  var changePage = function changePage() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    combineParams(params);
	
	    return loadTransactions(replace);
	  };
	
	  /**
	   * @description
	   * Helper function to iterate transaction collection and check for same day in bookingDate field
	   *
	   * @name isSameDay
	   * @type {function}
	   * @inner
	   * @param {string} today  String representing the current day
	   * @returns {function}
	   */
	  var isSameDay = function isSameDay(today) {
	    return function (item) {
	      return item.bookingDate === today;
	    };
	  };
	
	  /**
	   * @description
	   * Returns a promise that will be fulfilled with the check if there is at least one
	   * transaction booked for the current day
	   *
	   * @name checkTodaysTransactions
	   * @type {function}
	   * @inner
	   * @param {string} productId Id of the account
	   * @returns {Promise.<boolean>}
	   */
	  var checkTodaysTransactions = function checkTodaysTransactions(productId) {
	    var today = filter('date')(new Date(), 'yyyy-MM-dd');
	    var requestParameters = {
	      productId: productId,
	      from: 0,
	      size: 1,
	      bookingDateLessThan: today,
	      bookingDateGreaterThan: today
	    };
	
	    state.export.hasTodaysTransactions = false;
	    state.export.today = today;
	
	    return model.load(requestParameters).then(function (_ref) {
	      var data = _ref.data;
	      return data.some(isSameDay(today));
	    }).then(function (result) {
	      return state.export.hasTodaysTransactions = result;
	    }).catch(function (error) {
	      errors.transactionsError = uiError(errorMessages, error);
	      throw errors.transactionsError;
	    });
	  };
	
	  /**
	   * @description
	   * Handles account select
	   *
	   * @see cancelSearch
	   * @see loadTransactions
	   *
	   * @inner
	   * @name TransactionsController#onProductSelect
	   * @param {object} payload Object with selected product
	   */
	  function onProductSelect(payload) {
	    if (payload.product && transactions.params.productId === payload.product.id) {
	      return;
	    }
	    var productId = payload.product ? payload.product.id : null;
	    product.data = payload.product ? hooks.processProductSelected(payload.product) : null;
	
	    setRequestParams({
	      // Products params
	      productId: productId
	    });
	
	    transactions.items = hooks.processTransactions([]);
	
	    cancelSearch();
	    // Reset pagination status on changed product
	    combineParams({ currentPage: 1 });
	    loadTransactions(replace);
	    checkTodaysTransactions(productId);
	  }
	
	  /**
	   * @description
	   * Handles transaction select
	   *
	   * @inner
	   * @name TransactionsController#onTransactionClick
	   * @param {object} selectedTransaction Object with selected transaction
	   * @fires bb.event.transaction.selected
	   */
	  function onTransactionClick(selectedTransaction) {
	    transaction.data = selectedTransaction;
	    model.storeTransactionAsCurrent(selectedTransaction);
	    eventBus.publish(_constants.Message.TRANSACTION_SELECTED, {
	      transaction: selectedTransaction
	    });
	  }
	
	  /**
	   * @description
	   * Handles search field input
	   *
	   * @see search
	   *
	   * @inner
	   * @name TransactionsController#onTransactionSearchInput
	   */
	  var onTransactionSearchInput = debounce(function (value) {
	    if (value.length >= SEARCH_MIN_QUERY) {
	      transactionsSearch.params.from = 0;
	      search(value, replace);
	    }
	  }, SEARCH_INPUT_DEBOUNCE);
	
	  /**
	   * @description
	   * Handles search cancel
	   *
	   * @see cancelSearch
	   *
	   * @inner
	   * @name TransactionsController#onTransactionSearchCancel
	   */
	  function onTransactionSearchCancel() {
	    cancelSearch();
	  }
	
	  /**
	   * @description
	   * sets selected transaction from sync pref
	   *
	   * @inner
	   * @name TransactionsController#setupTransactionSelected
	   */
	  function setupTransactionSelected() {
	    model.getCurrentTransaction().then(function (currentTransaction) {
	      transaction.data = currentTransaction;
	    });
	  }
	
	  /**
	   * @description
	   * Handles transaction select
	   *
	   * @inner
	   * @name TransactionsController#onTransactionSelect
	   */
	  function onTransactionSelect(data) {
	    transaction.data = data.transaction;
	  }
	
	  /**
	   * @description
	   * Adds subscriptions to bus events
	   *
	   * @inner
	   * @name TransactionsController#bindEvents
	   */
	  function bindEvents() {
	    eventBus.subscribe(_constants.Message.PRODUCT_SELECTED, onProductSelect);
	    eventBus.subscribe(_constants.Message.TRANSACTION_SELECTED, onTransactionSelect);
	
	    window.addEventListener(_constants.Message.SEARCH, function (_ref2) {
	      var detail = _ref2.detail;
	
	      if (detail.action === searchActions.INPUT) {
	        onTransactionSearchInput(detail.text);
	      } else if (detail.action === searchActions.CANCEL) {
	        onTransactionSearchCancel();
	      }
	      scope.$digest();
	    });
	  }
	
	  /**
	   * @description
	   * Inits the bbIntent and adds intent handlers.
	   * 'view.account.category.transactions' intent can be used to
	   * update load query params with new productId, category, bookingDateGreaterThan
	   * and bookingDateLessThan params
	   *
	   * @inner
	   * @name TransactionsController#initIntent
	   */
	  function initIntent() {
	    if (intentPreferencesArray.indexOf(_constants.Intent.VIEW_CATEGORY_TRANSACTIONS) > -1) {
	      bbIntent.handle(_constants.Intent.VIEW_CATEGORY_TRANSACTIONS, function (payload) {
	        if (payload && payload.category !== '') {
	          transactions.showAll = true;
	
	          setRequestParams({
	            productId: payload.productId || null,
	            category: payload.category || null,
	            bookingDateGreaterThan: payload.bookingDateGreaterThan || null,
	            bookingDateLessThan: payload.bookingDateLessThan || null
	          });
	
	          loadTransactions(replace).then(setupTransactionSelected);
	        } else {
	          transactions.showAll = false;
	        }
	      });
	    }
	
	    bbIntent.init(function () {});
	  }
	
	  /**
	   * @description
	   * Widget initialization logic - called automatically in the angular cycle.
	   *
	   * @inner
	   * @name TransactionsController#$onInit
	   */
	  function $onInit() {
	    if (!initLoad) {
	      state.initialLoading = false;
	      return Promise.resolve(bindEvents());
	    }
	
	    return setupProductSelected().then(function (data) {
	      if (data) {
	        setRequestParams({ productId: data.id });
	      } else {
	        // No account has been selected
	        product.data = { productId: -1 };
	        if (!loadAllTransactionsInitially) {
	          return Promise.reject(bindEvents());
	        }
	      }
	
	      return bindEvents();
	    }).then(initIntent).then(function () {
	      /**
	       * This event (cxp.item.loaded) is deprecated in Mobile SDK version > 3.0
	       * and will be removed with the update to widget collection 3 (WC3)
	       */
	      eventBus.publish(_constants.Message.CXP_ITEM_LOADED, {
	        id: widget.getId()
	      });
	
	      eventBus.publish(_constants.Message.BB_ITEM_LOADED, {
	        id: widget.getId()
	      });
	    }).then(function () {
	      return loadTransactions(replace);
	    }).then(function () {
	      checkTodaysTransactions(transactions.params.productId);
	    }).then(setupTransactionSelected).then(function () {
	      return state.initialLoading = false;
	    }).catch(function () {
	      return state.initialLoading = false;
	    });
	  }
	
	  /**
	   * @name onSort
	   * @type {function}
	   * @description
	   * Loads sorted list of payments
	   * @param  {string} orderBy   Column key to sort
	   * @param  {string} direction Sort direction
	   * @param {object} header config object reference
	   */
	  var onSort = function onSort(orderBy, direction, header) {
	    var headerRef = header;
	    headerRef.sortable.direction = direction;
	    state.sort = {
	      orderBy: orderBy,
	      direction: direction
	    };
	    combineParams();
	    loadTransactions(replace);
	  };
	
	  /**
	   * @name exportToFile
	   * @type {function}
	   *
	   * @description
	   * Exports filtered list of payments to a file in specified format
	   * @param {string} format Specified format (CSV, PDF)
	   */
	  var exportToFile = function exportToFile(format) {
	    // Export payments for today if no filter applied
	    var requestParameters = Object.assign({}, transactionsSearch.params, {
	      // Remove pagination parameters
	      from: null,
	      size: null
	    });
	    if (!transactionsSearch.searching) {
	      if (!state.export.hasTodaysTransactions) {
	        state.exportTransactionsFailed = true;
	      }
	
	      requestParameters.bookingDateLessThan = state.export.today;
	      requestParameters.bookingDateGreaterThan = state.export.today;
	    } else if (!transactionsSearch.items.length) {
	      state.exportTransactionsFailed = true;
	    }
	
	    if (!state.exportTransactionsFailed) {
	      requestParameters.exportFormat = format;
	      var uri = model.getExportFileResource(requestParameters);
	      $window.location.assign(uri);
	    }
	  };
	
	  return {
	    /**
	     * @description
	     * The list of transactions
	     *
	     * @type {array.<object>} The array of transactions.
	     * May be empty if the transactions aren't loaded.
	     * @name transcations
	     */
	    get transactions() {
	      return transactions.items;
	    },
	    /**
	     * @description
	     * Selected product info
	     *
	     * @type {object}
	     * @name product
	     */
	    get product() {
	      return product.data;
	    },
	    /**
	     * @description
	     * Loading flag which is true while the list of transactions is loading
	     *
	     * @type {boolean}
	     * @name loading
	     */
	    get loading() {
	      return transactions.loading || transactionsSearch.loading;
	    },
	    /**
	     * @description
	     * Loading error
	     *
	     * @type {error}
	     * @name loadingError
	     */
	    get loadingError() {
	      return transactions.error;
	    },
	    /**
	     * @description
	     * Flag that indicates whether all the transactions have been loaded
	     *
	     * @type {boolean}
	     * @name allTransactionsLoaded
	     */
	    get allTransactionsLoaded() {
	      return transactions.loadedAll;
	    },
	    /**
	     * @description
	     * Searching flag which is true while user is searching transactions
	     *
	     * @type {boolean}
	     * @name searching
	     */
	    get searching() {
	      return transactionsSearch.searching;
	    },
	    /**
	     * @description
	     * Loading flag which is true while the list of transactions is loading
	     *
	     * @type {boolean}
	     * @name searchLoading
	     */
	    get searchLoading() {
	      return transactionsSearch.loading;
	    },
	    /**
	     * @description
	     * Search error
	     *
	     * @type {error}
	     * @name searchLoadingError
	     */
	    get searchLoadingError() {
	      return transactionsSearch.error;
	    },
	    /**
	     * @description
	     * The list of found transactions.
	     *
	     * @type {array.<object>} The array of transactions.
	     * @name searchTransactions
	     */
	    get searchTransactions() {
	      return transactionsSearch.items;
	    },
	    /**
	     * @description
	     * Check is search filter applied
	     *
	     * @type {boolean} search status
	     * @name searchTransactions
	     */
	    get isSearching() {
	      return transactionsSearch.searching;
	    },
	    /**
	     * @description
	     * Flag that indicates whether all the transactions have been loaded during a search
	     *
	     * @type {boolean}
	     * @name searchAllTransactionsLoaded
	     */
	    get searchAllTransactionsLoaded() {
	      return transactionsSearch.loadedAll;
	    },
	    /**
	     * @description
	     * Selected transaction info
	     *
	     * @type {object}
	     * @name transcation
	     */
	    get transaction() {
	      return transaction.data;
	    },
	    /**
	     * @description
	     * Show all available transactions
	     *
	     * @type {boolean}
	     * @name showAllTransactions
	     */
	    get showAllTransactions() {
	      return transactions.showAll;
	    },
	    cancelSearch: cancelSearch,
	    loadMore: loadMore,
	    search: search,
	    applySearchFilter: applySearchFilter,
	    searchMore: searchMore,
	    eventBus: eventBus,
	    onTransactionClick: onTransactionClick,
	    errors: errors,
	    state: state,
	    onSort: onSort,
	    /* Lifecycle hooks */
	    $onInit: $onInit,
	    changePage: changePage,
	    exportToFile: exportToFile,
	    dismissTime: dismissTime,
	    loadAllTransactionsInitially: loadAllTransactionsInitially
	  };
	}

/***/ }),

/***/ 88:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Widget events enum
	 * @type {object}
	 */
	var Event = {
	  TRANSACTION_SELECTED: 'bb.event.transaction.selected',
	  PRODUCT_SELECTED: 'bb.event.product.selected',
	  SEARCH: 'bb.event.transactions.search',
	
	  CXP_ITEM_LOADED: 'cxp.item.loaded',
	  BB_ITEM_LOADED: 'bb.item.loaded'
	};
	
	/**
	 * Widget actions enum
	 * @type {object}
	 */
	var Action = {};
	
	/**
	 * Widget messages enum
	 * @type {object}
	 */
	var Message = exports.Message = Object.assign({}, Action, Event);
	
	/**
	 * Widget static text
	 * @type {object}
	 */
	var Text = exports.Text = {};
	
	/**
	 * Widget intent enum
	 * @type {object}
	 */
	var Intent = exports.Intent = {
	  VIEW_CATEGORY_TRANSACTIONS: 'view.account.category.transactions'
	};

/***/ }),

/***/ 89:
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.processProductSelected = processProductSelected;
	exports.processRequestParams = processRequestParams;
	exports.processTransactions = processTransactions;
	exports.extendLoadMoreParams = extendLoadMoreParams;
	/**
	 * @name default-hooks
	 * @type {object}
	 *
	 * @description
	 * Default hooks for widget-bb-transactions-ng
	 */
	
	/**
	 * @name default-hooks#processProductSelected
	 * @type {function}
	 *
	 * @description
	 * Default hook for selected product processing
	 *
	 * @param {object} productSelected Product to process
	 * @returns {object}
	 */
	function processProductSelected(productSelected) {
	  return productSelected;
	}
	
	/**
	 * @name default-hooks#processRequestParams
	 * @type {function}
	 *
	 * @description
	 * Default hook for transactions query params processing/extending
	 *
	 * @param {object} params to process
	 * @param {?number} params.amountGreaterThan Amount greater than
	 * @param {?number} params.amountLessThan Amount less than
	 * @param {?string} params.bookingDateGreaterThan Booking date greater than
	 * @param {?string} params.bookingDateLessThan Booking date less than
	 * @param {?string} params.productId The product id
	 * @param {?string} params.type Type category
	 * @param {?string} params.orderBy The key to order by
	 * @param {?string} params.direction The direction to order by
	 * @param {?number} params.from The page to list from
	 * @param {?number} params.size The number of results per page
	 * @param {?string} params.query The search term used to search for transactions
	 * @returns {Object} params
	 */
	function processRequestParams(params) {
	  return params;
	}
	
	/**
	 * @name default-hooks#processTransactions
	 * @type {function}
	 *
	 * @description
	 * Default hook for transactions list post processing
	 *
	 * @param {Array} transactions to process
	 * @returns {Array} transactions
	 */
	function processTransactions(transactions) {
	  return transactions;
	}
	
	/**
	 * @name defaultPaginationType
	 * @type {function}
	 * @description
	 * defaultPaginationType default hook for setting load-more or pagination as default
	 *
	 * @returns {?string}
	 */
	var defaultPaginationType = exports.defaultPaginationType = function defaultPaginationType() {
	  return null;
	};
	
	/**
	 * @name defaultSortableColumn
	 *
	 * @description
	 * defaultSortableColumn default hook
	 *
	 * @type {function}
	 * @returns {?string}
	 */
	var defaultSortableColumn = exports.defaultSortableColumn = function defaultSortableColumn() {
	  return null;
	};
	
	/**
	 * @name defaultSortableDirection
	 *
	 * @description
	 * defaultSortableDirection default hook
	 *
	 * @type {function}
	 * @returns {?string}
	 */
	var defaultSortableDirection = exports.defaultSortableDirection = function defaultSortableDirection() {
	  return null;
	};
	
	/**
	 * @name isDefaultProductFirst
	 *
	 * @description
	 * isDefaultProductFirst default hook
	 *
	 * @type {function}
	 * @returns {boolean}
	 */
	var isDefaultProductFirst = exports.isDefaultProductFirst = function isDefaultProductFirst() {
	  return true;
	};
	
	/**
	 * @name default-hooks#extendLoadMoreParams
	 * @type {function}
	 *
	 * @description
	 * Default hook for extending loadMore params
	 *
	 * @param {object} params to extend
	 * @param {?number} params.amountGreaterThan Amount greater than
	 * @param {?number} params.amountLessThan Amount less than
	 * @param {?string} params.bookingDateGreaterThan Booking date greater than
	 * @param {?string} params.bookingDateLessThan Booking date less than
	 * @param {?string} params.productId The product id
	 * @param {?string} params.type Type category
	 * @returns {object} params
	 */
	function extendLoadMoreParams(params) {
	  return params;
	}
	
	/**
	 * @name isTransactionsShown
	 *
	 * @description
	 * Default hook for checking if the transactions should be visible
	 *
	 * @type {function}
	 * @returns {boolean}
	 */
	var isTransactionsShown = exports.isTransactionsShown = function isTransactionsShown() {
	  return true;
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=widget-bb-transactions-ng.js.map