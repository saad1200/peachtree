# widget-bb-transactions-ng


Version: **1.8.10**

Transactions widget.

## Imports

* lib-bb-event-bus-ng
* lib-bb-intent-ng
* lib-bb-model-errors
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-transactions-ng
* vendor-bb-angular

---

## Example

```javascript
<div ng-controller="TransactionsController as $ctrl">
  <ul>
    <li ng-repeat="transaction in $ctrl.transactions">{{transaction.id}}</li>
  </ul>
</div>
```

## Table of Contents
- **TransactionsController**<br/>    <a href="#TransactionsController#search">#search(query, merge)</a><br/>    <a href="#TransactionsController#applySearchFilter">#applySearchFilter(params, merge)</a><br/>    <a href="#TransactionsController#cancelSearch">#cancelSearch()</a><br/>    <a href="#TransactionsController#searchMore">#searchMore()</a><br/>    <a href="#TransactionsController#loadTransactions">#loadTransactions(merge)</a><br/>    <a href="#TransactionsController#loadMore">#loadMore(done)</a><br/>
- **widget-bb-transactions-ng**<br/>    <a href="#widget-bb-transactions-ngerrors">errors</a><br/>    <a href="#widget-bb-transactions-ngpageSize">pageSize</a><br/>    <a href="#widget-bb-transactions-ngcategoryTransactionsIntent">categoryTransactionsIntent</a><br/>    <a href="#widget-bb-transactions-ngmaxNavPages">maxNavPages</a><br/>    <a href="#widget-bb-transactions-ngpaginationType">paginationType</a><br/>    <a href="#widget-bb-transactions-ngdismissTime">dismissTime</a><br/>    <a href="#widget-bb-transactions-ngloadAllTransactionsInitially">loadAllTransactionsInitially</a><br/>    <a href="#widget-bb-transactions-nginitLoad">initLoad</a><br/>    <a href="#widget-bb-transactions-ngtransactions">transactions</a><br/>    <a href="#widget-bb-transactions-ngtransactionsSearch">transactionsSearch</a><br/>    <a href="#widget-bb-transactions-ngtransaction">transaction</a><br/>    <a href="#widget-bb-transactions-ngloadMorePromise">loadMorePromise</a><br/>    <a href="#widget-bb-transactions-ngsearchMorePromise">searchMorePromise</a><br/>    <a href="#widget-bb-transactions-ngonSort">onSort(orderBy, direction, header)</a><br/>    <a href="#widget-bb-transactions-ngexportToFile">exportToFile(format)</a><br/>    <a href="#widget-bb-transactions-ngtranscations">transcations</a><br/>    <a href="#widget-bb-transactions-ngloading">loading</a><br/>    <a href="#widget-bb-transactions-ngloadingError">loadingError</a><br/>    <a href="#widget-bb-transactions-ngallTransactionsLoaded">allTransactionsLoaded</a><br/>    <a href="#widget-bb-transactions-ngsearching">searching</a><br/>    <a href="#widget-bb-transactions-ngsearchLoading">searchLoading</a><br/>    <a href="#widget-bb-transactions-ngsearchLoadingError">searchLoadingError</a><br/>    <a href="#widget-bb-transactions-ngsearchTransactions">searchTransactions</a><br/>    <a href="#widget-bb-transactions-ngsearchAllTransactionsLoaded">searchAllTransactionsLoaded</a><br/>    <a href="#widget-bb-transactions-ngshowAllTransactions">showAllTransactions</a><br/>
- **default-hooks**<br/>    <a href="#default-hooks#processProductSelected">#processProductSelected(productSelected)</a><br/>    <a href="#default-hooks#processRequestParams">#processRequestParams(params)</a><br/>    <a href="#default-hooks#processTransactions">#processTransactions(transactions)</a><br/>    <a href="#default-hooks#extendLoadMoreParams">#extendLoadMoreParams(params)</a><br/>
- **widget-bb-transactions-ng**<br/>    <a href="#widget-bb-transactions-ngdefaultPaginationType">defaultPaginationType()</a><br/>    <a href="#widget-bb-transactions-ngdefaultSortableColumn">defaultSortableColumn()</a><br/>    <a href="#widget-bb-transactions-ngdefaultSortableDirection">defaultSortableDirection()</a><br/>    <a href="#widget-bb-transactions-ngisDefaultProductFirst">isDefaultProductFirst()</a><br/>    <a href="#widget-bb-transactions-ngisTransactionsShown">isTransactionsShown()</a><br/>

---

## TransactionsController

Transactions list controller

| Injector Key |
| :-- |
| *TransactionsController* |


### <a name="TransactionsController#search"></a>*#search(query, merge)*

Searches by a given query

| Parameter | Type | Description |
| :-- | :-- | :-- |
| query | String |  |
| merge | Function |  |

##### Returns

Promise of Array - **

### <a name="TransactionsController#applySearchFilter"></a>*#applySearchFilter(params, merge)*

Searches by given filter parameters

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object |  |
| merge | Function |  |

##### Returns

Promise of Array - **

### <a name="TransactionsController#cancelSearch"></a>*#cancelSearch()*

Resets search

### <a name="TransactionsController#searchMore"></a>*#searchMore()*

Loads more search results and appends them to the search result.

##### Returns

Promise of Array - *searchMorePromise*

### <a name="TransactionsController#loadTransactions"></a>*#loadTransactions(merge)*

Handles loading transactions

| Parameter | Type | Description |
| :-- | :-- | :-- |
| merge | Function | Defines how new items should be merged with current items |

##### Returns

Promise of Array - **

### <a name="TransactionsController#loadMore"></a>*#loadMore(done)*

Loads more transactions and append them to the transaction's list

| Parameter | Type | Description |
| :-- | :-- | :-- |
| done | Function | Callback function for `ui-bb-load-more-ng` component |

##### Returns

Promise of Array - *loadMorePromise*

---
### <a name="widget-bb-transactions-ngerrors"></a>*errors*


**Type:** *<a href="#errors">errors</a>*


---
### <a name="widget-bb-transactions-ngpageSize"></a>*pageSize*

Number of records to return per request

The transactionListSize property is deprecated in favor of bb.transaction.pageSize

**Type:** *Number*


---
### <a name="widget-bb-transactions-ngcategoryTransactionsIntent"></a>*categoryTransactionsIntent*

Intent event

**Type:** *String*


---
### <a name="widget-bb-transactions-ngmaxNavPages"></a>*maxNavPages*

Maximum number of navigation pages to display

**Type:** *Number*


---
### <a name="widget-bb-transactions-ngpaginationType"></a>*paginationType*

Type of pagination (pagination, load-more)

**Type:** *String*


---
### <a name="widget-bb-transactions-ngdismissTime"></a>*dismissTime*

Number of seconds to dismiss the notification

**Type:** *Number*


---
### <a name="widget-bb-transactions-ngloadAllTransactionsInitially"></a>*loadAllTransactionsInitially*

Defines whether data should be shown if no account is selected

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-nginitLoad"></a>*initLoad*

Defines whether any data should be shown on widget init

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-ngtransactions"></a>*transactions*


**Type:** *<a href="#transactions">transactions</a>*


---
### <a name="widget-bb-transactions-ngtransactionsSearch"></a>*transactionsSearch*


**Type:** *<a href="#transactionsSearch">transactionsSearch</a>*


---
### <a name="widget-bb-transactions-ngtransaction"></a>*transaction*


**Type:** *<a href="#transaction">transaction</a>*


---

## product

Selected product info

---
### <a name="widget-bb-transactions-ngloadMorePromise"></a>*loadMorePromise*


**Type:** *Promise of <a href="#void">void</a>*


---
### <a name="widget-bb-transactions-ngsearchMorePromise"></a>*searchMorePromise*


**Type:** *Promise of <a href="#void">void</a>*


---

---

### <a name="widget-bb-transactions-ngonSort"></a>*onSort(orderBy, direction, header)*

Loads sorted list of payments

| Parameter | Type | Description |
| :-- | :-- | :-- |
| orderBy | String | Column key to sort |
| direction | String | Sort direction |
| header | Object | config object reference |

---

### <a name="widget-bb-transactions-ngexportToFile"></a>*exportToFile(format)*

Exports filtered list of payments to a file in specified format

| Parameter | Type | Description |
| :-- | :-- | :-- |
| format | String | Specified format (CSV, PDF) |

---
### <a name="widget-bb-transactions-ngtranscations"></a>*transcations*

The list of transactions

**Type:** *Array of Object*


---
### <a name="widget-bb-transactions-ngloading"></a>*loading*

Loading flag which is true while the list of transactions is loading

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-ngloadingError"></a>*loadingError*

Loading error

**Type:** *Error*


---
### <a name="widget-bb-transactions-ngallTransactionsLoaded"></a>*allTransactionsLoaded*

Flag that indicates whether all the transactions have been loaded

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-ngsearching"></a>*searching*

Searching flag which is true while user is searching transactions

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-ngsearchLoading"></a>*searchLoading*

Loading flag which is true while the list of transactions is loading

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-ngsearchLoadingError"></a>*searchLoadingError*

Search error

**Type:** *Error*


---
### <a name="widget-bb-transactions-ngsearchTransactions"></a>*searchTransactions*

Check is search filter applied

**Type:** *Boolean*


---
### <a name="widget-bb-transactions-ngsearchAllTransactionsLoaded"></a>*searchAllTransactionsLoaded*

Flag that indicates whether all the transactions have been loaded during a search

**Type:** *Boolean*


---

## transcation

Selected transaction info

---
### <a name="widget-bb-transactions-ngshowAllTransactions"></a>*showAllTransactions*

Show all available transactions

**Type:** *Boolean*


---

## default-hooks

Default hooks for widget-bb-transactions-ng

### <a name="default-hooks#processProductSelected"></a>*#processProductSelected(productSelected)*

Default hook for selected product processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| productSelected | Object | Product to process |

##### Returns

Object - **

### <a name="default-hooks#processRequestParams"></a>*#processRequestParams(params)*

Default hook for transactions query params processing/extending

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | to process |
| params.amountGreaterThan | Number (optional) | Amount greater than |
| params.amountLessThan | Number (optional) | Amount less than |
| params.bookingDateGreaterThan | String (optional) | Booking date greater than |
| params.bookingDateLessThan | String (optional) | Booking date less than |
| params.productId | String (optional) | The product id |
| params.type | String (optional) | Type category |
| params.orderBy | String (optional) | The key to order by |
| params.direction | String (optional) | The direction to order by |
| params.from | Number (optional) | The page to list from |
| params.size | Number (optional) | The number of results per page |
| params.query | String (optional) | The search term used to search for transactions |

##### Returns

Object - *params*

### <a name="default-hooks#processTransactions"></a>*#processTransactions(transactions)*

Default hook for transactions list post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| transactions | Array | to process |

##### Returns

Array - *transactions*

### <a name="default-hooks#extendLoadMoreParams"></a>*#extendLoadMoreParams(params)*

Default hook for extending loadMore params

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | to extend |
| params.amountGreaterThan | Number (optional) | Amount greater than |
| params.amountLessThan | Number (optional) | Amount less than |
| params.bookingDateGreaterThan | String (optional) | Booking date greater than |
| params.bookingDateLessThan | String (optional) | Booking date less than |
| params.productId | String (optional) | The product id |
| params.type | String (optional) | Type category |

##### Returns

Object - *params*

---

### <a name="widget-bb-transactions-ngdefaultPaginationType"></a>*defaultPaginationType()*

defaultPaginationType default hook for setting load-more or pagination as default

##### Returns

String (optional) - **

---

### <a name="widget-bb-transactions-ngdefaultSortableColumn"></a>*defaultSortableColumn()*

defaultSortableColumn default hook

##### Returns

String (optional) - **

---

### <a name="widget-bb-transactions-ngdefaultSortableDirection"></a>*defaultSortableDirection()*

defaultSortableDirection default hook

##### Returns

String (optional) - **

---

### <a name="widget-bb-transactions-ngisDefaultProductFirst"></a>*isDefaultProductFirst()*

isDefaultProductFirst default hook

##### Returns

Boolean - **

---

### <a name="widget-bb-transactions-ngisTransactionsShown"></a>*isTransactionsShown()*

Default hook for checking if the transactions should be visible

##### Returns

Boolean - **

## Templates

* *template.ng.html*

---
