# ext-bbm-contact-details-ng


Version: **1.0.111**

Mobile extension for a contact details view in the Contacts widget.

## Imports

* ui-bb-avatar-ng
* ui-bb-i18n-ng

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-contact-details-ng</value>
</property>
```

## Table of Contents
