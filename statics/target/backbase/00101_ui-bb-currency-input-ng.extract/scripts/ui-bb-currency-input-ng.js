(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("ui-bb-dropdown-select"), require("lib-bb-currency-ng"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-currency-input-ng", ["vendor-bb-angular", "ui-bb-dropdown-select", "lib-bb-currency-ng"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-currency-input-ng"] = factory(require("vendor-bb-angular"), require("ui-bb-dropdown-select"), require("lib-bb-currency-ng"));
	else
		root["ui-bb-currency-input-ng"] = factory(root["vendor-bb-angular"], root["ui-bb-dropdown-select"], root["lib-bb-currency-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_7__, __WEBPACK_EXTERNAL_MODULE_38__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(37);

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_7__;

/***/ }),
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ }),
/* 13 */,
/* 14 */,
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */,
/* 36 */,
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _uiBbDropdownSelect = __webpack_require__(7);
	
	var _uiBbDropdownSelect2 = _interopRequireDefault(_uiBbDropdownSelect);
	
	var _libBbCurrencyNg = __webpack_require__(38);
	
	var _libBbCurrencyNg2 = _interopRequireDefault(_libBbCurrencyNg);
	
	var _currencyInput = __webpack_require__(39);
	
	var _currencyInput2 = _interopRequireDefault(_currencyInput);
	
	var _currencyFormatting = __webpack_require__(40);
	
	var _currencyFormatting2 = _interopRequireDefault(_currencyFormatting);
	
	var _decimalsFormatting = __webpack_require__(42);
	
	var _decimalsFormatting2 = _interopRequireDefault(_decimalsFormatting);
	
	__webpack_require__(43);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var dependencyKeys = [_libBbCurrencyNg2.default, _uiBbDropdownSelect2.default];
	
	/**
	 * @name default
	 * @type {string}
	 * @description The angular module name
	 */
	/**
	 * @module ui-bb-currency-input-ng
	 * @description
	 * Currency input UI component
	 *
	 * @example
	 * // In an extension:
	 * // file: scripts/index.js
	 * import uiBbCurrencyInputNgKey from 'ui-bb-currency-input-ng';
	 *
	 * export const dependencyKeys = [
	 *   uiBbCurrencyInputNgKey,
	 * ];
	 *
	 * // file: templates/template.ng.html
	 * <ui-bb-currency-input-ng
	 *   data-max-length="6"
	 *   data-decimal-max-length="2"
	 *   data-placeholder="000,000"
	 *   data-ng-model="$ctrl.payment.amount"
	 *   data-currencies="$ctrl.currencies">
	 * </ui-bb-currency-input-ng>
	 */
	exports.default = _vendorBbAngular2.default.module('ui-bb-currency-input-ng', dependencyKeys).directive('currencyFormatting', ['$filter', '$locale', _currencyFormatting2.default]).directive('decimalsFormatting', _decimalsFormatting2.default).directive('uiBbCurrencyInputNg', ['currencyAttr', _currencyInput2.default]).factory('currencyAttr', [_libBbCurrencyNg.bbCurrencyRuleKey, function (getRule) {
	  return {
	    decimalDigits: function decimalDigits(currencyCode) {
	      return (getRule(currencyCode) || {
	        decimalDigits: 2
	      }).decimalDigits;
	    }
	  };
	}]).name;

/***/ }),
/* 38 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_38__;

/***/ }),
/* 39 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	/**
	 * @name uiBbCurrencyInputNg
	 * @type {object}
	 *
	 * @property {string} placeholder Text to display for input's placeholder
	 * @property {string} max-length Maximum number of digits allowed in whole part
	 * @property {string} decimal-max-length Maximum number of digits allowed in decimal part
	 * @property {array} currencies List of available currencies
	 * @property {object} ng-model Currency input model
	 * @property {object} messages Localized messages
	 */
	
	var template = '\n  <div class="currency">\n    <ui-bb-dropdown-select\n      ng-model="currency"\n      ng-disabled="currencies.length <= 1"\n      ng-change="onCurrencyChange({ currency: $item });"\n      selected-as="$option">\n      <ui-bb-dropdown-option\n        option="currency.name"\n        ng-repeat="currency in currencies"\n        class="list-group-item-text"\n        aria-label="{{:: messages[\'label.currency\'] }}"\n      >\n        <a href="#">{{:: $option }}</a>\n      </ui-bb-dropdown-option>\n    </ui-bb-dropdown-select>\n  </div>\n  <div class="amount">\n    <input\n      currency-formatting\n      class="form-control text-right"\n      maxlength="{{maxLength}}"\n      placeholder="{{placeholder}}"\n      ng-model="whole"\n      aria-label="{{:: messages[\'label.amount\'] }}"\n    />\n  </div>\n  <div class="decimals">\n    <input\n      decimals-formatting\n      placeholder="{{decimalPlaceholder}}"\n      maxlength="{{decimalMaxLength}}"\n      class="form-control"\n      ng-model="decimals"\n      aria-label="{{:: messages[\'label.decimals\'] }}"\n      ng-disabled="decimalMaxLength === 0"\n    />\n  </div>\n';
	
	exports.default = function (currencyAttr) {
	  return {
	    restrict: 'E',
	    link: function link(scope, element, attrs, ngModelCtrl) {
	      var setDecimalAttrs = function setDecimalAttrs(decimalLength) {
	        Object.assign(scope, {
	          decimalMaxLength: decimalLength,
	          decimalPlaceholder: Array(decimalLength + 1).join('0')
	        });
	      };
	      // set default decimal field attributes
	      setDecimalAttrs(2);
	
	      var decimalInput = element[0].querySelector('.decimals input');
	
	      var validate = function validate(amount) {
	        var isValid = amount && parseFloat(amount.value) > 0;
	        ngModelCtrl.$setValidity('invalidAmount', isValid);
	      };
	
	      var fixDecimals = function fixDecimals(decimals) {
	        if (decimals === '' || scope.decimalMaxLength === 0) {
	          return undefined;
	        }
	        if (decimals && decimals.length < scope.decimalMaxLength) {
	          return decimals + '0'.repeat(scope.decimalMaxLength - decimals.length);
	        }
	        if (decimals && decimals.length > scope.decimalMaxLength) {
	          return decimals.substr(0, scope.decimalMaxLength);
	        }
	        return decimals;
	      };
	
	      var parse = function parse(amount) {
	        var _ref = amount && amount.value ? amount.value.toString().split('.') : [],
	            _ref2 = _slicedToArray(_ref, 2),
	            whole = _ref2[0],
	            decimals = _ref2[1];
	
	        return {
	          whole: whole,
	          decimals: fixDecimals(decimals)
	        };
	      };
	
	      element.find('input').on('keypress', function (e) {
	        // if ',' or '.' was pressed move focus to decimal field
	        if (e.key === ',' || e.key === '.') {
	          decimalInput.focus();
	        }
	      });
	
	      var dropdownSelectCtrl = void 0;
	      ngModelCtrl.$formatters.push(function (amount) {
	        if (amount && amount.currency) {
	          Object.assign(scope, {
	            currency: amount.currency || scope.currencies[0]
	          });
	          setDecimalAttrs(currencyAttr.decimalDigits(scope.currency));
	        }
	
	        Object.assign(scope, parse(amount));
	        validate(amount);
	
	        dropdownSelectCtrl = dropdownSelectCtrl || element.find('ui-bb-dropdown-select').controller('uiBbDropdownSelect');
	
	        // when amount changes externally
	        // update dropdown model
	        dropdownSelectCtrl.select(amount.currency);
	
	        return amount;
	      });
	
	      ngModelCtrl.$parsers.push(function (amount) {
	        validate(amount);
	
	        return amount;
	      });
	
	      scope.$watch('currency + whole + decimals', function () {
	        setDecimalAttrs(currencyAttr.decimalDigits(scope.currency));
	        if (scope.decimals && scope.decimals.length > scope.decimalMaxLength) {
	          Object.assign(scope, { decimals: scope.decimals.substr(0, scope.decimalMaxLength) });
	        }
	
	        var value = null;
	        if (scope.whole || scope.decimals) {
	          var decLength = scope.decimals ? scope.decimals.length : 0;
	          var decimals = (scope.decimalMaxLength > 0 ? '.' : '') + (scope.decimals || '') + Array(scope.decimalMaxLength - decLength + 1).join('0');
	          value = '' + (scope.whole || '0') + decimals;
	        }
	
	        var amount = {
	          value: value,
	          currency: scope.currency
	        };
	
	        ngModelCtrl.$setViewValue(amount);
	      });
	    },
	    require: 'ngModel',
	    template: template,
	    scope: {
	      onCurrencyChange: '&',
	      placeholder: '@',
	      maxLength: '<',
	      currencies: '<',
	      /**
	       * @description
	       * List of messages to be shown by component
	       *
	       * @name uiBbCurrencyInputNg#messages
	       * @type {object} messages
	       */
	      messages: '<'
	    }
	  };
	};

/***/ }),
/* 40 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _formattingUtils = __webpack_require__(41);
	
	var _formattingUtils2 = _interopRequireDefault(_formattingUtils);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var currencyFormatting = function currencyFormatting($filter, $locale) {
	  return {
	    require: 'ngModel',
	    restrict: 'A',
	    link: function link(scope, element, attrs, ngModel) {
	      var inputMaxLength = scope.maxLength;
	      var invalidChars = /[a-zA-Z!?>:;|<@#%^&*)(+/\\={}[\]_]/g;
	
	      // manually trigger the $formatters pipeline
	      function triggerRender() {
	        ngModel.$setViewValue(ngModel.$formatters.reduce(function (value, fn) {
	          return fn(value);
	        }, ngModel.$modelValue));
	      }
	
	      var filter = $filter('number');
	
	      // extend input max length by adding number of special characters (like ',')
	      var updateMaxLength = function updateMaxLength(number) {
	        return Object.assign(scope, {
	          maxLength: inputMaxLength + _formattingUtils2.default.specialCharsCount(number, filter(number, 0))
	        });
	      };
	
	      scope.$watch(function () {
	        return scope.$eval(attrs.currencyFilter);
	      }, function (f) {
	        filter = f ? $filter(f) : $filter('number');
	        triggerRender();
	      });
	
	      ngModel.$formatters.push(function (value) {
	        updateMaxLength(value);
	        return filter(value, 0);
	      });
	
	      ngModel.$parsers.push(function (value) {
	        if (value === '') {
	          return value;
	        }
	
	        var numericChars = void 0;
	        // ignore non-numeric characters
	        if (ngModel.$modelValue >= 0) {
	          numericChars = value.replace(invalidChars, '');
	        } else {
	          numericChars = value.replace(invalidChars, 0);
	        }
	
	        var decimalPosition = numericChars.indexOf($locale.NUMBER_FORMATS.DECIMAL_SEP);
	        var groupSeparator = $locale.NUMBER_FORMATS.GROUP_SEP.replace(/[.]/g, '\\$&');
	
	        var number = _formattingUtils2.default.toFloat(numericChars.replace(new RegExp(groupSeparator, 'g'), '').substring(0, decimalPosition !== -1 ? decimalPosition : numericChars.length)).toFixed();
	
	        if (!isNaN(number)) {
	          // in case when value is pasted, it can take advantage of
	          // extended max length (special character) and exceed input limits
	          // make sure that this doesn't happen
	          number = _formattingUtils2.default.truncateNumber(_formattingUtils2.default.toFloat(number), inputMaxLength).toFixed();
	
	          var formatted = filter(number, 0);
	          updateMaxLength(number);
	
	          // did we add a comma or currency symbol?
	          var specialCharactersCountChange = [numericChars, formatted].map(function (string) {
	            return _formattingUtils2.default.occurrences(string, _formattingUtils2.default.uniqueChars(numericChars, formatted));
	          }).reduce(function (prev, cur) {
	            return cur - prev;
	          });
	
	          // compute the new selection range, correcting for
	          // formatting introduced by the number or currency $filter
	          var selectionRange = [element[0].selectionStart, element[0].selectionEnd].map(function (position) {
	            return position + specialCharactersCountChange;
	          });
	
	          // set the formatted value in the view
	          ngModel.$setViewValue(formatted);
	          ngModel.$render();
	
	          // set the cursor back to its expected position
	          // (since $render resets the cursor the the end)
	          element[0].setSelectionRange(selectionRange[0], selectionRange[1]);
	        } else {
	          ngModel.$setViewValue(number.replace(invalidChars, 0));
	          ngModel.$render();
	
	          // reset input max length to default value
	          Object.assign(scope, { maxLength: inputMaxLength });
	        }
	        return number;
	      });
	    }
	  };
	};
	
	exports.default = currencyFormatting;

/***/ }),
/* 41 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var utils = {
	  // (haystack: String, needles: Array<String>) => Number
	  // eg. ('foo', ['o']) => 2
	  occurrences: function occurrences(haystack, needles) {
	    return needles
	    // get counts
	    .map(function (needle) {
	      var matches = needle.replace(/\[/g, '\\[').replace(/]/g, '\\]');
	      return (haystack.match(new RegExp('[' + matches + ']', 'g')) || []).length;
	    })
	    // sum counts
	    .reduce(function (prev, cur) {
	      return prev + cur;
	    }, 0);
	  },
	
	  // (currencyString: String) => Number
	  // eg. "$123.00" => 123.00, "EUR123.00" => 123.00
	  toFloat: function toFloat(currencyString) {
	    return parseFloat(currencyString.replace(/[^0-9.]/g, ''), 10);
	  },
	
	  // (array: Array) => Array
	  // eg. [1,2,2] => [1,2]
	  uniq: function uniq(array) {
	    return array.reduce(function (prev, cur) {
	      return prev.indexOf(cur) > -1 ? prev : prev.concat(cur);
	    }, []);
	  },
	
	  // (a: String, b: String) => Array<String>
	  // eg. "1,323.00", "$123.00" => ["$", ","]
	  uniqueChars: function uniqueChars(a, b) {
	    return utils.uniq(b.split('').filter(function (char) {
	      return !~a.indexOf(char);
	    }).concat(a.split('').filter(function (char) {
	      return !~b.indexOf(char);
	    })));
	  },
	
	  // (number: Number, limit: Number) => Number
	  // eg. (1234, 6) => 1234, (1234, 3) => 123
	  truncateNumber: function truncateNumber(number, limit) {
	    var truncatedNumber = number;
	    while (Math.log10(truncatedNumber) >= limit) {
	      truncatedNumber = Math.floor(truncatedNumber / 10);
	    }
	    return truncatedNumber;
	  },
	
	  // (number: Number, formatted: String) => Number
	  // eg. (1234, "1,234") => 1, (123, "123") => 0
	  specialCharsCount: function specialCharsCount(number, formatted) {
	    if (!number || !formatted) {
	      return 0;
	    }
	    var specialCharacters = utils.uniqueChars(number, formatted);
	    return utils.occurrences(formatted, specialCharacters);
	  }
	};
	
	exports.default = utils;

/***/ }),
/* 42 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var decimalsFormatting = function decimalsFormatting() {
	  return {
	    require: 'ngModel',
	    link: function link(scope, element, attrs, ngModelCtrl) {
	      ngModelCtrl.$parsers.push(function (value) {
	        ngModelCtrl.$setViewValue(value.replace(/[^0-9]/g, ''));
	        ngModelCtrl.$render();
	
	        return value;
	      });
	    }
	  };
	};
	
	exports.default = decimalsFormatting;

/***/ }),
/* 43 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(44);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(12)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!./index.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!./index.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 44 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(11)();
	// imports
	
	
	// module
	exports.push([module.id, "/* Component should define only limited structural styles */\nui-bb-currency-input-ng {\n  display: block;\n}\nui-bb-currency-input-ng .amount,\nui-bb-currency-input-ng .currency,\nui-bb-currency-input-ng .decimals {\n  float: left;\n}\nui-bb-currency-input-ng .amount {\n  width: 52%;\n  margin-left: 1%;\n  margin-right: 1%;\n}\nui-bb-currency-input-ng .currency {\n  width: 26%;\n  margin-right: 1%;\n}\nui-bb-currency-input-ng .decimals {\n  width: 18%;\n  margin-left: 1%;\n}\n", ""]);
	
	// exports


/***/ })
/******/ ])
});
;
//# sourceMappingURL=ui-bb-currency-input-ng.js.map