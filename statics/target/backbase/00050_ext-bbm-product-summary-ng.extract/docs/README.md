# ext-bbm-product-summary-ng


Version: **1.0.73**

Mobile extension for the product summary widget.

## Imports

* ui-bb-i18n-ng
* ui-bb-inline-status-ng
* ui-bbm-product-kind-table-view-ng

---

## Example

```javascript
<!-- product summary widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bbm-product-summary-ng</value>
</property>
```

## Table of Contents
- **ext-bbm-product-summary-ng**<br/>    <a href="#ext-bbm-product-summary-ngprocessKinds">processKinds(kinds)</a><br/>    <a href="#ext-bbm-product-summary-ngproductNameAsc">productNameAsc(productA, productB)</a><br/>
- **Type Definitions**<br/>    <a href="#ProductKindView">ProductKindView</a><br/>    <a href="#ProductView">ProductView</a><br/>

---

## Hooks

Hooks for widget-bb-product-summary-ng

---

### <a name="ext-bbm-product-summary-ngprocessKinds"></a>*processKinds(kinds)*

Hook for processing product kinds after initialization.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| kinds | Array of <a href="#ProductKind">ProductKind</a> | ProductKinds to process |

##### Returns

Array of <a href="#ProductKindView">ProductKindView</a> - **

---

### <a name="ext-bbm-product-summary-ngproductNameAsc"></a>*productNameAsc(productA, productB)*

Sort products alphabetically by name, ascending

| Parameter | Type | Description |
| :-- | :-- | :-- |
| productA | Object |  |
| productB | Object |  |

##### Returns

<a href="#('-1'">('-1'</a> or <a href="#'0'">'0'</a> or <a href="#'1')">'1')</a> - *result*

## Type Definitions


### <a name="ProductKindView"></a>*ProductKindView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The Product Kind identifier |
| name | String | The name of the Kind, suitable for display to users |
| products | Array of <a href="#ProductView">ProductView</a> | The products of this Kind |

### <a name="ProductView"></a>*ProductView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal Product Identifier |
| name | String | The product's name, suitable for display to users |
| identifier | String (optional) | The identifier of the Product from the user's perspective |
| primaryValue | String (optional) | The most important associated value to be displayed |
| secondaryValue | String (optional) | A secondary associated value to be displayed |
| secondaryLabel | String (optional) | A label to describe the secondary value |
| tertiaryValue | String (optional) | A tertiary associated value to be displayed |
| tertiaryLabel | String (optional) | A label to describe the tertiary value |
| currency | String (optional) | ISO currency code |

---
