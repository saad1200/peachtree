# model-bb-login-ng


Version: **1.0.47**

Model for widget-bb-login-ng and widget-bb-user-menu-ng

## Imports

* data-bb-cxp-authentication-http-ng
* vendor-bb-angular

---

## Example

```javascript
import modelLoginModuleKey, { modelLoginKey } from 'model-bb-login-ng';

angular
  .module('ExampleModule', [
    modelLoginModuleKey,
  ])
  .factory('someFactory', [
    modelLoginKey,
    // into
    function someFactory(loginModel) {
      // ...
    },
  ]);
```

## Table of Contents
- **loginModel**<br/>    <a href="#loginModel#login">#login(username, password)</a><br/>    <a href="#loginModel#logout">#logout()</a><br/>

---

## loginModel

Model for widget-bb-login-ng and widget-bb-user-menu-ng

### <a name="loginModel#login"></a>*#login(username, password)*

Makes a login request

| Parameter | Type | Description |
| :-- | :-- | :-- |
| username | String |  |
| password | String |  |

##### Returns

Promise - **

### <a name="loginModel#logout"></a>*#logout()*

Makes a logout request

##### Returns

Promise - **
