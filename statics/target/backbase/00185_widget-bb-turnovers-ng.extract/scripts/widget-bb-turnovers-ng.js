(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("lib-bb-event-bus-ng"), require("model-bb-turnovers-ng"), require("lib-bb-extension-helpers-ng"), require("lib-bb-currency-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-turnovers-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "lib-bb-model-errors", "lib-bb-widget-extension-ng", "lib-bb-event-bus-ng", "model-bb-turnovers-ng", "lib-bb-extension-helpers-ng", "lib-bb-currency-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-turnovers-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("lib-bb-event-bus-ng"), require("model-bb-turnovers-ng"), require("lib-bb-extension-helpers-ng"), require("lib-bb-currency-ng"));
	else
		root["widget-bb-turnovers-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"], root["lib-bb-widget-extension-ng"], root["lib-bb-event-bus-ng"], root["model-bb-turnovers-ng"], root["lib-bb-extension-helpers-ng"], root["lib-bb-currency-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_19__, __WEBPACK_EXTERNAL_MODULE_21__, __WEBPACK_EXTERNAL_MODULE_24__, __WEBPACK_EXTERNAL_MODULE_25__, __WEBPACK_EXTERNAL_MODULE_26__, __WEBPACK_EXTERNAL_MODULE_27__, __WEBPACK_EXTERNAL_MODULE_28__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(23);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_19__;

/***/ }),
/* 20 */,
/* 21 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_21__;

/***/ }),
/* 22 */,
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(15);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetExtensionNg = __webpack_require__(24);
	
	var _libBbWidgetExtensionNg2 = _interopRequireDefault(_libBbWidgetExtensionNg);
	
	var _libBbWidgetNg = __webpack_require__(19);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _libBbEventBusNg = __webpack_require__(25);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _modelBbTurnoversNg = __webpack_require__(26);
	
	var _modelBbTurnoversNg2 = _interopRequireDefault(_modelBbTurnoversNg);
	
	var _libBbExtensionHelpersNg = __webpack_require__(27);
	
	var _libBbExtensionHelpersNg2 = _interopRequireDefault(_libBbExtensionHelpersNg);
	
	var _libBbCurrencyNg = __webpack_require__(28);
	
	var _libBbCurrencyNg2 = _interopRequireDefault(_libBbCurrencyNg);
	
	var _defaultHooks = __webpack_require__(29);
	
	var _defaultHooks2 = _interopRequireDefault(_defaultHooks);
	
	var _controller = __webpack_require__(31);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var moduleKey = 'widget-bb-turnovers-ng'; /**
	                                           * @module widget-bb-turnovers-ng
	                                           *
	                                           * @description
	                                           * Turnovers
	                                           */
	
	var hooksKey = moduleKey + ':hooks';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_libBbWidgetNg2.default, _libBbEventBusNg2.default, _modelBbTurnoversNg2.default, _libBbExtensionHelpersNg2.default, _libBbCurrencyNg2.default]).factory(hooksKey, (0, _libBbWidgetExtensionNg2.default)(_defaultHooks2.default)).factory(_libBbExtensionHelpersNg.extensionHelpersContextKey, ['$compile', _libBbCurrencyNg.bbCurrencyRuleKey, function ($compile, getRule) {
	  return {
	    $compile: $compile,
	    getRule: getRule
	  };
	}]).controller('TurnoversController', [
	// dependencies to inject
	_libBbEventBusNg.eventBusKey, hooksKey, _modelBbTurnoversNg.modelTurnoversKey, _libBbWidgetNg.widgetKey,
	/* into */
	_controller2.default]).run([_libBbEventBusNg.eventBusKey, _libBbWidgetNg.widgetKey, function (bus, widget) {
	  bus.publish('cxp.item.loaded', {
	    id: widget.getId()
	  });
	}]).name;
	
	/**
	 * Turnover response object
	 * @typedef {object} Turnover
	 * @property {string} arrangementId Id of the arrangement this turnover belongs to
	 * @property {string} intervalDuration Duration of intervals returned
	 * @property {TurnoverItem[]} turnovers Array of turnover items
	 */
	
	/**
	 * Turnover response item
	 * @typedef {object} TurnoverItem
	 * @property {string} intervalStartDate Date in ISO format (2016-06-01T16:41:41.090Z)
	 * @property {object} debitAmount Debit amount object
	 * @property {string} debitAmount.currencyCode Debit amount currency code (ISO)
	 * @property {number} debitAmount.amount Debit amount value
	 * @property {object} creditAmount Credit amount object
	 * @property {string} creditAmount.currencyCode Credit amount currency code (ISO)
	 * @property {number} creditAmount.amount Credit amount value
	 * @property {object} balance Debit and credit difference object
	 * @property {string} balance.currencyCode Debit and credit difference currency code (ISO)
	 * @property {number} balance.amount Debit and credit difference value
	 */
	
	/**
	 * Amount object
	 * @typedef {object} Amount
	 * @property {string} currency Currency code
	 * @property {number} value
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKinds
	 * @property {Amount} aggregatedBalance Total balance of products
	 * @property {ProductKind[]} productKinds Array of Products Kinds
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKind
	 * @property {Amount} aggregatedBalance Total balance of product kind
	 * @property {!string} name Name of the product kind
	 * @property {Product[]} products Array of associated products
	 */
	
	/**
	 * Product type definition
	 * @typedef {object} Product
	 * @property {!string} id id of the Product
	 * @property {!string} name Name of the Product
	 * @property {!string} kind id of the ProductKind
	 * @property {string} alias Alias of the Product
	 * @property {string} IBAN International Bank Account Number
	 * @property {string} BBAN Basic Bank Account Number
	 * @property {string} currency Currency code
	 * @property {string} PANSuffix Primary Account Number Suffix
	 * @property {string} bookedBalance Booked balance
	 * @property {string} availableBalance Available balance
	 * @property {string} creditLimit Credit limit
	 * @property {string} currentInvestmentValue Current investment value
	 * @property {string} principalAmount Principal amount
	 * @property {string} accruedInterest Accrued interest
	 */
	
	/**
	 * BBSeries data object used to draw charts
	 * @typedef {object} BBSeries
	 * @property {string[]} labels Array of x axis labels
	 * @property {Dataset[]} datasets Array of all y axis value datasets
	 */
	
	/**
	 * Dataset object for y axis data
	 * @typedef {object} Dataset
	 * @property {number[]} data Array of data points to be drawn for each label
	 */

/***/ }),
/* 24 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_24__;

/***/ }),
/* 25 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_25__;

/***/ }),
/* 26 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_26__;

/***/ }),
/* 27 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_27__;

/***/ }),
/* 28 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_28__;

/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _constants = __webpack_require__(30);
	
	/**
	 * @name default-hooks#processTurnoverResponse
	 * @type {function}
	 *
	 * @description
	 * Default hook for turnovers response object post processing
	 *
	 * @param {Turnover} data turnover object to process
	 * @returns {Turnover} turnover response object
	 */
	var processTurnoverResponse = function processTurnoverResponse(data) {
	  return data || {};
	};
	
	/**
	 * @name default-hooks#processTurnoverSeries
	 * @type {function}
	 *
	 * @description
	 * Default hook for turnovers chart series object post processing
	 *
	 * @param {BBSeries} series chart series data
	 * @param {Turnover} data original turnover object
	 * @returns {object} processed series
	 */
	/* eslint no-unused-vars: ["error", { "args": "none" }] */
	/**
	 * @name default-hooks
	 * @type {object}
	 *
	 * @description
	 * Default hooks for widget-bb-turnovers-ng
	 */
	
	var processTurnoverSeries = function processTurnoverSeries(series, data) {
	  return series || {};
	};
	
	/**
	 * @name default-hooks#processSelectedProduct
	 * @type {function}
	 *
	 * @description
	 * Default hook to process product then it's selected
	 * @param {Product} product which is selected
	 * @returns {Product} product after processing
	 */
	var processSelectedProduct = function processSelectedProduct(selectedProduct) {
	  return selectedProduct;
	};
	
	/**
	 * @name default-hooks#processProductsList
	 * @type {function}
	 *
	 * @description
	 * Process passed products list before passing it to the view.
	 *
	 * @param {ProductKinds} products to process
	 * @returns {ProductKinds} processed products
	 */
	var processProductsList = function processProductsList(products) {
	  return products;
	};
	
	/**
	 * @name default-hooks#onTurnoversUpdate
	 * @type {function}
	 *
	 * @description
	 * Process parameters before they are sent to the model's load method
	 *
	 * @param {object} params to process
	 * @returns {object} processed params
	 */
	var onTurnoversUpdate = function onTurnoversUpdate(params) {
	  return params;
	};
	
	/**
	 * @name default-hooks#defaultPeriodStart
	 * @type {function}
	 *
	 * @description
	 * Sets period start property on init
	 *
	 * @returns {string} Start period string in format yyyy-mm-dd
	 */
	var defaultPeriodStart = function defaultPeriodStart() {
	  var date = new Date();
	  date.setMonth(date.getMonth() - _constants.DEFAULT_DURATION);
	  return date.toISOString().slice(0, 10);
	};
	
	/**
	 * @name default-hooks#defaultPeriodEnd
	 * @type {function}
	 *
	 * @description
	 * Sets period end property on init
	 *
	 * @returns {string} End period string in format yyyy-mm-dd
	 */
	var defaultPeriodEnd = function defaultPeriodEnd() {
	  return new Date().toISOString().slice(0, 10);
	};
	
	/**
	 * @name default-hooks#defaultInterval
	 * @type {function}
	 *
	 * @description
	 * Sets interval property on init
	 *
	 * @param {Interval} interval Available intervals
	 * @returns {string} One of the available intervals
	 */
	var defaultInterval = function defaultInterval(interval) {
	  return _constants.DEFAULT_INTERVAL;
	};
	
	/**
	 * @name default-hooks#defaultStartDay
	 * @type {function}
	 *
	 * @description
	 * Sets monthly interval start day on init
	 *
	 * @returns {string} One of the available intervals
	 */
	var defaultStartDay = function defaultStartDay() {
	  return _constants.DEFAULT_START_DAY;
	};
	
	/**
	 * @name default-hooks#processLoadError
	 * @type {function}
	 *
	 * @description
	 * Sets the error for missing parameters in the turnovers request
	 *
	 * @param {error} The error passed
	 * @returns {error} The actual error
	 */
	var processLoadError = function processLoadError(error) {
	  return error;
	};
	
	exports.default = {
	  processTurnoverResponse: processTurnoverResponse,
	  processTurnoverSeries: processTurnoverSeries,
	  processProductsList: processProductsList,
	  processSelectedProduct: processSelectedProduct,
	  onTurnoversUpdate: onTurnoversUpdate,
	  defaultPeriodStart: defaultPeriodStart,
	  defaultPeriodEnd: defaultPeriodEnd,
	  defaultInterval: defaultInterval,
	  defaultStartDay: defaultStartDay,
	  processLoadError: processLoadError
	};

/***/ }),
/* 30 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Available intervals
	 * @name INTERVAL
	 * @type {Interval}
	 */
	var INTERVAL = exports.INTERVAL = {
	  DAY: 'DAY',
	  WEEK: 'WEEK',
	  MONTH: 'MONTH',
	  YEAR: 'YEAR'
	};
	
	/**
	 * Default load interval
	 * @name DEFAULT_INTERVAL
	 * @type {string}
	 */
	var DEFAULT_INTERVAL = exports.DEFAULT_INTERVAL = INTERVAL.MONTH;
	
	/**
	 * Default load duration
	 * @name DEFAULT_DURATION
	 * @type {number}
	 */
	var DEFAULT_DURATION = exports.DEFAULT_DURATION = 6;
	
	/**
	 * Default start day for monthly interval
	 * @name DEFAULT_START_DAY
	 * @type {number}
	 */
	var DEFAULT_START_DAY = exports.DEFAULT_START_DAY = 1;
	
	/**
	 * Interval object
	 * @typedef {object} Interval
	 * @property {string} DAY Daily interval
	 * @property {string} WEEK Weekly interval
	 * @property {string} MONTH Monthly interval
	 * @property {string} YEAR Yearly interval
	 */

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = TurnoversController;
	
	var _libBbModelErrors = __webpack_require__(21);
	
	var _message = __webpack_require__(32);
	
	var _message2 = _interopRequireDefault(_message);
	
	var _constants = __webpack_require__(30);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /**
	                                                                                                                                                                                                                   * @module widget-bb-turnovers-ng
	                                                                                                                                                                                                                   * @name TurnoversController
	                                                                                                                                                                                                                   *
	                                                                                                                                                                                                                   * @description
	                                                                                                                                                                                                                   * Turnovers
	                                                                                                                                                                                                                   */
	
	var PRODUCT_SELECTED = _message2.default.PRODUCT_SELECTED,
	    PERIOD_START_CHANGED = _message2.default.PERIOD_START_CHANGED,
	    PERIOD_END_CHANGED = _message2.default.PERIOD_END_CHANGED,
	    TURNOVERS_LOAD_FAILED = _message2.default.TURNOVERS_LOAD_FAILED;
	function TurnoversController(bus, hooks, model) {
	  var $ctrl = this;
	
	  /**
	   * Converts error code to error message translation key
	   *
	   * @inner
	   * @name errorMessage
	   * @type {function}
	   * @param {string} code Error code
	   * @returns {string} Error message translation key
	   */
	  var errorMessage = function errorMessage(code) {
	    var _E_AUTH$E_CONNECTIVIT;
	
	    return (_E_AUTH$E_CONNECTIVIT = {}, _defineProperty(_E_AUTH$E_CONNECTIVIT, _libBbModelErrors.E_AUTH, 'error.load.auth'), _defineProperty(_E_AUTH$E_CONNECTIVIT, _libBbModelErrors.E_CONNECTIVITY, 'error.load.connectivity'), _defineProperty(_E_AUTH$E_CONNECTIVIT, _libBbModelErrors.E_USER, 'error.load.user'), _defineProperty(_E_AUTH$E_CONNECTIVIT, model.E_PARAMS, model.E_PARAMS), _E_AUTH$E_CONNECTIVIT)[code] || 'error.load.unexpected';
	  };
	
	  /**
	   * Updates turnovers list based on selected product
	   *
	   * @inner
	   * @name updateTurnovers
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateTurnovers = function updateTurnovers() {
	    return model.validateTurnoversParameters(hooks.onTurnoversUpdate({
	      arrangementId: $ctrl.selectedProduct ? $ctrl.selectedProduct.id : null,
	      periodStartDate: $ctrl.periodStartDate,
	      periodEndDate: $ctrl.periodEndDate,
	      intervalDuration: $ctrl.intervalDuration,
	      intervalStartDay: $ctrl.intervalStartDay
	    })).then(model.loadTurnovers).then(function (loaded) {
	      $ctrl.error = null;
	      $ctrl.data = hooks.processTurnoverResponse(loaded);
	      $ctrl.series = hooks.processTurnoverSeries(model.transformToSeries(loaded), loaded);
	    }).catch(function (error) {
	      $ctrl.error = hooks.processLoadError(errorMessage(error.code));
	      bus.publish(TURNOVERS_LOAD_FAILED, { error: error });
	    });
	  };
	
	  /**
	   * Updates the products list for the ui-bb-account-selector.
	   *
	   * @inner
	   * @name updateProductsList
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateProductsList = function updateProductsList(getFromStorage) {
	    return model.getProductsArray(getFromStorage).then(function (products) {
	      $ctrl.error = null;
	      $ctrl.products = hooks.processProductsList(products);
	      return $ctrl.products;
	    }).catch(function (error) {
	      $ctrl.error = errorMessage(error.code);
	      bus.publish(TURNOVERS_LOAD_FAILED, { error: error });
	    });
	  };
	
	  /**
	   * Updates selected product
	   *
	   * @inner
	   * @name updateProductSelected
	   * @type {function}
	   * @param {Product} selectedProduct - selected product to be used
	   */
	  var updateProductSelected = function updateProductSelected(selectedProduct) {
	    $ctrl.selectedProduct = hooks.processSelectedProduct(selectedProduct);
	    model.setSelectedProduct($ctrl.selectedProduct);
	  };
	
	  /**
	   * Initializes period data via hooks
	   *
	   * @inner
	   * @name initPeriodData
	   * @type {function}
	   */
	  var initPeriodData = function initPeriodData() {
	    $ctrl.periodStartDate = hooks.defaultPeriodStart();
	    $ctrl.periodEndDate = hooks.defaultPeriodEnd();
	    $ctrl.intervalDuration = hooks.defaultInterval(_constants.INTERVAL);
	    $ctrl.intervalStartDay = hooks.defaultStartDay();
	  };
	
	  /**
	   * Adds subscriptions to bus events
	   * @inner
	   * @name bindEvents
	   * @type {function}
	   */
	  function bindEvents() {
	    bus.subscribe(PRODUCT_SELECTED, function (selectedProduct) {
	      updateProductSelected(selectedProduct.product);
	      updateTurnovers();
	    });
	    bus.subscribe(PERIOD_START_CHANGED, updateTurnovers);
	    bus.subscribe(PERIOD_END_CHANGED, updateTurnovers);
	  }
	
	  /**
	   * AngularJS Lifecycle hook used to initialize the controller
	   *
	   * @name $onInit
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var $onInit = function $onInit() {
	    $ctrl.isLoading = true;
	    return updateProductsList().then(function () {
	      return model.getSelectedProduct();
	    }).then(updateProductSelected).then(initPeriodData).then(updateTurnovers).then(bindEvents).then(function () {
	      $ctrl.isLoading = false;
	    });
	  };
	
	  /**
	   * @description
	   * Handler to be called on period start date change
	   *
	   * @name onPeriodStartDateChanged
	   * @type {function}
	   * @returns {void}
	   */
	  var onPeriodStartDateChanged = function onPeriodStartDateChanged() {
	    bus.publish(PERIOD_START_CHANGED, $ctrl.periodStartDate);
	  };
	
	  /**
	   * @description
	   * Handler to be called on period end date change
	   *
	   * @name onPeriodEndDateChanged
	   * @type {function}
	   * @returns {void}
	   */
	  var onPeriodEndDateChanged = function onPeriodEndDateChanged() {
	    bus.publish(PERIOD_END_CHANGED, $ctrl.periodEndDate);
	  };
	
	  /**
	   * @description
	   * Handler to be used on product selection, is using
	   * selected product value from {@link Hooks.processSelectedProduct} hook
	   *
	   * @name onProductSelected
	   * @type {function}
	   * @returns {void}
	   */
	  var onProductSelected = function onProductSelected() {
	    bus.publish(PRODUCT_SELECTED, $ctrl.selectedProduct);
	  };
	
	  Object.assign($ctrl, {
	    $onInit: $onInit,
	
	    /**
	     * @description
	     * The value returned from {@link Hooks.processTurnoverResponse} hook.
	     * null if the data isn't loaded.
	     *
	     * @name data
	     * @type {Turnover}
	     */
	    data: null,
	
	    /**
	     * @description
	     * The value returned from {@link Hooks.processTurnoverSeries} hook.
	     * Formatted for use within chart UI component.
	     * null if the data isn't loaded
	     *
	     * @name series
	     * @type {BBSeries}
	     */
	    series: null,
	
	    /**
	     * @description
	     * The selected product for turnovers evaluation.
	     *
	     * @name selectedProduct
	     * @type {Product}
	     */
	    selectedProduct: null,
	
	    /**
	     * @description
	     * List of products to be used by account selector for turnovers evaluation.
	     * Is recieved from {@link Hooks.processProductsList}
	     *
	     * @name products
	     * @type {array}
	     */
	    products: [],
	    onProductSelected: onProductSelected,
	    onPeriodStartDateChanged: onPeriodStartDateChanged,
	    onPeriodEndDateChanged: onPeriodEndDateChanged,
	
	    /**
	     * @description
	     * Date of the turnovers evaluation period start
	     *
	     * @name periodStartDate
	     * @type {string}
	     */
	    periodStartDate: null,
	
	    /**
	     * @description
	     * Date of the turnovers evaluation period end
	     *
	     * @name periodEndDate
	     * @type {string}
	     */
	    periodEndDate: null,
	
	    /**
	     * @description
	     * Length of each periodic interval
	     *
	     * @name intervalDuration
	     * @type {string}
	     */
	    intervalDuration: null,
	
	    /**
	     * @description
	     * Day of a month to start turnover interval
	     *
	     * @name intervalStartDay
	     * @type {number}
	     */
	    intervalStartDay: null,
	
	    /**
	     * @description
	     * Loading status
	     *
	     * @name isLoading
	     * @type {boolean}
	     */
	    isLoading: false,
	
	    /**
	     * @description
	     * Object containing all available intervals as key:value pairs
	     *
	     * @name interval
	     * @type {Interval}
	     */
	    interval: _constants.INTERVAL,
	
	    /**
	     * @description
	     * The error encountered when attempting to fetch from the model
	     *
	     * @name error
	     * @type {ModelError}
	     */
	    error: null
	  });
	}

/***/ }),
/* 32 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  /**
	   * Triggered when product is selected.
	   * @event bb.event.product.selected
	   * @type {Product}
	   */
	  PRODUCT_SELECTED: 'bb.event.product.selected',
	  /**
	   * Triggered when period start date is changed.
	   * @event bb.event.turnovers.period.start.date.changed
	   * @type {any}
	   */
	  PERIOD_START_CHANGED: 'bb.event.turnovers.period.start.date.changed',
	  /**
	   * Triggered when period end date is changed.
	   * @event bb.event.turnovers.period.end.date.changed
	   * @type {any}
	   */
	  PERIOD_END_CHANGED: 'bb.event.turnovers.period.end.date.changed',
	  /**
	   * Triggered when turnovers widget fails to load.
	   * @event widget-bb-turnovers-ng.load.failed
	   * @type {any}
	   */
	  TURNOVERS_LOAD_FAILED: 'widget-bb-turnovers-ng.load.failed'
	};

/***/ })
/******/ ])
});
;
//# sourceMappingURL=widget-bb-turnovers-ng.js.map