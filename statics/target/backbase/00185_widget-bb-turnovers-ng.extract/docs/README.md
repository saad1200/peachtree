# widget-bb-turnovers-ng


Version: **1.0.15**

Turnovers

## Imports

* lib-bb-currency-ng
* lib-bb-event-bus-ng
* lib-bb-extension-helpers-ng
* lib-bb-model-errors
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-turnovers-ng
* vendor-bb-angular

---

## Table of Contents
- **widget-bb-turnovers-ng**<br/>    <a href="#widget-bb-turnovers-ngINTERVAL">INTERVAL</a><br/>    <a href="#widget-bb-turnovers-ngDEFAULT_INTERVAL">DEFAULT_INTERVAL</a><br/>    <a href="#widget-bb-turnovers-ngDEFAULT_DURATION">DEFAULT_DURATION</a><br/>    <a href="#widget-bb-turnovers-ngDEFAULT_START_DAY">DEFAULT_START_DAY</a><br/>    <a href="#widget-bb-turnovers-ng$onInit">$onInit()</a><br/>    <a href="#widget-bb-turnovers-ngonPeriodStartDateChanged">onPeriodStartDateChanged()</a><br/>    <a href="#widget-bb-turnovers-ngonPeriodEndDateChanged">onPeriodEndDateChanged()</a><br/>    <a href="#widget-bb-turnovers-ngonProductSelected">onProductSelected()</a><br/>    <a href="#widget-bb-turnovers-ngdata">data</a><br/>    <a href="#widget-bb-turnovers-ngseries">series</a><br/>    <a href="#widget-bb-turnovers-ngselectedProduct">selectedProduct</a><br/>    <a href="#widget-bb-turnovers-ngproducts">products</a><br/>    <a href="#widget-bb-turnovers-ngperiodStartDate">periodStartDate</a><br/>    <a href="#widget-bb-turnovers-ngperiodEndDate">periodEndDate</a><br/>    <a href="#widget-bb-turnovers-ngintervalDuration">intervalDuration</a><br/>    <a href="#widget-bb-turnovers-ngintervalStartDay">intervalStartDay</a><br/>    <a href="#widget-bb-turnovers-ngisLoading">isLoading</a><br/>    <a href="#widget-bb-turnovers-nginterval">interval</a><br/>    <a href="#widget-bb-turnovers-ngerror">error</a><br/>
- **default-hooks**<br/>    <a href="#default-hooks#processTurnoverResponse">#processTurnoverResponse(data)</a><br/>    <a href="#default-hooks#processTurnoverSeries">#processTurnoverSeries(series, data)</a><br/>    <a href="#default-hooks#processSelectedProduct">#processSelectedProduct(product)</a><br/>    <a href="#default-hooks#processProductsList">#processProductsList(products)</a><br/>    <a href="#default-hooks#onTurnoversUpdate">#onTurnoversUpdate(params)</a><br/>    <a href="#default-hooks#defaultPeriodStart">#defaultPeriodStart()</a><br/>    <a href="#default-hooks#defaultPeriodEnd">#defaultPeriodEnd()</a><br/>    <a href="#default-hooks#defaultInterval">#defaultInterval(interval)</a><br/>    <a href="#default-hooks#defaultStartDay">#defaultStartDay()</a><br/>    <a href="#default-hooks#processLoadError">#processLoadError(The)</a><br/>
- **Events**<br/>    <a href="#bb.event.product.selected">bb.event.product.selected</a><br/>    <a href="#bb.event.turnovers.period.start.date.changed">bb.event.turnovers.period.start.date.changed</a><br/>    <a href="#bb.event.turnovers.period.end.date.changed">bb.event.turnovers.period.end.date.changed</a><br/>    <a href="#widget-bb-turnovers-ng.load.failed">widget-bb-turnovers-ng.load.failed</a><br/>
- **Type Definitions**<br/>    <a href="#Interval">Interval</a><br/>    <a href="#Turnover">Turnover</a><br/>    <a href="#TurnoverItem">TurnoverItem</a><br/>    <a href="#Amount">Amount</a><br/>    <a href="#ProductKinds">ProductKinds</a><br/>    <a href="#ProductKind">ProductKind</a><br/>    <a href="#Product">Product</a><br/>    <a href="#BBSeries">BBSeries</a><br/>    <a href="#Dataset">Dataset</a><br/>

---
### <a name="widget-bb-turnovers-ngINTERVAL"></a>*INTERVAL*

Available intervals

**Type:** *<a href="#Interval">Interval</a>*


---
### <a name="widget-bb-turnovers-ngDEFAULT_INTERVAL"></a>*DEFAULT_INTERVAL*

Default load interval

**Type:** *String*


---
### <a name="widget-bb-turnovers-ngDEFAULT_DURATION"></a>*DEFAULT_DURATION*

Default load duration

**Type:** *Number*


---
### <a name="widget-bb-turnovers-ngDEFAULT_START_DAY"></a>*DEFAULT_START_DAY*

Default start day for monthly interval

**Type:** *Number*


---

### <a name="widget-bb-turnovers-ng$onInit"></a>*$onInit()*

AngularJS Lifecycle hook used to initialize the controller


##### Returns

Promise of <a href="#void">void</a> - **

---

### <a name="widget-bb-turnovers-ngonPeriodStartDateChanged"></a>*onPeriodStartDateChanged()*

Handler to be called on period start date change

##### Returns

<a href="#void">void</a> - **

---

### <a name="widget-bb-turnovers-ngonPeriodEndDateChanged"></a>*onPeriodEndDateChanged()*

Handler to be called on period end date change

##### Returns

<a href="#void">void</a> - **

---

### <a name="widget-bb-turnovers-ngonProductSelected"></a>*onProductSelected()*

Handler to be used on product selection, is using
selected product value from <a href="#Hooks.processSelectedProduct">Hooks.processSelectedProduct</a> hook

##### Returns

<a href="#void">void</a> - **

---
### <a name="widget-bb-turnovers-ngdata"></a>*data*

The value returned from <a href="#Hooks.processTurnoverResponse">Hooks.processTurnoverResponse</a> hook.
null if the data isn't loaded.

**Type:** *<a href="#Turnover">Turnover</a>*


---
### <a name="widget-bb-turnovers-ngseries"></a>*series*

The value returned from <a href="#Hooks.processTurnoverSeries">Hooks.processTurnoverSeries</a> hook.
Formatted for use within chart UI component.
null if the data isn't loaded

**Type:** *<a href="#BBSeries">BBSeries</a>*


---
### <a name="widget-bb-turnovers-ngselectedProduct"></a>*selectedProduct*

The selected product for turnovers evaluation.

**Type:** *<a href="#Product">Product</a>*


---
### <a name="widget-bb-turnovers-ngproducts"></a>*products*

List of products to be used by account selector for turnovers evaluation.
Is recieved from <a href="#Hooks.processProductsList">Hooks.processProductsList</a>

**Type:** *Array*


---
### <a name="widget-bb-turnovers-ngperiodStartDate"></a>*periodStartDate*

Date of the turnovers evaluation period start

**Type:** *String*


---
### <a name="widget-bb-turnovers-ngperiodEndDate"></a>*periodEndDate*

Date of the turnovers evaluation period end

**Type:** *String*


---
### <a name="widget-bb-turnovers-ngintervalDuration"></a>*intervalDuration*

Length of each periodic interval

**Type:** *String*


---
### <a name="widget-bb-turnovers-ngintervalStartDay"></a>*intervalStartDay*

Day of a month to start turnover interval

**Type:** *Number*


---
### <a name="widget-bb-turnovers-ngisLoading"></a>*isLoading*

Loading status

**Type:** *Boolean*


---
### <a name="widget-bb-turnovers-nginterval"></a>*interval*

Object containing all available intervals as key:value pairs

**Type:** *<a href="#Interval">Interval</a>*


---
### <a name="widget-bb-turnovers-ngerror"></a>*error*

The error encountered when attempting to fetch from the model

**Type:** *<a href="#ModelError">ModelError</a>*


---

## default-hooks

Default hooks for widget-bb-turnovers-ng

### <a name="default-hooks#processTurnoverResponse"></a>*#processTurnoverResponse(data)*

Default hook for turnovers response object post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#Turnover">Turnover</a> | turnover object to process |

##### Returns

<a href="#Turnover">Turnover</a> - *turnover response object*

### <a name="default-hooks#processTurnoverSeries"></a>*#processTurnoverSeries(series, data)*

Default hook for turnovers chart series object post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| series | <a href="#BBSeries">BBSeries</a> | chart series data |
| data | <a href="#Turnover">Turnover</a> | original turnover object |

##### Returns

Object - *processed series*

### <a name="default-hooks#processSelectedProduct"></a>*#processSelectedProduct(product)*

Default hook to process product then it's selected

| Parameter | Type | Description |
| :-- | :-- | :-- |
| product | <a href="#Product">Product</a> | which is selected |

##### Returns

<a href="#Product">Product</a> - *product after processing*

### <a name="default-hooks#processProductsList"></a>*#processProductsList(products)*

Process passed products list before passing it to the view.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| products | <a href="#ProductKinds">ProductKinds</a> | to process |

##### Returns

<a href="#ProductKinds">ProductKinds</a> - *processed products*

### <a name="default-hooks#onTurnoversUpdate"></a>*#onTurnoversUpdate(params)*

Process parameters before they are sent to the model's load method

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | to process |

##### Returns

Object - *processed params*

### <a name="default-hooks#defaultPeriodStart"></a>*#defaultPeriodStart()*

Sets period start property on init

##### Returns

String - *Start period string in format yyyy-mm-dd*

### <a name="default-hooks#defaultPeriodEnd"></a>*#defaultPeriodEnd()*

Sets period end property on init

##### Returns

String - *End period string in format yyyy-mm-dd*

### <a name="default-hooks#defaultInterval"></a>*#defaultInterval(interval)*

Sets interval property on init

| Parameter | Type | Description |
| :-- | :-- | :-- |
| interval | <a href="#Interval">Interval</a> | Available intervals |

##### Returns

String - *One of the available intervals*

### <a name="default-hooks#defaultStartDay"></a>*#defaultStartDay()*

Sets monthly interval start day on init

##### Returns

String - *One of the available intervals*

### <a name="default-hooks#processLoadError"></a>*#processLoadError(The)*

Sets the error for missing parameters in the turnovers request

| Parameter | Type | Description |
| :-- | :-- | :-- |
| The | Error | error passed |

##### Returns

Error - *The actual error*

---

## Events

### <a name="bb.event.product.selected"></a>*bb.event.product.selected*

Triggered when product is selected.

### <a name="bb.event.turnovers.period.start.date.changed"></a>*bb.event.turnovers.period.start.date.changed*

Triggered when period start date is changed.

### <a name="bb.event.turnovers.period.end.date.changed"></a>*bb.event.turnovers.period.end.date.changed*

Triggered when period end date is changed.

### <a name="widget-bb-turnovers-ng.load.failed"></a>*widget-bb-turnovers-ng.load.failed*

Triggered when turnovers widget fails to load.


---

## Type Definitions


### <a name="Interval"></a>*Interval*

Interval object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| DAY | String | Daily interval |
| WEEK | String | Weekly interval |
| MONTH | String | Monthly interval |
| YEAR | String | Yearly interval |

### <a name="Turnover"></a>*Turnover*

Turnover response object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| arrangementId | String | Id of the arrangement this turnover belongs to |
| intervalDuration | String | Duration of intervals returned |
| turnovers | Array of <a href="#TurnoverItem">TurnoverItem</a> | Array of turnover items |

### <a name="TurnoverItem"></a>*TurnoverItem*

Turnover response item

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| intervalStartDate | String | Date in ISO format (2016-06-01T16:41:41.090Z) |
| debitAmount | Object | Debit amount object |
| debitAmount.currencyCode | String | Debit amount currency code (ISO) |
| debitAmount.amount | Number | Debit amount value |
| creditAmount | Object | Credit amount object |
| creditAmount.currencyCode | String | Credit amount currency code (ISO) |
| creditAmount.amount | Number | Credit amount value |
| balance | Object | Debit and credit difference object |
| balance.currencyCode | String | Debit and credit difference currency code (ISO) |
| balance.amount | Number | Debit and credit difference value |

### <a name="Amount"></a>*Amount*

Amount object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String | Currency code |
| value | Number |  |

### <a name="ProductKinds"></a>*ProductKinds*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of products |
| productKinds | Array of <a href="#ProductKind">ProductKind</a> | Array of Products Kinds |

### <a name="ProductKind"></a>*ProductKind*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of product kind |
| name | <a href="#!string">!string</a> | Name of the product kind |
| products | Array of <a href="#Product">Product</a> | Array of associated products |

### <a name="Product"></a>*Product*

Product type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the Product |
| name | <a href="#!string">!string</a> | Name of the Product |
| kind | <a href="#!string">!string</a> | id of the ProductKind |
| alias | String | Alias of the Product |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | Currency code |
| PANSuffix | String | Primary Account Number Suffix |
| bookedBalance | String | Booked balance |
| availableBalance | String | Available balance |
| creditLimit | String | Credit limit |
| currentInvestmentValue | String | Current investment value |
| principalAmount | String | Principal amount |
| accruedInterest | String | Accrued interest |

### <a name="BBSeries"></a>*BBSeries*

BBSeries data object used to draw charts

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| labels | Array of String | Array of x axis labels |
| datasets | Array of <a href="#Dataset">Dataset</a> | Array of all y axis value datasets |

### <a name="Dataset"></a>*Dataset*

Dataset object for y axis data

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of Number | Array of data points to be drawn for each label |

---

## Templates

* *template.ng.html*

---
