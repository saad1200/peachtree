# data-bb-payment-orders-http-ng


Version: **1.0.1**

A data module for accessing the Payment Orders REST API.

## Imports

* vendor-bb-angular

---

## Example

```javascript
import paymentOrdersDataModuleKey, {
  paymentOrdersDataKey,
} from 'data-bb-payment-orders-http-ng';
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>    <a href="#paymentOrdersDataKey">paymentOrdersDataKey</a><br/>
- **PaymentOrdersData**<br/>    <a href="#PaymentOrdersData#getPaymentOrders">#getPaymentOrders(params)</a><br/>    <a href="#PaymentOrdersData#postPaymentOrdersRecord">#postPaymentOrdersRecord(data)</a><br/>    <a href="#PaymentOrdersData#getPaymentOrdersRecord">#getPaymentOrdersRecord(paymentOrderId, params)</a><br/>    <a href="#PaymentOrdersData#deletePaymentOrdersRecord">#deletePaymentOrdersRecord(paymentOrderId, data)</a><br/>    <a href="#PaymentOrdersData#putPaymentOrdersUpdateStatusRecord">#putPaymentOrdersUpdateStatusRecord(data)</a><br/>    <a href="#PaymentOrdersData#getPaymentOrdersCurrencies">#getPaymentOrdersCurrencies(params)</a><br/>    <a href="#PaymentOrdersData#getPaymentOrdersRate">#getPaymentOrdersRate(params)</a><br/>    <a href="#PaymentOrdersData#schemas">#schemas</a><br/>    <a href="#PaymentOrdersData#schemas.postPaymentOrdersRecord">#schemas.postPaymentOrdersRecord</a><br/>    <a href="#PaymentOrdersData#schemas.putPaymentOrdersUpdateStatusRecord">#schemas.putPaymentOrdersUpdateStatusRecord</a><br/>
- **PaymentOrdersDataProvider**<br/>    <a href="#PaymentOrdersDataProvider#setBaseUri">#setBaseUri(baseUri)</a><br/>    <a href="#PaymentOrdersDataProvider#$get">#$get()</a><br/>
- **Type Definitions**<br/>    <a href="#PaymentOrdersData.AccountIdentification">PaymentOrdersData.AccountIdentification</a><br/>    <a href="#PaymentOrdersData.BreachReportError">PaymentOrdersData.BreachReportError</a><br/>    <a href="#PaymentOrdersData.BreachReportItem">PaymentOrdersData.BreachReportItem</a><br/>    <a href="#PaymentOrdersData.Currencies-GET">PaymentOrdersData.Currencies-GET</a><br/>    <a href="#PaymentOrdersData.Currency">PaymentOrdersData.Currency</a><br/>    <a href="#PaymentOrdersData.DebtorAccount">PaymentOrdersData.DebtorAccount</a><br/>    <a href="#PaymentOrdersData.Definitions/BreachInfo">PaymentOrdersData.Definitions/BreachInfo</a><br/>    <a href="#PaymentOrdersData.Definitions/EntityDescription">PaymentOrdersData.Definitions/EntityDescription</a><br/>    <a href="#PaymentOrdersData.Definitions/TimeFrame">PaymentOrdersData.Definitions/TimeFrame</a><br/>    <a href="#PaymentOrdersData.IdentifiedPaymentOrder">PaymentOrdersData.IdentifiedPaymentOrder</a><br/>    <a href="#PaymentOrdersData.InvolvedParty">PaymentOrdersData.InvolvedParty</a><br/>    <a href="#PaymentOrdersData.PaymentOrder">PaymentOrdersData.PaymentOrder</a><br/>    <a href="#PaymentOrdersData.PaymentOrder-GET">PaymentOrdersData.PaymentOrder-GET</a><br/>    <a href="#PaymentOrdersData.PaymentOrders-GET">PaymentOrdersData.PaymentOrders-GET</a><br/>    <a href="#PaymentOrdersData.PaymentOrders-POST">PaymentOrdersData.PaymentOrders-POST</a><br/>    <a href="#PaymentOrdersData.PaymentOrders-POST-Response">PaymentOrdersData.PaymentOrders-POST-Response</a><br/>    <a href="#PaymentOrdersData.Rate-GET">PaymentOrdersData.Rate-GET</a><br/>    <a href="#PaymentOrdersData.StandingOrderSchedule">PaymentOrdersData.StandingOrderSchedule</a><br/>    <a href="#PaymentOrdersData.TransferTransactionInformation">PaymentOrdersData.TransferTransactionInformation</a><br/>    <a href="#PaymentOrdersData.UpdateStatus-PUT">PaymentOrdersData.UpdateStatus-PUT</a><br/>    <a href="#PaymentOrdersData.identification">PaymentOrdersData.identification</a><br/>    <a href="#PaymentOrdersData.postalAddress">PaymentOrdersData.postalAddress</a><br/>    <a href="#PaymentOrdersData.remittanceInformation">PaymentOrdersData.remittanceInformation</a><br/>    <a href="#PaymentOrdersData.remittanceInformation.structured">PaymentOrdersData.remittanceInformation.structured</a><br/>    <a href="#Response">Response</a><br/>

## Exports

### <a name="default"></a>*default*

Angular dependency injection module key

**Type:** *String*

### <a name="paymentOrdersDataKey"></a>*paymentOrdersDataKey*

Angular dependency injection key for the PaymentOrdersData service

**Type:** *String*


---

## PaymentOrdersData

Public api for data-bb-payment-orders-http-ng service

### <a name="PaymentOrdersData#getPaymentOrders"></a>*#getPaymentOrders(params)*

Retrieve list of payments orders

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.status | String | status. |
| params.from | Number (optional) | Page Number. Skip over pages of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | Record UUID. As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field:. |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentOrdersData.PaymentOrders-GET">PaymentOrdersData.PaymentOrders-GET</a> on success*

## Example

```javascript
paymentOrdersData
 .getPaymentOrders(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentOrdersData#postPaymentOrdersRecord"></a>*#postPaymentOrdersRecord(data)*

Start a new Payment Order initiation process

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#PaymentOrdersData.PaymentOrders-POST">PaymentOrdersData.PaymentOrders-POST</a> | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentOrdersData.PaymentOrders-POST-Response">PaymentOrdersData.PaymentOrders-POST-Response</a>, <a href="#PaymentOrdersData.PaymentOrders-POST-Response">PaymentOrdersData.PaymentOrders-POST-Response</a> on success  or rejects with data of <a href="#PaymentOrdersData.BreachReportError">PaymentOrdersData.BreachReportError</a> on error*

## Example

```javascript
paymentOrdersData
 .postPaymentOrdersRecord(data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentOrdersData#getPaymentOrdersRecord"></a>*#getPaymentOrdersRecord(paymentOrderId, params)*

Retrieve the single payment order

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentOrderId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentOrdersData.PaymentOrder-GET">PaymentOrdersData.PaymentOrder-GET</a> on success*

## Example

```javascript
paymentOrdersData
 .getPaymentOrdersRecord(paymentOrderId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentOrdersData#deletePaymentOrdersRecord"></a>*#deletePaymentOrdersRecord(paymentOrderId, data)*

Cancel a single payment order before it got Accepted for initiation

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentOrderId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentOrdersData
 .deletePaymentOrdersRecord(paymentOrderId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentOrdersData#putPaymentOrdersUpdateStatusRecord"></a>*#putPaymentOrdersUpdateStatusRecord(data)*

Update payment order status

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#PaymentOrdersData.UpdateStatus-PUT">PaymentOrdersData.UpdateStatus-PUT</a> | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentOrdersData.UpdateStatus-PUT">PaymentOrdersData.UpdateStatus-PUT</a> on success*

## Example

```javascript
paymentOrdersData
 .putPaymentOrdersUpdateStatusRecord(data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentOrdersData#getPaymentOrdersCurrencies"></a>*#getPaymentOrdersCurrencies(params)*

Get currencies available for payment

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentOrdersData.Currencies-GET">PaymentOrdersData.Currencies-GET</a> on success*

## Example

```javascript
paymentOrdersData
 .getPaymentOrdersCurrencies(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentOrdersData#getPaymentOrdersRate"></a>*#getPaymentOrdersRate(params)*

Get available rate for currencies

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |
| params.currencyFrom | String | Currency transfer from. Eg: EUR. |
| params.currencyTo | String | Currency transfer to. Eg: USD. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentOrdersData.Rate-GET">PaymentOrdersData.Rate-GET</a> on success*

## Example

```javascript
paymentOrdersData
 .getPaymentOrdersRate(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```
### <a name="PaymentOrdersData#schemas"></a>*#schemas*

Schema data. Keys of the object are names of the POST and PUT methods

Note: The schema is not strictly a JSON schema. It is a whitelisted set of
keys for each object property. The keys that are exposed are meant for validation
purposes.

The full list of *possible* keys for each property is:
type, minimum, maximum, minLength, maxLength, pattern, enum, format, default,
properties, items, minItems, maxItems, uniqueItems and required.

See http://json-schema.org/latest/json-schema-validation.html for more details
on the meaning of these keys.

The "required" array from JSON schema is tranformed into a "required" boolean
on each property. This is for ease of use.

**Type:** *Object*

### <a name="PaymentOrdersData#schemas.postPaymentOrdersRecord"></a>*#schemas.postPaymentOrdersRecord*

An object describing the JSON schema for the postPaymentOrdersRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "debtor": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "maxLength": 140,
          "required": true
        },
        "postalAddress": {
          "type": "object",
          "properties": {
            "addressLine": {
              "type": "string",
              "required": false
            },
            "country": {
              "type": "string",
              "required": false
            }
          },
          "required": false
        }
      },
      "required": false
    },
    "debtorAccount": {
      "type": "object",
      "properties": {
        "arrangementId": {
          "type": "string",
          "minLength": 1,
          "required": true
        }
      },
      "required": true
    },
    "batchBooking": {
      "type": "boolean",
      "default": false,
      "required": false
    },
    "instructionPriority": {
      "type": "string",
      "enum": [
        "NORM",
        "HIGH"
      ],
      "default": "NORM",
      "required": false
    },
    "requestedExecutionDate": {
      "type": "string",
      "format": "date-time",
      "required": true
    },
    "paymentMode": {
      "type": "string",
      "enum": [
        "SINGLE",
        "RECURRING"
      ],
      "default": "SINGLE",
      "required": false
    },
    "schedule": {
      "type": "object",
      "properties": {
        "nonWorkingDayExecutionStrategy": {
          "type": "string",
          "enum": [
            "BEFORE",
            "AFTER",
            "NONE"
          ],
          "required": false
        },
        "transferFrequency": {
          "type": "string",
          "enum": [
            "ONCE",
            "DAILY",
            "WEEKLY",
            "MONTHLY",
            "QUARTERLY",
            "YEARLY"
          ],
          "required": true
        },
        "on": {
          "type": "integer",
          "required": true
        },
        "startDate": {
          "type": "string",
          "format": "date-time",
          "required": true
        },
        "endDate": {
          "type": "string",
          "format": "date-time",
          "required": false
        },
        "repeat": {
          "type": "integer",
          "required": false
        },
        "every": {
          "type": "integer",
          "enum": [
            1,
            2
          ],
          "required": true
        },
        "nextExecutionDate": {
          "type": "string",
          "format": "date-time",
          "required": false
        }
      },
      "required": false
    },
    "creditTransferTransactionInformation": {
      "type": "array",
      "items": {
        "properties": {
          "instructedAmount": {
            "type": "object",
            "properties": {
              "amount": {
                "type": "string",
                "required": true
              },
              "currencyCode": {
                "type": "string",
                "pattern": "^[A-Z]{3}$",
                "required": true
              }
            },
            "required": true
          },
          "creditor": {
            "type": "object",
            "properties": {
              "name": {
                "type": "string",
                "maxLength": 140,
                "required": true
              },
              "postalAddress": {
                "type": "object",
                "properties": {
                  "addressLine": {
                    "type": "string",
                    "required": false
                  },
                  "country": {
                    "type": "string",
                    "required": false
                  }
                },
                "required": false
              }
            },
            "required": true
          },
          "creditorAccount": {
            "type": "object",
            "properties": {
              "identification": {
                "type": "object",
                "properties": {
                  "schemeName": {
                    "type": "string",
                    "maxLength": 35,
                    "required": false
                  },
                  "identification": {
                    "type": "string",
                    "maxLength": 34,
                    "required": true
                  }
                },
                "required": true
              },
              "name": {
                "type": "string",
                "maxLength": 140,
                "required": false
              }
            },
            "required": true
          },
          "remittanceInformation": {
            "type": "object",
            "properties": {
              "structured": {
                "type": "object",
                "properties": {
                  "reference": {
                    "type": "string",
                    "required": false
                  },
                  "code": {
                    "type": "string",
                    "required": false
                  },
                  "proprietary": {
                    "type": "string",
                    "required": false
                  }
                },
                "required": false
              },
              "unstructured": {
                "type": "string",
                "required": false
              }
            },
            "required": false
          },
          "endToEndIdentification": {
            "type": "string",
            "maxLength": 35,
            "required": false
          }
        }
      },
      "minItems": 1,
      "required": true
    }
  }
}
```
### <a name="PaymentOrdersData#schemas.putPaymentOrdersUpdateStatusRecord"></a>*#schemas.putPaymentOrdersUpdateStatusRecord*

An object describing the JSON schema for the putPaymentOrdersUpdateStatusRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "bankReferenceId": {
      "type": "string",
      "required": true
    },
    "status": {
      "type": "string",
      "enum": [
        "ENTERED",
        "ACCEPTED",
        "PROCESSED",
        "REJECTED"
      ],
      "required": true
    },
    "bankStatus": {
      "type": "string",
      "maxLength": 35,
      "required": true
    },
    "reasonCode": {
      "type": "string",
      "maxLength": 4,
      "required": false
    },
    "reasonText": {
      "type": "string",
      "maxLength": 35,
      "required": false
    },
    "errorDescription": {
      "type": "string",
      "maxLength": 105,
      "required": false
    }
  }
}
```

---

## PaymentOrdersDataProvider

Data service that can be configured with custom base URI.

| Injector Key |
| :-- |
| *data-bb-payment-orders-http-ng:paymentOrdersDataProvider* |


### <a name="PaymentOrdersDataProvider#setBaseUri"></a>*#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | String | Base URI which will be the prefix for all HTTP requests |

### <a name="PaymentOrdersDataProvider#$get"></a>*#$get()*


##### Returns

Object - *An instance of the service*

## Example

```javascript
// Configuring in an angular app:
angular.module(...)
  .config(['data-bb-payment-orders-http-ng:paymentOrdersDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });

// Configuring With config-bb-providers-ng:
export default [
  ['data-bb-payment-orders-http-ng:paymentOrdersDataProvider', (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
  }]
];
```

## Type Definitions


### <a name="PaymentOrdersData.AccountIdentification"></a>*PaymentOrdersData.AccountIdentification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| identification | <a href="#PaymentOrdersData.identification">PaymentOrdersData.identification</a> |  |
| name | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.BreachReportError"></a>*PaymentOrdersData.BreachReportError*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |
| payment | <a href="#PaymentOrdersData.PaymentOrder">PaymentOrdersData.PaymentOrder</a> (optional) |  |
| checkTime | String (optional) |  |
| breachReport | Array (optional) of <a href="#PaymentOrdersData.BreachReportItem">PaymentOrdersData.BreachReportItem</a> |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.BreachReportItem"></a>*PaymentOrdersData.BreachReportItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| limitedEntity | Array (optional) of <a href="#PaymentOrdersData.Definitions/EntityDescription">PaymentOrdersData.Definitions/EntityDescription</a> | When not set, user-BBID must be set |
| shadow | Boolean (optional) | Shadow limit flag. Applicable for certain entity-types |
| currency | String (optional) | Currency code |
| user-BBID | String (optional) | BBID of the user for whom the personal limit is assigned |
| breachInfo | Array of <a href="#PaymentOrdersData.Definitions/BreachInfo">PaymentOrdersData.Definitions/BreachInfo</a> | List of breached periodic limits related to a particular limitable entity |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.Currencies-GET"></a>*PaymentOrdersData.Currencies-GET*


**Type:** *Array of <a href="#PaymentOrdersData.Currency">PaymentOrdersData.Currency</a>*


### <a name="PaymentOrdersData.Currency"></a>*PaymentOrdersData.Currency*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| amount | String | The amount in the specified currency |
| currencyCode | String | The alpha-3 code (complying with ISO 4217) of the currency that qualifies the amount |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.DebtorAccount"></a>*PaymentOrdersData.DebtorAccount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| arrangementId | String | The unique account identifier |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.Definitions/BreachInfo"></a>*PaymentOrdersData.Definitions/BreachInfo*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| breachType | String | One of "THRESHOLD", "CONSUMPTION" |
| timeframe | <a href="#PaymentOrdersData.Definitions/TimeFrame">PaymentOrdersData.Definitions/TimeFrame</a> |  |
| currentConsumption | String |  |
| currentThreshold | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.Definitions/EntityDescription"></a>*PaymentOrdersData.Definitions/EntityDescription*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| ref | String |  |
| type | String |  |
| description | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.Definitions/TimeFrame"></a>*PaymentOrdersData.Definitions/TimeFrame*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| period | String |  |
| startTime | String |  |
| endTime | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.IdentifiedPaymentOrder"></a>*PaymentOrdersData.IdentifiedPaymentOrder*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |
| status | String |  |
| bankStatus | String (optional) | Internal status of the payment order in the core banking system. |
| reasonCode | String (optional) | Reason code the core banking system accepted/rejected the payment. |
| reasonText | String (optional) | Human readable reason the core banking system accepted/rejected the payment. |
| errorDescription | String (optional) | Additional information from the core banking system on why the payment was refused. |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.InvolvedParty"></a>*PaymentOrdersData.InvolvedParty*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String |  |
| postalAddress | <a href="#PaymentOrdersData.postalAddress">PaymentOrdersData.postalAddress</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.PaymentOrder"></a>*PaymentOrdersData.PaymentOrder*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| debtor | <a href="#PaymentOrdersData.InvolvedParty">PaymentOrdersData.InvolvedParty</a> (optional) | The identification of the debtor party |
| debtorAccount | <a href="#PaymentOrdersData.DebtorAccount">PaymentOrdersData.DebtorAccount</a> |  |
| batchBooking | Boolean (optional) | Indicate whenever there should be only one debit posting for the whole set of instructions |
| instructionPriority | String (optional) | One of "NORM", "HIGH" |
| requestedExecutionDate | String |  |
| paymentMode | String (optional) | One of "SINGLE", "RECURRING" |
| schedule | <a href="#PaymentOrdersData.StandingOrderSchedule">PaymentOrdersData.StandingOrderSchedule</a> (optional) |  |
| creditTransferTransactionInformation | Array of <a href="#PaymentOrdersData.TransferTransactionInformation">PaymentOrdersData.TransferTransactionInformation</a> |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.PaymentOrder-GET"></a>*PaymentOrdersData.PaymentOrder-GET*


**Type:** *Object*


### <a name="PaymentOrdersData.PaymentOrders-GET"></a>*PaymentOrdersData.PaymentOrders-GET*


**Type:** *Array of <a href="#PaymentOrdersData.IdentifiedPaymentOrder">PaymentOrdersData.IdentifiedPaymentOrder</a>*


### <a name="PaymentOrdersData.PaymentOrders-POST"></a>*PaymentOrdersData.PaymentOrders-POST*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| debtor | <a href="#PaymentOrdersData.InvolvedParty">PaymentOrdersData.InvolvedParty</a> (optional) | The identification of the debtor party |
| debtorAccount | <a href="#PaymentOrdersData.DebtorAccount">PaymentOrdersData.DebtorAccount</a> |  |
| batchBooking | Boolean (optional) | Indicate whenever there should be only one debit posting for the whole set of instructions |
| instructionPriority | String (optional) | One of "NORM", "HIGH" |
| requestedExecutionDate | String |  |
| paymentMode | String (optional) | One of "SINGLE", "RECURRING" |
| schedule | <a href="#PaymentOrdersData.StandingOrderSchedule">PaymentOrdersData.StandingOrderSchedule</a> (optional) |  |
| creditTransferTransactionInformation | Array of <a href="#PaymentOrdersData.TransferTransactionInformation">PaymentOrdersData.TransferTransactionInformation</a> |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.PaymentOrders-POST-Response"></a>*PaymentOrdersData.PaymentOrders-POST-Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |
| status | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.Rate-GET"></a>*PaymentOrdersData.Rate-GET*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| rate | Number | Rate for given currencies |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.StandingOrderSchedule"></a>*PaymentOrdersData.StandingOrderSchedule*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| nonWorkingDayExecutionStrategy | String (optional) | One of "BEFORE", "AFTER", "NONE" |
| transferFrequency | String | One of "ONCE", "DAILY", "WEEKLY", "MONTHLY", "QUARTERLY", "YEARLY" |
| on | <a href="#Integer">Integer</a> | Denotes day on which transfer should be executed. For weekly it will be 1..7 indicating weekday. For monthly it will be 1..31 indicating day of month. For yearly it will be 1..12 indicating month of the year |
| startDate | String | When to start executing the schedule. First transfer will be executed on first calculated date by schedule after this date |
| endDate | String (optional) | When to stop transfers. Transfers will not be executed after this date. Only one of endDate and repeat is possible. If neither repeat nor endDate is provided transfer will be executed until canceled |
| repeat | <a href="#Integer">Integer</a> (optional) | Number of transfer to be executed. Only one of endDate and repeat is possible. If neither repeat nor endDate is provided transfer will be executed until canceled |
| every | Number | One of 1, 2 |
| nextExecutionDate | String (optional) | Date when the next payment will be executed, taking in consideration bank holidays and cut-off times. It will be only retrieved when getting payments, it will be dismissed when creating or updating. |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.TransferTransactionInformation"></a>*PaymentOrdersData.TransferTransactionInformation*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| instructedAmount | <a href="#PaymentOrdersData.Currency">PaymentOrdersData.Currency</a> |  |
| creditor | <a href="#PaymentOrdersData.InvolvedParty">PaymentOrdersData.InvolvedParty</a> |  |
| creditorAccount | <a href="#PaymentOrdersData.AccountIdentification">PaymentOrdersData.AccountIdentification</a> |  |
| remittanceInformation | <a href="#PaymentOrdersData.remittanceInformation">PaymentOrdersData.remittanceInformation</a> (optional) |  |
| endToEndIdentification | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.UpdateStatus-PUT"></a>*PaymentOrdersData.UpdateStatus-PUT*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| bankReferenceId | String | This is the internal identifier from the bank that represents the payment order |
| status | String |  |
| bankStatus | String | Internal status of the payment order in the core banking system. |
| reasonCode | String (optional) | Reason code the core banking system accepted/rejected the payment |
| reasonText | String (optional) | Human readable reason the core banking system accepted/rejected the payment |
| errorDescription | String (optional) | Additional information from the core banking system on why the payment was refused |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.identification"></a>*PaymentOrdersData.identification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| schemeName | String (optional) |  |
| identification | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.postalAddress"></a>*PaymentOrdersData.postalAddress*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| addressLine | String (optional) |  |
| country | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.remittanceInformation"></a>*PaymentOrdersData.remittanceInformation*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| structured | <a href="#PaymentOrdersData.remittanceInformation.structured">PaymentOrdersData.remittanceInformation.structured</a> (optional) |  |
| unstructured | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentOrdersData.remittanceInformation.structured"></a>*PaymentOrdersData.remittanceInformation.structured*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| reference | String (optional) |  |
| code | String (optional) |  |
| proprietary | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="Response"></a>*Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Object | See method descriptions for possible return types |
| headers | Function | Getter headers function |
| status | Number | HTTP status code of the response. |
| statusText | String | HTTP status text of the response. |

---
