# ext-bb-user-menu-ng


Version: **2.0.25**

Default extension for User Menu widget.

## Imports

* ui-bb-avatar-ng
* ui-bb-i18n-ng
* vendor-bb-angular-ng-aria

---

## Example

```javascript
<!-- User Menu widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bb-user-menu-ng</value>
</property>
```

## Table of Contents
