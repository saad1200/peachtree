# ext-bb-personal-profile-ng


Version: **1.0.20**

Default extension for personal profile widget.

## Imports

* ui-bb-avatar-ng
* vendor-bb-angular-ng-aria

---

## Example

```javascript
<!-- payment widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bb-personal-profile-ng</value>
</property>
```

## Table of Contents

## Exports


## Hooks

Hooks for widget-bb-personal-profile-ng
