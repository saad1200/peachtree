# data-bb-transactions-http-ng


Version: **1.2.26**

A data module for accessing the Transactions REST API.

## Imports

* vendor-bb-angular

---

## Example

```javascript
import transactionsDataModuleKey, {
  transactionsDataKey,
} from 'data-bb-transactions-http-ng';
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>    <a href="#transactionsDataKey">transactionsDataKey</a><br/>
- **TransactionsData**<br/>    <a href="#TransactionsData#getTransactions">#getTransactions(params)</a><br/>    <a href="#TransactionsData#postTransactionsRecord">#postTransactionsRecord(data)</a><br/>    <a href="#TransactionsData#getTransactionsTurnovers">#getTransactionsTurnovers(params)</a><br/>    <a href="#TransactionsData#getTransactionsSpending">#getTransactionsSpending(params)</a><br/>    <a href="#TransactionsData#schemas">#schemas</a><br/>    <a href="#TransactionsData#schemas.postTransactionsRecord">#schemas.postTransactionsRecord</a><br/>
- **TransactionsDataProvider**<br/>    <a href="#TransactionsDataProvider#setBaseUri">#setBaseUri(baseUri)</a><br/>    <a href="#TransactionsDataProvider#$get">#$get()</a><br/>
- **Type Definitions**<br/>    <a href="#TransactionsData.Currency">TransactionsData.Currency</a><br/>    <a href="#TransactionsData.Spending-GET">TransactionsData.Spending-GET</a><br/>    <a href="#TransactionsData.SpendingItem">TransactionsData.SpendingItem</a><br/>    <a href="#TransactionsData.TransactionItemGet">TransactionsData.TransactionItemGet</a><br/>    <a href="#TransactionsData.TransactionItemPost">TransactionsData.TransactionItemPost</a><br/>    <a href="#TransactionsData.Transactions-BAD-REQUEST">TransactionsData.Transactions-BAD-REQUEST</a><br/>    <a href="#TransactionsData.Transactions-GET">TransactionsData.Transactions-GET</a><br/>    <a href="#TransactionsData.Transactions-INTERNAL-SERVER-ERROR">TransactionsData.Transactions-INTERNAL-SERVER-ERROR</a><br/>    <a href="#TransactionsData.Transactions-POST">TransactionsData.Transactions-POST</a><br/>    <a href="#TransactionsData.TransactionsId">TransactionsData.TransactionsId</a><br/>    <a href="#TransactionsData.Turnovers-GET">TransactionsData.Turnovers-GET</a><br/>    <a href="#Response">Response</a><br/>

## Exports

### <a name="default"></a>*default*

Angular dependency injection module key

**Type:** *String*

### <a name="transactionsDataKey"></a>*transactionsDataKey*

Angular dependency injection key for the TransactionsData service

**Type:** *String*


---

## TransactionsData

Public api for data-bb-transactions-http-ng service

### <a name="TransactionsData#getTransactions"></a>*#getTransactions(params)*

Retrieve list of all transactions by productId.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.amountGreaterThan | Number (optional) | Amount greater than. Eg: 5. |
| params.amountLessThan | Number (optional) | Amount less than. Eg: 5. |
| params.bookingDateGreaterThan | String (optional) | Booking date greater than. Eg: 2016-05-16. |
| params.bookingDateLessThan | String (optional) | Booking date less than. Eg: 2016-05-16. |
| params.arrangementId | String (optional) | The arrangement id. Eg: 11-22-33. |
| params.productId | String | The product id. |
| params.type | String (optional) | Type category. Eg: International payment. |
| params.description | String (optional) | The description of transaction. Eg: description. |
| params.reference | String (optional) | reference. Eg: reference. |
| params.typeGroup | String (optional) | The type group. Eg: Payment. |
| params.counterPartyName | String (optional) | The name of the counterparty. Eg: counterPartyName. |
| params.counterPartyAccountNumber | String (optional) | The International Bank Account Number of the counterparty. Eg: counterPartyAccountNumber. |
| params.creditDebitIndicator | String (optional) | Indicates whether the amount is credited or debited. Eg: InCRDT. |
| params.orderBy | String (optional) | The key to order by. Eg: amount. (defaults to bookingDate) |
| params.direction | String (optional) | The direction to order by. Eg: ASC. (defaults to DESC) |
| params.from | Number (optional) | The page to list from. Eg: 2. (defaults to 0) |
| params.size | Number (optional) | The number of results per page. Eg: 20. (defaults to 10) |
| params.query | String (optional) | The search term used to search for transactions. |
| params.cursor | String (optional) | Record UUID. As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#TransactionsData.Transactions-GET">TransactionsData.Transactions-GET</a> on success*

## Example

```javascript
transactionsData
 .getTransactions(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="TransactionsData#postTransactionsRecord"></a>*#postTransactionsRecord(data)*

# Transaction

This EndPoint allows creating/retrieving of Business/Retail banking transactions for a particular arrangement

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#TransactionsData.Transactions-POST">TransactionsData.Transactions-POST</a> | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#TransactionsData.TransactionsId">TransactionsData.TransactionsId</a> on success*

## Example

```javascript
transactionsData
 .postTransactionsRecord(data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="TransactionsData#getTransactionsTurnovers"></a>*#getTransactionsTurnovers(params)*

get request

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.arrangementId | String | Reference to the product to which the periodic balances belong. |
| params.periodStartDate | String | Date of the turnovers evaluation period start. Eg: 2016-12-31. |
| params.periodEndDate | String | Date of a turnovers evaluation period end. Eg: 2017-04-30. |
| params.intervalDuration | String | Length of each periodic interval. Eg: MONTH. |
| params.intervalStartDay | Number (optional) | Day of a month to start turnover interval. Eg: 1. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#TransactionsData.Turnovers-GET">TransactionsData.Turnovers-GET</a> on success  or rejects with data of <a href="#TransactionsData.Transactions-BAD-REQUEST">TransactionsData.Transactions-BAD-REQUEST</a>, <a href="#TransactionsData.Transactions-INTERNAL-SERVER-ERROR">TransactionsData.Transactions-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
transactionsData
 .getTransactionsTurnovers(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="TransactionsData#getTransactionsSpending"></a>*#getTransactionsSpending(params)*

get request

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |
| params.arrangementId | String | Reference to the product to which the spending analysis belong. |
| params.periodStartDate | String | Date of the spending evaluation period start. Eg: 2016-12-31. |
| params.periodEndDate | String | Date of a spending evaluation period end. Eg: 2017-04-30. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#TransactionsData.Spending-GET">TransactionsData.Spending-GET</a> on success  or rejects with data of <a href="#TransactionsData.Transactions-BAD-REQUEST">TransactionsData.Transactions-BAD-REQUEST</a>, <a href="#TransactionsData.Transactions-INTERNAL-SERVER-ERROR">TransactionsData.Transactions-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
transactionsData
 .getTransactionsSpending(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```
### <a name="TransactionsData#schemas"></a>*#schemas*

Schema data. Keys of the object are names of the POST and PUT methods

Note: The schema is not strictly a JSON schema. It is a whitelisted set of
keys for each object property. The keys that are exposed are meant for validation
purposes.

The full list of *possible* keys for each property is:
type, minimum, maximum, minLength, maxLength, pattern, enum, format, default,
properties, items, minItems, maxItems, uniqueItems and required.

See http://json-schema.org/latest/json-schema-validation.html for more details
on the meaning of these keys.

The "required" array from JSON schema is tranformed into a "required" boolean
on each property. This is for ease of use.

**Type:** *Object*

### <a name="TransactionsData#schemas.postTransactionsRecord"></a>*#schemas.postTransactionsRecord*

An object describing the JSON schema for the postTransactionsRecord method

**Type:** *Object*


## Example

```javascript
{
  "type": "array",
  "items": {
    "properties": {
      "arrangementId": {
        "type": "string",
        "pattern": "^[a-zA-Z0-9_.-]*$",
        "required": false
      },
      "externalId": {
        "type": "string",
        "pattern": "^[a-zA-Z0-9_.-]*$",
        "required": true
      },
      "externalArrangementId": {
        "type": "string",
        "pattern": "^[a-zA-Z0-9_.-]*$",
        "required": true
      },
      "reference": {
        "type": "string",
        "required": true
      },
      "description": {
        "type": "string",
        "required": true
      },
      "typeGroup": {
        "type": "string",
        "enum": [
          "Payment",
          "Withdrawal",
          "Loans",
          "Fees"
        ],
        "required": true
      },
      "type": {
        "type": "string",
        "enum": [
          "SEPA CT",
          "SEPA DD",
          "BACS (UK)",
          "Faster payment (UK)",
          "CHAPS (UK)",
          "International payment",
          "Loan redemption",
          "Interest settlement"
        ],
        "required": true
      },
      "category": {
        "type": "string",
        "required": false
      },
      "bookingDate": {
        "type": "string",
        "format": "date-time",
        "required": true
      },
      "valueDate": {
        "type": "string",
        "format": "date-time",
        "required": false
      },
      "amount": {
        "type": "number",
        "required": true
      },
      "currency": {
        "enum": [
          "AED",
          "AFN",
          "ALL",
          "AMD",
          "ANG",
          "AOA",
          "ARS",
          "AUD",
          "AWG",
          "AZN",
          "BAM",
          "BBD",
          "BDT",
          "BGN",
          "BHD",
          "BIF",
          "BMD",
          "BND",
          "BOB",
          "BOV",
          "BRL",
          "BSD",
          "BTN",
          "BWP",
          "BYN",
          "BZD",
          "CAD",
          "CDF",
          "CHE",
          "CHW",
          "CLF",
          "CLP",
          "CNY",
          "COP",
          "COU",
          "CRC",
          "CUC",
          "CUP",
          "CVE",
          "CZK",
          "DJF",
          "DKK",
          "DOP",
          "DZD",
          "EGP",
          "ERN",
          "ETB",
          "EUR",
          "FJD",
          "FKP",
          "GBP",
          "GEL",
          "GHS",
          "GIP",
          "GMD",
          "GNF",
          "GTQ",
          "GYD",
          "HKD",
          "HNL",
          "HRK",
          "HTG",
          "HUF",
          "IDR",
          "ILS",
          "INR",
          "IQD",
          "IRR",
          "ISK",
          "JMD",
          "JOD",
          "JPY",
          "KES",
          "KGS",
          "KHR",
          "KMF",
          "KPW",
          "KWD",
          "KYD",
          "KZT",
          "LAK",
          "LBP",
          "LKR",
          "LRD",
          "LSL",
          "LYD",
          "MAD",
          "MDL",
          "MGA",
          "MKD",
          "MMK",
          "MNT",
          "MOP",
          "MRO",
          "MUR",
          "MVR",
          "MWK",
          "MXN",
          "MXV",
          "MYR",
          "MZN",
          "NAD",
          "NGN",
          "NIO",
          "NOK",
          "NPR",
          "NZD",
          "OMR",
          "PAB",
          "PEN",
          "PGK",
          "PHP",
          "PKR",
          "PLN",
          "PYG",
          "QAR",
          "RON",
          "RSD",
          "RUB",
          "RWF",
          "SAR",
          "SBD",
          "SCR",
          "SDG",
          "SEK",
          "SGD",
          "SHP",
          "SLL",
          "SOS",
          "SRD",
          "SSP",
          "STD",
          "SVC",
          "SYP",
          "SZL",
          "THB",
          "TJS",
          "TMT",
          "TND",
          "TOP",
          "TRY",
          "TTD",
          "TWD",
          "TZS",
          "UAH",
          "UGX",
          "USD",
          "USN",
          "UYI",
          "UYU",
          "UZS",
          "VEF",
          "VND",
          "VUV",
          "WST",
          "YER",
          "ZAR",
          "ZMW",
          "ZWL"
        ],
        "required": true
      },
      "creditDebitIndicator": {
        "type": "string",
        "enum": [
          "CRDT",
          "DBIT"
        ],
        "required": true
      },
      "instructedAmount": {
        "type": "number",
        "required": false
      },
      "instructedCurrency": {
        "enum": [
          "AED",
          "AFN",
          "ALL",
          "AMD",
          "ANG",
          "AOA",
          "ARS",
          "AUD",
          "AWG",
          "AZN",
          "BAM",
          "BBD",
          "BDT",
          "BGN",
          "BHD",
          "BIF",
          "BMD",
          "BND",
          "BOB",
          "BOV",
          "BRL",
          "BSD",
          "BTN",
          "BWP",
          "BYN",
          "BZD",
          "CAD",
          "CDF",
          "CHE",
          "CHW",
          "CLF",
          "CLP",
          "CNY",
          "COP",
          "COU",
          "CRC",
          "CUC",
          "CUP",
          "CVE",
          "CZK",
          "DJF",
          "DKK",
          "DOP",
          "DZD",
          "EGP",
          "ERN",
          "ETB",
          "EUR",
          "FJD",
          "FKP",
          "GBP",
          "GEL",
          "GHS",
          "GIP",
          "GMD",
          "GNF",
          "GTQ",
          "GYD",
          "HKD",
          "HNL",
          "HRK",
          "HTG",
          "HUF",
          "IDR",
          "ILS",
          "INR",
          "IQD",
          "IRR",
          "ISK",
          "JMD",
          "JOD",
          "JPY",
          "KES",
          "KGS",
          "KHR",
          "KMF",
          "KPW",
          "KWD",
          "KYD",
          "KZT",
          "LAK",
          "LBP",
          "LKR",
          "LRD",
          "LSL",
          "LYD",
          "MAD",
          "MDL",
          "MGA",
          "MKD",
          "MMK",
          "MNT",
          "MOP",
          "MRO",
          "MUR",
          "MVR",
          "MWK",
          "MXN",
          "MXV",
          "MYR",
          "MZN",
          "NAD",
          "NGN",
          "NIO",
          "NOK",
          "NPR",
          "NZD",
          "OMR",
          "PAB",
          "PEN",
          "PGK",
          "PHP",
          "PKR",
          "PLN",
          "PYG",
          "QAR",
          "RON",
          "RSD",
          "RUB",
          "RWF",
          "SAR",
          "SBD",
          "SCR",
          "SDG",
          "SEK",
          "SGD",
          "SHP",
          "SLL",
          "SOS",
          "SRD",
          "SSP",
          "STD",
          "SVC",
          "SYP",
          "SZL",
          "THB",
          "TJS",
          "TMT",
          "TND",
          "TOP",
          "TRY",
          "TTD",
          "TWD",
          "TZS",
          "UAH",
          "UGX",
          "USD",
          "USN",
          "UYI",
          "UYU",
          "UZS",
          "VEF",
          "VND",
          "VUV",
          "WST",
          "YER",
          "ZAR",
          "ZMW",
          "ZWL"
        ],
        "required": false
      },
      "currencyExchangeRate": {
        "type": "number",
        "required": false
      },
      "counterPartyName": {
        "type": "string",
        "required": true
      },
      "counterPartyAccountNumber": {
        "type": "string",
        "required": false
      }
    }
  }
}
```

---

## TransactionsDataProvider

Data service that can be configured with custom base URI.

| Injector Key |
| :-- |
| *data-bb-transactions-http-ng:transactionsDataProvider* |


### <a name="TransactionsDataProvider#setBaseUri"></a>*#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | String | Base URI which will be the prefix for all HTTP requests |

### <a name="TransactionsDataProvider#$get"></a>*#$get()*


##### Returns

Object - *An instance of the service*

## Example

```javascript
// Configuring in an angular app:
angular.module(...)
  .config(['data-bb-transactions-http-ng:transactionsDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });

// Configuring With config-bb-providers-ng:
export default [
  ['data-bb-transactions-http-ng:transactionsDataProvider', (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
  }]
];
```

## Type Definitions


### <a name="TransactionsData.Currency"></a>*TransactionsData.Currency*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| amount | String | The amount in the specified currency |
| currencyCode | String | The alpha-3 code (complying with ISO 4217) of the currency that qualifies the amount |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.Spending-GET"></a>*TransactionsData.Spending-GET*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| totalSpending | <a href="#TransactionsData.Currency">TransactionsData.Currency</a> (optional) | The aggregate spending of all returned categories |
| items | Array (optional) of <a href="#TransactionsData.SpendingItem">TransactionsData.SpendingItem</a> |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.SpendingItem"></a>*TransactionsData.SpendingItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| category | String | Transactions category |
| totalAmount | <a href="#TransactionsData.Currency">TransactionsData.Currency</a> | The total amount of the aggregated transactions by category |
| trend | Number | Percentage value of the trend |
| portion | Number | What percentage of the total spending for a given period is this amount |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.TransactionItemGet"></a>*TransactionsData.TransactionItemGet*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | Internally used unique identification of the transaction |
| arrangementId | String | Reference to the product to which the transaction belongs |
| externalId | String (optional) | Internally used unique external identification of the transaction |
| externalArrangementId | String | External reference to the product to which the transaction belongs |
| productId | String | Reference to the product to which the transaction belongs |
| reference | String | A tag/label issued by the initiator of the transaction in order to be able to refer to the respective transaction |
| description | String |  |
| typeGroup | String | One of "Payment", "Withdrawal", "Loans", "Fees" |
| type | String | One of "SEPA CT", "SEPA DD", "BACS (UK)", "Faster payment (UK)", "CHAPS (UK)", "International payment", "Loan redemption", "Interest settlement" |
| category | String (optional) | Transaction category |
| bookingDate | String | The date the amount is posted to the balance of an account from a book keeping perspective. |
| valueDate | String (optional) | The date on which an amount posted to an account becomes interest bearing |
| amount | Number | The amount of the transaction |
| currency | String |  |
| creditDebitIndicator | String | One of "CRDT", "DBIT" |
| instructedAmount | Number (optional) | Only present if the transaction currency<>account currency |
| instructedCurrency | String (optional) |  |
| currencyExchangeRate | Number (optional) | The exchange rate (between both account and transaction currency) that was used for the conversion. To be used if those currencies are not the same |
| counterPartyName | String | The name of the counterparty |
| counterPartyAccountNumber | String (optional) | The International Bank Account Number of the counterparty |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.TransactionItemPost"></a>*TransactionsData.TransactionItemPost*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| arrangementId | String (optional) | Reference to the product to which the transaction belongs |
| externalId | String | Internally used unique external identification of the transaction |
| externalArrangementId | String | External reference to the product to which the transaction belongs |
| reference | String | A tag/label issued by the initiator of the transaction in order to be able to refer to the respective transaction |
| description | String |  |
| typeGroup | String | One of "Payment", "Withdrawal", "Loans", "Fees" |
| type | String | One of "SEPA CT", "SEPA DD", "BACS (UK)", "Faster payment (UK)", "CHAPS (UK)", "International payment", "Loan redemption", "Interest settlement" |
| category | String (optional) | Transaction category |
| bookingDate | String | The date the amount is posted to the balance of an account from a book keeping perspective. |
| valueDate | String (optional) | The date on which an amount posted to an account becomes interest bearing |
| amount | Number | The amount of the transaction |
| currency | String |  |
| creditDebitIndicator | String | One of "CRDT", "DBIT" |
| instructedAmount | Number (optional) | Only present if the transaction currency<>account currency |
| instructedCurrency | String (optional) |  |
| currencyExchangeRate | Number (optional) | The exchange rate (between both account and transaction currency) that was used for the conversion. To be used if those currencies are not the same |
| counterPartyName | String | The name of the counterparty |
| counterPartyAccountNumber | String (optional) | The International Bank Account Number of the counterparty |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.Transactions-BAD-REQUEST"></a>*TransactionsData.Transactions-BAD-REQUEST*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |
| errorCode | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.Transactions-GET"></a>*TransactionsData.Transactions-GET*


**Type:** *Array of <a href="#TransactionsData.TransactionItemGet">TransactionsData.TransactionItemGet</a>*


### <a name="TransactionsData.Transactions-INTERNAL-SERVER-ERROR"></a>*TransactionsData.Transactions-INTERNAL-SERVER-ERROR*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.Transactions-POST"></a>*TransactionsData.Transactions-POST*


**Type:** *Array of <a href="#TransactionsData.TransactionItemPost">TransactionsData.TransactionItemPost</a>*


### <a name="TransactionsData.TransactionsId"></a>*TransactionsData.TransactionsId*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | Internally used unique identification |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="TransactionsData.Turnovers-GET"></a>*TransactionsData.Turnovers-GET*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| arrangementId | String (optional) | Reference to the product to which the periodic balances belong |
| intervalDuration | String (optional) |  |
| turnovers | Array (optional) of <a href="#*">*</a> |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="Response"></a>*Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Object | See method descriptions for possible return types |
| headers | Function | Getter headers function |
| status | Number | HTTP status code of the response. |
| statusText | String | HTTP status text of the response. |

---
