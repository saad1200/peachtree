# lib-bb-widget-extension-ng

Provides a helper function that creates an angular injectable, which works in conjunction
with `lib-bb-start-ng` to:
 1. merge the custom hooks from the widgets extension with the defaults provided by the widget.
 2. provide the hooks with limited access to contextually useful services.

Extensible widgets should use this library to create hooks that can be consumed by a widget
extension.

## Imports

* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
// extension exports hooks, file: ext-bb-example-ng/scripts/index.js

export const hooks = ({ widget }) => ({
  prepareData: (data) => widget.getPreference('reverse') ? data.reverse() : data,
});

// widget consumes extension hook implementation, file: widget-bb-example-ng/scripts/index.js
import extendHooks from 'lib-bb-widget-extension-ng';

import * as defaultHooks from './default-hooks';

angular.module(...)
  .factory('my-widget:hooks', extendHooks(defaultHooks));
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default(defaultHooks)</a><br/>    <a href="#extensionHooksKey">extensionHooksKey</a><br/>
- **lib-bb-widget-extension-ng**<br/>    <a href="#lib-bb-widget-extension-ngextensionHooksKey">extensionHooksKey</a><br/>
- **Type Definitions**<br/>    <a href="#Hooks">Hooks</a><br/>    <a href="#HooksFactory">HooksFactory(context)</a><br/>    <a href="#HooksContext">HooksContext</a><br/>

## Exports


### <a name="default"></a>*default(defaultHooks)*

Create an angular injectable to help merge the widgets extension hooks with the default hooks
provided by the widget.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| defaultHooks | <a href="#Hooks">Hooks</a> | The default hook implementation |

##### Returns

<a href="#NgInjectedFunction">NgInjectedFunction</a> - **
### <a name="extensionHooksKey"></a>*extensionHooksKey*

The injector key to be used to access the extension hooks module

**Type:** *String*


---
### <a name="lib-bb-widget-extension-ngextensionHooksKey"></a>*extensionHooksKey*

The injector key to be used to access the extension hooks module

**Type:** *String*


## Type Definitions


### <a name="Hooks"></a>*Hooks*


**Type:** *Object of Function*



### <a name="HooksFactory"></a>*HooksFactory(context)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| context | <a href="#HooksContext">HooksContext</a> |  |

##### Returns

<a href="#Hooks">Hooks</a> - **

### <a name="HooksContext"></a>*HooksContext*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| widget | <a href="lib-bb-widget-ng.html#widget">widget</a> |  |

---
