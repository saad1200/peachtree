(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("data-bb-product-summary-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["data-bb-product-summary-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["data-bb-product-summary-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.productSummaryDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbProductSummaryHttp = __webpack_require__(3);
	
	var _dataBbProductSummaryHttp2 = _interopRequireDefault(_dataBbProductSummaryHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/* eslint-disable */
	/**
	 * @module data-bb-product-summary-http-ng
	 *
	 * @description A data module for accessing the Product Summary REST API.
	 *
	 * @returns {String} `data-bb-product-summary-http-ng`
	 * @example
	 * import productSummaryDataModuleKey, {
	 *   productSummaryDataKey,
	 * } from 'data-bb-product-summary-http-ng';
	 */
	
	var productSummaryDataModuleKey = 'data-bb-product-summary-http-ng';
	/**
	 * @name productSummaryDataKey
	 * @type {string}
	 * @description Angular dependency injection key for the ProductSummaryData service
	 */
	var productSummaryDataKey = exports.productSummaryDataKey = 'data-bb-product-summary-http-ng:productSummaryData';
	/**
	 * @name default
	 * @type {string}
	 * @description Angular dependency injection module key
	 */
	exports.default = _vendorBbAngular2.default.module(productSummaryDataModuleKey, [])
	
	/**
	 * @constructor ProductSummaryData
	 * @type {object}
	 *
	 * @description Public api for data-bb-product-summary-http-ng service
	 *
	 */
	.provider(productSummaryDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  /**
	   * @name ProductSummaryDataProvider
	   * @type {object}
	   * @ngkey data-bb-product-summary-http-ng:productSummaryDataProvider
	   * @description
	   * Data service that can be configured with custom base URI.
	   *
	   * @example
	   * // Configuring in an angular app:
	   * angular.module(...)
	   *   .config(['data-bb-product-summary-http-ng:productSummaryDataProvider',
	   *     (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *       });
	   *
	   * // Configuring With config-bb-providers-ng:
	   * export default [
	   *   ['data-bb-product-summary-http-ng:productSummaryDataProvider', (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *   }]
	   * ];
	   */
	  return {
	    /**
	     * @name ProductSummaryDataProvider#setBaseUri
	     * @type {function}
	     * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
	     */
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	
	    /**
	     * @name ProductSummaryDataProvider#$get
	     * @type {function}
	     * @return {object} An instance of the service
	     */
	    $get: ['$http',
	    /* into */
	    (0, _dataBbProductSummaryHttp2.default)(config)]
	  };
	}]).name;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	/* eslint-disable */
	exports.default = function (conf) {
	  return function (httpClient) {
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    /**
	     * The root defined types from the RAML.
	     * @private
	     */
	    var definedTypes = {};
	
	    definedTypes['ProductSummaryData.Productsummary-GET'] = { "properties": { "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false }, "currentAccounts": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "bookedBalance": { "type": "string", "required": false }, "availableBalance": { "type": "string", "required": false }, "creditLimit": { "type": "string", "required": false }, "IBAN": { "type": "string", "pattern": "^[A-Z]{2}\\d{2}[A-Z\\d]{0,30}$", "required": false }, "BBAN": { "type": "string", "required": false }, "currency": { "type": "string", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false }, "savingsAccounts": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "bookedBalance": { "type": "string", "required": false }, "accruedInterest": { "type": "string", "required": false }, "IBAN": { "type": "string", "pattern": "^[A-Z]{2}\\d{2}[A-Z\\d]{0,30}$", "required": false }, "BBAN": { "type": "string", "required": false }, "currency": { "type": "string", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false }, "termDeposits": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "principalAmount": { "type": "string", "required": false }, "accruedInterest": { "type": "string", "required": false }, "currency": { "type": "string", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false }, "productNumber": { "type": "string", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false }, "loans": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "bookedBalance": { "type": "string", "required": false }, "principalAmount": { "type": "string", "required": false }, "currency": { "type": "string", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false }, "productNumber": { "type": "string", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false }, "creditCards": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "bookedBalance": { "type": "string", "required": false }, "availableBalance": { "type": "string", "required": false }, "creditLimit": { "type": "string", "required": false }, "number": { "type": "string", "maxLength": 4, "required": false }, "currency": { "type": "string", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false }, "cardNumber": { "type": "number", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false }, "debitCards": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "number": { "type": "string", "maxLength": 4, "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false }, "cardNumber": { "type": "number", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false }, "investmentAccounts": { "type": "object", "properties": { "name": { "type": "string", "required": false }, "products": { "type": "array", "items": { "properties": { "currentInvestmentValue": { "type": "string", "required": false }, "currency": { "type": "string", "required": false }, "urgentTransferAllowed": { "type": "boolean", "required": false }, "productNumber": { "type": "string", "required": false } } }, "required": false }, "aggregatedBalance": { "type": "object", "properties": { "currency": { "type": "string", "required": false }, "value": { "type": "string", "required": false } }, "required": false } }, "required": false } } };
	
	    definedTypes['ProductSummaryData.Productsummary-BAD-REQUEST'] = { "properties": { "message": { "type": "string", "required": true } } };
	
	    definedTypes['ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR'] = { "properties": { "message": { "type": "string", "required": true } } };
	
	    definedTypes['ProductSummaryData.Productsummary-EXAMPLE'] = { "properties": {} };
	
	    definedTypes['ProductSummaryData.ProductsummaryByLegalEntityId-GET'] = { "properties": {} };
	
	    /**
	     * @typedef ProductSummaryData.AggregatedBalance
	     * @type {Object}
	     * @property {?String} currency
	     * @property {?String} value
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.CreditCard
	     * @type {Object}
	     * @property {?String} bookedBalance
	     * @property {?String} availableBalance
	     * @property {?String} creditLimit
	     * @property {?String} number
	     * @property {?String} currency
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?Number} cardNumber
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.CurrentAccount
	     * @type {Object}
	     * @property {?String} bookedBalance
	     * @property {?String} availableBalance
	     * @property {?String} creditLimit
	     * @property {?String} IBAN
	     * @property {?String} BBAN
	     * @property {?String} currency
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.DebitCard
	     * @type {Object}
	     * @property {?String} number
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?Number} cardNumber
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.InvestmentAccount
	     * @type {Object}
	     * @property {?String} currentInvestmentValue
	     * @property {?String} currency
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?String} productNumber
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.Loan
	     * @type {Object}
	     * @property {?String} bookedBalance
	     * @property {?String} principalAmount
	     * @property {?String} currency
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?String} productNumber
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.Productsummary-BAD-REQUEST
	     * @type {Object}
	     * @property {String} message
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.Productsummary-EXAMPLE
	     * @type {*}
	     */
	
	    /**
	     * @typedef ProductSummaryData.Productsummary-GET
	     * @type {Object}
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?ProductSummaryData.currentAccounts} currentAccounts
	     * @property {?ProductSummaryData.savingsAccounts} savingsAccounts
	     * @property {?ProductSummaryData.termDeposits} termDeposits
	     * @property {?ProductSummaryData.loans} loans
	     * @property {?ProductSummaryData.creditCards} creditCards
	     * @property {?ProductSummaryData.debitCards} debitCards
	     * @property {?ProductSummaryData.investmentAccounts} investmentAccounts
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR
	     * @type {Object}
	     * @property {String} message
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.ProductsummaryByLegalEntityId-GET
	     * @type {Array.<ProductSummaryData.ProductsummaryByLegalentityidItem>}
	     */
	
	    /**
	     * @typedef ProductSummaryData.ProductsummaryByLegalentityidItem
	     * @type {Object}
	     * @property {String} id
	     * @property {String} externalArrangementId
	     * @property {String} externalLegalEntityId
	     * @property {String} externalProductId
	     * @property {?String} name
	     * @property {?String} alias
	     * @property {?Number} bookedBalance
	     * @property {?Number} availableBalance
	     * @property {?Number} creditLimit
	     * @property {?String} IBAN
	     * @property {?String} BBAN
	     * @property {?String} currency
	     * @property {?Boolean} externalTransferAllowed
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?String} accruedInterest
	     * @property {?String} Number
	     * @property {?Number} principalAmount
	     * @property {?Number} currentInvestmentValue
	     * @property {?String} legalEntityId
	     * @property {?String} productId
	     * @property {?String} productNumber
	     * @property {?String} productKindName
	     * @property {?String} productTypeName
	     * @property {?String} BIC
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.SavingsAccount
	     * @type {Object}
	     * @property {?String} bookedBalance
	     * @property {?String} accruedInterest
	     * @property {?String} IBAN
	     * @property {?String} BBAN
	     * @property {?String} currency
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.TermDeposit
	     * @type {Object}
	     * @property {?String} principalAmount
	     * @property {?String} accruedInterest
	     * @property {?String} currency
	     * @property {?Boolean} urgentTransferAllowed
	     * @property {?String} productNumber
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.creditCards
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.CreditCard>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.currentAccounts
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.CurrentAccount>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.debitCards
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.DebitCard>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.investmentAccounts
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.InvestmentAccount>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.loans
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.Loan>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.savingsAccounts
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.SavingsAccount>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /**
	     * @typedef ProductSummaryData.termDeposits
	     * @type {Object}
	     * @property {?String} name
	     * @property {?Array.<ProductSummaryData.TermDeposit>} products
	     * @property {?ProductSummaryData.AggregatedBalance} aggregatedBalance
	     * @property {?Object} additions Container object for custom API extensions
	     */
	
	    /*
	     * @name parse
	     * @type {Function}
	     * @private
	     * @description Should be overitten by transformRespone on a project level
	     */
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers,
	        status: res.status,
	        statusText: res.statusText
	      };
	    }
	
	    /**
	    * @name ProductSummaryData#getProductsummary
	    * @type {Function}
	    * @description Retrieve list of products summaries.
	    
	    * @param {Object} params Map of query parameters.
	      
	    
	    * @returns {Promise.<Response>} Resolves data value as {@link ProductSummaryData.Productsummary-GET} on success  or rejects with data of {@link ProductSummaryData.Productsummary-BAD-REQUEST}, {@link ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR} on error
	    *
	    * @example
	    * productSummaryData
	    *  .getProductsummary(params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getProductsummary(params) {
	      var url = '' + baseUri + version + '/productsummary';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name ProductSummaryData#getProductsummaryDebitaccounts
	    * @type {Function}
	    * @description All accounts available to transfer from
	    
	    * @param {Object} params Map of query parameters.
	      
	    
	    * @returns {Promise.<Response>} Resolves data value as {@link ProductSummaryData.Productsummary-GET} on success  or rejects with data of {@link ProductSummaryData.Productsummary-BAD-REQUEST}, {@link ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR} on error
	    *
	    * @example
	    * productSummaryData
	    *  .getProductsummaryDebitaccounts(params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getProductsummaryDebitaccounts(params) {
	      var url = '' + baseUri + version + '/productsummary/debitaccounts';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name ProductSummaryData#getProductsummaryCreditaccounts
	    * @type {Function}
	    * @description All accounts available for transfer to
	    
	    * @param {?Object} params Map of query parameters.
	      
	    * @param {?string} params.debitAccountId The debit account id to filter with. Eg: 11-22-33.
	      
	    
	    * @returns {Promise.<Response>} Resolves data value as {@link ProductSummaryData.Productsummary-GET} on success  or rejects with data of {@link ProductSummaryData.Productsummary-BAD-REQUEST}, {@link ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR} on error
	    *
	    * @example
	    * productSummaryData
	    *  .getProductsummaryCreditaccounts(params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getProductsummaryCreditaccounts(params) {
	      var url = '' + baseUri + version + '/productsummary/creditaccounts';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name ProductSummaryData#getProductsummaryArrangements
	    * @type {Function}
	    * @description Retrieve list of products summaries, flat structure.
	    
	    * @param {?Object} params Map of query parameters.
	      
	    * @param {?string} params.legalEntityId legalEntityId. Eg: id9876543210.
	      
	    * @param {?number} params.from Page Number. Skip over pages of elements by specifying a start value for the query. Eg: 20. (defaults to 0)
	      
	    * @param {?string} params.cursor Record UUID. As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "")
	      
	    * @param {?number} params.size Limit the number of elements on the response. When used in combination with cursor, the value
	    is allowed to be a negative number to indicate requesting records upwards from the starting point indicated
	    by the cursor. Eg: 80. (defaults to 10)
	      
	    * @param {?string} params.orderBy Order by field: "name", "externalArrangementId", "externalLegalEntityId", "externalProductId", "alias", "bookedBalance", "availableBalance", "creditLimit", "IBAN", "BBAN", "currency", "externalTransferAllowed", "urgentTransferAllowed", "accruedInterest", "Number", "principalAmount", "currentInvestmentValue", "legalEntityId", "productId", "productNumber".
	      
	    * @param {?string} params.direction Direction. (defaults to DESC)
	      
	    
	    * @returns {Promise.<Response>} Resolves data value as {@link ProductSummaryData.ProductsummaryByLegalEntityId-GET} on success  or rejects with data of {@link ProductSummaryData.Productsummary-BAD-REQUEST}, {@link ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR} on error
	    *
	    * @example
	    * productSummaryData
	    *  .getProductsummaryArrangements(params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getProductsummaryArrangements(params) {
	      var url = '' + baseUri + version + '/productsummary/arrangements';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name ProductSummaryData#getProductsummaryConfigurationRecord
	    * @type {Function}
	    * @description Retrieve list of products summaries, flat structure.
	    
	    * @param {string} legalEntityId 
	      
	    
	    * @param {?Object} params Map of query parameters.
	      
	    * @param {?number} params.from Page Number. Skip over pages of elements by specifying a start value for the query. Eg: 20. (defaults to 0)
	      
	    * @param {?string} params.cursor Record UUID. As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "")
	      
	    * @param {?number} params.size Limit the number of elements on the response. When used in combination with cursor, the value
	    is allowed to be a negative number to indicate requesting records upwards from the starting point indicated
	    by the cursor. Eg: 80. (defaults to 10)
	      
	    * @param {?string} params.orderBy Order by field: "name", "externalArrangementId", "externalLegalEntityId", "externalProductId", "alias", "bookedBalance", "availableBalance", "creditLimit", "IBAN", "BBAN", "currency", "externalTransferAllowed", "urgentTransferAllowed", "accruedInterest", "Number", "principalAmount", "currentInvestmentValue", "legalEntityId", "productId", "productNumber".
	      
	    * @param {?string} params.direction Direction. (defaults to DESC)
	      
	    
	    * @returns {Promise.<Response>} Resolves data value as {@link ProductSummaryData.ProductsummaryByLegalEntityId-GET} on success  or rejects with data of {@link ProductSummaryData.Productsummary-BAD-REQUEST}, {@link ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR} on error
	    *
	    * @example
	    * productSummaryData
	    *  .getProductsummaryConfigurationRecord(legalEntityId, params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getProductsummaryConfigurationRecord(legalEntityId, params) {
	      var url = '' + baseUri + version + '/productsummary/configuration/' + legalEntityId;
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    var schemas = {};
	
	    /**
	     * @typedef Response
	     * @type {Object}
	     * @property {Object} data See method descriptions for possible return types
	     * @property {Function} headers Getter headers function
	     * @property {Number} status HTTP status code of the response.
	     * @property {String} statusText HTTP status text of the response.
	     */
	
	    return {
	
	      getProductsummary: getProductsummary,
	
	      getProductsummaryDebitaccounts: getProductsummaryDebitaccounts,
	
	      getProductsummaryCreditaccounts: getProductsummaryCreditaccounts,
	
	      getProductsummaryArrangements: getProductsummaryArrangements,
	
	      getProductsummaryConfigurationRecord: getProductsummaryConfigurationRecord,
	
	      schemas: schemas
	    };
	  };
	};

/***/ })
/******/ ])
});
;
//# sourceMappingURL=data-bb-product-summary-http-ng.js.map