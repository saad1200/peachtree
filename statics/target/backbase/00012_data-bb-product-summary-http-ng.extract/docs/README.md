# data-bb-product-summary-http-ng


Version: **1.1.1**

A data module for accessing the Product Summary REST API.

## Imports

* vendor-bb-angular

---

## Example

```javascript
import productSummaryDataModuleKey, {
  productSummaryDataKey,
} from 'data-bb-product-summary-http-ng';
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>    <a href="#productSummaryDataKey">productSummaryDataKey</a><br/>
- **ProductSummaryData**<br/>    <a href="#ProductSummaryData#getProductsummary">#getProductsummary(params)</a><br/>    <a href="#ProductSummaryData#getProductsummaryDebitaccounts">#getProductsummaryDebitaccounts(params)</a><br/>    <a href="#ProductSummaryData#getProductsummaryCreditaccounts">#getProductsummaryCreditaccounts(params)</a><br/>    <a href="#ProductSummaryData#getProductsummaryArrangements">#getProductsummaryArrangements(params)</a><br/>    <a href="#ProductSummaryData#getProductsummaryConfigurationRecord">#getProductsummaryConfigurationRecord(legalEntityId, params)</a><br/>
- **ProductSummaryDataProvider**<br/>    <a href="#ProductSummaryDataProvider#setBaseUri">#setBaseUri(baseUri)</a><br/>    <a href="#ProductSummaryDataProvider#$get">#$get()</a><br/>
- **Type Definitions**<br/>    <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a><br/>    <a href="#ProductSummaryData.CreditCard">ProductSummaryData.CreditCard</a><br/>    <a href="#ProductSummaryData.CurrentAccount">ProductSummaryData.CurrentAccount</a><br/>    <a href="#ProductSummaryData.DebitCard">ProductSummaryData.DebitCard</a><br/>    <a href="#ProductSummaryData.InvestmentAccount">ProductSummaryData.InvestmentAccount</a><br/>    <a href="#ProductSummaryData.Loan">ProductSummaryData.Loan</a><br/>    <a href="#ProductSummaryData.Productsummary-BAD-REQUEST">ProductSummaryData.Productsummary-BAD-REQUEST</a><br/>    <a href="#ProductSummaryData.Productsummary-EXAMPLE">ProductSummaryData.Productsummary-EXAMPLE</a><br/>    <a href="#ProductSummaryData.Productsummary-GET">ProductSummaryData.Productsummary-GET</a><br/>    <a href="#ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR">ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR</a><br/>    <a href="#ProductSummaryData.ProductsummaryByLegalEntityId-GET">ProductSummaryData.ProductsummaryByLegalEntityId-GET</a><br/>    <a href="#ProductSummaryData.ProductsummaryByLegalentityidItem">ProductSummaryData.ProductsummaryByLegalentityidItem</a><br/>    <a href="#ProductSummaryData.SavingsAccount">ProductSummaryData.SavingsAccount</a><br/>    <a href="#ProductSummaryData.TermDeposit">ProductSummaryData.TermDeposit</a><br/>    <a href="#ProductSummaryData.creditCards">ProductSummaryData.creditCards</a><br/>    <a href="#ProductSummaryData.currentAccounts">ProductSummaryData.currentAccounts</a><br/>    <a href="#ProductSummaryData.debitCards">ProductSummaryData.debitCards</a><br/>    <a href="#ProductSummaryData.investmentAccounts">ProductSummaryData.investmentAccounts</a><br/>    <a href="#ProductSummaryData.loans">ProductSummaryData.loans</a><br/>    <a href="#ProductSummaryData.savingsAccounts">ProductSummaryData.savingsAccounts</a><br/>    <a href="#ProductSummaryData.termDeposits">ProductSummaryData.termDeposits</a><br/>    <a href="#Response">Response</a><br/>

## Exports

### <a name="default"></a>*default*

Angular dependency injection module key

**Type:** *String*

### <a name="productSummaryDataKey"></a>*productSummaryDataKey*

Angular dependency injection key for the ProductSummaryData service

**Type:** *String*


---

## ProductSummaryData

Public api for data-bb-product-summary-http-ng service

### <a name="ProductSummaryData#getProductsummary"></a>*#getProductsummary(params)*

Retrieve list of products summaries.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#ProductSummaryData.Productsummary-GET">ProductSummaryData.Productsummary-GET</a> on success  or rejects with data of <a href="#ProductSummaryData.Productsummary-BAD-REQUEST">ProductSummaryData.Productsummary-BAD-REQUEST</a>, <a href="#ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR">ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
productSummaryData
 .getProductsummary(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="ProductSummaryData#getProductsummaryDebitaccounts"></a>*#getProductsummaryDebitaccounts(params)*

All accounts available to transfer from

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#ProductSummaryData.Productsummary-GET">ProductSummaryData.Productsummary-GET</a> on success  or rejects with data of <a href="#ProductSummaryData.Productsummary-BAD-REQUEST">ProductSummaryData.Productsummary-BAD-REQUEST</a>, <a href="#ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR">ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
productSummaryData
 .getProductsummaryDebitaccounts(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="ProductSummaryData#getProductsummaryCreditaccounts"></a>*#getProductsummaryCreditaccounts(params)*

All accounts available for transfer to

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.debitAccountId | String (optional) | The debit account id to filter with. Eg: 11-22-33. |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#ProductSummaryData.Productsummary-GET">ProductSummaryData.Productsummary-GET</a> on success  or rejects with data of <a href="#ProductSummaryData.Productsummary-BAD-REQUEST">ProductSummaryData.Productsummary-BAD-REQUEST</a>, <a href="#ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR">ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
productSummaryData
 .getProductsummaryCreditaccounts(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="ProductSummaryData#getProductsummaryArrangements"></a>*#getProductsummaryArrangements(params)*

Retrieve list of products summaries, flat structure.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.legalEntityId | String (optional) | legalEntityId. Eg: id9876543210. |
| params.from | Number (optional) | Page Number. Skip over pages of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | Record UUID. As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field: "name", "externalArrangementId", "externalLegalEntityId", "externalProductId", "alias", "bookedBalance", "availableBalance", "creditLimit", "IBAN", "BBAN", "currency", "externalTransferAllowed", "urgentTransferAllowed", "accruedInterest", "Number", "principalAmount", "currentInvestmentValue", "legalEntityId", "productId", "productNumber". |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#ProductSummaryData.ProductsummaryByLegalEntityId-GET">ProductSummaryData.ProductsummaryByLegalEntityId-GET</a> on success  or rejects with data of <a href="#ProductSummaryData.Productsummary-BAD-REQUEST">ProductSummaryData.Productsummary-BAD-REQUEST</a>, <a href="#ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR">ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
productSummaryData
 .getProductsummaryArrangements(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="ProductSummaryData#getProductsummaryConfigurationRecord"></a>*#getProductsummaryConfigurationRecord(legalEntityId, params)*

Retrieve list of products summaries, flat structure.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| legalEntityId | String |  |
| params | Object (optional) | Map of query parameters. |
| params.from | Number (optional) | Page Number. Skip over pages of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | Record UUID. As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field: "name", "externalArrangementId", "externalLegalEntityId", "externalProductId", "alias", "bookedBalance", "availableBalance", "creditLimit", "IBAN", "BBAN", "currency", "externalTransferAllowed", "urgentTransferAllowed", "accruedInterest", "Number", "principalAmount", "currentInvestmentValue", "legalEntityId", "productId", "productNumber". |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#ProductSummaryData.ProductsummaryByLegalEntityId-GET">ProductSummaryData.ProductsummaryByLegalEntityId-GET</a> on success  or rejects with data of <a href="#ProductSummaryData.Productsummary-BAD-REQUEST">ProductSummaryData.Productsummary-BAD-REQUEST</a>, <a href="#ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR">ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR</a> on error*

## Example

```javascript
productSummaryData
 .getProductsummaryConfigurationRecord(legalEntityId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

---

## ProductSummaryDataProvider

Data service that can be configured with custom base URI.

| Injector Key |
| :-- |
| *data-bb-product-summary-http-ng:productSummaryDataProvider* |


### <a name="ProductSummaryDataProvider#setBaseUri"></a>*#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | String | Base URI which will be the prefix for all HTTP requests |

### <a name="ProductSummaryDataProvider#$get"></a>*#$get()*


##### Returns

Object - *An instance of the service*

## Example

```javascript
// Configuring in an angular app:
angular.module(...)
  .config(['data-bb-product-summary-http-ng:productSummaryDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });

// Configuring With config-bb-providers-ng:
export default [
  ['data-bb-product-summary-http-ng:productSummaryDataProvider', (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
  }]
];
```

## Type Definitions


### <a name="ProductSummaryData.AggregatedBalance"></a>*ProductSummaryData.AggregatedBalance*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String (optional) |  |
| value | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.CreditCard"></a>*ProductSummaryData.CreditCard*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| bookedBalance | String (optional) |  |
| availableBalance | String (optional) |  |
| creditLimit | String (optional) |  |
| number | String (optional) |  |
| currency | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| cardNumber | Number (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.CurrentAccount"></a>*ProductSummaryData.CurrentAccount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| bookedBalance | String (optional) |  |
| availableBalance | String (optional) |  |
| creditLimit | String (optional) |  |
| IBAN | String (optional) |  |
| BBAN | String (optional) |  |
| currency | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.DebitCard"></a>*ProductSummaryData.DebitCard*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| number | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| cardNumber | Number (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.InvestmentAccount"></a>*ProductSummaryData.InvestmentAccount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currentInvestmentValue | String (optional) |  |
| currency | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| productNumber | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.Loan"></a>*ProductSummaryData.Loan*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| bookedBalance | String (optional) |  |
| principalAmount | String (optional) |  |
| currency | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| productNumber | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.Productsummary-BAD-REQUEST"></a>*ProductSummaryData.Productsummary-BAD-REQUEST*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.Productsummary-EXAMPLE"></a>*ProductSummaryData.Productsummary-EXAMPLE*


**Type:** *<a href="#*">*</a>*


### <a name="ProductSummaryData.Productsummary-GET"></a>*ProductSummaryData.Productsummary-GET*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| currentAccounts | <a href="#ProductSummaryData.currentAccounts">ProductSummaryData.currentAccounts</a> (optional) |  |
| savingsAccounts | <a href="#ProductSummaryData.savingsAccounts">ProductSummaryData.savingsAccounts</a> (optional) |  |
| termDeposits | <a href="#ProductSummaryData.termDeposits">ProductSummaryData.termDeposits</a> (optional) |  |
| loans | <a href="#ProductSummaryData.loans">ProductSummaryData.loans</a> (optional) |  |
| creditCards | <a href="#ProductSummaryData.creditCards">ProductSummaryData.creditCards</a> (optional) |  |
| debitCards | <a href="#ProductSummaryData.debitCards">ProductSummaryData.debitCards</a> (optional) |  |
| investmentAccounts | <a href="#ProductSummaryData.investmentAccounts">ProductSummaryData.investmentAccounts</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR"></a>*ProductSummaryData.Productsummary-INTERNAL-SERVER-ERROR*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.ProductsummaryByLegalEntityId-GET"></a>*ProductSummaryData.ProductsummaryByLegalEntityId-GET*


**Type:** *Array of <a href="#ProductSummaryData.ProductsummaryByLegalentityidItem">ProductSummaryData.ProductsummaryByLegalentityidItem</a>*


### <a name="ProductSummaryData.ProductsummaryByLegalentityidItem"></a>*ProductSummaryData.ProductsummaryByLegalentityidItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |
| externalArrangementId | String |  |
| externalLegalEntityId | String |  |
| externalProductId | String |  |
| name | String (optional) |  |
| alias | String (optional) |  |
| bookedBalance | Number (optional) |  |
| availableBalance | Number (optional) |  |
| creditLimit | Number (optional) |  |
| IBAN | String (optional) |  |
| BBAN | String (optional) |  |
| currency | String (optional) |  |
| externalTransferAllowed | Boolean (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| accruedInterest | String (optional) |  |
| Number | String (optional) |  |
| principalAmount | Number (optional) |  |
| currentInvestmentValue | Number (optional) |  |
| legalEntityId | String (optional) |  |
| productId | String (optional) |  |
| productNumber | String (optional) |  |
| productKindName | String (optional) |  |
| productTypeName | String (optional) |  |
| BIC | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.SavingsAccount"></a>*ProductSummaryData.SavingsAccount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| bookedBalance | String (optional) |  |
| accruedInterest | String (optional) |  |
| IBAN | String (optional) |  |
| BBAN | String (optional) |  |
| currency | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.TermDeposit"></a>*ProductSummaryData.TermDeposit*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| principalAmount | String (optional) |  |
| accruedInterest | String (optional) |  |
| currency | String (optional) |  |
| urgentTransferAllowed | Boolean (optional) |  |
| productNumber | String (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.creditCards"></a>*ProductSummaryData.creditCards*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.CreditCard">ProductSummaryData.CreditCard</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.currentAccounts"></a>*ProductSummaryData.currentAccounts*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.CurrentAccount">ProductSummaryData.CurrentAccount</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.debitCards"></a>*ProductSummaryData.debitCards*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.DebitCard">ProductSummaryData.DebitCard</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.investmentAccounts"></a>*ProductSummaryData.investmentAccounts*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.InvestmentAccount">ProductSummaryData.InvestmentAccount</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.loans"></a>*ProductSummaryData.loans*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.Loan">ProductSummaryData.Loan</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.savingsAccounts"></a>*ProductSummaryData.savingsAccounts*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.SavingsAccount">ProductSummaryData.SavingsAccount</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="ProductSummaryData.termDeposits"></a>*ProductSummaryData.termDeposits*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String (optional) |  |
| products | Array (optional) of <a href="#ProductSummaryData.TermDeposit">ProductSummaryData.TermDeposit</a> |  |
| aggregatedBalance | <a href="#ProductSummaryData.AggregatedBalance">ProductSummaryData.AggregatedBalance</a> (optional) |  |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="Response"></a>*Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Object | See method descriptions for possible return types |
| headers | Function | Getter headers function |
| status | Number | HTTP status code of the response. |
| statusText | String | HTTP status text of the response. |

---
