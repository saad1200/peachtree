# ext-bbm-notification-center-details-ng


Version: **1.0.17**

Mobile extension for a notiication center details.

## Imports

* lib-bbm-plugins
* ui-bb-date-label-filter-ng
* ui-bb-i18n-ng
* ui-bb-list-ng

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-notification-details-ng</value>
</property>
```

## Table of Contents
