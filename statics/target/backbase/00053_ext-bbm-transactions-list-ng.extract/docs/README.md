# ext-bbm-transactions-list-ng


Version: **1.0.102**

Mobile extension for the transactions list widget.

## Imports

* ui-bb-format-amount
* ui-bb-i18n-ng
* ui-bb-inline-status-ng
* ui-bbm-list-ng

---

## Example

```javascript
<!-- transactions widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bbm-transactions-list-ng</value>
</property>
```

## Table of Contents
- **Hooks**<br/>    <a href="#Hooks#processProductSelected">#processProductSelected(product)</a><br/>    <a href="#Hooks#processTransactions">#processTransactions(transactions)</a><br/>
- **ext-bbm-transactions-list-ng**<br/>    <a href="#ext-bbm-transactions-list-nggetSignedAmount">getSignedAmount(transaction)</a><br/>
- **Type Definitions**<br/>    <a href="#ProductView">ProductView</a><br/>

## Exports


## Hooks

Hooks for widget-bb-transactions-ng

### <a name="Hooks#processProductSelected"></a>*#processProductSelected(product)*

Hook to process the selected product.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| product | Object | The original product data from the API |

##### Returns

<a href="#ProductView">ProductView</a> - *Processed product*

### <a name="Hooks#processTransactions"></a>*#processTransactions(transactions)*

Hook to process the list of transactions.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| transactions | <a href="#array<object>">array<object></a> | The original transactions from the API |

##### Returns

<a href="#array<object>">array<object></a> - *The list of processed transactions*

---

### <a name="ext-bbm-transactions-list-nggetSignedAmount"></a>*getSignedAmount(transaction)*

Based on credit/debit indicator, put right sign on the transaction amount

| Parameter | Type | Description |
| :-- | :-- | :-- |
| transaction | Object | Transaction object |

##### Returns

Number - *Signed amount*

## Type Definitions


### <a name="ProductView"></a>*ProductView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal Product Identifier |
| name | String | The product's name, suitable for display to users |
| identifier | String (optional) | The identifier of the Product from the user's perspective |
| primaryValue | String (optional) | The most important associated value to be displayed |
| secondaryValue | String (optional) | A secondary associated value to be displayed |
| secondaryLabel | String (optional) | A label to describe the secondary value |

---
