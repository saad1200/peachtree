(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("ui-bb-format-amount"), require("ui-bb-i18n-ng"), require("ui-bbm-list-ng"), require("ui-bb-inline-status-ng"));
	else if(typeof define === 'function' && define.amd)
		define("ext-bbm-transactions-list-ng", ["ui-bb-format-amount", "ui-bb-i18n-ng", "ui-bbm-list-ng", "ui-bb-inline-status-ng"], factory);
	else if(typeof exports === 'object')
		exports["ext-bbm-transactions-list-ng"] = factory(require("ui-bb-format-amount"), require("ui-bb-i18n-ng"), require("ui-bbm-list-ng"), require("ui-bb-inline-status-ng"));
	else
		root["ext-bbm-transactions-list-ng"] = factory(root["ui-bb-format-amount"], root["ui-bb-i18n-ng"], root["ui-bbm-list-ng"], root["ui-bb-inline-status-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_6__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_38__, __WEBPACK_EXTERNAL_MODULE_39__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(37);

/***/ }),

/***/ 6:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),

/***/ 37:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.hooks = exports.dependencyKeys = exports.helpers = undefined;
	
	var _uiBbFormatAmount = __webpack_require__(6);
	
	var _uiBbFormatAmount2 = _interopRequireDefault(_uiBbFormatAmount);
	
	var _uiBbmListNg = __webpack_require__(38);
	
	var _uiBbmListNg2 = _interopRequireDefault(_uiBbmListNg);
	
	var _uiBbInlineStatusNg = __webpack_require__(39);
	
	var _uiBbInlineStatusNg2 = _interopRequireDefault(_uiBbInlineStatusNg);
	
	var _uiBbI18nNg = __webpack_require__(8);
	
	var _uiBbI18nNg2 = _interopRequireDefault(_uiBbI18nNg);
	
	var _debitCreditSign = __webpack_require__(40);
	
	var _debitCreditSign2 = _interopRequireDefault(_debitCreditSign);
	
	var _productKindView = __webpack_require__(41);
	
	var _productKindView2 = _interopRequireDefault(_productKindView);
	
	var _helpers = __webpack_require__(42);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var helpers = exports.helpers = _helpers2.default; /**
	                                                    * @module ext-bbm-transactions-list-ng
	                                                    *
	                                                    * @description
	                                                    * Mobile extension for the transactions list widget.
	                                                    *
	                                                    * @example
	                                                    * <!-- transactions widget model.xml -->
	                                                    * <property name="extension" viewHint="text-input,admin">
	                                                    *  <value type="string">ext-bbm-transactions-list-ng</value>
	                                                    * </property>
	                                                    */
	var dependencyKeys = exports.dependencyKeys = [_uiBbFormatAmount2.default, _uiBbmListNg2.default, _uiBbInlineStatusNg2.default, _uiBbI18nNg2.default];
	
	/**
	 * @name groupTransactions
	 *
	 * @description
	 * Groups transactions by date
	 * We assume that scheduledDate is always date only, e.g. "2016-10-14"
	 * Otherwise this method should be improved
	 *
	 * @type {function}
	 * @param {array} transactions
	 * @returns {array} groups - Transactions grouped by date
	 * @inner
	 */
	function groupTransactions(transactions) {
	  var groups = [];
	
	  transactions.forEach(function (transaction) {
	    var date = transaction.bookingDate;
	    var group = groups.find(function (currGroup) {
	      return currGroup.date === date;
	    });
	
	    if (!group) {
	      group = {
	        date: date,
	        transactions: []
	      };
	      groups.push(group);
	    }
	
	    group.transactions.push(transaction);
	  });
	
	  return groups;
	}
	
	/**
	 * @name Hooks
	 * @type {object}
	 *
	 * @description
	 * Hooks for widget-bb-transactions-ng
	 */
	var hooks = exports.hooks = {
	  /**
	   * @name Hooks#processProductSelected
	   *
	   * @description
	   * Hook to process the selected product.
	   *
	   * @type {function}
	   * @param {object} product The original product data from the API
	   * @returns {ProductView} Processed product
	   */
	  processProductSelected: _productKindView2.default,
	
	  /**
	   * @name Hooks#processTransactions
	   * @description
	   * Hook to process the list of transactions.
	   *
	   * @type {function}
	   * @param {array<object>} transactions The original transactions from the API
	   * @returns {array<object>} The list of processed transactions
	   */
	  processTransactions: function processTransactions(transactions) {
	    return groupTransactions(transactions.map(_debitCreditSign2.default));
	  }
	};

/***/ }),

/***/ 38:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_38__;

/***/ }),

/***/ 39:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_39__;

/***/ }),

/***/ 40:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var positiveSignKey = exports.positiveSignKey = 'CRDT';
	var negativeSignKey = exports.negativeSignKey = 'DBIT';
	
	var creditDebitKeysToSign = {};
	creditDebitKeysToSign[positiveSignKey] = '+';
	creditDebitKeysToSign[negativeSignKey] = '-';
	
	/**
	 * @description
	 * Adds debitCreditSign property to transaction object based on debitCreditIndicator key
	 *
	 * @param {Object} transaction Transaction object
	 * @returns {Object} new copy of Transaction object
	 */
	
	exports.default = function (transaction) {
	  return Object.assign({
	    debitCreditSign: creditDebitKeysToSign[transaction.creditDebitIndicator]
	  }, transaction);
	};

/***/ }),

/***/ 41:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var maskCardNumber = function maskCardNumber(suffix) {
	  if (!suffix) {
	    return '';
	  }
	  return 'XXXX-XXXX-XXXX-' + suffix;
	};
	
	var defaultViewModel = function defaultViewModel(product) {
	  return {
	    id: product.id,
	    name: product.name
	  };
	};
	
	var viewModelFactories = {
	  currentAccounts: function currentAccounts(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      identifier: product.IBAN || product.BBAN,
	      primaryValue: product.bookedBalance,
	      secondaryValue: product.availableBalance,
	      secondaryLabel: 'label.availableBalance',
	      tertiaryValue: product.creditLimit,
	      tertiaryLabel: 'label.creditLimit',
	      currency: product.currency
	    };
	  },
	
	  savingsAccounts: function savingsAccounts(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      identifier: product.BBAN || product.IBAN,
	      primaryValue: product.bookedBalance,
	      secondaryValue: product.accruedInterest,
	      secondaryLabel: 'label.accruedInterestAmount',
	      currency: product.currency
	    };
	  },
	
	  termDeposits: function termDeposits(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      primaryValue: product.principalAmount,
	      secondaryValue: product.accruedInterest,
	      secondaryLabel: 'label.accruedInterestAmount',
	      currency: product.currency
	    };
	  },
	
	  creditCards: function creditCards(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      identifier: maskCardNumber(product.number),
	      primaryValue: product.bookedBalance,
	      secondaryValue: product.creditLimit,
	      secondaryLabel: 'label.creditLimit',
	      tertiaryValue: product.availableBalance,
	      tertiaryLabel: 'label.availableBalance',
	      currency: product.currency
	    };
	  },
	
	  debitCards: function debitCards(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      identifier: maskCardNumber(product.number)
	    };
	  },
	
	  loans: function loans(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      primaryValue: product.bookedBalance,
	      currency: product.currency
	    };
	  },
	
	  investmentAccounts: function investmentAccounts(product) {
	    return {
	      id: product.id,
	      name: product.name,
	      primaryValue: product.currentInvestmentValue,
	      currency: product.currency
	    };
	  }
	};
	
	/**
	 * Prepare the fields of a Product into a form ready for display to the User
	 *
	 * @param {object} product The source Product from the API
	 * @type {function}
	 * @returns {ProductView}
	 * @inner
	 */
	
	exports.default = function (product) {
	  var isProcessedProduct = {}.hasOwnProperty.call(product, 'identifier') || {}.hasOwnProperty.call(product, 'primaryValue');
	
	  if (isProcessedProduct) {
	    return product;
	  }
	
	  var kind = product.kind;
	  if (!{}.hasOwnProperty.call(viewModelFactories, kind)) {
	    throw new TypeError('Unhandled product kind: ' + kind);
	  }
	
	  return viewModelFactories[kind](product) || defaultViewModel(product);
	};
	
	/**
	 * @typedef ProductView
	 * @type {object}
	 * @property {string} id The internal Product Identifier
	 * @property {string} name The product's name, suitable for display to users
	 * @property {?string} identifier The identifier of the Product from the user's perspective
	 * @property {?string} primaryValue The most important associated value to be displayed
	 * @property {?string} secondaryValue A secondary associated value to be displayed
	 * @property {?string} secondaryLabel A label to describe the secondary value
	 */

/***/ }),

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _debitCreditSign = __webpack_require__(40);
	
	exports.default = function () {
	  return {
	    /**
	     * @description
	     * Based on credit/debit indicator, put right sign on the transaction amount
	     *
	     * @name getSignedAmount
	     * @type {function}
	     * @param {object} transaction Transaction object
	     * @returns {number} Signed amount
	     */
	    getSignedAmount: function getSignedAmount(transaction) {
	      return transaction.amount * (transaction.creditDebitIndicator === _debitCreditSign.negativeSignKey ? -1 : 1);
	    }
	  };
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=ext-bbm-transactions-list-ng.js.map