# ext-bbm-payment-select-account-ng


Version: **1.0.132**

Mobile extension for the payment select account page in the Mobile payment widget.
Renders either a list of debit accounts, or a list of credit accounts and contacts.

## Imports

* ui-bb-avatar-ng
* ui-bb-format-amount

---

## Example

```javascript
<!-- File model.xml of widget-bbm-payment-ng -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-payment-select-account-ng</value>
</property>
```

## Table of Contents
