(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("data-bb-transactions-http-ng"), require("data-bb-product-summary-http-ng"), require("lib-bb-storage-ng"), require("lib-bb-model-errors"));
	else if(typeof define === 'function' && define.amd)
		define("model-bb-transactions-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "data-bb-transactions-http-ng", "data-bb-product-summary-http-ng", "lib-bb-storage-ng", "lib-bb-model-errors"], factory);
	else if(typeof exports === 'object')
		exports["model-bb-transactions-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("data-bb-transactions-http-ng"), require("data-bb-product-summary-http-ng"), require("lib-bb-storage-ng"), require("lib-bb-model-errors"));
	else
		root["model-bb-transactions-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["data-bb-transactions-http-ng"], root["data-bb-product-summary-http-ng"], root["lib-bb-storage-ng"], root["lib-bb-model-errors"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_63__, __WEBPACK_EXTERNAL_MODULE_64__, __WEBPACK_EXTERNAL_MODULE_65__, __WEBPACK_EXTERNAL_MODULE_66__, __WEBPACK_EXTERNAL_MODULE_68__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(62);

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),

/***/ 62:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.modelTransactionsKey = exports.modelTransactionsModuleKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetNg = __webpack_require__(63);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _dataBbTransactionsHttpNg = __webpack_require__(64);
	
	var _dataBbTransactionsHttpNg2 = _interopRequireDefault(_dataBbTransactionsHttpNg);
	
	var _dataBbProductSummaryHttpNg = __webpack_require__(65);
	
	var _dataBbProductSummaryHttpNg2 = _interopRequireDefault(_dataBbProductSummaryHttpNg);
	
	var _libBbStorageNg = __webpack_require__(66);
	
	var _libBbStorageNg2 = _interopRequireDefault(_libBbStorageNg);
	
	var _transactions = __webpack_require__(67);
	
	var _transactions2 = _interopRequireDefault(_transactions);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module model-bb-transactions-ng
	 *
	 * @description
	 * Transactions model module.
	 *
	 * @usage
	 * import modelTransactionsModuleKey, {
	 *   modelTransactionsKey,
	 * } from 'model-bb-transactions-ng';
	 *
	 * angular.module('widget-bb-transactions-ng', [
	 *   modelTransactionsModuleKey,
	 * ])
	 * .controller('TransactionsController', [
	 *   modelTransactionsKey,
	 *   ...,
	 * ])
	 */
	var modelTransactionsModuleKey = exports.modelTransactionsModuleKey = 'model-bb-transactions-ng';
	var modelTransactionsKey = exports.modelTransactionsKey = 'model-bb-transactions-ng:model';
	
	/**
	 * @name default
	 * @type {string}
	 * @description
	 * Transactions Model
	 */
	exports.default = _vendorBbAngular2.default.module(modelTransactionsModuleKey, [_dataBbTransactionsHttpNg2.default, _dataBbProductSummaryHttpNg2.default, _libBbWidgetNg2.default, _libBbStorageNg2.default]).factory(modelTransactionsKey, [_dataBbTransactionsHttpNg.transactionsDataKey, _dataBbProductSummaryHttpNg.productSummaryDataKey, _libBbWidgetNg.widgetKey, _libBbStorageNg.bbStorageServiceKey,
	/* into */
	_transactions2.default]).name;

/***/ }),

/***/ 63:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_63__;

/***/ }),

/***/ 64:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_64__;

/***/ }),

/***/ 65:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_65__;

/***/ }),

/***/ 66:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_66__;

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = transactionsModel;
	
	var _libBbModelErrors = __webpack_require__(68);
	
	var _constants = __webpack_require__(69);
	
	/**
	 * Transactions model factory
	 * @returns {object}
	 * @inner
	 */
	function transactionsModel(transactionsData, productSummaryData, widget, bbStorage) {
	  /**
	   * @name transactionsModel#loadProducts
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Load products.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var loadProductSummary = function loadProductSummary() {
	    return productSummaryData.getProductsummary().then(function (_ref) {
	      var data = _ref.data;
	
	      bbStorage.setItem(_constants.BbStorageKeys.PRODUCT_SUMMARY, data);
	      return data;
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  };
	
	  /**
	   * @name transactionsModel#getProducts
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Get product list.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to array with products.
	   */
	  var getProducts = function getProducts() {
	    return bbStorage.getItem(_constants.BbStorageKeys.PRODUCT_SUMMARY).then(function (data) {
	      return data || loadProductSummary();
	    }).then(function (data) {
	      return Object.keys(data).filter(function (kind) {
	        return data[kind].products && data[kind].products.length;
	      }).reduce(function (products, kind) {
	        var extendedProducts = data[kind].products.map(function (product) {
	          return Object.assign(product, { kind: kind });
	        });
	        return products.concat(extendedProducts);
	      }, []);
	    });
	  };
	
	  /**
	   * @name transactionsModel#getProductViewModel
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Get product view model contains product name and product number.
	   *
	   * @param {Product} product Product
	   * @returns {ProductViewModel} product view model
	   */
	  var getProductViewModel = function getProductViewModel(product) {
	    return {
	      productName: product.name,
	      productNumber: product.IBAN || product.BBAN || product.productNumber
	    };
	  };
	
	  /**
	   * @name transactionsModel#processResponse
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Process response of loading transaction list.
	   *
	   * @param {Product} product Product
	   * @returns {Promise.<Array>} Processed transaction data.
	   */
	  var processResponse = function processResponse(response) {
	    return getProducts().then(function (products) {
	      var data = response.data.map(function (transaction) {
	        var accountOfTransaction = products.find(function (product) {
	          return product.id === transaction.productId;
	        });
	
	        if (accountOfTransaction) {
	          Object.assign(transaction, getProductViewModel(accountOfTransaction));
	        }
	
	        return transaction;
	      });
	
	      return {
	        totalCount: parseInt(response.headers('x-total-count'), 10) || 0,
	        data: data
	      };
	    });
	  };
	
	  /**
	   * @public
	   * @name transactionsModel#load
	   * @type {function}
	   *
	   * @description
	   * Load transactions.
	   *
	   * @param {object} params Request parameters
	   * @returns {Promise.<Array>} List of transactions as a promise.
	   */
	  var load = function load(params) {
	    return transactionsData.getTransactions(params).then(processResponse).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  };
	
	  /**
	   * @name transactions@getExportFileResource
	   * @type {function}
	   * @description
	   * Compound URI by data module method and query parameters
	   *
	   * @param {object} params Request parameters
	   */
	  var getExportFileResource = function getExportFileResource(params) {
	    var query = Object.keys(params).reduce(function (array, key) {
	      if (Object.prototype.hasOwnProperty.call(params, key) && params[key]) {
	        array.push(key + '=' + params[key]);
	      }
	
	      return array;
	    }, []).join('&');
	
	    return transactionsData.getTransactionsUri('export?' + query, params);
	  };
	
	  /**
	   * @name transactionsModel#getDefaultProduct
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Get default product.
	   *
	   * @param {object} id Product ID
	   * @param {Product[]} products Products list
	   * @returns {Promise.<Product|null, ModelError>}
	   * A Promise resolving to object with default Product or null.
	   */
	  var findProductById = function findProductById(id, products) {
	    return products.find(function (product) {
	      return product.id === id;
	    });
	  };
	
	  /**
	   * @name transactionsModel#getProductFromList
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Get product from list.
	   *
	   * @param {object} id Product ID
	   * @param {object} getFirstInstead Product ID
	   * @returns {Promise.<?Product, ModelError>}
	   * A Promise resolving to object with Product or null.
	   */
	  var getProductFromList = function getProductFromList(id, getFirstInstead) {
	    return getProducts().then(function (products) {
	      var defaultProduct = getFirstInstead && products[0] ? products[0] : null;
	      return id ? findProductById(id, products) || defaultProduct : defaultProduct;
	    });
	  };
	
	  /**
	   * @public
	   * @name transactionsModel#getSelectedProduct
	   * @type {function}
	   *
	   * @description
	   * Get current selected product.
	   *
	   * @returns {Promise.<Product, ModelError>} A Promise with Product.
	   */
	  var getSelectedProduct = function getSelectedProduct() {
	    var getFirstInstead = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
	    return bbStorage.getItem(_constants.BbStorageKeys.PRODUCT_SELECTED).then(function (id) {
	      return getProductFromList(id, getFirstInstead);
	    });
	  };
	
	  /**
	   * @name transactionsModel#getCurrentTransaction
	   * @type {function}
	   *
	   * @description
	   * Tries to read the current transaction from sync preferences
	   *
	   * @returns {object} Transaction data
	   */
	  var getCurrentTransaction = function getCurrentTransaction() {
	    return bbStorage.getItem(_constants.BbStorageKeys.TRANSACTION_SELECTED);
	  };
	
	  /**
	   * @public
	   * @name transactionsModel#storeTransactionAsCurrent
	   * @type {function}
	   *
	   * @description
	   * Stores a given transaction as current in sync preferences
	   *
	   * @param {object} transaction Transaction data
	   */
	  var storeTransactionAsCurrent = function storeTransactionAsCurrent(transaction) {
	    return bbStorage.setItem(_constants.BbStorageKeys.TRANSACTION_SELECTED, transaction);
	  };
	
	  /**
	   * @name transactionsModel
	   * @type {object}
	   */
	  return {
	    load: load,
	    getSelectedProduct: getSelectedProduct,
	    getCurrentTransaction: getCurrentTransaction,
	    storeTransactionAsCurrent: storeTransactionAsCurrent,
	    getExportFileResource: getExportFileResource
	  };
	}
	
	/**
	 * ProductKinds type definition
	 * @typedef {Object} ProductKinds
	 * @property {aggregatedBalance} total            - Aggregated balance object
	 * @property {ProductKind[]}     ProductKinds     - array of Products Kinds
	 */
	
	/**
	 * aggregatedBalance type definition
	 * @typedef {Object} aggregatedBalance
	 * @property {string}            currency         - Currency of balance
	 * @property {string}            value            - Amount of balance
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {Object} ProductKind
	 * @property {!string}    id                      - id of the ProductKind
	 * (currentAccounts, savingsAccounts, termDeposits, creditCards, debitCards, loans,
	 *  investmentAccounts)
	 * @property {!string}    name                    - name of the ProductKind
	 * @property {string}     aggregatedBalance       - aggregated balance
	 * @property {string}     currency                - currency code
	 * @property {Product[]}  products                - array of Products
	 */
	
	/**
	 * Product type definition
	 * @typedef {Object} Product
	 * @property {!string}    id                      - id of the Product
	 * @property {string}     name                    - name of the Product
	 * @property {string}     kind                    - id of the ProductKind
	 * @property {string}     alias                   - alias of the Product
	 * @property {string}     IBAN                    - International Bank Account Number
	 * @property {string}     BBAN                    - Basic Bank Account Number
	 * @property {string}     currency                - currency code
	 * @property {string}     PANSuffix               - Primary Account Number Suffix
	 * @property {string}     bookedBalance           - booked balance
	 * @property {string}     availableBalance        - available balance
	 * @property {string}     creditLimit             - credit limit
	 * @property {string}     currentInvestmentValue  - current investment value
	 * @property {string}     principalAmount         - principal amount
	 * @property {string}     accruedInterest         - accrued interest
	 */
	
	/**
	 * Product view model type definition
	 * @typedef {Object} ProductViewModel
	 * @property {string}     productName             - name of the Product
	 * @property {string}     productNumber           - number of the Product
	 */

/***/ }),

/***/ 68:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_68__;

/***/ }),

/***/ 69:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Widget preferences enum
	 * @type {object}
	 */
	var Preference = exports.Preference = {};
	
	/**
	 * bbStorage keys enum
	 * @type {object}
	 */
	var BbStorageKeys = exports.BbStorageKeys = {
	  PRODUCT_SELECTED: 'bb.product.selected',
	  PRODUCT_SUMMARY: 'bb.product.summary.data',
	  TRANSACTION_SELECTED: 'bb.transaction.selected'
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=model-bb-transactions-ng.js.map