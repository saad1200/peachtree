# model-bb-transactions-ng


Version: **1.4.26**

Transactions model module.

## Imports

* data-bb-product-summary-http-ng
* data-bb-transactions-http-ng
* lib-bb-model-errors
* lib-bb-storage-ng
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
import modelTransactionsModuleKey, {
  modelTransactionsKey,
} from 'model-bb-transactions-ng';

angular.module('widget-bb-transactions-ng', [
  modelTransactionsModuleKey,
])
.controller('TransactionsController', [
  modelTransactionsKey,
  ...,
])
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **transactionsModel**<br/>    <a href="#transactionsModel#load">#load(params)</a><br/>    <a href="#transactionsModel#getSelectedProduct">#getSelectedProduct()</a><br/>    <a href="#transactionsModel#getCurrentTransaction">#getCurrentTransaction()</a><br/>    <a href="#transactionsModel#storeTransactionAsCurrent">#storeTransactionAsCurrent(transaction)</a><br/>
- **model-bb-transactions-ng**<br/>    <a href="#model-bb-transactions-ngtransactions@getExportFileResource">transactions@getExportFileResource(params)</a><br/>
- **Type Definitions**<br/>    <a href="#ProductKinds">ProductKinds</a><br/>    <a href="#aggregatedBalance">aggregatedBalance</a><br/>    <a href="#ProductKind">ProductKind</a><br/>    <a href="#Product">Product</a><br/>    <a href="#ProductViewModel">ProductViewModel</a><br/>

## Exports

### <a name="default"></a>*default*

Transactions Model

**Type:** *String*


---

## transactionsModel


### <a name="transactionsModel#load"></a>*#load(params)*

Load transactions.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Request parameters |

##### Returns

Promise of Array - *List of transactions as a promise.*

### <a name="transactionsModel#getSelectedProduct"></a>*#getSelectedProduct()*

Get current selected product.

##### Returns

Promise of <a href="#Product">Product</a>, <a href="#ModelError">ModelError</a> - *A Promise with Product.*

### <a name="transactionsModel#getCurrentTransaction"></a>*#getCurrentTransaction()*

Tries to read the current transaction from sync preferences

##### Returns

Object - *Transaction data*

### <a name="transactionsModel#storeTransactionAsCurrent"></a>*#storeTransactionAsCurrent(transaction)*

Stores a given transaction as current in sync preferences

| Parameter | Type | Description |
| :-- | :-- | :-- |
| transaction | Object | Transaction data |

---

### <a name="model-bb-transactions-ngtransactions@getExportFileResource"></a>*transactions@getExportFileResource(params)*

Compound URI by data module method and query parameters

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Request parameters |

## Type Definitions


### <a name="ProductKinds"></a>*ProductKinds*

ProductKinds type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| total | <a href="#aggregatedBalance">aggregatedBalance</a> | Aggregated balance object |
| ProductKinds | Array of <a href="#ProductKind">ProductKind</a> | array of Products Kinds |

### <a name="aggregatedBalance"></a>*aggregatedBalance*

aggregatedBalance type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String | Currency of balance |
| value | String | Amount of balance |

### <a name="ProductKind"></a>*ProductKind*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the ProductKind (currentAccounts, savingsAccounts, termDeposits, creditCards, debitCards, loans, investmentAccounts) |
| name | <a href="#!string">!string</a> | name of the ProductKind |
| aggregatedBalance | String | aggregated balance |
| currency | String | currency code |
| products | Array of <a href="#Product">Product</a> | array of Products |

### <a name="Product"></a>*Product*

Product type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the Product |
| name | String | name of the Product |
| kind | String | id of the ProductKind |
| alias | String | alias of the Product |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | currency code |
| PANSuffix | String | Primary Account Number Suffix |
| bookedBalance | String | booked balance |
| availableBalance | String | available balance |
| creditLimit | String | credit limit |
| currentInvestmentValue | String | current investment value |
| principalAmount | String | principal amount |
| accruedInterest | String | accrued interest |

### <a name="ProductViewModel"></a>*ProductViewModel*

Product view model type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| productName | String | name of the Product |
| productNumber | String | number of the Product |

---
