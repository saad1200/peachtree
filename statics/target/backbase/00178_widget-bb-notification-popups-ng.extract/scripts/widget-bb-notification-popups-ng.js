(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-event-bus-ng"), require("model-bb-notifications-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-notification-popups-ng", ["vendor-bb-angular", "lib-bb-event-bus-ng", "model-bb-notifications-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-notification-popups-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-event-bus-ng"), require("model-bb-notifications-ng"));
	else
		root["widget-bb-notification-popups-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-event-bus-ng"], root["model-bb-notifications-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_34__, __WEBPACK_EXTERNAL_MODULE_45__, __WEBPACK_EXTERNAL_MODULE_57__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(63);

/***/ }),

/***/ 34:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_34__;

/***/ }),

/***/ 45:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_45__;

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_57__;

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(34);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _modelBbNotificationsNg = __webpack_require__(57);
	
	var _modelBbNotificationsNg2 = _interopRequireDefault(_modelBbNotificationsNg);
	
	var _libBbEventBusNg = __webpack_require__(45);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _controller = __webpack_require__(64);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module widget-bb-notification-popups-ng
	 *
	 * @description
	 * Notification popups.
	 *
	 * @example
	 * <div ng-controller="NotificationsPopupsController as $ctrl">
	 *  <ul ng-repeat="notification in $ctrl.notifications">
	 *    <li>{{notification.id}}</li>
	 *  </ul>
	 * </div>
	 */
	exports.default = _vendorBbAngular2.default.module('widget-bb-notification-popups-ng', [_modelBbNotificationsNg2.default, _libBbEventBusNg2.default]).controller('NotificationsPopupsController', [_modelBbNotificationsNg.modelNotificationsKey, _libBbEventBusNg.eventBusKey, '$timeout', _controller2.default]).name;

/***/ }),

/***/ 64:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = NotificationsPopupsController;
	
	var _modelBbNotificationsNg = __webpack_require__(57);
	
	var _constants = __webpack_require__(65);
	
	var LOCAL_NOTIFICATION_ID_PREFIX = 'LOCAL_NOTIFICATION:';
	
	function NotificationsPopupsController(model, eventBus, $timeout) {
	  /**
	   * @name NotificationsPopupsController
	   * @ngkey NotificationsPopupsController
	   * @type {object}
	   * @description
	   * Notification Popups controller.
	   */
	  var $ctrl = this;
	
	  var preferences = model.getNotificationPreferences();
	  var isFeNotificationsEnabled = preferences.listenFeNotify;
	  var pollingInterval = preferences.pollingInterval;
	
	  var HIDING_TIMEOUTS = {
	    ALERT: preferences.dismissAlertTime,
	    WARNING: preferences.dismissWarningTime,
	    INFO: preferences.dismissInfoTime,
	    SUCCESS: preferences.dismissSuccessTime
	  };
	
	  var hidingIntervalRef = null;
	  var pollingRef = null;
	
	  /**
	   * @description
	   * Get notifications by ID.
	   *
	   * @name getNotificationById
	   *
	   * @inner
	   * @type {function}
	   * @param {string} id Notification ID
	   * @param {module:model-bb-notifications-ng.Notification[]} list Array of notifications
	   * @returns {module:model-bb-notifications-ng.Notification} A notification object
	   */
	  var getNotificationById = function getNotificationById(id) {
	    var list = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : $ctrl.notifications;
	    return list.find(function (item) {
	      return item.id === id;
	    });
	  };
	
	  /**
	   * @description
	   * Remove notification from array.
	   *
	   * @name removeNotification
	   *
	   * @inner
	   * @type {function}
	   * @param {module:model-bb-notifications-ng.Notification} item A notification object
	   * @param {module:model-bb-notifications-ng.Notification[]} list Array of notifications
	   */
	  var removeNotification = function removeNotification(item) {
	    var list = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : $ctrl.notifications;
	
	    var index = list.indexOf(item);
	    if (index !== -1) {
	      list.splice(index, 1);
	    }
	  };
	
	  /**
	   * @description
	   * Start polling for hiding notifications.
	   *
	   * @name startHideNotificationPolling
	   *
	   * @inner
	   * @type {function}
	   */
	  var startHideNotificationPolling = function startHideNotificationPolling() {
	    var notificationToHide = $ctrl.notifications.find(function (item) {
	      return HIDING_TIMEOUTS[item.level] > 0;
	    });
	    if (notificationToHide) {
	      $timeout.cancel(hidingIntervalRef);
	      hidingIntervalRef = $timeout(function () {
	        removeNotification(notificationToHide);
	        startHideNotificationPolling();
	      }, HIDING_TIMEOUTS[notificationToHide.level]);
	    }
	  };
	
	  /**
	   * @description
	   * return notification type for ui.bootstrap.alert directive according to notifications level:
	   *
	   * ALERT: alert-danger
	   * INFO: alert-info
	   * WARNING: alert-warning
	   * SUCCESS: alert-success
	   *
	   * @name NotificationsPopupsController#getNotificationType
	   * @type {function}
	   * @param {Object} notification A notification object
	   * @returns {Promise.<string>} A Promise with result of marking
	   */
	  var getNotificationType = function getNotificationType(notification) {
	    return _constants.NotificationType[notification.level];
	  };
	
	  /**
	   * @description
	   * removes notification from list.
	   *
	   * @name NotificationsPopupsController#closeNotification
	   * @type {function}
	   * @param {string} id Notification ID
	   * @param {boolean} sticky Type of notification for closing
	   * @fires bb.event.number.of.unread.changed
	   */
	  var closeNotification = function closeNotification(id) {
	    var sticky = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
	
	    var listOfNotifications = sticky ? $ctrl.stickyNotifications : $ctrl.notifications;
	    var notification = getNotificationById(id, listOfNotifications);
	    if (notification) {
	      removeNotification(notification, listOfNotifications);
	      if (!notification.local) {
	        model.putReadRecord(notification.id, { read: true }).then(function () {
	          if (!sticky) {
	            notification.read = true;
	            eventBus.publish(_constants.Event.NOTIFICATION_CHANGE_READ_STATUS, notification);
	          }
	        });
	      }
	    }
	  };
	
	  /**
	   * @description
	   * Add notification from pub-sub
	   *
	   * @name NotificationsPopupsController#addLocalNotification
	   *
	   * @inner
	   * @type {function}
	   * @param {Object} item Notification object
	   */
	  var addLocalNotification = function addLocalNotification(item) {
	    $ctrl.notifications.push(Object.assign({}, item, {
	      id: LOCAL_NOTIFICATION_ID_PREFIX + Math.random(),
	      local: true
	    }));
	
	    startHideNotificationPolling();
	  };
	
	  /**
	   * @description
	   * Funtcion that performs when load stream iteration was successful
	   *
	   * @name onLoadStreamSuccess
	   *
	   * @inner
	   * @type {function}
	   * @param {module:model-bb-notifications-ng.Notification[]} raw Array of notifications
	   */
	  var onLoadStreamSuccess = function onLoadStreamSuccess(raw) {
	    var newNonStickyNotifications = raw.data.filter(function (item) {
	      return !item.expiresOn;
	    });
	
	    eventBus.publish(_constants.Event.NUMBER_OF_UNREAD_CHANGED, newNonStickyNotifications.length);
	
	    // always update sticky notification, but concat regular ones
	    $ctrl.notifications = $ctrl.notifications.concat(newNonStickyNotifications);
	    $ctrl.stickyNotifications = raw.data.filter(function (item) {
	      return item.expiresOn;
	    });
	
	    pollingRef = raw.ref;
	
	    startHideNotificationPolling();
	  };
	
	  /**
	   * @description
	   * Funtcion that performs when error occurs in load stream iteration
	   *
	   * @name onLoadStreamError
	   *
	   * @inner
	   * @type {function}
	   */
	  var stopPolling = function stopPolling() {
	    return model.stopPolling(pollingRef);
	  };
	
	  /**
	   * @description
	   * Init stream polling.
	   *
	   * @name initPolling
	   *
	   * @inner
	   * @type {function}
	   */
	  var initPolling = function initPolling() {
	    var pollingOptions = {
	      type: _modelBbNotificationsNg.PollingType.STREAM,
	      pollingInterval: pollingInterval
	    };
	
	    pollingRef = model.initPolling(pollingOptions);
	
	    eventBus.subscribe(_constants.Event.NOTIFICATION_STREAM_SUCCESS, onLoadStreamSuccess);
	    eventBus.subscribe(_constants.Event.NOTIFICATION_STREAM_ERROR, stopPolling);
	  };
	
	  /*
	   * Widget initialization logic.
	   */
	  var $onInit = function $onInit() {
	    if (isFeNotificationsEnabled) {
	      eventBus.subscribe(_constants.Event.NOTIFICATION_CREATE_LOCAL, addLocalNotification);
	    }
	
	    initPolling();
	  };
	
	  /*
	   * Widget destroying logic.
	   */
	  var $onDestroy = function $onDestroy() {
	    return stopPolling();
	  };
	
	  Object.assign($ctrl, {
	    /**
	     * @description
	     * The array of notifications. Empty if no notifications were received.
	     *
	     * @name NotificationsPopupsController#notifications
	     * @type {module:model-bb-notifications-ng.Notification[]}
	     */
	    notifications: [],
	    /**
	     * @description
	     * The array of sticky notifications. Empty if no sticky notifications were received.
	     *
	     * @name NotificationsPopupsController#stickyNotifications
	     * @type {module:model-bb-notifications-ng.Notification[]}
	     */
	    stickyNotifications: [],
	    /**
	     * @description
	     * True if notifications is loading
	     *
	     * @name NotificationsPopupsController#isNotificationsLoading
	     * @type {boolean}
	     */
	    isNotificationsLoading: false,
	    /**
	     * @description
	     * Checks the list of notifications is empty or not
	     *
	     * @name NotificationsPopupsController#hasNotifications
	     * @type {function}
	     * @returns {boolean} false if notifications list is empty
	     */
	    hasNotifications: function hasNotifications() {
	      return !!$ctrl.notifications.length;
	    },
	    /**
	     * @description
	     * True if sticky notifications can be dismissing
	     *
	     * @name NotificationsPopupsController#dismissSticky
	     * @type {boolean}
	     */
	    dismissSticky: preferences.dismissSticky,
	
	    getNotificationType: getNotificationType,
	    closeNotification: closeNotification,
	    $onInit: $onInit,
	    $onDestroy: $onDestroy
	  });
	}

/***/ }),

/***/ 65:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @description
	 * Widget events enum (events generated by the widget)
	 *
	 * @name Event
	 * @type {object}
	 */
	var Event = exports.Event = {
	  NUMBER_OF_UNREAD_CHANGED: 'bb.event.number.of.unread.changed',
	  NOTIFICATION_CHANGE_READ_STATUS: 'bb.event.notification.change.read.status',
	  NOTIFICATION_CREATE_LOCAL: 'bb.event.notifications.notify',
	  NOTIFICATION_STREAM_SUCCESS: 'bb.event.notifications.stream.success',
	  NOTIFICATION_STREAM_ERROR: 'bb.event.notifications.stream.error'
	};
	
	/**
	 * @description
	 * List of css-classes to be used for detect notification type
	 *
	 * @name NotificationType
	 * @type {object}
	 */
	var NotificationType = exports.NotificationType = {
	  ALERT: 'danger',
	  WARNING: 'warning',
	  INFO: 'info',
	  SUCCESS: 'success'
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=widget-bb-notification-popups-ng.js.map