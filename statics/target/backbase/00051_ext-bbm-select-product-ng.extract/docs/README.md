# ext-bbm-select-product-ng


Version: **1.0.209**

Mobile extension for a select product step in the Payment widget.

## Imports

* ui-bb-avatar-ng
* ui-bb-i18n-ng

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-select-product-ng</value>
</property>
```

## Table of Contents
- **Helpers**<br/>    <a href="#Helpers#hasExternalAccounts">#hasExternalAccounts(accounts)</a><br/>    <a href="#Helpers#onSelectAccount">#onSelectAccount($ctrl, account)</a><br/>
- **Hooks**<br/>    <a href="#Hooks#groupAccountsTo">#groupAccountsTo(accountsTo)</a><br/>

---

## Helpers

Helpers for ext-bbm-select-product-ng

### <a name="Helpers#hasExternalAccounts"></a>*#hasExternalAccounts(accounts)*

Helper to check whether the given list of accounts contains some external accounts

| Parameter | Type | Description |
| :-- | :-- | :-- |
| accounts | <a href="#array<object>">array<object></a> | List of accounts |

##### Returns

Boolean - *True, if there is at least one external account, false otherwise*

### <a name="Helpers#onSelectAccount"></a>*#onSelectAccount($ctrl, account)*

Helper to process account select action

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | Instance of widget angular controller |
| account | Object | Selected account object |

##### Returns

<a href="#void">void</a> - **

##### Fires Events:

> bb.event.payment.form.step

> bb.event.account.selected


---

## Hooks

Hooks for widget-bb-payment-ng

### <a name="Hooks#groupAccountsTo"></a>*#groupAccountsTo(accountsTo)*

Hook for grouping accounts. Used only for Mobile.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| accountsTo | <a href="#array<object>">array<object></a> | List of beneficiary accounts |

##### Returns

<a href="#array<object>">array<object></a> - *List of grouped accounts*
