# model-bb-payment-ng


Version: **1.8.90**

Payment widget model.

## Imports

* data-bb-contact-http-ng
* data-bb-payments-http-ng
* data-bb-product-summary-http-ng
* lib-bb-model-errors
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
import modelPaymentModuleKey, {
  modelPaymentKey,
} from 'model-bb-payment-ng';

angular.module('widget-bb-payment-ng', [
  modelPaymentModuleKey,
])
.controller('PaymentController', [
  modelPaymentKey,
  ...,
])
```

## Table of Contents
- **paymentModel**<br/>    <a href="#paymentModel#getAccountsFrom">#getAccountsFrom()</a><br/>    <a href="#paymentModel#getAccountsTo">#getAccountsTo(debitAccountId)</a><br/>    <a href="#paymentModel#getExternals">#getExternals()</a><br/>    <a href="#paymentModel#createContact">#createContact(contact)</a><br/>    <a href="#paymentModel#makePayment">#makePayment(paymentParams)</a><br/>    <a href="#paymentModel#getPayments">#getPayments(params)</a><br/>    <a href="#paymentModel#getStandingOrders">#getStandingOrders(params)</a><br/>    <a href="#paymentModel#storeAccounts">#storeAccounts(accounts)</a><br/>    <a href="#paymentModel#getAccounts">#getAccounts()</a><br/>    <a href="#paymentModel#getPayment">#getPayment()</a><br/>    <a href="#paymentModel#storePayment">#storePayment(payment)</a><br/>    <a href="#paymentModel#getPaymentPreferences">#getPaymentPreferences()</a><br/>    <a href="#paymentModel#getPaymentSchedule">#getPaymentSchedule(payment, endingType)</a><br/>    <a href="#paymentModel#newPayment">#newPayment()</a><br/>    <a href="#paymentModel#getCurrencies">#getCurrencies()</a><br/>    <a href="#paymentModel#getRate">#getRate()</a><br/>    <a href="#paymentModel#getPaymentsAuthorizations">#getPaymentsAuthorizations(params)</a><br/>    <a href="#paymentModel#authorizePayment">#authorizePayment()</a><br/>    <a href="#paymentModel#authorizeMultiplePayments">#authorizeMultiplePayments()</a><br/>    <a href="#paymentModel#rejectPayment">#rejectPayment()</a><br/>    <a href="#paymentModel#rejectMultiplePayments">#rejectMultiplePayments()</a><br/>    <a href="#paymentModel#deletePayment">#deletePayment(paymentId)</a><br/>    <a href="#paymentModel#deleteMultiplePayments">#deleteMultiplePayments()</a><br/>    <a href="#paymentModel#getDirectDebits">#getDirectDebits(params)</a><br/>    <a href="#paymentModel#refundDirectDebit">#refundDirectDebit(directDebitId, params)</a><br/>    <a href="#paymentModel#refuseDirectDebit">#refuseDirectDebit(directDebitId, params)</a><br/>
- **Type Definitions**<br/>    <a href="#AccountView">AccountView</a><br/>    <a href="#Currency">Currency</a><br/>    <a href="#Rate">Rate</a><br/>    <a href="#AccountIdentification">AccountIdentification</a><br/>    <a href="#Schedule">Schedule</a><br/>    <a href="#Payments">Payments</a><br/>    <a href="#Payment">Payment</a><br/>    <a href="#DirectDebits">DirectDebits</a><br/>    <a href="#DirectDebit">DirectDebit</a><br/>

---

## description

Payment description condition values

---

## singleTransfer

Single transfer frequency object

---

## paymentModes

Available payment modes

---

## endingTypes

Available payment ending types

---

## externalType

Identifier and name for external account product kind

---

## paymentModel

Payment widget and Authorization widget model service.

### <a name="paymentModel#getAccountsFrom"></a>*#getAccountsFrom()*

Load accounts available to payment from.

##### Returns

Promise of Array of <a href="#AccountView">AccountView</a> - *A Promise with flat accounts list.*

### <a name="paymentModel#getAccountsTo"></a>*#getAccountsTo(debitAccountId)*

Load accounts available for payment to.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccountId | String | Filter account list with debitAccountId param |

##### Returns

Promise of Array of <a href="#AccountView">AccountView</a> - *A Promise with flat accounts list.*

### <a name="paymentModel#getExternals"></a>*#getExternals()*

Load external accounts from contact list.

##### Returns

Promise of Array of <a href="#AccountView">AccountView</a> - *A Promise with flat accounts list.*

### <a name="paymentModel#createContact"></a>*#createContact(contact)*

Creates a new contact

| Parameter | Type | Description |
| :-- | :-- | :-- |
| contact | Object | Contact data |

##### Returns

Promise - **

### <a name="paymentModel#makePayment"></a>*#makePayment(paymentParams)*

Send payment to endpoint.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentParams | Object | Payment params to send |

##### Returns

Promise of Object - *A Promise with response.*

### <a name="paymentModel#getPayments"></a>*#getPayments(params)*

Get payments data.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Params to send to the request |

##### Returns

Promise of <a href="#Payments">Payments</a>, <a href="#ModelError">ModelError</a> - *A Promise*

### <a name="paymentModel#getStandingOrders"></a>*#getStandingOrders(params)*

Get standing orders data.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Params to send to the request |

##### Returns

Promise of <a href="#Payments">Payments</a>, <a href="#ModelError">ModelError</a> - *A Promise*

### <a name="paymentModel#storeAccounts"></a>*#storeAccounts(accounts)*

Stores a given list of accounts as current in sync preferences (mobile)

| Parameter | Type | Description |
| :-- | :-- | :-- |
| accounts | Array of <a href="#AccountView">AccountView</a> | Accounts data |

### <a name="paymentModel#getAccounts"></a>*#getAccounts()*

Tries to read the current accounts from sync preferences (mobile)

##### Returns

Array of <a href="#AccountView">AccountView</a> - *Accounts data*

### <a name="paymentModel#getPayment"></a>*#getPayment()*

Tries to read the stored payment from sync preferences (mobile)

##### Returns

<a href="#Payment">Payment</a> - *Payment data*

### <a name="paymentModel#storePayment"></a>*#storePayment(payment)*

Stores a given payment as current in sync preferences (mobile)

| Parameter | Type | Description |
| :-- | :-- | :-- |
| payment | <a href="#Payment">Payment</a> | Payment data |

### <a name="paymentModel#getPaymentPreferences"></a>*#getPaymentPreferences()*

Tries to read the stored payment from sync preferences (mobile)

##### Returns

<a href="#Payment">Payment</a> - *Payment data*

### <a name="paymentModel#getPaymentSchedule"></a>*#getPaymentSchedule(payment, endingType)*

Compiles "schedule" object according to the schema

| Parameter | Type | Description |
| :-- | :-- | :-- |
| payment | <a href="#Payment">Payment</a> |  |
| endingType | String |  |

##### Returns

Object - *schedule object*

### <a name="paymentModel#newPayment"></a>*#newPayment()*

Create new payment model with default values

##### Returns

<a href="#Payment">Payment</a> - **

### <a name="paymentModel#getCurrencies"></a>*#getCurrencies()*

Get currencies available for payment.

##### Returns

Array of <a href="#Currency">Currency</a> - **

### <a name="paymentModel#getRate"></a>*#getRate()*

Get currencies available for payment.

##### Returns

<a href="#Rate">Rate</a> - *rate*

### <a name="paymentModel#getPaymentsAuthorizations"></a>*#getPaymentsAuthorizations(params)*

Retrieve list of only payments, which can be authorized or rejected.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Parameters for the query string |

##### Returns

Promise of Array - *A Promise with an array of payment authorisations.*

### <a name="paymentModel#authorizePayment"></a>*#authorizePayment()*

Authorizes a payment.

##### Returns

Promise - **

### <a name="paymentModel#authorizeMultiplePayments"></a>*#authorizeMultiplePayments()*

Authorizes multiple payments at once.

##### Returns

Promise - **

### <a name="paymentModel#rejectPayment"></a>*#rejectPayment()*

Authorizes a payment.

##### Returns

Promise - **

### <a name="paymentModel#rejectMultiplePayments"></a>*#rejectMultiplePayments()*

Rejects multiple payments at once.

##### Returns

Promise - **

### <a name="paymentModel#deletePayment"></a>*#deletePayment(paymentId)*

Delete a single payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentId | String | Id of the payment |

##### Returns

Promise - **

### <a name="paymentModel#deleteMultiplePayments"></a>*#deleteMultiplePayments()*

Deletes multiple payments at once.

##### Returns

Promise - **

### <a name="paymentModel#getDirectDebits"></a>*#getDirectDebits(params)*

Get direct debits data.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Params to send to the request |

##### Returns

Promise of <a href="#DirectDebits">DirectDebits</a>, <a href="#ModelError">ModelError</a> - *A Promise*

### <a name="paymentModel#refundDirectDebit"></a>*#refundDirectDebit(directDebitId, params)*

Refund direct debit.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| directDebitId | String | Direct Debit ID |
| params | Object | Params to send to the request |

##### Returns

Promise of <a href="#DirectDebits">DirectDebits</a>, <a href="#ModelError">ModelError</a> - *A Promise*

### <a name="paymentModel#refuseDirectDebit"></a>*#refuseDirectDebit(directDebitId, params)*

Refuse direct debit.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| directDebitId | String | Direct Debit ID |
| params | Object | Params to send to the request |

##### Returns

Promise of <a href="#DirectDebits">DirectDebits</a>, <a href="#ModelError">ModelError</a> - *A Promise*

## Type Definitions


### <a name="AccountView"></a>*AccountView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal account identifier |
| name | String | The account's name, suitable for display to users |
| identifier | String (optional) | The identifier of the account from the user's perspective |
| amount | String (optional) | The most important associated value to be displayed |
| currency | String (optional) | Account currency |

### <a name="Currency"></a>*Currency*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal identifier |
| name | String | Currency name, suitable for display to users |

### <a name="Rate"></a>*Rate*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| rate | Number |  |

### <a name="AccountIdentification"></a>*AccountIdentification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| scheme | String | Identification of the product |
| counterpartyName | String (optional) | Name of the counterparty |
| identification | String | Unique identification of the product |

### <a name="Schedule"></a>*Schedule*

Schedule for recurring transfer. Mandatory if paymentMode is RECURRING

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| nonWorkingDayExecutionStrategy | String |  |
| transferFrequency | String | Denotes frequency type of transfer |
| on | Number | Denotes day on which transfer should be executed |
| repeat | Number | Number of transfer to be executed |
| every | Number | Indicates skip interval of transfer |
| startDate | Date | When to start executing the schedule |
| endDate | Date | When to stop transfers |

### <a name="Payments"></a>*Payments*

Payments type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of <a href="#Payment">Payment</a> | Array of Payment orders |
| totalCount | Number | Number of items in the collection |

### <a name="Payment"></a>*Payment*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| status | String | Authorization status of the payment |
| debitAccountIdentification | <a href="#AccountIdentification">AccountIdentification</a> (optional) | Data of the payment source account |
| creditAccountIdentification | <a href="#AccountIdentification">AccountIdentification</a> (optional) | Data of the payment target account |
| amount | Number | Amount of the payment |
| currency | String | Currency of the payment |
| date | String | Execution date of the payment |
| description | String | Description of the payment |
| paymentMode | String | Mode of the payment |
| schedule | <a href="#Schedule">Schedule</a> | Schedule for recurring transfer |
| urgent | Boolean (optional) | Flag if it is an urgent payment |

### <a name="DirectDebits"></a>*DirectDebits*

Direct Debits type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of <a href="#DirectDebit">DirectDebit</a> | Direct Debits orders |
| totalCount | Number | Number of items in the collection |

### <a name="DirectDebit"></a>*DirectDebit*

Direct Debit type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | Direct Debit ID |
| status | String | Direct Debit status |
| refundDayCount | Number (optional) |  |
| creditorReference | String | Creditor ID |
| mandateReference | String | Mandate ID |
| debitAccountIdentification | <a href="#AccountIdentification">AccountIdentification</a> | Data of the payment debit account |
| creditAccountIdentification | <a href="#AccountIdentification">AccountIdentification</a> | Data of the payment credit account |
| amount | Number | Amount of the payment |
| currency | String | Currency of the payment |
| date | String | Execution date of the payment |
| description | String | Description of the payment |
| paymentMode | String | Mode of the payment |
| schedule | <a href="#Schedule">Schedule</a> | Schedule for recurring transfer |

---
