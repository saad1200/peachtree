# widget-bb-payment-ng


Version: **1.0.237**

Payment widget.

## Imports

* lib-bb-event-bus-ng
* lib-bb-model-errors
* lib-bb-storage-ng
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-payment-ng
* vendor-bb-angular

---

## Example

```javascript
<div ng-controller="PaymentController as $ctrl">
   <div ng-if="$ctrl.payment.from">
     <h4 ng-bind="$ctrl.payment.from.name"></h4>
     <p ng-bind="$ctrl.payment.from.identifier"></p>
     <ui-bb-format-amount
       class="pull-right"
       amount="$ctrl.payment.from.amount"
       currency="$ctrl.payment.from.currency">
     </ui-bb-format-amount>
   </div>
 </div>
```

## Table of Contents
- **widget-bb-payment-ng**<br/>    <a href="#widget-bb-payment-ngLOADING_MSG_DELAY">LOADING_MSG_DELAY</a><br/>
- **PaymentController**<br/>    <a href="#PaymentController#storePayment">#storePayment(paymentData)</a><br/>    <a href="#PaymentController#storeAccounts">#storeAccounts(paymentData)</a><br/>    <a href="#PaymentController#processSelectProductType">#processSelectProductType(isCreditProduct)</a><br/>    <a href="#PaymentController#updateRate">#updateRate()</a><br/>    <a href="#PaymentController#onAccountFromChange">#onAccountFromChange()</a><br/>    <a href="#PaymentController#resetPayment">#resetPayment()</a><br/>    <a href="#PaymentController#$onInit">#$onInit()</a><br/>    <a href="#PaymentController#saveContact">#saveContact(form)</a><br/>    <a href="#PaymentController#paymentDataValid">#paymentDataValid(paymentData)</a><br/>    <a href="#PaymentController#canSelectUrgentPayment">#canSelectUrgentPayment(debitAccount)</a><br/>    <a href="#PaymentController#makePayment">#makePayment(paymentData)</a><br/>    <a href="#PaymentController#canSaveNewContact">#canSaveNewContact(beneficiary, creditAccounts)</a><br/>    <a href="#PaymentController#setSaveContactFlag">#setSaveContactFlag()</a><br/>    <a href="#PaymentController#storeSaveContactFlag">#storeSaveContactFlag()</a><br/>    <a href="#PaymentController#accountsFrom">#accountsFrom</a><br/>    <a href="#PaymentController#accountsTo">#accountsTo</a><br/>    <a href="#PaymentController#externalsTo">#externalsTo</a><br/>    <a href="#PaymentController#accountsLoading">#accountsLoading</a><br/>    <a href="#PaymentController#accountsLoadError">#accountsLoadError</a><br/>    <a href="#PaymentController#currencies">#currencies</a><br/>    <a href="#PaymentController#rate">#rate</a><br/>    <a href="#PaymentController#payment">#payment</a><br/>    <a href="#PaymentController#paymentLoading">#paymentLoading</a><br/>    <a href="#PaymentController#paymentSubmitMessage">#paymentSubmitMessage</a><br/>
- **Hooks**<br/>    <a href="#Hooks#groupAccountsTo">#groupAccountsTo(accountsTo)</a><br/>    <a href="#Hooks#processAccountsTo">#processAccountsTo(debitAccount, getCreditAccounts, getExternalContacts)</a><br/>    <a href="#Hooks#getRecurringTransactionDay">#getRecurringTransactionDay(startDate, transferFrequency)</a><br/>
- **Type Definitions**<br/>    <a href="#AccountView">AccountView</a><br/>    <a href="#Currency">Currency</a><br/>    <a href="#Rate">Rate</a><br/>    <a href="#AccountIdentification">AccountIdentification</a><br/>    <a href="#Schedule">Schedule</a><br/>    <a href="#Payment">Payment</a><br/>    <a href="#ErrorMessage">ErrorMessage</a><br/>    <a href="#ModelError">ModelError</a><br/>

---

## Message

Widget messages enum

---

## Preference

Widget preferences enum

---
### <a name="widget-bb-payment-ngLOADING_MSG_DELAY"></a>*LOADING_MSG_DELAY*

Number of ms after which loading message should appear
for actions that change form content after initialization

**Type:** *Number*


---

## PaymentController

Payment widget controller.

| Injector Key |
| :-- |
| *PaymentController* |


### <a name="PaymentController#storePayment"></a>*#storePayment(paymentData)*

Method to store the payment in sync pref

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentData | <a href="#Payment">Payment</a> |  |

##### Returns

<a href="#void">void</a> - **

### <a name="PaymentController#storeAccounts"></a>*#storeAccounts(paymentData)*

Method to store accounts in sync pref

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentData | <a href="#Payment">Payment</a> |  |

##### Returns

<a href="#void">void</a> - **

### <a name="PaymentController#processSelectProductType"></a>*#processSelectProductType(isCreditProduct)*

Handles the account type select action (for extensions)

| Parameter | Type | Description |
| :-- | :-- | :-- |
| isCreditProduct | Boolean |  |

##### Returns

Promise - **

### <a name="PaymentController#updateRate"></a>*#updateRate()*

Retrieves exchange rate for two currencies.

##### Returns

Promise of <a href="#void">void</a> - **

### <a name="PaymentController#onAccountFromChange"></a>*#onAccountFromChange()*

Account from change handler.

##### Returns

Promise of <a href="#void">void</a> - **

### <a name="PaymentController#resetPayment"></a>*#resetPayment()*

Resets payment model, updates accounts and currency lists

##### Returns

Promise of <a href="#void">void</a> - **

### <a name="PaymentController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller


##### Returns

Promise of <a href="#void">void</a> - **

### <a name="PaymentController#saveContact"></a>*#saveContact(form)*

saves a new contact using widget's model

| Parameter | Type | Description |
| :-- | :-- | :-- |
| form | Object | Contact form object |

##### Returns

Promise - **

### <a name="PaymentController#paymentDataValid"></a>*#paymentDataValid(paymentData)*

Checks if payment data is valid
It cannot have recurring payment details if recurring payment is disabled by admin

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentData | <a href="#Payment">Payment</a> |  |

##### Returns

Boolean - **

### <a name="PaymentController#canSelectUrgentPayment"></a>*#canSelectUrgentPayment(debitAccount)*

Checks if layout should show 'Urgent payment' switcher

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccount | Object | selected |

##### Returns

Boolean - **

### <a name="PaymentController#makePayment"></a>*#makePayment(paymentData)*

Sends payment model to endpoint

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentData | <a href="#Payment">Payment</a> |  |

##### Returns

Promise of <a href="#void">void</a> - **

### <a name="PaymentController#canSaveNewContact"></a>*#canSaveNewContact(beneficiary, creditAccounts)*

Checks if layout should show 'save beneficiary' switcher

| Parameter | Type | Description |
| :-- | :-- | :-- |
| beneficiary | Object | Recipient data |
| creditAccounts | Array | Credit accounts and contacts collection |

##### Returns

Boolean - **

### <a name="PaymentController#setSaveContactFlag"></a>*#setSaveContactFlag()*

Sets the flag that indicates if the contact should be saved when
submitting a payment

##### Returns

<a href="#void">void</a> - **

### <a name="PaymentController#storeSaveContactFlag"></a>*#storeSaveContactFlag()*

Method to store the saveNewContact flag in the sync pref

##### Returns

<a href="#void">void</a> - **
### <a name="PaymentController#accountsFrom"></a>*#accountsFrom*

List of accounts to payment from

**Type:** *Array of <a href="#AccountsView">AccountsView</a>*

### <a name="PaymentController#accountsTo"></a>*#accountsTo*

List of accounts to payment to

**Type:** *Array of <a href="#AccountsView">AccountsView</a>*

### <a name="PaymentController#externalsTo"></a>*#externalsTo*

List of external contacts that can be populated in credit account list

**Type:** *Array of <a href="#AccountsView">AccountsView</a>*

### <a name="PaymentController#accountsLoading"></a>*#accountsLoading*

Flag that tells if contact list is being updated

**Type:** *Boolean*

### <a name="PaymentController#accountsLoadError"></a>*#accountsLoadError*

Store model error key which can be used for translation in the extension

Possible values:
- account.model.error.auth
- account.model.error.connectivity
- account.model.error.user
- account.model.error.unexpected

**Type:** *String*

### <a name="PaymentController#currencies"></a>*#currencies*

List of currencies available for payment

**Type:** *Array of <a href="#Currency">Currency</a>*

### <a name="PaymentController#rate"></a>*#rate*

Rate defined for cross-currency payments

**Type:** *<a href="#Rate">Rate</a>*

### <a name="PaymentController#payment"></a>*#payment*

Payment object, containing info - from account, to account, amount, ...

**Type:** *<a href="#Payment">Payment</a>*

### <a name="PaymentController#paymentLoading"></a>*#paymentLoading*

Flag that tells if new payment is being processed

**Type:** *Boolean*

### <a name="PaymentController#paymentSubmitMessage"></a>*#paymentSubmitMessage*

Store model error key which can be used for translation in the extension

Possible values:
- payment.model.error.auth
- payment.model.error.connectivity
- payment.model.error.user
- payment.model.error.unexpected

**Type:** *String*


---

## Hooks

Hooks for widget-bb-payment-ng

### <a name="Hooks#groupAccountsTo"></a>*#groupAccountsTo(accountsTo)*

Hook for grouping accounts. Used only for Mobile.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| accountsTo | Array of Object | List of beneficiary accounts |

##### Returns

Array of Object - *List of grouped accounts*

### <a name="Hooks#processAccountsTo"></a>*#processAccountsTo(debitAccount, getCreditAccounts, getExternalContacts)*

Hook for processing account list in 'to' field (credit).
Assigned to [$ctrl.accountsTo]<a href="#PaymentController#AccountView">PaymentController#AccountView</a>

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccount | <a href="#ProductKind">ProductKind</a> | Selected debit account (can be null) |
| getCreditAccounts | Function | Function to retrieve all credit accounts |
| getExternalContacts | Function | Function to retrieve all external contacts formatted like Product kind |

##### Returns

Promise of Array of <a href="#any">any</a> - *Promise that retrieves array of accounts.*

### <a name="Hooks#getRecurringTransactionDay"></a>*#getRecurringTransactionDay(startDate, transferFrequency)*

Denotes day on which transfer should be executed.
For weekly it will be 1..7 indicating weekday.
For monthly it will be 1..31 indicating day of month.
For yearly it will be 1..12 indicating month of the year
Assigned to [$ctrl.schedule.on]<a href="#PaymentController#makePayment">PaymentController#makePayment</a>

| Parameter | Type | Description |
| :-- | :-- | :-- |
| startDate | Date | Start date of recurring payment |
| transferFrequency | Object | Recurring frequency |

##### Returns

Number - **

## Type Definitions


### <a name="AccountView"></a>*AccountView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal account identifier |
| name | String | The account's name, suitable for display to users |
| identifier | String (optional) | The identifier of the account from the user's perspective |
| amount | String (optional) | The most important associated value to be displayed |
| currency | String (optional) | Account currency |

### <a name="Currency"></a>*Currency*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal identifier |
| name | String | Currency name, suitable for display to users |

### <a name="Rate"></a>*Rate*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| rate | Number |  |

### <a name="AccountIdentification"></a>*AccountIdentification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| scheme | String | Identification of the product |
| identification | String | Unique identification of the product |

### <a name="Schedule"></a>*Schedule*

Schedule for recurring transfer. Mandatory if paymentMode is RECURRING

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| nonWorkingDayExecutionStrategy | String |  |
| transferFrequency | String | Denotes frequency type of transfer |
| on | Number | Denotes day on which transfer should be executed |
| repeat | Number | Number of transfer to be executed |
| every | Number | Indicates skip interval of transfer |
| startDate | Date | When to start executing the schedule |
| endDate | Date | When to stop transfers |

### <a name="Payment"></a>*Payment*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| debitAccountIdentification | <a href="#AccountIdentification">AccountIdentification</a> (optional) |  |
| creditAccountIdentification | <a href="#AccountIdentification">AccountIdentification</a> (optional) |  |
| amount | Number |  |
| currency | Number |  |
| date | String |  |
| paymentReference | String |  |
| description | String |  |
| paymentMode | String |  |
| schedule | <a href="#Schedule">Schedule</a> |  |

### <a name="ErrorMessage"></a>*ErrorMessage*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| messageKey | String |  |

### <a name="ModelError"></a>*ModelError*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| code | <a href="#ErrorCode">ErrorCode</a> |  |

---

## Templates

* *template.ng.html*

---
