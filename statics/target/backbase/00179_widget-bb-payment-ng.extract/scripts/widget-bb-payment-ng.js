(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("lib-bb-event-bus-ng"), require("model-bb-payment-ng"), require("lib-bb-storage-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-payment-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "lib-bb-model-errors", "lib-bb-widget-extension-ng", "lib-bb-event-bus-ng", "model-bb-payment-ng", "lib-bb-storage-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-payment-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("lib-bb-event-bus-ng"), require("model-bb-payment-ng"), require("lib-bb-storage-ng"));
	else
		root["widget-bb-payment-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"], root["lib-bb-widget-extension-ng"], root["lib-bb-event-bus-ng"], root["model-bb-payment-ng"], root["lib-bb-storage-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_94__, __WEBPACK_EXTERNAL_MODULE_109__, __WEBPACK_EXTERNAL_MODULE_114__, __WEBPACK_EXTERNAL_MODULE_124__, __WEBPACK_EXTERNAL_MODULE_125__, __WEBPACK_EXTERNAL_MODULE_126__, __WEBPACK_EXTERNAL_MODULE_135__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(134);

/***/ }),

/***/ 94:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_94__;

/***/ }),

/***/ 109:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_109__;

/***/ }),

/***/ 114:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_114__;

/***/ }),

/***/ 124:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_124__;

/***/ }),

/***/ 125:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_125__;

/***/ }),

/***/ 126:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_126__;

/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(94);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetExtensionNg = __webpack_require__(124);
	
	var _libBbWidgetExtensionNg2 = _interopRequireDefault(_libBbWidgetExtensionNg);
	
	var _libBbEventBusNg = __webpack_require__(125);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _libBbWidgetNg = __webpack_require__(109);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _modelBbPaymentNg = __webpack_require__(126);
	
	var _modelBbPaymentNg2 = _interopRequireDefault(_modelBbPaymentNg);
	
	var _libBbStorageNg = __webpack_require__(135);
	
	var _libBbStorageNg2 = _interopRequireDefault(_libBbStorageNg);
	
	var _controller = __webpack_require__(136);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	var _defaultHooks = __webpack_require__(139);
	
	var defaultHooks = _interopRequireWildcard(_defaultHooks);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module widget-bb-payment-ng
	 *
	 * @description
	 * Payment widget.
	 *
	 * @usage
	 *  <div ng-controller="PaymentController as $ctrl">
	 *    <div ng-if="$ctrl.payment.from">
	 *      <h4 ng-bind="$ctrl.payment.from.name"></h4>
	 *      <p ng-bind="$ctrl.payment.from.identifier"></p>
	 *      <ui-bb-format-amount
	 *        class="pull-right"
	 *        amount="$ctrl.payment.from.amount"
	 *        currency="$ctrl.payment.from.currency">
	 *      </ui-bb-format-amount>
	 *    </div>
	 *  </div>
	 */
	var hooksKey = 'widget-bb-payment-ng:hooks';
	
	exports.default = _vendorBbAngular2.default.module('widget-bb-payment-ng', [_modelBbPaymentNg2.default, _libBbWidgetNg2.default, _libBbEventBusNg2.default, _libBbStorageNg2.default]).factory(hooksKey, (0, _libBbWidgetExtensionNg2.default)(defaultHooks)).controller('PaymentController', [_modelBbPaymentNg.modelPaymentKey, _libBbEventBusNg.eventBusKey, hooksKey, '$q', _libBbStorageNg.bbStorageServiceKey, _libBbWidgetNg.widgetKey,
	/* into */
	_controller2.default]).name;
	
	/**
	 * @typedef {object} AccountView
	 * @property {string} id The internal account identifier
	 * @property {string} name The account's name, suitable for display to users
	 * @property {?string} identifier The identifier of the account from the user's perspective
	 * @property {?string} amount The most important associated value to be displayed
	 * @property {?string} currency Account currency
	 */
	
	/**
	 * @typedef {object} Currency
	 * @property {string} id The internal identifier
	 * @property {string} name Currency name, suitable for display to users
	 */
	
	/**
	 * @typedef {object} Rate
	 * @property {number} rate
	 */
	
	/**
	 * @typedef {object} AccountIdentification
	 * @property {string} scheme Identification of the product
	 * @property {string} identification Unique identification of the product
	 */
	
	/**
	 * @typedef {object} Schedule
	 * @description
	 * Schedule for recurring transfer. Mandatory if paymentMode is RECURRING
	 *
	 * @property {string} nonWorkingDayExecutionStrategy
	 * @property {string} transferFrequency - Denotes frequency type of transfer
	 * @property {number} on - Denotes day on which transfer should be executed
	 * @property {number} repeat - Number of transfer to be executed
	 * @property {number} every - Indicates skip interval of transfer
	 * @property {date} startDate - When to start executing the schedule
	 * @property {date} endDate - When to stop transfers
	 */
	
	/**
	 * @typedef {object} Payment
	 * @property {?AccountIdentification} debitAccountIdentification
	 * @property {?AccountIdentification} creditAccountIdentification
	 * @property {number} amount
	 * @property {number} currency
	 * @property {string} date
	 * @property {string} paymentReference
	 * @property {string} description
	 * @property {string} paymentMode
	 * @property {Schedule} schedule
	 */

/***/ }),

/***/ 135:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_135__;

/***/ }),

/***/ 136:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.default = PaymentController;
	
	var _message = __webpack_require__(137);
	
	var _constants = __webpack_require__(138);
	
	function PaymentController(model, bus, hooks, Promise, bbStorage, widget) {
	  var $ctrl = this;
	
	  var singleTransfer = model.singleTransfer,
	      endingTypes = model.endingTypes;
	
	  /**
	   * @type {Promise} - loads the accounts from
	   */
	
	  var accountsFromRequest = void 0;
	
	  /**
	   * @type {Promise} - loads the accounts to
	   */
	  var accountsToRequest = void 0;
	
	  /**
	   * @type {Payment} payment - payment model, which converts to params on a payment request
	   */
	  var payment = void 0;
	
	  /**
	   * Payment preferences set in the widget preferences
	   */
	  var paymentPreferences = model.getPaymentPreferences();
	
	  /**
	   * @description
	   * Method to store the payment in sync pref
	   *
	   * @param {Payment} paymentData
	   * @type {function}
	   * @name PaymentController#storePayment
	   * @returns {void}
	   */
	  var storePayment = function storePayment(paymentData) {
	    model.storePayment(paymentData);
	  };
	
	  /**
	   * @description
	   * Method to store accounts in sync pref
	   *
	   * @param {Payment} paymentData
	   * @type {function}
	   * @name PaymentController#storeAccounts
	   * @returns {void}
	   */
	  var storeAccounts = function storeAccounts(accounts) {
	    model.storeAccounts(accounts);
	  };
	
	  var initPayment = function initPayment() {
	    payment = model.getPayment();
	
	    if (!payment) {
	      payment = model.newPayment();
	
	      // Update view model fields
	      Object.assign(payment, {
	        amount: {
	          value: null,
	          currency: null
	        },
	        endingType: endingTypes.NEVER,
	        from: null,
	        to: null
	      });
	    } else {
	      // when retrieved from sync pref as JSON, string to be converted to date
	      payment.schedule.startDate = new Date(payment.schedule.startDate);
	      payment.schedule.endDate = new Date(payment.schedule.endDate);
	    }
	
	    $ctrl.payment = payment;
	  };
	
	  /**
	   * Updates the accounts list for the from selector.
	   *
	   * @inner
	   * @name updateAccountsFrom
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateAccountsFrom = function updateAccountsFrom() {
	    $ctrl.accountsLoading = true;
	
	    accountsFromRequest = model.getAccountsFrom().then(function (accounts) {
	      $ctrl.accountsLoading = false;
	      $ctrl.accountsFrom = accounts;
	
	      if (!$ctrl.payment.from && accounts) {
	        $ctrl.payment.from = accounts[0];
	      }
	    }).catch(function (modelError) {
	      $ctrl.accountsLoading = false;
	      $ctrl.accountsLoadError = (0, _message.createAccountsLoadErrorMessage)(modelError);
	    });
	
	    return accountsFromRequest;
	  };
	
	  /**
	   * @description
	   * Proxy function to method on the model
	   *
	   * @inner
	   * @name getAccountsTo
	   * @type {function}
	   * @param {String} Filter account list with debitAccountId param
	   * @returns {Promise.<AccountView[]>} A Promise with flat accounts list.
	   */
	  var getAccountsTo = function getAccountsTo(debitAccountId) {
	    return model.getAccountsTo(debitAccountId);
	  };
	
	  /**
	   * @description
	   * Proxy function to method on the model
	   *
	   * @inner
	   * @name getExternals
	   * @type {function}
	   * @returns {Promise.<AccountView[]>} A Promise with flat accounts list.
	   */
	  var getExternals = function getExternals() {
	    return model.getExternals();
	  };
	
	  /**
	   * @description
	   * Updates the accounts list for the to selector. Uses account from as a filter for model method
	   *
	   * @inner
	   * @name updateAccountsTo
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateAccountsTo = function updateAccountsTo() {
	    $ctrl.accountsLoading = true;
	    var debitAccount = payment.from || {};
	    var creditAccount = payment.to || {};
	    if (debitAccount.id && (creditAccount.id === debitAccount.id || !debitAccount.externalTransferAllowed && (creditAccount.external || creditAccount.isNew))) {
	      delete payment.to;
	    }
	
	    var accountsTo = hooks.processAccountsTo(debitAccount, getAccountsTo, getExternals);
	
	    accountsToRequest = Promise.resolve(accountsTo).then(function (accounts) {
	      $ctrl.accountsLoading = false;
	      $ctrl.accountsTo = hooks.groupAccountsTo(accounts);
	    }, function (modelError) {
	      $ctrl.accountsLoading = false;
	      $ctrl.accountsLoadError = (0, _message.createAccountsLoadErrorMessage)(modelError);
	    });
	
	    return accountsToRequest;
	  };
	
	  /**
	   * @description
	   * Handles the account type select action (for extensions)
	   *
	   * @name PaymentController#processSelectProductType
	   * @type {function}
	   * @param {boolean} isCreditProduct
	   * @returns {Promise}
	   */
	  var processSelectProductType = function processSelectProductType(isCreditProduct) {
	    var request = isCreditProduct ? accountsFromRequest : accountsToRequest;
	    var accounts = isCreditProduct ? $ctrl.accountsFrom : $ctrl.accountsTo;
	    var getEventName = function getEventName(name) {
	      return (isCreditProduct ? 'ACCOUNT_FROM_' : 'ACCOUNT_TO_') + name;
	    };
	
	    bus.publish(_constants.Message[getEventName('LOAD')]);
	
	    return request.then(function () {
	      bus.publish(_constants.Message[getEventName('DONE')]);
	
	      // Store state of last selection
	      processSelectProductType.isAccountsFrom = isCreditProduct;
	      processSelectProductType.accounts = Object.assign({}, accounts);
	
	      storePayment(payment);
	      storeAccounts({
	        isAccountsFrom: isCreditProduct,
	        accounts: accounts
	      });
	
	      bus.publish(_constants.Message.PAYMENT_ACCOUNT_SELECT);
	    }).catch(function () {
	      bus.publish(_constants.Message[getEventName('FAILED')]);
	    });
	  };
	
	  /**
	   * getCurrencies caches currencies
	   *
	   * @inner
	   * @name getCurrencies
	   * @type {function}
	   * @returns {Promise.<Currency[]>}
	   */
	  var originCurrencies = void 0;
	  var getCurrencies = function getCurrencies() {
	    var defaultItem = {
	      id: '',
	      crossCurrencyAllowed: true
	    };
	    var currentItem = payment.from || defaultItem;
	
	    if (!currentItem.crossCurrencyAllowed) {
	      return Promise.resolve([]);
	    }
	
	    return Promise.resolve(originCurrencies || model.getCurrencies().then(function (currencies) {
	      return originCurrencies = currencies;
	    })
	    // Return empty array if currencies cannot be loaded
	    .catch(function () {
	      return [];
	    }));
	  };
	
	  /**
	   * Updates the currency list available for the payment and a value.
	   *
	   * @inner
	   * @name updateCurrencyList
	   * @type {function}
	   * @returns {object} Promise for Currencies get request
	   */
	  var updateCurrencyList = function updateCurrencyList() {
	    return getCurrencies().then(function (currencies) {
	      var debitAccount = payment.from;
	      var currentCurrency = void 0;
	      $ctrl.currencies = [].concat(currencies);
	
	      if (debitAccount) {
	        currentCurrency = currencies.find(function (currency) {
	          return currency.name === debitAccount.currency;
	        });
	        if (!currentCurrency) {
	          currentCurrency = debitAccount.currency;
	          $ctrl.currencies.push({ name: currentCurrency });
	        } else {
	          currentCurrency = currentCurrency.name;
	        }
	      } else {
	        currentCurrency = currencies[0] && currencies[0].name || '';
	      }
	
	      var sortByCurrentCurrency = function sortByCurrentCurrency(a, b) {
	        if (a.name === currentCurrency) {
	          return -1;
	        } else if (b.name === currentCurrency) {
	          return 1;
	        }
	        return 0;
	      };
	
	      $ctrl.currencies.sort(sortByCurrentCurrency);
	
	      payment.amount = {
	        currency: currentCurrency,
	        value: payment.amount.value
	      };
	    });
	  };
	
	  /**
	   * @description
	   * Retrieves exchange rate for two currencies.
	   *
	   * @name PaymentController#updateRate
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateRate = function updateRate(currencyFrom, currencyTo) {
	    delete $ctrl.rate;
	    var areDifferentCurrencies = currencyFrom && currencyTo && currencyTo !== currencyFrom;
	    if (!areDifferentCurrencies) {
	      return Promise.resolve();
	    }
	
	    return model.getRate({
	      currencyFrom: currencyFrom,
	      currencyTo: currencyTo
	    }).then(function (_ref) {
	      var rate = _ref.rate;
	
	      $ctrl.rate = rate;
	    });
	  };
	
	  /**
	   * @description
	   * Account from change handler.
	   *
	   * @see updateCurrencyList
	   * @see updateAccountsTo
	   *
	   * @name PaymentController#onAccountFromChange
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var onAccountFromChange = function onAccountFromChange() {
	    updateCurrencyList();
	    return updateAccountsTo();
	  };
	
	  /**
	   * @description
	   * Sets the accounts read from sync preferences
	   *
	   * @inner
	   * @name setupAccounts
	   * @type {function}
	   */
	  var setupAccounts = function setupAccounts() {
	    var data = model.getAccounts() || {};
	    processSelectProductType.accounts = hooks.groupAccountsTo(data.accounts);
	    processSelectProductType.isAccountsFrom = data.isAccountsFrom;
	  };
	
	  /**
	   * @description
	   * Sets the saveNewContact flag read from sync preferences
	   *
	   * @inner
	   * @name setupSaveContactFlag
	   * @type {function}
	   */
	  var setupSaveContactsFlag = function setupSaveContactsFlag() {
	    return bbStorage.getItem(_constants.Preference.SAVE_CONTACT).then(function (flag) {
	      $ctrl.saveNewContact = flag;
	    });
	  };
	
	  /**
	   * @description
	   * Resets payment model, updates accounts and currency lists
	   *
	   * @type {function}
	   * @name PaymentController#resetPayment
	   * @returns {Promise.<void>}
	   */
	  var resetPayment = function resetPayment() {
	    initPayment();
	
	    return updateAccountsFrom().then(updateCurrencyList).then(updateAccountsTo).then(setupAccounts).then(setupSaveContactsFlag);
	  };
	
	  /**
	   * @description
	   * Adds subscriptions to bus events
	   *
	   * @inner
	   * @name bindEvents
	   * @type {function}
	   */
	  var bindEvents = function bindEvents() {
	    bus.subscribe(_constants.Message.PAYMENT_DONE, function () {
	      model.storePayment(null);
	      if (!$ctrl.preventResetOnDone) {
	        resetPayment();
	      }
	    });
	
	    bus.subscribe(_constants.Message.PAYMENT_REVIEW_STEP, function (paymentData) {
	      payment = paymentData;
	      $ctrl.payment = payment;
	    });
	
	    bus.subscribe(_constants.Message.ACCOUNT_SELECTED, function (data) {
	      if (data.isAccountsFrom) {
	        payment.from = data.account;
	        onAccountFromChange();
	      } else {
	        payment.to = data.account;
	      }
	    });
	
	    bus.subscribe(_constants.Message.CONTACT_CREATE_DONE, function () {
	      updateAccountsTo();
	    });
	
	    bus.subscribe(_constants.Message.CONTACT_UPDATE_DONE, function () {
	      updateAccountsTo();
	    });
	
	    bus.subscribe(_constants.Message.CONTACT_DELETE_DONE, function () {
	      updateAccountsTo();
	    });
	
	    bbStorage.subscribe(_constants.Preference.SAVE_CONTACT, function (flag) {
	      $ctrl.saveNewContact = flag;
	    });
	
	    /**
	     * This event (cxp.item.loaded) is deprecated in Mobile SDK version > 3.0
	     * and will be removed with the update to widget collection 3 (WC3)
	     */
	    bus.publish('cxp.item.loaded', {
	      id: widget.getId()
	    });
	
	    bus.publish('bb.item.loaded', {
	      id: widget.getId()
	    });
	  };
	
	  var clearMessage = function clearMessage() {
	    $ctrl.paymentSubmitMessage = null;
	  };
	
	  /**
	   * AngularJS Lifecycle hook used to initialize the controller
	   *
	   * @type {function}
	   * @name PaymentController#$onInit
	   * @returns {Promise.<void>}
	   */
	  var $onInit = function $onInit() {
	    return resetPayment().then(bindEvents).then(function () {
	      $ctrl.loadingMsgDelay = _constants.LOADING_MSG_DELAY;
	    });
	  };
	
	  /**
	   * @name onContactCreateStart
	   *
	   * @description
	   * Handles contact create start
	   *
	   * @inner
	   * @type {function}
	   * @returns {*}
	   */
	  var onContactCreateStart = function onContactCreateStart() {
	    bus.publish(_constants.Message.CONTACT_CREATE_START);
	  };
	
	  /**
	   * @name onContactCreateDone
	   *
	   * @description
	   * Handles contact create done
	   *
	   * @inner
	   * @param {object} form Contact form object
	   * @type {function}
	   */
	  var onContactCreateDone = function onContactCreateDone() {
	    bus.publish(_constants.Message.CONTACT_CREATE_DONE);
	  };
	
	  /**
	   * @name onContactCreateFail
	   *
	   * @description
	   * Handles contact create fail
	   *
	   * @inner
	   * @param {object} err Error object
	   * @type {function}
	   */
	  var onContactCreateFail = function onContactCreateFail(error) {
	    bus.publish(_constants.Message.CONTACT_CREATE_FAILED, { error: error });
	  };
	
	  /**
	   * @name PaymentController#saveContact
	   *
	   * @description
	   * saves a new contact using widget's model
	   *
	   * @param {object} form Contact form object
	   * @type {function}
	   * @return {Promise}
	   */
	  var saveContact = function saveContact(contact) {
	    onContactCreateStart();
	    return model.createContact(contact).then(function () {
	      return onContactCreateDone();
	    }).catch(onContactCreateFail);
	  };
	
	  /**
	   * @description
	   * Checks if payment data is valid
	   * It cannot have recurring payment details if recurring payment is disabled by admin
	   *
	   * @name PaymentController#paymentDataValid
	   * @type {function}
	   * @param {Payment} paymentData
	   * @returns {Boolean}
	   */
	  var paymentDataValid = function paymentDataValid(paymentData) {
	    return $ctrl.paymentPreferences.recurring || !$ctrl.paymentPreferences.recurring && ((paymentData.schedule || {}).transferFrequency || {}).value === model.singleTransfer.value;
	  };
	
	  /**
	   * @description
	   * Checks if layout should show 'Urgent payment' switcher
	   *
	   * @name PaymentController#canSelectUrgentPayment
	   * @type {function}
	   * @param {object} debitAccount selected
	   * @returns {boolean}
	   */
	  var canSelectUrgentPayment = function canSelectUrgentPayment() {
	    var debitAccount = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    return paymentPreferences.urgent && debitAccount && !!debitAccount.urgentTransferAllowed;
	  };
	
	  /**
	   * @description
	   * Sends payment model to endpoint
	   *
	   * @name PaymentController#makePayment
	   * @type {function}
	   * @param {Payment} paymentData
	   * @returns {Promise.<void>}
	   */
	  var makePayment = function makePayment(paymentData) {
	    $ctrl.paymentLoading = true;
	
	    var debitAccountId = paymentData.from.id,
	        _paymentData$to = paymentData.to,
	        creditAccountId = _paymentData$to.id,
	        creditAccountIdentifier = _paymentData$to.identifier,
	        name = _paymentData$to.name,
	        isExternal = _paymentData$to.external,
	        _paymentData$amount = paymentData.amount,
	        amount = _paymentData$amount.value,
	        currency = _paymentData$amount.currency,
	        description = paymentData.description,
	        endingType = paymentData.endingType,
	        urgent = paymentData.urgent,
	        additions = paymentData.additions;
	
	
	    var contact = {
	      name: name,
	      accounts: [{
	        IBAN: creditAccountIdentifier
	      }]
	    };
	
	    // For transfers in the same FI account ID scheme is used
	    var creditAccountIdentification = {
	      scheme: 'ID',
	      identification: creditAccountId
	    };
	
	    // For transfers to other party,
	    // counterparty name and account IBAN must be included
	    if (isExternal || !creditAccountId) {
	      creditAccountIdentification = {
	        counterpartyName: name,
	        scheme: 'IBAN',
	        identification: creditAccountIdentifier
	      };
	    }
	
	    // add new contact (if needed)
	    if ($ctrl.saveNewContact && contact.name) {
	      saveContact(contact).then(function () {
	        $ctrl.saveNewContact = false;
	      });
	    }
	
	    bus.publish(_constants.Message.PAYMENT_START);
	    return new Promise(function (resolve, reject) {
	      // make sure we are processing single payment if recurring is disabled via preferences
	      if (!paymentDataValid(paymentData)) {
	        reject({ code: 'E_USER' });
	        return;
	      }
	      resolve();
	    }).then(function () {
	      return model.makePayment(Object.assign({
	        debitAccountIdentification: {
	          scheme: 'ID',
	          identification: debitAccountId
	        },
	        creditAccountIdentification: creditAccountIdentification,
	        amount: amount,
	        currency: currency,
	        description: description
	      }, model.getPaymentSchedule(paymentData, endingType, hooks.getRecurringTransactionDay), paymentPreferences.urgent ? { urgent: !!urgent } : {}, additions != null && (typeof additions === 'undefined' ? 'undefined' : _typeof(additions)) === 'object' ? { additions: additions } : {}));
	    }).then(function () {
	      $ctrl.paymentLoading = false;
	      bus.publish(_constants.Message.PAYMENT_FORM_STEP);
	      bus.publish(_constants.Message.PAYMENT_DONE);
	    }, function (error) {
	      $ctrl.paymentLoading = false;
	      $ctrl.paymentSubmitMessage = (0, _message.createPaymentErrorMessage)(error);
	      bus.publish(_constants.Message.PAYMENT_FORM_STEP);
	      bus.publish(_constants.Message.PAYMENT_FAILED);
	      return Promise.reject();
	    });
	  };
	
	  /**
	   * @description
	   * Checks if layout should show 'save beneficiary' switcher
	   *
	   * @name PaymentController#canSaveNewContact
	   * @type {function}
	   * @param {object} beneficiary  Recipient data
	   * @param {array} creditAccounts Credit accounts and contacts collection
	   * @returns {boolean}
	   */
	  var canSaveNewContact = function canSaveNewContact(beneficiary) {
	    var creditAccounts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
	
	    var accounts = creditAccounts || []; // to cover null arg issue
	    var newBeneficiaryAdded = !!(beneficiary && beneficiary.name && beneficiary.identifier);
	    var itIsNotInCollection = newBeneficiaryAdded && !accounts.filter(function (item) {
	      return item.name === beneficiary.name && item.identifier === beneficiary.identifier;
	    }).length;
	    var canBeSaved = newBeneficiaryAdded && itIsNotInCollection;
	
	    // reset switcher before any further appearance
	    if (canBeSaved === false) {
	      $ctrl.saveNewContact = false;
	    }
	    return canBeSaved;
	  };
	
	  /**
	   * @description
	   * Sets the flag that indicates if the contact should be saved when
	   * submitting a payment
	   *
	   * @name PaymentController#setSaveContactFlag
	   * @type {function}
	   * @returns {void}
	   */
	  var setSaveContactFlag = function setSaveContactFlag(flag) {
	    $ctrl.saveNewContact = flag;
	  };
	
	  /**
	   * @description
	   * Method to store the saveNewContact flag in the sync pref
	   *
	   * @name PaymentController#storeSaveContactFlag
	   * @type {function}
	   * @returns {void}
	   */
	  var storeSaveContactFlag = function storeSaveContactFlag() {
	    bbStorage.setItem(_constants.Preference.SAVE_CONTACT, $ctrl.saveNewContact);
	  };
	
	  /**
	   * @name PaymentController
	   * @ngkey PaymentController
	   * @type {object}
	   * @description
	   * Payment widget controller.
	   */
	  Object.assign($ctrl, {
	    /**
	     * @description
	     * List of accounts to payment from
	     *
	     * @name PaymentController#accountsFrom
	     * @type {AccountsView[]} accountsFrom
	     */
	    accountsFrom: null,
	    /**
	     * @description
	     * List of accounts to payment to
	     *
	     * @name PaymentController#accountsTo
	     * @type {AccountsView[]} accountsTo
	     */
	    accountsTo: null,
	    /**
	     * @description
	     * List of external contacts that can be populated in credit account list
	     *
	     * @name PaymentController#externalsTo
	     * @type {AccountsView[]} externalsTo
	     */
	    externalsTo: null,
	    /**
	     * @description
	     * Flag that tells if contact list is being updated
	     *
	     * @name PaymentController#accountsLoading
	     * @type {boolean} accountsLoading
	     */
	    accountsLoading: false,
	    loadingMsgDelay: 0,
	    /**
	     * @description
	     * Store model error key which can be used for translation in the extension
	     *
	     * Possible values:
	     * - account.model.error.auth
	     * - account.model.error.connectivity
	     * - account.model.error.user
	     * - account.model.error.unexpected
	     *
	     * @name PaymentController#accountsLoadError
	     * @type {string}
	     */
	    accountsLoadError: null,
	    onAccountFromChange: onAccountFromChange,
	    /**
	     * @description
	     * List of currencies available for payment
	     *
	     * @name PaymentController#currencies
	     * @type {Currency[]}
	     */
	    currencies: null,
	    /**
	     * @description
	     * Rate defined for cross-currency payments
	     *
	     * @name PaymentController#rate
	     * @type {Rate}
	     */
	    rate: null,
	    /**
	     * @description
	     * Payment object, containing info - from account, to account, amount, ...
	     *
	     * @name PaymentController#payment
	     * @type {Payment} payment
	     */
	    payment: payment,
	    /**
	     * @description
	     * Flag that tells if new payment is being processed
	     *
	     * @name PaymentController#paymentLoading
	     * @type {boolean} paymentLoading
	     */
	    paymentLoading: false,
	    /**
	     * @description
	     * Store model error key which can be used for translation in the extension
	     *
	     * Possible values:
	     * - payment.model.error.auth
	     * - payment.model.error.connectivity
	     * - payment.model.error.user
	     * - payment.model.error.unexpected
	     *
	     * @name PaymentController#paymentSubmitMessage
	     * @type {string}
	     */
	    paymentSubmitMessage: null,
	    clearMessage: clearMessage,
	    resetPayment: resetPayment,
	    paymentDataValid: paymentDataValid,
	    makePayment: makePayment,
	    processSelectProductType: processSelectProductType,
	    updateRate: updateRate,
	    storePayment: storePayment,
	    storeAccounts: storeAccounts,
	    saveContact: saveContact,
	    canSaveNewContact: canSaveNewContact,
	    setSaveContactFlag: setSaveContactFlag,
	    storeSaveContactFlag: storeSaveContactFlag,
	    canSelectUrgentPayment: canSelectUrgentPayment,
	    $onInit: $onInit,
	    // constants
	    singleTransfer: singleTransfer,
	    endingTypes: endingTypes,
	    paymentPreferences: paymentPreferences
	  });
	}

/***/ }),

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.createPaymentErrorMessage = exports.paymentErrorMessages = exports.createAccountsLoadErrorMessage = exports.accountsErrorMessages = undefined;
	
	var _accountsErrorMessage, _paymentErrorMessages;
	
	var _libBbModelErrors = __webpack_require__(114);
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var accountsErrorMessages = exports.accountsErrorMessages = (_accountsErrorMessage = {}, _defineProperty(_accountsErrorMessage, _libBbModelErrors.E_AUTH, 'account.model.error.auth'), _defineProperty(_accountsErrorMessage, _libBbModelErrors.E_CONNECTIVITY, 'account.model.error.connectivity'), _defineProperty(_accountsErrorMessage, _libBbModelErrors.E_USER, 'account.model.error.user'), _defineProperty(_accountsErrorMessage, _libBbModelErrors.E_UNEXPECTED, 'account.model.error.unexpected'), _accountsErrorMessage);
	
	/**
	 * @description
	 * Create i18n error key from error model based on error message code
	 *
	 * @inner
	 * @type {function}
	 * @param {ModelError} modelError
	 * @returns {ErrorMessage}
	 */
	var createAccountsLoadErrorMessage = exports.createAccountsLoadErrorMessage = function createAccountsLoadErrorMessage(modelError) {
	  return {
	    messageKey: accountsErrorMessages[modelError.code] || accountsErrorMessages[_libBbModelErrors.E_UNEXPECTED],
	    type: 'warning'
	  };
	};
	
	var paymentErrorMessages = exports.paymentErrorMessages = (_paymentErrorMessages = {}, _defineProperty(_paymentErrorMessages, _libBbModelErrors.E_AUTH, 'payment.model.error.auth'), _defineProperty(_paymentErrorMessages, _libBbModelErrors.E_CONNECTIVITY, 'payment.model.error.connectivity'), _defineProperty(_paymentErrorMessages, _libBbModelErrors.E_USER, 'payment.model.error.user'), _defineProperty(_paymentErrorMessages, _libBbModelErrors.E_UNEXPECTED, 'payment.model.error.unexpected'), _paymentErrorMessages);
	
	/**
	 * @description
	 * Create i18n error key from error model based on error message code
	 *
	 * @inner
	 * @type {function}
	 * @param {ModelError} modelError
	 * @returns {ErrorMessage}
	 */
	var createPaymentErrorMessage = exports.createPaymentErrorMessage = function createPaymentErrorMessage(modelError) {
	  return {
	    messageKey: paymentErrorMessages[modelError.code] || paymentErrorMessages[_libBbModelErrors.E_UNEXPECTED],
	    type: 'warning'
	  };
	};
	
	/**
	 * @typedef {object} ErrorMessage
	 * @property {string} messageKey
	 */
	
	/**
	 * @typedef {object} ModelError
	 * @property {ErrorCode} code
	 */

/***/ }),

/***/ 138:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Widget events enum
	 * @type {object}
	 */
	var Event = {
	  ACCOUNT_SELECTED: 'bb.event.account.selected',
	  ACCOUNT_FROM_LOAD: 'bb.event.account.from.load',
	  ACCOUNT_FROM_DONE: 'bb.event.account.from.done',
	  ACCOUNT_FROM_FAILED: 'bb.event.account.from.failed',
	  ACCOUNT_TO_LOAD: 'bb.event.account.to.load',
	  ACCOUNT_TO_DONE: 'bb.event.account.to.done',
	  ACCOUNT_TO_FAILED: 'bb.event.account.to.failed',
	  CONTACT_CREATE_START: 'bb.event.contact.create.start',
	  CONTACT_CREATE_DONE: 'bb.event.contact.create.done',
	  CONTACT_CREATE_FAILED: 'bb.event.contact.create.failed',
	  CONTACT_DELETE_DONE: 'bb.event.contact.delete.done',
	  CONTACT_UPDATE_DONE: 'bb.event.contact.update.done',
	  PAYMENT_START: 'bb.event.payment.start',
	  PAYMENT_DONE: 'bb.event.payment.done',
	  PAYMENT_FAILED: 'bb.event.payment.failed',
	  PAYMENT_FORM_STEP: 'bb.event.payment.form.step',
	  PAYMENT_REVIEW_STEP: 'bb.event.payment.review.step'
	};
	
	/**
	 * Widget actions enum
	 * @type {object}
	 */
	var Action = {
	  PAYMENT_ACCOUNT_SELECT: 'bb.action.account.select'
	};
	
	/**
	 * Widget messages enum
	 * @name Message
	 * @type {object}
	 */
	var Message = exports.Message = Object.assign({}, Action, Event);
	
	/**
	 * Widget preferences enum
	 * @name Preference
	 * @type {object}
	 */
	var Preference = exports.Preference = {
	  SAVE_CONTACT: 'bb.payment.save.contact'
	};
	
	/**
	 * Number of ms after which loading message should appear
	 * for actions that change form content after initialization
	 * @name LOADING_MSG_DELAY
	 * @type {number}
	 */
	var LOADING_MSG_DELAY = exports.LOADING_MSG_DELAY = 500;

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.groupAccountsTo = groupAccountsTo;
	exports.processAccountsTo = processAccountsTo;
	exports.getRecurringTransactionDay = getRecurringTransactionDay;
	/* eslint-disable import/prefer-default-export */
	/* eslint no-unused-vars: ["error", { "args": "none" }] */
	
	/**
	 * @name Hooks
	 * @type {object}
	 *
	 * @description
	 * Hooks for widget-bb-payment-ng
	 */
	
	/**
	 * @name Hooks#groupAccountsTo
	 * @type {function}
	 *
	 * @description
	 * Hook for grouping accounts. Used only for Mobile.
	 *
	 * @param {array.<object>} accountsTo List of beneficiary accounts
	 * @returns {array.<object>} List of grouped accounts
	 */
	function groupAccountsTo(accountsTo) {
	  return accountsTo;
	}
	
	/**
	 * @name Hooks#processAccountsTo
	 * @type {function}
	 *
	 * @description
	 * Hook for processing account list in 'to' field (credit).
	 * Assigned to [$ctrl.accountsTo]{@link PaymentController#AccountView}
	 *
	 * @param {ProductKind} debitAccount Selected debit account (can be null)
	 * @param {function} getCreditAccounts Function to retrieve all credit accounts
	 * @param {function} getExternalContacts Function to retrieve all external contacts
	 * formatted like Product kind
	 * @returns {Promise.<any[]>} Promise that retrieves array of accounts.
	 */
	function processAccountsTo(debitAccount, getCreditAccounts, getExternalContacts) {
	  return getCreditAccounts(debitAccount.id || null);
	}
	
	/**
	 * @name Hooks#getRecurringTransactionDay
	 * @type {function}
	 *
	 * @description
	 * Denotes day on which transfer should be executed.
	 * For weekly it will be 1..7 indicating weekday.
	 * For monthly it will be 1..31 indicating day of month.
	 * For yearly it will be 1..12 indicating month of the year
	 * Assigned to [$ctrl.schedule.on]{@link PaymentController#makePayment}
	 *
	 * @param {date} startDate Start date of recurring payment
	 * @param {object} transferFrequency Recurring frequency
	 * @returns {number}
	 */
	function getRecurringTransactionDay(startDate, transferFrequency) {
	  return startDate.getDate();
	}

/***/ })

/******/ })
});
;
//# sourceMappingURL=widget-bb-payment-ng.js.map