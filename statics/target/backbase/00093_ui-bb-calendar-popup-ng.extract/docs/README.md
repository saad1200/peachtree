# ui-bb-calendar-popup-ng


Version: **1.1.3**

UI datepicker popup component

## Imports

* vendor-bb-angular
* vendor-bb-uib-datepicker
* vendor-bb-uib-datepicker-popup
* vendor-bb-uib-position

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbCalendarPopupKey from 'ui-bb-calendar-popup-ng';

export const dependencyKeys = [
  uiBbCalendarPopupKey,
];

// file: templates/template.ng.html
<ui-bb-calendar-popup
  data-date="$ctrl.date"
  data-config="$ctrl.calendarPopupConfig"
  data-disabled="false">
</ui-bb-calendar-popup>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **ui-bb-calendar-popup-ng**<br/>    <a href="#ui-bb-calendar-popup-nguiBbCalendarPopupController">uiBbCalendarPopupController()</a><br/>    <a href="#ui-bb-calendar-popup-ngformatViewValue">formatViewValue(value)</a><br/>    <a href="#ui-bb-calendar-popup-ngtoggle">toggle()</a><br/>    <a href="#ui-bb-calendar-popup-ng$onChanges">$onChanges(changesObject)</a><br/>    <a href="#ui-bb-calendar-popup-ng$onInit">$onInit()</a><br/>

## Exports

### <a name="default"></a>*default*

The angular module name

**Type:** *String*


---

## uiBbCalendarPopupComponent


| Property | Type | Description |
| :-- | :-- | :-- |
| date | String | Date model |
| config | Object | Configuration object |
| disabled | Boolean | Defines whether or not component is disabled |

---

### <a name="ui-bb-calendar-popup-nguiBbCalendarPopupController"></a>*uiBbCalendarPopupController()*

Calendar popup controller

---

### <a name="ui-bb-calendar-popup-ngformatViewValue"></a>*formatViewValue(value)*

Displays translation of "calendar.label.today" key or "today" string in the input field
when today date is selected

| Parameter | Type | Description |
| :-- | :-- | :-- |
| value | String | Date value of type string |

##### Returns

String - **

---

### <a name="ui-bb-calendar-popup-ngtoggle"></a>*toggle()*

Toggling open/close state of the calendar

---

### <a name="ui-bb-calendar-popup-ng$onChanges"></a>*$onChanges(changesObject)*

Adjusts selected date and minDate of current control
to the value passed through a binding

| Parameter | Type | Description |
| :-- | :-- | :-- |
| changesObject | Object | Object containing changes in one-way bindings |

---

### <a name="ui-bb-calendar-popup-ng$onInit"></a>*$onInit()*

Set default value on icon click
