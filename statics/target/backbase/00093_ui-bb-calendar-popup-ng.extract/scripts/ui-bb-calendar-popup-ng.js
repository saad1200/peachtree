(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("vendor-bb-uib-datepicker"), require("vendor-bb-uib-position"), require("vendor-bb-uib-datepicker-popup"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-calendar-popup-ng", ["vendor-bb-angular", "vendor-bb-uib-datepicker", "vendor-bb-uib-position", "vendor-bb-uib-datepicker-popup"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-calendar-popup-ng"] = factory(require("vendor-bb-angular"), require("vendor-bb-uib-datepicker"), require("vendor-bb-uib-position"), require("vendor-bb-uib-datepicker-popup"));
	else
		root["ui-bb-calendar-popup-ng"] = factory(root["vendor-bb-angular"], root["vendor-bb-uib-datepicker"], root["vendor-bb-uib-position"], root["vendor-bb-uib-datepicker-popup"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_16__, __WEBPACK_EXTERNAL_MODULE_17__, __WEBPACK_EXTERNAL_MODULE_18__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(15);

/***/ }),
/* 1 */,
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _vendorBbUibDatepicker = __webpack_require__(16);
	
	var _vendorBbUibDatepicker2 = _interopRequireDefault(_vendorBbUibDatepicker);
	
	var _vendorBbUibPosition = __webpack_require__(17);
	
	var _vendorBbUibPosition2 = _interopRequireDefault(_vendorBbUibPosition);
	
	var _vendorBbUibDatepickerPopup = __webpack_require__(18);
	
	var _vendorBbUibDatepickerPopup2 = _interopRequireDefault(_vendorBbUibDatepickerPopup);
	
	var _calendarPopup = __webpack_require__(19);
	
	var _calendarPopup2 = _interopRequireDefault(_calendarPopup);
	
	var _calendarPopup3 = __webpack_require__(20);
	
	var _calendarPopup4 = _interopRequireDefault(_calendarPopup3);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module ui-bb-calendar-popup-ng
	 * @description
	 * UI datepicker popup component
	 *
	 * @example
	 * // In an extension:
	 * // file: scripts/index.js
	 * import uiBbCalendarPopupKey from 'ui-bb-calendar-popup-ng';
	 *
	 * export const dependencyKeys = [
	 *   uiBbCalendarPopupKey,
	 * ];
	 *
	 * // file: templates/template.ng.html
	 * <ui-bb-calendar-popup
	 *   data-date="$ctrl.date"
	 *   data-config="$ctrl.calendarPopupConfig"
	 *   data-disabled="false">
	 * </ui-bb-calendar-popup>
	 */
	
	var dependencyKeys = [_vendorBbUibPosition2.default, _vendorBbUibDatepicker2.default, _vendorBbUibDatepickerPopup2.default];
	
	/**
	 * @name default
	 * @type {string}
	 * @description The angular module name
	 */
	exports.default = _vendorBbAngular2.default.module('ui-bb-calendar-popup-ng', dependencyKeys).controller('uiBbCalendarPopupController', _calendarPopup2.default, ['$filter', '$locale']).component('uiBbCalendarPopup', _calendarPopup4.default).name;

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_16__;

/***/ }),
/* 17 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_17__;

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_18__;

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBbCalendarPopupController
	 * @ngkey uiBbCalendarPopupController
	 * @type {function}
	 * @description Calendar popup controller
	 */
	function uiBbCalendarPopupController($filter, $locale) {
	  var _this = this;
	
	  var today = new Date();
	
	  // default datepicker config
	  var defaultConfig = {
	    formatDayHeader: 'EEE',
	    formatDayTitle: 'MMM yyyy',
	    formatMonth: 'MMM',
	    maxMode: 'day',
	    minDate: today,
	    maxDate: new Date(today.getFullYear() + 2, today.getMonth(), today.getDate()),
	    showWeeks: false
	  };
	
	  var dateFormat = 'shortDate';
	
	  // take care of the options here
	  this.options = Object.assign(defaultConfig, this.config);
	
	  /**
	   * @description
	   * Displays translation of "calendar.label.today" key or "today" string in the input field
	   * when today date is selected
	   *
	   * @name formatViewValue
	   * @type {function}
	   * @param {string} value Date value of type string
	   * @returns {string}
	   */
	  this.formatViewValue = function (value) {
	    if (value) {
	      var date = new Date('' + value);
	      return date.toDateString() !== today.toDateString() ? $filter('date')(date, dateFormat) : $filter('i18n')('calendar.label.today') || 'today';
	    }
	
	    return value;
	  };
	
	  /**
	   * @description
	   * Toggling open/close state of the calendar
	   *
	   * @name toggle
	   * @type {function}
	   */
	  this.toggle = function () {
	    _this.opened = !_this.opened;
	  };
	
	  /**
	   * @description
	   * Adjusts selected date and minDate of current control
	   * to the value passed through a binding
	   *
	   * @name $onChanges
	   * @type {function}
	   * @param {object} changesObject Object containing changes in one-way bindings
	   */
	  this.$onChanges = function (changesObject) {
	    if (!changesObject.config) {
	      return;
	    }
	
	    var changes = changesObject.config.currentValue;
	
	    if (changes && changes.minDate) {
	      // update minDate setting
	      _this.options.minDate = changes.minDate;
	
	      // update selected date if mindate is later than selected date
	      if (_this.date && _this.date - changes.minDate < 0) {
	        _this.date = changes.minDate;
	      }
	    }
	
	    if (changes && changes.maxDate) {
	      // update maxDate setting
	      _this.options.maxDate = changes.maxDate;
	
	      // update selected date if maxdate is before than selected date
	      if (_this.date && _this.date - changes.maxDate > 0) {
	        _this.date = changes.maxDate;
	      }
	    }
	  };
	
	  /**
	   * @name $onInit
	   * @type {function}
	   *
	   * @description
	   * Set default value on icon click
	   */
	  this.$onInit = function () {
	    _this.dateFormatPlaceholder = $locale.DATETIME_FORMATS.shortDate.toLowerCase();
	    _this.dateFormat = dateFormat;
	
	    _this.onClick = _this.onClick === undefined ? true : _this.onClick;
	  };
	}
	
	exports.default = uiBbCalendarPopupController;

/***/ }),
/* 20 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBbCalendarPopupComponent
	 * @type {object}
	 *
	 * @property {string} date Date model
	 * @property {object} config Configuration object
	 * @property {boolean} disabled Defines whether or not component is disabled
	 */
	var uiBbCalendarPopupComponent = {
	  bindings: {
	    date: '=',
	    config: '<',
	    disabled: '<',
	    onClick: '<',
	    onFocus: '<'
	  },
	  template: '<div class="input-group">\n    <input type="text" \n      class="form-control" \n      uib-datepicker-popup="{{$ctrl.dateFormat}}" \n      data-ng-disabled="$ctrl.disabled"\n      value={{$ctrl.formatViewValue($ctrl.date)}}\n      data-show-button-bar="false"\n      placeholder="{{$ctrl.dateFormatPlaceholder}}"\n      data-ng-focus="!$ctrl.onFocus || $ctrl.toggle()"\n      data-ng-model="$ctrl.date" \n      data-is-open="$ctrl.opened"\n      data-datepicker-options="$ctrl.options"\n    />\n    <span class="input-group-btn">\n      <button type="button" class="btn btn-default" \n        data-ng-click="!$ctrl.onClick || $ctrl.toggle()"\n        data-ng-disabled="$ctrl.disabled">\n        <i class="fa fa-calendar" aria-hidden="true"></i>\n      </button>\n    </span>\n  </div>',
	  controller: 'uiBbCalendarPopupController'
	};
	
	exports.default = uiBbCalendarPopupComponent;

/***/ })
/******/ ])
});
;
//# sourceMappingURL=ui-bb-calendar-popup-ng.js.map