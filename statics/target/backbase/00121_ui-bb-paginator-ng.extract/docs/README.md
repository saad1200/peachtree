# undefined


Version: **1.1.27**


## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **undefined**<br/>    <a href="#UiBbPaginatorController">UiBbPaginatorController()</a><br/>

## Exports

### <a name="default"></a>*default*

Pagination directive

**Type:** *String*


---

### <a name="UiBbPaginatorController"></a>*UiBbPaginatorController()*

Paginator directive's controller
