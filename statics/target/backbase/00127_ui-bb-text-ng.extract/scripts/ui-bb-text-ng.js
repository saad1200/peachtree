(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-text-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-text-ng"] = factory(require("vendor-bb-angular"));
	else
		root["ui-bb-text-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_94__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(119);

/***/ }),

/***/ 94:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_94__;

/***/ }),

/***/ 119:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(94);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _text = __webpack_require__(120);
	
	var _text2 = _interopRequireDefault(_text);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module ui-bb-text-ng
	 * @description
	 * Component to avoid enter not regex valid symbols on input
	 *
	 * @example
	 * // In an extension:
	 * // file: scripts/index.js
	 * import uiBbTextNg from 'ui-bb-text-ng';
	 *
	 * export const dependencyKeys = [
	 *   uiBbTextNg,
	 * ];
	 *
	 * // file: templates/template.ng.html
	 * <textarea name="description"
	 * 	ng-model="$ctrl.payment.description"
	 * 	regex="$ctrl.paymentPreferences.REGEX"
	 * 	ui-bb-text-ng="ui-bb-text-ng">
	 * </textarea>
	 */
	
	exports.default = _vendorBbAngular2.default.module('ui-bb-text-ng', []).directive('uiBbTextNg', _text2.default).name;

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBBTextNg
	 * @type {function}
	 *
	 * @property {string} regex regular expression of enabled symbols
	 */
	var textDirective = function textDirective() {
	  return {
	    restrict: 'A',
	    scope: {
	      regex: '<'
	    },
	    require: '?ngModel',
	    link: function link(scope, element, attrs, ngModel) {
	      if (!ngModel) return;
	
	      var viewValue = '';
	      var REGEX = new RegExp(scope.regex);
	
	      /**
	       * @description
	       * Method to validate string using REGEX
	       * @param {String} text
	       */
	      var isValid = function isValid(text) {
	        return text === '' || REGEX.test(text);
	      };
	
	      // to avoid spaces, newlines, tabs
	      /* eslint no-param-reassign: ["error", { "props": false }] */
	      if (!attrs.ngTrim) {
	        attrs.ngTrim = 'false';
	      }
	
	      // view to model
	      ngModel.$parsers.push(function (value) {
	        var returnValue = void 0;
	
	        if (!isValid(value)) {
	          returnValue = viewValue;
	          ngModel.$setViewValue(viewValue);
	          ngModel.$render();
	        } else {
	          returnValue = value;
	          viewValue = returnValue;
	        }
	
	        return returnValue;
	      });
	
	      // model to view
	      ngModel.$formatters.push(function (value) {
	        var returnValue = void 0;
	
	        if (!isValid(value)) {
	          returnValue = '';
	        } else {
	          returnValue = value;
	        }
	
	        return returnValue;
	      });
	
	      /**
	       * @description
	       * watch for regex
	       */
	      scope.$watch('regex', function (newRegex) {
	        REGEX = new RegExp(newRegex);
	      });
	    }
	  };
	};
	
	exports.default = textDirective;

/***/ })

/******/ })
});
;
//# sourceMappingURL=ui-bb-text-ng.js.map