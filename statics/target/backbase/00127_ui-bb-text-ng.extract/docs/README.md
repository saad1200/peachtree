# ui-bb-text-ng


Version: **1.0.209**

Component to avoid enter not regex valid symbols on input

## Imports

* vendor-bb-angular

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbTextNg from 'ui-bb-text-ng';

export const dependencyKeys = [
  uiBbTextNg,
];

// file: templates/template.ng.html
<textarea name="description"
	ng-model="$ctrl.payment.description"
	regex="$ctrl.paymentPreferences.REGEX"
	ui-bb-text-ng="ui-bb-text-ng">
</textarea>
```

## Table of Contents
- **ui-bb-text-ng**<br/>    <a href="#ui-bb-text-nguiBBTextNg">uiBBTextNg()</a><br/>

---

### <a name="ui-bb-text-nguiBBTextNg"></a>*uiBBTextNg()*


| Property | Type | Description |
| :-- | :-- | :-- |
| regex | String | regular expression of enabled symbols |
