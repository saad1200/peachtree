# widget-bbm-notification-center-ng


Version: **1.0.31**

Mobile notification center widget.

## Imports

* lib-bb-event-bus-ng
* lib-bb-intent-ng
* lib-bb-storage-ng
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-notifications-ng
* vendor-bb-angular

---

## Table of Contents
- **DetailsController**<br/>    <a href="#DetailsController#$onInit">#$onInit()</a><br/>
- **ListController**<br/>    <a href="#ListController#$onInit">#$onInit()</a><br/>
- **widget-bbm-notification-center-ng**<br/>    <a href="#widget-bbm-notification-center-ngloadMoreNotifications">loadMoreNotifications()</a><br/>    <a href="#widget-bbm-notification-center-ngshowNotificationsDetails">showNotificationsDetails(id)</a><br/>    <a href="#widget-bbm-notification-center-nghasNotifications">hasNotifications()</a><br/>    <a href="#widget-bbm-notification-center-ngsetNotificationRead">setNotificationRead(notificationId, read)</a><br/>    <a href="#widget-bbm-notification-center-ngchangeNotificationRead">changeNotificationRead(notificationId)</a><br/>
- **Hooks**<br/>    <a href="#Hooks#processNotifications">#processNotifications(notifications)</a><br/>
- **widget-bbm-notification-center-ng**<br/>    <a href="#widget-bbm-notification-center-nggetTotalCount">getTotalCount()</a><br/>    <a href="#widget-bbm-notification-center-nggetPageSize">getPageSize()</a><br/>    <a href="#widget-bbm-notification-center-nghasMoreNotifications">hasMoreNotifications()</a><br/>
- **Type Definitions**<br/>    <a href="#Notification">Notification</a><br/>

---

## DetailsController

Notification center details controller.
Loads notification on start.

| Injector Key |
| :-- |
| *DetailsController* |


### <a name="DetailsController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller.

Preloads the notification details and prepares the view model.

##### Fires Events:

> cxp.item.loaded

> bb.item.loaded


---

## ListController

Notification center list controller.
Loads notifications on start.

| Injector Key |
| :-- |
| *ListController* |


### <a name="ListController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller.

Preloads notifications and prepares the view model.

##### Fires Events:

> cxp.item.loaded

> bb.item.loaded


---

### <a name="widget-bbm-notification-center-ngloadMoreNotifications"></a>*loadMoreNotifications()*

Loads more notifications.

##### Returns

Promise of Array - **

---

### <a name="widget-bbm-notification-center-ngshowNotificationsDetails"></a>*showNotificationsDetails(id)*

Handles the intent to show the notification details


| Parameter | Type | Description |
| :-- | :-- | :-- |
| id | String | Id of the notification |

---

### <a name="widget-bbm-notification-center-nghasNotifications"></a>*hasNotifications()*

Checks if there are notifications

##### Returns

Boolean - **

---

### <a name="widget-bbm-notification-center-ngsetNotificationRead"></a>*setNotificationRead(notificationId, read)*

Set notification read status

| Parameter | Type | Description |
| :-- | :-- | :-- |
| notificationId | String | Id of the notification |
| read | Boolean | read status of notification |

---

### <a name="widget-bbm-notification-center-ngchangeNotificationRead"></a>*changeNotificationRead(notificationId)*

Change notification read status

| Parameter | Type | Description |
| :-- | :-- | :-- |
| notificationId | String | Id of the notification |

---

## Hooks

Hooks for widget-bbm-notification-center-ng.

### <a name="Hooks#processNotifications"></a>*#processNotifications(notifications)*

Processes the list of notifications.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| notifications | Array of <a href="#Notification">Notification</a> | Original list of notifications from the model. |

##### Returns

Array of <a href="#Notification">Notification</a> - *Processed list of notifications.*

---

### <a name="widget-bbm-notification-center-nggetTotalCount"></a>*getTotalCount()*

Notifications array size

##### Returns

Number - **

---

### <a name="widget-bbm-notification-center-nggetPageSize"></a>*getPageSize()*

Page size property

##### Returns

Number - **

---

### <a name="widget-bbm-notification-center-nghasMoreNotifications"></a>*hasMoreNotifications()*

Checks if there are more notifications to load

##### Returns

Boolean - *hasMore*

## Type Definitions


### <a name="Notification"></a>*Notification*


**Type:** *Object*


---

## Templates

* *template.ng.html*

---
