# widget-bb-user-menu-ng


Version: **1.1.3**

User Menu widget

## Imports

* lib-bb-widget-ng
* model-bb-login-ng
* vendor-bb-angular

---

## Table of Contents
- **UserMenuController**<br/>    <a href="#UserMenuController#logout">#logout()</a><br/>    <a href="#UserMenuController#loggedUser">#loggedUser()</a><br/>    <a href="#UserMenuController#userDataUrl">#userDataUrl</a><br/>

---

## UserMenuController

User Menu widget

### <a name="UserMenuController#logout"></a>*#logout()*

Logout function

##### Returns

Promise - **

### <a name="UserMenuController#loggedUser"></a>*#loggedUser()*

Retrieves logged in user id
Returns empty string if user is not found

##### Returns

String - **
### <a name="UserMenuController#userDataUrl"></a>*#userDataUrl*


**Type:** *String*


## Templates

* *template.ng.html*

---
