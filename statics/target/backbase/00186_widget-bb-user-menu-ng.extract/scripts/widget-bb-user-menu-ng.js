(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("model-bb-login-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-user-menu-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "model-bb-login-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-user-menu-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("model-bb-login-ng"));
	else
		root["widget-bb-user-menu-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["model-bb-login-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_24__, __WEBPACK_EXTERNAL_MODULE_32__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(35);

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),

/***/ 24:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_24__;

/***/ }),

/***/ 32:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_32__;

/***/ }),

/***/ 35:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetNg = __webpack_require__(24);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _modelBbLoginNg = __webpack_require__(32);
	
	var _modelBbLoginNg2 = _interopRequireDefault(_modelBbLoginNg);
	
	var _controller = __webpack_require__(36);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module widget-bb-user-menu-ng
	 *
	 * @description
	 * User Menu widget
	 */
	exports.default = _vendorBbAngular2.default.module('widget-bb-user-menu-ng', [_modelBbLoginNg2.default, _libBbWidgetNg2.default]).controller('UserMenuController', [
	// dependencies to inject
	_modelBbLoginNg.modelLoginKey, _libBbWidgetNg.widgetKey,
	/* into */
	_controller2.default]).name;

/***/ }),

/***/ 36:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = UserMenuController;
	/* global window */
	/**
	 * @name UserMenuController
	 * @type {object}
	 *
	 * @description
	 * User Menu widget
	 */
	function UserMenuController(model, widgetInstance) {
	  var $ctrl = this;
	  var portal = window.b$ && window.b$.portal;
	
	  var logoutRedirectPage = widgetInstance.getStringPreference('logoutRedirectPage');
	  var userDataUrl = widgetInstance.getStringPreference('userDataUrl');
	
	  var logoutRedirectUrl = portal ? portal.config.serverRoot + '/' + portal.portalName + '/' + logoutRedirectPage : logoutRedirectPage;
	
	  var logout = function logout() {
	    model.logout().then(function () {
	      window.location.assign(logoutRedirectUrl);
	    });
	  };
	
	  var currentUser = null;
	  var loggedUser = function loggedUser() {
	    if (currentUser) {
	      return currentUser;
	    }
	
	    // CXP 5.x will have portal var, CX 6 not
	    // Use different approaches to determine user name based on that
	    if (portal) {
	      if (portal.loggedInUserId && portal.loggedInUserId !== 'null') {
	        currentUser = portal.loggedInUserId;
	      }
	    } else {
	      // TODO!!! Find a way to get current user in CX 6
	      currentUser = 'admin';
	    }
	
	    currentUser = currentUser || '';
	    return currentUser;
	  };
	
	  Object.assign($ctrl, {
	    /**
	     * @description Logout function
	     * @type {function}
	     *
	     * @name UserMenuController#logout
	     * @returns {Promise}
	     */
	    logout: logout,
	    /**
	     * @description Retrieves logged in user id
	     * Returns empty string if user is not found
	     * @type {function}
	     *
	     * @name UserMenuController#loggedUser
	     * @returns {string}
	     */
	    loggedUser: loggedUser,
	    /**
	     * @name UserMenuController#userDataUrl
	     * @type {string}
	     */
	    userDataUrl: userDataUrl
	  });
	}

/***/ })

/******/ })
});
;
//# sourceMappingURL=widget-bb-user-menu-ng.js.map