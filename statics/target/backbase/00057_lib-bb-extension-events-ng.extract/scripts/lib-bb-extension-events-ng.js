(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-event-bus-ng"), require("lib-bb-notifications-ng"), require("lib-bb-widget-extension-ng"));
	else if(typeof define === 'function' && define.amd)
		define("lib-bb-extension-events-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "lib-bb-event-bus-ng", "lib-bb-notifications-ng", "lib-bb-widget-extension-ng"], factory);
	else if(typeof exports === 'object')
		exports["lib-bb-extension-events-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-event-bus-ng"), require("lib-bb-notifications-ng"), require("lib-bb-widget-extension-ng"));
	else
		root["lib-bb-extension-events-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["lib-bb-event-bus-ng"], root["lib-bb-notifications-ng"], root["lib-bb-widget-extension-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_12__, __WEBPACK_EXTERNAL_MODULE_13__, __WEBPACK_EXTERNAL_MODULE_14__, __WEBPACK_EXTERNAL_MODULE_15__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(11);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.extensionEventsDefaultContextKey = exports.extensionEventsContextKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(5);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetNg = __webpack_require__(12);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _libBbEventBusNg = __webpack_require__(13);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _libBbNotificationsNg = __webpack_require__(14);
	
	var _libBbNotificationsNg2 = _interopRequireDefault(_libBbNotificationsNg);
	
	var _libBbWidgetExtensionNg = __webpack_require__(15);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var moduleKey = 'lib-bb-extension-events-ng';
	
	/**
	 * @name extensionEventsContextKey
	 * @type {string}
	 * @description
	 * The injector key to be used to provide an alternative context to the extension module's events
	 *
	 * @example
	 * // "TODO" Widget index.js
	 *
	 * import bbExtensionEventsModuleKey, {
	 *   extensionEventsContextKey,
	 * } from 'lib-bb-extension-events-ng';
	 *
	 * import todoModelModuleKey, { modelTodoKey } from 'model-bb-todo-ng';
	 *
	 * // Add TODO `model` to the `context` provided to the extension `events` key
	 * export default angular.module(..., [
	 *   ...,
	 *   extensionEventsContextKey,
	 *   todoModelModuleKey,
	 * ])
	 * .factory(extensionEventsContextKey, [
	 *   modelTodoKey,
	 *   (model) => ({
	 *     model,
	 *   }),
	 * ])
	 */
	/**
	 * @module lib-bb-extension-events-ng
	 *
	 * @description Allows extensions to define 'events' (see
	 * {@link module:lib-bb-event-bus-ng.lib-bb-event-bus-ng}) to subscribe to.
	 *
	 * Event subscriptions are created from the exported `events` of the extension module.
	 *
	 * Events can either be exported as an object, or a function which returns an object.
	 *
	 * If the `events` is a function it will receive an {@link EventContext} object.
	 *
	 * The object returned should be a map of event name to callback (see example below).
	 *
	 * @example
	 * // My "TODO" widget extension:
	 * export const events = ({ notifications, $filter }) => ({
	 *   // subscribes to 'model-bb-todo.load-list.failed' event
	 *   'model-bb-todo.load-list.failed': () => {
	 *     notifications.notifyAlert($filter('i18n')('notification.load-list.failed'));
	 *   },
	 * });
	 */
	
	/**
	 * The default context passed to the `events` function of the extension. This context can be
	 * extended by individual widgets, so consult the widget docs for additional context properties.
	 * @typedef EventContext
	 * @type {Object}
	 * @property {Object} $filter Angular's $filter service.
	 * See {@link https://docs.angularjs.org/api/ng/service/$filter}
	 * @property {module:lib-bb-widget.BBWidget} widget The widget instance
	 * @property {module:lib-bb-notifications-ng.Notifications} notifications The notifications service
	 * @property {module:lib-bb-event-bus-ng.publish} publish The publish function of the event bus
	 */
	var extensionEventsContextKey = exports.extensionEventsContextKey = moduleKey + ':context';
	var extensionEventsDefaultContextKey = exports.extensionEventsDefaultContextKey = moduleKey + ':default-context';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_libBbWidgetExtensionNg.bbWidgetExtensionModuleKey, _libBbWidgetNg2.default, _libBbEventBusNg2.default, _libBbNotificationsNg2.default]).value(extensionEventsContextKey, {}).factory(extensionEventsDefaultContextKey, ['$filter', _libBbEventBusNg.eventBusKey, _libBbWidgetNg.widgetKey, _libBbNotificationsNg.notificationsKey, function ($filter, eventBus, widget, notifications) {
	  return {
	    $filter: $filter,
	    widget: widget,
	    notifications: notifications,
	    publish: eventBus.publish
	  };
	}]).run([_libBbWidgetExtensionNg.bbWidgetExtensionKey, _libBbEventBusNg.eventBusKey, extensionEventsDefaultContextKey, extensionEventsContextKey, function (ext, eventBus, defaultContext, context) {
	  // Attach event listeners
	  var events = typeof ext.events === 'function' ? ext.events(Object.assign({}, defaultContext, context)) : ext.events || {};
	  Object.keys(events).forEach(function (event) {
	    eventBus.subscribe(event, events[event]);
	  });
	}]).name;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_12__;

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_13__;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_14__;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ })
/******/ ])
});
;
//# sourceMappingURL=lib-bb-extension-events-ng.js.map