# lib-bb-extension-events-ng

Allows extensions to define 'events' (see
<a href="lib-bb-event-bus-ng.html#lib-bb-event-bus-ng">lib-bb-event-bus-ng</a>) to subscribe to.

Event subscriptions are created from the exported `events` of the extension module.

Events can either be exported as an object, or a function which returns an object.

If the `events` is a function it will receive an <a href="#EventContext">EventContext</a> object.

The object returned should be a map of event name to callback (see example below).

## Imports

* lib-bb-event-bus-ng
* lib-bb-notifications-ng
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
// My "TODO" widget extension:
export const events = ({ notifications, $filter }) => ({
  // subscribes to 'model-bb-todo.load-list.failed' event
  'model-bb-todo.load-list.failed': () => {
    notifications.notifyAlert($filter('i18n')('notification.load-list.failed'));
  },
});
```

## Table of Contents
- **Exports**<br/>    <a href="#extensionEventsContextKey">extensionEventsContextKey</a><br/>
- **Type Definitions**<br/>    <a href="#EventContext">EventContext</a><br/>

## Exports

### <a name="extensionEventsContextKey"></a>*extensionEventsContextKey*

The injector key to be used to provide an alternative context to the extension module's events

**Type:** *String*


## Example

```javascript
// "TODO" Widget index.js

import bbExtensionEventsModuleKey, {
  extensionEventsContextKey,
} from 'lib-bb-extension-events-ng';

import todoModelModuleKey, { modelTodoKey } from 'model-bb-todo-ng';

// Add TODO `model` to the `context` provided to the extension `events` key
export default angular.module(..., [
  ...,
  extensionEventsContextKey,
  todoModelModuleKey,
])
.factory(extensionEventsContextKey, [
  modelTodoKey,
  (model) => ({
    model,
  }),
])
```

## Type Definitions


### <a name="EventContext"></a>*EventContext*

The default context passed to the `events` function of the extension. This context can be
extended by individual widgets, so consult the widget docs for additional context properties.

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| $filter | Object | Angular's $filter service. See <a href="https://docs.angularjs.org/api/ng/service/$filter">https://docs.angularjs.org/api/ng/service/$filter</a> |
| widget | <a href="lib-bb-widget.html#BBWidget">BBWidget</a> | The widget instance |
| notifications | <a href="lib-bb-notifications-ng.html#Notifications">Notifications</a> | The notifications service |
| publish | <a href="lib-bb-event-bus-ng.html#publish">publish</a> | The publish function of the event bus |

---
