(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("data-bb-transactions-http-ng"), require("data-bb-product-summary-http-ng"), require("lib-bb-storage-ng"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"));
	else if(typeof define === 'function' && define.amd)
		define("model-bb-turnovers-ng", ["vendor-bb-angular", "data-bb-transactions-http-ng", "data-bb-product-summary-http-ng", "lib-bb-storage-ng", "lib-bb-widget-ng", "lib-bb-model-errors"], factory);
	else if(typeof exports === 'object')
		exports["model-bb-turnovers-ng"] = factory(require("vendor-bb-angular"), require("data-bb-transactions-http-ng"), require("data-bb-product-summary-http-ng"), require("lib-bb-storage-ng"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"));
	else
		root["model-bb-turnovers-ng"] = factory(root["vendor-bb-angular"], root["data-bb-transactions-http-ng"], root["data-bb-product-summary-http-ng"], root["lib-bb-storage-ng"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_16__, __WEBPACK_EXTERNAL_MODULE_17__, __WEBPACK_EXTERNAL_MODULE_18__, __WEBPACK_EXTERNAL_MODULE_19__, __WEBPACK_EXTERNAL_MODULE_21__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(14);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.modelTurnoversKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(15);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbTransactionsHttpNg = __webpack_require__(16);
	
	var _dataBbTransactionsHttpNg2 = _interopRequireDefault(_dataBbTransactionsHttpNg);
	
	var _dataBbProductSummaryHttpNg = __webpack_require__(17);
	
	var _dataBbProductSummaryHttpNg2 = _interopRequireDefault(_dataBbProductSummaryHttpNg);
	
	var _libBbStorageNg = __webpack_require__(18);
	
	var _libBbStorageNg2 = _interopRequireDefault(_libBbStorageNg);
	
	var _libBbWidgetNg = __webpack_require__(19);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _turnovers = __webpack_require__(20);
	
	var _turnovers2 = _interopRequireDefault(_turnovers);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module model-bb-turnovers-ng
	 *
	 * @description
	 * Model for widget-bb-turnovers-ng
	 *
	 * @example
	 * import modelTurnoversModuleKey, { modelTurnoversKey } from 'model-bb-turnovers-ng';
	 *
	 * angular
	 *   .module('ExampleModule', [
	 *     modelTurnoversModuleKey,
	 *   ])
	 *   .factory('someFactory', [
	 *     modelTurnoversKey,
	 *     // into
	 *     function someFactory(turnoversModel) {
	 *       // ...
	 *     },
	 *   ]);
	 */
	var moduleKey = 'model-bb-turnovers-ng';
	var modelTurnoversKey = exports.modelTurnoversKey = moduleKey + ':model';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_dataBbTransactionsHttpNg2.default, _dataBbProductSummaryHttpNg2.default, _libBbStorageNg2.default, _libBbWidgetNg2.default]).factory(modelTurnoversKey, ['$q', _dataBbTransactionsHttpNg.transactionsDataKey, _dataBbProductSummaryHttpNg.productSummaryDataKey, _libBbStorageNg.bbStorageServiceKey, _libBbWidgetNg.widgetKey,
	/* into */
	_turnovers2.default]).name;
	
	/**
	 * Turnover response object
	 * @typedef {object} Turnover
	 * @property {string} arrangementId Id of the arrangement this turnover belongs to
	 * @property {string} intervalDuration Duration of intervals returned
	 * @property {TurnoverItem[]} turnovers Array of turnover items
	 */
	
	/**
	 * Turnover response item
	 * @typedef {object} TurnoverItem
	 * @property {string} intervalStartDate Date in ISO format (2016-06-01T16:41:41.090Z)
	 * @property {object} debitAmount Debit amount object
	 * @property {string} debitAmount.currencyCode Debit amount currency code (ISO)
	 * @property {number} debitAmount.amount Debit amount value
	 * @property {object} creditAmount Credit amount object
	 * @property {string} creditAmount.currencyCode Credit amount currency code (ISO)
	 * @property {number} creditAmount.amount Credit amount value
	 * @property {object} balance Debit and credit difference object
	 * @property {string} balance.currencyCode Debit and credit difference currency code (ISO)
	 * @property {number} balance.amount Debit and credit difference value
	 */
	
	/**
	 * Amount object
	 * @typedef {object} Amount
	 * @property {string} currency Currency code
	 * @property {number} value
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKinds
	 * @property {Amount} aggregatedBalance Total balance of products
	 * @property {ProductKind[]} productKinds Array of Products Kinds
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKind
	 * @property {Amount} aggregatedBalance Total balance of product kind
	 * @property {!string} name Name of the product kind
	 * @property {Product[]} products Array of associated products
	 */
	
	/**
	 * Product type definition
	 * @typedef {object} Product
	 * @property {!string} id id of the Product
	 * @property {!string} name Name of the Product
	 * @property {!string} kind id of the ProductKind
	 * @property {string} alias Alias of the Product
	 * @property {string} IBAN International Bank Account Number
	 * @property {string} BBAN Basic Bank Account Number
	 * @property {string} currency Currency code
	 * @property {string} PANSuffix Primary Account Number Suffix
	 * @property {string} bookedBalance Booked balance
	 * @property {string} availableBalance Available balance
	 * @property {string} creditLimit Credit limit
	 * @property {string} currentInvestmentValue Current investment value
	 * @property {string} principalAmount Principal amount
	 * @property {string} accruedInterest Accrued interest
	 */
	
	/**
	 * BBSeries data object used to draw charts
	 * @typedef {object} BBSeries
	 * @property {string[]} labels Array of x axis labels
	 * @property {Dataset[]} datasets Array of all y axis value datasets
	 */
	
	/**
	 * Dataset object for y axis data
	 * @typedef {object} Dataset
	 * @property {number[]} data Array of data points to be drawn for each label
	 */

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_16__;

/***/ }),
/* 17 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_17__;

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_18__;

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_19__;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = turnoversModel;
	
	var _libBbModelErrors = __webpack_require__(21);
	
	var _constants = __webpack_require__(22);
	
	/**
	 * Model factory for model-bb-turnovers-ng
	 *
	 * @inner
	 * @type {function}
	 * @param {Object} Promise An ES2015 compatible `Promise` object.
	 *
	 * @return {Object}
	 */
	function turnoversModel(Promise, transactionsData, productSummaryData, bbStorage, widgetInstance) {
	  /**
	   * @name turnoversModel#FROM_STORAGE
	   * @inner
	   * @type {boolean}
	   *
	   * @description
	   * Defines if products are recieved from bb-storage or from API always
	   */
	  var FROM_STORAGE = widgetInstance.getBooleanPreference(_constants.preferencesKeys.IS_PRODUCTS_LIST_FROM_STORAGE);
	
	  /**
	   * @name turnoversModel#IS_FIRST_PRODUCT_DEFAULT
	   * @inner
	   * @type {boolean}
	   *
	   * @description
	   * Defines if the first product is selected by default
	   */
	  var GET_FIRST_AS_DEFAULT = widgetInstance.getBooleanPreference(_constants.preferencesKeys.IS_FIRST_PRODUCT_DEFAULT);
	
	  /**
	   * @name turnoversModel#productKindHasProducts
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Checks if product kind has any products
	   *
	   * @param {Object} productKind a product kind object
	   * @return {boolean} 'true' if has any products
	   */
	  var productKindHasProducts = function productKindHasProducts(productKind) {
	    return productKind.products && productKind.products.length;
	  };
	
	  /**
	   * @name turnoversModel#validateTurnoversParameters
	   * @type {function}
	   *
	   * @description
	   * Checks if all required parameters are set
	   *
	   * @returns {Promise.<object>}
	   * A Promise resolving to object with parameters.
	   */
	  var validateTurnoversParameters = function validateTurnoversParameters(params) {
	    if (params.arrangementId && params.periodStartDate && params.periodEndDate && params.intervalDuration) {
	      return Promise.resolve(params);
	    }
	
	    return Promise.reject({
	      code: _constants.E_PARAMS
	    });
	  };
	
	  /**
	   * @name turnoversModel#loadProducts
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Load products from API.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var loadProducts = function loadProducts() {
	    return productSummaryData.getProductsummary().then(function (_ref) {
	      var data = _ref.data;
	
	      bbStorage.setItem(_constants.bbStorageKeys.PRODUCT_SUMMARY, data);
	      return data;
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  };
	
	  /**
	   * @name turnoversModel#getProducts
	   * @type {function}
	   *
	   * @description
	   * Get products.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var getProducts = function getProducts() {
	    return bbStorage.getItem(_constants.bbStorageKeys.PRODUCT_SUMMARY).then(function (data) {
	      return FROM_STORAGE && data ? data : loadProducts();
	    });
	  };
	
	  /**
	   * @name turnoversModel#getProductsArray
	   * @type {function}
	   *
	   * @description
	   * Get products.
	   *
	   * @param {boolean} keepEmptyProducts defines if empty product kinds should be passed.
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to array with Products.
	   */
	  var getProductsArray = function getProductsArray() {
	    var keepEmptyProducts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
	    return getProducts().then(function (data) {
	      return Object.keys(data).filter(function (kind) {
	        return keepEmptyProducts || productKindHasProducts(data[kind]);
	      }).reduce(function (products, kind) {
	        var extendedProducts = data[kind].products.map(function (product) {
	          return Object.assign(product, { kind: kind });
	        });
	        return products.concat(extendedProducts);
	      }, []);
	    });
	  };
	
	  /**
	   * @name turnoversModel#getProductByID
	   * @inner
	   * @type {function}
	   *
	   * @description
	   * Get products kinds.
	   *
	   * @param {any} productId product ID to get a stored product record
	   *
	   * @returns {Promise.<ProductKinds, ModelError>}
	   * A Promise resolving to object with ProductsKinds and TotalBalance.
	   */
	  var getProductByID = function getProductByID(productId) {
	    return getProductsArray().then(function (products) {
	      var defaultProduct = GET_FIRST_AS_DEFAULT && products[0] ? products[0] : null;
	      return productId ? products.find(function (product) {
	        return product.id === productId;
	      }) : defaultProduct;
	    });
	  };
	
	  /**
	   * @name turnoversModel#getSelectedProduct
	   * @type {function}
	   *
	   * @description
	   * Get current selected product.
	   *
	   * @returns {Promise.<Product, ModelError>}
	   * A Promise resolving to a selected product object.
	   */
	  var getSelectedProduct = function getSelectedProduct() {
	    return bbStorage.getItem(_constants.bbStorageKeys.PRODUCT_SELECTED).then(function (id) {
	      return getProductByID(id);
	    });
	  };
	
	  /**
	   * @name turnoversModel#setSelectedProduct
	   * @type {function}
	   *
	   * @description
	   * Set selected product to the sorage
	   *
	   * @param {Product} selectedProduct The selected product value
	   */
	  var setSelectedProduct = function setSelectedProduct(selectedProduct) {
	    if (selectedProduct) {
	      bbStorage.setItem(_constants.bbStorageKeys.PRODUCT_SELECTED, selectedProduct.id);
	    } else {
	      bbStorage.removeItem(_constants.bbStorageKeys.PRODUCT_SELECTED);
	    }
	  };
	
	  /**
	   * @name turnoversModel#loadTurnovers
	   * @type {function}
	   *
	   * @description
	   * Load product turnovers
	   *
	   * @param {object} params Request parameters
	   * @returns {Promise.<Turnover, ModelError>} A Promise with turnover or error data
	   */
	  var loadTurnovers = function loadTurnovers(params) {
	    return transactionsData.getTransactionsTurnovers(params).then(function (response) {
	      return response.data;
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  };
	
	  /**
	   * @name turnoversModel#transformToSeries
	   * @type {function}
	   *
	   * @description
	   * Transforms data into format suitable for chart UI components
	   *
	   * @param {Turnover} data Turnover data
	   * @returns {BBSeries} Data in format suitable for chart UI components
	   */
	  var transformToSeries = function transformToSeries(data) {
	    return {
	      labels: data.turnovers.map(function (turnover) {
	        return turnover.intervalStartDate;
	      }),
	      datasets: [{
	        data: data.turnovers.map(function (item) {
	          return item.debitAmount.amount;
	        })
	      }, {
	        data: data.turnovers.map(function (item) {
	          return item.creditAmount.amount;
	        })
	      }]
	    };
	  };
	
	  /**
	   * @name turnoversModel#isFirstProductDefault
	   * @type {function}
	   *
	   * @description
	   * Defines if the first product is selected by default
	   *
	   * @returns {boolean} GET_FIRST_AS_DEFAULT
	   */
	  var isFirstProductDefault = function isFirstProductDefault() {
	    return GET_FIRST_AS_DEFAULT;
	  };
	
	  /**
	   * @name turnoversModel#isProductsListFromStorage
	   * @type {function}
	   *
	   * @description
	   * Defines if products are recieved from bb-storage or from API always
	   *
	   * @returns {boolean} FROM_STORAGE
	   */
	  var isProductsListFromStorage = function isProductsListFromStorage() {
	    return FROM_STORAGE;
	  };
	
	  /**
	   * @name turnoversModel
	   * @type {Object}
	   */
	  return {
	    E_PARAMS: _constants.E_PARAMS,
	    loadTurnovers: loadTurnovers,
	    validateTurnoversParameters: validateTurnoversParameters,
	    transformToSeries: transformToSeries,
	    getProducts: getProducts,
	    getProductsArray: getProductsArray,
	    getSelectedProduct: getSelectedProduct,
	    setSelectedProduct: setSelectedProduct,
	    isFirstProductDefault: isFirstProductDefault,
	    isProductsListFromStorage: isProductsListFromStorage
	  };
	}

/***/ }),
/* 21 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_21__;

/***/ }),
/* 22 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name bbStorageKeys
	 * @description
	 * bbStorage keys enum
	 * @type {object}
	 */
	var bbStorageKeys = exports.bbStorageKeys = {
	  PRODUCT_SELECTED: 'bb.product.selected',
	  PRODUCT_SUMMARY: 'bb.product.summary.data'
	};
	
	/**
	 * @name preferencesKeys
	 * @description
	 * Preferences keys enum
	 * @type {object}
	 */
	var preferencesKeys = exports.preferencesKeys = {
	  IS_FIRST_PRODUCT_DEFAULT: 'bb.turnovers.useFirstProductAsDefault',
	  IS_PRODUCTS_LIST_FROM_STORAGE: 'bb.turnovers.getProductsFromStorage'
	};
	
	/**
	 * @name E_PARAMS
	 * @description
	 * Additional model error in case required parameters are missing
	 * @type {string}
	 */
	var E_PARAMS = exports.E_PARAMS = 'error.load.params';

/***/ })
/******/ ])
});
;
//# sourceMappingURL=model-bb-turnovers-ng.js.map