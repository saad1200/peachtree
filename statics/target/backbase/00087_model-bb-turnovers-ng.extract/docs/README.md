# model-bb-turnovers-ng


Version: **1.0.15**

Model for widget-bb-turnovers-ng

## Imports

* data-bb-product-summary-http-ng
* data-bb-transactions-http-ng
* lib-bb-model-errors
* lib-bb-storage-ng
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
import modelTurnoversModuleKey, { modelTurnoversKey } from 'model-bb-turnovers-ng';

angular
  .module('ExampleModule', [
    modelTurnoversModuleKey,
  ])
  .factory('someFactory', [
    modelTurnoversKey,
    // into
    function someFactory(turnoversModel) {
      // ...
    },
  ]);
```

## Table of Contents
- **model-bb-turnovers-ng**<br/>    <a href="#model-bb-turnovers-ngE_PARAMS">E_PARAMS</a><br/>
- **turnoversModel**<br/>    <a href="#turnoversModel#validateTurnoversParameters">#validateTurnoversParameters()</a><br/>    <a href="#turnoversModel#getProducts">#getProducts()</a><br/>    <a href="#turnoversModel#getProductsArray">#getProductsArray(keepEmptyProducts)</a><br/>    <a href="#turnoversModel#getSelectedProduct">#getSelectedProduct()</a><br/>    <a href="#turnoversModel#setSelectedProduct">#setSelectedProduct(selectedProduct)</a><br/>    <a href="#turnoversModel#loadTurnovers">#loadTurnovers(params)</a><br/>    <a href="#turnoversModel#transformToSeries">#transformToSeries(data)</a><br/>    <a href="#turnoversModel#isFirstProductDefault">#isFirstProductDefault()</a><br/>    <a href="#turnoversModel#isProductsListFromStorage">#isProductsListFromStorage()</a><br/>
- **Type Definitions**<br/>    <a href="#Turnover">Turnover</a><br/>    <a href="#TurnoverItem">TurnoverItem</a><br/>    <a href="#Amount">Amount</a><br/>    <a href="#ProductKinds">ProductKinds</a><br/>    <a href="#ProductKind">ProductKind</a><br/>    <a href="#Product">Product</a><br/>    <a href="#BBSeries">BBSeries</a><br/>    <a href="#Dataset">Dataset</a><br/>

---

## bbStorageKeys

bbStorage keys enum

---

## preferencesKeys

Preferences keys enum

---
### <a name="model-bb-turnovers-ngE_PARAMS"></a>*E_PARAMS*

Additional model error in case required parameters are missing

**Type:** *String*


---

## turnoversModel


### <a name="turnoversModel#validateTurnoversParameters"></a>*#validateTurnoversParameters()*

Checks if all required parameters are set

##### Returns

Promise of Object - *A Promise resolving to object with parameters.*

### <a name="turnoversModel#getProducts"></a>*#getProducts()*

Get products.

##### Returns

Promise of <a href="#ProductKinds">ProductKinds</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with ProductsKinds and TotalBalance.*

### <a name="turnoversModel#getProductsArray"></a>*#getProductsArray(keepEmptyProducts)*

Get products.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| keepEmptyProducts | Boolean | defines if empty product kinds should be passed. |

##### Returns

Promise of <a href="#ProductKinds">ProductKinds</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to array with Products.*

### <a name="turnoversModel#getSelectedProduct"></a>*#getSelectedProduct()*

Get current selected product.

##### Returns

Promise of <a href="#Product">Product</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to a selected product object.*

### <a name="turnoversModel#setSelectedProduct"></a>*#setSelectedProduct(selectedProduct)*

Set selected product to the sorage

| Parameter | Type | Description |
| :-- | :-- | :-- |
| selectedProduct | <a href="#Product">Product</a> | The selected product value |

### <a name="turnoversModel#loadTurnovers"></a>*#loadTurnovers(params)*

Load product turnovers

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Request parameters |

##### Returns

Promise of <a href="#Turnover">Turnover</a>, <a href="#ModelError">ModelError</a> - *A Promise with turnover or error data*

### <a name="turnoversModel#transformToSeries"></a>*#transformToSeries(data)*

Transforms data into format suitable for chart UI components

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#Turnover">Turnover</a> | Turnover data |

##### Returns

<a href="#BBSeries">BBSeries</a> - *Data in format suitable for chart UI components*

### <a name="turnoversModel#isFirstProductDefault"></a>*#isFirstProductDefault()*

Defines if the first product is selected by default

##### Returns

Boolean - *GET_FIRST_AS_DEFAULT*

### <a name="turnoversModel#isProductsListFromStorage"></a>*#isProductsListFromStorage()*

Defines if products are recieved from bb-storage or from API always

##### Returns

Boolean - *FROM_STORAGE*

## Type Definitions


### <a name="Turnover"></a>*Turnover*

Turnover response object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| arrangementId | String | Id of the arrangement this turnover belongs to |
| intervalDuration | String | Duration of intervals returned |
| turnovers | Array of <a href="#TurnoverItem">TurnoverItem</a> | Array of turnover items |

### <a name="TurnoverItem"></a>*TurnoverItem*

Turnover response item

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| intervalStartDate | String | Date in ISO format (2016-06-01T16:41:41.090Z) |
| debitAmount | Object | Debit amount object |
| debitAmount.currencyCode | String | Debit amount currency code (ISO) |
| debitAmount.amount | Number | Debit amount value |
| creditAmount | Object | Credit amount object |
| creditAmount.currencyCode | String | Credit amount currency code (ISO) |
| creditAmount.amount | Number | Credit amount value |
| balance | Object | Debit and credit difference object |
| balance.currencyCode | String | Debit and credit difference currency code (ISO) |
| balance.amount | Number | Debit and credit difference value |

### <a name="Amount"></a>*Amount*

Amount object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String | Currency code |
| value | Number |  |

### <a name="ProductKinds"></a>*ProductKinds*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of products |
| productKinds | Array of <a href="#ProductKind">ProductKind</a> | Array of Products Kinds |

### <a name="ProductKind"></a>*ProductKind*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of product kind |
| name | <a href="#!string">!string</a> | Name of the product kind |
| products | Array of <a href="#Product">Product</a> | Array of associated products |

### <a name="Product"></a>*Product*

Product type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the Product |
| name | <a href="#!string">!string</a> | Name of the Product |
| kind | <a href="#!string">!string</a> | id of the ProductKind |
| alias | String | Alias of the Product |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | Currency code |
| PANSuffix | String | Primary Account Number Suffix |
| bookedBalance | String | Booked balance |
| availableBalance | String | Available balance |
| creditLimit | String | Credit limit |
| currentInvestmentValue | String | Current investment value |
| principalAmount | String | Principal amount |
| accruedInterest | String | Accrued interest |

### <a name="BBSeries"></a>*BBSeries*

BBSeries data object used to draw charts

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| labels | Array of String | Array of x axis labels |
| datasets | Array of <a href="#Dataset">Dataset</a> | Array of all y axis value datasets |

### <a name="Dataset"></a>*Dataset*

Dataset object for y axis data

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of Number | Array of data points to be drawn for each label |

---
