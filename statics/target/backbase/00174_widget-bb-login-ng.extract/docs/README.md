# widget-bb-login-ng


Version: **2.0.25**

Login widget

## Imports

* lib-bb-widget-ng
* model-bb-login-ng
* vendor-bb-angular

---

## Table of Contents
- **LoginController**<br/>    <a href="#LoginController#$onInit">#$onInit()</a><br/>    <a href="#LoginController#login">#login()</a><br/>    <a href="#LoginController#username">#username</a><br/>    <a href="#LoginController#password">#password</a><br/>
- **widget-bb-login-ng**<br/>    <a href="#widget-bb-login-ngisLoading">isLoading</a><br/>

---

## LoginController

Login widget

### <a name="LoginController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller

##### Returns

<a href="#void">void</a> - **

### <a name="LoginController#login"></a>*#login()*

Login function

##### Returns

Promise - **
### <a name="LoginController#username"></a>*#username*


**Type:** *String*

### <a name="LoginController#password"></a>*#password*


**Type:** *String*


---
### <a name="widget-bb-login-ngisLoading"></a>*isLoading*

Loading status

**Type:** *Boolean*


## Templates

* *template.ng.html*

---
