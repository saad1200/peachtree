(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("data-bb-messaging-service-http-ng"), require("lib-bb-model-errors"));
	else if(typeof define === 'function' && define.amd)
		define("model-bb-messages-ng", ["vendor-bb-angular", "data-bb-messaging-service-http-ng", "lib-bb-model-errors"], factory);
	else if(typeof exports === 'object')
		exports["model-bb-messages-ng"] = factory(require("vendor-bb-angular"), require("data-bb-messaging-service-http-ng"), require("lib-bb-model-errors"));
	else
		root["model-bb-messages-ng"] = factory(root["vendor-bb-angular"], root["data-bb-messaging-service-http-ng"], root["lib-bb-model-errors"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_27__, __WEBPACK_EXTERNAL_MODULE_28__, __WEBPACK_EXTERNAL_MODULE_30__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(26);

/***/ }),

/***/ 26:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.modelMessagesKey = exports.moduleKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(27);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbMessagingServiceHttpNg = __webpack_require__(28);
	
	var _dataBbMessagingServiceHttpNg2 = _interopRequireDefault(_dataBbMessagingServiceHttpNg);
	
	var _messages = __webpack_require__(29);
	
	var _messages2 = _interopRequireDefault(_messages);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var moduleKey = exports.moduleKey = 'model-bb-messages-ng';
	var modelMessagesKey = exports.modelMessagesKey = 'model-bb-messages-ng:model';
	
	exports.default = _vendorBbAngular2.default.module('model-bb-messages-ng', [_dataBbMessagingServiceHttpNg2.default]).factory(modelMessagesKey, [_dataBbMessagingServiceHttpNg.messagingServiceDataKey, '$q', '$timeout',
	/* into */
	_messages2.default]).name;

/***/ }),

/***/ 27:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_27__;

/***/ }),

/***/ 28:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_28__;

/***/ }),

/***/ 29:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = Model;
	
	var _libBbModelErrors = __webpack_require__(30);
	
	/**
	 * @description Parses X500Principal string
	 * @param {String} X500Principal X500Principal format value
	 * @returns {String} when token matched, null when value not found
	 * @type {function}
	 * @private
	 */
	var getX500PrincipalValue = function getX500PrincipalValue(X500Principal) {
	  var regExp = new RegExp('CN=([^,]+)', 'i');
	  if (regExp.test(X500Principal)) {
	    var match = regExp.exec(X500Principal);
	    return match[1];
	  }
	
	  return null;
	};
	
	/**
	 * @description Model for widget-bb-messages-ng
	 * @param {MessagingData} messagingData A Data module to allow access to messaging data.
	 * @inner
	 * @return {MessagingModel}
	 */
	function Model(messagingData, Promise, $timeout) {
	  var userId = 'me';
	
	  /**
	   * @name MessagingModel#getRecipients
	   * @description Fetches available recipients
	   * @type {function}
	   * @returns {Promise.<Object>} A Promise with recipient data
	   */
	  function getRecipients() {
	    return messagingData.getMessageCenterUsersRecipients().then(function (res) {
	      return res.data.addresses.map(function (x500value) {
	        return {
	          name: getX500PrincipalValue(x500value),
	          address: x500value
	        };
	      });
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  }
	
	  /**
	   * @name MessagingModel#loadConversations
	   * @description Loads users conversation threads
	   * @type {function}
	   * @param {object} params query parameters to pass to backend
	   * @returns {Promise.<Object>} A wrapper of conversations in the following format:
	   *                             {conversations: [], totalCount: 0}
	   */
	  function loadConversations() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    return messagingData.getMessageCenterUsersConversations(userId, params).then(function (res) {
	      return {
	        conversations: res.data.conversations.map(function (conversation) {
	          return Object.assign({
	            otherUserName: getX500PrincipalValue(conversation.otherUser),
	            status: params.status || 'inbox'
	          }, conversation);
	        }),
	        totalCount: res.data.totalCount
	      };
	    }).catch(function (e) {
	      throw (0, _libBbModelErrors.fromHttpError)(e);
	    });
	  }
	
	  /**
	   * @name MessagingModel#loadArchivedConversations
	   * @description Loads archived users conversation threads
	   * @type {function}
	   * @param {object} params query parameters to pass to backend
	   * @returns {Promise.<Object>} A wrapper of conversations in the following format:
	   *                             {conversations: [], totalCount: 0}
	   */
	  function loadArchivedConversations() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    return loadConversations(Object.assign({ status: 'archived' }, params));
	  }
	
	  /**
	   * @name MessagingModel#loadSentConversations
	   * @description Loads sent users conversation threads
	   * @type {function}
	   * @param {object} params query parameters to pass to backend
	   * @returns {Promise.<Object>} A wrapper of conversations in the following format:
	   *                             {conversations: [], totalCount: 0}
	   */
	  function loadSentConversations() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    return loadConversations(Object.assign({ status: 'sent' }, params));
	  }
	
	  /**
	   * @name MessagingModel#loadMessages
	   * @description Loads messages of a given conversation
	   * @param {string} conversationId conversation identifier
	   * @type {function}
	   * @returns {Promise} An array of messages
	   */
	  function loadMessages(conversationId) {
	    return messagingData.getMessageCenterUsersConversationsMessages(userId, conversationId, {}).then(function (res) {
	      return res.data.messages.map(function (message) {
	        return Object.assign({
	          senderName: getX500PrincipalValue(message.sender)
	        }, message);
	      });
	    });
	  }
	
	  /**
	   * @name MessagingModel#getUnreadMessagesCount
	   * @description Gets user's unread messages count
	   * @type {function}
	   * @param {object} params query parameters to pass to backend
	   * @returns {Promise.<{unreadMessagesCount: number}>}
	   *          a promise holding user's unread messages count
	   */
	  function getUnreadMessagesCount() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    return messagingData.getMessageCenterUsersUnreadConversationCount(userId, params).then(function (res) {
	      return {
	        unreadMessagesCount: res.data.unreadCount
	      };
	    });
	  }
	
	  /**
	   * @name MessagingModel#removeConversations
	   * @description Removes given conversation
	   * @param {string} conversationId Conversation Id to be removed
	   * @type {function}
	   * @returns {Promise.<Object>} An array of conversations
	   */
	  function removeConversation(conversationId) {
	    return messagingData.deleteMessageCenterUsersConversationsRecord(userId, conversationId, {});
	  }
	
	  /**
	   * @name MessagingModel#loadDrafts
	   * @description Loads users drafts
	   * @type {function}
	   * @param {object} params query parameters to pass to backend
	   * @returns {Promise.<Object>} A wrapper of draft items: {"drafts": []}
	   */
	  function loadDrafts() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	
	    return messagingData.getUsersDrafts(userId, params).then(function (res) {
	      return res.data;
	    });
	  }
	
	  /**
	   * @name MessagingModel#createDraft
	   * @description Saves draft
	   * @param {any} pDraft Draft to save
	   * @type {function}
	   * @returns {Promise.<{id: string}>} a promise holding created draft ID
	   */
	  function createDraft(pDraft) {
	    var draft = Object.assign({}, pDraft, { recipients: [pDraft.recipients] });
	    return messagingData.postMessageCenterUsersDraftsRecord(userId, draft).then(function (res) {
	      return res.data;
	    });
	  }
	
	  function sendDraftWithRetry(draftId, messageBody) {
	    var attempt = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
	
	    return messagingData.postMessageCenterUsersDraftsSendDraftRequestRecord(userId, draftId, { body: messageBody }).catch(function (error) {
	      if (error.status === 404 && attempt < 3) {
	        return $timeout(function () {
	          return sendDraftWithRetry(draftId, messageBody, attempt + 1);
	        }, 1000);
	      }
	      throw error;
	    });
	  }
	
	  /**
	   * @name MessagingModel#sendDraft
	   * @description Sends given draft.
	   * @param {string} draftId Draft ID to be sent
	   * @param {messageBody} body with which message will be sent
	   * @type {function}
	   * @returns {Promise} an empty promise
	   */
	  function sendDraft(draftId, messageBody) {
	    return sendDraftWithRetry(draftId, messageBody);
	  }
	
	  /**
	   * @name MessagingModel#createReplyDraft
	   * @description Creates a draft for replying to a conversation
	   * @param {string} conversationId the conversation ID for which reply draft will be created
	   * @param {object} draft object with "body" property in it
	   * @type {function}
	   * @returns {Promise.<{id: string}>} a promise holding created draft ID
	   */
	  function createReplyDraft(conversationId, draft) {
	    return messagingData.postMessageCenterUsersConversationsDraftsRecord(userId, conversationId, draft).then(function (res) {
	      return res.data;
	    });
	  }
	
	  /**
	   * @name MessagingModel#updateReplyDraft
	   * @description Updates the response draft
	   * @param {string} conversationId the conversation ID for which response draft will be updated
	   * @param {object} draft object to be updated
	   * @type {function}
	   * @returns {Promise} an empty promise
	   */
	  function updateReplyDraft(conversationId, draft) {
	    return messagingData.putMessageCenterUsersConversationsDraftsRecord(userId, conversationId, draft.id, draft).then(function (res) {
	      return res.data;
	    });
	  }
	
	  function sortDraftsByUpdatedDateDescending(draftA, draftB) {
	    var dateA = new Date(draftA.updatedDate);
	    var dateB = new Date(draftB.updatedDate);
	
	    return dateB.getTime() - dateA.getTime();
	  }
	
	  /**
	   * @name MessagingModel#getLatestConversationDraft
	   * @description Gets latest (i.e. last updated) draft belonging to the specified conversation
	   * @param {string} conversationId the conversation ID for which draft will be fetched
	   * @type {function}
	   * @returns {Promise.<Object>} a promise containing latest conversation
	   */
	  function getLatestConversationDraft(conversationId) {
	    return messagingData.getMessageCenterUsersConversationsDrafts(userId, conversationId).then(function (res) {
	      return res.data.drafts && res.data.drafts.length ? res.data.drafts.sort(sortDraftsByUpdatedDateDescending)[0] : {};
	    });
	  }
	
	  /**
	   * @name MessaginModel#saveConversationDraft
	   * @description Saves (creates or updates) conversation draft.
	   * @param {string} conversationId the conversation ID for which draft will be saved
	   * @param {object} draft draft to be saved.
	   * @type {function}
	   * @return {Promise} promise object. If draft has been updated, then the Promise will be empty.
	   * If draft was created, then the promise will hold object with an ID of created draft.
	   */
	  function saveConversationDraft(conversationId, draft) {
	    return draft.id ? updateReplyDraft(conversationId, draft) : createReplyDraft(conversationId, draft);
	  }
	
	  function markMessageAsRead(conversationId, messageId) {
	    return messagingData.postMessageCenterUsersConversationsMessagesReadMessageRequestRecord(userId, conversationId, messageId);
	  }
	
	  /**
	   * @name MessagingModel#markUnreadMessagesAsRead(conversation, messages)
	   * @description Marks messages whose recipient is current user as read.
	   * @param {object} conversation conversation object for which messages need to be marked as read
	   * @param {array} messages array of messages. The array will be filtered and only messages whose
	   * recipient is current user will be marked as read.
	   */
	  function markUnreadMessagesAsRead(conversation, messages) {
	    var unreadMessages = (messages || []).filter(function (message) {
	      return message.status === 'UNREAD' && message.sender === conversation.otherUser;
	    });
	
	    var markAsReadRequests = unreadMessages.map(function (unreadMessage) {
	      return markMessageAsRead(conversation.id, unreadMessage.id);
	    });
	
	    return Promise.all(markAsReadRequests);
	  }
	
	  /**
	   * @name MessagingModel
	   * @type {object}
	   *
	   * @description
	   * Model factory for widget-bb-messaging-ng
	   */
	  return {
	    getRecipients: getRecipients,
	
	    // Conversations
	    loadConversations: loadConversations,
	    loadArchivedConversations: loadArchivedConversations,
	    loadSentConversations: loadSentConversations,
	
	    loadMessages: loadMessages,
	    getUnreadMessagesCount: getUnreadMessagesCount,
	    removeConversation: removeConversation,
	    getLatestConversationDraft: getLatestConversationDraft,
	    saveConversationDraft: saveConversationDraft,
	    markUnreadMessagesAsRead: markUnreadMessagesAsRead,
	
	    // Drafts
	    loadDrafts: loadDrafts,
	    createDraft: createDraft,
	    sendDraft: sendDraft,
	
	    // Response drafts
	    createReplyDraft: createReplyDraft,
	    updateReplyDraft: updateReplyDraft
	  };
	}

/***/ }),

/***/ 30:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_30__;

/***/ })

/******/ })
});
;
//# sourceMappingURL=model-bb-messages-ng.js.map