# ext-bb-turnovers-ng


Version: **1.1.3**

Default extension for widget-bb-turnovers-ng

## Imports

* lib-bb-styles
* ui-bb-chartjs-chart-bar-ng
* ui-bb-dropdown-select
* ui-bb-empty-state-ng
* ui-bb-format-amount
* ui-bb-i18n-ng
* ui-bb-substitute-error-ng
* vendor-bb-angular-ng-aria

---

## Table of Contents
- **ext-bb-turnovers-ng**<br/>    <a href="#ext-bb-turnovers-ngPERIODS">PERIODS</a><br/>    <a href="#ext-bb-turnovers-ngDATASET_LABELS">DATASET_LABELS</a><br/>    <a href="#ext-bb-turnovers-ngBAR_COUNT_BREAK_POINT">BAR_COUNT_BREAK_POINT</a><br/>    <a href="#ext-bb-turnovers-ngMAX_Y_TICKS">MAX_Y_TICKS</a><br/>    <a href="#ext-bb-turnovers-ngCSS_SELECTORS">CSS_SELECTORS</a><br/>    <a href="#ext-bb-turnovers-nggetPeriods">getPeriods()</a><br/>    <a href="#ext-bb-turnovers-ngperiodToDate">periodToDate(period)</a><br/>    <a href="#ext-bb-turnovers-nggetDefaultPeriod">getDefaultPeriod()</a><br/>    <a href="#ext-bb-turnovers-ngchartPlugins">chartPlugins</a><br/>    <a href="#ext-bb-turnovers-ngcustomizeTooltip">customizeTooltip(tooltip, element, data, chart)</a><br/>    <a href="#ext-bb-turnovers-ngformatX">formatX(ticks, data)</a><br/>    <a href="#ext-bb-turnovers-ngformatY">formatY(ticks, data)</a><br/>    <a href="#ext-bb-turnovers-ngchartOptions">chartOptions</a><br/>    <a href="#ext-bb-turnovers-nghasDataToDraw">hasDataToDraw(series)</a><br/>    <a href="#ext-bb-turnovers-ngdefaultPeriodStart">defaultPeriodStart()</a><br/>    <a href="#ext-bb-turnovers-ngdefaultInterval">defaultInterval(interval)</a><br/>    <a href="#ext-bb-turnovers-ngprocessTurnoverSeries">processTurnoverSeries(series, data)</a><br/>    <a href="#ext-bb-turnovers-ngprocessLoadError">processLoadError(The)</a><br/>
- **Type Definitions**<br/>    <a href="#CSS">CSS</a><br/>    <a href="#Period">Period</a><br/>    <a href="#Interval">Interval</a><br/>    <a href="#Turnover">Turnover</a><br/>    <a href="#TurnoverItem">TurnoverItem</a><br/>    <a href="#BBSeries">BBSeries</a><br/>    <a href="#TurnoversBBSeries">TurnoversBBSeries</a><br/>    <a href="#Dataset">Dataset</a><br/>    <a href="#TurnoversDataset">TurnoversDataset</a><br/>    <a href="#ChartjsSettings">ChartjsSettings</a><br/>

---
### <a name="ext-bb-turnovers-ngPERIODS"></a>*PERIODS*

Periods definition array

**Type:** *Array of <a href="#Period">Period</a>*


---
### <a name="ext-bb-turnovers-ngDATASET_LABELS"></a>*DATASET_LABELS*

Array of dataset labels

**Type:** *Array of String*


---
### <a name="ext-bb-turnovers-ngBAR_COUNT_BREAK_POINT"></a>*BAR_COUNT_BREAK_POINT*

Number of bars from which they should be put closer together

**Type:** *Number*


---
### <a name="ext-bb-turnovers-ngMAX_Y_TICKS"></a>*MAX_Y_TICKS*

Maximum number of ticks on Y axis

**Type:** *Number*


---
### <a name="ext-bb-turnovers-ngCSS_SELECTORS"></a>*CSS_SELECTORS*

Object with all selectors needed for correct styling of canvas parts

**Type:** *<a href="#CSS">CSS</a>*


---

### <a name="ext-bb-turnovers-nggetPeriods"></a>*getPeriods()*

Retrieves list of all periods

##### Returns

Array of <a href="#Period">Period</a> - *List of all available periods*

---

### <a name="ext-bb-turnovers-ngperiodToDate"></a>*periodToDate(period)*

Checks period object and converts it into format needed
for request (yyyy-mm-dd)

| Parameter | Type | Description |
| :-- | :-- | :-- |
| period | <a href="#Period">Period</a> |  |

##### Returns

String - *Formatted date*

---

### <a name="ext-bb-turnovers-nggetDefaultPeriod"></a>*getDefaultPeriod()*

Finds default period from period list

##### Returns

<a href="#Period">Period</a> - *Period marked as default*

---

---
### <a name="ext-bb-turnovers-ngchartPlugins"></a>*chartPlugins*

Array of plugins used to transform Chart.js rendering in the extension

**Type:** *Array*


---

### <a name="ext-bb-turnovers-ngcustomizeTooltip"></a>*customizeTooltip(tooltip, element, data, chart)*

Creates custom tooltip content and places tooltip element on top
of the currently active bar

| Parameter | Type | Description |
| :-- | :-- | :-- |
| tooltip | Object | object containing tooltip related data like positions, current data point, styling from chart options, etc. |
| element | Object | in DOM |
| data | <a href="#TurnoversBBSeries">TurnoversBBSeries</a> | array of data used to draw the chart |
| chart | Object | instance |

---

### <a name="ext-bb-turnovers-ngformatX"></a>*formatX(ticks, data)*

X axis tick formatter

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ticks | Array | Array of scale ticks |
| data | <a href="#TurnoversBBSeries">TurnoversBBSeries</a> | Chart series |

##### Returns

Array - *Formatted array of ticks*

---

### <a name="ext-bb-turnovers-ngformatY"></a>*formatY(ticks, data)*

Y axis tick formatter

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ticks | Array | Array of scale ticks |
| data | <a href="#TurnoversBBSeries">TurnoversBBSeries</a> | Chart series |

##### Returns

Array - *Formatted array of ticks*

---
### <a name="ext-bb-turnovers-ngchartOptions"></a>*chartOptions*

Object with chart options that need to be overriden

**Type:** *<a href="#ChartjsSettings">ChartjsSettings</a>*


---

### <a name="ext-bb-turnovers-nghasDataToDraw"></a>*hasDataToDraw(series)*

Checks chart series object to see if there are actual chart points to draw

| Parameter | Type | Description |
| :-- | :-- | :-- |
| series | <a href="#TurnoversBBSeries">TurnoversBBSeries</a> | Chart series |

##### Returns

Boolean - **

---

### <a name="ext-bb-turnovers-ngdefaultPeriodStart"></a>*defaultPeriodStart()*

Sets period start property on init

##### Returns

String - *Start period string in format yyyy-mm-dd*

---

### <a name="ext-bb-turnovers-ngdefaultInterval"></a>*defaultInterval(interval)*

Sets interval property on init

| Parameter | Type | Description |
| :-- | :-- | :-- |
| interval | <a href="#Interval">Interval</a> | Available intervals |

##### Returns

String - *One of the available intervals*

---

### <a name="ext-bb-turnovers-ngprocessTurnoverSeries"></a>*processTurnoverSeries(series, data)*

Default hook for turnovers chart series object post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| series | <a href="#BBSeries">BBSeries</a> | chart series data |
| data | <a href="#Turnover">Turnover</a> | original turnover object |

##### Returns

<a href="#TurnoversBBSeries">TurnoversBBSeries</a> - *processed series*

---

### <a name="ext-bb-turnovers-ngprocessLoadError"></a>*processLoadError(The)*

Overwrite the default hook and don't return passed error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| The | Error | error passed |

##### Returns

String - *The actual error*

## Type Definitions


### <a name="CSS"></a>*CSS*

Object that containes all CSS selectors needed to style canvas parts

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| axisBase | String | Axis CSS selector prefix |
| axisX | String | X axis CSS selector |
| axisY | String | Y axis CSS selector |
| arrowOuter | String | Chart's tooltip CSS selector (outer) |
| arrowInner | String | Chart's tooltip CSS selector (inner) |
| layoutBreak | String | Selector for getting breaking point between small and medium screen |
| arrowNear | String | CSS class for tooltip's arrow moved to the front |
| arrowFar | String | CSS class for tooltip's arrow moved to the back |
| legend | String | CSS class for legend wrapper |

### <a name="Period"></a>*Period*

Object used to create list of period options

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| interval | <a href="#Interval">Interval</a> | Interval object |
| duration | Number | Number of intervals (for creation period of multiple days, weeks, months...) |
| label | String | Key used to generate localized title for the option |
| default | Boolean (optional) | Optional flag to mark default period. If there is no default, first period will be shown |

### <a name="Interval"></a>*Interval*

Interval object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| DAY | String | Daily interval |
| WEEK | String | Weekly interval |
| MONTH | String | Monthly interval |
| YEAR | String | Yearly interval |

### <a name="Turnover"></a>*Turnover*

Turnover response object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| arrangementId | String | Id of the arrangement this turnover belongs to |
| intervalDuration | String | Duration of intervals returned |
| turnovers | Array of <a href="#TurnoverItem">TurnoverItem</a> | Array of turnover items |

### <a name="TurnoverItem"></a>*TurnoverItem*

Turnover response item

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| intervalStartDate | String | Date in ISO format (2016-06-01T16:41:41.090Z) |
| debitAmount | Object | Debit amount object |
| debitAmount.currencyCode | String | Debit amount currency code (ISO) |
| debitAmount.amount | Number | Debit amount value |
| creditAmount | Object | Credit amount object |
| creditAmount.currencyCode | String | Credit amount currency code (ISO) |
| creditAmount.amount | Number | Credit amount value |
| balance | Object | Debit and credit difference object |
| balance.currencyCode | String | Debit and credit difference currency code (ISO) |
| balance.amount | Number | Debit and credit difference value |

### <a name="BBSeries"></a>*BBSeries*

BBSeries data object used to draw charts

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| labels | Array of String | Array of x axis labels |
| datasets | Array of <a href="#Dataset">Dataset</a> | Array of all y axis value datasets |

### <a name="TurnoversBBSeries"></a>*TurnoversBBSeries*

Turnovers specific BBSeries object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| labels | Array of String | Array of x axis labels |
| datasets | Array of <a href="#TurnoversDataset">TurnoversDataset</a> | Array of all y axis value datasets |
| original | <a href="#Turnover">Turnover</a> | Original turnover object |
| updated | Boolean | Flag that signals that series are processed by hook |

### <a name="Dataset"></a>*Dataset*

Dataset object for y axis data

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of Number | Array of data points to be drawn for each label |

### <a name="TurnoversDataset"></a>*TurnoversDataset*

Turnovers specific dataset object for y axis

*Extends*: <a href="#Dataset">Dataset</a>

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| backgroundColor | String | Background color of bars that represent this dataset |
| hoverBackgroundColor | String | Hover color of bars that represent this dataset |

### <a name="ChartjsSettings"></a>*ChartjsSettings*

Settings object with options available for bar chart.
More info <a href="http://www.chartjs.org/docs/latest/charts/bar.html">http://www.chartjs.org/docs/latest/charts/bar.html</a>

**Type:** *Object*


---
