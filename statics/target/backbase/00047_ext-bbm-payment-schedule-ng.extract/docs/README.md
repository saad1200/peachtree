# ext-bbm-payment-schedule-ng


Version: **1.0.68**

Mobile extension for the payment schedule view in the Mobile payment widget.

## Imports

* lib-bbm-plugins
* ui-bb-i18n-ng
* ui-bbm-datepicker-ng
* ui-bbm-dropdown-ng
* ui-bbm-textfield-ng

---

## Example

```javascript
<!-- File model.xml of widget-bbm-payment-ng -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-payment-schedule-ng</value>
</property>
```

## Table of Contents
- **Helpers**<br/>    <a href="#Helpers#getFrequencyOptions">#getFrequencyOptions()</a><br/>    <a href="#Helpers#getMinimumRecurrenceEndDate">#getMinimumRecurrenceEndDate()</a><br/>    <a href="#Helpers#getMinimumExecutionDate">#getMinimumExecutionDate()</a><br/>    <a href="#Helpers#getMinimumStartDate">#getMinimumStartDate()</a><br/>    <a href="#Helpers#getRecurrenceEndingOptions">#getRecurrenceEndingOptions()</a><br/>    <a href="#Helpers#getStartDateLabel">#getStartDateLabel(ctrl)</a><br/>    <a href="#Helpers#getStartDateTitle">#getStartDateTitle(ctrl)</a><br/>    <a href="#Helpers#isPaymentRecurring">#isPaymentRecurring(ctrl)</a><br/>    <a href="#Helpers#isRecurrenceRepeatVisible">#isRecurrenceRepeatVisible(ctrl)</a><br/>    <a href="#Helpers#isRecurrenceEndDateVisible">#isRecurrenceEndDateVisible(ctrl)</a><br/>    <a href="#Helpers#onScheduleFormSubmit">#onScheduleFormSubmit(ctrl)</a><br/>
- **Type Definitions**<br/>    <a href="#PaymentFrequency">PaymentFrequency</a><br/>

---

## Helpers

Helpers for ext-bbm-payment-schedule-ng

### <a name="Helpers#getFrequencyOptions"></a>*#getFrequencyOptions()*

Returns a list of frequency options.

##### Returns

Array of <a href="#PaymentFrequency">PaymentFrequency</a> - **

### <a name="Helpers#getMinimumRecurrenceEndDate"></a>*#getMinimumRecurrenceEndDate()*

Returns a minimum allowed date to start a recurring payment.

##### Returns

String - **

### <a name="Helpers#getMinimumExecutionDate"></a>*#getMinimumExecutionDate()*

Returns a minimum allowed date to make a payment.

##### Returns

String - **

### <a name="Helpers#getMinimumStartDate"></a>*#getMinimumStartDate()*

Returns a minimum allowed date to start a recurring payment.

##### Returns

String - **

### <a name="Helpers#getRecurrenceEndingOptions"></a>*#getRecurrenceEndingOptions()*

Returns a list of possible recurring payment endings.

##### Returns

Array of <a href="#RecurrenceEnding">RecurrenceEnding</a> - **

### <a name="Helpers#getStartDateLabel"></a>*#getStartDateLabel(ctrl)*

Returns the label of the start date field.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#ScheduleController">ScheduleController</a> | Instance of the controller |

##### Returns

String - **

### <a name="Helpers#getStartDateTitle"></a>*#getStartDateTitle(ctrl)*

Returns the title of the start date datepicker.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#ScheduleController">ScheduleController</a> | Instance of the controller |

##### Returns

String - **

### <a name="Helpers#isPaymentRecurring"></a>*#isPaymentRecurring(ctrl)*

Checks whether the payment is recurring.
Returns true, if the payment is recurring, false otherwise.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#ScheduleController">ScheduleController</a> |  |

##### Returns

Boolean - **

### <a name="Helpers#isRecurrenceRepeatVisible"></a>*#isRecurrenceRepeatVisible(ctrl)*

Defines whether the recurrence repeat field should be visible.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#ScheduleController">ScheduleController</a> |  |

##### Returns

Boolean - **

### <a name="Helpers#isRecurrenceEndDateVisible"></a>*#isRecurrenceEndDateVisible(ctrl)*

Defines whether the recurrence end date field should be visible.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#ScheduleController">ScheduleController</a> |  |

##### Returns

Boolean - **

### <a name="Helpers#onScheduleFormSubmit"></a>*#onScheduleFormSubmit(ctrl)*

Handles submit of the schedule payment form.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#ScheduleController">ScheduleController</a> | Instance of the controller |

##### Returns

String - **

## Type Definitions


### <a name="PaymentFrequency"></a>*PaymentFrequency*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | Frequency identifier |
| text | String | Frequency as a text to be displayed |

---
