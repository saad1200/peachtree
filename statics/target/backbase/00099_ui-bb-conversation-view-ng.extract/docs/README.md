# ui-bb-conversation-view-ng


Version: **1.0.16**


## Imports

* vendor-bb-angular

---

## Table of Contents

---

## uiBbConversationViewNg

Conversation view directive which can be used to display a list of messages in conversation.

| Property | Type | Description |
| :-- | :-- | :-- |
| conversation | Object | conversation to show |
| messages | Object | messages belonging to the conversation. |
| draft | Object | optional draft object to be displayed in response field |
| on-close | Function | function to be called when the view is closed |
| on-reply-send | Function | function to be called to send a response draft |

## Example

```javascript
<ui-bb-conversation-view-ng on-click="$conversationViewCtrl.viewConversation(conversation)"
  conversation="$conversationViewCtrl.conversation"
  messages="$conversationViewCtrl.messages"
  draft="$conversationViewCtrl.draft"
  on-close="$conversationViewCtrl.close()"
  on-reply-send="$conversationViewCtrl.sendReply(draft)">
</ui-bb-conversation-view-ng>
```
