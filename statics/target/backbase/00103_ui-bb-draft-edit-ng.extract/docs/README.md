# ui-bb-draft-edit-ng


Version: **1.0.16**


## Imports

* vendor-bb-angular

---

## Table of Contents
- **Type Definitions**<br/>    <a href="#UiBbDraftEditNgConfig">UiBbDraftEditNgConfig</a><br/>    <a href="#UiBbDraftEditNgMessages">UiBbDraftEditNgMessages</a><br/>    <a href="#Recipient">Recipient</a><br/>

---

## uiBbDraftEditNg


| Property | Type | Description |
| :-- | :-- | :-- |
| config | <a href="#UiBbDraftEditNgConfig">UiBbDraftEditNgConfig</a> | config object |
| onClose | Function | function to call when user closes component |
| onSend | Function | function to call to send the draft |
| messages | <a href="#UiBbDraftEditNgMessages">UiBbDraftEditNgMessages</a> | object containing localized labels |

## Example

```javascript
<ui-bb-draft-edit-ng config="$draftCtrl.config"
                         on-close="$draftCtrl.dismiss()"
                         on-send="$draftCtrl.send(draft)"
                         messages="{
                                   formHeader: ('ui-bb-draft-ng.form.header' | i18n),
                                   formSubject: ('ui-bb-draft-ng.form.subject' | i18n),
                                   formTopic: ('ui-bb-draft-ng.form.topic' | i18n),
                                   formMessage: ('ui-bb-draft-ng.form.message' | i18n),
                                   formSend: ('ui-bb-draft-ng.form.send' | i18n),
                                   errorSend: ('ui-bb-draft-ng.error.send' | i18n),
                                   }">
      </ui-bb-draft-edit-ng>
```

## Type Definitions


### <a name="UiBbDraftEditNgConfig"></a>*UiBbDraftEditNgConfig*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| subjectMaxLength | Number | Max allowed length for new draft subject. |
| recipients | <a href="#Recipient">Recipient</a> | Recipients choice list for a new a draft. |

### <a name="UiBbDraftEditNgMessages"></a>*UiBbDraftEditNgMessages*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| formHeader | String | text for form heading |
| formTopic | String | label for topic/category/recipient field |
| formSubject | String | label for subject field |
| formMessage | String | label for message body text area |
| formSend | String | label for send button |
| errorSend | String | error to be shown if draft sending fails |

### <a name="Recipient"></a>*Recipient*

Recipient object holds recipient label to value mapping.
The keys of the object represent label shown to the user.
The values represent values to be sent to the backend API.

**Type:** *<a href="#Object<string">Object<string</a>, <a href="#string>">string></a>*


---
