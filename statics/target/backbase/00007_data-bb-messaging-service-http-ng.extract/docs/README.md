# data-bb-messaging-service-http-ng

A data module for accessing the Messaging Service REST API.

## Imports

* vendor-bb-angular

---

## Exports

### <a name="default"></a>*default*

Angular dependency injection module key

**Type:** *String*

### <a name="messagingServiceDataKey"></a>*messagingServiceDataKey*

Angular dependency injection key for the MessagingServiceData service

**Type:** *String*


---

## Example

```javascript
import messagingServiceDataModuleKey, {
  messagingServiceDataKey,
} from 'data-bb-messaging-service-http-ng';
```

---

## MessagingServiceData

Public api for data-bb-messaging-service-http-ng service

### <a name="MessagingServiceData#getMessageCenterUsersUnreadConversationCount"></a>*#getMessageCenterUsersUnreadConversationCount(userId, params)*

Returns a count of unread conversations for a given user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .getMessageCenterUsersUnreadConversationCount(userId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#getMessageCenterUsersRecipients"></a>*#getMessageCenterUsersRecipients(userId, params)*

Returns available contacts for user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .getMessageCenterUsersRecipients(userId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#getMessageCenterUsersDrafts"></a>*#getMessageCenterUsersDrafts(userId, params)*

Returns list of user drafts

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| params | Object (optional) | Map of query parameters. |
| params.sort | String (optional) | Comma separated field names. No prefix for ascending, prefix with - for descending. Eg: subject,-updatedDate. (defaults to -updatedDate) |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .getMessageCenterUsersDrafts(userId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#postMessageCenterUsersDraftsRecord"></a>*#postMessageCenterUsersDraftsRecord(userId, data)*

This method creates a draft

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .postMessageCenterUsersDraftsRecord(userId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#getMessageCenterUsersConversations"></a>*#getMessageCenterUsersConversations(userId, params)*

Returns preview of conversations available for user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| params | Object (optional) | Map of query parameters. |
| params.status | String (optional) | Returns only those conversations that have provided status. Currently only status=archived is supported. Missing query param would return only non archived non deleted conversations. |
| params.from | Number (optional) | The page from which the results are listed. Eg: 3. (defaults to 0) |
| params.size | Number (optional) | Specifies the number of results to be shown on the page. Eg: 20. (defaults to 10) |
| params.sort | String (optional) | Comma separated field names. No prefix for ascending, prefix with - for descending. Eg: subject,-timestamp. (defaults to -timestamp) |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .getMessageCenterUsersConversations(userId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#putMessageCenterUsersDraftsRecord"></a>*#putMessageCenterUsersDraftsRecord(userId, draftId, data)*

This method updates draft with given id for given user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| draftId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .putMessageCenterUsersDraftsRecord(userId, draftId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#deleteMessageCenterUsersDraftsRecord"></a>*#deleteMessageCenterUsersDraftsRecord(userId, draftId, data)*

This method deletes existing draft

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| draftId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .deleteMessageCenterUsersDraftsRecord(userId, draftId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#postMessageCenterUsersDraftsSendDraftRequestRecord"></a>*#postMessageCenterUsersDraftsSendDraftRequestRecord(userId, draftId, data)*

This method creates send request for draft. If body is added draft will be updated with provided data.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| draftId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .postMessageCenterUsersDraftsSendDraftRequestRecord(userId, draftId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#deleteMessageCenterUsersConversationsRecord"></a>*#deleteMessageCenterUsersConversationsRecord(userId, conversationId, data)*

This method deletes conversation for given user. However conversation may be resurrected if another party updates it.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .deleteMessageCenterUsersConversationsRecord(userId, conversationId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#postMessageCenterUsersConversationsArchiveConversationRequestRecord"></a>*#postMessageCenterUsersConversationsArchiveConversationRequestRecord(userId, conversationId, data)*

This method puts given conversation in user's archive box

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .postMessageCenterUsersConversationsArchiveConversationRequestRecord(userId, conversationId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#getMessageCenterUsersConversationsDrafts"></a>*#getMessageCenterUsersConversationsDrafts(userId, conversationId, params)*

Returns drafts that have been created in conversation by given user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| params | Object (optional) | Map of query parameters. |
| params.status | String (optional) | statuses of drafts to return. No statuses would match no drafts. |
| params.limit | Number (optional) | maximum amount of drafts to provide. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .getMessageCenterUsersConversationsDrafts(userId, conversationId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#postMessageCenterUsersConversationsDraftsRecord"></a>*#postMessageCenterUsersConversationsDraftsRecord(userId, conversationId, data)*

This method creates a draft in conversation for user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .postMessageCenterUsersConversationsDraftsRecord(userId, conversationId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#getMessageCenterUsersConversationsMessages"></a>*#getMessageCenterUsersConversationsMessages(userId, conversationId, params)*

Returns all messages that have been sent in conversation by all parties

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .getMessageCenterUsersConversationsMessages(userId, conversationId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#putMessageCenterUsersConversationsDraftsRecord"></a>*#putMessageCenterUsersConversationsDraftsRecord(userId, conversationId, draftId, data)*

This method updates draft in conversation for user

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| draftId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .putMessageCenterUsersConversationsDraftsRecord(userId, conversationId, draftId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="MessagingServiceData#postMessageCenterUsersConversationsMessagesReadMessageRequestRecord"></a>*#postMessageCenterUsersConversationsMessagesReadMessageRequestRecord(userId, conversationId, messageId, data)*

This method marks message as read

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| conversationId | String |  |
| messageId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
messagingServiceData
 .postMessageCenterUsersConversationsMessagesReadMessageRequestRecord(userId, conversationId, messageId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```
### <a name="MessagingServiceData#schemas"></a>*#schemas*

Schema data. Keys of the object are names of the POST and PUT methods

Note: The schema is not strictly a JSON schema. It is a whitelisted set of
keys for each object property. The keys that are exposed are meant for validation
purposes.

The full list of *possible* keys for each property is:
type, minimum, maximum, minLength, maxLength, pattern, enum, format, default,
properties, items, minItems, maxItems, uniqueItems and required.

See http://json-schema.org/latest/json-schema-validation.html for more details
on the meaning of these keys.

The "required" array from JSON schema is tranformed into a "required" boolean
on each property. This is for ease of use.

**Type:** *Object*

### <a name="MessagingServiceData#schemas.postMessageCenterUsersDraftsRecord"></a>*#schemas.postMessageCenterUsersDraftsRecord*

An object describing the JSON schema for the postMessageCenterUsersDraftsRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "body": {
      "type": "string",
      "required": false
    },
    "subject": {
      "type": "string",
      "required": false
    },
    "category": {
      "type": "string",
      "required": false
    },
    "recipients": {
      "type": "array",
      "items": {
        "properties": {}
      },
      "required": false
    },
    "important": {
      "type": "boolean",
      "required": false
    }
  }
}
```
### <a name="MessagingServiceData#schemas.putMessageCenterUsersDraftsRecord"></a>*#schemas.putMessageCenterUsersDraftsRecord*

An object describing the JSON schema for the putMessageCenterUsersDraftsRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "body": {
      "type": "string",
      "required": false
    },
    "subject": {
      "type": "string",
      "required": false
    },
    "category": {
      "type": "string",
      "required": false
    },
    "recipients": {
      "type": "array",
      "items": {
        "properties": {}
      },
      "required": false
    },
    "important": {
      "type": "boolean",
      "required": false
    }
  }
}
```
### <a name="MessagingServiceData#schemas.postMessageCenterUsersDraftsSendDraftRequestRecord"></a>*#schemas.postMessageCenterUsersDraftsSendDraftRequestRecord*

An object describing the JSON schema for the postMessageCenterUsersDraftsSendDraftRequestRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "body": {
      "type": "string",
      "required": false
    }
  }
}
```
### <a name="MessagingServiceData#schemas.postMessageCenterUsersConversationsDraftsRecord"></a>*#schemas.postMessageCenterUsersConversationsDraftsRecord*

An object describing the JSON schema for the postMessageCenterUsersConversationsDraftsRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "body": {
      "type": "string",
      "required": false
    }
  }
}
```
### <a name="MessagingServiceData#schemas.putMessageCenterUsersConversationsDraftsRecord"></a>*#schemas.putMessageCenterUsersConversationsDraftsRecord*

An object describing the JSON schema for the putMessageCenterUsersConversationsDraftsRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "body": {
      "type": "string",
      "required": false
    }
  }
}
```

---

## MessagingServiceDataProvider

Data service that can be configured with custom base URI.

| Injector Key |
| :-- |
| *data-bb-messaging-service-http-ng:messagingServiceDataProvider* |


### <a name="MessagingServiceDataProvider#setBaseUri"></a>*#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | String | Base URI which will be the prefix for all HTTP requests |

### <a name="MessagingServiceDataProvider#$get"></a>*#$get()*


##### Returns

Object - *An instance of the service*

## Example

```javascript
// Configuring in an angular app:
angular.module(...)
  .config(['data-bb-messaging-service-http-ng:messagingServiceDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });

// Configuring With config-bb-providers-ng:
export default [
  ['data-bb-messaging-service-http-ng:messagingServiceDataProvider', (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
  }]
];
```

## Type Definitions


### <a name="Response"></a>*Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Object | See method descriptions for possible return types |
| headers | Function | Getter headers function |
| status | Number | HTTP status code of the response. |
| statusText | String | HTTP status text of the response. |

---
