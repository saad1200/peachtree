# widget-bb-spending-ng


Version: **1.1.12**

Spending analysis widget

## Imports

* lib-bb-event-bus-ng
* lib-bb-extension-helpers-ng
* lib-bb-intent-ng
* lib-bb-model-errors
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-spending-ng
* vendor-bb-angular

---

## Example

```javascript
<div ng-controller="SpendingAnalysisController as $ctrl">
  <ui-bb-chartjs-chart-donut-ng
    data-series="$ctrl.series"
  ></ui-bb-chartjs-chart-donut-ng>
</div>
```

## Table of Contents
- **widget-bb-spending-ng**<br/>    <a href="#widget-bb-spending-ngMS_IN_MIN">MS_IN_MIN</a><br/>    <a href="#widget-bb-spending-ngDEFAULT_PERIOD_START">DEFAULT_PERIOD_START</a><br/>    <a href="#widget-bb-spending-ngDEFAULT_PERIOD_END">DEFAULT_PERIOD_END</a><br/>    <a href="#widget-bb-spending-ngsetSelectedSpendingItem">setSelectedSpendingItem(item)</a><br/>    <a href="#widget-bb-spending-ng$onInit">$onInit()</a><br/>    <a href="#widget-bb-spending-ngonPeriodStartDateChanged">onPeriodStartDateChanged(startDate)</a><br/>    <a href="#widget-bb-spending-ngonPeriodEndDateChanged">onPeriodEndDateChanged(endDate)</a><br/>    <a href="#widget-bb-spending-ngonProductSelected">onProductSelected()</a><br/>    <a href="#widget-bb-spending-ngselectedProduct">selectedProduct</a><br/>    <a href="#widget-bb-spending-ngproducts">products</a><br/>    <a href="#widget-bb-spending-ngitems">items</a><br/>    <a href="#widget-bb-spending-ngperiodStartDate">periodStartDate</a><br/>    <a href="#widget-bb-spending-ngperiodEndDate">periodEndDate</a><br/>    <a href="#widget-bb-spending-ngselectedSpendingItem">selectedSpendingItem</a><br/>    <a href="#widget-bb-spending-ngisLoading">isLoading</a><br/>
- **default-hooks**<br/>    <a href="#default-hooks#processSpendingItems">#processSpendingItems(items)</a><br/>    <a href="#default-hooks#processSpendingSeries">#processSpendingSeries(series, data)</a><br/>    <a href="#default-hooks#onSpendingUpdate">#onSpendingUpdate(params)</a><br/>    <a href="#default-hooks#processSelectedProduct">#processSelectedProduct(product)</a><br/>    <a href="#default-hooks#processProductsList">#processProductsList(products)</a><br/>    <a href="#default-hooks#defaultPeriodStart">#defaultPeriodStart()</a><br/>    <a href="#default-hooks#defaultPeriodEnd">#defaultPeriodEnd()</a><br/>    <a href="#default-hooks#onSelectedItemChanged">#onSelectedItemChanged(item)</a><br/>    <a href="#default-hooks#processLoadError">#processLoadError(The)</a><br/>
- **Events**<br/>    <a href="#bb.event.product.selected">bb.event.product.selected</a><br/>    <a href="#widget-bb-spending-ng.load.failed">widget-bb-spending-ng.load.failed</a><br/>    <a href="#bb.event.spending.period.start.date.changed">bb.event.spending.period.start.date.changed</a><br/>    <a href="#bb.event.spending.period.end.date.changed">bb.event.spending.period.end.date.changed</a><br/>
- **Type Definitions**<br/>    <a href="#Spending">Spending</a><br/>    <a href="#SpendingItem">SpendingItem</a><br/>    <a href="#Amount">Amount</a><br/>    <a href="#ProductKinds">ProductKinds</a><br/>    <a href="#ProductKind">ProductKind</a><br/>    <a href="#Product">Product</a><br/>

---
### <a name="widget-bb-spending-ngMS_IN_MIN"></a>*MS_IN_MIN*

Amount of milliseconds in a minute

**Type:** *Number*


---
### <a name="widget-bb-spending-ngDEFAULT_PERIOD_START"></a>*DEFAULT_PERIOD_START*

First day in the current month

**Type:** *Number*


---
### <a name="widget-bb-spending-ngDEFAULT_PERIOD_END"></a>*DEFAULT_PERIOD_END*

Last day in the current month

**Type:** *Number*


---

## Intents

An object with all the intents names

---

### <a name="widget-bb-spending-ngsetSelectedSpendingItem"></a>*setSelectedSpendingItem(item)*

Setter for the selected spending item

| Parameter | Type | Description |
| :-- | :-- | :-- |
| item | <a href="#any">any</a> | selected item |

##### Returns

<a href="#void">void</a> - **

---

### <a name="widget-bb-spending-ng$onInit"></a>*$onInit()*

AngularJS Lifecycle hook used to initialize the controller


##### Returns

Promise of <a href="#void">void</a> - **

---

### <a name="widget-bb-spending-ngonPeriodStartDateChanged"></a>*onPeriodStartDateChanged(startDate)*

Handler to be called on period start date change

| Parameter | Type | Description |
| :-- | :-- | :-- |
| startDate | String | Date as string in format yyyy-mm-dd |

##### Returns

<a href="#void">void</a> - **

---

### <a name="widget-bb-spending-ngonPeriodEndDateChanged"></a>*onPeriodEndDateChanged(endDate)*

Handler to be called on period end date change

| Parameter | Type | Description |
| :-- | :-- | :-- |
| endDate | String | Date as string in format yyyy-mm-dd |

##### Returns

<a href="#void">void</a> - **

---

### <a name="widget-bb-spending-ngonProductSelected"></a>*onProductSelected()*

Handler to be used on product selection, is using
selected product value from <a href="#Hooks.processSelectedProduct">Hooks.processSelectedProduct</a> hook

##### Returns

<a href="#void">void</a> - **

---
### <a name="widget-bb-spending-ngselectedProduct"></a>*selectedProduct*

The selected product for spending evaluation.

**Type:** *<a href="#Product">Product</a>*


---
### <a name="widget-bb-spending-ngproducts"></a>*products*

List of products to be used by account selector for spending evaluation.
Is recieved from <a href="#Hooks.processProductsList">Hooks.processProductsList</a>

**Type:** *<a href="#ProductKinds">ProductKinds</a>*


---
### <a name="widget-bb-spending-ngitems"></a>*items*

The value returned from <a href="#Hooks.processItems">Hooks.processItems</a> hook.
null if the items aren't loaded.

**Type:** *<a href="#Spending">Spending</a>*


---

## series

The value returned from <a href="#Hooks.processSpendingSeries">Hooks.processSpendingSeries</a> hook.
Formatted for use within chart UI component.
null if the data isn't loaded

---
### <a name="widget-bb-spending-ngperiodStartDate"></a>*periodStartDate*

Date of the spending evaluation period start

**Type:** *String*


---
### <a name="widget-bb-spending-ngperiodEndDate"></a>*periodEndDate*

Date of the spending evaluation period end

**Type:** *String*


---
### <a name="widget-bb-spending-ngselectedSpendingItem"></a>*selectedSpendingItem*

selected spending item

**Type:** *<a href="#any">any</a>*


---
### <a name="widget-bb-spending-ngisLoading"></a>*isLoading*

Loading status

**Type:** *Boolean*


---

---

## default-hooks

Default hooks for widget-bb-spending-ng

### <a name="default-hooks#processSpendingItems"></a>*#processSpendingItems(items)*

Default hook for spending collection post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| items | <a href="#Spending">Spending</a> | to process |

##### Returns

<a href="#Spending">Spending</a> - *total spendings by category*

### <a name="default-hooks#processSpendingSeries"></a>*#processSpendingSeries(series, data)*

Default hook for spending chart series object post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| series | <a href="#BBSeries">BBSeries</a> | chart series data |
| data | <a href="#Spending">Spending</a> | original spending object |

##### Returns

Object - *processed series*

### <a name="default-hooks#onSpendingUpdate"></a>*#onSpendingUpdate(params)*

Process parameters before they are sent to the model's load method

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | to process |

##### Returns

Object - *processed params*

### <a name="default-hooks#processSelectedProduct"></a>*#processSelectedProduct(product)*

Default hook to process product then it's selected

| Parameter | Type | Description |
| :-- | :-- | :-- |
| product | <a href="#Product">Product</a> | which is selected |

##### Returns

<a href="#Product">Product</a> - *product after processing*

### <a name="default-hooks#processProductsList"></a>*#processProductsList(products)*

Process passed products list before passing it to the view.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| products | <a href="#ProductKinds">ProductKinds</a> | to process |

##### Returns

<a href="#ProductKinds">ProductKinds</a> - *processed products*

### <a name="default-hooks#defaultPeriodStart"></a>*#defaultPeriodStart()*

Sets period start property on init

##### Returns

String - *Start period string in format yyyy-mm-dd*

### <a name="default-hooks#defaultPeriodEnd"></a>*#defaultPeriodEnd()*

Sets period end property on init

##### Returns

String - *End period string in format yyyy-mm-dd*

### <a name="default-hooks#onSelectedItemChanged"></a>*#onSelectedItemChanged(item)*

Sets a selected spending item

| Parameter | Type | Description |
| :-- | :-- | :-- |
| item | <a href="#any">any</a> | selected spending item |

##### Returns

<a href="#any">any</a> - *- selected spending item*

### <a name="default-hooks#processLoadError"></a>*#processLoadError(The)*

Sets the error for missing parameters in the turnovers request

| Parameter | Type | Description |
| :-- | :-- | :-- |
| The | Error | error passed |

##### Returns

Error - *The actual error*

---

## Events

### <a name="bb.event.product.selected"></a>*bb.event.product.selected*

Triggered when product is selected.

### <a name="widget-bb-spending-ng.load.failed"></a>*widget-bb-spending-ng.load.failed*

Triggered when spending widget fails to load.

### <a name="bb.event.spending.period.start.date.changed"></a>*bb.event.spending.period.start.date.changed*

Triggered when period start date is changed.

### <a name="bb.event.spending.period.end.date.changed"></a>*bb.event.spending.period.end.date.changed*

Triggered when period end date is changed.


---

## Type Definitions


### <a name="Spending"></a>*Spending*

Spending response object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| totalSpending | Object | Total spending object |
| totalSpending.amount | Number | Total spending value |
| totalSpending.currencyCode | String | Total spending currency code (ISO) |
| items | Array of <a href="#SpendingItem">SpendingItem</a> | Array of spending items |

### <a name="SpendingItem"></a>*SpendingItem*

Spending response item

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| category | String | Transactions category |
| totalAmount | Object | The total amount of the aggregated transactions by category |
| totalAmount.currencyCode | String | Total amount currency code (ISO) |
| totalAmount.amount | Number | Total amount value |
| trend | Number | Percentage value of the trend |
| portion | Number | Percentage of the total spending for a given period |

### <a name="Amount"></a>*Amount*

Amount object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String | Currency code |
| value | Number |  |

### <a name="ProductKinds"></a>*ProductKinds*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of products |
| productKinds | Array of <a href="#ProductKind">ProductKind</a> | Array of Products Kinds |

### <a name="ProductKind"></a>*ProductKind*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | <a href="#Amount">Amount</a> | Total balance of product kind |
| name | <a href="#!string">!string</a> | Name of the product kind |
| products | Array of <a href="#Product">Product</a> | Array of associated products |

### <a name="Product"></a>*Product*

Product type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the Product |
| name | <a href="#!string">!string</a> | Name of the Product |
| kind | <a href="#!string">!string</a> | id of the ProductKind |
| alias | String | Alias of the Product |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | Currency code |
| PANSuffix | String | Primary Account Number Suffix |
| bookedBalance | String | Booked balance |
| availableBalance | String | Available balance |
| creditLimit | String | Credit limit |
| currentInvestmentValue | String | Current investment value |
| principalAmount | String | Principal amount |
| accruedInterest | String | Accrued interest |

---

## Templates

* *template.ng.html*

---
