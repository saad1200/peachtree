(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-event-bus-ng"), require("model-bb-spending-ng"), require("lib-bb-widget-extension-ng"), require("lib-bb-extension-helpers-ng"), require("lib-bb-intent-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-spending-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "lib-bb-model-errors", "lib-bb-event-bus-ng", "model-bb-spending-ng", "lib-bb-widget-extension-ng", "lib-bb-extension-helpers-ng", "lib-bb-intent-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-spending-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-event-bus-ng"), require("model-bb-spending-ng"), require("lib-bb-widget-extension-ng"), require("lib-bb-extension-helpers-ng"), require("lib-bb-intent-ng"));
	else
		root["widget-bb-spending-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"], root["lib-bb-event-bus-ng"], root["model-bb-spending-ng"], root["lib-bb-widget-extension-ng"], root["lib-bb-extension-helpers-ng"], root["lib-bb-intent-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_14__, __WEBPACK_EXTERNAL_MODULE_18__, __WEBPACK_EXTERNAL_MODULE_20__, __WEBPACK_EXTERNAL_MODULE_26__, __WEBPACK_EXTERNAL_MODULE_27__, __WEBPACK_EXTERNAL_MODULE_28__, __WEBPACK_EXTERNAL_MODULE_29__, __WEBPACK_EXTERNAL_MODULE_30__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(25);

/***/ }),
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */,
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_14__;

/***/ }),
/* 15 */,
/* 16 */,
/* 17 */,
/* 18 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_18__;

/***/ }),
/* 19 */,
/* 20 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_20__;

/***/ }),
/* 21 */,
/* 22 */,
/* 23 */,
/* 24 */,
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(14);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetNg = __webpack_require__(18);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _libBbEventBusNg = __webpack_require__(26);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _modelBbSpendingNg = __webpack_require__(27);
	
	var _modelBbSpendingNg2 = _interopRequireDefault(_modelBbSpendingNg);
	
	var _libBbWidgetExtensionNg = __webpack_require__(28);
	
	var _libBbWidgetExtensionNg2 = _interopRequireDefault(_libBbWidgetExtensionNg);
	
	var _libBbExtensionHelpersNg = __webpack_require__(29);
	
	var _libBbExtensionHelpersNg2 = _interopRequireDefault(_libBbExtensionHelpersNg);
	
	var _libBbIntentNg = __webpack_require__(30);
	
	var _libBbIntentNg2 = _interopRequireDefault(_libBbIntentNg);
	
	var _controller = __webpack_require__(31);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	var _defaultHooks = __webpack_require__(34);
	
	var _defaultHooks2 = _interopRequireDefault(_defaultHooks);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var moduleKey = 'widget-bb-spending-ng'; /**
	                                          * @module widget-bb-spending-ng
	                                          *
	                                          * @description
	                                          * Spending analysis widget
	                                          *
	                                          * @usage
	                                          * <div ng-controller="SpendingAnalysisController as $ctrl">
	                                          *   <ui-bb-chartjs-chart-donut-ng
	                                          *     data-series="$ctrl.series"
	                                          *   ></ui-bb-chartjs-chart-donut-ng>
	                                          * </div>
	                                          */
	
	var hooksKey = moduleKey + ':hooks';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_libBbWidgetNg2.default, _libBbEventBusNg2.default, _libBbIntentNg2.default, _modelBbSpendingNg2.default, _libBbExtensionHelpersNg2.default]).factory(_libBbExtensionHelpersNg.extensionHelpersContextKey, ['$compile', '$rootScope', function ($compile, $rootScope) {
	  return {
	    $compile: $compile,
	    $rootScope: $rootScope
	  };
	}]).factory(hooksKey, (0, _libBbWidgetExtensionNg2.default)(_defaultHooks2.default)).controller('SpendingController', [
	// dependencies to inject
	_libBbEventBusNg.eventBusKey, hooksKey, _modelBbSpendingNg.modelSpendingKey, _libBbIntentNg.bbIntentKey,
	/* into */
	_controller2.default]).run([_libBbEventBusNg.eventBusKey, _libBbWidgetNg.widgetKey, function (bus, widget) {
	  bus.publish('cxp.item.loaded', {
	    id: widget.getId()
	  });
	}]).name;
	
	/**
	 * Spending response object
	 * @typedef {object} Spending
	 * @property {object} totalSpending Total spending object
	 * @property {number} totalSpending.amount Total spending value
	 * @property {string} totalSpending.currencyCode Total spending currency code (ISO)
	 * @property {SpendingItem[]} items Array of spending items
	 */
	
	/**
	 * Spending response item
	 * @typedef {object} SpendingItem
	 * @property {string} category Transactions category
	 * @property {object} totalAmount The total amount of the aggregated transactions by category
	 * @property {string} totalAmount.currencyCode Total amount currency code (ISO)
	 * @property {number} totalAmount.amount Total amount value
	 * @property {number} trend Percentage value of the trend
	 * @property {number} portion Percentage of the total spending for a given period
	 */
	
	/**
	 * Amount object
	 * @typedef {object} Amount
	 * @property {string} currency Currency code
	 * @property {number} value
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKinds
	 * @property {Amount} aggregatedBalance Total balance of products
	 * @property {ProductKind[]} productKinds Array of Products Kinds
	 */
	
	/**
	 * ProductKind type definition
	 * @typedef {object} ProductKind
	 * @property {Amount} aggregatedBalance Total balance of product kind
	 * @property {!string} name Name of the product kind
	 * @property {Product[]} products Array of associated products
	 */
	
	/**
	 * Product type definition
	 * @typedef {object} Product
	 * @property {!string} id id of the Product
	 * @property {!string} name Name of the Product
	 * @property {!string} kind id of the ProductKind
	 * @property {string} alias Alias of the Product
	 * @property {string} IBAN International Bank Account Number
	 * @property {string} BBAN Basic Bank Account Number
	 * @property {string} currency Currency code
	 * @property {string} PANSuffix Primary Account Number Suffix
	 * @property {string} bookedBalance Booked balance
	 * @property {string} availableBalance Available balance
	 * @property {string} creditLimit Credit limit
	 * @property {string} currentInvestmentValue Current investment value
	 * @property {string} principalAmount Principal amount
	 * @property {string} accruedInterest Accrued interest
	 */

/***/ }),
/* 26 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_26__;

/***/ }),
/* 27 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_27__;

/***/ }),
/* 28 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_28__;

/***/ }),
/* 29 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_29__;

/***/ }),
/* 30 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_30__;

/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = SpendingController;
	
	var _libBbModelErrors = __webpack_require__(20);
	
	var _message = __webpack_require__(32);
	
	var _message2 = _interopRequireDefault(_message);
	
	var _constants = __webpack_require__(33);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; } /**
	                                                                                                                                                                                                                   * @module widget-bb-spending-ng
	                                                                                                                                                                                                                   * @name SpendingController
	                                                                                                                                                                                                                   *
	                                                                                                                                                                                                                   * @description
	                                                                                                                                                                                                                   * Spending
	                                                                                                                                                                                                                   */
	
	var PRODUCT_SELECTED = _message2.default.PRODUCT_SELECTED,
	    SPENDING_LOAD_FAILED = _message2.default.SPENDING_LOAD_FAILED,
	    PERIOD_START_CHANGED = _message2.default.PERIOD_START_CHANGED,
	    PERIOD_END_CHANGED = _message2.default.PERIOD_END_CHANGED;
	function SpendingController(bus, hooks, model, bbIntent) {
	  var $ctrl = this;
	
	  /**
	   * @description
	   * A set of intents that the controller uses or handles.
	   *
	   * @name intents
	   * @type {Object}
	   * @inner
	   */
	  var intents = {};
	
	  /**
	  * @description
	  * The intent to select a spending item
	  *
	  * @name intents#selectSpendingItem
	  * @type {function}
	  * @inner
	  */
	  intents.selectSpendingItem = bbIntent.create(_constants.Intent.SELECT_SPENDING_ITEM, function () {});
	
	  bbIntent.init(function () {});
	
	  /**
	   * Converts error code to error message translation key
	   *
	   * @inner
	   * @name errorMessage
	   * @type {function}
	   * @param {string} code Error code
	   * @returns {string} Error message translation key
	   */
	  var errorMessage = function errorMessage(code) {
	    var _E_AUTH$E_CONNECTIVIT;
	
	    return (_E_AUTH$E_CONNECTIVIT = {}, _defineProperty(_E_AUTH$E_CONNECTIVIT, _libBbModelErrors.E_AUTH, 'error.load.auth'), _defineProperty(_E_AUTH$E_CONNECTIVIT, _libBbModelErrors.E_CONNECTIVITY, 'error.load.connectivity'), _defineProperty(_E_AUTH$E_CONNECTIVIT, _libBbModelErrors.E_USER, 'error.load.user'), _defineProperty(_E_AUTH$E_CONNECTIVIT, model.E_PARAMS, model.E_PARAMS), _E_AUTH$E_CONNECTIVIT)[code] || 'error.load.unexpected';
	  };
	
	  /**
	   * @description
	   * Setter for the selected spending item.
	   * It creates an intent to select a spending item 'view.account.category.transactions'
	   * that can be handled by transaction widget
	   *
	   * @name setSelectedSpendingItem
	   * @param {any} item -  selected item
	   * @type {function}
	   * @returns {void}
	   */
	  var setSelectedSpendingItem = function setSelectedSpendingItem(item) {
	    $ctrl.selectedSpendingItem = hooks.onSelectedItemChanged(item);
	    intents.selectSpendingItem($ctrl.selectedSpendingItem);
	  };
	
	  /**
	   * Updates spending list based on selected product
	   *
	   * @inner
	   * @name updateSpending
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateSpending = function updateSpending() {
	    return model.validateSpendingParameters(hooks.onSpendingUpdate({
	      arrangementId: $ctrl.selectedProduct ? $ctrl.selectedProduct.id : null,
	      periodStartDate: $ctrl.periodStartDate,
	      periodEndDate: $ctrl.periodEndDate
	    })).then(model.loadSpending).then(function (loaded) {
	      $ctrl.items = hooks.processSpendingItems(loaded);
	      $ctrl.series = hooks.processSpendingSeries(model.transformToSeries(loaded), loaded);
	      setSelectedSpendingItem(null);
	    }).catch(function (error) {
	      $ctrl.error = hooks.processLoadError(errorMessage(error.code));
	      bus.publish(SPENDING_LOAD_FAILED, { error: error });
	    });
	  };
	
	  /**
	   * Updates selected product
	   *
	   * @inner
	   * @name updateProductSelected
	   * @type {function}
	   * @param {any} selectedProduct - selected product to be used
	   */
	  var updateProductSelected = function updateProductSelected(selectedProduct) {
	    $ctrl.selectedProduct = hooks.processSelectedProduct(selectedProduct);
	    model.setSelectedProduct($ctrl.selectedProduct);
	  };
	
	  /**
	   * Adds subscriptions to bus events
	   * @inner
	   * @name bindEvents
	   * @type {function}
	   */
	  function bindEvents() {
	    bus.subscribe(PRODUCT_SELECTED, function (selectedProduct) {
	      updateProductSelected(selectedProduct.product);
	      updateSpending();
	    });
	    bus.subscribe(PERIOD_START_CHANGED, function (startDate) {
	      $ctrl.periodStartDate = startDate;
	      updateSpending();
	    });
	    bus.subscribe(PERIOD_END_CHANGED, function (endDate) {
	      $ctrl.periodEndDate = endDate;
	      updateSpending();
	    });
	  }
	
	  /**
	   * Initializes period data via hooks
	   *
	   * @inner
	   * @name initPeriodData
	   * @type {function}
	   */
	  var initPeriodData = function initPeriodData() {
	    $ctrl.periodStartDate = hooks.defaultPeriodStart();
	    $ctrl.periodEndDate = hooks.defaultPeriodEnd();
	  };
	
	  /**
	   * Updates the products list for the ui-bb-account-selector.
	   *
	   * @inner
	   * @name updateProductsList
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var updateProductsList = function updateProductsList(getFromStorage) {
	    return model.getProductsArray(getFromStorage).then(function (products) {
	      $ctrl.products = hooks.processProductsList(products);
	      return $ctrl.products;
	    }).catch(function (error) {
	      $ctrl.error = errorMessage(error.code);
	      bus.publish(SPENDING_LOAD_FAILED, { error: error });
	    });
	  };
	
	  /**
	   * AngularJS Lifecycle hook used to initialize the controller
	   *
	   * @name $onInit
	   * @type {function}
	   * @returns {Promise.<void>}
	   */
	  var $onInit = function $onInit() {
	    $ctrl.isLoading = true;
	    return updateProductsList().then(function () {
	      return model.getSelectedProduct();
	    }).then(updateProductSelected).then(initPeriodData).then(updateSpending).then(bindEvents).then(function () {
	      $ctrl.isLoading = false;
	    });
	  };
	
	  /**
	   * @description
	   * Handler to be called on period start date change
	   *
	   * @name onPeriodStartDateChanged
	   * @type {function}
	   * @param {string} startDate Date as string in format yyyy-mm-dd
	   * @returns {void}
	   */
	  var onPeriodStartDateChanged = function onPeriodStartDateChanged(startDate) {
	    $ctrl.periodStartDate = startDate;
	    bus.publish(PERIOD_START_CHANGED, $ctrl.periodStartDate);
	  };
	
	  /**
	   * @description
	   * Handler to be called on period end date change
	   *
	   * @name onPeriodEndDateChanged
	   * @type {function}
	   * @param {string} endDate Date as string in format yyyy-mm-dd
	   * @returns {void}
	   */
	  var onPeriodEndDateChanged = function onPeriodEndDateChanged(endDate) {
	    $ctrl.periodEndDate = endDate;
	    bus.publish(PERIOD_END_CHANGED, $ctrl.periodEndDate);
	  };
	
	  /**
	   * @description
	   * Handler to be used on product selection, is using
	   * selected product value from {@link Hooks.processSelectedProduct} hook
	   *
	   * @name onProductSelected
	   * @type {function}
	   * @returns {void}
	   */
	  var onProductSelected = function onProductSelected() {
	    bus.publish(PRODUCT_SELECTED, $ctrl.selectedProduct);
	  };
	
	  Object.assign($ctrl, {
	    $onInit: $onInit,
	
	    /**
	     * @description
	     * The selected product for spending evaluation.
	     *
	     * @name selectedProduct
	     * @type {Product}
	     */
	    selectedProduct: null,
	
	    /**
	     * @description
	     * List of products to be used by account selector for spending evaluation.
	     * Is recieved from {@link Hooks.processProductsList}
	     *
	     * @name products
	     * @type {ProductKinds}
	     */
	    products: [],
	
	    /**
	     * @description
	     * The value returned from {@link Hooks.processItems} hook.
	     * null if the items aren't loaded.
	     *
	     * @name items
	     * @type {Spending}
	     */
	    items: null,
	
	    /**
	     * @description
	     * The value returned from {@link Hooks.processSpendingSeries} hook.
	     * Formatted for use within chart UI component.
	     * null if the data isn't loaded
	     *
	     * @name series
	     * @type {object}
	     */
	    series: null,
	
	    /**
	     * @description
	     * Date of the spending evaluation period start
	     *
	     * @name periodStartDate
	     * @type {string}
	     */
	    periodStartDate: null,
	
	    /**
	     * @description
	     * Date of the spending evaluation period end
	     *
	     * @name periodEndDate
	     * @type {string}
	     */
	    periodEndDate: null,
	
	    /**
	     * @description
	     * selected spending item
	     *
	     * @name selectedSpendingItem
	     * @type {any}
	     */
	    selectedSpendingItem: null,
	
	    /**
	     * @description
	     * Setter for the selected spending item
	     *
	     * @name setSelectedSpendingItem
	     * @param {any} item -  selected item
	     * @type {function}
	     * @returns {void}
	     */
	    setSelectedSpendingItem: setSelectedSpendingItem,
	
	    /**
	     * @description
	     * Loading status
	     *
	     * @name isLoading
	     * @type {boolean}
	     */
	    isLoading: false,
	    onPeriodStartDateChanged: onPeriodStartDateChanged,
	    onPeriodEndDateChanged: onPeriodEndDateChanged,
	    onProductSelected: onProductSelected,
	
	    /**
	     * @description
	     * The error encountered when attempting to fetch from the model
	     *
	     * @name SpendingController#error
	     * @type {ModelError}
	     */
	    error: null
	  });
	}

/***/ }),
/* 32 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  /**
	   * Triggered when product is selected.
	   * @event bb.event.product.selected
	   * @type {Product}
	   */
	  PRODUCT_SELECTED: 'bb.event.product.selected',
	
	  /**
	  * Triggered when spending widget fails to load.
	  * @event widget-bb-spending-ng.load.failed
	  * @type {any}
	  */
	  SPENDING_LOAD_FAILED: 'widget-bb-spending-ng.load.failed',
	
	  /**
	  * Triggered when period start date is changed.
	  * @event bb.event.spending.period.start.date.changed
	  * @type {any}
	  */
	  PERIOD_START_CHANGED: 'bb.event.spending.period.start.date.changed',
	  /**
	   * Triggered when period end date is changed.
	   * @event bb.event.spending.period.end.date.changed
	   * @type {any}
	   */
	  PERIOD_END_CHANGED: 'bb.event.spending.period.end.date.changed'
	};

/***/ }),
/* 33 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * Current date
	 * Cache the date object to negate possible miss synchronization
	 * @type {object}
	 */
	
	var CURRENT_DATE_OBJECT = new Date();
	
	/**
	 * Current year
	 * @type {number}
	 */
	var CURRENT_YEAR = CURRENT_DATE_OBJECT.getFullYear();
	
	/**
	 * Current month
	 * @type {number}
	 */
	var CURRENT_MONTH = CURRENT_DATE_OBJECT.getMonth();
	
	/**
	 * @name MS_IN_MIN
	 * @description
	 * Amount of milliseconds in a minute
	 * @type {number}
	 */
	var MS_IN_MIN = exports.MS_IN_MIN = 60000;
	
	/**
	 * @name DEFAULT_PERIOD_START
	 * @description
	 * First day in the current month
	 * @type {number}
	 */
	var DEFAULT_PERIOD_START = exports.DEFAULT_PERIOD_START = new Date(CURRENT_YEAR, CURRENT_MONTH, 1);
	
	/**
	 * @name DEFAULT_PERIOD_END
	 * @description
	 * Last day in the current month
	 * @type {number}
	 */
	var DEFAULT_PERIOD_END = exports.DEFAULT_PERIOD_END = new Date(CURRENT_YEAR, CURRENT_MONTH + 1, 0);
	
	/**
	 * @name Intents
	 * @description
	 * An object with all the intents names
	 * @type {Object}
	 */
	var Intent = exports.Intent = {
	  SELECT_SPENDING_ITEM: 'view.account.category.transactions'
	};

/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _constants = __webpack_require__(33);
	
	/**
	 * @name default-hooks#processSpendingItems
	 * @type {function}
	 *
	 * @description
	 * Default hook for spending collection post processing
	 *
	 * @param {Spending} items to process
	 * @returns {Spending} total spendings by category
	 */
	var processSpendingItems = function processSpendingItems(items) {
	  return items || [];
	};
	
	/**
	 * @name default-hooks#processSpendingSeries
	 * @type {function}
	 *
	 * @description
	 * Default hook for spending chart series object post processing
	 *
	 * @param {BBSeries} series chart series data
	 * @param {Spending} data original spending object
	 * @returns {object} processed series
	 */
	/* eslint no-unused-vars: ["error", { "args": "none" }] */
	/**
	 * @name default-hooks
	 * @type {object}
	 *
	 * @description
	 * Default hooks for widget-bb-spending-ng
	 */
	
	var processSpendingSeries = function processSpendingSeries(series, data) {
	  return series || {};
	};
	
	/**
	 * @name default-hooks#onSpendingUpdate
	 * @type {function}
	 *
	 * @description
	 * Process parameters before they are sent to the model's load method
	 *
	 * @param {object} params to process
	 * @returns {object} processed params
	 */
	var onSpendingUpdate = function onSpendingUpdate(params) {
	  return params;
	};
	
	/**
	 * @name default-hooks#processSelectedProduct
	 * @type {function}
	 *
	 * @description
	 * Default hook to process product then it's selected
	 * @param {Product} product which is selected
	 * @returns {Product} product after processing
	 */
	var processSelectedProduct = function processSelectedProduct(selectedProduct) {
	  return selectedProduct;
	};
	
	/**
	 * @name default-hooks#processProductsList
	 * @type {function}
	 *
	 * @description
	 * Process passed products list before passing it to the view.
	 *
	 * @param {ProductKinds} products to process
	 * @returns {ProductKinds} processed products
	 */
	var processProductsList = function processProductsList(products) {
	  return products;
	};
	
	/**
	 * @name default-hooks#defaultPeriodStart
	 * @type {function}
	 *
	 * @description
	 * Sets period start property on init
	 *
	 * @returns {string} Start period string in format yyyy-mm-dd
	 */
	var defaultPeriodStart = function defaultPeriodStart() {
	  return new Date(_constants.DEFAULT_PERIOD_START.getTime() - _constants.DEFAULT_PERIOD_START.getTimezoneOffset() * _constants.MS_IN_MIN).toISOString().slice(0, 10);
	};
	
	/**
	 * @name default-hooks#defaultPeriodEnd
	 * @type {function}
	 *
	 * @description
	 * Sets period end property on init
	 *
	 * @returns {string} End period string in format yyyy-mm-dd
	 */
	var defaultPeriodEnd = function defaultPeriodEnd() {
	  return new Date(_constants.DEFAULT_PERIOD_END.getTime() - _constants.DEFAULT_PERIOD_END.getTimezoneOffset() * _constants.MS_IN_MIN).toISOString().slice(0, 10);
	};
	
	/**
	 * @name default-hooks#onSelectedItemChanged
	 * @type {function}
	 *
	 * @description
	 * Sets a selected spending item
	 *
	 * @param {any} item - selected spending item
	 * @returns {any} - selected spending item
	 */
	var onSelectedItemChanged = function onSelectedItemChanged(item) {
	  return item;
	};
	/**
	 * @name default-hooks#processLoadError
	 * @type {function}
	 *
	 * @description
	 * Sets the error for missing parameters in the turnovers request
	 *
	 * @param {error} The error passed
	 * @returns {error} The actual error
	 */
	var processLoadError = function processLoadError(error) {
	  return error;
	};
	
	exports.default = {
	  processSpendingItems: processSpendingItems,
	  processSpendingSeries: processSpendingSeries,
	  onSpendingUpdate: onSpendingUpdate,
	  processSelectedProduct: processSelectedProduct,
	  processProductsList: processProductsList,
	  defaultPeriodStart: defaultPeriodStart,
	  defaultPeriodEnd: defaultPeriodEnd,
	  processLoadError: processLoadError,
	  onSelectedItemChanged: onSelectedItemChanged
	};

/***/ })
/******/ ])
});
;
//# sourceMappingURL=widget-bb-spending-ng.js.map