# widget-bbm-payment-ng


Version: **1.0.96**

Mobile Payment widget.

## Imports

* lib-bb-event-bus-ng
* lib-bb-intent-ng
* lib-bb-storage-ng
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-payment-ng
* vendor-bb-angular

---

## Table of Contents
- **FormController**<br/>    <a href="#FormController#preferences">#preferences</a><br/>    <a href="#FormController#canSaveContact">#canSaveContact()</a><br/>    <a href="#FormController#resetPayment">#resetPayment()</a><br/>    <a href="#FormController#selectBeneficiary">#selectBeneficiary()</a><br/>    <a href="#FormController#selectDebitAccount">#selectDebitAccount()</a><br/>    <a href="#FormController#selectSchedule">#selectSchedule()</a><br/>    <a href="#FormController#setSaveContact">#setSaveContact(saveContact)</a><br/>    <a href="#FormController#setUrgentPayment">#setUrgentPayment(urgent)</a><br/>    <a href="#FormController#submitPayment">#submitPayment()</a><br/>    <a href="#FormController#$onInit">#$onInit()</a><br/>
- **widget-bbm-payment-ng**<br/>    <a href="#widget-bbm-payment-ngisSameContact">isSameContact(contactA, contactB)</a><br/>
- **ReviewController**<br/>    <a href="#ReviewController#preferences">#preferences</a><br/>    <a href="#ReviewController#$onInit">#$onInit()</a><br/>    <a href="#ReviewController#submitPayment">#submitPayment()</a><br/>
- **ScheduleController**<br/>    <a href="#ScheduleController#preferences">#preferences</a><br/>    <a href="#ScheduleController#$onInit">#$onInit()</a><br/>    <a href="#ScheduleController#submitSchedule">#submitSchedule()</a><br/>
- **SelectAccountController**<br/>    <a href="#SelectAccountController#preferences">#preferences</a><br/>    <a href="#SelectAccountController#$onInit">#$onInit()</a><br/>    <a href="#SelectAccountController#selectAccount">#selectAccount(account)</a><br/>    <a href="#SelectAccountController#AccountType">#AccountType</a><br/>
- **widget-bbm-payment-ng**<br/>    <a href="#widget-bbm-payment-ngaccountType">accountType</a><br/>    <a href="#widget-bbm-payment-ngisUrgentPaymentAllowed">isUrgentPaymentAllowed()</a><br/>
- **Hooks**<br/>    <a href="#Hooks#processDebitAccounts">#processDebitAccounts(debitAccounts)</a><br/>    <a href="#Hooks#processBeneficiaries">#processBeneficiaries(creditAccounts, contacts)</a><br/>    <a href="#Hooks#processInitialPaymentState">#processInitialPaymentState(payment)</a><br/>    <a href="#Hooks#processPaymentPayload">#processPaymentPayload(paymentPayload)</a><br/>
- **Type Definitions**<br/>    <a href="#ContactIdentification">ContactIdentification</a><br/>    <a href="#CreditAccountIdentification">CreditAccountIdentification</a><br/>    <a href="#DebitAccountIdentification">DebitAccountIdentification</a><br/>    <a href="#PaymentPayload">PaymentPayload</a><br/>    <a href="#SchedulePayload">SchedulePayload</a><br/>    <a href="#ContactAccount">ContactAccount</a><br/>    <a href="#ContactCreatePayload">ContactCreatePayload</a><br/>    <a href="#AccountView">AccountView</a><br/>    <a href="#PaymentView">PaymentView</a><br/>    <a href="#BeneficiariesState">BeneficiariesState</a><br/>    <a href="#DebitAccountsState">DebitAccountsState</a><br/>    <a href="#PaymentState">PaymentState</a><br/>    <a href="#Schedule">Schedule</a><br/>    <a href="#Payment">Payment</a><br/>    <a href="#Amount">Amount</a><br/>    <a href="#Currency">Currency</a><br/>

---

## FormController

Payment widget form controller.
Loads debit accounts and beneficiaries on start.
Provides API to make a payment.

| Injector Key |
| :-- |
| *FormController* |

### <a name="FormController#preferences"></a>*#preferences*

Payment preferences set in the widget preferences.

**Type:** *Object*


### <a name="FormController#canSaveContact"></a>*#canSaveContact()*

Checks whether the beneficiary can be saved to the address book as a new contact.

##### Returns

Boolean - **

### <a name="FormController#resetPayment"></a>*#resetPayment()*

Resets the payment form.

### <a name="FormController#selectBeneficiary"></a>*#selectBeneficiary()*

Initiates the process of selecting of the beneficiary by calling
the "view.payment.account.select" intent with type = "credit".

Before calling the intent it ensures, that beneficiaries are loaded.

##### Fires Events:

> bb.event.payment.selectAccount.load.start

> bb.event.payment.selectAccount.load.done

> bb.event.payment.selectAccount.failed


### <a name="FormController#selectDebitAccount"></a>*#selectDebitAccount()*

Initiates the process of selecting of the debit account by calling
the "view.payment.account.select" intent with type = "debit".

Before calling the intent it ensures, that debit accounts are loaded.

##### Fires Events:

> bb.event.payment.selectAccount.load.start

> bb.event.payment.selectAccount.load.done

> bb.event.payment.selectAccount.failed


### <a name="FormController#selectSchedule"></a>*#selectSchedule()*

Initiates the process of a scheduling a payment by calling
the "view.payment.schedule.select" intent, that navigates the user
to the Payment Schedule view.

### <a name="FormController#setSaveContact"></a>*#setSaveContact(saveContact)*

Updates state of the "Save contact" flag.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| saveContact | Boolean |  |

### <a name="FormController#setUrgentPayment"></a>*#setUrgentPayment(urgent)*

Updates state of the "urgent" flag.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| urgent | Boolean |  |

### <a name="FormController#submitPayment"></a>*#submitPayment()*

Depending on the preference either navigates the user to the review page
or makes the payment.

##### Returns

Promise of <a href="#void">void</a> - *Promise that resolves once the operation is complete.*

### <a name="FormController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller.

Preloads debit accounts and beneficiaries. Prepares the view model.

##### Fires Events:

> cxp.item.loaded


---

### <a name="widget-bbm-payment-ngisSameContact"></a>*isSameContact(contactA, contactB)*

Checks if given contacts A and B are the same contact.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| contactA | <a href="#AccountView">AccountView</a> |  |
| contactB | <a href="#AccountView">AccountView</a> |  |

##### Returns

Boolean - **

---

## ReviewController

Payment widget review controller.
Provides API to make a payment.

| Injector Key |
| :-- |
| *ReviewController* |

### <a name="ReviewController#preferences"></a>*#preferences*

Payment preferences set in the widget preferences.

**Type:** *Object*


### <a name="ReviewController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller.

Prepares the view model.

##### Fires Events:

> cxp.item.loaded


### <a name="ReviewController#submitPayment"></a>*#submitPayment()*

Submits the payment.

##### Returns

Promise of <a href="#void">void</a> - **

---

## ScheduleController

Payment widget Schedule controller.
Provides API to set a schedule of a payment.

| Injector Key |
| :-- |
| *ScheduleController* |

### <a name="ScheduleController#preferences"></a>*#preferences*

Payment preferences set in the widget preferences.

**Type:** *Object*


### <a name="ScheduleController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller.

Prepares the view model.

##### Fires Events:

> cxp.item.loaded


### <a name="ScheduleController#submitSchedule"></a>*#submitSchedule()*

Fulfils the select schedule intent with the given data.

---

## SelectAccountController

Payment widget Select account controller.
Provides API to select an account.

| Injector Key |
| :-- |
| *SelectAccountController* |

### <a name="SelectAccountController#preferences"></a>*#preferences*

Payment preferences set in the widget preferences.

**Type:** *Object*


### <a name="SelectAccountController#$onInit"></a>*#$onInit()*

AngularJS Lifecycle hook used to initialize the controller.

Prepares the view model.

##### Fires Events:

> cxp.item.loaded


### <a name="SelectAccountController#selectAccount"></a>*#selectAccount(account)*

Fulfils the select account intent with the given account.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| account | <a href="#AccountView">AccountView</a> |  |
### <a name="SelectAccountController#AccountType"></a>*#AccountType*

Enumeration of available types of accounts.

**Type:** *Object*


---
### <a name="widget-bbm-payment-ngaccountType"></a>*accountType*

The type of the account that needs to be selected.
Possible values are "debit" or "credit".

**Type:** *String (optional)*


---

### <a name="widget-bbm-payment-ngisUrgentPaymentAllowed"></a>*isUrgentPaymentAllowed()*

Checks if urgent payment is available for the current transaction.

Returns true, if beneficiary allows urgent payments, payment is scheduled only for once,
payment is not scheduled for future, otherwise false.

##### Returns

Boolean - **

---

## Hooks

Hooks for widget-bbm-payment-ng.

### <a name="Hooks#processDebitAccounts"></a>*#processDebitAccounts(debitAccounts)*

Processes the list of debit accounts.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccounts | Array of <a href="#AccountView">AccountView</a> | Original list of debit accounts from the model. |

##### Returns

Array of <a href="#AccountView">AccountView</a> - *Processed list of debit accounts.*

### <a name="Hooks#processBeneficiaries"></a>*#processBeneficiaries(creditAccounts, contacts)*

Processes the list of beneficiaries. By default it merges credit accounts and
contacts into a single list of beneficiaries.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| creditAccounts | Array of <a href="#AccountView">AccountView</a> | Original list of credit accounts from the model. |
| contacts | Array of <a href="#AccountView">AccountView</a> | Original list of contacts from the model. |

##### Returns

Array of <a href="#AccountView">AccountView</a> - *Processed list of beneficiaries.*

### <a name="Hooks#processInitialPaymentState"></a>*#processInitialPaymentState(payment)*

Processes the initial payment object.

The widget uses this hook on start when the initial payment object is created.
Also the widget uses this when it resets the payment and starts another one.

Use it to add custom properties to the payment object.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| payment | <a href="#Payment">Payment</a> | Payment state, that is supposed to be processed |

##### Returns

<a href="#Payment">Payment</a> - **

### <a name="Hooks#processPaymentPayload"></a>*#processPaymentPayload(paymentPayload)*

Processes the payload of a the payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentPayload | <a href="#PaymentPayload">PaymentPayload</a> | Payment payload, that is supposed to be processed |

##### Returns

<a href="#Payment">Payment</a> - **

## Type Definitions


### <a name="ContactIdentification"></a>*ContactIdentification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| counterpartyName | String | Counterparty name. Only required when 'scheme' is set to IBAN/BBAN. |
| identification | String | Identification of the product. Different schemes are supported: IBAN, BBAN, ID |
| scheme | String | The name of the scheme. For contacts is always "IBAN". |

### <a name="CreditAccountIdentification"></a>*CreditAccountIdentification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| identification | String | Credit account ID |
| scheme | String | The name of the scheme. For credit accounts is always "ID". |

### <a name="DebitAccountIdentification"></a>*DebitAccountIdentification*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| identification | String | Debit account ID |
| scheme | String | The name of the scheme. For debit accounts is always "ID". |

### <a name="PaymentPayload"></a>*PaymentPayload*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| debitAccountIdentification | <a href="#DebitAccountIdentification">DebitAccountIdentification</a> | Identification of the payment debit account |
| creditAccountIdentification | <a href="#CreditAccountIdentification">CreditAccountIdentification</a> | Identification of the payment credit account |
| amount | Number | The amount of the payment |
| currency | String | The alpha-3 code (complying with ISO 4217) of the currency that qualifies the amount |
| description | String | The description for the payment. |
| paymentMode | String | Denotes whether payment will be single or will be recurring. Possible values are "SINGLE" and "RECURRING" |

### <a name="SchedulePayload"></a>*SchedulePayload*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| transferFrequency | String | Denotes how frequently the transfer should be made |
| on | Number | Denotes day on which transfer should be executed. For weekly it will be 1..7 indicating weekday. For monthly it will be 1..31 indicating day of month. For yearly it will be 1..12 indicating month of the year |
| startDate | String | When to start executing the schedule. First transfer will be executed on first calculated date by schedule after this date |
| repeat | Number | Number of transfer to be executed. Only one of endDate and repeat is possible. If neither repeat nor endDate is provided transfer will be executed until canceled |
| every | Number | Indicates skip interval of transfer. 1 would mean execute every time, 2 - every other time |
| endDate | String (optional) | When to stop transfers. Transfers will not be executed after this date. Only one of endDate and repeat is possible. If neither repeat nor endDate is provided transfer will be executed until canceled |

### <a name="ContactAccount"></a>*ContactAccount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| IBAN | String | Contact's IBAN |

### <a name="ContactCreatePayload"></a>*ContactCreatePayload*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String | Contact's name |
| accounts | Array of <a href="#ContactAccount">ContactAccount</a> | List of contact's accounts |

### <a name="AccountView"></a>*AccountView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The internal account identifier |
| name | String | The account's name, suitable for display to users |
| identifier | String (optional) | The identifier of the account from the user's perspective |
| amount | String (optional) | The most important associated value to be displayed |
| currency | String (optional) | Account currency |
| external | Boolean (optional) | Whether the account is external |

### <a name="PaymentView"></a>*PaymentView*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| beneficiaries | <a href="#BeneficiariesState">BeneficiariesState</a> | State of the beneficiaries |
| debitAccounts | <a href="#DebitAccountsState">DebitAccountsState</a> | State of the debit accounts |
| payment | <a href="#PaymentState">PaymentState</a> | State of the payment |
| saveContact | Boolean | Whether the beneficiary should be saved to address book |

### <a name="BeneficiariesState"></a>*BeneficiariesState*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| error | Error | Error if beneficiaries request failed |
| loading | Boolean | Indicates whether beneficiaries are being loading |
| data | Array of <a href="#AccountView">AccountView</a> | List of beneficiaries |

### <a name="DebitAccountsState"></a>*DebitAccountsState*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| error | Error | Error if accounts request failed |
| loading | Boolean | Indicates whether debit accounts are being loading |
| data | Array of <a href="#AccountView">AccountView</a> | List of accounts |

### <a name="PaymentState"></a>*PaymentState*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| error | Error | Error if payment request failed |
| loading | Boolean | Indicates whether a payment request is being sending |
| data | <a href="#Payment">Payment</a> | Payment data |

### <a name="Schedule"></a>*Schedule*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| transferFrequency | String | How frequently the transfer should be made |
| startDate | Date | When to start executing the schedule |
| endDate | Date (optional) | When to stop transfers |

### <a name="Payment"></a>*Payment*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| debitAccount | <a href="#AccountView">AccountView</a> | Selected debit account |
| beneficiary | <a href="#AccountView">AccountView</a> | Selected beneficiary |
| amount | <a href="#Amount">Amount</a> | Amount and currency of the payment |
| description | String | Description of the payment |
| schedule | <a href="#Schedule">Schedule</a> | Schedule for recurring transfer |

### <a name="Amount"></a>*Amount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| currency | String | Currency code |
| value | Number | Amount value |

### <a name="Currency"></a>*Currency*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| name | String | Currency name, suitable for display to users |

---

## Templates

* *template.ng.html*

---
