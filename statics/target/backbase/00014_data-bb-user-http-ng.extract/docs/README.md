# data-bb-user-http-ng

A data module for accessing the User REST API.

## Imports

* vendor-bb-angular

---

## Exports

### <a name="default"></a>*default*

Angular dependency injection module key

**Type:** *String*

### <a name="userDataKey"></a>*userDataKey*

Angular dependency injection key for the UserData service

**Type:** *String*


---

## Example

```javascript
import userDataModuleKey, {
  userDataKey,
} from 'data-bb-user-http-ng';
```

---

## UserData

Public api for data-bb-user-http-ng service

### <a name="UserData#getUsers"></a>*#getUsers(params)*

# Users list

Initial call to retrieve the list of enrolled Users that belong to a given Legal Entity. - Returns data as UserData.GetUsers on success or UserData.BadRequest|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |
| params.entityId | String | Legal Entity ID. Eg: 1002. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .getUsers(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="UserData#postUsersRecord"></a>*#postUsersRecord(data)*

# Create user

Creates a new user that belongs to the given legal entity - Returns data as UserData.IdItem on success or UserData.BadRequest|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#UserData.UserCreateItem">UserData.UserCreateItem</a> | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .postUsersRecord(data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="UserData#getUsersRecord"></a>*#getUsersRecord(userId, params)*

# User

Get a single User by ID. - Returns data as UserData.UserItem on success or UserData.BadRequest|UserData.ExceptionSchema|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .getUsersRecord(userId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="UserData#getUsersExternalIdRecord"></a>*#getUsersExternalIdRecord(externalId, params)*

# Get User By External Id

Initial call to retrieve a single user from the platform
using the external identifier. - Returns data as UserData.UserItem on success or UserData.BadRequest|UserData.ExceptionSchema|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| externalId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .getUsersExternalIdRecord(externalId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="UserData#postUsersEntitlementsAdminRecord"></a>*#postUsersEntitlementsAdminRecord(data)*

# Create Admin User

Create Admin required to manage entitlements for the Bank and every Legal Entity. - Returns data as UserData.BadRequest|UserData.ExceptionSchema|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | <a href="#UserData.EntitlementsAdminCreateItem">UserData.EntitlementsAdminCreateItem</a> | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .postUsersEntitlementsAdminRecord(data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="UserData#getUsersLegalentities"></a>*#getUsersLegalentities(userId, params)*

# Legal Entities by User list

Retrieve all Legal Entities that the User Belongs to - Returns data as UserData.GetLegalEntities on success or UserData.BadRequest|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| userId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .getUsersLegalentities(userId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="UserData#getUsersExternalIdLegalentities"></a>*#getUsersExternalIdLegalentities(externalId, params)*

# Legal Entities by User list

Retrieve the Legal Entity that the User Belongs to - Returns data as UserData.GetLegalEntity on success or UserData.BadRequest|UserData.InternalServerError on error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| externalId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - *A promise resolving to response object*

## Example

```javascript
userData
 .getUsersExternalIdLegalentities(externalId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```
### <a name="UserData#schemas"></a>*#schemas*

Schema data. Keys of the object are names of the POST and PUT methods

Note: The schema is not strictly a JSON schema. It is a whitelisted set of
keys for each object property. The keys that are exposed are meant for validation
purposes.

The full list of *possible* keys for each property is:
type, minimum, maximum, minLength, maxLength, pattern, enum, format, default,
properties, items, minItems, maxItems, uniqueItems and required.

See http://json-schema.org/latest/json-schema-validation.html for more details
on the meaning of these keys.

The "required" array from JSON schema is tranformed into a "required" boolean
on each property. This is for ease of use.

**Type:** *Object*

### <a name="UserData#schemas.postUsersRecord"></a>*#schemas.postUsersRecord*

An object describing the JSON schema for the postUsersRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "externalId": {
      "type": "string",
      "required": true
    },
    "legalEntityExternalId": {
      "type": "string",
      "required": true
    },
    "firstName": {
      "type": "string",
      "required": true
    }
  }
}
```
### <a name="UserData#schemas.postUsersEntitlementsAdminRecord"></a>*#schemas.postUsersEntitlementsAdminRecord*

An object describing the JSON schema for the postUsersEntitlementsAdminRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "externalId": {
      "type": "string",
      "required": true
    },
    "legalEntityExternalId": {
      "type": "string",
      "required": true
    }
  }
}
```

---

## UserDataProvider

Data service that can be configured with custom base URI.

| Injector Key |
| :-- |
| *data-bb-user-http-ng:userDataProvider* |


### <a name="UserDataProvider#setBaseUri"></a>*#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | String | Base URI which will be the prefix for all HTTP requests |

### <a name="UserDataProvider#$get"></a>*#$get()*


##### Returns

Object - *An instance of the service*

## Example

```javascript
// Configuring in an angular app:
angular.module(...)
  .config(['data-bb-user-http-ng:userDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });

// Configuring With config-bb-providers-ng:
export default [
  ['data-bb-user-http-ng:userDataProvider', (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
  }]
];
```

## Type Definitions


### <a name="UserData.BadRequest"></a>*UserData.BadRequest*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |

### <a name="UserData.InternalServerError"></a>*UserData.InternalServerError*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |

### <a name="UserData.ExceptionSchema"></a>*UserData.ExceptionSchema*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| message | String |  |
| errorCode | String (optional) |  |

### <a name="UserData.UserItem"></a>*UserData.UserItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| externalId | String |  |
| legalEntityId | String |  |
| id | String (optional) |  |
| imageAvatar | String (optional) |  |
| firstName | String (optional) |  |
| lastName | String (optional) |  |
| dateOfBirth | String (optional) |  |
| street | String (optional) |  |
| houseNumber | String (optional) |  |
| postalCode | String (optional) |  |
| area | String (optional) |  |
| city | String (optional) |  |
| citizenship | String (optional) |  |
| phone | Array (optional) of String |  |
| email | String (optional) |  |

### <a name="UserData.GetUsers"></a>*UserData.GetUsers*


**Type:** *Array of <a href="#UserData.GetUsersItem">UserData.GetUsersItem</a>*


### <a name="UserData.GetUsersItem"></a>*UserData.GetUsersItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| externalId | String |  |
| legalEntityId | String |  |
| id | String (optional) |  |
| imageAvatar | String (optional) |  |
| firstName | String (optional) |  |
| lastName | String (optional) |  |
| dateOfBirth | String (optional) |  |
| street | String (optional) |  |
| houseNumber | String (optional) |  |
| postalCode | String (optional) |  |
| area | String (optional) |  |
| city | String (optional) |  |
| citizenship | String (optional) |  |
| phone | Array (optional) of String |  |
| email | String (optional) |  |

### <a name="UserData.GetLegalEntity"></a>*UserData.GetLegalEntity*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |
| externalId | String |  |
| name | String |  |
| parentId | String (optional) |  |
| isParent | Boolean (optional) |  |

### <a name="UserData.GetLegalEntities"></a>*UserData.GetLegalEntities*


**Type:** *Array of <a href="#UserData.GetLegalEntitiesItem">UserData.GetLegalEntitiesItem</a>*


### <a name="UserData.GetLegalEntitiesItem"></a>*UserData.GetLegalEntitiesItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |
| externalId | String |  |
| name | String |  |
| parentId | String (optional) |  |
| isParent | Boolean (optional) |  |

### <a name="UserData.IdItem"></a>*UserData.IdItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |

### <a name="UserData.UserCreateItem"></a>*UserData.UserCreateItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| externalId | String |  |
| legalEntityExternalId | String |  |
| firstName | String |  |

### <a name="UserData.EntitlementsAdminCreateItem"></a>*UserData.EntitlementsAdminCreateItem*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| externalId | String |  |
| legalEntityExternalId | String |  |

### <a name="Response"></a>*Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Object | See method descriptions for possible return types |
| headers | Function | Getter headers function |
| status | Number | HTTP status code of the response. |
| statusText | String | HTTP status text of the response. |

---
