(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"));
	else if(typeof define === 'function' && define.amd)
		define("data-bb-user-http-ng", ["vendor-bb-angular"], factory);
	else if(typeof exports === 'object')
		exports["data-bb-user-http-ng"] = factory(require("vendor-bb-angular"));
	else
		root["data-bb-user-http-ng"] = factory(root["vendor-bb-angular"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.userDataKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbUserHttp = __webpack_require__(3);
	
	var _dataBbUserHttp2 = _interopRequireDefault(_dataBbUserHttp);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module data-bb-user-http-ng
	 *
	 * @description A data module for accessing the User REST API.
	 *
	 * @returns {String} `data-bb-user-http-ng`
	 * @example
	 * import userDataModuleKey, {
	 *   userDataKey,
	 * } from 'data-bb-user-http-ng';
	 */
	
	var userDataModuleKey = 'data-bb-user-http-ng';
	/**
	 * @name userDataKey
	 * @type {string}
	 * @description Angular dependency injection key for the UserData service
	 */
	var userDataKey = exports.userDataKey = 'data-bb-user-http-ng:userData';
	/**
	 * @name default
	 * @type {string}
	 * @description Angular dependency injection module key
	 */
	exports.default = _vendorBbAngular2.default.module(userDataModuleKey, [])
	
	/**
	 * @constructor UserData
	 * @type {object}
	 *
	 * @description Public api for data-bb-user-http-ng service
	 *
	 */
	.provider(userDataKey, [function () {
	  var config = {
	    baseUri: '/'
	  };
	
	  /**
	   * @name UserDataProvider
	   * @type {object}
	   * @ngkey data-bb-user-http-ng:userDataProvider
	   * @description
	   * Data service that can be configured with custom base URI.
	   *
	   * @example
	   * // Configuring in an angular app:
	   * angular.module(...)
	   *   .config(['data-bb-user-http-ng:userDataProvider',
	   *     (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *       });
	   *
	   * // Configuring With config-bb-providers-ng:
	   * export default [
	   *   ['data-bb-user-http-ng:userDataProvider', (dataProvider) => {
	   *       dataProvider.setBaseUri('http://my-service.com/');
	   *   }]
	   * ];
	   */
	  return {
	    /**
	     * @name UserDataProvider#setBaseUri
	     * @type {function}
	     * @param {string} baseUri Base URI which will be the prefix for all HTTP requests
	     */
	    setBaseUri: function setBaseUri(baseUri) {
	      config.baseUri = baseUri;
	    },
	
	    /**
	     * @name UserDataProvider#$get
	     * @type {function}
	     * @return {object} An instance of the service
	     */
	    $get: ['$http',
	    /* into */
	    (0, _dataBbUserHttp2.default)(config)]
	  };
	}]).name;

/***/ },
/* 2 */
/***/ function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	exports.default = function (conf) {
	  return function (httpClient) {
	    // Base param constants
	    var baseUri = conf.baseUri || '';
	
	    var version = 'v2';
	
	    /**
	     * The root defined types from the RAML.
	     * @private
	     */
	    var definedTypes = {};
	
	    definedTypes['UserData.BadRequest'] = { "properties": { "message": { "type": "string", "required": true } } };
	
	    definedTypes['UserData.InternalServerError'] = { "properties": { "message": { "type": "string", "required": true } } };
	
	    definedTypes['UserData.ExceptionSchema'] = { "properties": { "message": { "type": "string", "required": true }, "errorCode": { "type": "string", "required": false } } };
	
	    definedTypes['UserData.UserItem'] = { "properties": { "externalId": { "type": "string", "required": true }, "legalEntityId": { "type": "string", "required": true }, "id": { "type": "string", "required": false }, "imageAvatar": { "type": "string", "required": false }, "firstName": { "type": "string", "required": false }, "lastName": { "type": "string", "required": false }, "dateOfBirth": { "type": "string", "pattern": "^[0-9]{2}-[0-9]{2}-[0-9]{4}$", "required": false }, "street": { "type": "string", "required": false }, "houseNumber": { "type": "string", "required": false }, "postalCode": { "type": "string", "required": false }, "area": { "type": "string", "required": false }, "city": { "type": "string", "required": false }, "citizenship": { "type": "string", "required": false }, "phone": { "type": "array", "items": { "properties": {} }, "required": false }, "email": { "type": "string", "required": false } } };
	
	    definedTypes['UserData.GetUsers'] = { "properties": {} };
	
	    definedTypes['UserData.GetLegalEntity'] = { "properties": { "id": { "type": "string", "required": true }, "externalId": { "type": "string", "required": true }, "name": { "type": "string", "required": true }, "parentId": { "type": "string", "required": false }, "isParent": { "type": "boolean", "default": false, "required": false } } };
	
	    definedTypes['UserData.GetLegalEntities'] = { "properties": {} };
	
	    definedTypes['UserData.IdItem'] = { "properties": { "id": { "type": "string", "required": true } } };
	
	    definedTypes['UserData.UserCreateItem'] = { "properties": { "externalId": { "type": "string", "required": true }, "legalEntityExternalId": { "type": "string", "required": true }, "firstName": { "type": "string", "required": true } } };
	
	    definedTypes['UserData.EntitlementsAdminCreateItem'] = { "properties": { "externalId": { "type": "string", "required": true }, "legalEntityExternalId": { "type": "string", "required": true } } };
	
	    /**
	     * @typedef UserData.BadRequest
	     * @type {Object}
	     * @property {String} message
	     */
	
	    /**
	     * @typedef UserData.InternalServerError
	     * @type {Object}
	     * @property {String} message
	     */
	
	    /**
	     * @typedef UserData.ExceptionSchema
	     * @type {Object}
	     * @property {String} message
	     * @property {?String} errorCode
	     */
	
	    /**
	     * @typedef UserData.UserItem
	     * @type {Object}
	     * @property {String} externalId
	     * @property {String} legalEntityId
	     * @property {?String} id
	     * @property {?String} imageAvatar
	     * @property {?String} firstName
	     * @property {?String} lastName
	     * @property {?String} dateOfBirth
	     * @property {?String} street
	     * @property {?String} houseNumber
	     * @property {?String} postalCode
	     * @property {?String} area
	     * @property {?String} city
	     * @property {?String} citizenship
	     * @property {?Array.<String>} phone
	     * @property {?String} email
	     */
	
	    /**
	     * @typedef UserData.GetUsers
	     * @type {Array.<UserData.GetUsersItem>}
	     */
	
	    /**
	     * @typedef UserData.GetUsersItem
	     * @type {Object}
	     * @property {String} externalId
	     * @property {String} legalEntityId
	     * @property {?String} id
	     * @property {?String} imageAvatar
	     * @property {?String} firstName
	     * @property {?String} lastName
	     * @property {?String} dateOfBirth
	     * @property {?String} street
	     * @property {?String} houseNumber
	     * @property {?String} postalCode
	     * @property {?String} area
	     * @property {?String} city
	     * @property {?String} citizenship
	     * @property {?Array.<String>} phone
	     * @property {?String} email
	     */
	
	    /**
	     * @typedef UserData.GetLegalEntity
	     * @type {Object}
	     * @property {String} id
	     * @property {String} externalId
	     * @property {String} name
	     * @property {?String} parentId
	     * @property {?Boolean} isParent
	     */
	
	    /**
	     * @typedef UserData.GetLegalEntities
	     * @type {Array.<UserData.GetLegalEntitiesItem>}
	     */
	
	    /**
	     * @typedef UserData.GetLegalEntitiesItem
	     * @type {Object}
	     * @property {String} id
	     * @property {String} externalId
	     * @property {String} name
	     * @property {?String} parentId
	     * @property {?Boolean} isParent
	     */
	
	    /**
	     * @typedef UserData.IdItem
	     * @type {Object}
	     * @property {String} id
	     */
	
	    /**
	     * @typedef UserData.UserCreateItem
	     * @type {Object}
	     * @property {String} externalId
	     * @property {String} legalEntityExternalId
	     * @property {String} firstName
	     */
	
	    /**
	     * @typedef UserData.EntitlementsAdminCreateItem
	     * @type {Object}
	     * @property {String} externalId
	     * @property {String} legalEntityExternalId
	     */
	
	    /*
	     * @name parse
	     * @type {Function}
	     * @private
	     * @description Should be overitten by transformRespone on a project level
	     */
	    function parse(res) {
	      return {
	        data: res.data,
	        headers: res.headers,
	        status: res.status,
	        statusText: res.statusText
	      };
	    }
	
	    /**
	    * @name UserData#getUsers
	    * @type {Function}
	    * @description # Users list
	    Initial call to retrieve the list of enrolled Users that belong to a given Legal Entity. - Returns data as UserData.GetUsers on success or UserData.BadRequest|UserData.InternalServerError on error
	    
	    * @param {Object} params Map of query parameters.
	      
	    * @param {string} params.entityId Legal Entity ID. Eg: 1002.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .getUsers(params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getUsers(params) {
	      var url = '' + baseUri + version + '/users';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name UserData#postUsersRecord
	    * @type {Function}
	    * @description # Create user
	    Creates a new user that belongs to the given legal entity - Returns data as UserData.IdItem on success or UserData.BadRequest|UserData.InternalServerError on error
	    
	    * @param {UserData.UserCreateItem} data Data to be sent as the request message data.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .postUsersRecord(data)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function postUsersRecord(data) {
	      var url = '' + baseUri + version + '/users';
	
	      return httpClient({
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name UserData#getUsersRecord
	    * @type {Function}
	    * @description # User
	    Get a single User by ID. - Returns data as UserData.UserItem on success or UserData.BadRequest|UserData.ExceptionSchema|UserData.InternalServerError on error
	    
	    * @param {string} userId 
	      
	    
	    * @param {Object} params Map of query parameters.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .getUsersRecord(userId, params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getUsersRecord(userId, params) {
	      var url = '' + baseUri + version + '/users/' + userId;
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name UserData#getUsersExternalIdRecord
	    * @type {Function}
	    * @description # Get User By External Id
	    Initial call to retrieve a single user from the platform
	    using the external identifier. - Returns data as UserData.UserItem on success or UserData.BadRequest|UserData.ExceptionSchema|UserData.InternalServerError on error
	    
	    * @param {string} externalId 
	      
	    
	    * @param {Object} params Map of query parameters.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .getUsersExternalIdRecord(externalId, params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getUsersExternalIdRecord(externalId, params) {
	      var url = '' + baseUri + version + '/users/externalId/' + externalId;
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name UserData#postUsersEntitlementsAdminRecord
	    * @type {Function}
	    * @description # Create Admin User
	    Create Admin required to manage entitlements for the Bank and every Legal Entity. - Returns data as UserData.BadRequest|UserData.ExceptionSchema|UserData.InternalServerError on error
	    
	    * @param {UserData.EntitlementsAdminCreateItem} data Data to be sent as the request message data.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .postUsersEntitlementsAdminRecord(data)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function postUsersEntitlementsAdminRecord(data) {
	      var url = '' + baseUri + version + '/users/entitlementsAdmin';
	
	      return httpClient({
	        method: 'POST',
	        url: url,
	
	        data: data
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name UserData#getUsersLegalentities
	    * @type {Function}
	    * @description # Legal Entities by User list
	    Retrieve all Legal Entities that the User Belongs to - Returns data as UserData.GetLegalEntities on success or UserData.BadRequest|UserData.InternalServerError on error
	    
	    * @param {string} userId 
	      
	    
	    * @param {Object} params Map of query parameters.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .getUsersLegalentities(userId, params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getUsersLegalentities(userId, params) {
	      var url = '' + baseUri + version + '/users/' + userId + '/legalentities';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	    * @name UserData#getUsersExternalIdLegalentities
	    * @type {Function}
	    * @description # Legal Entities by User list
	    Retrieve the Legal Entity that the User Belongs to - Returns data as UserData.GetLegalEntity on success or UserData.BadRequest|UserData.InternalServerError on error
	    
	    * @param {string} externalId 
	      
	    
	    * @param {Object} params Map of query parameters.
	      
	    
	    * @returns {Promise.<Response>} A promise resolving to response object
	    *
	    * @example
	    * userData
	    *  .getUsersExternalIdLegalentities(externalId, params)
	    *  .then(function(result){
	    *    console.log('headers', result.headers)
	    *    console.log('data', result.data);
	    *  });
	    */
	    function getUsersExternalIdLegalentities(externalId, params) {
	      var url = '' + baseUri + version + '/users/externalId/' + externalId + '/legalentities';
	
	      return httpClient({
	        method: 'GET',
	        url: url,
	
	        params: params
	
	      }).then(parse).catch(function (err) {
	        throw parse(err);
	      });
	    }
	
	    /**
	     * @description
	     * Schema data. Keys of the object are names of the POST and PUT methods
	     *
	     * Note: The schema is not strictly a JSON schema. It is a whitelisted set of
	     * keys for each object property. The keys that are exposed are meant for validation
	     * purposes.
	     *
	     * The full list of *possible* keys for each property is:
	     * type, minimum, maximum, minLength, maxLength, pattern, enum, format, default,
	     * properties, items, minItems, maxItems, uniqueItems and required.
	     *
	     * See http://json-schema.org/latest/json-schema-validation.html for more details
	     * on the meaning of these keys.
	     *
	     * The "required" array from JSON schema is tranformed into a "required" boolean
	     * on each property. This is for ease of use.
	     *
	     * @name UserData#schemas
	     * @type {Object}
	     */
	    var schemas = {};
	
	    /**
	     * @description
	     * An object describing the JSON schema for the postUsersRecord method
	     *
	     * @name UserData#schemas.postUsersRecord
	     * @type {Object}
	     * @example
	     * {
	    "properties": {
	      "externalId": {
	        "type": "string",
	        "required": true
	      },
	      "legalEntityExternalId": {
	        "type": "string",
	        "required": true
	      },
	      "firstName": {
	        "type": "string",
	        "required": true
	      }
	    }
	    }
	     */
	
	    schemas.postUsersRecord = definedTypes['UserData.UserCreateItem'];
	
	    /**
	     * @description
	     * An object describing the JSON schema for the postUsersEntitlementsAdminRecord method
	     *
	     * @name UserData#schemas.postUsersEntitlementsAdminRecord
	     * @type {Object}
	     * @example
	     * {
	    "properties": {
	      "externalId": {
	        "type": "string",
	        "required": true
	      },
	      "legalEntityExternalId": {
	        "type": "string",
	        "required": true
	      }
	    }
	    }
	     */
	
	    schemas.postUsersEntitlementsAdminRecord = definedTypes['UserData.EntitlementsAdminCreateItem'];
	
	    /**
	     * @typedef Response
	     * @type {Object}
	     * @property {Object} data See method descriptions for possible return types
	     * @property {Function} headers Getter headers function
	     * @property {Number} status HTTP status code of the response.
	     * @property {String} statusText HTTP status text of the response.
	     */
	
	    return {
	
	      getUsers: getUsers,
	
	      postUsersRecord: postUsersRecord,
	
	      getUsersRecord: getUsersRecord,
	
	      getUsersExternalIdRecord: getUsersExternalIdRecord,
	
	      postUsersEntitlementsAdminRecord: postUsersEntitlementsAdminRecord,
	
	      getUsersLegalentities: getUsersLegalentities,
	
	      getUsersExternalIdLegalentities: getUsersExternalIdLegalentities,
	
	      schemas: schemas
	    };
	  };
	};

/***/ }
/******/ ])
});
;
//# sourceMappingURL=data-bb-user-http-ng.js.map