# model-bb-notifications-ng


Version: **1.2.59**

Notification widgets model.

## Imports

* data-bb-notifications-http-ng
* lib-bb-event-bus-ng
* lib-bb-model-errors
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
import modelNotificationsModuleKey, { modelNotificationsKey } from 'model-bb-notifications-ng';
```

## Table of Contents
- **NotificationModel**<br/>    <a href="#NotificationModel#load">#load(params)</a><br/>    <a href="#NotificationModel#loadStream">#loadStream(params)</a><br/>    <a href="#NotificationModel#create">#create(params)</a><br/>    <a href="#NotificationModel#loadUnreadCount">#loadUnreadCount()</a><br/>    <a href="#NotificationModel#putReadRecord">#putReadRecord(notificationID, data)</a><br/>    <a href="#NotificationModel#deleteRecord">#deleteRecord(notificationID)</a><br/>    <a href="#NotificationModel#getNotificationPreferences">#getNotificationPreferences()</a><br/>    <a href="#NotificationModel#stopPolling">#stopPolling(ref)</a><br/>    <a href="#NotificationModel#initPolling">#initPolling(options)</a><br/>
- **Type Definitions**<br/>    <a href="#undefined">undefined</a><br/>    <a href="#Event">Event</a><br/>    <a href="#PollingType">PollingType</a><br/>    <a href="#PollingOptions">PollingOptions</a><br/>    <a href="#UnreadCount">UnreadCount</a><br/>    <a href="#Notifications">Notifications</a><br/>    <a href="#Notification">Notification</a><br/>

## Exports


## PollingType

Polling types

---

## NotificationModel

Model factory for widget-bb-notification-center-ng, widget-bb-notification-badge-ng,
widget-bb-notification-popups-ng

### <a name="NotificationModel#load"></a>*#load(params)*

Load notifications.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Optional configuration object. |

##### Returns

Promise of <a href="#Notifications">Notifications</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with Notifications.*

### <a name="NotificationModel#loadStream"></a>*#loadStream(params)*

Load notifications stream.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Optional configuration object. |

##### Returns

Promise of <a href="#Notifications">Notifications</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with Notifications.*

### <a name="NotificationModel#create"></a>*#create(params)*

Create notification.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | <a href="data-bb-notifications-ng.html#NotificationsData.CreateNotificationsCommand">NotificationsData.CreateNotificationsCommand</a> | Configuration object to create notification |

##### Returns

Promise of Object, <a href="#ModelError">ModelError</a> - **

### <a name="NotificationModel#loadUnreadCount"></a>*#loadUnreadCount()*

Load unread count of notifications.

##### Returns

<a href="#Promise<UnreadCount">Promise<UnreadCount</a>, <a href="#ModelError>">ModelError></a> - **

### <a name="NotificationModel#putReadRecord"></a>*#putReadRecord(notificationID, data)*

Set read/unread status of notification.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| notificationID | String | Notification ID |
| data | Object | Object with new read status |

##### Returns

Promise of <a href="#void">void</a>, <a href="#ModelError">ModelError</a> - **

### <a name="NotificationModel#deleteRecord"></a>*#deleteRecord(notificationID)*

Delete notification.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| notificationID | String | Notification ID |

##### Returns

Promise of <a href="#void">void</a>, <a href="#ModelError">ModelError</a> - **

### <a name="NotificationModel#getNotificationPreferences"></a>*#getNotificationPreferences()*

Getting notifications preferences from widget
Usage of "itemsPerPage" is deprecated in favor of "pageSize"

##### Returns

Object - *Preferences object*

### <a name="NotificationModel#stopPolling"></a>*#stopPolling(ref)*

Stop polling.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ref | String | Polling (interval) reference |

### <a name="NotificationModel#initPolling"></a>*#initPolling(options)*

Subscribe to stream notifications loading.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| options | <a href="#PollingOptions">PollingOptions</a> | Subscribe options |

##### Returns

String - *Polling (interval) reference*

## Type Definitions


### <a name="undefined"></a>*undefined*

Widget preferences enum
Usage of "itemsPerPage" is deprecated in favor of "pageSize"


**Type:** *Object*


### <a name="Event"></a>*Event*

Event enums

**Type:** *Object*


### <a name="PollingType"></a>*PollingType*

polling type enums

**Type:** *Object*


### <a name="PollingOptions"></a>*PollingOptions*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| type | String | Polling type |
| pollingInterval | Number | Polling interval time |
| params | Object (optional) | Optional parameters that apply to polling method |

### <a name="UnreadCount"></a>*UnreadCount*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| unread | Number | total count of unread notifications |

### <a name="Notifications"></a>*Notifications*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of <a href="#Notification">Notification</a> | array of Notifications |
| totalCount | Number (optional) | total count of notifications if needed |

### <a name="Notification"></a>*Notification*

Notification type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | Notification ID |
| message | String | Notification message |
| level | String | Severity Level |
| createdOn | String | Creation date |
| link | String (optional) | ptional link in message |
| validFrom | String (optional) | the date when the notification will be available |
| expiresOn | String (optional) | End date of sticky notification |
| read | Boolean | Read status |
| origin | String | Notification Group |

---
