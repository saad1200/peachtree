# widget-bb-initiate-payment-ng


Version: **1.1.5**

Initiate payment widget

## Imports

* lib-bb-event-bus-ng
* lib-bb-model-errors
* lib-bb-storage-ng
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-payment-orders-ng
* vendor-bb-angular

---

## Table of Contents
- **Type Definitions**<br/>    <a href="#Validation">Validation</a><br/>    <a href="#ValidationMessage">ValidationMessage</a><br/>

---

## Preference

Widget preferences enum

---

## singleTransfer

Single transfer constant

---

## EndingType

Available payment order ending types

---

---

## Type Definitions


### <a name="Validation"></a>*Validation*

Validation object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| valid | Boolean |  |
| messages | Array of <a href="#ValidationMessage">ValidationMessage</a> |  |

### <a name="ValidationMessage"></a>*ValidationMessage*

Validation Messages object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| messageKey | String |  |
| type | String |  |

---

## Templates

* *template.ng.html*

---
