# model-bb-payment-orders-ng


Version: **1.0.15**

Model for widget-bb-payment-orders-ng

## Imports

* data-bb-contact-http-ng
* data-bb-payment-orders-http-ng
* data-bb-product-summary-http-ng
* lib-bb-model-errors
* vendor-bb-angular

---

## Example

```javascript
import modelPaymentOrdersModuleKey, { modelPaymentOrdersKey } from 'model-bb-payment-orders-ng';

angular
  .module('ExampleModule', [
    modelPaymentOrdersModuleKey,
  ])
  .factory('someFactory', [
    modelPaymentOrdersKey,
    // into
    function someFactory(paymentOrdersModel) {
      // ...
    },
  ]);
```

## Table of Contents
- **paymentOrdersModel**<br/>    <a href="#paymentOrdersModel#createPaymentOrder">#createPaymentOrder(paymentOrderParams)</a><br/>    <a href="#paymentOrdersModel#getCurrencies">#getCurrencies()</a><br/>    <a href="#paymentOrdersModel#getAccountsFrom">#getAccountsFrom()</a><br/>    <a href="#paymentOrdersModel#getAccountsTo">#getAccountsTo(debitAccountId)</a><br/>    <a href="#paymentOrdersModel#getExternals">#getExternals()</a><br/>    <a href="#paymentOrdersModel#getRate">#getRate(rateParams)</a><br/>    <a href="#paymentOrdersModel#createContact">#createContact(contact)</a><br/>

---

## ExternalType

Identifier and name for external account product kind

---

## paymentOrdersModel


### <a name="paymentOrdersModel#createPaymentOrder"></a>*#createPaymentOrder(paymentOrderParams)*

Create new payment order.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentOrderParams | Object | New payment order data |

##### Returns

Promise of Object - *A Promise with response.*

### <a name="paymentOrdersModel#getCurrencies"></a>*#getCurrencies()*

Get available currencies.

##### Returns

Promise of Array of Object - *A Promise with response.*

### <a name="paymentOrdersModel#getAccountsFrom"></a>*#getAccountsFrom()*

Load accounts available to payment from.

##### Returns

Promise of Array of Object - *A Promise with flat accounts list.*

### <a name="paymentOrdersModel#getAccountsTo"></a>*#getAccountsTo(debitAccountId)*

Load accounts available for payment to.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccountId | String | Filter account list with debitAccountId param |

##### Returns

Promise of Array of Object - *A Promise with flat accounts list.*

### <a name="paymentOrdersModel#getExternals"></a>*#getExternals()*

Load external accounts from contact list.

##### Returns

Promise of Array of Object - *A Promise with flat accounts list.*

### <a name="paymentOrdersModel#getRate"></a>*#getRate(rateParams)*

Get currencies available for payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| rateParams | Object | Parameters for getRate request |

##### Returns

Number - *Rate number*

### <a name="paymentOrdersModel#createContact"></a>*#createContact(contact)*

Creates a new contact

| Parameter | Type | Description |
| :-- | :-- | :-- |
| contact | Object | Contact data |

##### Returns

Promise - *A Promise object for create contact request*
