(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("data-bb-payment-orders-http-ng"), require("data-bb-product-summary-http-ng"), require("data-bb-contact-http-ng"), require("lib-bb-model-errors"));
	else if(typeof define === 'function' && define.amd)
		define("model-bb-payment-orders-ng", ["vendor-bb-angular", "data-bb-payment-orders-http-ng", "data-bb-product-summary-http-ng", "data-bb-contact-http-ng", "lib-bb-model-errors"], factory);
	else if(typeof exports === 'object')
		exports["model-bb-payment-orders-ng"] = factory(require("vendor-bb-angular"), require("data-bb-payment-orders-http-ng"), require("data-bb-product-summary-http-ng"), require("data-bb-contact-http-ng"), require("lib-bb-model-errors"));
	else
		root["model-bb-payment-orders-ng"] = factory(root["vendor-bb-angular"], root["data-bb-payment-orders-http-ng"], root["data-bb-product-summary-http-ng"], root["data-bb-contact-http-ng"], root["lib-bb-model-errors"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_55__, __WEBPACK_EXTERNAL_MODULE_56__, __WEBPACK_EXTERNAL_MODULE_57__, __WEBPACK_EXTERNAL_MODULE_58__, __WEBPACK_EXTERNAL_MODULE_60__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(54);

/***/ }),

/***/ 54:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.modelPaymentOrdersKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(55);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _dataBbPaymentOrdersHttpNg = __webpack_require__(56);
	
	var _dataBbPaymentOrdersHttpNg2 = _interopRequireDefault(_dataBbPaymentOrdersHttpNg);
	
	var _dataBbProductSummaryHttpNg = __webpack_require__(57);
	
	var _dataBbProductSummaryHttpNg2 = _interopRequireDefault(_dataBbProductSummaryHttpNg);
	
	var _dataBbContactHttpNg = __webpack_require__(58);
	
	var _dataBbContactHttpNg2 = _interopRequireDefault(_dataBbContactHttpNg);
	
	var _paymentOrders = __webpack_require__(59);
	
	var _paymentOrders2 = _interopRequireDefault(_paymentOrders);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var moduleKey = 'model-bb-payment-orders-ng'; /**
	                                               * @module model-bb-payment-orders-ng
	                                               *
	                                               * @description
	                                               * Model for widget-bb-payment-orders-ng
	                                               *
	                                               * @example
	                                               * import modelPaymentOrdersModuleKey, { modelPaymentOrdersKey } from 'model-bb-payment-orders-ng';
	                                               *
	                                               * angular
	                                               *   .module('ExampleModule', [
	                                               *     modelPaymentOrdersModuleKey,
	                                               *   ])
	                                               *   .factory('someFactory', [
	                                               *     modelPaymentOrdersKey,
	                                               *     // into
	                                               *     function someFactory(paymentOrdersModel) {
	                                               *       // ...
	                                               *     },
	                                               *   ]);
	                                               */
	var modelPaymentOrdersKey = exports.modelPaymentOrdersKey = moduleKey + ':model';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_dataBbPaymentOrdersHttpNg2.default, _dataBbProductSummaryHttpNg2.default, _dataBbContactHttpNg2.default]).factory(modelPaymentOrdersKey, [_dataBbPaymentOrdersHttpNg.paymentOrdersDataKey, _dataBbProductSummaryHttpNg.productSummaryDataKey, _dataBbContactHttpNg.contactDataKey,
	/* into */
	_paymentOrders2.default]).name;

/***/ }),

/***/ 55:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_55__;

/***/ }),

/***/ 56:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_56__;

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_57__;

/***/ }),

/***/ 58:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_58__;

/***/ }),

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = paymentOrdersModel;
	
	var _libBbModelErrors = __webpack_require__(60);
	
	var _accountModel = __webpack_require__(61);
	
	var _accountModel2 = _interopRequireDefault(_accountModel);
	
	var _constants = __webpack_require__(62);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	/**
	 * @description
	 * Method to normalize products data
	 *
	 * @inner
	 * @param {object} rawData Raw response data object
	 * @returns {object[]} An array of products
	 */
	var convertToAccountsArray = function convertToAccountsArray(rawData) {
	  return Object.keys(rawData).filter(function (kind) {
	    return rawData[kind].products && rawData[kind].products.length > 0;
	  }).reduce(function (memo, kind) {
	    return memo.concat(rawData[kind].products.map((0, _accountModel2.default)(kind)));
	  }, []);
	};
	
	/**
	 * @description
	 * Method to format external contacts data as product kind.
	 *
	 * @inner
	 * @param {object} rawData Contact object.
	 * @returns {object} External product object.
	 */
	var convertExternalsToProductKind = function convertExternalsToProductKind(rawData) {
	  return _defineProperty({}, _constants.ExternalType.IDENTIFIER, {
	    name: _constants.ExternalType.NAME,
	    products: rawData,
	    aggregatedBalance: 0.0
	  });
	};
	
	/**
	 * Model factory for model-bb-payment-orders-ng
	 *
	 * @inner
	 * @type {function}
	 * @param {Object} Promise An ES2015 compatible `Promise` object.
	 *
	 * @return {Object}
	 */
	function paymentOrdersModel(paymentOrdersData, productSummaryData, contactData) {
	  /**
	   * @name paymentOrdersModel#createPaymentOrder
	   * @type {function}
	   *
	   * @description
	   * Create new payment order.
	   *
	   * @param {object} paymentOrderParams New payment order data
	   * @returns {Promise.<object>} A Promise with response.
	   */
	  var createPaymentOrder = function createPaymentOrder(paymentOrderParams) {
	    return paymentOrdersData.postPaymentOrdersRecord(paymentOrderParams).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @name paymentOrdersModel#getCurrencies
	   * @type {function}
	   *
	   * @description
	   * Get available currencies.
	   *
	   * @returns {Promise.<object[]>} A Promise with response.
	   */
	  var getCurrencies = function getCurrencies() {
	    return paymentOrdersData.getPaymentOrdersCurrencies()
	    // Convert currencies format to the format used in the widgets
	    .then(function (_ref2) {
	      var data = _ref2.data;
	      return data.map(function (_ref3) {
	        var code = _ref3.code;
	        return { name: code };
	      });
	    }).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @name paymentOrdersModel#getAccountsFrom
	   * @type {function}
	   *
	   * @description
	   * Load accounts available to payment from.
	   *
	   * @returns {Promise.<object[]>} A Promise with flat accounts list.
	   */
	  var getAccountsFrom = function getAccountsFrom() {
	    return productSummaryData.getProductsummaryDebitaccounts().then(function (_ref4) {
	      var data = _ref4.data;
	      return data;
	    }).then(convertToAccountsArray).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @name paymentOrdersModel#getAccountsTo
	   * @type {function}
	   *
	   * @description
	   * Load accounts available for payment to.
	   *
	   * @param {string} debitAccountId Filter account list with debitAccountId param
	   * @returns {Promise.<object[]>} A Promise with flat accounts list.
	   */
	  var getAccountsTo = function getAccountsTo(debitAccountId) {
	    return productSummaryData.getProductsummaryCreditaccounts({ debitAccountId: debitAccountId }).then(function (_ref5) {
	      var data = _ref5.data;
	      return data;
	    }).then(convertToAccountsArray).then(function (accounts) {
	      return accounts.filter(function (account) {
	        return account.id !== debitAccountId;
	      });
	    }).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @name paymentOrdersModel#getExternals
	   * @type {function}
	   *
	   * @description
	   * Load external accounts from contact list.
	   *
	   * @returns {Promise.<object[]>} A Promise with flat accounts list.
	   */
	  var getExternals = function getExternals() {
	    return contactData.getContacts().then(function (_ref6) {
	      var data = _ref6.data;
	      return data;
	    }).then(convertExternalsToProductKind).then(convertToAccountsArray).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @description
	   * Get currencies available for payment.
	   *
	   * @name paymentOrdersModel#getRate
	   * @type {function}
	   * @param {object} rateParams Parameters for getRate request
	   * @returns {number} Rate number
	   */
	  var getRate = function getRate(rateParams) {
	    return paymentOrdersData.getPaymentOrdersRate(rateParams).then(function (_ref7) {
	      var data = _ref7.data;
	      return data.rate;
	    }).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @name paymentOrdersModel#createContact
	   * @type {function}
	   *
	   * @description
	   * Creates a new contact
	   *
	   * @param {object} contact Contact data
	   * @returns {Promise} A Promise object for create contact request
	   */
	  var createContact = function createContact(contact) {
	    return contactData.postContactsRecord(contact).catch(function (httpErrorResponse) {
	      throw (0, _libBbModelErrors.fromHttpError)(httpErrorResponse);
	    });
	  };
	
	  /**
	   * @name paymentOrdersModel
	   * @type {Object}
	   */
	  return {
	    createPaymentOrder: createPaymentOrder,
	    getCurrencies: getCurrencies,
	    getAccountsFrom: getAccountsFrom,
	    getAccountsTo: getAccountsTo,
	    getExternals: getExternals,
	    getRate: getRate,
	    createContact: createContact
	  };
	}

/***/ }),

/***/ 60:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_60__;

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _constants = __webpack_require__(62);
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var maskCardNumber = function maskCardNumber(suffix) {
	  return suffix ? 'XXXX-XXXX-XXXX-' + suffix : '';
	};
	
	var defaultViewModelFactory = function defaultViewModelFactory(_ref) {
	  var id = _ref.id,
	      name = _ref.name,
	      currency = _ref.currency,
	      externalTransferAllowed = _ref.externalTransferAllowed,
	      crossCurrencyAllowed = _ref.crossCurrencyAllowed;
	  return {
	    id: id,
	    name: name,
	    currency: currency,
	    externalTransferAllowed: externalTransferAllowed,
	    crossCurrencyAllowed: crossCurrencyAllowed
	  };
	};
	
	var viewModelFactories = _defineProperty({
	  currentAccounts: function currentAccounts(account) {
	    return Object.assign({
	      identifier: account.IBAN || account.BBAN,
	      amount: account.availableBalance
	    }, account);
	  },
	
	  savingsAccounts: function savingsAccounts(account) {
	    return Object.assign({
	      identifier: account.BBAN || account.IBAN,
	      amount: account.bookedBalance
	    }, account);
	  },
	
	  termDeposits: function termDeposits(account) {
	    return Object.assign({
	      amount: account.principalAmount
	    }, account);
	  },
	
	  loans: function loans(account) {
	    return Object.assign({
	      amount: account.bookedBalance
	    }, account);
	  },
	
	  creditCards: function creditCards(account) {
	    return Object.assign({
	      identifier: maskCardNumber(account.cardNumberSuffix),
	      amount: account.availableBalance
	    }, account);
	  },
	
	  investmentAccounts: function investmentAccounts(account) {
	    return Object.assign({
	      amount: account.currentInvestmentValue
	    }, account);
	  }
	
	}, _constants.ExternalType.IDENTIFIER, function (contact) {
	  return Object.assign({
	    identifier: contact.accounts[0].IBAN,
	    external: true
	  }, contact);
	});
	
	var viewModelFactory = function viewModelFactory(kind, account) {
	  return (viewModelFactories[kind] || defaultViewModelFactory)(account);
	};
	
	/**
	 * Prepare the fields of a account into a form ready for display to the User
	 *
	 * @inner
	 * @param {object} account The source account from the API
	 * @returns {AccountView}
	 */
	
	exports.default = function (kindId) {
	  return function (account) {
	    return viewModelFactory(kindId, account);
	  };
	};

/***/ }),

/***/ 62:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @description
	 * Identifier and name for external account product kind
	 *
	 * @name ExternalType
	 * @type {object}
	 */
	// eslint-disable-next-line import/prefer-default-export
	var ExternalType = exports.ExternalType = {
	  IDENTIFIER: 'ExternalAccounts',
	  NAME: 'Contacts'
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=model-bb-payment-orders-ng.js.map