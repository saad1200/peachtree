# ext-bb-payment-ng


Version: **1.0.209**

Payment default extension.

## Imports

* ui-bb-account-card
* ui-bb-account-selector
* ui-bb-calendar-popup-ng
* ui-bb-confirm-ng
* ui-bb-credit-suggest-ng
* ui-bb-currency-input-ng
* ui-bb-dropdown-select
* ui-bb-expandable-ng
* ui-bb-i18n-ng
* ui-bb-loading-indicator-ng
* ui-bb-number-input-ng
* ui-bb-parent-responsiveness-ng
* ui-bb-substitute-error-ng
* ui-bb-switcher-ng
* vendor-bb-angular-ng-aria
* vendor-bb-angular-ng-messages
* vendor-bb-uib-alert
* vendor-bb-uib-popover

---

## Example

```javascript
<!-- payment widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bb-payment-ng</value>
</property>

Usage of ui-bb-account-card component in template

<ui-bb-account-card
  account-name="$ctrl.payment.from.name"
  account-number="$ctrl.payment.from.identifier"
  amount="$ctrl.payment.from.amount"
  currency="$ctrl.payment.from.currency"
  show-avatar="true">
</ui-bb-account-card>

Usage of ui-bb-account-selector component in template

<ui-bb-account-selector
  ng-model="$ctrl.payment.from"
  accounts="$ctrl.accountsFrom"
  ng-change="$ctrl.onAccountFromChange()"
  custom-template-id="ui-bb-account-selector/option-template.htm"
  required>
</ui-bb-account-selector>

where
accounts {array} List of accounts
ng-change {function} Method that will be executed when product selection is changed
custom-template-id {string} Id of the template that will be used for rendering
instead of default one

Usage of ui-bb-currency-input-ng component in template

<ui-bb-currency-input-ng
  max-length="6"
  decimal-max-length="2"
  placeholder="0"
  ng-model="$ctrl.payment.amount"
  currencies="$ctrl.currencies">
</ui-bb-currency-input-ng>

where
max-length {number} Maximum number of digits allowed in whole part
decimal-max-length {number} Maximum number of digits allowed in decimal part
placeholder {string} Text to display for input's placeholder
currencies {string[]} List of currencies available

Usage of ui-bb-credit-suggest-ng component in template

<ui-bb-credit-suggest-ng
  name="credit"
  ng-model="$ctrl.payment.to"
  accounts="$ctrl.accountsTo"
  get-accounts="ext.helpers.getAccounts(search, accounts)"
  messages="{
    noResults: ('ui-bb-credit-suggest-ng.filter.noResults' | i18n),
  }"
  allow-external="$ctrl.payment.from.externalTransferAllowed"
  custom-template-id="ui-bb-credit-suggest-ng/template/option.html"
  required>
</ui-bb-credit-suggest-ng>

where
accounts {array} List of accounts to filter and select with user input
get-accounts {function} External method for transform accounts array into custom structure
Can be defined in extensions helpers
messages {object} Localized messages
allow-external {boolean} Are external accounts included in list.
If not, IBAN field stays disabled
custom-template-id {string} Id of the template that will be used for rendering
instead of default one
```

## Table of Contents
- **ext-bb-payment-ng**<br/>    <a href="#ext-bb-payment-ngtransferFrequencies">transferFrequencies</a><br/>    <a href="#ext-bb-payment-ngextendFrequencies">extendFrequencies(Single, Payment)</a><br/>    <a href="#ext-bb-payment-nggetScheduleText">getScheduleText(controller)</a><br/>    <a href="#ext-bb-payment-ngtoggle">toggle(event, model, controller)</a><br/>    <a href="#ext-bb-payment-nggetAccounts">getAccounts(search, accounts)</a><br/>    <a href="#ext-bb-payment-ngshowCrossCurrencyMessage">showCrossCurrencyMessage(controller)</a><br/>    <a href="#ext-bb-payment-ngmakePayment">makePayment($ctrl, $parent)</a><br/>    <a href="#ext-bb-payment-ngprocessAccountsTo">processAccountsTo(debitAccount, getCreditAccounts, getExternalContacts)</a><br/>    <a href="#ext-bb-payment-nggetRecurringTransactionDay">getRecurringTransactionDay(startDate, transferFrequency)</a><br/>

---
### <a name="ext-bb-payment-ngtransferFrequencies"></a>*transferFrequencies*

Array of recurring frequency objects with the following properties (all mandatory)

**Type:** *Array*


| Property | Type | Description |
| :-- | :-- | :-- |
| object.name | String | Translation key of the label that will be displayed to the end user |
| object.value | String | Denotes frequency type of transfer. Possible values: DAILY/WEEKLY/MONTHLY/YEARLY |
| object.every | Number | Indicates skip interval of transfer. 1 would mean execute every time, 2 - every other time |

## Example

```javascript
{
  name: 'form.schedule.frequency.weekly',
  value: 'WEEKLY',
  every: 1,
},
{
  name: 'form.schedule.frequency.bi.weekly',
  value: 'WEEKLY',
  every: 2,
}
```

---

### <a name="ext-bb-payment-ngextendFrequencies"></a>*extendFrequencies(Single, Payment)*

Extend default single payment transfer frequency with extension specific frequencies

| Parameter | Type | Description |
| :-- | :-- | :-- |
| Single | Object | transfer object from widget controller |
| Payment | Object | preferences obtained from widget |

##### Returns

Array - *Array with all frequencies available*

---

### <a name="ext-bb-payment-nggetScheduleText"></a>*getScheduleText(controller)*

Compiles the scheduling description out of payment object params.

In this process, following translation keys are being used:

form.schedule.starting, for word "Starting"

form.schedule.today, for word "Today"

form.schedule.on, for word "On" (used before date to form "on 01.01.2017")

form.schedule.until, for word "until" (used before date to form "until 01.01.2017")

form.schedule.repeat.count, for word "times" (used after repeat count to form "5 times")

and name of the transfer frequency set in constants file

| Parameter | Type | Description |
| :-- | :-- | :-- |
| controller | Object | PaymentController |

##### Returns

String - *Compiled text that can be used as scheduling value in views*

---

### <a name="ext-bb-payment-ngtoggle"></a>*toggle(event, model, controller)*

Toggle group in credit suggest component

| Parameter | Type | Description |
| :-- | :-- | :-- |
| event | Object |  |
| model | Object |  |
| controller | Object |  |

---

### <a name="ext-bb-payment-nggetAccounts"></a>*getAccounts(search, accounts)*

Transforms accounts list to custom structure

| Parameter | Type | Description |
| :-- | :-- | :-- |
| search | String |  |
| accounts | <a href="#array<object>">array<object></a> |  |

##### Returns

<a href="#array<object>">array<object></a> - *viewAccounts*

---

### <a name="ext-bb-payment-ngshowCrossCurrencyMessage"></a>*showCrossCurrencyMessage(controller)*

If cross currency message should be shown

| Parameter | Type | Description |
| :-- | :-- | :-- |
| controller | Object |  |

##### Returns

Object - *messages*

---

### <a name="ext-bb-payment-ngmakePayment"></a>*makePayment($ctrl, $parent)*

Makes new payment request and changes step on success

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | Payment controller |
| $parent | Object | Parent ng scope |

##### Returns

Object - *Payment request Promise*

---

### <a name="ext-bb-payment-ngprocessAccountsTo"></a>*processAccountsTo(debitAccount, getCreditAccounts, getExternalContacts)*

Hook for processing credit account list.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| debitAccount | <a href="#ProductKind">ProductKind</a> | Selected debit account (can be null) |
| getCreditAccounts | Function | Function to retrieve all credit accounts |
| getExternalContacts | Function | Function to retrieve all external contacts formatted like Product kind |

##### Returns

Promise of Array of <a href="#any">any</a> - *Promise that retrieves array of accounts.*

---

### <a name="ext-bb-payment-nggetRecurringTransactionDay"></a>*getRecurringTransactionDay(startDate, transferFrequency)*

Denotes day on which transfer should be executed.

For weekly it will be 1..7 indicating weekday.

For monthly it will be 1..31 indicating day of month.

For yearly it will be 1..12 indicating month of the year

Assigned to [$ctrl.schedule.on]<a href="#PaymentController#makePayment">PaymentController#makePayment</a>

| Parameter | Type | Description |
| :-- | :-- | :-- |
| startDate | Date | Start date of recurring payment |
| transferFrequency | Object | Recurring frequency |

##### Returns

Number - **
