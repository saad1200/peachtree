(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("ui-bb-account-card"), require("ui-bb-account-selector"), require("ui-bb-confirm-ng"), require("ui-bb-dropdown-select"), require("ui-bb-substitute-error-ng"), require("vendor-bb-uib-alert"), require("vendor-bb-angular-ng-messages"), require("ui-bb-expandable-ng"), require("ui-bb-currency-input-ng"), require("ui-bb-calendar-popup-ng"), require("ui-bb-credit-suggest-ng"), require("ui-bb-switcher-ng"), require("ui-bb-loading-indicator-ng"), require("ui-bb-i18n-ng"), require("ui-bb-parent-responsiveness-ng"), require("vendor-bb-uib-popover"), require("vendor-bb-angular-ng-aria"), require("ui-bb-number-input-ng"));
	else if(typeof define === 'function' && define.amd)
		define("ext-bb-payment-ng", ["ui-bb-account-card", "ui-bb-account-selector", "ui-bb-confirm-ng", "ui-bb-dropdown-select", "ui-bb-substitute-error-ng", "vendor-bb-uib-alert", "vendor-bb-angular-ng-messages", "ui-bb-expandable-ng", "ui-bb-currency-input-ng", "ui-bb-calendar-popup-ng", "ui-bb-credit-suggest-ng", "ui-bb-switcher-ng", "ui-bb-loading-indicator-ng", "ui-bb-i18n-ng", "ui-bb-parent-responsiveness-ng", "vendor-bb-uib-popover", "vendor-bb-angular-ng-aria", "ui-bb-number-input-ng"], factory);
	else if(typeof exports === 'object')
		exports["ext-bb-payment-ng"] = factory(require("ui-bb-account-card"), require("ui-bb-account-selector"), require("ui-bb-confirm-ng"), require("ui-bb-dropdown-select"), require("ui-bb-substitute-error-ng"), require("vendor-bb-uib-alert"), require("vendor-bb-angular-ng-messages"), require("ui-bb-expandable-ng"), require("ui-bb-currency-input-ng"), require("ui-bb-calendar-popup-ng"), require("ui-bb-credit-suggest-ng"), require("ui-bb-switcher-ng"), require("ui-bb-loading-indicator-ng"), require("ui-bb-i18n-ng"), require("ui-bb-parent-responsiveness-ng"), require("vendor-bb-uib-popover"), require("vendor-bb-angular-ng-aria"), require("ui-bb-number-input-ng"));
	else
		root["ext-bb-payment-ng"] = factory(root["ui-bb-account-card"], root["ui-bb-account-selector"], root["ui-bb-confirm-ng"], root["ui-bb-dropdown-select"], root["ui-bb-substitute-error-ng"], root["vendor-bb-uib-alert"], root["vendor-bb-angular-ng-messages"], root["ui-bb-expandable-ng"], root["ui-bb-currency-input-ng"], root["ui-bb-calendar-popup-ng"], root["ui-bb-credit-suggest-ng"], root["ui-bb-switcher-ng"], root["ui-bb-loading-indicator-ng"], root["ui-bb-i18n-ng"], root["ui-bb-parent-responsiveness-ng"], root["vendor-bb-uib-popover"], root["vendor-bb-angular-ng-aria"], root["ui-bb-number-input-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_6__, __WEBPACK_EXTERNAL_MODULE_7__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_9__, __WEBPACK_EXTERNAL_MODULE_10__, __WEBPACK_EXTERNAL_MODULE_11__, __WEBPACK_EXTERNAL_MODULE_12__, __WEBPACK_EXTERNAL_MODULE_13__, __WEBPACK_EXTERNAL_MODULE_14__, __WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_16__, __WEBPACK_EXTERNAL_MODULE_17__, __WEBPACK_EXTERNAL_MODULE_18__, __WEBPACK_EXTERNAL_MODULE_19__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.hooks = exports.helpers = exports.dependencyKeys = undefined;
	
	var _uiBbAccountCard = __webpack_require__(2);
	
	var _uiBbAccountCard2 = _interopRequireDefault(_uiBbAccountCard);
	
	var _uiBbAccountSelector = __webpack_require__(3);
	
	var _uiBbAccountSelector2 = _interopRequireDefault(_uiBbAccountSelector);
	
	var _uiBbConfirmNg = __webpack_require__(4);
	
	var _uiBbConfirmNg2 = _interopRequireDefault(_uiBbConfirmNg);
	
	var _uiBbDropdownSelect = __webpack_require__(5);
	
	var _uiBbDropdownSelect2 = _interopRequireDefault(_uiBbDropdownSelect);
	
	var _uiBbSubstituteErrorNg = __webpack_require__(6);
	
	var _uiBbSubstituteErrorNg2 = _interopRequireDefault(_uiBbSubstituteErrorNg);
	
	var _vendorBbUibAlert = __webpack_require__(7);
	
	var _vendorBbUibAlert2 = _interopRequireDefault(_vendorBbUibAlert);
	
	var _vendorBbAngularNgMessages = __webpack_require__(8);
	
	var _vendorBbAngularNgMessages2 = _interopRequireDefault(_vendorBbAngularNgMessages);
	
	var _uiBbExpandableNg = __webpack_require__(9);
	
	var _uiBbExpandableNg2 = _interopRequireDefault(_uiBbExpandableNg);
	
	var _uiBbCurrencyInputNg = __webpack_require__(10);
	
	var _uiBbCurrencyInputNg2 = _interopRequireDefault(_uiBbCurrencyInputNg);
	
	var _uiBbCalendarPopupNg = __webpack_require__(11);
	
	var _uiBbCalendarPopupNg2 = _interopRequireDefault(_uiBbCalendarPopupNg);
	
	var _uiBbCreditSuggestNg = __webpack_require__(12);
	
	var _uiBbCreditSuggestNg2 = _interopRequireDefault(_uiBbCreditSuggestNg);
	
	var _uiBbSwitcherNg = __webpack_require__(13);
	
	var _uiBbSwitcherNg2 = _interopRequireDefault(_uiBbSwitcherNg);
	
	var _uiBbLoadingIndicatorNg = __webpack_require__(14);
	
	var _uiBbLoadingIndicatorNg2 = _interopRequireDefault(_uiBbLoadingIndicatorNg);
	
	var _uiBbI18nNg = __webpack_require__(15);
	
	var _uiBbI18nNg2 = _interopRequireDefault(_uiBbI18nNg);
	
	var _uiBbParentResponsivenessNg = __webpack_require__(16);
	
	var _uiBbParentResponsivenessNg2 = _interopRequireDefault(_uiBbParentResponsivenessNg);
	
	var _vendorBbUibPopover = __webpack_require__(17);
	
	var _vendorBbUibPopover2 = _interopRequireDefault(_vendorBbUibPopover);
	
	var _vendorBbAngularNgAria = __webpack_require__(18);
	
	var _vendorBbAngularNgAria2 = _interopRequireDefault(_vendorBbAngularNgAria);
	
	var _uiBbNumberInputNg = __webpack_require__(19);
	
	var _uiBbNumberInputNg2 = _interopRequireDefault(_uiBbNumberInputNg);
	
	var _helpers = __webpack_require__(20);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	var _hooks = __webpack_require__(22);
	
	var extHooks = _interopRequireWildcard(_hooks);
	
	__webpack_require__(23);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var dependencyKeys = exports.dependencyKeys = [_uiBbAccountCard2.default, _uiBbAccountSelector2.default, _uiBbConfirmNg2.default, _uiBbDropdownSelect2.default, _uiBbSubstituteErrorNg2.default, _vendorBbUibAlert2.default, _vendorBbAngularNgMessages2.default, _uiBbExpandableNg2.default, _uiBbCurrencyInputNg2.default, _uiBbCalendarPopupNg2.default, _uiBbCreditSuggestNg2.default, _uiBbSwitcherNg2.default, _uiBbLoadingIndicatorNg2.default, _uiBbI18nNg2.default, _uiBbParentResponsivenessNg2.default, _vendorBbUibPopover2.default, _vendorBbAngularNgAria2.default, _uiBbNumberInputNg2.default]; /**
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @module ext-bb-payment-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @description
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Payment default extension.
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-account-card
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-account-selector
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-confirm-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-dropdown-select
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-substitute-error-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires vendor-bb-uib-alert
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires vendor-bb-angular-ng-messages
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-expandable-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-currency-input-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-calendar-popup-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-credit-suggest-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-switcher-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-loading-indicator-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-i18n-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires ui-bb-parent-responsiveness-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires vendor-bb-uib-popover
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @requires vendor-bb-angular-ng-aria
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * @example
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * <!-- payment widget model.xml -->
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * <property name="extension" viewHint="text-input,admin">
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *  <value type="string">ext-bb-payment-ng</value>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * </property>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Usage of ui-bb-account-card component in template
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * <ui-bb-account-card
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   account-name="$ctrl.payment.from.name"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   account-number="$ctrl.payment.from.identifier"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   amount="$ctrl.payment.from.amount"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   currency="$ctrl.payment.from.currency"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   show-avatar="true">
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * </ui-bb-account-card>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Usage of ui-bb-account-selector component in template
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * <ui-bb-account-selector
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   ng-model="$ctrl.payment.from"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   accounts="$ctrl.accountsFrom"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   ng-change="$ctrl.onAccountFromChange()"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   custom-template-id="ui-bb-account-selector/option-template.htm"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   required>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * </ui-bb-account-selector>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * where
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * accounts {array} List of accounts
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * ng-change {function} Method that will be executed when product selection is changed
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * custom-template-id {string} Id of the template that will be used for rendering
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * instead of default one
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Usage of ui-bb-currency-input-ng component in template
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * <ui-bb-currency-input-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   max-length="6"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   decimal-max-length="2"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   placeholder="0"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   ng-model="$ctrl.payment.amount"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   currencies="$ctrl.currencies">
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * </ui-bb-currency-input-ng>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * where
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * max-length {number} Maximum number of digits allowed in whole part
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * decimal-max-length {number} Maximum number of digits allowed in decimal part
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * placeholder {string} Text to display for input's placeholder
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * currencies {string[]} List of currencies available
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Usage of ui-bb-credit-suggest-ng component in template
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * <ui-bb-credit-suggest-ng
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   name="credit"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   ng-model="$ctrl.payment.to"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   accounts="$ctrl.accountsTo"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   get-accounts="ext.helpers.getAccounts(search, accounts)"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   messages="{
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *     noResults: ('ui-bb-credit-suggest-ng.filter.noResults' | i18n),
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   }"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   allow-external="$ctrl.payment.from.externalTransferAllowed"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   custom-template-id="ui-bb-credit-suggest-ng/template/option.html"
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *   required>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * </ui-bb-credit-suggest-ng>
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 *
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * where
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * accounts {array} List of accounts to filter and select with user input
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * get-accounts {function} External method for transform accounts array into custom structure
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * Can be defined in extensions helpers
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * messages {object} Localized messages
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * allow-external {boolean} Are external accounts included in list.
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * If not, IBAN field stays disabled
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * custom-template-id {string} Id of the template that will be used for rendering
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 * instead of default one
	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 */
	/* eslint-disable import/prefer-default-export */
	var helpers = exports.helpers = _helpers2.default;
	var hooks = exports.hooks = extHooks;

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_7__;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_9__;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_10__;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_11__;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_12__;

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_13__;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_14__;

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_16__;

/***/ }),
/* 17 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_17__;

/***/ }),
/* 18 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_18__;

/***/ }),
/* 19 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_19__;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _constants = __webpack_require__(21);
	
	var _constants2 = _interopRequireDefault(_constants);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var helpers = function helpers(context) {
	  var dateFilter = context.$filter('date');
	  var i18nFilter = context.$filter('i18n');
	  var lowercaseFilter = context.$filter('lowercase');
	  var expanded = {};
	  var groupItemsNumber = 4;
	  var groupNames = {
	    INTERNAL: i18nFilter('ui-bb-credit-suggest-ng.group.internal'),
	    EXTERNAL: i18nFilter('ui-bb-credit-suggest-ng.group.external')
	  };
	  var crossCurrencyMessages = { 'cross-currency': true };
	
	  var minOccurrences = 2;
	  var maxOccurences = 200;
	
	  var specialSymbols = "/-?:().,'+";
	  var escapedSpecialSymbols = specialSymbols.split('').map(function (a) {
	    return '|\\' + a;
	  }).join('');
	  var paymentReferencePattern = '^(\\w|\\d| ' + escapedSpecialSymbols + ')*$';
	
	  return {
	    minOccurrences: minOccurrences,
	    maxOccurences: maxOccurences,
	    paymentReferencePattern: paymentReferencePattern,
	
	    /**
	     * @description
	     * Extend default single payment transfer frequency with extension specific frequencies
	     *
	     * @name extendFrequencies
	     * @type {function}
	     * @param {object} Single transfer object from widget controller
	     * @param {object} Payment preferences obtained from widget
	     * @returns {array} Array with all frequencies available
	     */
	    extendFrequencies: function extendFrequencies(singleTransfer, paymentPreferences) {
	      return [singleTransfer].concat(paymentPreferences.recurring ? _constants2.default : []);
	    },
	
	    /**
	     * @description
	     * Compiles the scheduling description out of payment object params.
	     *
	     * In this process, following translation keys are being used:
	     *
	     * form.schedule.starting, for word "Starting"
	     *
	     * form.schedule.today, for word "Today"
	     *
	     * form.schedule.on, for word "On" (used before date to form "on 01.01.2017")
	     *
	     * form.schedule.until, for word "until" (used before date to form "until 01.01.2017")
	     *
	     * form.schedule.repeat.count, for word "times" (used after repeat count to form "5 times")
	     *
	     * and name of the transfer frequency set in constants file
	     *
	     * @name getScheduleText
	     * @type {function}
	     * @param {object} controller PaymentController
	     * @returns {string} Compiled text that can be used as scheduling value in views
	     */
	    getScheduleText: function getScheduleText(_ref) {
	      var payment = _ref.payment,
	          endingTypes = _ref.endingTypes,
	          singleTransfer = _ref.singleTransfer;
	
	      var words = [];
	      var now = new Date();
	      var multipleOccurrences = payment.schedule.transferFrequency.value !== singleTransfer.value;
	      var isToday = payment.schedule.startDate.setHours(0, 0, 0, 0) === now.setHours(0, 0, 0, 0);
	      var hasEnd = payment.endingType !== endingTypes.NEVER;
	
	      // add frequency
	      words.push(i18nFilter(payment.schedule.transferFrequency.name));
	      words.push('-');
	
	      // if there are multiple occurrences, add word starting
	      if (multipleOccurrences) {
	        words.push(i18nFilter('form.schedule.starting'));
	      }
	
	      // if start date is today use word instead of date
	      if (isToday) {
	        var todayString = i18nFilter('form.schedule.today');
	        // for multiple occurrences, there is a prefix word, so this on should be lowercase
	        if (multipleOccurrences) {
	          todayString = lowercaseFilter(todayString);
	        }
	
	        words.push(todayString);
	      }
	
	      // for single transfer in the future, we need prefix word on
	      if (!multipleOccurrences && !isToday) {
	        words.push(lowercaseFilter(i18nFilter('form.schedule.on')));
	      }
	
	      if (!isToday) {
	        words.push(dateFilter(payment.schedule.startDate));
	      }
	
	      // if there are limited number of occurrences,
	      // add comma for the last word, so that there is no space between them
	      if (hasEnd && multipleOccurrences && !(payment.endingType === endingTypes.AFTER && (!payment.schedule.repeat || payment.schedule.repeat < minOccurrences))) {
	        words[words.length - 1] += ',';
	      }
	
	      if (hasEnd && multipleOccurrences) {
	        // there is an end for this schedule
	        // based on the type of ending construct words differently
	        if (payment.endingType === endingTypes.ON) {
	          words.push(i18nFilter('form.schedule.until'));
	          words.push(dateFilter(payment.schedule.endDate));
	        } else if (payment.schedule.repeat >= minOccurrences) {
	          words.push(payment.schedule.repeat);
	          words.push(payment.schedule.repeat ? i18nFilter('form.schedule.repeat.count') : '');
	        }
	      }
	
	      // output everything together
	      return words.join(' ');
	    },
	
	    /**
	     * @description
	     * Toggle group in credit suggest component
	     *
	     * @name toggle
	     * @type {function}
	     * @param {object} event
	     * @param {object} model
	     * @param {object} controller
	     */
	    toggle: function toggle(event, model, _ref2) {
	      var open = _ref2.open;
	
	      // Prevent suggestion list to be closed
	      event.stopPropagation();
	
	      expanded[model.more || model.less] = !!model.more;
	
	      open();
	    },
	
	    /**
	     * @description
	     * Transforms accounts list to custom structure
	     *
	     * @name getAccounts
	     * @type {function}
	     * @param {string} search
	     * @param {array<object>} accounts
	     * @returns {array<object>} viewAccounts
	     */
	    getAccounts: function getAccounts(search, accounts) {
	      var _accountsGroups;
	
	      if (search || !accounts) {
	        return [].concat(accounts || []);
	      }
	
	      var accountsGroups = (_accountsGroups = {}, _defineProperty(_accountsGroups, groupNames.INTERNAL, accounts.filter(function (account) {
	        return !account.external;
	      })), _defineProperty(_accountsGroups, groupNames.EXTERNAL, accounts.filter(function (account) {
	        return account.external;
	      })), _accountsGroups);
	
	      if (accountsGroups[groupNames.EXTERNAL].length === 0) {
	        return [].concat(accounts || []);
	      }
	
	      // add group names
	      var groups = Object.keys(accountsGroups);
	      groups.forEach(function (groupName) {
	        var group = accountsGroups[groupName];
	        var firstAccount = group[0];
	        if (firstAccount) {
	          group[0] = Object.assign({
	            group: groupName
	          }, firstAccount);
	        }
	      });
	
	      // add toggle to internal group
	      var internalGroup = accountsGroups[groupNames.INTERNAL];
	      var viewGroup = void 0;
	      if (expanded[groupNames.INTERNAL]) {
	        viewGroup = internalGroup;
	        var lastAccount = viewGroup[viewGroup.length - 1];
	        if (lastAccount) {
	          viewGroup[viewGroup.length - 1] = Object.assign({
	            less: groupNames.INTERNAL
	          }, lastAccount);
	        }
	      } else {
	        viewGroup = internalGroup.slice(0, groupItemsNumber);
	        if (internalGroup.length > groupItemsNumber) {
	          var _lastAccount = viewGroup[viewGroup.length - 1];
	          if (_lastAccount) {
	            viewGroup[viewGroup.length - 1] = Object.assign({
	              more: groupNames.INTERNAL
	            }, _lastAccount);
	          }
	        }
	      }
	
	      return [].concat(viewGroup, accountsGroups[groupNames.EXTERNAL]);
	    },
	
	    /**
	     * @description
	     * If cross currency message should be shown
	     *
	     * @name showCrossCurrencyMessage
	     * @type {function}
	     * @param {object} controller
	     * @returns {object} messages
	     */
	    showCrossCurrencyMessage: function showCrossCurrencyMessage($ctrl) {
	      return $ctrl.paymentPreferences.showExchangeRate && $ctrl.rate && $ctrl.payment.from && parseFloat($ctrl.payment.amount.value) && $ctrl.payment.amount.currency !== $ctrl.payment.from.currency ? crossCurrencyMessages : null;
	    },
	
	    /**
	     * @description
	     * Makes new payment request and changes step on success
	     *
	     * @name makePayment
	     * @type {function}
	     * @param {object} $ctrl Payment controller
	     * @param {object} $parent Parent ng scope
	     * @returns {object} Payment request Promise
	     */
	    makePayment: function makePayment($ctrl, $parent) {
	      return $ctrl.makePayment($ctrl.payment).then(function () {
	        // eslint-disable-next-line no-param-reassign
	        $parent.step = 'confirmation';
	      });
	    }
	  };
	};
	
	exports.default = helpers;

/***/ }),
/* 21 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @description
	 * Array of recurring frequency objects with the following properties (all mandatory)
	 *
	 * @property {string} object.name Translation key of the label
	 * that will be displayed to the end user
	 * @property {string} object.value Denotes frequency type of transfer.
	 * Possible values: DAILY/WEEKLY/MONTHLY/YEARLY
	 * @property {number} object.every Indicates skip interval of transfer.
	 * 1 would mean execute every time, 2 - every other time
	 *
	 * @example
	 * {
	 *   name: 'form.schedule.frequency.weekly',
	 *   value: 'WEEKLY',
	 *   every: 1,
	 * },
	 * {
	 *   name: 'form.schedule.frequency.bi.weekly',
	 *   value: 'WEEKLY',
	 *   every: 2,
	 * }
	 *
	 * @name transferFrequencies
	 * @type {array}
	 */
	var transferFrequencies = [{
	  name: 'form.schedule.frequency.daily',
	  value: 'DAILY',
	  every: 1
	}, {
	  name: 'form.schedule.frequency.weekly',
	  value: 'WEEKLY',
	  every: 1
	}, {
	  name: 'form.schedule.frequency.monthly',
	  value: 'MONTHLY',
	  every: 1
	}, {
	  name: 'form.schedule.frequency.quarterly',
	  value: 'MONTHLY',
	  every: 3
	}, {
	  name: 'form.schedule.frequency.annually',
	  value: 'YEARLY',
	  every: 1
	}];
	
	exports.default = transferFrequencies;

/***/ }),
/* 22 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.processAccountsTo = processAccountsTo;
	exports.getRecurringTransactionDay = getRecurringTransactionDay;
	/**
	 * @name processAccountsTo
	 * @description
	 * Hook for processing credit account list.
	 *
	 * @type {function}
	 * @param debitAccount {ProductKind} Selected debit account (can be null)
	 * @param getCreditAccounts {function} Function to retrieve all credit accounts
	 * @param getExternalContacts {function} Function to retrieve all external contacts
	 * formatted like Product kind
	 * @returns {Promise.<any[]>} Promise that retrieves array of accounts.
	 */
	function processAccountsTo(debitAccount, getCreditAccounts, getExternalAccounts) {
	  return getCreditAccounts(debitAccount.id || null).then(function (accounts) {
	    if (!debitAccount.id || debitAccount.externalTransferAllowed) {
	      return getExternalAccounts().then(function (contacts) {
	        return accounts.concat(contacts);
	      });
	    }
	    return accounts;
	  });
	}
	
	/**
	 * @name getRecurringTransactionDay
	 * @type {function}
	 *
	 * @description
	 * Denotes day on which transfer should be executed.
	 *
	 * For weekly it will be 1..7 indicating weekday.
	 *
	 * For monthly it will be 1..31 indicating day of month.
	 *
	 * For yearly it will be 1..12 indicating month of the year
	 *
	 * Assigned to [$ctrl.schedule.on]{@link PaymentController#makePayment}
	 *
	 * @param startDate {Date} Start date of recurring payment
	 * @param transferFrequency {object} Recurring frequency
	 * @returns {number}
	 */
	function getRecurringTransactionDay(startDate, transferFrequency) {
	  var transactionDay = void 0;
	  switch (transferFrequency.value) {
	    case 'YEARLY':
	      transactionDay = startDate.getMonth() + 1;
	      break;
	    case 'MONTHLY':
	      transactionDay = startDate.getDate();
	      break;
	    case 'WEEKLY':
	    default:
	      transactionDay = startDate.getDay() + 1;
	      break;
	  }
	
	  return transactionDay;
	}

/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(24);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(26)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!./index.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!./index.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(25)();
	// imports
	
	
	// module
	exports.push([module.id, ".ext-bb-payment-ng textarea {\n  resize: none;\n}\n\n.ext-bb-payment-ng .amount-decimals,\n.ext-bb-payment-ng .amount-decimal-points {\n  display: inline-block;\n}\n\n.ext-bb-payment-ng .occurence-field {\n  display: inline;\n}\n\n.ext-bb-payment-ng .cross-currency-info > span {\n  display: inline-block;\n}", ""]);
	
	// exports


/***/ }),
/* 25 */
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ })
/******/ ])
});
;
//# sourceMappingURL=ext-bb-payment-ng.js.map