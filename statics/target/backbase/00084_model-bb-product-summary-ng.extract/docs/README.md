# model-bb-product-summary-ng


Version: **1.4.26**

Product summary widget model.

## Imports

* data-bb-arrangements-http-ng
* data-bb-product-summary-http-ng
* lib-bb-model-errors
* lib-bb-storage-ng
* lib-bb-widget-ng
* vendor-bb-angular

---

## Example

```javascript
import modelProductSummaryModuleKey, {
  modelProductSummaryKey,
} from 'model-bb-product-summary-ng';

angular.module('widget-bb-product-summary-ng', [
  modelProductSummaryModuleKey,
])
.controller('ProductSummaryController', [
  modelProductSummaryKey,
  ...,
])
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>    <a href="#modelProductSummaryKey">modelProductSummaryKey</a><br/>
- **ProductSummaryModel**<br/>    <a href="#ProductSummaryModel#load">#load(forceLoad)</a><br/>    <a href="#ProductSummaryModel#getProductSelected">#getProductSelected()</a><br/>    <a href="#ProductSummaryModel#setProductSelected">#setProductSelected(product)</a><br/>    <a href="#ProductSummaryModel#loadByLegalEntityId">#loadByLegalEntityId(params)</a><br/>    <a href="#ProductSummaryModel#getProductDetails">#getProductDetails(productId)</a><br/>    <a href="#ProductSummaryModel#getAccountsOverviewPreferences">#getAccountsOverviewPreferences()</a><br/>
- **Type Definitions**<br/>    <a href="#ProductKinds">ProductKinds</a><br/>    <a href="#ProductKind">ProductKind</a><br/>    <a href="#Products">Products</a><br/>    <a href="#Product">Product</a><br/>    <a href="#TotalBalance">TotalBalance</a><br/>    <a href="#ProductDetails">ProductDetails</a><br/>    <a href="#DebitCard">DebitCard</a><br/>

## Exports

### <a name="default"></a>*default*

Angular module name

**Type:** *String*

### <a name="modelProductSummaryKey"></a>*modelProductSummaryKey*

Injector name of the model service

**Type:** *String*


---

## ProductSummaryModel

Product Summary model service

### <a name="ProductSummaryModel#load"></a>*#load(forceLoad)*

Load Product Summary data.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| forceLoad | Object | True if should always load data from server |

##### Returns

Promise of <a href="#ProductKinds">ProductKinds</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with ProductsKinds and TotalBalance.*

### <a name="ProductSummaryModel#getProductSelected"></a>*#getProductSelected()*

Temporary. Get current selected product.

##### Returns

Promise of <a href="#Product">Product</a> - *A Promise resolving to Product.*

### <a name="ProductSummaryModel#setProductSelected"></a>*#setProductSelected(product)*

Set current selected product

| Parameter | Type | Description |
| :-- | :-- | :-- |
| product | <a href="#Product">Product</a> | A product to select |

### <a name="ProductSummaryModel#loadByLegalEntityId"></a>*#loadByLegalEntityId(params)*

Load some data.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | optional configuration object |

##### Returns

Promise of <a href="#Products">Products</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with Accounts by legalEntityId.*

### <a name="ProductSummaryModel#getProductDetails"></a>*#getProductDetails(productId)*

Get all the details of a product.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| productId | String | Id of the requested product |

##### Returns

Promise of <a href="#ProductDetails">ProductDetails</a>, <a href="#ModelError">ModelError</a> - *A Promise resolving to object with Account details.*

### <a name="ProductSummaryModel#getAccountsOverviewPreferences"></a>*#getAccountsOverviewPreferences()*

Getting accounts preferences from widget

##### Returns

Object - *Preferences object*

## Type Definitions


### <a name="ProductKinds"></a>*ProductKinds*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| total | <a href="#TotalBalance">TotalBalance</a> | total balance of products |
| productKinds | Array of <a href="#ProductKind">ProductKind</a> | array of Products Kinds |

### <a name="ProductKind"></a>*ProductKind*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the ProductKind (currentAccounts, savingsAccounts, termDeposits, creditCards, debitCards, loans, investmentAccounts) |
| name | <a href="#!string">!string</a> | name of the ProductKind |
| aggregatedBalance | String | aggregated balance |
| currency | String | currency code |
| products | Array of <a href="#Product">Product</a> | array of Products |

### <a name="Products"></a>*Products*

ProductKind type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| totalCount | Number | total number of products |
| products | Array of <a href="#Product">Product</a> | array of Products |

### <a name="Product"></a>*Product*

Product type definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | <a href="#!string">!string</a> | id of the Product |
| name | <a href="#!string">!string</a> | name of the Product |
| kind | <a href="#!string">!string</a> | id of the ProductKind |
| alias | String | alias of the Product |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | currency code |
| PANSuffix | String | Primary Account Number Suffix |
| bookedBalance | String | booked balance |
| availableBalance | String | available balance |
| creditLimit | String | credit limit |
| currentInvestmentValue | String | current investment value |
| principalAmount | String | principal amount |
| accruedInterest | String | accrued interest |

### <a name="TotalBalance"></a>*TotalBalance*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | String | aggregated balance |
| currency | String | currency code |

### <a name="ProductDetails"></a>*ProductDetails*

Product details definition

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| externalArrangementId | <a href="#!string">!string</a> | id of the external arrangement |
| externalLegalEntityId | <a href="#!string">!string</a> | id of the external legal entity |
| externalProductId | <a href="#!string">!string</a> | id of the external product |
| name | String | name of the account |
| alias | String | alias of the account |
| bookedBalance | Number | booked balance |
| availableBalance | Number | available balance |
| creditLimit | Number | credit limit |
| IBAN | String | International Bank Account Number |
| BBAN | String | Basic Bank Account Number |
| currency | String | 3 characteres currency code |
| externalTransferAllowed | Boolean | is external transfer allowed |
| urgentTransferAllowed | Boolean | is urgent transfer allowed |
| accruedInterest | String | accrued interest |
| number | String | number |
| principalAmount | Number | principal amount |
| currentInvestmentValue | Number | current investment value |
| legalEntityId | <a href="#!string">!string</a> | id of the legal entity |
| productId | <a href="#!string">!string</a> | id of the product |
| productNumber | String | number of the product |
| accountOpeningDate | String | date when account was opened |
| accountInterestRate | Number | account interest rate |
| valueDateBalance | Number | value date balance |
| overdraftAmount | Number | overdraft amount |
| overdraftInterestRate | Number | overdraft interest rate |
| overdraftExpiryDate | Number | overdraft expiry date |
| overdraftLimit | Number | overdraft limit |
| bankBranchCode | String | bank branch code |
| startDate | Date | start date |
| term | String | term |
| maturityDate | Date | maturity date |
| maturityAmount | Number | maturity amount |
| autoRenevalIndicator | Boolean | is auto renewal enabled |
| interestPaymentFrequency | String | interest payment frequency |
| interestSettlementAccount | String | interest settlement account |
| outstandingPrincipal | Number | outstanding principal |
| monthlyInstalmentAmount | Number | monthly instalment amount |
| minimumRequiredBalance | Number | minimum required balance |
| creditCardAccountNumber | String | credit card account number |
| validThru | Date | credit card validity through date |
| applicableInterestRate | Number | applicable interest rate |
| remainingCredit | Number | remaining credit |
| outstandingPayment | Number | outstanding payment |
| minimunPayment | Number | minimum payment |
| minimunPaymentDueDate | Date | minimum payment due date |
| totalInvestmentValue | Number | total investment value |
| debitCard | Array of <a href="#DebitCard">DebitCard</a> | debit card collection |

### <a name="DebitCard"></a>*DebitCard*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| number | String | debit card number |
| expiryDate | Date | debit card expiry date |

---
