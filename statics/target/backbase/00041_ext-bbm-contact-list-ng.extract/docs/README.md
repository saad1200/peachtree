# ext-bbm-contact-list-ng


Version: **1.0.123**

Mobile extension for a contact list in the Contacts widget.

## Imports

* ui-bb-avatar-ng
* ui-bb-i18n-ng
* ui-bb-inline-status-ng
* ui-bbm-list-ng

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-contact-list-ng</value>
</property>
```

## Table of Contents
- **Hooks**<br/>    <a href="#Hooks#processContacts">#processContacts(contacts)</a><br/>
- **ext-bbm-contact-list-ng**<br/>    <a href="#ext-bbm-contact-list-ngselectPrevContact">selectPrevContact(contacts, index, contact)</a><br/>

---

## Hooks

Hooks for widget-bb-contact-ng

### <a name="Hooks#processContacts"></a>*#processContacts(contacts)*

Hook for processing the list of contacts

| Parameter | Type | Description |
| :-- | :-- | :-- |
| contacts | <a href="#array<object>">array<object></a> | Original list of contacts |

##### Returns

<a href="#array<object>">array<object></a> - *Processed the list of contacts*

---

### <a name="ext-bbm-contact-list-ngselectPrevContact"></a>*selectPrevContact(contacts, index, contact)*

Returns previous (or the first) contact based
on the currently selected item id or item index (deprecated).

| Parameter | Type | Description |
| :-- | :-- | :-- |
| contacts | <a href="#*">*</a> | Processed contacts |
| index | Number | Currently selected contact index (deprecated) |
| contact | Object | Currently selected contact |

##### Returns

Object - *Previous or the first contact from the contacts*
