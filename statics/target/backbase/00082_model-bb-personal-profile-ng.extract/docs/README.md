# model-bb-personal-profile-ng


Version: **1.0.20**

Personal profile widget model.

## Imports

* data-bb-user-http-ng
* lib-bb-model-errors
* vendor-bb-angular

---

## Example

```javascript
import modelPersonalProfileModuleKey, {
  modelPersonalProfileKey,
} from 'model-bb-personal-profile-ng';

angular.module('widget-bb-payment-ng', [
  modelPersonalProfileModuleKey,
])
.controller('PersonalProfileController', [
  modelPersonalProfileKey,
  ...,
])
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **PersonalProfileModel**<br/>    <a href="#PersonalProfileModel#load">#load()</a><br/>
- **Type Definitions**<br/>    <a href="#User">User</a><br/>

## Exports

### <a name="default"></a>*default*

Personal Profile Model

**Type:** *String*


---

## PersonalProfileModel

Personal Profile model service

### <a name="PersonalProfileModel#load"></a>*#load()*

Loads the data for the current logged in user

##### Returns

<a href="#Promise<User">Promise<User</a>, <a href="#ModelError>">ModelError></a> - *A Promise with the user's data.*

## Type Definitions


### <a name="User"></a>*User*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| bbid | String | Internal Backbase identifier |
| exid | String | External bank identifier |
| entityId | String | External entity identifier |
| id | String | Internally used unique identification of the user |
| imageAvatar | String | Base64 encoded picture of the user |
| firstName | String | The given name of a user |
| lastName | String | The given family name of a user |
| dateOfBirth | String | The date the user was born in format "DD-MM-YYYY" |
| street | String | Street name (part of the address) |
| houseNumber | String | House number (part of the address) |
| postalCode | String | Postal code (part of the address) |
| area | String | Area (part of the address) |
| city | String | City (part of the address) |
| citizenship | String | Country where the user is citizen of |
| email | String | The primary e-mail address of the user |
| phone | Array | The phone numbers where the user can be reached |

---
