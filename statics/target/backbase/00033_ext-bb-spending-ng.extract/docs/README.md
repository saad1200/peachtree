# ext-bb-spending-ng


Version: **1.0.28**

Default extension for widget-bb-spending-ng

## Imports

* lib-bb-styles
* ui-bb-change-period-ng
* ui-bb-chartjs-chart-donut-ng
* ui-bb-empty-state-ng
* ui-bb-format-amount
* ui-bb-i18n-ng
* ui-bb-substitute-error-ng
* vendor-bb-angular-ng-aria

---

## Table of Contents
- **ext-bb-spending-ng**<br/>    <a href="#ext-bb-spending-ngDEFAULT_TOOLTIP_SELECTOR">DEFAULT_TOOLTIP_SELECTOR</a><br/>    <a href="#ext-bb-spending-ngARROW_INNER_CSS_SELECTOR">ARROW_INNER_CSS_SELECTOR</a><br/>    <a href="#ext-bb-spending-ngARROW_OUTER_CSS_SELECTOR">ARROW_OUTER_CSS_SELECTOR</a><br/>    <a href="#ext-bb-spending-ngCHART_SECTOR_SHIFT_SIZE">CHART_SECTOR_SHIFT_SIZE</a><br/>    <a href="#ext-bb-spending-ngMS_IN_MIN">MS_IN_MIN</a><br/>    <a href="#ext-bb-spending-ngRENDERING_ANIMATION_TIME">RENDERING_ANIMATION_TIME</a><br/>    <a href="#ext-bb-spending-ngonDonutChartSectorClickAnimation">onDonutChartSectorClickAnimation(chartInstance, event)</a><br/>    <a href="#ext-bb-spending-ngperiodLabelFormatter">periodLabelFormatter(date)</a><br/>    <a href="#ext-bb-spending-ngcustomizeTooltip">customizeTooltip(tooltip, element, data, chart)</a><br/>    <a href="#ext-bb-spending-ngchartPlugins">chartPlugins</a><br/>    <a href="#ext-bb-spending-ngonPeriodStartChange">onPeriodStartChange(ctrl)</a><br/>    <a href="#ext-bb-spending-ngonPeriodEndChange">onPeriodEndChange(ctrl)</a><br/>    <a href="#ext-bb-spending-ngisSeriesDataAvailable">isSeriesDataAvailable(series)</a><br/>    <a href="#ext-bb-spending-nggetClickHandler">getClickHandler($ctrl)</a><br/>    <a href="#ext-bb-spending-ngprocessSpendingSeries">processSpendingSeries(series, data)</a><br/>    <a href="#ext-bb-spending-ngprocessLoadError">processLoadError(The)</a><br/>
- **Type Definitions**<br/>    <a href="#Position">Position</a><br/>    <a href="#Spending">Spending</a><br/>    <a href="#SpendingItem">SpendingItem</a><br/>    <a href="#ExtendedSpendingItem">ExtendedSpendingItem</a><br/>    <a href="#BBSeries">BBSeries</a><br/>    <a href="#SpendingsBBSeries">SpendingsBBSeries</a><br/>    <a href="#Dataset">Dataset</a><br/>    <a href="#SpendingsDataset">SpendingsDataset</a><br/>

---
### <a name="ext-bb-spending-ngDEFAULT_TOOLTIP_SELECTOR"></a>*DEFAULT_TOOLTIP_SELECTOR*

Chart's tooltip CSS selector

**Type:** *String*


---
### <a name="ext-bb-spending-ngARROW_INNER_CSS_SELECTOR"></a>*ARROW_INNER_CSS_SELECTOR*

Chart tooltip's arrow CSS selector (inner layer)

**Type:** *String*


---
### <a name="ext-bb-spending-ngARROW_OUTER_CSS_SELECTOR"></a>*ARROW_OUTER_CSS_SELECTOR*

Chart tooltip's arrow CSS selector (outer layer)

**Type:** *String*


---

## CHART_SLICE_LABEL

Slice labels configuration

| Property | Type | Description |
| :-- | :-- | :-- |
| dataAttr | String | Data attribute that will be attached to the wrapper that contains the label |
| offset | Number | Icon's offset from the outer edge of the chart |
| minimum | Number | Minimum value of portion required to create a label |

---
### <a name="ext-bb-spending-ngCHART_SECTOR_SHIFT_SIZE"></a>*CHART_SECTOR_SHIFT_SIZE*

Slice shifting size (pixels) and size of the canvas padding (which is
needed to provide a space for the shifting animation). Is also
used for icons offset calculation.

**Type:** *Number*


---
### <a name="ext-bb-spending-ngMS_IN_MIN"></a>*MS_IN_MIN*

Amount of milliseconds in a minute

**Type:** *Number*


---
### <a name="ext-bb-spending-ngRENDERING_ANIMATION_TIME"></a>*RENDERING_ANIMATION_TIME*

Time in milliseconds to have a donut slice animation on click

**Type:** *Number*


---

### <a name="ext-bb-spending-ngonDonutChartSectorClickAnimation"></a>*onDonutChartSectorClickAnimation(chartInstance, event)*

onClick handler with a visualisation for donut sector

| Parameter | Type | Description |
| :-- | :-- | :-- |
| chartInstance | <a href="#*">*</a> | the Chart.js object |
| event | <a href="#*">*</a> | Chart.js click event object with a clicked area |

##### Returns

Object - *name and isSelected flag of the active sector (category)*

---

### <a name="ext-bb-spending-ngperiodLabelFormatter"></a>*periodLabelFormatter(date)*

Period label formatter helper. In case period is larger than current date
(end date of current month) it returns translation for spending.label.period.now,
otherwise it returns formatted date

| Parameter | Type | Description |
| :-- | :-- | :-- |
| date | Date |  |

##### Returns

String - *Formatted label*

---

### <a name="ext-bb-spending-ngcustomizeTooltip"></a>*customizeTooltip(tooltip, element, data, chart)*

Creates custom tooltip content and places tooltip element on the edge
of currently active portion

| Parameter | Type | Description |
| :-- | :-- | :-- |
| tooltip | Object | object containing tooltip related data like positions, current data point, styling from chart options, etc. |
| element | Object | in DOM |
| data | <a href="#SpendingsBBSeries">SpendingsBBSeries</a> | array of data used to draw the chart |
| chart | Object | instance |

---

## donutChartOptions

Chart.js options object to be used by component.
Has higher priority, can be used to override default options
values in a chart.

---
### <a name="ext-bb-spending-ngchartPlugins"></a>*chartPlugins*

Array of plugins used to transform Chart.js rendering in the extension

**Type:** *Array*


---

### <a name="ext-bb-spending-ngonPeriodStartChange"></a>*onPeriodStartChange(ctrl)*

Callback on period start date value change. This function ensures
that controller property is also updated

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#SpendingController">SpendingController</a> |  |

---

### <a name="ext-bb-spending-ngonPeriodEndChange"></a>*onPeriodEndChange(ctrl)*

Callback on period value change. Calls
controller's onPeriodEndChanged listener

| Parameter | Type | Description |
| :-- | :-- | :-- |
| ctrl | <a href="#SpendingController">SpendingController</a> |  |

---

### <a name="ext-bb-spending-ngisSeriesDataAvailable"></a>*isSeriesDataAvailable(series)*

Checks if data for chart is available in series object

| Parameter | Type | Description |
| :-- | :-- | :-- |
| series | <a href="#SpendingsBBSeries">SpendingsBBSeries</a> |  |

##### Returns

Boolean - *returns true if series data is not empty*

---

### <a name="ext-bb-spending-nggetClickHandler"></a>*getClickHandler($ctrl)*

Function to create new handler for the chart click handling

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | spendings controller |

##### Returns

Function - *chart click handler*

---

### <a name="ext-bb-spending-ngprocessSpendingSeries"></a>*processSpendingSeries(series, data)*

Hook for spending chart series object post processing

| Parameter | Type | Description |
| :-- | :-- | :-- |
| series | <a href="#BBSeries">BBSeries</a> | chart series data |
| data | <a href="#Spending">Spending</a> | original spending object |

##### Returns

<a href="#SpendingsBBSeries">SpendingsBBSeries</a> - *processed series*

---

### <a name="ext-bb-spending-ngprocessLoadError"></a>*processLoadError(The)*

Overwrite the default hook and don't return passed error

| Parameter | Type | Description |
| :-- | :-- | :-- |
| The | Error | error passed |

##### Returns

String - *The actual error*

## Type Definitions


### <a name="Position"></a>*Position*

Element absolute position object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| left | Number | Distance from left edge of the area |
| top | Number | Distance from the top of area |

### <a name="Spending"></a>*Spending*

Spending response object

**Type:** *Array*


| Property | Type | Description |
| :-- | :-- | :-- |
| items | Array of <a href="#SpendingItem">SpendingItem</a> | Array of spending items |

### <a name="SpendingItem"></a>*SpendingItem*

Spending response item

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| category | String | Transactions category |
| totalAmount | Object | The total amount of the aggregated transactions by category |
| totalAmount.currencyCode | String | Total amount currency code (ISO) |
| totalAmount.amount | Number | Total amount value |
| trend | Number | Percentage value of the trend |
| portion | Number | Percentage of the total spending for a given period |

### <a name="ExtendedSpendingItem"></a>*ExtendedSpendingItem*

Extended spending response item

*Extends*: <a href="#SpendingItem">SpendingItem</a>

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| totalPortion | Number | Percentage of total spending for a given period and all periods larger than that period |

### <a name="BBSeries"></a>*BBSeries*

BBSeries data object used to draw charts

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| labels | Array of String | Array of chart labels |
| datasets | Array of <a href="#Dataset">Dataset</a> | Array of all datasets |

### <a name="SpendingsBBSeries"></a>*SpendingsBBSeries*

Spendings specific BBSeries object

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| labels | Array of String | Array of chart labels |
| datasets | Array of <a href="#SpendingsDataset">SpendingsDataset</a> | Array of all datasets |
| spendings | Array of <a href="#ExtendedSpendingItem">ExtendedSpendingItem</a> | Extended spending array |

### <a name="Dataset"></a>*Dataset*

Dataset object for chart data

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Array of Number | Array of data points to be drawn for each label |

### <a name="SpendingsDataset"></a>*SpendingsDataset*

Spendings specific dataset object for chart

*Extends*: <a href="#Dataset">Dataset</a>

**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| backgroundColor | String | Background color for chart slices |
| hoverBackgroundColor | String | Hover background color for chart slices |
| borderWidth | Number | Border size between slices |

---
