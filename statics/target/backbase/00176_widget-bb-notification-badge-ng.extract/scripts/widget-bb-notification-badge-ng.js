(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-event-bus-ng"), require("lib-bb-model-errors"), require("model-bb-notifications-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-notification-badge-ng", ["vendor-bb-angular", "lib-bb-event-bus-ng", "lib-bb-model-errors", "model-bb-notifications-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-notification-badge-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-event-bus-ng"), require("lib-bb-model-errors"), require("model-bb-notifications-ng"));
	else
		root["widget-bb-notification-badge-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-event-bus-ng"], root["lib-bb-model-errors"], root["model-bb-notifications-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_34__, __WEBPACK_EXTERNAL_MODULE_45__, __WEBPACK_EXTERNAL_MODULE_47__, __WEBPACK_EXTERNAL_MODULE_57__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(56);

/***/ }),

/***/ 34:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_34__;

/***/ }),

/***/ 45:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_45__;

/***/ }),

/***/ 47:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_47__;

/***/ }),

/***/ 56:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(34);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _modelBbNotificationsNg = __webpack_require__(57);
	
	var _modelBbNotificationsNg2 = _interopRequireDefault(_modelBbNotificationsNg);
	
	var _libBbEventBusNg = __webpack_require__(45);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _controller = __webpack_require__(58);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module widget-bb-notification-badge-ng
	 *
	 * @description
	 * Notifications badge widget.
	 *
	 * @example
	 * <div ng-controller="NotificationsBadgeController as $ctrl">
	 *  <span>{{$ctrl.numberOfUnread}}</span>
	 * </div>
	 */
	exports.default = _vendorBbAngular2.default.module('widget-bb-notification-badge-ng', [_modelBbNotificationsNg2.default, _libBbEventBusNg2.default]).controller('NotificationsBadgeController', [_modelBbNotificationsNg.modelNotificationsKey, _libBbEventBusNg.eventBusKey, _controller2.default]).name;

/***/ }),

/***/ 57:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_57__;

/***/ }),

/***/ 58:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _errorMessages;
	
	exports.default = NotificationsBadgeController;
	
	var _libBbModelErrors = __webpack_require__(47);
	
	var _modelBbNotificationsNg = __webpack_require__(57);
	
	var _constants = __webpack_require__(59);
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var errorMessages = (_errorMessages = {}, _defineProperty(_errorMessages, _libBbModelErrors.E_AUTH, _constants.MessageKey.ERROR_AUTH), _defineProperty(_errorMessages, _libBbModelErrors.E_CONNECTIVITY, _constants.MessageKey.ERROR_CONNECTION), _defineProperty(_errorMessages, _libBbModelErrors.E_UNEXPECTED, _constants.MessageKey.ERROR_UNEXPECTED), _defineProperty(_errorMessages, _libBbModelErrors.E_USER, _constants.MessageKey.ERROR_USER), _errorMessages);
	
	var uiError = function uiError(messageMap, modelError) {
	  return {
	    message: messageMap[modelError.code]
	  };
	};
	
	/**
	 * Defines the default page size for the notifications list in popover
	 * as defined in the widget model.xml
	 * @type {int}
	 */
	var DEFAULT_PAGE_SIZE = 15;
	
	/**
	 * Defines widget page enumeration
	 * @name Page
	 * @inner
	 * @enum {string}
	 * @type {object}
	 */
	var Page = {
	  DETAILS: 'details',
	  LIST: 'list'
	};
	
	function NotificationsBadgeController(model, eventBus) {
	  /**
	   * @name NotificationsBadgeController
	   * @ngkey NotificationsBadgeController
	   * @type {Object}
	   * @description
	   * Notifications badge controller.
	   */
	  var $ctrl = this;
	
	  var preferences = model.getNotificationPreferences();
	
	  /**
	   * @name pageSize
	   * @type {Number} pageSize
	   * @description
	   * Number of records to return per request
	   *
	   * The itemsPerPage property is deprecated in favor of pageSize
	   */
	  var pageSize = preferences.itemsPerPage || preferences.pageSize || DEFAULT_PAGE_SIZE;
	  var pollingInterval = preferences.pollingInterval;
	
	  var notifications = {
	    params: {
	      cursor: null,
	      size: pageSize
	    }
	  };
	
	  var pollingRef = null;
	
	  /**
	   * @description
	   * Holds current controller state
	   *
	   * @name NotificationsBadgeController#state
	   *
	   * @type {object}
	   */
	  var state = {
	    page: Page.LIST,
	    badge: {
	      numberOfUnread: 0,
	      showUnreadCount: preferences.badgeCounter,
	      loading: false
	    },
	    popover: {
	      isOpen: false,
	      loading: false,
	      error: null
	    },
	    notifications: {
	      active: null,
	      data: [],
	      loading: false,
	      get hasMore() {
	        return !!notifications.params.cursor;
	      },
	      error: null
	    }
	  };
	
	  /**
	   * @description
	   * Funtcion that performs before load stream request
	   *
	   * @name beforeLoadStream
	   *
	   * @inner
	   * @type {function}
	   */
	  var beforeLoadUnreadCount = function beforeLoadUnreadCount() {
	    state.badge.loading = true;
	  };
	
	  /**
	   * @description
	   * Funtcion that performs before load stream request
	   *
	   * @name beforeLoadStream
	   *
	   * @inner
	   * @type {function}
	   */
	  var afterLoadUnreadCount = function afterLoadUnreadCount() {
	    state.badge.loading = false;
	  };
	
	  /**
	   * @description
	   * Function that performs when unread count iteration was successful
	   *
	   * @name onLoadUnreadCountSuccess
	   *
	   * @inner
	   * @type {function}
	   * @param {module:model-bb-notifications-ng.UnreadCount} raw Unread count response object
	   */
	  var onLoadUnreadCountSuccess = function onLoadUnreadCountSuccess(raw) {
	    state.badge.numberOfUnread = raw.data.unread || 0;
	
	    pollingRef = raw.ref;
	  };
	
	  /**
	   * @description
	   * Load count of unread notifications.
	   *
	   * @name NotificationsBadgeController#loadUnreadCount
	   *
	   * @inner
	   * @type {function}
	   * @returns {Promise.<module:model-bb-notifications-ng.UnreadCount, ModelError>}
	   * A Promise with UnreadCount
	   */
	  var loadUnreadCount = function loadUnreadCount() {
	    beforeLoadUnreadCount();
	
	    return model.loadUnreadCount().then(onLoadUnreadCountSuccess).then(afterLoadUnreadCount);
	  };
	
	  /**
	   * Merges new notifications with existing notifications
	   *
	   * @inner
	   * @name NotificationsBadgeController#append
	   * @type {function}
	   * @returns {module:model-bb-notifications-ng.Notification[]}
	   * merged array of old and new notifications
	   */
	  var append = function append(newItems, existingItems) {
	    return [].concat(_toConsumableArray(existingItems), _toConsumableArray(newItems));
	  };
	
	  /**
	   * Replaces existing notifications with new notifications
	   *
	   * @inner
	   * @name NotificationsBadgeController#replace
	   * @type {function}
	   * @returns {module:model-bb-notifications-ng.Notification[]} new notifications
	   */
	  var replace = function replace(items) {
	    return items;
	  };
	
	  /**
	   * @description
	   * Load notifications.
	   *
	   * @name NotificationsBadgeController#loadNotifications
	   *
	   * @inner
	   * @type {function}
	   * @param {object} [params={}] Custom params for request
	   * @param {function} [merge=append] Determines whether to use the old array on notifications
	   * @param {boolean} [applyParams=true] True if need save request params
	   * @returns {Promise.<module:model-bb-notifications-ng.Notifications, ModelError>}
	   * A Promise with loaded notifications
	   */
	  var loadNotifications = function loadNotifications() {
	    var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
	    var merge = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : append;
	    var applyParams = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
	
	    var currentParams = Object.assign({}, notifications.params, params);
	    state.notifications.loading = true;
	
	    return model.load(currentParams).then(function (raw) {
	      state.notifications.loading = false;
	      state.notifications.data = merge(raw.data, state.notifications.data);
	      if (applyParams) {
	        notifications.params = currentParams;
	      }
	      notifications.params.cursor = raw.cursor || null;
	    }).catch(function (error) {
	      state.notifications.loading = false;
	      state.notifications.error = uiError(errorMessages, error);
	
	      throw error;
	    });
	  };
	
	  /**
	   * @description
	   * Load next notification.
	   *
	   * @name NotificationsBadgeController#loadNextNotification
	   *
	   * @inner
	   * @type {function}
	   * @returns {null|Promise.<module:model-bb-notifications-ng.Notifications, ModelError>}
	   * A Promise with loaded notification
	   */
	  var loadNextNotification = function loadNextNotification() {
	    return notifications.params.cursor ? loadNotifications({ size: 1 }, append, false) : null;
	  };
	
	  /**
	   * @description
	   * Load more notifications.
	   *
	   * @name NotificationsBadgeController#loadMore
	   * @type {function}
	   * @param {function} done Callback function for `ui-bb-load-more-ng` component
	   * @returns {null|Promise.<module:model-bb-notifications-ng.Notifications, ModelError>}
	   * A Promise with loaded notifications
	   */
	  var loadMore = function loadMore(done) {
	    if (state.notifications.loading) {
	      return null;
	    }
	
	    return loadNotifications().then(done).catch(done);
	  };
	
	  /**
	   * @name NotificationsBadgeController#reload
	   *
	   * @description
	   * Reloads the current collection
	   *
	   * @inner
	   * @type {function}
	   * @returns {Promise.<module:model-bb-notifications-ng.Notifications, ModelError>}
	   */
	  var reload = function reload() {
	    notifications.params.cursor = null;
	    state.notifications.data = [];
	    state.notifications.error = null;
	    state.popover.error = null;
	
	    return loadNotifications(replace);
	  };
	
	  /**
	   * @name NotificationsBadgeController#navigateTo
	   * @description Changes current page in widget
	   * @type {function}
	   * @inner
	   * @param {string} page Page to navigate to
	   * @param {null|module:model-bb-notifications-ng.Notification} item Notification item
	   */
	  function navigateTo(page) {
	    var item = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
	
	    state.page = page;
	    $ctrl.state.notifications.active = item;
	  }
	
	  /**
	   * @name NotificationsBadgeController#viewNotificationDetails
	   * @type {function}
	   * @description Rest active notification and navigates the user to the Notifications list view.
	   */
	  var viewNotificationList = function viewNotificationList() {
	    if (state.page !== Page.LIST) {
	      navigateTo(Page.LIST);
	    }
	  };
	
	  /**
	   * @name NotificationsBadgeController#togglePopover
	   * @type {function}
	   * @description
	   * Load notifications and toggles the popover
	   */
	  var togglePopover = function togglePopover() {
	    state.popover.isOpen = !state.popover.isOpen;
	
	    if (state.popover.isOpen) {
	      state.popover.loading = true;
	
	      viewNotificationList();
	      reload().then(function () {
	        state.popover.loading = false;
	      }).catch(function (error) {
	        state.popover.loading = false;
	        state.popover.error = uiError(errorMessages, error);
	      });
	    }
	  };
	
	  /**
	   * @description
	   * Get notifications by ID.
	   *
	   * @name NotificationsBadgeController#getNotificationById
	   *
	   * @inner
	   * @type {function}
	   * @param {string} id Notification ID
	   * @returns {module:model-bb-notifications-ng.Notification} A notification object
	   */
	  var getNotificationById = function getNotificationById(id) {
	    return $ctrl.state.notifications.data.find(function (item) {
	      return item.id === id;
	    });
	  };
	
	  /**
	   * @description
	   * Mark notification as read/unread.
	   *
	   * @name NotificationsBadgeController#markNotification
	   * @type {function}
	   * @param {string} id Notification ID.
	   * @param {boolean} read True if notification was read.
	   * @returns {null|Promise.<void, ModelError>}
	   * A Promise with result of marking notification or null if notification is updating
	   */
	  var markNotification = function markNotification(id) {
	    var read = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
	
	    var notification = getNotificationById(id);
	    if (notification.isUpdating) {
	      return null;
	    }
	    notification.isUpdating = true;
	
	    return model.putReadRecord(notification.id, { read: read }).then(function () {
	      notification.isUpdating = false;
	      notification.read = read;
	      eventBus.publish(_constants.Event.NOTIFICATION_CHANGE_READ_STATUS, notification);
	    }).catch(function (error) {
	      notification.read = !read;
	      state.notifications.error = uiError(errorMessages, error);
	      notification.isUpdating = false;
	    });
	  };
	
	  /**
	   * @name NotificationsBadgeController#viewNotificationDetails
	   * @description Set active notification and navigates the user to the Notifications Detail view.
	   * @type {function}
	   * @param {module:model-bb-notifications-ng.Notification} item Active notification
	   */
	  var viewNotificationDetails = function viewNotificationDetails(item) {
	    if (!item.read) {
	      markNotification(item.id);
	    }
	    if (state.page !== Page.DETAILS) {
	      navigateTo(Page.DETAILS, item);
	    }
	  };
	
	  /**
	   * @description
	   * Remove notification from array.
	   *
	   * @name NotificationsBadgeController#removeNotification
	   *
	   * @inner
	   * @type {function}
	   * @param {module:model-bb-notifications-ng.Notification} item A notification object
	   */
	  var removeNotification = function removeNotification(item) {
	    var index = $ctrl.state.notifications.data.indexOf(item);
	    if (index !== -1) {
	      $ctrl.state.notifications.data.splice(index, 1);
	      eventBus.publish(_constants.Event.NOTIFICATION_DELETED, item);
	    }
	  };
	
	  /**
	   * @description
	   * Delete notification.
	   *
	   * @name NotificationsBadgeController#deleteNotification
	   * @type {function}
	   * @param {string} id Notification ID.
	   * @returns {null|Promise.<void, ModelError>}
	   * A Promise with result of deleting notification or null if notification is updating
	   */
	  var deleteNotification = function deleteNotification(id) {
	    var notification = getNotificationById(id);
	    if (notification.isUpdating) {
	      return null;
	    }
	    notification.isUpdating = true;
	
	    return model.deleteRecord(id).then(function () {
	      if (!notification.read) {
	        eventBus.publish(_constants.Event.NUMBER_OF_UNREAD_CHANGED, -1);
	      }
	
	      removeNotification(notification);
	    }).then(function () {
	      return navigateTo(Page.LIST);
	    }).then(function () {
	      return loadNextNotification();
	    }).catch(function (error) {
	      state.notifications.error = uiError(errorMessages, error);
	      notification.isUpdating = false;
	    });
	  };
	
	  /**
	   * @name NotificationsBadgeController#isNotificationUnRead
	   * @description
	   * if notification is unread returns true else false
	   *
	   * @type {function}
	   * @param {module:model-bb-notifications-ng.Notification} item Notification item
	   * @returns {boolean} True if notification is read or false if notification is unread
	   */
	  var isNotificationUnRead = function isNotificationUnRead(item) {
	    return Boolean(!item.read);
	  };
	
	  /**
	   * @description
	   * Function that performs when error occurs in load unread count iteration
	   *
	   * @name onLoadUnreadCountError
	   *
	   * @inner
	   * @type {function}
	   */
	  var stopPolling = function stopPolling() {
	    return model.stopPolling(pollingRef);
	  };
	
	  /**
	   * @description
	   * Init unread count polling.
	   *
	   * @name initPolling
	   *
	   * @inner
	   * @type {function}
	   */
	  var initPolling = function initPolling() {
	    var pollingOptions = {
	      type: _modelBbNotificationsNg.PollingType.UNREAD_COUNT,
	      pollingInterval: pollingInterval
	    };
	
	    pollingRef = model.initPolling(pollingOptions);
	
	    eventBus.subscribe(_constants.Event.NOTIFICATION_UNREAD_COUNT_SUCCESS, onLoadUnreadCountSuccess);
	    eventBus.subscribe(_constants.Event.NOTIFICATION_UNREAD_COUNT_ERROR, stopPolling);
	  };
	
	  /*
	   * Widget initialization logic.
	   */
	  var $onInit = function $onInit() {
	    eventBus.subscribe(_constants.Event.NUMBER_OF_UNREAD_CHANGED, function (count) {
	      state.badge.numberOfUnread += count;
	    });
	
	    eventBus.subscribe(_constants.Event.NOTIFICATION_CHANGE_READ_STATUS, function (response) {
	      var notification = getNotificationById(response.id);
	
	      state.badge.numberOfUnread += response.read ? -1 : 1;
	
	      if (notification) {
	        notification.read = response.read;
	      }
	    });
	
	    eventBus.subscribe(_constants.Event.NOTIFICATION_DELETED, function (response) {
	      var notification = getNotificationById(response.id);
	
	      if (notification) {
	        removeNotification(notification);
	        navigateTo(Page.LIST);
	        loadNextNotification();
	      }
	    });
	
	    loadUnreadCount();
	
	    initPolling();
	  };
	
	  /*
	   * Widget destroying logic.
	   */
	  var $onDestroy = function $onDestroy() {
	    return stopPolling();
	  };
	
	  Object.assign($ctrl, {
	    state: state,
	    markNotification: markNotification,
	    viewNotificationList: viewNotificationList,
	    viewNotificationDetails: viewNotificationDetails,
	    deleteNotification: deleteNotification,
	    isNotificationUnRead: isNotificationUnRead,
	    loadMore: loadMore,
	    togglePopover: togglePopover,
	    $onInit: $onInit,
	    $onDestroy: $onDestroy
	  });
	}

/***/ }),

/***/ 59:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @description
	 * Widget events enum (events generated by the widget)
	 *
	 * @name Event
	 * @type {Object}
	 */
	var Event = exports.Event = {
	  NUMBER_OF_UNREAD_CHANGED: 'bb.event.number.of.unread.changed',
	  NOTIFICATION_CHANGE_READ_STATUS: 'bb.event.notification.change.read.status',
	  NOTIFICATION_DELETED: 'bb.event.notification.deleted',
	  NOTIFICATION_UNREAD_COUNT_SUCCESS: 'bb.event.notifications.unread.count.success',
	  NOTIFICATION_UNREAD_COUNT_ERROR: 'bb.event.notifications.unread.count.error'
	};
	
	/**
	 * @description
	 * Widget static messages for the template
	 *
	 * @name Message
	 * @type {Object}
	 */
	var MessageKey = exports.MessageKey = {
	  ERROR_AUTH: 'model.error.auth',
	  ERROR_CONNECTION: 'model.error.connectivity',
	  ERROR_USER: 'model.error.user',
	  ERROR_UNEXPECTED: 'model.error.unexpected'
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=widget-bb-notification-badge-ng.js.map