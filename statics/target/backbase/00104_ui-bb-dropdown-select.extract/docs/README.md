# ui-bb-dropdown-select


Version: **1.0.80**

UI dropdown select component

## Imports

* vendor-bb-angular
* vendor-bb-uib-dropdown

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbDropdownSelectKey from 'ui-bb-dropdown-select';

export const dependencyKeys = [
  uiBbDropdownSelectKey,
];

// file: templates/template.ng.html
<ui-bb-dropdown-select
  ng-model="item"
  selected-as="$option.name">
  <ui-bb-dropdown-option
    option="item"
    ng-repeat="item in items"
    class="list-group-item-text">
      {{:: $option.name }}
  </ui-bb-dropdown-option>
</ui-bb-dropdown-select>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **uiBbDropdownSelectDirective**<br/>    <a href="#uiBbDropdownSelectDirective#onBlur">#onBlur(event)</a><br/>

## Exports

### <a name="default"></a>*default*

The angular module name

**Type:** *String*


---

## uiBbDropdownOption


---

## uiBbDropdownSelectDirective


| Property | Type | Description |
| :-- | :-- | :-- |
| is-open | Boolean | Defines whether or not the dropdown is open |
| ng-disabled | Boolean | Defines whether or not the dropdown is disabled |
| ng-model | Object | Dropdown model |
| ng-change | Function | Callback function triggered when dropdown item is selected |
| selected-as | Function | Allows to customize selected value |

### <a name="uiBbDropdownSelectDirective#onBlur"></a>*#onBlur(event)*

Close menu if next focused element is outside of container

| Parameter | Type | Description |
| :-- | :-- | :-- |
| event | Object |  |

---

## uiBbDropdownSelectedDirective

