(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("ui-bb-account-card"), require("ui-bb-inline-status-ng"), require("ui-bb-confirm-ng"), require("ui-bb-substitute-error-ng"), require("ui-bb-notification-stripe-ng"), require("ui-bb-iban-ng"), require("ui-bb-i18n-ng"), require("ui-bb-load-more-ng"), require("ui-bb-loading-indicator-ng"), require("ui-bb-track-form-changes-ng"), require("vendor-bb-angular-ng-messages"), require("vendor-bb-angular-ng-aria"));
	else if(typeof define === 'function' && define.amd)
		define("ext-bb-contact-list-ng", ["ui-bb-account-card", "ui-bb-inline-status-ng", "ui-bb-confirm-ng", "ui-bb-substitute-error-ng", "ui-bb-notification-stripe-ng", "ui-bb-iban-ng", "ui-bb-i18n-ng", "ui-bb-load-more-ng", "ui-bb-loading-indicator-ng", "ui-bb-track-form-changes-ng", "vendor-bb-angular-ng-messages", "vendor-bb-angular-ng-aria"], factory);
	else if(typeof exports === 'object')
		exports["ext-bb-contact-list-ng"] = factory(require("ui-bb-account-card"), require("ui-bb-inline-status-ng"), require("ui-bb-confirm-ng"), require("ui-bb-substitute-error-ng"), require("ui-bb-notification-stripe-ng"), require("ui-bb-iban-ng"), require("ui-bb-i18n-ng"), require("ui-bb-load-more-ng"), require("ui-bb-loading-indicator-ng"), require("ui-bb-track-form-changes-ng"), require("vendor-bb-angular-ng-messages"), require("vendor-bb-angular-ng-aria"));
	else
		root["ext-bb-contact-list-ng"] = factory(root["ui-bb-account-card"], root["ui-bb-inline-status-ng"], root["ui-bb-confirm-ng"], root["ui-bb-substitute-error-ng"], root["ui-bb-notification-stripe-ng"], root["ui-bb-iban-ng"], root["ui-bb-i18n-ng"], root["ui-bb-load-more-ng"], root["ui-bb-loading-indicator-ng"], root["ui-bb-track-form-changes-ng"], root["vendor-bb-angular-ng-messages"], root["vendor-bb-angular-ng-aria"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_6__, __WEBPACK_EXTERNAL_MODULE_7__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_9__, __WEBPACK_EXTERNAL_MODULE_10__, __WEBPACK_EXTERNAL_MODULE_11__, __WEBPACK_EXTERNAL_MODULE_12__, __WEBPACK_EXTERNAL_MODULE_13__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.dependencyKeys = exports.helpers = exports.hooks = undefined;
	
	var _uiBbAccountCard = __webpack_require__(2);
	
	var _uiBbAccountCard2 = _interopRequireDefault(_uiBbAccountCard);
	
	var _uiBbInlineStatusNg = __webpack_require__(3);
	
	var _uiBbInlineStatusNg2 = _interopRequireDefault(_uiBbInlineStatusNg);
	
	var _uiBbConfirmNg = __webpack_require__(4);
	
	var _uiBbConfirmNg2 = _interopRequireDefault(_uiBbConfirmNg);
	
	var _uiBbSubstituteErrorNg = __webpack_require__(5);
	
	var _uiBbSubstituteErrorNg2 = _interopRequireDefault(_uiBbSubstituteErrorNg);
	
	var _uiBbNotificationStripeNg = __webpack_require__(6);
	
	var _uiBbNotificationStripeNg2 = _interopRequireDefault(_uiBbNotificationStripeNg);
	
	var _uiBbIbanNg = __webpack_require__(7);
	
	var _uiBbIbanNg2 = _interopRequireDefault(_uiBbIbanNg);
	
	var _uiBbI18nNg = __webpack_require__(8);
	
	var _uiBbI18nNg2 = _interopRequireDefault(_uiBbI18nNg);
	
	var _uiBbLoadMoreNg = __webpack_require__(9);
	
	var _uiBbLoadMoreNg2 = _interopRequireDefault(_uiBbLoadMoreNg);
	
	var _uiBbLoadingIndicatorNg = __webpack_require__(10);
	
	var _uiBbLoadingIndicatorNg2 = _interopRequireDefault(_uiBbLoadingIndicatorNg);
	
	var _uiBbTrackFormChangesNg = __webpack_require__(11);
	
	var _uiBbTrackFormChangesNg2 = _interopRequireDefault(_uiBbTrackFormChangesNg);
	
	var _vendorBbAngularNgMessages = __webpack_require__(12);
	
	var _vendorBbAngularNgMessages2 = _interopRequireDefault(_vendorBbAngularNgMessages);
	
	var _vendorBbAngularNgAria = __webpack_require__(13);
	
	var _vendorBbAngularNgAria2 = _interopRequireDefault(_vendorBbAngularNgAria);
	
	var _hooks = __webpack_require__(14);
	
	var extHooks = _interopRequireWildcard(_hooks);
	
	var _helpers = __webpack_require__(15);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module ext-bb-contact-list-ng
	 *
	 * @description
	 * Default extension for contact widget.
	 *
	 * @requires ui-bb-account-card
	 *
	 * @example
	 * <!-- payment widget model.xml -->
	 * <property name="extension" viewHint="text-input,admin">
	 *  <value type="string">ext-bb-contact-list-ng</value>
	 * </property>
	 *
	 * Usage of ui-bb-account-card component in template
	 *
	 * <ui-bb-account-card
	 *   account-name="contact.name"
	 *   account-image="contact.image
	 *   account-number="contact.IBAN"
	 *   show-avatar="true">
	 * </ui-bb-account-card>
	 */
	var hooks = exports.hooks = extHooks;
	var helpers = exports.helpers = _helpers2.default;
	
	var dependencyKeys = exports.dependencyKeys = [_uiBbAccountCard2.default, _uiBbInlineStatusNg2.default, _uiBbConfirmNg2.default, _uiBbSubstituteErrorNg2.default, _uiBbNotificationStripeNg2.default, _uiBbIbanNg2.default, _uiBbI18nNg2.default, _uiBbLoadMoreNg2.default, _uiBbLoadingIndicatorNg2.default, _vendorBbAngularNgMessages2.default, _vendorBbAngularNgAria2.default, _uiBbTrackFormChangesNg2.default];

/***/ }),
/* 2 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),
/* 4 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),
/* 5 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_6__;

/***/ }),
/* 7 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_7__;

/***/ }),
/* 8 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),
/* 9 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_9__;

/***/ }),
/* 10 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_10__;

/***/ }),
/* 11 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_11__;

/***/ }),
/* 12 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_12__;

/***/ }),
/* 13 */
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_13__;

/***/ }),
/* 14 */
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.deleteContact = deleteContact;
	exports.processContacts = processContacts;
	exports.getSelectedContact = getSelectedContact;
	/**
	 * @name deleteContact
	 *
	 * @description
	 * Delete contact action handler
	 *
	 * @param {object} contact Contact object
	 * @type {function}
	 * @param {function} confirm Called to confirm delete action
	 */
	function deleteContact(contact, confirm) {
	  confirm();
	}
	
	/**
	 * @name processContacts
	 *
	 * @description
	 * Extension hook for pre-processing contacts
	 *
	 * @param {array} contacts
	 * @type {function}
	 * @returns {array} contacts Array of contacts
	 */
	function processContacts() {
	  var contacts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	
	  return contacts;
	}
	
	/**
	 * @name getSelectedContact
	 *
	 * @description
	 * Selects the contact from contacts by id
	 * Or returns null if nothing is found
	 *
	 * @param {array} contacts Array of contacts
	 * @param {object} contact Contact object
	 * @type {function}
	 * @returns {object|null} Returns found contact or null
	 */
	function getSelectedContact() {
	  var contacts = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
	  var contact = arguments[1];
	
	  // decoupling original contact list from selected contact
	  var firstContact = contacts[0] ? Object.assign({}, contacts[0]) : null;
	  if (!contact) {
	    return firstContact;
	  }
	
	  var foundContact = contacts.find(function (item) {
	    return item.id === contact.id;
	  });
	  return foundContact ? Object.assign({}, foundContact) : firstContact;
	}

/***/ }),
/* 15 */
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var resetFormState = function resetFormState(form) {
	  if (form) {
	    form.$setUntouched();
	    form.$setPristine();
	  }
	};
	
	var selectedContact = void 0;
	
	var helpers = function helpers(context) {
	  var filter = context.$filter;
	
	  return {
	    getSelectedContact: function getSelectedContact() {
	      return selectedContact;
	    },
	    setSelectedContact: function setSelectedContact(contact) {
	      selectedContact = contact;
	    },
	    isFormPristine: function isFormPristine(form) {
	      return !form || form.$pristine;
	    },
	    cancelEditForm: function cancelEditForm(form) {
	      return resetFormState(form);
	    },
	    saveContact: function saveContact($ctrl, form) {
	      var contact = $ctrl.state.contact.data;
	      return $ctrl.saveContact(contact).then(function () {
	        return resetFormState(form);
	      });
	    },
	    notificationMessage: function notificationMessage(statusObject) {
	      var message = statusObject.text || '';
	      if (statusObject.i18n) {
	        message = filter('i18n')(statusObject.i18n);
	      }
	
	      return message;
	    }
	  };
	};
	
	exports.default = helpers;

/***/ })
/******/ ])
});
;
//# sourceMappingURL=ext-bb-contact-list-ng.js.map