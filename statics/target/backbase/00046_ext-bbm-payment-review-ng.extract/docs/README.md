# ext-bbm-payment-review-ng


Version: **2.1.68**

Mobile extension for a payment review step in the Mobile payment widget.

## Imports

* lib-bbm-plugins
* ui-bb-format-amount
* ui-bb-i18n-ng

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-payment-review-ng</value>
</property>
```

## Table of Contents
- **Helpers**<br/>    <a href="#Helpers#getScheduleSummary">#getScheduleSummary(schedule)</a><br/>

---

## Helpers

Helpers for ext-bbm-payment-form-ng

### <a name="Helpers#getScheduleSummary"></a>*#getScheduleSummary(schedule)*

Returns a summary of the given payment schedule as a string.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| schedule | <a href="#Schedule">Schedule</a> |  |

##### Returns

String - **
