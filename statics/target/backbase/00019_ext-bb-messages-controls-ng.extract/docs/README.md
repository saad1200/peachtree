# ext-bb-messages-controls-ng


Version: **1.0.16**

Message center default extension.

## Imports

* ui-bb-confirm-ng
* ui-bb-draft-edit-ng
* ui-bb-i18n-ng
* vendor-bb-uib-accordion

---

## Example

```javascript
<!-- messages widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bb-messages-controls-ng</value>
</property>
```

## Table of Contents
