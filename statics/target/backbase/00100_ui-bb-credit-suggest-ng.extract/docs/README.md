# ui-bb-credit-suggest-ng


Version: **1.2.229**

Credit suggest input UI component

## Imports

* ui-bb-avatar-ng
* ui-bb-i18n-ng
* ui-bb-iban-ng
* vendor-bb-angular
* vendor-bb-uib-debounce
* vendor-bb-uib-position
* vendor-bb-uib-typeahead

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbCreditSuggestKey from 'ui-bb-credit-suggest-ng';

export const dependencyKeys = [
  uiBbCreditSuggestKey,
];

// file: templates/template.ng.html
<ui-bb-credit-suggest-ng
  name="credit"
  data-ng-model="$ctrl.payment.to"
  data-accounts="$ctrl.accountsTo"
  data-iban-validation-classes
  required
></ui-bb-credit-suggest-ng>
```

## Table of Contents
- **uiBBCreditSuggest**<br/>    <a href="#uiBBCreditSuggest#messages">#messages</a><br/>    <a href="#uiBBCreditSuggest#customTemplateId">#customTemplateId</a><br/>
- **ui-bb-credit-suggest-ng**<br/>    <a href="#ui-bb-credit-suggest-nguiBbCreditSuggestController">uiBbCreditSuggestController()</a><br/>

---

## uiBBCreditSuggest


| Property | Type | Description |
| :-- | :-- | :-- |
| accounts | Array of Object | List of accounts to filter and select with user input |
| messages | Object | Localized messages |
| custom-template-id | String | Template ID (or URL) which will be rendered as an option in dropdown |
| allow-external | Boolean | Are external accounts included in list. If not, IBAN field stays disabled |
| get-accounts | Function | External method for transform accounts array into custom structure Can be defined in extensions helpers |
| iban-validation-classes | <a href="#void">void</a> | Append has-success and has-error classes to IBAN field on validation |
### <a name="uiBBCreditSuggest#messages"></a>*#messages*

List of messages to be shown by component

**Type:** *Object*

### <a name="uiBBCreditSuggest#customTemplateId"></a>*#customTemplateId*

Template ID (or URL) which will be rendered
as an option in dropdown

**Type:** *String*


---

### <a name="ui-bb-credit-suggest-nguiBbCreditSuggestController"></a>*uiBbCreditSuggestController()*

Credit suggest component controller.
