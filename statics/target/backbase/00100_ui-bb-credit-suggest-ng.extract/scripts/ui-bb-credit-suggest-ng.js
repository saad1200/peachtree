(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("ui-bb-i18n-ng"), require("ui-bb-iban-ng"), require("ui-bb-avatar-ng"), require("vendor-bb-angular"), require("vendor-bb-uib-debounce"), require("vendor-bb-uib-position"), require("vendor-bb-uib-typeahead"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-credit-suggest-ng", ["ui-bb-i18n-ng", "ui-bb-iban-ng", "ui-bb-avatar-ng", "vendor-bb-angular", "vendor-bb-uib-debounce", "vendor-bb-uib-position", "vendor-bb-uib-typeahead"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-credit-suggest-ng"] = factory(require("ui-bb-i18n-ng"), require("ui-bb-iban-ng"), require("ui-bb-avatar-ng"), require("vendor-bb-angular"), require("vendor-bb-uib-debounce"), require("vendor-bb-uib-position"), require("vendor-bb-uib-typeahead"));
	else
		root["ui-bb-credit-suggest-ng"] = factory(root["ui-bb-i18n-ng"], root["ui-bb-iban-ng"], root["ui-bb-avatar-ng"], root["vendor-bb-angular"], root["vendor-bb-uib-debounce"], root["vendor-bb-uib-position"], root["vendor-bb-uib-typeahead"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_28__, __WEBPACK_EXTERNAL_MODULE_47__, __WEBPACK_EXTERNAL_MODULE_55__, __WEBPACK_EXTERNAL_MODULE_64__, __WEBPACK_EXTERNAL_MODULE_65__, __WEBPACK_EXTERNAL_MODULE_66__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(63);

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),

/***/ 24:
/***/ (function(module, exports) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	// css base code, injected by the css-loader
	module.exports = function() {
		var list = [];
	
		// return the list of modules as css string
		list.toString = function toString() {
			var result = [];
			for(var i = 0; i < this.length; i++) {
				var item = this[i];
				if(item[2]) {
					result.push("@media " + item[2] + "{" + item[1] + "}");
				} else {
					result.push(item[1]);
				}
			}
			return result.join("");
		};
	
		// import a list of modules into the list
		list.i = function(modules, mediaQuery) {
			if(typeof modules === "string")
				modules = [[null, modules, ""]];
			var alreadyImportedModules = {};
			for(var i = 0; i < this.length; i++) {
				var id = this[i][0];
				if(typeof id === "number")
					alreadyImportedModules[id] = true;
			}
			for(i = 0; i < modules.length; i++) {
				var item = modules[i];
				// skip already imported module
				// this implementation is not 100% perfect for weird media query combinations
				//  when a module is imported multiple times with different media queries.
				//  I hope this will never occur (Hey this way we have smaller bundles)
				if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
					if(mediaQuery && !item[2]) {
						item[2] = mediaQuery;
					} else if(mediaQuery) {
						item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
					}
					list.push(item);
				}
			}
		};
		return list;
	};


/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

	/*
		MIT License http://www.opensource.org/licenses/mit-license.php
		Author Tobias Koppers @sokra
	*/
	var stylesInDom = {},
		memoize = function(fn) {
			var memo;
			return function () {
				if (typeof memo === "undefined") memo = fn.apply(this, arguments);
				return memo;
			};
		},
		isOldIE = memoize(function() {
			return /msie [6-9]\b/.test(self.navigator.userAgent.toLowerCase());
		}),
		getHeadElement = memoize(function () {
			return document.head || document.getElementsByTagName("head")[0];
		}),
		singletonElement = null,
		singletonCounter = 0,
		styleElementsInsertedAtTop = [];
	
	module.exports = function(list, options) {
		if(false) {
			if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
		}
	
		options = options || {};
		// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
		// tags it will allow on a page
		if (typeof options.singleton === "undefined") options.singleton = isOldIE();
	
		// By default, add <style> tags to the bottom of <head>.
		if (typeof options.insertAt === "undefined") options.insertAt = "bottom";
	
		var styles = listToStyles(list);
		addStylesToDom(styles, options);
	
		return function update(newList) {
			var mayRemove = [];
			for(var i = 0; i < styles.length; i++) {
				var item = styles[i];
				var domStyle = stylesInDom[item.id];
				domStyle.refs--;
				mayRemove.push(domStyle);
			}
			if(newList) {
				var newStyles = listToStyles(newList);
				addStylesToDom(newStyles, options);
			}
			for(var i = 0; i < mayRemove.length; i++) {
				var domStyle = mayRemove[i];
				if(domStyle.refs === 0) {
					for(var j = 0; j < domStyle.parts.length; j++)
						domStyle.parts[j]();
					delete stylesInDom[domStyle.id];
				}
			}
		};
	}
	
	function addStylesToDom(styles, options) {
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			if(domStyle) {
				domStyle.refs++;
				for(var j = 0; j < domStyle.parts.length; j++) {
					domStyle.parts[j](item.parts[j]);
				}
				for(; j < item.parts.length; j++) {
					domStyle.parts.push(addStyle(item.parts[j], options));
				}
			} else {
				var parts = [];
				for(var j = 0; j < item.parts.length; j++) {
					parts.push(addStyle(item.parts[j], options));
				}
				stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
			}
		}
	}
	
	function listToStyles(list) {
		var styles = [];
		var newStyles = {};
		for(var i = 0; i < list.length; i++) {
			var item = list[i];
			var id = item[0];
			var css = item[1];
			var media = item[2];
			var sourceMap = item[3];
			var part = {css: css, media: media, sourceMap: sourceMap};
			if(!newStyles[id])
				styles.push(newStyles[id] = {id: id, parts: [part]});
			else
				newStyles[id].parts.push(part);
		}
		return styles;
	}
	
	function insertStyleElement(options, styleElement) {
		var head = getHeadElement();
		var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
		if (options.insertAt === "top") {
			if(!lastStyleElementInsertedAtTop) {
				head.insertBefore(styleElement, head.firstChild);
			} else if(lastStyleElementInsertedAtTop.nextSibling) {
				head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
			} else {
				head.appendChild(styleElement);
			}
			styleElementsInsertedAtTop.push(styleElement);
		} else if (options.insertAt === "bottom") {
			head.appendChild(styleElement);
		} else {
			throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
		}
	}
	
	function removeStyleElement(styleElement) {
		styleElement.parentNode.removeChild(styleElement);
		var idx = styleElementsInsertedAtTop.indexOf(styleElement);
		if(idx >= 0) {
			styleElementsInsertedAtTop.splice(idx, 1);
		}
	}
	
	function createStyleElement(options) {
		var styleElement = document.createElement("style");
		styleElement.type = "text/css";
		insertStyleElement(options, styleElement);
		return styleElement;
	}
	
	function createLinkElement(options) {
		var linkElement = document.createElement("link");
		linkElement.rel = "stylesheet";
		insertStyleElement(options, linkElement);
		return linkElement;
	}
	
	function addStyle(obj, options) {
		var styleElement, update, remove;
	
		if (options.singleton) {
			var styleIndex = singletonCounter++;
			styleElement = singletonElement || (singletonElement = createStyleElement(options));
			update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
			remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
		} else if(obj.sourceMap &&
			typeof URL === "function" &&
			typeof URL.createObjectURL === "function" &&
			typeof URL.revokeObjectURL === "function" &&
			typeof Blob === "function" &&
			typeof btoa === "function") {
			styleElement = createLinkElement(options);
			update = updateLink.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
				if(styleElement.href)
					URL.revokeObjectURL(styleElement.href);
			};
		} else {
			styleElement = createStyleElement(options);
			update = applyToTag.bind(null, styleElement);
			remove = function() {
				removeStyleElement(styleElement);
			};
		}
	
		update(obj);
	
		return function updateStyle(newObj) {
			if(newObj) {
				if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
					return;
				update(obj = newObj);
			} else {
				remove();
			}
		};
	}
	
	var replaceText = (function () {
		var textStore = [];
	
		return function (index, replacement) {
			textStore[index] = replacement;
			return textStore.filter(Boolean).join('\n');
		};
	})();
	
	function applyToSingletonTag(styleElement, index, remove, obj) {
		var css = remove ? "" : obj.css;
	
		if (styleElement.styleSheet) {
			styleElement.styleSheet.cssText = replaceText(index, css);
		} else {
			var cssNode = document.createTextNode(css);
			var childNodes = styleElement.childNodes;
			if (childNodes[index]) styleElement.removeChild(childNodes[index]);
			if (childNodes.length) {
				styleElement.insertBefore(cssNode, childNodes[index]);
			} else {
				styleElement.appendChild(cssNode);
			}
		}
	}
	
	function applyToTag(styleElement, obj) {
		var css = obj.css;
		var media = obj.media;
	
		if(media) {
			styleElement.setAttribute("media", media)
		}
	
		if(styleElement.styleSheet) {
			styleElement.styleSheet.cssText = css;
		} else {
			while(styleElement.firstChild) {
				styleElement.removeChild(styleElement.firstChild);
			}
			styleElement.appendChild(document.createTextNode(css));
		}
	}
	
	function updateLink(linkElement, obj) {
		var css = obj.css;
		var sourceMap = obj.sourceMap;
	
		if(sourceMap) {
			// http://stackoverflow.com/a/26603875
			css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
		}
	
		var blob = new Blob([css], { type: "text/css" });
	
		var oldSrc = linkElement.href;
	
		linkElement.href = URL.createObjectURL(blob);
	
		if(oldSrc)
			URL.revokeObjectURL(oldSrc);
	}


/***/ }),

/***/ 28:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_28__;

/***/ }),

/***/ 47:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_47__;

/***/ }),

/***/ 55:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_55__;

/***/ }),

/***/ 63:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(55);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _vendorBbUibDebounce = __webpack_require__(64);
	
	var _vendorBbUibDebounce2 = _interopRequireDefault(_vendorBbUibDebounce);
	
	var _vendorBbUibPosition = __webpack_require__(65);
	
	var _vendorBbUibPosition2 = _interopRequireDefault(_vendorBbUibPosition);
	
	var _vendorBbUibTypeahead = __webpack_require__(66);
	
	var _vendorBbUibTypeahead2 = _interopRequireDefault(_vendorBbUibTypeahead);
	
	var _uiBbAvatarNg = __webpack_require__(47);
	
	var _uiBbAvatarNg2 = _interopRequireDefault(_uiBbAvatarNg);
	
	var _uiBbI18nNg = __webpack_require__(8);
	
	var _uiBbI18nNg2 = _interopRequireDefault(_uiBbI18nNg);
	
	var _uiBbIbanNg = __webpack_require__(28);
	
	var _uiBbIbanNg2 = _interopRequireDefault(_uiBbIbanNg);
	
	__webpack_require__(67);
	
	var _component = __webpack_require__(69);
	
	var _component2 = _interopRequireDefault(_component);
	
	var _controller = __webpack_require__(70);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module ui-bb-credit-suggest-ng
	 * @description
	 * Credit suggest input UI component
	 *
	 * @example
	 * // In an extension:
	 * // file: scripts/index.js
	 * import uiBbCreditSuggestKey from 'ui-bb-credit-suggest-ng';
	 *
	 * export const dependencyKeys = [
	 *   uiBbCreditSuggestKey,
	 * ];
	 *
	 * // file: templates/template.ng.html
	 * <ui-bb-credit-suggest-ng
	 *   name="credit"
	 *   data-ng-model="$ctrl.payment.to"
	 *   data-accounts="$ctrl.accountsTo"
	 *   data-iban-validation-classes
	 *   required
	 * ></ui-bb-credit-suggest-ng>
	 */
	
	exports.default = _vendorBbAngular2.default.module('ui-bb-credit-suggest-ng', [_uiBbAvatarNg2.default, _vendorBbUibDebounce2.default, _vendorBbUibPosition2.default, _vendorBbUibTypeahead2.default, _uiBbI18nNg2.default, _uiBbIbanNg2.default]).component('uiBbCreditSuggestNg', _component2.default).controller('controller', ['$element', '$attrs', '$templateCache', _controller2.default]).name;

/***/ }),

/***/ 64:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_64__;

/***/ }),

/***/ 65:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_65__;

/***/ }),

/***/ 66:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_66__;

/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

	// style-loader: Adds some css to the DOM by adding a <style> tag
	
	// load the styles
	var content = __webpack_require__(68);
	if(typeof content === 'string') content = [[module.id, content, '']];
	// add the styles to the DOM
	var update = __webpack_require__(25)(content, {});
	if(content.locals) module.exports = content.locals;
	// Hot Module Replacement
	if(false) {
		// When the styles change, update the <style> tags
		if(!content.locals) {
			module.hot.accept("!!../../node_modules/css-loader/index.js!./index.css", function() {
				var newContent = require("!!../../node_modules/css-loader/index.js!./index.css");
				if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
				update(newContent);
			});
		}
		// When the module is disposed, remove the <style> tags
		module.hot.dispose(function() { update(); });
	}

/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

	exports = module.exports = __webpack_require__(24)();
	// imports
	
	
	// module
	exports.push([module.id, "/* Component should define only limited structural styles */\nui-bb-credit-suggest-ng {\n  display: block;\n  position: relative;\n}\n\nui-bb-credit-suggest-ng .dropdown-menu {\n  position: absolute;\n  overflow-y: scroll;\n  width: 100%;\n}\n\nui-bb-credit-suggest-ng .dropdown-menu > li > a.ext-account {\n  position: relative;\n}\n\nui-bb-credit-suggest-ng .no-results {\n  position: absolute;\n  width: 100%;\n}\n", ""]);
	
	// exports


/***/ }),

/***/ 69:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBBCreditSuggest
	 * @type {object}
	 *
	 * @property {object[]} accounts List of accounts to filter and select with user input
	 * @property {object} messages Localized messages
	 * @property {string} custom-template-id Template ID (or URL)
	 * which will be rendered as an option in dropdown
	 * @property {boolean} allow-external Are external accounts included in list.
	 * If not, IBAN field stays disabled
	 * @property {function} get-accounts
	 * External method for transform accounts array into custom structure
	 * Can be defined in extensions helpers
	 * @property {void} iban-validation-classes Append has-success and has-error classes
	 * to IBAN field on validation
	 */
	var component = {
	  bindings: {
	    accounts: '<',
	    /**
	     * @description
	     * List of messages to be shown by component
	     *
	     * @name uiBBCreditSuggest#messages
	     * @type {object} messages
	     */
	    messages: '<',
	    /**
	     * @description
	     * Template ID (or URL) which will be rendered
	     * as an option in dropdown
	     *
	     * @name uiBBCreditSuggest#customTemplateId
	     * @type {string} customTemplateId
	     */
	    customTemplateId: '@',
	    allowExternal: '<',
	    getAccounts: '&'
	  },
	  controller: 'controller',
	  template: '\n    <div class="credit-suggest input-group has-feedback">\n      <input type="text"\n        placeholder="{{ $ctrl.messages.filterPlaceholder }}"\n        data-ng-model="$ctrl.selectedSetter"\n        data-ng-model-options="{\n          getterSetter: true,\n          allowInvalid: true\n        }"\n        class="form-control"\n        uib-typeahead="account as account.name for account in $ctrl.filterAccounts($viewValue)"\n        typeahead-no-results="$ctrl.noResults"\n        typeahead-min-length="0"\n        typeahead-popup-template-url="ui-bb-credit-suggest-ng/template/popup.html"\n        typeahead-template-url="{{\n          $ctrl.customTemplateId || \'ui-bb-credit-suggest-ng/template/option.html\'\n        }}"\n        data-ng-focus="$ctrl.startEdit()"\n        data-ng-blur="$ctrl.finishEdit()"\n        aria-label="{{:: $ctrl.messages.hint }}"\n        required>\n      <input data-ui-bb-iban\n        type="text"\n        class="form-control"\n        data-ng-class="{\n          \'has-success\': $ctrl.markIbanStatus && $ctrl.validateIban(),\n          \'has-error\': $ctrl.markIbanStatus && !$ctrl.validateIban(),\n        }"\n        placeholder="{{ $ctrl.messages.accountPlaceholder }}"\n        data-ng-model="$ctrl.selected.identifier"\n        aria-label="{{ $ctrl.messages.accountPlaceholder }}"\n        data-ng-model-options="{ allowInvalid: true }"\n        data-ng-change="$ctrl.validateIban()"\n        data-ng-focus="$ctrl.ibanInFocus = true"\n        data-ng-blur="$ctrl.validateIban(); $ctrl.ibanInFocus = false"\n        data-ng-disabled="!$ctrl.allowExternal || \n          !($ctrl.selected.isNew || $ctrl.selected.external)"\n        data-ng-required="$ctrl.allowExternal && $ctrl.selected.isNew || $ctrl.selected.external">\n      <span class="fa fa-check form-control-feedback success-feedback"\n        data-ng-if="$ctrl.markIbanStatus && $ctrl.validateIban()"\n        aria-hidden="true">\n      </span>\n      <span class="fa fa-close form-control-feedback error-feedback"\n        data-ng-if="$ctrl.markIbanStatus && !$ctrl.validateIban()"\n        aria-hidden="true">\n      </span>\n      <span class="input-group-addon" data-ng-click="$ctrl.open()">\n        <i class="fa fa-book" aria-hidden="true"></i>\n      </span>\n    </div>\n    <div data-ng-if="$ctrl.noResults && $ctrl.filterInFocus" class="form-control no-results">\n      {{ $ctrl.messages.noResults }} <b>\'{{$ctrl.selected.name}}\'</b>\n    </div>\n  '
	};
	
	exports.default = component;

/***/ }),

/***/ 70:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	exports.default = controller;
	
	function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }
	
	/**
	 * @name uiBbCreditSuggestController
	 * @ngkey uiBbCreditSuggestController
	 * @type {function}
	 * @description
	 * Credit suggest component controller.
	 */
	function controller($element, $attrs, $templateCache) {
	  var ctrl = {};
	  var EMPTY_VALUE = {
	    name: '',
	    identifier: '',
	    isNew: true
	  };
	
	  var ngModelCtrl = $element.controller('ngModel');
	  var filterInput = $element.find('input').eq(0);
	  var ibanInput = $element.find('input').eq(1);
	
	  var filterInputNgModelCtrl = void 0;
	  var ibanInputNgModelCtrl = void 0;
	  var isIbanValid = void 0;
	  var lastSelected = void 0;
	
	  /**
	   * @description
	   * Called after this controller's element and its children have been linked
	   * Initialize necessary functionality
	   *
	   * @name uiBbCreditSuggestController#$postLink
	   * @type {function} $postLink
	   */
	  var $postLink = function $postLink() {
	    // ngModel controller on input is not inited when
	    // $onInit is triggered on whole component
	    filterInputNgModelCtrl = filterInput.controller('ngModel');
	    ibanInputNgModelCtrl = ibanInput.controller('ngModel');
	    isIbanValid = ibanInputNgModelCtrl.$validators.uiBbIban;
	    delete ibanInputNgModelCtrl.$validators.uiBbIban;
	
	    // clone account if needed on edit iban
	    ibanInputNgModelCtrl.$parsers.push(function (any) {
	      if (!ctrl.selected.isNew) {
	        ctrl.selected = Object.assign({}, EMPTY_VALUE, ctrl.selected);
	      }
	      return any;
	    });
	  };
	
	  var isMyAccount = function isMyAccount(identifier) {
	    return ctrl.accounts && !!ctrl.accounts.find(function (acc) {
	      return !acc.external && acc.identifier === identifier;
	    });
	  };
	
	  var validateIban = function validateIban(modelValue) {
	    // need to show error on blur only
	    // but show valid asap
	    var isIbanFocused = $element[0].querySelector('input:focus') === ibanInput[0];
	    var identifier = modelValue && modelValue.identifier || ibanInputNgModelCtrl.$modelValue;
	
	    var isValid = isIbanFocused || isMyAccount(identifier) || isIbanValid(identifier);
	
	    ngModelCtrl.$setValidity('iban', isValid);
	    return isValid;
	  };
	
	  // eslint-disable-next-line no-bitwise
	  var filterByName = function filterByName(item, search) {
	    return item.name && ~item.name.toLowerCase().indexOf(search);
	  };
	
	  /**
	   * @description
	   * Filters accounts by name
	   * If getAccounts is defined - uses as a data composer for accounts
	   *
	   * @name uiBbCreditSuggestController#filterAccouns
	   * @type {function}
	   * @param {string} search
	   */
	  var filterAccounts = function filterAccounts(search) {
	    var viewAccounts = ctrl.getAccounts({
	      search: search,
	      accounts: ctrl.accounts
	    }) || ctrl.accounts;
	
	    if (search) {
	      var normalizedSearch = search.toLowerCase();
	      var filteredAccounts = viewAccounts.filter(function (item) {
	        return filterByName(item, normalizedSearch);
	      });
	
	      if (!ctrl.allowExternal) {
	        return filteredAccounts;
	      }
	
	      var newRecipient = Object.assign({}, EMPTY_VALUE, {
	        name: search,
	        identifier: ctrl.selected && ctrl.selected.identifier || ''
	      });
	
	      return [newRecipient].concat(_toConsumableArray(filteredAccounts));
	    }
	
	    return viewAccounts;
	  };
	
	  /**
	   * @description
	   * Open a filtered list of accounts
	   *
	   * @name uiBbCreditSuggestController#open
	   * @type {function} open
	   */
	  var open = function open() {
	    // hack for typeahead
	    // @see https://github.com/angular-ui/bootstrap/blob/master/src/typeahead/typeahead.js#L435
	    var viewValue = filterInputNgModelCtrl.$viewValue;
	    filterInputNgModelCtrl.$viewValue = '';
	
	    filterInput.triggerHandler('focus');
	    filterInput[0].select();
	
	    filterInputNgModelCtrl.$viewValue = viewValue;
	  };
	
	  /**
	   * @description
	   * Triggered when filter field gets focus
	   *
	   * @name uiBbCreditSuggestController#startEdit
	   * @type {function} startEdit
	   */
	  var startEdit = function startEdit() {
	    // update no results manually
	    // typeahead is not triggering filterAccouns
	    ctrl.noResults = false;
	    ctrl.filterInFocus = true;
	  };
	
	  /**
	   * @description
	   * Triggered when filter field looses focus
	   *
	   * @name uiBbCreditSuggestController#finishEdit
	   * @type {function} finishEdit
	   */
	  var finishEdit = function finishEdit() {
	    ctrl.filterInFocus = false;
	
	    // if externals are not allowed, don't allow new accounts
	    // revert to previously valid state
	    if (!ctrl.allowExternal && ctrl.selected.isNew && ctrl.selected.name !== '') {
	      ctrl.selected = lastSelected;
	    }
	  };
	
	  /**
	   * @description
	   * Default angular function running on digest cycle
	   * Applies selected credit to model.
	   *
	   * @name uiBbCreditSuggestController#$doCheck
	   * @type {function} $doCheck
	   */
	  var $doCheck = function $doCheck() {
	    var selected = ctrl.selected;
	    ngModelCtrl.$setViewValue(selected);
	
	    if (!selected) {
	      ngModelCtrl.$setPristine();
	    }
	
	    ctrl.markIbanStatus = $attrs.ibanValidationClasses !== undefined && ibanInput[0].value !== '' && !ctrl.ibanInFocus;
	  };
	
	  var selectedSetter = function selectedSetter(newValue) {
	    ctrl.selected = ctrl.selected || Object.assign({}, EMPTY_VALUE);
	
	    if (typeof newValue === 'string') {
	      if (ctrl.selected.isNew) {
	        ctrl.selected.name = newValue;
	      } else {
	        ctrl.selected = Object.assign({}, EMPTY_VALUE, {
	          name: newValue
	        });
	      }
	    } else if (newValue || (typeof newValue === 'undefined' ? 'undefined' : _typeof(newValue)) === 'object') {
	      ctrl.selected = newValue;
	    }
	
	    if (!(ctrl.allowExternal || ctrl.selected.isNew)) {
	      lastSelected = ctrl.selected;
	    }
	
	    return ctrl.selected;
	  };
	
	  ngModelCtrl.$validators.iban = validateIban;
	  ngModelCtrl.$formatters.push(function (credit) {
	    ctrl.selected = credit;
	  });
	
	  // set default templates
	  var popupTemplate = $templateCache.get('ui-bb-credit-suggest-ng/template/popup.html');
	  if (!popupTemplate) {
	    $templateCache.put('ui-bb-credit-suggest-ng/template/popup.html', $templateCache.get('uib/template/typeahead/typeahead-popup.html'));
	  }
	
	  var optionTemplate = $templateCache.get('ui-bb-credit-suggest-ng/template/option.html');
	  if (!optionTemplate) {
	    $templateCache.put('ui-bb-credit-suggest-ng/template/option.html', $templateCache.get('uib/template/typeahead/typeahead-match.html'));
	  }
	
	  Object.assign(ctrl, {
	    // models
	    /**
	     * @name uiBbCreditSuggestController#selected
	     * @type {AccountView} selected
	     */
	    selected: undefined,
	    selectedSetter: selectedSetter,
	
	    // methods
	    $postLink: $postLink,
	    $doCheck: $doCheck,
	    filterAccounts: filterAccounts,
	    startEdit: startEdit,
	    finishEdit: finishEdit,
	    validateIban: validateIban,
	    open: open,
	
	    // flags
	    filterInFocus: false,
	    ibanInFocus: false,
	    markIbanStatus: false
	  });
	
	  return ctrl;
	}

/***/ })

/******/ })
});
;
//# sourceMappingURL=ui-bb-credit-suggest-ng.js.map