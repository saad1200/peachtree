# ext-bb-login-ng


Version: **1.0.47**

Login extension for login widget.

## Imports

* ui-bb-i18n-ng
* vendor-bb-angular-ng-aria

---

## Example

```javascript
<!-- login widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bb-login-ng</value>
</property>
```

## Table of Contents
