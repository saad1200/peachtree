# ext-bbm-personal-profile-ng


Version: **1.0.20**

Mobile extension for personal profile widget.

## Imports

* ui-bb-avatar-ng
* ui-bb-i18n-ng

---

## Example

```javascript
<!-- personal profile widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bbm-personal-profile-ng</value>
</property>
```

## Table of Contents
