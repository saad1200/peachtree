# lib-bb-view-model-ng

Adds a state container (See
<a href="lib-bb-state-container.html#lib-bb-state-container">lib-bb-state-container</a>) to the widget's scope as `vm`.

This can be used to give widget *extensions* ownership over the view model.

The view model is also added to the context for extension intents and helpers, See also:
 - Extension <a href="lib-bb-extension-intents-ng.html#IntentContext">IntentContext</a>
 - Extension <a href="lib-bb-extension-helpers-ng.html#HelperContext">HelperContext</a>

## Imports

* lib-bb-state-container
* vendor-bb-angular

---

## Example

```javascript
// Extension JS: Set a "view state" on the view model when an intent is triggered
export const intents = ({ viewModel: vm }) => ({
  viewList: handleRequest('todos.list', vm.createAction((state) => {
    state.template = '#widget-bb-todo-ng/list.html';
    return state;
  }))
});

<!-- Extension template: Use `vm` on the scope -->
<h2>Todo</h2>
<div>
  <ng-include src="vm.template"></ng-include>
</div>
<script type="text/ng-template" id="#widget-bb-todo-ng/list.html">
  <ul><li ng-repeat="todo in vm.todos track by todo.id">{{todo.title}}</li></ul>
</script>
```

## Table of Contents
