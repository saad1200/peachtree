(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-state-container"));
	else if(typeof define === 'function' && define.amd)
		define("lib-bb-view-model-ng", ["vendor-bb-angular", "lib-bb-state-container"], factory);
	else if(typeof exports === 'object')
		exports["lib-bb-view-model-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-state-container"));
	else
		root["lib-bb-view-model-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-state-container"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_70__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(69);

/***/ }),

/***/ 5:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.bbViewModelKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(5);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbStateContainer = __webpack_require__(70);
	
	var _libBbStateContainer2 = _interopRequireDefault(_libBbStateContainer);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @module lib-bb-view-model-ng
	 *
	 * @description Adds a state container (See
	 * {@link module:lib-bb-state-container.lib-bb-state-container}) to the widget's scope as `vm`.
	 *
	 * This can be used to give widget *extensions* ownership over the view model.
	 *
	 * The view model is also added to the context for extension intents and helpers, See also:
	 *  - Extension {@link module:lib-bb-extension-intents-ng.IntentContext}
	 *  - Extension {@link module:lib-bb-extension-helpers-ng.HelperContext}
	 *
	 * @example
	 * // Extension JS: Set a "view state" on the view model when an intent is triggered
	 * export const intents = ({ viewModel: vm }) => ({
	 *   viewList: handleRequest('todos.list', vm.createAction((state) => {
	 *     state.template = '#widget-bb-todo-ng/list.html';
	 *     return state;
	 *   }))
	 * });
	 *
	 * <!-- Extension template: Use `vm` on the scope -->
	 * <h2>Todo</h2>
	 * <div>
	 *   <ng-include src="vm.template"></ng-include>
	 * </div>
	 * <script type="text/ng-template" id="#widget-bb-todo-ng/list.html">
	 *   <ul><li ng-repeat="todo in vm.todos track by todo.id">{{todo.title}}</li></ul>
	 * </script>
	 */
	
	var moduleKey = 'lib-bb-view-model-ng';
	
	var bbViewModelKey = exports.bbViewModelKey = moduleKey + ':viewModel';
	
	exports.default = _vendorBbAngular2.default.module(moduleKey, []
	
	// @improvement allow providing initial state (possibly similar to
	// `context` for extension helpers/etc.)
	).factory(bbViewModelKey, function () {
	  return (0, _libBbStateContainer2.default)({});
	}).run(['$rootScope', bbViewModelKey, function ($scope, viewModel) {
	  Object.defineProperty($scope, 'vm', {
	    get: function get() {
	      return viewModel.getState();
	    }
	  });
	}]).name;

/***/ }),

/***/ 70:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_70__;

/***/ })

/******/ })
});
;
//# sourceMappingURL=lib-bb-view-model-ng.js.map