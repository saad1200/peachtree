(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("ui-bb-calendar-popup-ng"), require("ui-bb-track-form-changes-ng"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-transaction-search-filter-ng", ["vendor-bb-angular", "ui-bb-calendar-popup-ng", "ui-bb-track-form-changes-ng"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-transaction-search-filter-ng"] = factory(require("vendor-bb-angular"), require("ui-bb-calendar-popup-ng"), require("ui-bb-track-form-changes-ng"));
	else
		root["ui-bb-transaction-search-filter-ng"] = factory(root["vendor-bb-angular"], root["ui-bb-calendar-popup-ng"], root["ui-bb-track-form-changes-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_78__, __WEBPACK_EXTERNAL_MODULE_79__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(77);

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),

/***/ 77:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _uiBbCalendarPopupNg = __webpack_require__(78);
	
	var _uiBbCalendarPopupNg2 = _interopRequireDefault(_uiBbCalendarPopupNg);
	
	var _uiBbTrackFormChangesNg = __webpack_require__(79);
	
	var _uiBbTrackFormChangesNg2 = _interopRequireDefault(_uiBbTrackFormChangesNg);
	
	var _component = __webpack_require__(80);
	
	var _component2 = _interopRequireDefault(_component);
	
	var _controller = __webpack_require__(81);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @name default
	 * @type {string}
	 * @description The angular module name
	 */
	
	exports.default = _vendorBbAngular2.default.module('ui-bb-transaction-search-filter-ng', [_uiBbCalendarPopupNg2.default, _uiBbTrackFormChangesNg2.default]).component('uiBbTransactionSearchFilter', _component2.default).controller('uiBbTransactionSearchFilterController', ['$scope', _controller2.default]).name; /**
	                                                                                                                                                                                                                                                                                                                       * @module ui-bb-transaction-search-filter-ng
	                                                                                                                                                                                                                                                                                                                       * @description
	                                                                                                                                                                                                                                                                                                                       * UI transaction-search filter component
	                                                                                                                                                                                                                                                                                                                       *
	                                                                                                                                                                                                                                                                                                                       * @example
	                                                                                                                                                                                                                                                                                                                       * // In an extension:
	                                                                                                                                                                                                                                                                                                                       * // file: scripts/index.js
	                                                                                                                                                                                                                                                                                                                       * import uiBbTransactionSearchFilterKey from 'ui-bb-transaction-search-filter-ng';
	                                                                                                                                                                                                                                                                                                                       *
	                                                                                                                                                                                                                                                                                                                       * export const dependencyKeys = [
	                                                                                                                                                                                                                                                                                                                       *   uiBbTransactionSearchFilterKey,
	                                                                                                                                                                                                                                                                                                                       * ];
	                                                                                                                                                                                                                                                                                                                       *
	                                                                                                                                                                                                                                                                                                                       * // file: templates/template.ng.html
	                                                                                                                                                                                                                                                                                                                       * <ui-bb-transaction-search-filter-ng
	                                                                                                                                                                                                                                                                                                                       * on-filter="$ctrl.filter()"
	                                                                                                                                                                                                                                                                                                                       * button-labels="{
	                                                                                                                                                                                                                                                                                                                       *  main: 'Filter',
	                                                                                                                                                                                                                                                                                                                       *  apply: 'Apply',
	                                                                                                                                                                                                                                                                                                                       *  cancel: 'Cancel',
	                                                                                                                                                                                                                                                                                                                       * }"
	                                                                                                                                                                                                                                                                                                                       * field-labels="{
	                                                                                                                                                                                                                                                                                                                       *  creditDebitIndicator: 'Credit/Debit indicator',
	                                                                                                                                                                                                                                                                                                                       *  credit: 'Credit',
	                                                                                                                                                                                                                                                                                                                       *  debit: 'Debit',
	                                                                                                                                                                                                                                                                                                                       *  dateRange: 'Date range',
	                                                                                                                                                                                                                                                                                                                       *  amountRange: 'Amount range',
	                                                                                                                                                                                                                                                                                                                       *  amountFrom: 'Amount from',
	                                                                                                                                                                                                                                                                                                                       *  amountTo: 'Amount to',
	                                                                                                                                                                                                                                                                                                                       * }"
	                                                                                                                                                                                                                                                                                                                       * on-clear-filter="$ctrl.clearFilter">
	                                                                                                                                                                                                                                                                                                                       * </ui-bb-transaction-search-filter-ng>
	                                                                                                                                                                                                                                                                                                                       */

/***/ }),

/***/ 78:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_78__;

/***/ }),

/***/ 79:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_79__;

/***/ }),

/***/ 80:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	var uiBbTransactionSearchFilterComponent = {
	  controller: 'uiBbTransactionSearchFilterController',
	  bindings: {
	    onFilter: '&',
	    buttonLabels: '<',
	    fieldLabels: '<',
	    onClearFilter: '='
	  },
	  template: '\n  <form data-role="search"\n    name="searchFilterForm"\n    data-ng-submit="$ctrl.onApplyFilter(searchFilterForm)"\n    data-ui-bb-track-changes="$ctrl.filterParams">\n    <div>\n      <div class="clearfix row">\n      <div class="col-md-6 pull-left">\n        <input\n          type="text"\n          class="form-control"\n          data-ng-model="$ctrl.filterParams.query"\n          data-role="search"\n          placeholder="{{ $ctrl.fieldLabels.search }}"\n          title="{{ $ctrl.fieldLabels.search }}"/>\n      </div>\n      <div class="col-md-1 pull-left">\n        <button\n          type="button"\n          class="btn btn-default"\n          data-role="filter"\n          data-ng-click="$ctrl.toggleFilter()"\n          title="{{ $ctrl.buttonLabels.main}}">\n          <i class="fa fa-sliders" aria-hidden="true"></i>\n          {{ $ctrl.buttonLabels.main }}\n        </button></div>\n      </div>\n    </div>\n    <div class="clearfix">\n      <div data-ng-show="$ctrl.state.isFilterOpen">\n        <div class="row">\n          <div class="col-md-12">\n            <label data-role="credit-debit-indicator-label">\n            {{ $ctrl.fieldLabels.creditDebitIndicator }}</label>\n          </div>\n        </div>\n        <div class="row">\n          <div class="form-group col-md-6">\n            <div class="input-group">\n              <select class="form-control"\n                data-ng-model="$ctrl.filterParams.creditDebitIndicator.value"\n                data-role="select-debit-credit">\n                <option value="" disabled></option>\n                <option data-role="credit"\n                  value="{{ $ctrl.filterParams.creditDebitIndicator.credit }}">\n                  {{ $ctrl.fieldLabels.credit }}\n                </option>\n                <option data-role="debit"\n                  value="{{ $ctrl.filterParams.creditDebitIndicator.debit }}">\n                  {{ $ctrl.fieldLabels.debit }}\n                </option>\n              </select>\n            </div>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12">\n            <label data-role="date-range-label">\n            {{ $ctrl.fieldLabels.dateRange }}</label>\n          </div>\n        </div>\n        <div class="row">\n          <div class="form-group col-md-6">\n            <div class="input-group">\n              <ui-bb-calendar-popup\n                data-config="$ctrl.fromDate.config"\n                data-date="$ctrl.fromDate.value"\n                disabled="false"\n                data-role="from-date">\n              </ui-bb-calendar-popup>\n            </div>\n          </div>\n          <div class="form-group col-md-6">\n            <div class="input-group">\n              <ui-bb-calendar-popup\n                data-config="$ctrl.toDate.config"\n                data-date="$ctrl.toDate.value"\n                disabled="!$ctrl.fromDate.value"\n                data-role="to-date">\n              </ui-bb-calendar-popup>\n            </div>\n          </div>\n        </div>\n        <div class="row">\n          <div class="col-md-12">\n            <label data-role="amount-range-label">{{ $ctrl.fieldLabels.amountRange }}</label>\n          </div>\n        </div>\n        <div class="row">\n          <div class="form-group col-md-6">\n            <div class="input-group">\n              <input\n                type="number"\n                min="0"\n                class="form-control"\n                data-ng-model="$ctrl.filterParams.amountFrom"\n                data-ng-keypress="$ctrl.validateAmountKeypress($event)"\n                data-ng-paste="$ctrl.validateAmountPaste($event)"\n                placeholder="{{ $ctrl.fieldLabels.amountFrom }}"\n                title="{{ $ctrl.fieldLabels.amountFrom }}"\n                data-role="amount-from"/>\n            </div>\n          </div>\n          <div class="form-group col-md-6">\n            <div class="input-group">\n              <input\n                type="number"\n                min="0"\n                class="form-control"\n                data-ng-model="$ctrl.filterParams.amountTo"\n                data-ng-keypress="$ctrl.validateAmountKeypress($event)"\n                data-ng-paste="$ctrl.validateAmountPaste($event)"\n                placeholder="{{ $ctrl.fieldLabels.amountTo }}"\n                title="{{ $ctrl.fieldLabels.amountTo }}"\n                data-role="amount-to"/>\n            </div>\n          </div>\n        </div>\n        <div class="row">\n          <div class="form-group col-md-3 pull-right">\n            <button\n              type="submit"\n              data-ng-disabled="$ctrl.isFormValid(searchFilterForm)"\n              class="btn btn-default btn-block"\n              title="{{ $ctrl.buttonLabels.apply }}"\n              data-role="apply">\n              {{ $ctrl.buttonLabels.apply }}\n            </button>\n          </div>\n          <div class="form-group col-md-3 pull-right">\n            <button\n              type="button"\n              class="btn btn-default btn-block"\n              data-ng-click="$ctrl.onClearFilter(searchFilterForm)"\n              title="{{ $ctrl.buttonLabels.cancel }}"\n              data-role="cancel">\n              {{ $ctrl.buttonLabels.cancel }}\n            </button>\n          </div>\n        </div>\n      </div>\n    </div>\n  </form>\n  '
	};
	
	exports.default = uiBbTransactionSearchFilterComponent;

/***/ }),

/***/ 81:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = uiBbTransactionSearchFilterController;
	function uiBbTransactionSearchFilterController($scope) {
	  /**
	   * @name uiBbTransactionSearchFilterController
	   * @ngkey uiBbTransactionSearchFilterController
	   * @type {object}
	   * @description TransactionSearch filter controller
	   */
	  var $ctrl = this;
	
	  /**
	  * @description
	  * state object.
	  *
	  * @name uiBbTransactionSearchFilterController#state
	  * @type {object}
	  */
	  var state = {
	    isFilterOpen: false
	  };
	
	  /**
	   * @description
	   * `filterParams` parameters object.
	   *
	   * @name uiBbTransactionSearchFilterController#filterParams
	   * @type {object}
	   */
	  var filterParams = {
	    query: '',
	    toDate: null,
	    fromDate: null,
	    amountFrom: null,
	    amountTo: null,
	    creditDebitIndicator: {
	      value: null,
	      credit: 'CRDT',
	      debit: 'DBIT'
	    }
	  };
	
	  var fromDate = {
	    value: null,
	    config: { minDate: null, maxDate: new Date(), maxMode: 'year' }
	  };
	
	  var toDate = {
	    value: null,
	    config: { minDate: null, maxDate: new Date(), maxMode: 'year' }
	  };
	
	  /**
	   * @description
	   * Watcher object evaluating current toDate param config.
	   *
	   * @type {object}
	   */
	  $scope.$watch(function () {
	    return fromDate.value;
	  }, function (newVal) {
	    toDate.config = Object.assign({}, toDate.config, {
	      minDate: newVal ? new Date(newVal) : null
	    });
	  });
	
	  /**
	   * @description
	   * Watcher object evaluation current fromDate param config.
	   *
	   * @type {object}
	   */
	  $scope.$watch(function () {
	    return toDate.value;
	  }, function (newVal) {
	    fromDate.config = Object.assign({}, fromDate.config, {
	      maxDate: newVal ? new Date(newVal) : new Date()
	    });
	  });
	
	  /**
	   * @description
	   * Toggle filter component.
	   *
	   * @public
	   * @name uiBbTransactionSearchFilterController#toggleFilter
	   * @type {function}
	   * @returns {boolean} A status of filter component
	   */
	  var toggleFilter = function toggleFilter() {
	    return state.isFilterOpen = !state.isFilterOpen;
	  };
	
	  /**
	   * @description
	   * Normalize date to required format.
	   *
	   * @inner
	   * @name uiBbTransactionSearchFilterController#getNormalizedDate
	   * @type {function}
	   * @returns {Date|null} Date object or null
	   */
	  var getNormalizedDate = function getNormalizedDate(date) {
	    if (date === null || date === undefined) {
	      return null;
	    }
	    var userOffset = date.getTimezoneOffset() * 60000;
	    return '' + new Date(date - userOffset).toISOString().slice(0, 10);
	  };
	
	  /**
	   * @description
	   * Normalize search query param.
	   *
	   * @inner
	   * @name uiBbTransactionSearchFilterController#normalizeQueryParam
	   * @type {function}
	   * @returns {Object|null} Object or null
	   */
	  var normalizeQueryParam = function normalizeQueryParam(param) {
	    return param === '' ? null : param;
	  };
	
	  /**
	   * @description
	   * Get all filter params.
	   *
	   * @inner
	   * @name uiBbTransactionSearchFilterController#getParams
	   * @type {function}
	   * @returns {Promise} A Promise that adds values to a given argument
	   */
	  var getParams = function getParams() {
	    return Object.assign({}, {
	      query: normalizeQueryParam(filterParams.query),
	      fromDate: getNormalizedDate(fromDate.value),
	      toDate: getNormalizedDate(toDate.value),
	      amountFrom: filterParams.amountFrom,
	      amountTo: filterParams.amountTo,
	      creditDebitIndicator: filterParams.creditDebitIndicator.value
	    });
	  };
	
	  /**
	   * @description
	   * Reset form state method.
	   *
	   * @inner
	   * @name uiBbTransactionSearchFilterController#resetFormState
	   * @type {function}
	   */
	  var resetFormState = function resetFormState(form) {
	    if (form) {
	      form.$setUntouched();
	      form.$setPristine();
	    }
	  };
	
	  /**
	   * @description
	   * Call filter method.
	   *
	   * @name uiBbTransactionSearchFilterController#onApplyFilter
	   * @type {function}
	   */
	  var onApplyFilter = function onApplyFilter(form) {
	    $ctrl.onFilter({ params: getParams() });
	    resetFormState(form);
	  };
	
	  /**
	   * @description
	   * Reset filter parameters.
	   *
	   * @name uiBbTransactionSearchFilterController#onClearFilter
	   * @type {function}
	   */
	  var onClearFilter = function onClearFilter(form) {
	    Object.assign(filterParams, {
	      query: '',
	      toDate: null,
	      fromDate: null,
	      amountFrom: null,
	      amountTo: null,
	      creditDebitIndicator: {
	        value: null,
	        credit: 'CRDT',
	        debit: 'DBIT'
	      }
	    });
	
	    Object.assign(fromDate, {
	      value: null,
	      config: { minDate: null, maxDate: new Date(), maxMode: 'year' }
	    });
	
	    Object.assign(toDate, {
	      value: null,
	      config: { minDate: null, maxDate: new Date(), maxMode: 'year' }
	    });
	
	    Object.assign(state, {
	      isFilterOpen: false
	    });
	
	    onApplyFilter(form);
	  };
	
	  /**
	   * @description
	   * Validates the key pressed in the number input field.
	   * Prevents the event if the key is an invalid one (not a number)
	   * Allows arrow keys
	   *
	   * @public
	   * @name uiBbTransactionSearchFilterController#validateAmountKeypress
	   * @type {function}
	   * @param {KeyboardEvent} event - the browser event
	   */
	  var validateAmountKeypress = function validateAmountKeypress(event) {
	    var validKey = event.metaKey // ctrl/alt/cmd
	    || event.which <= 0 // arrow keys
	    || event.which === 8 // delete key
	    || /[0-9]/.test(String.fromCharCode(event.which)); // numbers
	
	    if (!validKey) {
	      event.preventDefault();
	    }
	  };
	
	  /**
	   * @description
	   * Validates the pasted value in the input field.
	   * Prevents the event if the value is an invalid one (not a number)
	   *
	   * @public
	   * @name uiBbTransactionSearchFilterController#validateAmountPaste
	   * @type {function}
	   * @param {KeyboardEvent} event - the browser event
	   */
	  var validateAmountPaste = function validateAmountPaste(event) {
	    var pasteData = event.clipboardData.getData('text/plain');
	    if (pasteData.match(/[^0-9]/)) {
	      event.preventDefault();
	    }
	  };
	
	  /**
	   * @description
	   * Check if form state is pristine or invalid.
	   *
	   * @public
	   * @name uiBbTransactionSearchFilterController#isFormValid
	   * @type {function}
	   */
	  var isFormValid = function isFormValid(form) {
	    return !form || form.$pristine || form.$invalid;
	  };
	
	  Object.assign($ctrl, {
	    state: state,
	    toDate: toDate,
	    fromDate: fromDate,
	    filterParams: filterParams,
	    onApplyFilter: onApplyFilter,
	    onClearFilter: onClearFilter,
	    toggleFilter: toggleFilter,
	    isFormValid: isFormValid,
	    validateAmountKeypress: validateAmountKeypress,
	    validateAmountPaste: validateAmountPaste
	  });
	}

/***/ })

/******/ })
});
;
//# sourceMappingURL=ui-bb-transaction-search-filter-ng.js.map