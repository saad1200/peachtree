# ui-bb-transaction-search-filter-ng


Version: **1.2.47**

UI transaction-search filter component

## Imports

* ui-bb-calendar-popup-ng
* ui-bb-track-form-changes-ng
* vendor-bb-angular

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbTransactionSearchFilterKey from 'ui-bb-transaction-search-filter-ng';

export const dependencyKeys = [
  uiBbTransactionSearchFilterKey,
];

// file: templates/template.ng.html
<ui-bb-transaction-search-filter-ng
on-filter="$ctrl.filter()"
button-labels="{
 main: 'Filter',
 apply: 'Apply',
 cancel: 'Cancel',
}"
field-labels="{
 creditDebitIndicator: 'Credit/Debit indicator',
 credit: 'Credit',
 debit: 'Debit',
 dateRange: 'Date range',
 amountRange: 'Amount range',
 amountFrom: 'Amount from',
 amountTo: 'Amount to',
}"
on-clear-filter="$ctrl.clearFilter">
</ui-bb-transaction-search-filter-ng>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **uiBbTransactionSearchFilterController**<br/>    <a href="#uiBbTransactionSearchFilterController#state">#state</a><br/>    <a href="#uiBbTransactionSearchFilterController#filterParams">#filterParams</a><br/>    <a href="#uiBbTransactionSearchFilterController#toggleFilter">#toggleFilter()</a><br/>    <a href="#uiBbTransactionSearchFilterController#onApplyFilter">#onApplyFilter()</a><br/>    <a href="#uiBbTransactionSearchFilterController#onClearFilter">#onClearFilter()</a><br/>    <a href="#uiBbTransactionSearchFilterController#validateAmountKeypress">#validateAmountKeypress(event)</a><br/>    <a href="#uiBbTransactionSearchFilterController#validateAmountPaste">#validateAmountPaste(event)</a><br/>    <a href="#uiBbTransactionSearchFilterController#isFormValid">#isFormValid()</a><br/>

## Exports

### <a name="default"></a>*default*

The angular module name

**Type:** *String*


---

## uiBbTransactionSearchFilterController

TransactionSearch filter controller

| Injector Key |
| :-- |
| *uiBbTransactionSearchFilterController* |

### <a name="uiBbTransactionSearchFilterController#state"></a>*#state*

state object.

**Type:** *Object*

### <a name="uiBbTransactionSearchFilterController#filterParams"></a>*#filterParams*

`filterParams` parameters object.

**Type:** *Object*


### <a name="uiBbTransactionSearchFilterController#toggleFilter"></a>*#toggleFilter()*

Toggle filter component.

##### Returns

Boolean - *A status of filter component*

### <a name="uiBbTransactionSearchFilterController#onApplyFilter"></a>*#onApplyFilter()*

Call filter method.

### <a name="uiBbTransactionSearchFilterController#onClearFilter"></a>*#onClearFilter()*

Reset filter parameters.

### <a name="uiBbTransactionSearchFilterController#validateAmountKeypress"></a>*#validateAmountKeypress(event)*

Validates the key pressed in the number input field.
Prevents the event if the key is an invalid one (not a number)
Allows arrow keys

| Parameter | Type | Description |
| :-- | :-- | :-- |
| event | <a href="#KeyboardEvent">KeyboardEvent</a> | the browser event |

### <a name="uiBbTransactionSearchFilterController#validateAmountPaste"></a>*#validateAmountPaste(event)*

Validates the pasted value in the input field.
Prevents the event if the value is an invalid one (not a number)

| Parameter | Type | Description |
| :-- | :-- | :-- |
| event | <a href="#KeyboardEvent">KeyboardEvent</a> | the browser event |

### <a name="uiBbTransactionSearchFilterController#isFormValid"></a>*#isFormValid()*

Check if form state is pristine or invalid.
