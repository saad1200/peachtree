(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("vendor-bb-angular-sanitize"), require("vendor-bb-uib-modal"));
	else if(typeof define === 'function' && define.amd)
		define("ui-bb-confirm-ng", ["vendor-bb-angular", "vendor-bb-angular-sanitize", "vendor-bb-uib-modal"], factory);
	else if(typeof exports === 'object')
		exports["ui-bb-confirm-ng"] = factory(require("vendor-bb-angular"), require("vendor-bb-angular-sanitize"), require("vendor-bb-uib-modal"));
	else
		root["ui-bb-confirm-ng"] = factory(root["vendor-bb-angular"], root["vendor-bb-angular-sanitize"], root["vendor-bb-uib-modal"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_2__, __WEBPACK_EXTERNAL_MODULE_32__, __WEBPACK_EXTERNAL_MODULE_33__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(31);

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),

/***/ 31:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(2);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _vendorBbAngularSanitize = __webpack_require__(32);
	
	var _vendorBbAngularSanitize2 = _interopRequireDefault(_vendorBbAngularSanitize);
	
	var _vendorBbUibModal = __webpack_require__(33);
	
	var _vendorBbUibModal2 = _interopRequireDefault(_vendorBbUibModal);
	
	var _confirm = __webpack_require__(34);
	
	var _confirm2 = _interopRequireDefault(_confirm);
	
	var _confirm3 = __webpack_require__(35);
	
	var _confirm4 = _interopRequireDefault(_confirm3);
	
	var _instance = __webpack_require__(36);
	
	var _instance2 = _interopRequireDefault(_instance);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	/**
	 * @name default
	 * @type {string}
	 * @description The angular module name
	 */
	/**
	 * @module ui-bb-confirm-ng
	 * @description
	 * UI component for displaying the confirmation modal.
	 *
	 * @example
	 * // In an extension:
	 * // file: scripts/index.js
	 * import uiBbConfirmKey from 'ui-bb-confirm-ng';
	 *
	 * export const dependencyKeys = [
	 *   uiBbConfirmKey,
	 * ];
	 *
	 * // file: templates/template.ng.html
	 * <button ui-bb-confirm
	 *   heading="{{$ctrl.state.text.heading}}"
	 *   bodytext="{{$ctrl.state.text.body}}"
	 *   ok="{{$ctrl.state.text.ok}}"
	 *   cancel="{{$ctrl.state.text.cancel}}"
	 *   on-confirm="$ctrl.confirmSelection()"
	 *   aria-hidden="true">Confirm
	 * </button>
	 *
	 * <button ui-bb-confirm
	 *   heading="{{$ctrl.state.text.heading}}"
	 *   bodytext="{{$ctrl.state.text.body}}"
	 *   danger="{{$ctrl.state.text.delete}}"
	 *   cancel="{{$ctrl.state.text.cancel}}"
	 *   on-confirm="$ctrl.deleteContact()"
	 *   aria-hidden="true">Delete
	 * </button>
	 */
	exports.default = _vendorBbAngular2.default.module('ui-bb-confirm-ng', [_vendorBbUibModal2.default, _vendorBbAngularSanitize2.default]).directive('uiBbConfirm', _confirm2.default).controller('uiBbConfirmInstanceController', ['$uibModalInstance', _instance2.default]).controller('uiBbConfirmController', ['$scope', '$uibModal', _confirm4.default]).name;

/***/ }),

/***/ 32:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_32__;

/***/ }),

/***/ 33:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_33__;

/***/ }),

/***/ 34:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBbConfirmDirective
	 * @type {object}
	 *
	 * @property {string} bodytext Modal body text
	 * @property {string} heading Modal heading
	 * @property {string} ok Confirm button text
	 * @property {string} cancel Cancel button text
	 * @property {string} danger Danger action button text
	 * @property {string} size Optional modal size, default 'sm'
	 * @property {function} on-confirm Function to be invoked on clicking confirm button
	 * @property {function} on-cancel Function to be invoked on clicking cancel button
	 * @property {function} on-danger Function to be invoked on clicking danger action button
	 * @property {boolean} buttonsCentered Flag to instruct directive center footer buttons
	 * @property {boolean} skipConfirmation Flag to instruct directive to skip showing
	 * confirmation dialog and immediately execute on-confirm callback
	 */
	var uiBbConfirmDirective = function uiBbConfirmDirective() {
	  return {
	    restrict: 'A',
	    scope: {
	      bodytext: '@',
	      heading: '@',
	      ok: '@',
	      cancel: '@',
	      danger: '@',
	      size: '@',
	      onConfirm: '&',
	      onCancel: '&',
	      onDanger: '&',
	      buttonsCentered: '=?',
	      skipConfirmation: '=?'
	    },
	    link: function link(scope, element, attr, ctrl) {
	      element.on('click', ctrl.openConfirmModal);
	    },
	    controller: 'uiBbConfirmController'
	  };
	};
	
	exports.default = uiBbConfirmDirective;

/***/ }),

/***/ 35:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	/**
	 * @name uiBbConfirmController
	 * @ngkey uiBbConfirmController
	 * @type {function}
	 */
	var uiBbConfirmController = function uiBbConfirmController(scope, $uibModal) {
	  var modalTemplate = '\n    <div class="modal-header text-center">\n      <h3 class="modal-title" data-ng-bind="heading"></h3>\n    </div>\n    <div class="modal-body text-center" data-ng-bind-html="bodytext"></div>\n    <div class="modal-footer" data-ng-class="{ \'text-center\': buttonsCentered }">\n      <button class="btn btn-primary"\n        type="button"\n        data-ng-if="!!ok"\n        data-ng-bind="ok"\n        data-ng-click="instanceCtrl.ok($event)">\n      </button>\n      <button class="btn btn-danger"\n        type="button"\n        data-ng-if="!!danger"\n        data-ng-bind="danger"\n        data-ng-click="instanceCtrl.danger($event)">\n      </button>\n      <button class="btn btn-default"\n        type="button"\n        data-ng-bind="cancel || \'Cancel\'"\n        data-ng-click="instanceCtrl.cancel($event)">\n      </button>\n    </div>\n  ';
	
	  /**
	   * Executes confirm callback, if specified
	   * @type {function}
	   */
	  var confirm = function confirm() {
	    if (scope.onConfirm) {
	      scope.onConfirm();
	    }
	  };
	
	  /**
	   * Executes danger action callback, if specified
	   * @type {function}
	   */
	  var danger = function danger() {
	    if (scope.onDanger) {
	      scope.onDanger();
	    }
	  };
	
	  /**
	   * Executes cancel callback, if specified
	   * @type {function}
	   */
	  var cancel = function cancel() {
	    if (scope.onCancel) {
	      scope.onCancel();
	    }
	  };
	
	  /**
	   * Opens confirmation dialog.
	   * Unles "skipConfirmation" property is set to "true"
	   * @type {function}
	   */
	  var openConfirmModal = function openConfirmModal() {
	    // If "skip-confirmation" is set to true, immediatly execute confirmation callback
	    if (scope.skipConfirmation) {
	      scope.$apply(confirm);
	      return;
	    }
	
	    var modalInstance = $uibModal.open({
	      animation: false,
	      template: modalTemplate,
	      controller: 'uiBbConfirmInstanceController',
	      controllerAs: 'instanceCtrl',
	      size: scope.size || 'sm',
	      scope: scope
	    });
	
	    var affirmativeAction = function affirmativeAction() {
	      if (scope.danger) {
	        danger();
	      } else {
	        confirm();
	      }
	    };
	    modalInstance.result.then(affirmativeAction, cancel);
	  };
	
	  return { openConfirmModal: openConfirmModal };
	};
	
	exports.default = uiBbConfirmController;

/***/ }),

/***/ 36:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = uiBbConfirmInstanceController;
	/**
	 * @name uiBbConfirmInstanceController
	 * @ngkey uiBbConfirmInstanceController
	 * @type {function}
	 * @description
	 * Modal instance controller
	 */
	function uiBbConfirmInstanceController($uibModalInstance) {
	  var handleEvent = function handleEvent(event) {
	    event.stopPropagation();
	  };
	
	  var ok = function ok($event) {
	    handleEvent($event);
	    $uibModalInstance.close();
	  };
	
	  var danger = function danger($event) {
	    handleEvent($event);
	    $uibModalInstance.close();
	  };
	
	  var cancel = function cancel($event) {
	    handleEvent($event);
	    $uibModalInstance.dismiss('cancel');
	  };
	
	  return {
	    ok: ok,
	    danger: danger,
	    cancel: cancel
	  };
	}

/***/ })

/******/ })
});
;
//# sourceMappingURL=ui-bb-confirm-ng.js.map