# ui-bb-confirm-ng


Version: **1.1.69**

UI component for displaying the confirmation modal.

## Imports

* vendor-bb-angular
* vendor-bb-angular-sanitize
* vendor-bb-uib-modal

---

## Example

```javascript
// In an extension:
// file: scripts/index.js
import uiBbConfirmKey from 'ui-bb-confirm-ng';

export const dependencyKeys = [
  uiBbConfirmKey,
];

// file: templates/template.ng.html
<button ui-bb-confirm
  heading="{{$ctrl.state.text.heading}}"
  bodytext="{{$ctrl.state.text.body}}"
  ok="{{$ctrl.state.text.ok}}"
  cancel="{{$ctrl.state.text.cancel}}"
  on-confirm="$ctrl.confirmSelection()"
  aria-hidden="true">Confirm
</button>

<button ui-bb-confirm
  heading="{{$ctrl.state.text.heading}}"
  bodytext="{{$ctrl.state.text.body}}"
  danger="{{$ctrl.state.text.delete}}"
  cancel="{{$ctrl.state.text.cancel}}"
  on-confirm="$ctrl.deleteContact()"
  aria-hidden="true">Delete
</button>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **ui-bb-confirm-ng**<br/>    <a href="#ui-bb-confirm-nguiBbConfirmController">uiBbConfirmController()</a><br/>    <a href="#ui-bb-confirm-nguiBbConfirmInstanceController">uiBbConfirmInstanceController()</a><br/>

## Exports

### <a name="default"></a>*default*

The angular module name

**Type:** *String*


---

### <a name="ui-bb-confirm-nguiBbConfirmController"></a>*uiBbConfirmController()*


---

## uiBbConfirmDirective


| Property | Type | Description |
| :-- | :-- | :-- |
| bodytext | String | Modal body text |
| heading | String | Modal heading |
| ok | String | Confirm button text |
| cancel | String | Cancel button text |
| danger | String | Danger action button text |
| size | String | Optional modal size, default 'sm' |
| on-confirm | Function | Function to be invoked on clicking confirm button |
| on-cancel | Function | Function to be invoked on clicking cancel button |
| on-danger | Function | Function to be invoked on clicking danger action button |
| buttonsCentered | Boolean | Flag to instruct directive center footer buttons |
| skipConfirmation | Boolean | Flag to instruct directive to skip showing confirmation dialog and immediately execute on-confirm callback |

---

### <a name="ui-bb-confirm-nguiBbConfirmInstanceController"></a>*uiBbConfirmInstanceController()*

Modal instance controller
