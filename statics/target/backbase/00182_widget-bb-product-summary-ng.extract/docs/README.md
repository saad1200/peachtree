# widget-bb-product-summary-ng


Version: **1.0.80**

Product summary.

## Imports

* lib-bb-event-bus-ng
* lib-bb-model-errors
* lib-bb-widget-extension-ng
* lib-bb-widget-ng
* model-bb-product-summary-ng
* vendor-bb-angular

---

## Example

```javascript
<div ng-controller="ProductSummaryController as $ctrl">
   <ul>
      <li ng-repeat="product in $ctrl.products">{{product.id}}</li>
   </ul>
 </div>
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>
- **ProductSummaryController**<br/>    <a href="#ProductSummaryController#selectProduct">#selectProduct(product)</a><br/>    <a href="#ProductSummaryController#productSelected">#productSelected</a><br/>    <a href="#ProductSummaryController#productKinds">#productKinds</a><br/>    <a href="#ProductSummaryController#isProductLoading">#isProductLoading</a><br/>    <a href="#ProductSummaryController#productKindsError">#productKindsError</a><br/>    <a href="#ProductSummaryController#hasProducts">#hasProducts()</a><br/>    <a href="#ProductSummaryController#total">#total</a><br/>
- **Hooks**<br/>    <a href="#Hooks#processKinds">#processKinds(kinds)</a><br/>    <a href="#Hooks#processProductSelected">#processProductSelected(kinds)</a><br/>
- **Events**<br/>    <a href="#bb.event.product.selected">bb.event.product.selected</a><br/>
- **Type Definitions**<br/>    <a href="#TotalBalance">TotalBalance</a><br/>

## Exports

### <a name="default"></a>*default*

Angular module name

**Type:** *String*


---

## ProductSummaryController

Product summary controller.

| Injector Key |
| :-- |
| *ProductSummaryController* |


### <a name="ProductSummaryController#selectProduct"></a>*#selectProduct(product)*

Sets the alternate workflow when a user selects a Product from the overview.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| product | <a href="#Product">Product</a> | Product to select. |

##### Fires Events:

> bb.event.product.selected

> bb.event.product.selected.[product-kind]

### <a name="ProductSummaryController#productSelected"></a>*#productSelected*

The selected product.
The value returned from <a href="#Hooks.processProductSelected">Hooks.processProductSelected</a> hook

**Type:** *<a href="#any">any</a>*

### <a name="ProductSummaryController#productKinds"></a>*#productKinds*

The value returned from <a href="#Hooks.processKinds">Hooks.processKinds</a> hook.
null if the products aren't loaded.

**Type:** *<a href="#any">any</a>*

### <a name="ProductSummaryController#isProductLoading"></a>*#isProductLoading*

Loading status of the products

**Type:** *Boolean*

### <a name="ProductSummaryController#productKindsError"></a>*#productKindsError*

The error encountered when attempting to fetch the products from the model

**Type:** *<a href="#ModelError">ModelError</a>*


### <a name="ProductSummaryController#hasProducts"></a>*#hasProducts()*

Checks the list is empty or not

##### Returns

Boolean - *false if product list is empty*
### <a name="ProductSummaryController#total"></a>*#total*

The total balance for the products

**Type:** *<a href="#TotalBalance">TotalBalance</a>*


---

## Hooks

Hooks for widget-bb-product-summary-ng

### <a name="Hooks#processKinds"></a>*#processKinds(kinds)*

Hook for processing product kinds after initialization.
Assigned to [$ctrl.productKinds]<a href="#ProductSummaryController#productKinds">ProductSummaryController#productKinds</a>

| Parameter | Type | Description |
| :-- | :-- | :-- |
| kinds | Array of <a href="#ProductKind">ProductKind</a> | ProductKinds to process |

##### Returns

<a href="#any">any</a> - *Depends on hook implementation.*

### <a name="Hooks#processProductSelected"></a>*#processProductSelected(kinds)*

Hook for processing selected product after selection update
Assigned to [$ctrl.productSelected]<a href="#ProductSummaryController#productSelected">ProductSummaryController#productSelected</a>

| Parameter | Type | Description |
| :-- | :-- | :-- |
| kinds | <a href="#Product">Product</a> | Product to process |

##### Returns

<a href="#any">any</a> - *Depends on hook implementation.*

---

## Events

### <a name="bb.event.product.selected"></a>*bb.event.product.selected*

Triggered when product is selected.


---

## Type Definitions


### <a name="TotalBalance"></a>*TotalBalance*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| aggregatedBalance | String | aggregated balance |
| currency | String | currency code |

---

## Templates

* *template.ng.html*

---
