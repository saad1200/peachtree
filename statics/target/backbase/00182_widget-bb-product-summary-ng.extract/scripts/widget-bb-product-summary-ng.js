(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("model-bb-product-summary-ng"), require("lib-bb-event-bus-ng"));
	else if(typeof define === 'function' && define.amd)
		define("widget-bb-product-summary-ng", ["vendor-bb-angular", "lib-bb-widget-ng", "lib-bb-model-errors", "lib-bb-widget-extension-ng", "model-bb-product-summary-ng", "lib-bb-event-bus-ng"], factory);
	else if(typeof exports === 'object')
		exports["widget-bb-product-summary-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-widget-ng"), require("lib-bb-model-errors"), require("lib-bb-widget-extension-ng"), require("model-bb-product-summary-ng"), require("lib-bb-event-bus-ng"));
	else
		root["widget-bb-product-summary-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-widget-ng"], root["lib-bb-model-errors"], root["lib-bb-widget-extension-ng"], root["model-bb-product-summary-ng"], root["lib-bb-event-bus-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_44__, __WEBPACK_EXTERNAL_MODULE_59__, __WEBPACK_EXTERNAL_MODULE_61__, __WEBPACK_EXTERNAL_MODULE_79__, __WEBPACK_EXTERNAL_MODULE_80__, __WEBPACK_EXTERNAL_MODULE_82__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(86);

/***/ }),

/***/ 44:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_44__;

/***/ }),

/***/ 59:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_59__;

/***/ }),

/***/ 61:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_61__;

/***/ }),

/***/ 79:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_79__;

/***/ }),

/***/ 80:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_80__;

/***/ }),

/***/ 82:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_82__;

/***/ }),

/***/ 86:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _vendorBbAngular = __webpack_require__(44);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbWidgetExtensionNg = __webpack_require__(79);
	
	var _libBbWidgetExtensionNg2 = _interopRequireDefault(_libBbWidgetExtensionNg);
	
	var _libBbWidgetNg = __webpack_require__(59);
	
	var _libBbWidgetNg2 = _interopRequireDefault(_libBbWidgetNg);
	
	var _modelBbProductSummaryNg = __webpack_require__(80);
	
	var _modelBbProductSummaryNg2 = _interopRequireDefault(_modelBbProductSummaryNg);
	
	var _libBbEventBusNg = __webpack_require__(82);
	
	var _libBbEventBusNg2 = _interopRequireDefault(_libBbEventBusNg);
	
	var _controller = __webpack_require__(87);
	
	var _controller2 = _interopRequireDefault(_controller);
	
	var _defaultHooks = __webpack_require__(89);
	
	var defaultHooks = _interopRequireWildcard(_defaultHooks);
	
	function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var hooksKey = 'widget-bb-product-summary-ng:hooks';
	
	/**
	 * @name default
	 * @type {string}
	 * @description Angular module name
	 */
	/**
	 * @module widget-bb-product-summary-ng
	 *
	 * @description
	 * Product summary.
	 *
	 * @borrows module:model-bb-product-summary-ng.Product as Product
	 * @borrows module:model-bb-product-summary-ng.ProductKind as ProductKind
	 * @borrows module:lib-bb-model-errors.ModelError as ModelError
	 *
	 * @example
	 *  <div ng-controller="ProductSummaryController as $ctrl">
	 *    <ul>
	 *       <li ng-repeat="product in $ctrl.products">{{product.id}}</li>
	 *    </ul>
	 *  </div>
	 */
	exports.default = _vendorBbAngular2.default.module('widget-bb-product-summary-ng', [_modelBbProductSummaryNg2.default, _libBbEventBusNg2.default, _libBbWidgetNg2.default]).factory(hooksKey, (0, _libBbWidgetExtensionNg2.default)(defaultHooks)).controller('ProductSummaryController', [_modelBbProductSummaryNg.modelProductSummaryKey, hooksKey, _libBbEventBusNg.eventBusKey, _libBbWidgetNg.widgetKey,
	/* into */
	_controller2.default]).name;

/***/ }),

/***/ 87:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _errorMessages;
	
	exports.default = ProductSummaryController;
	
	var _libBbModelErrors = __webpack_require__(61);
	
	var _message = __webpack_require__(88);
	
	var _message2 = _interopRequireDefault(_message);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var PRODUCT_SELECTED = _message2.default.PRODUCT_SELECTED,
	    REFLOW_TRIGGERED = _message2.default.REFLOW_TRIGGERED;
	
	
	var errorMessages = (_errorMessages = {}, _defineProperty(_errorMessages, _libBbModelErrors.E_AUTH, 'model.error.auth'), _defineProperty(_errorMessages, _libBbModelErrors.E_CONNECTIVITY, 'model.error.connectivity'), _defineProperty(_errorMessages, _libBbModelErrors.E_USER, 'model.error.user'), _defineProperty(_errorMessages, _libBbModelErrors.E_UNEXPECTED, 'model.error.unexpected'), _errorMessages);
	
	var uiError = function uiError(messageMap, modelError) {
	  return {
	    message: messageMap[modelError.code]
	  };
	};
	
	function ProductSummaryController(model, hooks, eventBus, widget) {
	  /**
	   * @name ProductSummaryController
	   * @ngkey ProductSummaryController
	   * @type {object}
	   * @description
	   * Product summary controller.
	   *
	   */
	  var $ctrl = this;
	
	  /**
	   * @description
	   * Sets the alternate workflow when a user selects a Product from the overview.
	   *
	   * @name ProductSummaryController#selectProduct
	   * @type {function}
	   * @param {Product} product Product to select.
	   * @fires bb.event.product.selected
	   * @fires bb.event.product.selected.[product-kind]
	   */
	  var selectProduct = function selectProduct(product) {
	    model.setProductSelected(product);
	    eventBus.publish(PRODUCT_SELECTED, { product: product });
	    eventBus.publish(PRODUCT_SELECTED + '.' + product.kind, { product: product });
	  };
	
	  /**
	   * Handles account select
	   */
	  function updateProductSelected() {
	    model.getProductSelected().then(function (productSelected) {
	      $ctrl.productSelected = hooks.processProductSelected(productSelected);
	    });
	  }
	
	  /**
	   * Products loading logic
	   */
	  var loadProducts = function loadProducts() {
	    var forceLoad = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
	
	    $ctrl.isProductLoading = true;
	
	    return model.load(forceLoad).then(function (_ref) {
	      var productKinds = _ref.productKinds,
	          total = _ref.total;
	
	      $ctrl.isProductLoading = false;
	
	      $ctrl.total = total;
	      $ctrl.productKinds = hooks.processKinds(productKinds);
	    }).then(updateProductSelected).catch(function (error) {
	      $ctrl.isProductLoading = false;
	      $ctrl.productKindsError = uiError(errorMessages, error);
	    });
	  };
	
	  /**
	   * Adds subscriptions to bus events
	   */
	  function bindEvents() {
	    eventBus.subscribe(PRODUCT_SELECTED, updateProductSelected);
	    eventBus.subscribe(REFLOW_TRIGGERED, loadProducts);
	  }
	
	  /*
	   * Widget initialization logic.
	   */
	  var $onInit = function $onInit() {
	    /**
	     * This event (cxp.item.loaded) is deprecated in Mobile SDK version > 3.0
	     * and will be removed with the update to widget collection 3 (WC3)
	     */
	    eventBus.publish('cxp.item.loaded', {
	      id: widget.getId()
	    });
	
	    eventBus.publish('bb.item.loaded', {
	      id: widget.getId()
	    });
	
	    return loadProducts().then(bindEvents);
	  };
	
	  Object.assign($ctrl, {
	    /**
	     * @description
	     * The selected product.
	     * The value returned from {@link Hooks.processProductSelected} hook
	     *
	     * @name ProductSummaryController#productSelected
	     * @type {any}
	     */
	    productSelected: null,
	    /**
	     * @description
	     * The value returned from {@link Hooks.processKinds} hook.
	     * null if the products aren't loaded.
	     *
	     * @name ProductSummaryController#productKinds
	     * @type {any}
	     */
	    productKinds: null,
	    /**
	     * @description
	     * Loading status of the products
	     *
	     * @name ProductSummaryController#isProductLoading
	     * @type {boolean}
	     */
	    isProductLoading: false,
	    /**
	     * @description
	     * The error encountered when attempting to fetch the products from the model
	     *
	     * @name ProductSummaryController#productKindsError
	     * @type {ModelError}
	     */
	    productKindsError: null,
	    /**
	     * @description
	     * Checks the list is empty or not
	     *
	     * @name ProductSummaryController#hasProducts
	     * @type {function}
	     * @returns {boolean} false if product list is empty
	     */
	    hasProducts: function hasProducts() {
	      return !!$ctrl.productKinds.length;
	    },
	    /**
	     * @description
	     * The total balance for the products
	     *
	     * @name ProductSummaryController#total
	     * @type {TotalBalance}
	     */
	    total: null,
	    selectProduct: selectProduct,
	    $onInit: $onInit
	  });
	}
	
	/**
	 * @typedef {Object} TotalBalance
	 * @property {string} aggregatedBalance - aggregated balance
	 * @property {string} currency - currency code
	 */

/***/ }),

/***/ 88:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.default = {
	  /**
	  * Triggered when product is selected.
	  * @event bb.event.product.selected
	  * @type {Product}
	  */
	  PRODUCT_SELECTED: 'bb.event.product.selected',
	  REFLOW_TRIGGERED: 'bb.event.product.accounts.reflow'
	};

/***/ }),

/***/ 89:
/***/ (function(module, exports) {

	"use strict";
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.processKinds = processKinds;
	exports.processProductSelected = processProductSelected;
	// There is only 1 hook, otherwise there would be more named exports
	/* eslint-disable import/prefer-default-export */
	
	/**
	 * @name Hooks
	 * @type {object}
	 *
	 * @description
	 * Hooks for widget-bb-product-summary-ng
	 */
	
	/**
	 * @name Hooks#processKinds
	 * @type {function}
	 *
	 * @description
	 * Hook for processing product kinds after initialization.
	 * Assigned to [$ctrl.productKinds]{@link ProductSummaryController#productKinds}
	 *
	 * @param kinds {ProductKind[]} ProductKinds to process
	 * @returns {any} Depends on hook implementation.
	 */
	function processKinds(kinds) {
	  return kinds;
	}
	
	/**
	 * @name Hooks#processProductSelected
	 * @type {function}
	 *
	 * @description
	 * Hook for processing selected product after selection update
	 * Assigned to [$ctrl.productSelected]{@link ProductSummaryController#productSelected}
	 *
	 * @param kinds {Product} Product to process
	 * @returns {any} Depends on hook implementation.
	 */
	function processProductSelected(product) {
	  return product;
	}

/***/ })

/******/ })
});
;
//# sourceMappingURL=widget-bb-product-summary-ng.js.map