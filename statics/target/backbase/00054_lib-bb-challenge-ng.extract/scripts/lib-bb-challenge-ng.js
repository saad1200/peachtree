(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("vendor-bb-angular"), require("lib-bb-intent-ng"));
	else if(typeof define === 'function' && define.amd)
		define("lib-bb-challenge-ng", ["vendor-bb-angular", "lib-bb-intent-ng"], factory);
	else if(typeof exports === 'object')
		exports["lib-bb-challenge-ng"] = factory(require("vendor-bb-angular"), require("lib-bb-intent-ng"));
	else
		root["lib-bb-challenge-ng"] = factory(root["vendor-bb-angular"], root["lib-bb-intent-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_43__, __WEBPACK_EXTERNAL_MODULE_51__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(50);

/***/ }),

/***/ 43:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_43__;

/***/ }),

/***/ 50:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.bbChallengeKey = undefined;
	
	var _vendorBbAngular = __webpack_require__(43);
	
	var _vendorBbAngular2 = _interopRequireDefault(_vendorBbAngular);
	
	var _libBbIntentNg = __webpack_require__(51);
	
	var _libBbIntentNg2 = _interopRequireDefault(_libBbIntentNg);
	
	var _bbChallenge = __webpack_require__(52);
	
	var _bbChallenge2 = _interopRequireDefault(_bbChallenge);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var moduleKey = 'lib-bb-challenge-ng';
	/**
	 * The dependency injection key for the BbIntent Service
	 * @name bbIntentKey
	 * @type {string}
	 */
	var bbChallengeKey = exports.bbChallengeKey = moduleKey + ':challenge';
	
	/**
	 * @name default
	 * @type {string}
	 * @description Angular module name
	 */
	exports.default = _vendorBbAngular2.default.module(moduleKey, [_libBbIntentNg2.default]).factory(bbChallengeKey, [_libBbIntentNg.bbIntentKey, _bbChallenge2.default]).name;

/***/ }),

/***/ 51:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_51__;

/***/ }),

/***/ 52:
/***/ (function(module, exports) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	/**
	 * @module lib-bb-challenge
	 * @description Handles challenge response
	 * @example
	 * // index.js
	 * ///////////
	 * // Import lib-bb-challenge-ng
	 * import bbChallengeModuleKey, {
	 *   bbChallengeKey,
	 * } from 'lib-bb-challenge-ng';
	 *
	 * // Inject it in your module
	 * export default angular
	 *   .module(moduleKey, [
	 *     contactDataModuleKey,
	 *     ...
	 *     bbChallengeModuleKey,
	 *   ])
	 *   .factory(modelContactKey, [
	 *     ...
	 *     bbChallengeKey,
	 *     // Into
	 *     contactModel,
	 *   ])
	 *
	 * // model.js
	 * ///////////
	 * // Creates the wrapped method
	 * const postContactRecordChallenge = bbChallenge
	 *       .create(contactData.postContactRecord, {
	 *         id: 'contacts.auth.create',
	 *       });
	 *
	 *  // Call the method as if it was the original
	 *  postContactRecordChallenge(contact)
	 *   .then(handleSuccesss)
	 *   .catch(handleError);
	 *
	 */
	var noop = function noop() {};
	
	var intents = {};
	
	var challengeResponse = {
	  STATUS_CODE: '401',
	  HEADER_ATTRIBUTE: 'WWW-Authenticate'
	};
	
	var challengeRequest = {
	  HEADER_ATTRIBUTE: 'X-MFA'
	};
	
	var parseChallengeHeader = function parseChallengeHeader(header) {
	  var result = [];
	  var QUOTE = '"';
	  var DELIMITER = ',';
	  var KEY_VALUE_SEPARATOR = '=';
	
	  var next = header;
	  var keyLen = void 0;
	  var key = void 0;
	  var value = void 0;
	  var valueLen = void 0;
	  var valueIndex = void 0;
	  while (next.length > 0) {
	    keyLen = next.indexOf(KEY_VALUE_SEPARATOR);
	    key = next.substring(0, keyLen).trim();
	    next = next.substring(keyLen + 1);
	    value = null;
	    valueLen = 0;
	    valueIndex = 0;
	    if (next[0] === QUOTE) {
	      // Value is a string
	      do {
	        valueIndex++;
	        if (valueIndex > next.length) {
	          throw new Error('Malformed attribute value');
	        }
	
	        if (next[valueIndex] === QUOTE) {
	          if (next[valueIndex - 1] !== '\\') {
	            valueLen = valueIndex + 1;
	            value = next.substring(0, valueLen);
	          }
	        }
	      } while (!value);
	    } else {
	      // Value is a token
	      valueLen = next.indexOf(DELIMITER);
	      value = next.substring(valueIndex, valueLen).trim();
	    }
	    next = next.substring(valueLen + 1);
	    result.push('"' + key + '": ' + value);
	  }
	
	  return JSON.parse('{' + result.join(',') + '}');
	};
	
	var processChallengeResponse = function processChallengeResponse(response) {
	  return Object.entries(response).map(function (_ref) {
	    var _ref2 = _slicedToArray(_ref, 2),
	        key = _ref2[0],
	        value = _ref2[1];
	
	    if (typeof value === 'string') {
	      return key + '="' + value + '"';
	    }
	    return key + '=' + value;
	  }).join(', ');
	};
	
	var isChallengeStatus = function isChallengeStatus(responseStatus) {
	  return responseStatus === challengeResponse.STATUS_CODE;
	};
	
	var isChallengeResponse = function isChallengeResponse(status, header) {
	  return Boolean(isChallengeStatus(status) && header);
	};
	
	var bbChallengeHandler = function bbChallengeHandler(request, intentId) {
	  return new Promise(function (resolve, reject) {
	    request().then(resolve).catch(function (httpErrorResponse) {
	      var status = httpErrorResponse.status;
	      var header = httpErrorResponse.headers(challengeResponse.HEADER_ATTRIBUTE);
	
	      // If not challenge reject promise
	      if (!isChallengeResponse(status, header)) {
	        reject(httpErrorResponse);
	      }
	
	      // Config bbIntent with the resolve/reject method, replay call with headers
	      intents[intentId].onIntentResolved = function (response) {
	        var headerResult = processChallengeResponse(response);
	        var customHeaders = _defineProperty({}, challengeRequest.HEADER_ATTRIBUTE, headerResult);
	        intents[intentId].onIntentResolved = noop;
	        request(customHeaders).then(resolve).catch(reject);
	      };
	
	      intents[intentId].intent(parseChallengeHeader(header));
	    });
	  });
	};
	
	exports.default = function (bbIntent) {
	  return {
	    /**
	     * @name create
	     * @type {function}
	     * @description Creates a wrapped method that will check if it is receiving
	     * a challenge response, if so it will open an intent to handle it, then will
	     * wait for a response with the challenge resolution and will recreate the
	     * request with extra headers attributes to fulfill the challenge.
	     * Otherwise it will just bypass control to the method that is wrapping.
	     * @param  {function} method  Method to be wrapped, must return a promise
	     * @param  {object} options Options map
	     * @param  {string} options.id Id of the intent that will be created.
	     * @return {function}       Wrapped method
	     */
	    create: function create(method, options) {
	      var config = {
	        onIntentResolved: noop
	      };
	      config.intent = bbIntent.create(options.id, function (response) {
	        config.onIntentResolved(response);
	      });
	      bbIntent.init(noop);
	
	      intents[options.id] = config;
	
	      return function () {
	        for (var _len = arguments.length, params = Array(_len), _key = 0; _key < _len; _key++) {
	          params[_key] = arguments[_key];
	        }
	
	        var request = function request(headers) {
	          return method.apply(null, [].concat(params, [headers]));
	        };
	        return bbChallengeHandler(request, options.id);
	      };
	    }
	  };
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=lib-bb-challenge-ng.js.map