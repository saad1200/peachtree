# lib-bb-challenge


Version: **1.0.30**

Handles challenge response

## Imports

* lib-bb-intent-ng
* vendor-bb-angular

---

## Example

```javascript
// index.js
///////////
// Import lib-bb-challenge-ng
import bbChallengeModuleKey, {
  bbChallengeKey,
} from 'lib-bb-challenge-ng';

// Inject it in your module
export default angular
  .module(moduleKey, [
    contactDataModuleKey,
    ...
    bbChallengeModuleKey,
  ])
  .factory(modelContactKey, [
    ...
    bbChallengeKey,
    // Into
    contactModel,
  ])

// model.js
///////////
// Creates the wrapped method
const postContactRecordChallenge = bbChallenge
      .create(contactData.postContactRecord, {
        id: 'contacts.auth.create',
      });

 // Call the method as if it was the original
 postContactRecordChallenge(contact)
  .then(handleSuccesss)
  .catch(handleError);
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>    <a href="#bbIntentKey">bbIntentKey</a><br/>
- **lib-bb-challenge**<br/>    <a href="#lib-bb-challengecreate">create(method, options)</a><br/>

## Exports

### <a name="default"></a>*default*

Angular module name

**Type:** *String*

### <a name="bbIntentKey"></a>*bbIntentKey*

The dependency injection key for the BbIntent Service

**Type:** *String*


---

### <a name="lib-bb-challengecreate"></a>*create(method, options)*

Creates a wrapped method that will check if it is receiving
a challenge response, if so it will open an intent to handle it, then will
wait for a response with the challenge resolution and will recreate the
request with extra headers attributes to fulfill the challenge.
Otherwise it will just bypass control to the method that is wrapping.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| method | Function | Method to be wrapped, must return a promise |
| options | Object | Options map |
| options.id | String | Id of the intent that will be created. |

##### Returns

Function - *Wrapped method*
