# lib-bb-widget

Provides access to the details of the instance of the widget in the
portal, such as its ID and preferences.

## Example

```javascript
// file: index.js
import bbWidget from 'lib-bb-widget';

const widget = bbWidget(widgetInstance);
widget.getPreference('foo');
```

## Table of Contents
- **Widget**<br/>    <a href="#Widget#getPreference">#getPreference(name)</a><br/>    <a href="#Widget#getStringPreference">#getStringPreference(name)</a><br/>    <a href="#Widget#getLongPreference">#getLongPreference(name)</a><br/>    <a href="#Widget#getDoublePreference">#getDoublePreference(name)</a><br/>    <a href="#Widget#getBooleanPreference">#getBooleanPreference(name)</a><br/>    <a href="#Widget#getStringArrayPreference">#getStringArrayPreference(name)</a><br/>    <a href="#Widget#getNullPreference">#getNullPreference(name)</a><br/>    <a href="#Widget#getRawPreference">#getRawPreference(name)</a><br/>    <a href="#Widget#setPreference">#setPreference(name, value)</a><br/>    <a href="#Widget#savePreference">#savePreference(name, value)</a><br/>    <a href="#Widget#getId">#getId()</a><br/>
- **Type Definitions**<br/>    <a href="#WidgetAdapter">WidgetAdapter</a><br/>    <a href="#WidgetAdapter#GetPreference">WidgetAdapter#GetPreference(name)</a><br/>

---

## Widget

A service that provides access to the instance of the widget in the portal.


### <a name="Widget#getPreference"></a>*#getPreference(name)*


#### Deprecated: 0.2.3
Gets a preference from widget configuration (model.xml) and attempts to return it as a string

*DEPRECATED*
`getPreference` is deprecated in favor of the type specific `get<Type>Preference` methods.
This makes the use of the preferences in the context of a widget more reliable, as they are
parsed/coerced into the expected type in a reliable and consistent way, instead of ad-hoc
whenever a preference is used.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

String (optional) - *The parsed value of the preference*

### <a name="Widget#getStringPreference"></a>*#getStringPreference(name)*

Gets a preference from widget configuration (model.xml) and attempts to return it as a string


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

String (optional) - *The parsed value of the preference*

### <a name="Widget#getLongPreference"></a>*#getLongPreference(name)*

Gets a preference from widget configuration (model.xml) and attempts to return it as a integer
number.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

Number (optional) - *The parsed value of the preference*

### <a name="Widget#getDoublePreference"></a>*#getDoublePreference(name)*

Gets a preference from widget configuration (model.xml) and attempts to return it as a decimal
number.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

Number (optional) - *The parsed value of the preference*

### <a name="Widget#getBooleanPreference"></a>*#getBooleanPreference(name)*

Gets a preference from widget configuration (model.xml) and attempts to return it as a boolean.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

Boolean (optional) - *The parsed value of the preference*

### <a name="Widget#getStringArrayPreference"></a>*#getStringArrayPreference(name)*

Gets a preference from widget configuration (model.xml) and attempts to return it as an array
of strings, split on commas.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

Array (optional) of String - *The parsed value of the preference*

### <a name="Widget#getNullPreference"></a>*#getNullPreference(name)*

Gets a preference from widget configuration (model.xml) and attempts to return it as an null.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

Null (optional) - *The parsed value of the preference*

### <a name="Widget#getRawPreference"></a>*#getRawPreference(name)*

Gets a preference from widget as returned by portal client. This method is provided
as a "escape hatch" when none of the types methods work, but should generally be
avoided in common use due to its reliance on the underlying portal client implementation.

*N.B.* The return type is dependant on the underlying portal client implementation, and may
change across portal client versions.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |

##### Returns

<a href="#any">any</a> - *The value of the preference directly from the portal client*

### <a name="Widget#setPreference"></a>*#setPreference(name, value)*


#### Deprecated: 1.1.0
Sets a given value for a given preference

*DEPRECATED*
`setPreference` is deprecated in favor of `savePreference` which also persists the value to
the portal.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |
| value | String |  |

##### Returns

Object or String - *preference*

### <a name="Widget#savePreference"></a>*#savePreference(name, value)*

Sets a given value for a given preference and persists it to the portal.


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String |  |
| value | String |  |

##### Returns

Promise of <a href="#void">void</a> - **

### <a name="Widget#getId"></a>*#getId()*

Returns the widget's instance ID

##### Returns

String - **

## Type Definitions


### <a name="WidgetAdapter"></a>*WidgetAdapter*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String | The widget instance's unique identifier |
| getPreference | <a href="#WidgetAdapter#GetPreference">WidgetAdapter#GetPreference</a> | Get the named preference |
| setPreference | <a href="#WidgetAdapter#SetPreference">WidgetAdapter#SetPreference</a> | Set the names preference to value |
| saveModel | <a href="#WidgetAdapter#SaveModel">WidgetAdapter#SaveModel</a> | Persist the updated model preference values if required |
| contextRoot | String | The root path of the current portal/experience context |


### <a name="WidgetAdapter#GetPreference"></a>*WidgetAdapter#GetPreference(name)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| name | String | The name of the preference to get |

##### Returns

String or Undefined - *The current value of the requested preference*

---
