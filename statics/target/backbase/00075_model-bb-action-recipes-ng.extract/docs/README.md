# model-bb-action-recipes-ng


Version: **2.1.5**

Model for widget-bb-action-recipes-ng

## Imports

* data-bb-action-recipes-http-ng
* data-bb-product-summary-http-ng
* lib-bb-model-errors
* vendor-bb-angular

---

## Example

```javascript
import modelActionRecipesModuleKey, { modelActionRecipesKey } from 'model-bb-action-recipes-ng';

angular
  .module('ExampleModule', [
    modelActionRecipesModuleKey,
  ])
  .factory('someFactory', [
    modelActionRecipesKey,
    // into
    function someFactory(actionRecipesModel) {
      // ...
    },
  ]);
```

## Table of Contents
- **actionRecipesModel**<br/>    <a href="#actionRecipesModel#loadRecipes">#loadRecipes()</a><br/>    <a href="#actionRecipesModel#loadSpecifications">#loadSpecifications()</a><br/>    <a href="#actionRecipesModel#loadAccounts">#loadAccounts()</a><br/>    <a href="#actionRecipesModel#load">#load()</a><br/>    <a href="#actionRecipesModel#activate">#activate(recipe)</a><br/>    <a href="#actionRecipesModel#deactivate">#deactivate(recipe)</a><br/>

---

## actionRecipesModel

Action recipes  widget model service.

### <a name="actionRecipesModel#loadRecipes"></a>*#loadRecipes()*

Load users recipe data.

##### Returns

Promise of Object - *A Promise with an array of Recipes*

### <a name="actionRecipesModel#loadSpecifications"></a>*#loadSpecifications()*

Load available Recipe Specifications

##### Returns

Promise of Object - *A Promise with an array of specification data*

### <a name="actionRecipesModel#loadAccounts"></a>*#loadAccounts()*

Load users account data

##### Returns

Promise of Object - *A Promise with an array of account data*

### <a name="actionRecipesModel#load"></a>*#load()*

Load all necessary data to display Recipes:
Recipes, Specifications and Accounts.

##### Returns

Promise of Object - *A Promise with an array of account data*

### <a name="actionRecipesModel#activate"></a>*#activate(recipe)*

Activates given action recipe

| Parameter | Type | Description |
| :-- | :-- | :-- |
| recipe | Object | recipe to activate |

##### Returns

Promise - *promise which is resolved with the activated recipe*

### <a name="actionRecipesModel#deactivate"></a>*#deactivate(recipe)*

Deactivates given action recipe

| Parameter | Type | Description |
| :-- | :-- | :-- |
| recipe | Object | recipe to deactivate |

##### Returns

Promise - *promise which is resolved with deactivated recipe*
