(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("ui-bb-i18n-ng"), require("ui-bb-date-label-filter-ng"), require("ui-bb-inline-status-ng"), require("ui-bbm-list-ng"));
	else if(typeof define === 'function' && define.amd)
		define("ext-bbm-notification-center-list-ng", ["ui-bb-i18n-ng", "ui-bb-date-label-filter-ng", "ui-bb-inline-status-ng", "ui-bbm-list-ng"], factory);
	else if(typeof exports === 'object')
		exports["ext-bbm-notification-center-list-ng"] = factory(require("ui-bb-i18n-ng"), require("ui-bb-date-label-filter-ng"), require("ui-bb-inline-status-ng"), require("ui-bbm-list-ng"));
	else
		root["ext-bbm-notification-center-list-ng"] = factory(root["ui-bb-i18n-ng"], root["ui-bb-date-label-filter-ng"], root["ui-bb-inline-status-ng"], root["ui-bbm-list-ng"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_3__, __WEBPACK_EXTERNAL_MODULE_4__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_31__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ({

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(30);

/***/ }),

/***/ 3:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_3__;

/***/ }),

/***/ 4:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_4__;

/***/ }),

/***/ 8:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),

/***/ 30:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	exports.dependencyKeys = exports.helpers = undefined;
	
	var _uiBbI18nNg = __webpack_require__(3);
	
	var _uiBbI18nNg2 = _interopRequireDefault(_uiBbI18nNg);
	
	var _uiBbDateLabelFilterNg = __webpack_require__(4);
	
	var _uiBbDateLabelFilterNg2 = _interopRequireDefault(_uiBbDateLabelFilterNg);
	
	var _uiBbmListNg = __webpack_require__(31);
	
	var _uiBbmListNg2 = _interopRequireDefault(_uiBbmListNg);
	
	var _uiBbInlineStatusNg = __webpack_require__(8);
	
	var _uiBbInlineStatusNg2 = _interopRequireDefault(_uiBbInlineStatusNg);
	
	var _helpers = __webpack_require__(32);
	
	var _helpers2 = _interopRequireDefault(_helpers);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	var helpers = exports.helpers = _helpers2.default; /**
	                                                    * @module ext-bbm-notification-center-list-ng
	                                                    *
	                                                    * @description
	                                                    * Mobile extension for a notiication center list.
	                                                    *
	                                                    * @example
	                                                    * <!-- Contact widget model.xml -->
	                                                    * <property name="extension" viewHint="text-input,admin">
	                                                    *   <value type="string">ext-bbm-notification-list-ng</value>
	                                                    * </property>
	                                                    */
	var dependencyKeys = exports.dependencyKeys = [_uiBbI18nNg2.default, _uiBbDateLabelFilterNg2.default, _uiBbmListNg2.default, _uiBbInlineStatusNg2.default];

/***/ }),

/***/ 31:
/***/ (function(module, exports) {

	module.exports = __WEBPACK_EXTERNAL_MODULE_31__;

/***/ }),

/***/ 32:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _DateLabelKey;
	
	var _uiBbDateLabelFilterNg = __webpack_require__(4);
	
	function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
	
	var LevelIconClass = {
	  ALERT: 'fa-exclamation-circle',
	  WARNING: 'fa-exclamation-triangle',
	  INFO: 'fa-info-circle',
	  SUCCESS: 'fa-check-circle'
	};
	
	var DateLabelKey = (_DateLabelKey = {}, _defineProperty(_DateLabelKey, _uiBbDateLabelFilterNg.TimePeriod.NOW, 'calendar.label.now'), _defineProperty(_DateLabelKey, _uiBbDateLabelFilterNg.TimePeriod.TODAY, 'calendar.label.today'), _defineProperty(_DateLabelKey, _uiBbDateLabelFilterNg.TimePeriod.YESTERDAY, 'calendar.label.yesterday'), _DateLabelKey);
	
	exports.default = function (_ref) {
	  var $filter = _ref.$filter;
	
	  /**
	   * The standard ISO-8601 supports the following formats for time offsets:
	   * ±[hh]:[mm], ±[hh][mm], or ±[hh]
	   * However iOS does support only ±[hh]:[mm] format.
	   * Thus we make sure that the given date string has the following
	   * variation of the ISO-8601 standard:
	   * "YYYY-MM-DDThh:mm:ss.SSS±hh:mm"
	   * @name normalizeDate
	   * @inner
	   * @param dateStr
	   */
	  var normalizeDate = function normalizeDate(dateStr) {
	    var filteredDate = $filter('date')(dateStr, 'yyyy-MM-ddTHH:mm:ss.sssZ');
	    return filteredDate.replace(/(\d{2}):?(\d{2})$/, '$1:$2');
	  };
	
	  return {
	    getLevelIcon: function getLevelIcon(level) {
	      return LevelIconClass[level];
	    },
	
	    getDateLabel: function getDateLabel(notification) {
	      var date = normalizeDate(notification.createdOn);
	      var labelKey = void 0;
	      var resultDateLabel = void 0;
	
	      if (!notification.isOpen) {
	        labelKey = DateLabelKey[$filter('dateLabel')(date)];
	        if (labelKey) {
	          if (labelKey === DateLabelKey[_uiBbDateLabelFilterNg.TimePeriod.NOW]) {
	            resultDateLabel = $filter('i18n')(labelKey);
	          } else {
	            resultDateLabel = $filter('i18n')(labelKey) + $filter('i18n')('calendar.label.at') + $filter('date')(date, 'hh:mm');
	          }
	        }
	      }
	
	      return labelKey ? resultDateLabel : $filter('date')(date, 'd MMMM yyyy');
	    }
	  };
	};

/***/ })

/******/ })
});
;
//# sourceMappingURL=ext-bbm-notification-center-list-ng.js.map