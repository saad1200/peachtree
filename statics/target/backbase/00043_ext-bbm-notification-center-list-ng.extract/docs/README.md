# ext-bbm-notification-center-list-ng


Version: **1.0.31**

Mobile extension for a notiication center list.

## Imports

* ui-bb-date-label-filter-ng
* ui-bb-i18n-ng
* ui-bb-inline-status-ng
* ui-bbm-list-ng

---

## Example

```javascript
<!-- Contact widget model.xml -->
<property name="extension" viewHint="text-input,admin">
  <value type="string">ext-bbm-notification-list-ng</value>
</property>
```

## Table of Contents
