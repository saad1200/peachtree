# ext-bb-messages-inbox-ng


Version: **1.1.10**

Message center inbox extension.

## Imports

* ui-bb-confirm-ng
* ui-bb-conversation-list-ng
* ui-bb-conversation-view-ng
* ui-bb-i18n-ng
* ui-bb-notification-stripe-ng
* ui-bb-substitute-error-ng
* vendor-bb-uib-accordion
* vendor-bb-uib-pagination

---

## Example

```javascript
<!-- messages widget model.xml -->
<property name="extension" viewHint="text-input,admin">
 <value type="string">ext-bb-messages-inbox-ng</value>
</property>
```

## Table of Contents
- **ext-bb-messages-inbox-ng**<br/>    <a href="#ext-bb-messages-inbox-ngremoveSelectedConversations">removeSelectedConversations($ctrl)</a><br/>

---

### <a name="ext-bb-messages-inbox-ngremoveSelectedConversations"></a>*removeSelectedConversations($ctrl)*

Removes selected conversations and updates conversation list

| Parameter | Type | Description |
| :-- | :-- | :-- |
| $ctrl | Object | The extension controller |

##### Returns

Promise - *Promise to be fulfilled after loading list*
