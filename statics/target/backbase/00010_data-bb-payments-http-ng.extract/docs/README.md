# data-bb-payments-http-ng


Version: **1.3.0**

A data module for accessing the Payments REST API.

## Imports

* vendor-bb-angular

---

## Example

```javascript
import paymentsDataModuleKey, {
  paymentsDataKey,
} from 'data-bb-payments-http-ng';
```

## Table of Contents
- **Exports**<br/>    <a href="#default">default</a><br/>    <a href="#paymentsDataKey">paymentsDataKey</a><br/>
- **PaymentsData**<br/>    <a href="#PaymentsData#getPayments">#getPayments(params)</a><br/>    <a href="#PaymentsData#postPaymentsRecord">#postPaymentsRecord(data)</a><br/>    <a href="#PaymentsData#getPaymentsRecord">#getPaymentsRecord(paymentId, params)</a><br/>    <a href="#PaymentsData#deletePaymentsRecord">#deletePaymentsRecord(paymentId, data)</a><br/>    <a href="#PaymentsData#getPaymentsStandingOrders">#getPaymentsStandingOrders(params)</a><br/>    <a href="#PaymentsData#getPaymentsAuthorizations">#getPaymentsAuthorizations(params)</a><br/>    <a href="#PaymentsData#getPaymentsDirectDebits">#getPaymentsDirectDebits(params)</a><br/>    <a href="#PaymentsData#getPaymentsCurrencies">#getPaymentsCurrencies(params)</a><br/>    <a href="#PaymentsData#getPaymentsRate">#getPaymentsRate(params)</a><br/>    <a href="#PaymentsData#postPaymentsAuthorizationsRecord">#postPaymentsAuthorizationsRecord(paymentId, data)</a><br/>    <a href="#PaymentsData#postPaymentsRejectionsRecord">#postPaymentsRejectionsRecord(paymentId, data)</a><br/>    <a href="#PaymentsData#postPaymentsDirectDebitsRefundsRecord">#postPaymentsDirectDebitsRefundsRecord(directDebitId, data)</a><br/>    <a href="#PaymentsData#postPaymentsDirectDebitsRefusalsRecord">#postPaymentsDirectDebitsRefusalsRecord(directDebitId, data)</a><br/>    <a href="#PaymentsData#schemas">#schemas</a><br/>    <a href="#PaymentsData#schemas.postPaymentsRecord">#schemas.postPaymentsRecord</a><br/>
- **PaymentsDataProvider**<br/>    <a href="#PaymentsDataProvider#setBaseUri">#setBaseUri(baseUri)</a><br/>    <a href="#PaymentsDataProvider#$get">#$get()</a><br/>
- **Type Definitions**<br/>    <a href="#PaymentsData.DirectDebit">PaymentsData.DirectDebit</a><br/>    <a href="#PaymentsData.DirectDebits">PaymentsData.DirectDebits</a><br/>    <a href="#Response">Response</a><br/>

## Exports

### <a name="default"></a>*default*

Angular dependency injection module key

**Type:** *String*

### <a name="paymentsDataKey"></a>*paymentsDataKey*

Angular dependency injection key for the PaymentsData service

**Type:** *String*


---

## PaymentsData

Public api for data-bb-payments-http-ng service

### <a name="PaymentsData#getPayments"></a>*#getPayments(params)*

Retrieve list of payments.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.from | Number (optional) | Skip over a page of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field. |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .getPayments(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#postPaymentsRecord"></a>*#postPaymentsRecord(data)*

Create new payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .postPaymentsRecord(data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#getPaymentsRecord"></a>*#getPaymentsRecord(paymentId, params)*

Retrieve single payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentId | String |  |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .getPaymentsRecord(paymentId, params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#deletePaymentsRecord"></a>*#deletePaymentsRecord(paymentId, data)*

Delete a single payment by Id

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .deletePaymentsRecord(paymentId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#getPaymentsStandingOrders"></a>*#getPaymentsStandingOrders(params)*

Retrieve list of standing orders

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.from | Number (optional) | Skip over a page of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field. |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .getPaymentsStandingOrders(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#getPaymentsAuthorizations"></a>*#getPaymentsAuthorizations(params)*

Retrieve list of only payments, which can be authorized or rejected.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.from | Number (optional) | Skip over a page of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field. |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .getPaymentsAuthorizations(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#getPaymentsDirectDebits"></a>*#getPaymentsDirectDebits(params)*

Retrieve list of direct debits.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object (optional) | Map of query parameters. |
| params.from | Number (optional) | Skip over a page of elements by specifying a start value for the query. Eg: 20. (defaults to 0) |
| params.cursor | String (optional) | As an alternative for specifying 'from' this allows to point to the record to start the selection from. Eg: 76d5be8b-e80d-4842-8ce6-ea67519e8f74. (defaults to "") |
| params.size | Number (optional) | Limit the number of elements on the response. When used in combination with cursor, the value is allowed to be a negative number to indicate requesting records upwards from the starting point indicated by the cursor. Eg: 80. (defaults to 10) |
| params.orderBy | String (optional) | Order by field. |
| params.direction | String (optional) | Direction. (defaults to DESC) |

##### Returns

Promise of <a href="#Response">Response</a> - *Resolves data value as <a href="#PaymentsData.DirectDebits">PaymentsData.DirectDebits</a> on success*

## Example

```javascript
paymentsData
 .getPaymentsDirectDebits(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#getPaymentsCurrencies"></a>*#getPaymentsCurrencies(params)*

Get currencies available for payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .getPaymentsCurrencies(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#getPaymentsRate"></a>*#getPaymentsRate(params)*

Get available rate for currencies.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| params | Object | Map of query parameters. |
| params.currencyFrom | String | Currency transfer from. Eg: EUR. |
| params.currencyTo | String | Currency transfer to. Eg: USD. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .getPaymentsRate(params)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#postPaymentsAuthorizationsRecord"></a>*#postPaymentsAuthorizationsRecord(paymentId, data)*

Authorize payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .postPaymentsAuthorizationsRecord(paymentId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#postPaymentsRejectionsRecord"></a>*#postPaymentsRejectionsRecord(paymentId, data)*

Reject payment.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| paymentId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .postPaymentsRejectionsRecord(paymentId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#postPaymentsDirectDebitsRefundsRecord"></a>*#postPaymentsDirectDebitsRefundsRecord(directDebitId, data)*

Refund direct debit.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| directDebitId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .postPaymentsDirectDebitsRefundsRecord(directDebitId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```

### <a name="PaymentsData#postPaymentsDirectDebitsRefusalsRecord"></a>*#postPaymentsDirectDebitsRefusalsRecord(directDebitId, data)*

Refuse direct debit.

| Parameter | Type | Description |
| :-- | :-- | :-- |
| directDebitId | String |  |
| data | Object (optional) | Data to be sent as the request message data. |

##### Returns

Promise of <a href="#Response">Response</a> - **

## Example

```javascript
paymentsData
 .postPaymentsDirectDebitsRefusalsRecord(directDebitId, data)
 .then(function(result){
   console.log('headers', result.headers)
   console.log('data', result.data);
 });
```
### <a name="PaymentsData#schemas"></a>*#schemas*

Schema data. Keys of the object are names of the POST and PUT methods

Note: The schema is not strictly a JSON schema. It is a whitelisted set of
keys for each object property. The keys that are exposed are meant for validation
purposes.

The full list of *possible* keys for each property is:
type, minimum, maximum, minLength, maxLength, pattern, enum, format, default,
properties, items, minItems, maxItems, uniqueItems and required.

See http://json-schema.org/latest/json-schema-validation.html for more details
on the meaning of these keys.

The "required" array from JSON schema is tranformed into a "required" boolean
on each property. This is for ease of use.

**Type:** *Object*

### <a name="PaymentsData#schemas.postPaymentsRecord"></a>*#schemas.postPaymentsRecord*

An object describing the JSON schema for the postPaymentsRecord method

**Type:** *Object*


## Example

```javascript
{
  "properties": {
    "debitAccountIdentification": {
      "type": "object",
      "properties": {
        "counterpartyName": {
          "type": "string",
          "maxLength": 256,
          "required": true
        },
        "scheme": {
          "type": "string",
          "enum": [
            "BBAN",
            "IBAN",
            "ID"
          ],
          "required": true
        },
        "identification": {
          "type": "string",
          "required": true
        },
        "counterpartyBIC": {
          "type": "string",
          "pattern": "[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}",
          "required": false
        },
        "counterpartyCountry": {
          "type": "string",
          "minLength": 2,
          "maxLength": 2,
          "required": false
        },
        "counterpartyBankName": {
          "type": "string",
          "maxLength": 256,
          "required": false
        }
      },
      "required": false
    },
    "creditAccountIdentification": {
      "type": "object",
      "properties": {
        "counterpartyName": {
          "type": "string",
          "maxLength": 256,
          "required": true
        },
        "scheme": {
          "type": "string",
          "enum": [
            "BBAN",
            "IBAN",
            "ID"
          ],
          "required": true
        },
        "identification": {
          "type": "string",
          "required": true
        },
        "counterpartyBIC": {
          "type": "string",
          "pattern": "[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}",
          "required": false
        },
        "counterpartyCountry": {
          "type": "string",
          "minLength": 2,
          "maxLength": 2,
          "required": false
        },
        "counterpartyBankName": {
          "type": "string",
          "maxLength": 256,
          "required": false
        }
      },
      "required": false
    },
    "amount": {
      "type": "string",
      "required": true
    },
    "currency": {
      "type": "string",
      "pattern": "^[A-Z]{3}$",
      "required": true
    },
    "paymentMode": {
      "type": "string",
      "enum": [
        "SINGLE",
        "RECURRING"
      ],
      "default": "SINGLE",
      "required": true
    },
    "date": {
      "type": "string",
      "format": "date",
      "required": false
    },
    "schedule": {
      "type": "object",
      "properties": {
        "nonWorkingDayExecutionStrategy": {
          "type": "string",
          "enum": [
            "BEFORE",
            "AFTER",
            "NONE"
          ],
          "required": false
        },
        "transferFrequency": {
          "type": "string",
          "enum": [
            "ONCE",
            "DAILY",
            "WEEKLY",
            "MONTHLY",
            "YEARLY"
          ],
          "required": true
        },
        "on": {
          "type": "integer",
          "required": true
        },
        "startDate": {
          "type": "string",
          "format": "date",
          "required": true
        },
        "endDate": {
          "type": "string",
          "format": "date",
          "required": false
        },
        "repeat": {
          "type": "integer",
          "required": false
        },
        "every": {
          "type": "integer",
          "enum": [
            1,
            2
          ],
          "required": true
        },
        "nextExecutionDate": {
          "type": "string",
          "format": "date",
          "required": false
        }
      },
      "required": false
    },
    "description": {
      "type": "string",
      "maxLength": 256,
      "required": false
    },
    "urgent": {
      "type": "boolean",
      "required": false
    },
    "paymentReference": {
      "type": "string",
      "maxLength": 256,
      "required": false
    },
    "createdBy": {
      "type": "string",
      "maxLength": 256,
      "required": false
    },
    "createdAt": {
      "type": "string",
      "format": "date-time",
      "required": false
    }
  }
}
```

---

## PaymentsDataProvider

Data service that can be configured with custom base URI.

| Injector Key |
| :-- |
| *data-bb-payments-http-ng:paymentsDataProvider* |


### <a name="PaymentsDataProvider#setBaseUri"></a>*#setBaseUri(baseUri)*


| Parameter | Type | Description |
| :-- | :-- | :-- |
| baseUri | String | Base URI which will be the prefix for all HTTP requests |

### <a name="PaymentsDataProvider#$get"></a>*#$get()*


##### Returns

Object - *An instance of the service*

## Example

```javascript
// Configuring in an angular app:
angular.module(...)
  .config(['data-bb-payments-http-ng:paymentsDataProvider',
    (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
      });

// Configuring With config-bb-providers-ng:
export default [
  ['data-bb-payments-http-ng:paymentsDataProvider', (dataProvider) => {
      dataProvider.setBaseUri('http://my-service.com/');
  }]
];
```

## Type Definitions


### <a name="PaymentsData.DirectDebit"></a>*PaymentsData.DirectDebit*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| id | String |  |
| status | String | One of "ANNOUNCED", "PROCESSED", "REJECTED", "REFUNDED", "REFUSED", "REFUND_REQUESTED", "REFUSAL_REQUESTED" |
| refundDayCount | <a href="#Integer">Integer</a> (optional) | The number of days that indicates how many days the refund is available after the direct debit was processed |
| creditorReference | String | Client reference creditor alphanumeric number like 123AB0078 |
| mandateReference | String | Client reference mandate alphanumeric number like 123AB0078 |
| additions | Object (optional) | Container object for custom API extensions |

### <a name="PaymentsData.DirectDebits"></a>*PaymentsData.DirectDebits*


**Type:** *Array of <a href="#PaymentsData.DirectDebit">PaymentsData.DirectDebit</a>*


### <a name="Response"></a>*Response*


**Type:** *Object*


| Property | Type | Description |
| :-- | :-- | :-- |
| data | Object | See method descriptions for possible return types |
| headers | Function | Getter headers function |
| status | Number | HTTP status code of the response. |
| statusText | String | HTTP status text of the response. |

---
